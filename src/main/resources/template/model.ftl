<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument
	xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
	xmlns:v="urn:schemas-microsoft-com:vml"
	xmlns:w10="urn:schemas-microsoft-com:office:word"
	xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core"
	xmlns:aml="http://schemas.microsoft.com/aml/2001/core"
	xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
	xmlns:o="urn:schemas-microsoft-com:office:office"
	xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
	w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no"
	xml:space="preserve">
	<o:DocumentProperties>
		<o:Author>ENZ-PC</o:Author>
		<o:LastAuthor>%E9%AA%9A%E5%B9%B4%E3%81%AE%E6%9C%8B%E5%</o:LastAuthor>
		<o:Created>2014-10-29T12:08:00Z</o:Created>
		<o:LastSaved>2019-05-21T08:56:29Z</o:LastSaved>
		<o:TotalTime>4320</o:TotalTime>
		<o:Pages>1</o:Pages>
		<o:Words>0</o:Words>
		<o:Characters>0</o:Characters>
		<o:Lines>0</o:Lines>
		<o:Paragraphs>0</o:Paragraphs>
		<o:CharactersWithSpaces>0</o:CharactersWithSpaces>
		<o:Version>14</o:Version>
	</o:DocumentProperties>
	<o:CustomDocumentProperties>
		<o:KSOProductBuildVer dt:dt="string">2052-11.1.0.8696</o:KSOProductBuildVer>
	</o:CustomDocumentProperties>
	<w:fonts>
		<w:defaultFonts w:ascii="Times New Roman" w:fareast="宋体"
			w:h-ansi="Times New Roman" w:cs="Times New Roman" />
		<w:font w:name="Times New Roman">
			<w:panose-1 w:val="02020603050405020304" />
			<w:charset w:val="00" />
			<w:family w:val="Auto" />
			<w:pitch w:val="Default" />
			<w:sig w:usb-0="E0002EFF" w:usb-1="C0007843" w:usb-2="00000009"
				w:usb-3="00000000" w:csb-0="400001FF" w:csb-1="FFFF0000" />
		</w:font>
		<w:font w:name="宋体">
			<w:panose-1 w:val="02010600030101010101" />
			<w:charset w:val="86" />
			<w:family w:val="Auto" />
			<w:pitch w:val="Default" />
			<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000006"
				w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000" />
		</w:font>
		<w:font w:name="Wingdings">
			<w:panose-1 w:val="05000000000000000000" />
			<w:charset w:val="02" />
			<w:family w:val="Auto" />
			<w:pitch w:val="Default" />
			<w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000"
				w:usb-3="00000000" w:csb-0="80000000" w:csb-1="00000000" />
		</w:font>
		<w:font w:name="Arial">
			<w:panose-1 w:val="020B0604020202020204" />
			<w:charset w:val="01" />
			<w:family w:val="SWiss" />
			<w:pitch w:val="Default" />
			<w:sig w:usb-0="E0002AFF" w:usb-1="C0007843" w:usb-2="00000009"
				w:usb-3="00000000" w:csb-0="400001FF" w:csb-1="FFFF0000" />
		</w:font>
		<w:font w:name="黑体">
			<w:panose-1 w:val="02010609060101010101" />
			<w:charset w:val="86" />
			<w:family w:val="Auto" />
			<w:pitch w:val="Default" />
			<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016"
				w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000" />
		</w:font>
		<w:font w:name="Courier New">
			<w:panose-1 w:val="02070309020205020404" />
			<w:charset w:val="01" />
			<w:family w:val="Modern" />
			<w:pitch w:val="Default" />
			<w:sig w:usb-0="E0002AFF" w:usb-1="C0007843" w:usb-2="00000009"
				w:usb-3="00000000" w:csb-0="400001FF" w:csb-1="FFFF0000" />
		</w:font>
		<w:font w:name="Symbol">
			<w:panose-1 w:val="05050102010706020507" />
			<w:charset w:val="02" />
			<w:family w:val="Roman" />
			<w:pitch w:val="Default" />
			<w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000"
				w:usb-3="00000000" w:csb-0="80000000" w:csb-1="00000000" />
		</w:font>
		<w:font w:name="Calibri">
			<w:panose-1 w:val="020F0502020204030204" />
			<w:charset w:val="00" />
			<w:family w:val="SWiss" />
			<w:pitch w:val="Default" />
			<w:sig w:usb-0="E00002FF" w:usb-1="4000ACFF" w:usb-2="00000001"
				w:usb-3="00000000" w:csb-0="2000019F" w:csb-1="00000000" />
		</w:font>
		<w:font w:name="微软雅黑">
			<w:panose-1 w:val="020B0503020204020204" />
			<w:charset w:val="86" />
			<w:family w:val="Auto" />
			<w:pitch w:val="Default" />
			<w:sig w:usb-0="A0000287" w:usb-1="28CF3C52" w:usb-2="00000016"
				w:usb-3="00000000" w:csb-0="0004001F" w:csb-1="00000000" />
		</w:font>
		<w:font w:name="Arial Unicode MS">
			<w:panose-1 w:val="020B0604020202020204" />
			<w:charset w:val="86" />
			<w:family w:val="Auto" />
			<w:pitch w:val="Default" />
			<w:sig w:usb-0="FFFFFFFF" w:usb-1="E9FFFFFF" w:usb-2="0000003F"
				w:usb-3="00000000" w:csb-0="603F01FF" w:csb-1="FFFF0000" />
		</w:font>
		<w:font w:name="Courier New">
			<w:panose-1 w:val="02070309020205020404" />
			<w:charset w:val="86" />
			<w:family w:val="Auto" />
			<w:pitch w:val="Default" />
			<w:sig w:usb-0="E0002AFF" w:usb-1="C0007843" w:usb-2="00000009"
				w:usb-3="00000000" w:csb-0="400001FF" w:csb-1="FFFF0000" />
		</w:font>
	</w:fonts>
	<w:styles>
		<w:latentStyles w:defLockedState="off" w:latentStyleCount="260">
			<w:lsdException w:name="Normal" />
			<w:lsdException w:name="heading 1" />
			<w:lsdException w:name="heading 2" />
			<w:lsdException w:name="heading 3" />
			<w:lsdException w:name="heading 4" />
			<w:lsdException w:name="heading 5" />
			<w:lsdException w:name="heading 6" />
			<w:lsdException w:name="heading 7" />
			<w:lsdException w:name="heading 8" />
			<w:lsdException w:name="heading 9" />
			<w:lsdException w:name="index 1" />
			<w:lsdException w:name="index 2" />
			<w:lsdException w:name="index 3" />
			<w:lsdException w:name="index 4" />
			<w:lsdException w:name="index 5" />
			<w:lsdException w:name="index 6" />
			<w:lsdException w:name="index 7" />
			<w:lsdException w:name="index 8" />
			<w:lsdException w:name="index 9" />
			<w:lsdException w:name="toc 1" />
			<w:lsdException w:name="toc 2" />
			<w:lsdException w:name="toc 3" />
			<w:lsdException w:name="toc 4" />
			<w:lsdException w:name="toc 5" />
			<w:lsdException w:name="toc 6" />
			<w:lsdException w:name="toc 7" />
			<w:lsdException w:name="toc 8" />
			<w:lsdException w:name="toc 9" />
			<w:lsdException w:name="Normal Indent" />
			<w:lsdException w:name="footnote text" />
			<w:lsdException w:name="annotation text" />
			<w:lsdException w:name="header" />
			<w:lsdException w:name="footer" />
			<w:lsdException w:name="index heading" />
			<w:lsdException w:name="caption" />
			<w:lsdException w:name="table of figures" />
			<w:lsdException w:name="envelope address" />
			<w:lsdException w:name="envelope return" />
			<w:lsdException w:name="footnote reference" />
			<w:lsdException w:name="annotation reference" />
			<w:lsdException w:name="line number" />
			<w:lsdException w:name="page number" />
			<w:lsdException w:name="endnote reference" />
			<w:lsdException w:name="endnote text" />
			<w:lsdException w:name="table of authorities" />
			<w:lsdException w:name="macro" />
			<w:lsdException w:name="toa heading" />
			<w:lsdException w:name="List" />
			<w:lsdException w:name="List Bullet" />
			<w:lsdException w:name="List Number" />
			<w:lsdException w:name="List 2" />
			<w:lsdException w:name="List 3" />
			<w:lsdException w:name="List 4" />
			<w:lsdException w:name="List 5" />
			<w:lsdException w:name="List Bullet 2" />
			<w:lsdException w:name="List Bullet 3" />
			<w:lsdException w:name="List Bullet 4" />
			<w:lsdException w:name="List Bullet 5" />
			<w:lsdException w:name="List Number 2" />
			<w:lsdException w:name="List Number 3" />
			<w:lsdException w:name="List Number 4" />
			<w:lsdException w:name="List Number 5" />
			<w:lsdException w:name="Title" />
			<w:lsdException w:name="Closing" />
			<w:lsdException w:name="Signature" />
			<w:lsdException w:name="Default Paragraph Font" />
			<w:lsdException w:name="Body Text" />
			<w:lsdException w:name="Body Text Indent" />
			<w:lsdException w:name="List Continue" />
			<w:lsdException w:name="List Continue 2" />
			<w:lsdException w:name="List Continue 3" />
			<w:lsdException w:name="List Continue 4" />
			<w:lsdException w:name="List Continue 5" />
			<w:lsdException w:name="Message Header" />
			<w:lsdException w:name="Subtitle" />
			<w:lsdException w:name="Salutation" />
			<w:lsdException w:name="Date" />
			<w:lsdException w:name="Body Text First Indent" />
			<w:lsdException w:name="Body Text First Indent 2" />
			<w:lsdException w:name="Note Heading" />
			<w:lsdException w:name="Body Text 2" />
			<w:lsdException w:name="Body Text 3" />
			<w:lsdException w:name="Body Text Indent 2" />
			<w:lsdException w:name="Body Text Indent 3" />
			<w:lsdException w:name="Block Text" />
			<w:lsdException w:name="Hyperlink" />
			<w:lsdException w:name="FollowedHyperlink" />
			<w:lsdException w:name="Strong" />
			<w:lsdException w:name="Emphasis" />
			<w:lsdException w:name="Document Map" />
			<w:lsdException w:name="Plain Text" />
			<w:lsdException w:name="E-mail Signature" />
			<w:lsdException w:name="Normal (Web)" />
			<w:lsdException w:name="HTML Acronym" />
			<w:lsdException w:name="HTML Address" />
			<w:lsdException w:name="HTML Cite" />
			<w:lsdException w:name="HTML Code" />
			<w:lsdException w:name="HTML Definition" />
			<w:lsdException w:name="HTML Keyboard" />
			<w:lsdException w:name="HTML Preformatted" />
			<w:lsdException w:name="HTML Sample" />
			<w:lsdException w:name="HTML Typewriter" />
			<w:lsdException w:name="HTML Variable" />
			<w:lsdException w:name="Normal Table" />
			<w:lsdException w:name="annotation subject" />
			<w:lsdException w:name="Table Simple 1" />
			<w:lsdException w:name="Table Simple 2" />
			<w:lsdException w:name="Table Simple 3" />
			<w:lsdException w:name="Table Classic 1" />
			<w:lsdException w:name="Table Classic 2" />
			<w:lsdException w:name="Table Classic 3" />
			<w:lsdException w:name="Table Classic 4" />
			<w:lsdException w:name="Table Colorful 1" />
			<w:lsdException w:name="Table Colorful 2" />
			<w:lsdException w:name="Table Colorful 3" />
			<w:lsdException w:name="Table Columns 1" />
			<w:lsdException w:name="Table Columns 2" />
			<w:lsdException w:name="Table Columns 3" />
			<w:lsdException w:name="Table Columns 4" />
			<w:lsdException w:name="Table Columns 5" />
			<w:lsdException w:name="Table Grid 1" />
			<w:lsdException w:name="Table Grid 2" />
			<w:lsdException w:name="Table Grid 3" />
			<w:lsdException w:name="Table Grid 4" />
			<w:lsdException w:name="Table Grid 5" />
			<w:lsdException w:name="Table Grid 6" />
			<w:lsdException w:name="Table Grid 7" />
			<w:lsdException w:name="Table Grid 8" />
			<w:lsdException w:name="Table List 1" />
			<w:lsdException w:name="Table List 2" />
			<w:lsdException w:name="Table List 3" />
			<w:lsdException w:name="Table List 4" />
			<w:lsdException w:name="Table List 5" />
			<w:lsdException w:name="Table List 6" />
			<w:lsdException w:name="Table List 7" />
			<w:lsdException w:name="Table List 8" />
			<w:lsdException w:name="Table 3D effects 1" />
			<w:lsdException w:name="Table 3D effects 2" />
			<w:lsdException w:name="Table 3D effects 3" />
			<w:lsdException w:name="Table Contemporary" />
			<w:lsdException w:name="Table Elegant" />
			<w:lsdException w:name="Table Professional" />
			<w:lsdException w:name="Table Subtle 1" />
			<w:lsdException w:name="Table Subtle 2" />
			<w:lsdException w:name="Table Web 1" />
			<w:lsdException w:name="Table Web 2" />
			<w:lsdException w:name="Table Web 3" />
			<w:lsdException w:name="Balloon Text" />
			<w:lsdException w:name="Table Grid" />
			<w:lsdException w:name="Table Theme" />
			<w:lsdException w:name="Light Shading" />
			<w:lsdException w:name="Light List" />
			<w:lsdException w:name="Light Grid" />
			<w:lsdException w:name="Medium Shading 1" />
			<w:lsdException w:name="Medium Shading 2" />
			<w:lsdException w:name="Medium List 1" />
			<w:lsdException w:name="Medium List 2" />
			<w:lsdException w:name="Medium Grid 1" />
			<w:lsdException w:name="Medium Grid 2" />
			<w:lsdException w:name="Medium Grid 3" />
			<w:lsdException w:name="Dark List" />
			<w:lsdException w:name="Colorful Shading" />
			<w:lsdException w:name="Colorful List" />
			<w:lsdException w:name="Colorful Grid" />
			<w:lsdException w:name="Light Shading Accent 1" />
			<w:lsdException w:name="Light List Accent 1" />
			<w:lsdException w:name="Light Grid Accent 1" />
			<w:lsdException w:name="Medium Shading 1 Accent 1" />
			<w:lsdException w:name="Medium Shading 2 Accent 1" />
			<w:lsdException w:name="Medium List 1 Accent 1" />
			<w:lsdException w:name="Medium List 2 Accent 1" />
			<w:lsdException w:name="Medium Grid 1 Accent 1" />
			<w:lsdException w:name="Medium Grid 2 Accent 1" />
			<w:lsdException w:name="Medium Grid 3 Accent 1" />
			<w:lsdException w:name="Dark List Accent 1" />
			<w:lsdException w:name="Colorful Shading Accent 1" />
			<w:lsdException w:name="Colorful List Accent 1" />
			<w:lsdException w:name="Colorful Grid Accent 1" />
			<w:lsdException w:name="Light Shading Accent 2" />
			<w:lsdException w:name="Light List Accent 2" />
			<w:lsdException w:name="Light Grid Accent 2" />
			<w:lsdException w:name="Medium Shading 1 Accent 2" />
			<w:lsdException w:name="Medium Shading 2 Accent 2" />
			<w:lsdException w:name="Medium List 1 Accent 2" />
			<w:lsdException w:name="Medium List 2 Accent 2" />
			<w:lsdException w:name="Medium Grid 1 Accent 2" />
			<w:lsdException w:name="Medium Grid 2 Accent 2" />
			<w:lsdException w:name="Medium Grid 3 Accent 2" />
			<w:lsdException w:name="Dark List Accent 2" />
			<w:lsdException w:name="Colorful Shading Accent 2" />
			<w:lsdException w:name="Colorful List Accent 2" />
			<w:lsdException w:name="Colorful Grid Accent 2" />
			<w:lsdException w:name="Light Shading Accent 3" />
			<w:lsdException w:name="Light List Accent 3" />
			<w:lsdException w:name="Light Grid Accent 3" />
			<w:lsdException w:name="Medium Shading 1 Accent 3" />
			<w:lsdException w:name="Medium Shading 2 Accent 3" />
			<w:lsdException w:name="Medium List 1 Accent 3" />
			<w:lsdException w:name="Medium List 2 Accent 3" />
			<w:lsdException w:name="Medium Grid 1 Accent 3" />
			<w:lsdException w:name="Medium Grid 2 Accent 3" />
			<w:lsdException w:name="Medium Grid 3 Accent 3" />
			<w:lsdException w:name="Dark List Accent 3" />
			<w:lsdException w:name="Colorful Shading Accent 3" />
			<w:lsdException w:name="Colorful List Accent 3" />
			<w:lsdException w:name="Colorful Grid Accent 3" />
			<w:lsdException w:name="Light Shading Accent 4" />
			<w:lsdException w:name="Light List Accent 4" />
			<w:lsdException w:name="Light Grid Accent 4" />
			<w:lsdException w:name="Medium Shading 1 Accent 4" />
			<w:lsdException w:name="Medium Shading 2 Accent 4" />
			<w:lsdException w:name="Medium List 1 Accent 4" />
			<w:lsdException w:name="Medium List 2 Accent 4" />
			<w:lsdException w:name="Medium Grid 1 Accent 4" />
			<w:lsdException w:name="Medium Grid 2 Accent 4" />
			<w:lsdException w:name="Medium Grid 3 Accent 4" />
			<w:lsdException w:name="Dark List Accent 4" />
			<w:lsdException w:name="Colorful Shading Accent 4" />
			<w:lsdException w:name="Colorful List Accent 4" />
			<w:lsdException w:name="Colorful Grid Accent 4" />
			<w:lsdException w:name="Light Shading Accent 5" />
			<w:lsdException w:name="Light List Accent 5" />
			<w:lsdException w:name="Light Grid Accent 5" />
			<w:lsdException w:name="Medium Shading 1 Accent 5" />
			<w:lsdException w:name="Medium Shading 2 Accent 5" />
			<w:lsdException w:name="Medium List 1 Accent 5" />
			<w:lsdException w:name="Medium List 2 Accent 5" />
			<w:lsdException w:name="Medium Grid 1 Accent 5" />
			<w:lsdException w:name="Medium Grid 2 Accent 5" />
			<w:lsdException w:name="Medium Grid 3 Accent 5" />
			<w:lsdException w:name="Dark List Accent 5" />
			<w:lsdException w:name="Colorful Shading Accent 5" />
			<w:lsdException w:name="Colorful List Accent 5" />
			<w:lsdException w:name="Colorful Grid Accent 5" />
			<w:lsdException w:name="Light Shading Accent 6" />
			<w:lsdException w:name="Light List Accent 6" />
			<w:lsdException w:name="Light Grid Accent 6" />
			<w:lsdException w:name="Medium Shading 1 Accent 6" />
			<w:lsdException w:name="Medium Shading 2 Accent 6" />
			<w:lsdException w:name="Medium List 1 Accent 6" />
			<w:lsdException w:name="Medium List 2 Accent 6" />
			<w:lsdException w:name="Medium Grid 1 Accent 6" />
			<w:lsdException w:name="Medium Grid 2 Accent 6" />
			<w:lsdException w:name="Medium Grid 3 Accent 6" />
			<w:lsdException w:name="Dark List Accent 6" />
			<w:lsdException w:name="Colorful Shading Accent 6" />
			<w:lsdException w:name="Colorful List Accent 6" />
			<w:lsdException w:name="Colorful Grid Accent 6" />
		</w:latentStyles>
		<w:style w:type="paragraph" w:styleId="a1" w:default="on">
			<w:name w:val="Normal" />
			<w:pPr>
				<w:widowControl w:val="off" />
				<w:jc w:val="both" />
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="Calibri" w:h-ansi="Calibri" w:fareast="宋体"
					w:cs="Times New Roman" w:hint="default" />
				<w:kern w:val="2" />
				<w:sz w:val="21" />
				<w:sz-cs w:val="24" />
				<w:lang w:val="EN-US" w:fareast="ZH-CN" />
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="2">
			<w:name w:val="heading 1" />
			<w:basedOn w:val="a1" />
			<w:next w:val="a1" />
			<w:pPr>
				<w:keepNext />
				<w:keepLines />
				<w:spacing w:before="340" w:before-lines="0"
					w:before-autospacing="off" w:after="330" w:after-autospacing="off"
					w:line="576" w:line-rule="auto" />
				<w:outlineLvl w:val="0" />
			</w:pPr>
			<w:rPr>
				<w:b />
				<w:kern w:val="44" />
				<w:sz w:val="44" />
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="3">
			<w:name w:val="heading 2" />
			<w:basedOn w:val="a1" />
			<w:next w:val="a1" />
			<w:pPr>
				<w:keepNext />
				<w:keepLines />
				<w:spacing w:before="260" w:before-lines="0"
					w:before-autospacing="off" w:after="260" w:after-autospacing="off"
					w:line="413" w:line-rule="auto" />
				<w:outlineLvl w:val="1" />
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:fareast="黑体"
					w:hint="default" />
				<w:b />
				<w:sz w:val="32" />
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="4">
			<w:name w:val="heading 3" />
			<w:basedOn w:val="a1" />
			<w:next w:val="a1" />
			<w:pPr>
				<w:keepNext />
				<w:keepLines />
				<w:spacing w:before="260" w:before-lines="0"
					w:before-autospacing="off" w:after="260" w:after-autospacing="off"
					w:line="413" w:line-rule="auto" />
				<w:outlineLvl w:val="2" />
			</w:pPr>
			<w:rPr>
				<w:b />
				<w:sz w:val="32" />
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="5">
			<w:name w:val="heading 4" />
			<w:basedOn w:val="a1" />
			<w:next w:val="a1" />
			<w:link w:val="a10" />
			<w:pPr>
				<w:keepNext />
				<w:keepLines />
				<w:spacing w:before="280" w:before-lines="0"
					w:before-autospacing="off" w:after="290" w:after-autospacing="off"
					w:line="372" w:line-rule="auto" />
				<w:outlineLvl w:val="3" />
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:fareast="黑体"
					w:hint="default" />
				<w:b />
				<w:sz w:val="28" />
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:styleId="a8" w:default="on">
			<w:name w:val="Default Paragraph Font" />
			<w:semiHidden />
		</w:style>
		<w:style w:type="table" w:styleId="a6" w:default="on">
			<w:name w:val="Normal Table" />
			<w:semiHidden />
			<w:tblPr>
				<w:tblCellMar>
					<w:top w:w="0" w:type="dxa" />
					<w:left w:w="108" w:type="dxa" />
					<w:bottom w:w="0" w:type="dxa" />
					<w:right w:w="108" w:type="dxa" />
				</w:tblCellMar>
			</w:tblPr>
		</w:style>
		<w:style w:type="table" w:styleId="a7">
			<w:name w:val="Table Grid" />
			<w:basedOn w:val="a6" />
			<w:pPr>
				<w:pStyle w:val="a6" />
				<w:widowControl w:val="off" />
				<w:jc w:val="both" />
			</w:pPr>
			<w:tblPr>
				<w:tblBorders>
					<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
						w:color="auto" />
					<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
						w:color="auto" />
					<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
						w:color="auto" />
					<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
						w:color="auto" />
					<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
						w:color="auto" />
					<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
						w:color="auto" />
				</w:tblBorders>
			</w:tblPr>
		</w:style>
		<w:style w:type="character" w:styleId="a9">
			<w:name w:val="Hyperlink" />
			<w:basedOn w:val="a8" />
			<w:rPr>
				<w:color w:val="0000FF" />
				<w:u w:val="single" />
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:styleId="a10">
			<w:name w:val="标题 4 Char" />
			<w:link w:val="5" />
			<w:rPr>
				<w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:fareast="黑体"
					w:hint="default" />
				<w:b />
				<w:sz w:val="28" />
			</w:rPr>
		</w:style>
	</w:styles>
	<w:bgPict>
		<w:background />
		<v:background id="_x0000_s1025">
			<v:fill on="f" focussize="0,0" />
		</v:background>
	</w:bgPict>
	<w:docPr>
		<w:view w:val="print" />
		<w:zoom w:percent="110" />
		<w:characterSpacingControl w:val="CompressPunctuation" />
		<w:documentProtection w:enforcement="off" />
		<w:doNotEmbedSystemFonts />
		<w:defaultTabStop w:val="420" />
		<w:drawingGridVerticalSpacing w:val="156" />
		<w:displayHorizontalDrawingGridEvery w:val="1" />
		<w:displayVerticalDrawingGridEvery w:val="1" />
		<w:compat>
			<w:adjustLineHeightInTable />
			<w:ulTrailSpace />
			<w:doNotExpandShiftReturn />
			<w:balanceSingleByteDoubleByteWidth />
			<w:useFELayout />
			<w:spaceForUL />
			<w:breakWrappedTables />
			<w:dontGrowAutofit />
			<w:useFELayout />
		</w:compat>
	</w:docPr>
	<w:body>
		<wx:sect>
			<w:p>
				<w:pPr>
					<w:pStyle w:val="3" />
					<w:rPr>
						<w:rFonts w:hint="fareast" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
					<w:t>市场报告</w:t>
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:pStyle w:val="3" />
					<w:rPr>
						<w:rFonts w:hint="fareast" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
					<w:t>城市供求情况——办公</w:t>
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
					<w:pict>
						<w:binData w:name="wordml://1.png">${report1.base64?if_exists}</w:binData>
						<v:shape id="图片 12" o:spid="_x0000_s1026" o:spt="75" alt="1"
							type="#_x0000_t75" style="height:300pt;width:465pt;" filled="f"
							o:preferrelative="t" stroked="f" coordsize="21600,21600">
							<v:path />
							<v:fill on="f" focussize="0,0" />
							<v:stroke on="f" />
							<v:imagedata src="wordml://1.png" o:title="1" />
							<o:lock v:ext="edit" aspectratio="t" />
							<w10:wrap type="none" />
							<w10:anchorlock />
						</v:shape>
					</w:pict>
				</w:r>
			</w:p>
			<#if report1.table??>
			<w:tbl>
				<w:tblPr>
					<w:tblStyle w:val="a7" />
					<w:tblW w:w="10682" w:type="dxa" />
					<w:jc w:val="center" />
					<w:tblInd w:w="0" w:type="dxa" />
					<w:tblBorders>
						<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
					</w:tblBorders>
					<w:tblLayout w:type="Fixed" />
					<w:tblCellMar>
						<w:left w:w="108" w:type="dxa" />
						<w:right w:w="108" w:type="dxa" />
					</w:tblCellMar>
				</w:tblPr>
				<w:tblGrid>
					<w:gridCol w:w="2670" />
					<w:gridCol w:w="2670" />
					<w:gridCol w:w="2671" />
					<w:gridCol w:w="2671" />
				</w:tblGrid>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:left w:w="108" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="10682" w:type="dxa" />
							<w:gridSpan w:val="4" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${report1.title?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:left w:w="108" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>年份</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>供应面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交价格（元/㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<#list report1.table as table>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:left w:w="108" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.time?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gongying?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.chengjiao?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.price?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				</#list>
			</w:tbl>
			</#if>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
					<w:br w:type="page" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:pStyle w:val="5" />
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:fldChar w:fldCharType="begin" />
				</w:r>
				<w:r>
					<w:instrText> HYPERLINK &quot;http://localhost:9999/jrzh/pages/reptile/newHTML/pages/marketReport/marketReport.html&quot; \l &quot;chengshigongqiu_zhuzhai&quot; </w:instrText>
				</w:r>
				<w:r>
					<w:fldChar w:fldCharType="separate" />
				</w:r>
				<w:r>
					<w:rPr>
						<w:rStyle w:val="a9" />
						<w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑"
							w:cs="微软雅黑" w:hint="fareast" />
						<w:i w:val="off" />
						<w:caps w:val="off" />
						<w:color w:val="000000" />
						<w:spacing w:val="0" />
						<w:sz-cs w:val="18" />
						<w:u w:val="none" />
						<w:shd w:val="clear" w:color="auto" w:fill="FFFFFF" />
					</w:rPr>
					<w:t>城市供求情况——住宅</w:t>
				</w:r>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:fldChar w:fldCharType="end" />
				</w:r>
			</w:p>
			<w:p>
				<w:r>
					<w:pict>
						<w:binData w:name="wordml://2.png">${report2.base64?if_exists}</w:binData>
						<v:shape id="_x0000_s1027" o:spt="75" alt="2" type="#_x0000_t75"
							style="height:300pt;width:465pt;" filled="f" o:preferrelative="t"
							stroked="f" coordsize="21600,21600">
							<v:path />
							<v:fill on="f" focussize="0,0" />
							<v:stroke on="f" />
							<v:imagedata src="wordml://2.png" o:title="2" />
							<o:lock v:ext="edit" aspectratio="t" />
							<w10:wrap type="none" />
							<w10:anchorlock />
						</v:shape>
					</w:pict>
				</w:r>
			</w:p>
			<#if report2.table??>
			<w:tbl>
				<w:tblPr>
					<w:tblStyle w:val="a7" />
					<w:tblW w:w="10682" w:type="dxa" />
					<w:jc w:val="center" />
					<w:tblInd w:w="0" w:type="dxa" />
					<w:tblBorders>
						<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
					</w:tblBorders>
					<w:tblLayout w:type="Fixed" />
					<w:tblCellMar>
						<w:top w:w="0" w:type="dxa" />
						<w:left w:w="108" w:type="dxa" />
						<w:bottom w:w="0" w:type="dxa" />
						<w:right w:w="108" w:type="dxa" />
					</w:tblCellMar>
				</w:tblPr>
				<w:tblGrid>
					<w:gridCol w:w="2670" />
					<w:gridCol w:w="2670" />
					<w:gridCol w:w="2671" />
					<w:gridCol w:w="2671" />
				</w:tblGrid>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="10682" w:type="dxa" />
							<w:gridSpan w:val="4" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${report2.title?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>年份</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>供应面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交价格（元/㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<#list report2.table as table> 
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.time?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gongying?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.chengjiao?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.price?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				</#list>
			</w:tbl>
			</#if>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:br w:type="page" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:pStyle w:val="5" />
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:fldChar w:fldCharType="begin" />
				</w:r>
				<w:r>
					<w:instrText> HYPERLINK &quot;http://localhost:9999/jrzh/pages/reptile/newHTML/pages/marketReport/marketReport.html&quot; \l &quot;quyugongqiu_bangong&quot; </w:instrText>
				</w:r>
				<w:r>
					<w:fldChar w:fldCharType="separate" />
				</w:r>
				<w:r>
					<w:rPr>
						<w:rStyle w:val="a9" />
						<w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑"
							w:cs="微软雅黑" w:hint="fareast" />
						<w:i w:val="off" />
						<w:caps w:val="off" />
						<w:color w:val="000000" />
						<w:spacing w:val="0" />
						<w:sz-cs w:val="18" />
						<w:u w:val="none" />
						<w:shd w:val="clear" w:color="auto" w:fill="FFFFFF" />
					</w:rPr>
					<w:t>区域供求情况——办公</w:t>
				</w:r>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:fldChar w:fldCharType="end" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:pict>
						<w:binData w:name="wordml://3.png">${report3.base64?if_exists}</w:binData>
						<v:shape id="_x0000_s1028" o:spt="75" alt="3" type="#_x0000_t75"
							style="height:300pt;width:465pt;" filled="f" o:preferrelative="t"
							stroked="f" coordsize="21600,21600">
							<v:path />
							<v:fill on="f" focussize="0,0" />
							<v:stroke on="f" />
							<v:imagedata src="wordml://3.png" o:title="3" />
							<o:lock v:ext="edit" aspectratio="t" />
							<w10:wrap type="none" />
							<w10:anchorlock />
						</v:shape>
					</w:pict>
				</w:r>
			</w:p>
			<#if report3.table??>
			<w:tbl>
				<w:tblPr>
					<w:tblStyle w:val="a7" />
					<w:tblW w:w="10682" w:type="dxa" />
					<w:jc w:val="center" />
					<w:tblInd w:w="0" w:type="dxa" />
					<w:tblBorders>
						<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
					</w:tblBorders>
					<w:tblLayout w:type="Fixed" />
					<w:tblCellMar>
						<w:top w:w="0" w:type="dxa" />
						<w:left w:w="108" w:type="dxa" />
						<w:bottom w:w="0" w:type="dxa" />
						<w:right w:w="108" w:type="dxa" />
					</w:tblCellMar>
				</w:tblPr>
				<w:tblGrid>
					<w:gridCol w:w="2670" />
					<w:gridCol w:w="2670" />
					<w:gridCol w:w="2671" />
					<w:gridCol w:w="2671" />
				</w:tblGrid>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="10682" w:type="dxa" />
							<w:gridSpan w:val="4" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${report3.title?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>年份</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>供应面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交价格（元/㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<#list report3.table as table>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.time?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gongying?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.chengjiao?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.price?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				</#list>
			</w:tbl>
			</#if>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:br w:type="page" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:pStyle w:val="5" />
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:fldChar w:fldCharType="begin" />
				</w:r>
				<w:r>
					<w:instrText> HYPERLINK &quot;http://localhost:9999/jrzh/pages/reptile/newHTML/pages/marketReport/marketReport.html&quot; \l &quot;quyugongqiu_zhuzhai&quot; </w:instrText>
				</w:r>
				<w:r>
					<w:fldChar w:fldCharType="separate" />
				</w:r>
				<w:r>
					<w:rPr>
						<w:rStyle w:val="a9" />
						<w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑"
							w:cs="微软雅黑" w:hint="fareast" />
						<w:i w:val="off" />
						<w:caps w:val="off" />
						<w:color w:val="000000" />
						<w:spacing w:val="0" />
						<w:sz-cs w:val="18" />
						<w:u w:val="none" />
						<w:shd w:val="clear" w:color="auto" w:fill="FFFFFF" />
					</w:rPr>
					<w:t>区域供求情况——住宅</w:t>
				</w:r>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:fldChar w:fldCharType="end" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:pict>
						<w:binData w:name="wordml://4.png">${report4.base64?if_exists}</w:binData>
						<v:shape id="_x0000_s1029" o:spt="75" alt="4" type="#_x0000_t75"
							style="height:300pt;width:465pt;" filled="f" o:preferrelative="t"
							stroked="f" coordsize="21600,21600">
							<v:path />
							<v:fill on="f" focussize="0,0" />
							<v:stroke on="f" />
							<v:imagedata src="wordml://4.png" o:title="4" />
							<o:lock v:ext="edit" aspectratio="t" />
							<w10:wrap type="none" />
							<w10:anchorlock />
						</v:shape>
					</w:pict>
				</w:r>
			</w:p>
			<#if report4.table??>
			<w:tbl>
				<w:tblPr>
					<w:tblStyle w:val="a7" />
					<w:tblW w:w="10682" w:type="dxa" />
					<w:jc w:val="center" />
					<w:tblInd w:w="0" w:type="dxa" />
					<w:tblBorders>
						<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
					</w:tblBorders>
					<w:tblLayout w:type="Fixed" />
					<w:tblCellMar>
						<w:top w:w="0" w:type="dxa" />
						<w:left w:w="108" w:type="dxa" />
						<w:bottom w:w="0" w:type="dxa" />
						<w:right w:w="108" w:type="dxa" />
					</w:tblCellMar>
				</w:tblPr>
				<w:tblGrid>
					<w:gridCol w:w="2670" />
					<w:gridCol w:w="2670" />
					<w:gridCol w:w="2671" />
					<w:gridCol w:w="2671" />
				</w:tblGrid>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="10682" w:type="dxa" />
							<w:gridSpan w:val="4" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${report4.title?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>年份</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>供应面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交价格（元/㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<#list report4.table as table>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.time?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gongying?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.chengjiao?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.price?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				</#list>
			</w:tbl>
			</#if>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:br w:type="page" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:pStyle w:val="5" />
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:fldChar w:fldCharType="begin" />
				</w:r>
				<w:r>
					<w:instrText> HYPERLINK &quot;http://localhost:9999/jrzh/pages/reptile/newHTML/pages/marketReport/marketReport.html&quot; \l &quot;shangpinfanggongxiaojiaqingkuang&quot; </w:instrText>
				</w:r>
				<w:r>
					<w:fldChar w:fldCharType="separate" />
				</w:r>
				<w:r>
					<w:rPr>
						<w:rStyle w:val="a9" />
						<w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑"
							w:cs="微软雅黑" w:hint="fareast" />
						<w:i w:val="off" />
						<w:caps w:val="off" />
						<w:color w:val="000000" />
						<w:spacing w:val="0" />
						<w:sz-cs w:val="18" />
						<w:u w:val="none" />
						<w:shd w:val="clear" w:color="auto" w:fill="FFFFFF" />
					</w:rPr>
					<w:t>商品房供销价情况</w:t>
				</w:r>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:fldChar w:fldCharType="end" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:pict>
						<w:binData w:name="wordml://5.png">${report5.base64?if_exists}</w:binData>
						<v:shape id="_x0000_s1030" o:spt="75" alt="5" type="#_x0000_t75"
							style="height:300pt;width:465pt;" filled="f" o:preferrelative="t"
							stroked="f" coordsize="21600,21600">
							<v:path />
							<v:fill on="f" focussize="0,0" />
							<v:stroke on="f" />
							<v:imagedata src="wordml://5.png" o:title="5" />
							<o:lock v:ext="edit" aspectratio="t" />
							<w10:wrap type="none" />
							<w10:anchorlock />
						</v:shape>
					</w:pict>
				</w:r>
			</w:p>
			<#if report5.table??>
			<w:tbl>
				<w:tblPr>
					<w:tblStyle w:val="a7" />
					<w:tblW w:w="10682" w:type="dxa" />
					<w:jc w:val="center" />
					<w:tblInd w:w="0" w:type="dxa" />
					<w:tblBorders>
						<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
					</w:tblBorders>
					<w:tblLayout w:type="Fixed" />
					<w:tblCellMar>
						<w:top w:w="0" w:type="dxa" />
						<w:left w:w="108" w:type="dxa" />
						<w:bottom w:w="0" w:type="dxa" />
						<w:right w:w="108" w:type="dxa" />
					</w:tblCellMar>
				</w:tblPr>
				<w:tblGrid>
					<w:gridCol w:w="2670" />
					<w:gridCol w:w="2670" />
					<w:gridCol w:w="2671" />
					<w:gridCol w:w="2671" />
				</w:tblGrid>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="10682" w:type="dxa" />
							<w:gridSpan w:val="4" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${report5.title?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>年份</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>供应面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交价格（元/㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<#list report5.table as table>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.time?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gongying?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.chengjiao?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.price?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				</#list>
			</w:tbl>
			</#if>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:br w:type="page" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:pStyle w:val="5" />
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:fldChar w:fldCharType="begin" />
				</w:r>
				<w:r>
					<w:instrText> HYPERLINK &quot;http://localhost:9999/jrzh/pages/reptile/newHTML/pages/marketReport/marketReport.html&quot; \l &quot;shangpinfangzhuzhaigongxiaojiaqingkuang&quot; </w:instrText>
				</w:r>
				<w:r>
					<w:fldChar w:fldCharType="separate" />
				</w:r>
				<w:r>
					<w:rPr>
						<w:rStyle w:val="a9" />
						<w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑"
							w:cs="微软雅黑" w:hint="fareast" />
						<w:i w:val="off" />
						<w:caps w:val="off" />
						<w:color w:val="000000" />
						<w:spacing w:val="0" />
						<w:sz-cs w:val="18" />
						<w:u w:val="none" />
						<w:shd w:val="clear" w:color="auto" w:fill="FFFFFF" />
					</w:rPr>
					<w:t>商品房住宅供销价情况</w:t>
				</w:r>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:fldChar w:fldCharType="end" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:pict>
						<w:binData w:name="wordml://6.png">${report10.base64?if_exists}</w:binData>
						<v:shape id="_x0000_s1031" o:spt="75" alt="6" type="#_x0000_t75"
							style="height:300pt;width:465pt;" filled="f" o:preferrelative="t"
							stroked="f" coordsize="21600,21600">
							<v:path />
							<v:fill on="f" focussize="0,0" />
							<v:stroke on="f" />
							<v:imagedata src="wordml://6.png" o:title="6" />
							<o:lock v:ext="edit" aspectratio="t" />
							<w10:wrap type="none" />
							<w10:anchorlock />
						</v:shape>
					</w:pict>
				</w:r>
			</w:p>
			<#if report10.table??>
			<w:tbl>
				<w:tblPr>
					<w:tblStyle w:val="a7" />
					<w:tblW w:w="10682" w:type="dxa" />
					<w:jc w:val="center" />
					<w:tblInd w:w="0" w:type="dxa" />
					<w:tblBorders>
						<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
					</w:tblBorders>
					<w:tblLayout w:type="Fixed" />
					<w:tblCellMar>
						<w:top w:w="0" w:type="dxa" />
						<w:left w:w="108" w:type="dxa" />
						<w:bottom w:w="0" w:type="dxa" />
						<w:right w:w="108" w:type="dxa" />
					</w:tblCellMar>
				</w:tblPr>
				<w:tblGrid>
					<w:gridCol w:w="2670" />
					<w:gridCol w:w="2670" />
					<w:gridCol w:w="2671" />
					<w:gridCol w:w="2671" />
				</w:tblGrid>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="10682" w:type="dxa" />
							<w:gridSpan w:val="4" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${report10.title?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>年份</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>供应面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交价格（元/㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<#list report10.table as table>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="108" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:jc w:val="center" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.time?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2670" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gongying?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.chengjiao?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2671" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:vAlign w:val="top" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.price?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				</#list>
			</w:tbl>
			</#if>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:br w:type="page" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:pStyle w:val="5" />
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:fldChar w:fldCharType="begin" />
				</w:r>
				<w:r>
					<w:instrText> HYPERLINK &quot;http://localhost:9999/jrzh/pages/reptile/newHTML/pages/marketReport/marketReport.html&quot; \l &quot;fenyetaixiaoshouliangjia&quot; </w:instrText>
				</w:r>
				<w:r>
					<w:fldChar w:fldCharType="separate" />
				</w:r>
				<w:r>
					<w:rPr>
						<w:rStyle w:val="a9" />
						<w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑"
							w:cs="微软雅黑" w:hint="fareast" />
						<w:i w:val="off" />
						<w:caps w:val="off" />
						<w:color w:val="000000" />
						<w:spacing w:val="0" />
						<w:sz-cs w:val="18" />
						<w:u w:val="none" />
						<w:shd w:val="clear" w:color="auto" w:fill="FFFFFF" />
					</w:rPr>
					<w:t>分业态销售量价</w:t>
				</w:r>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:fldChar w:fldCharType="end" />
				</w:r>
			</w:p>
			<w:tbl>
				<w:tblPr>
					<w:tblW w:w="10496" w:type="dxa" />
					<w:tblInd w:w="0" w:type="dxa" />
					<w:tblLayout w:type="Fixed" />
					<w:tblCellMar>
						<w:top w:w="0" w:type="dxa" />
						<w:left w:w="0" w:type="dxa" />
						<w:bottom w:w="0" w:type="dxa" />
						<w:right w:w="0" w:type="dxa" />
					</w:tblCellMar>
				</w:tblPr>
				<w:tblGrid>
					<w:gridCol w:w="1291" />
					<w:gridCol w:w="2510" />
					<w:gridCol w:w="2569" />
					<w:gridCol w:w="2256" />
					<w:gridCol w:w="1870" />
				</w:tblGrid>
				<w:tr>
					<w:tblPrEx>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="10496" w:type="dxa" />
							<w:gridSpan w:val="5" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:b />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${report6.title?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1291" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>物业类型</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2510" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>供应量（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2569" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>成交量（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2256" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>成交价格（元/㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1870" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>供求比</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<#list report6.table as table>
				<w:tr>
					<w:tblPrEx>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1291" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>年份</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="9205" w:type="dxa" />
							<w:gridSpan w:val="4" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>${table.time?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1291" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.type_house?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2510" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gongying_house?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2569" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.chengjiao_house?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2256" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.price_house?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1870" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gqb_house?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1291" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.type_villa?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2510" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gongying_villa?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2569" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.chengjiao_villa?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2256" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.price_villa?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1870" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gqb_villa?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1291" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.type_business?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2510" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gongying_business?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2569" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:tabs>
									<w:tab w:val="left" w:pos="564" />
									<w:tab w:val="center" w:pos="1162" />
								</w:tabs>
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.chengjiao_business?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2256" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.price_business?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1870" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gqb_business?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1291" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.type_office?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2510" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gongying_office?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2569" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.chengjiao_office?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2256" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.price_office?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1870" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.gqb_office?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				</#list>
			</w:tbl>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:br w:type="page" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:pStyle w:val="5" />
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:fldChar w:fldCharType="begin" />
				</w:r>
				<w:r>
					<w:instrText> HYPERLINK &quot;http://localhost:9999/jrzh/pages/reptile/newHTML/pages/marketReport/marketReport.html&quot; \l &quot;gongkaizhaopaiguashichangdianxingdikuailiebiao&quot; </w:instrText>
				</w:r>
				<w:r>
					<w:fldChar w:fldCharType="separate" />
				</w:r>
				<w:r>
					<w:rPr>
						<w:rStyle w:val="a9" />
						<w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑"
							w:cs="微软雅黑" w:hint="fareast" />
						<w:i w:val="off" />
						<w:caps w:val="off" />
						<w:color w:val="000000" />
						<w:spacing w:val="0" />
						<w:sz-cs w:val="18" />
						<w:u w:val="none" />
						<w:shd w:val="clear" w:color="auto" w:fill="FFFFFF" />
					</w:rPr>
					<w:t>公开招拍挂市场典型地块列表</w:t>
				</w:r>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:fldChar w:fldCharType="end" />
				</w:r>
			</w:p>
			<w:tbl>
				<w:tblPr>
					<w:tblW w:w="10496" w:type="dxa" />
					<w:tblInd w:w="0" w:type="dxa" />
					<w:tblLayout w:type="Fixed" />
					<w:tblCellMar>
						<w:top w:w="0" w:type="dxa" />
						<w:left w:w="0" w:type="dxa" />
						<w:bottom w:w="0" w:type="dxa" />
						<w:right w:w="0" w:type="dxa" />
					</w:tblCellMar>
				</w:tblPr>
				<w:tblGrid>
					<w:gridCol w:w="2630" />
					<w:gridCol w:w="1582" />
					<w:gridCol w:w="1500" />
					<w:gridCol w:w="1597" />
					<w:gridCol w:w="3187" />
				</w:tblGrid>
				<w:tr>
					<w:tblPrEx>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="10496" w:type="dxa" />
							<w:gridSpan w:val="5" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:b />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${report7.title?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2630" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>地块名称</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1582" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>规划建面（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1500" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>成交价（万元）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1597" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>楼面价（元/㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="3187" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>拿地企业</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<#if report7.table??>
				<#list report7.table as table>
				<w:tr>
					<w:tblPrEx>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2630" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>${table.address?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1582" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>${table.acreage?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1500" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>${table.tudi_price?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1597" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.price?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="3187" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap w:val="0" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>${table.buyer?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				</#list>
				<#else>
				<w:tr>
					<w:tblPrEx>
						<w:shd w:val="clear" w:color="auto" w:fill="auto" />
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="624" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="10496" w:type="dxa" />
							<w:gridSpan w:val="5" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:noWrap />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:cs="宋体"
										w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="22" />
									<w:sz-cs w:val="22" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:cs="宋体"
										w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="22" />
									<w:sz-cs w:val="22" />
									<w:u w:val="none" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>暂无数据</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				</#if>
			</w:tbl>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:br w:type="page" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:pStyle w:val="5" />
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:fldChar w:fldCharType="begin" />
				</w:r>
				<w:r>
					<w:instrText> HYPERLINK &quot;http://localhost:9999/jrzh/pages/reptile/newHTML/pages/marketReport/marketReport.html&quot; \l &quot;quyutudichengjiaoqingkuang&quot; </w:instrText>
				</w:r>
				<w:r>
					<w:fldChar w:fldCharType="separate" />
				</w:r>
				<w:r>
					<w:rPr>
						<w:rStyle w:val="a9" />
						<w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑"
							w:cs="微软雅黑" w:hint="fareast" />
						<w:i w:val="off" />
						<w:caps w:val="off" />
						<w:color w:val="000000" />
						<w:spacing w:val="0" />
						<w:sz-cs w:val="18" />
						<w:u w:val="none" />
						<w:shd w:val="clear" w:color="auto" w:fill="FFFFFF" />
					</w:rPr>
					<w:t>区域土地成交情况</w:t>
				</w:r>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:fldChar w:fldCharType="end" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:pict>
						<w:binData w:name="wordml://7.png">${report8.base64?if_exists}</w:binData>
						<v:shape id="_x0000_s1032" o:spt="75" alt="7" type="#_x0000_t75"
							style="height:300pt;width:465pt;" filled="f" o:preferrelative="t"
							stroked="f" coordsize="21600,21600">
							<v:path />
							<v:fill on="f" focussize="0,0" />
							<v:stroke on="f" />
							<v:imagedata src="wordml://7.png" o:title="7" />
							<o:lock v:ext="edit" aspectratio="t" />
							<w10:wrap type="none" />
							<w10:anchorlock />
						</v:shape>
					</w:pict>
				</w:r>
			</w:p>
			<#if report8.table??>
			<w:tbl>
				<w:tblPr>
					<w:tblW w:w="10496" w:type="dxa" />
					<w:tblInd w:w="0" w:type="dxa" />
					<w:shd w:val="clear" w:color="auto" w:fill="auto" />
					<w:tblCellMar>
						<w:top w:w="0" w:type="dxa" />
						<w:left w:w="0" w:type="dxa" />
						<w:bottom w:w="0" w:type="dxa" />
						<w:right w:w="0" w:type="dxa" />
					</w:tblCellMar>
				</w:tblPr>
				<w:tblGrid>
					<w:gridCol w:w="890" />
					<w:gridCol w:w="2401" />
					<w:gridCol w:w="2402" />
					<w:gridCol w:w="2401" />
					<w:gridCol w:w="2402" />
				</w:tblGrid>
				<w:tr>
					<w:tblPrEx>
						<w:shd w:val="clear" w:color="auto" w:fill="auto" />
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="10496" w:type="dxa" />
							<w:gridSpan w:val="5" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="default" />
									<w:b />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:b />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:bdr w:val="nil" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>${report8.title?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:shd w:val="clear" w:color="auto" w:fill="auto" />
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="890" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:bdr w:val="nil" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>年份</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2401" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:bdr w:val="nil" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>住宅面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2402" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:bdr w:val="nil" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>商业面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2401" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:bdr w:val="nil" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>办公面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2402" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:bdr w:val="nil" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>车位面积（㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<#list report8.table as table>
				<w:tr>
					<w:tblPrEx>
						<w:shd w:val="clear" w:color="auto" w:fill="auto" />
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa" />
							<w:left w:w="0" w:type="dxa" />
							<w:bottom w:w="0" w:type="dxa" />
							<w:right w:w="0" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr>
						<w:trHeight w:val="300" w:h-rule="atLeast" />
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="890" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:bdr w:val="nil" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>${table.time?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2401" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:bdr w:val="nil" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>${table.houseArea?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2402" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:bdr w:val="nil" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>${table.businessArea?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2401" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:bdr w:val="nil" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>${table.officeArea?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="2402" w:type="dxa" />
							<w:tcBorders>
								<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
								<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
									w:color="000000" />
							</w:tcBorders>
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:tcMar>
								<w:top w:w="15" w:type="dxa" />
								<w:left w:w="15" w:type="dxa" />
								<w:right w:w="15" w:type="dxa" />
							</w:tcMar>
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:keepNext w:val="off" />
								<w:keepLines w:val="off" />
								<w:widowControl />
								<w:supressLineNumbers w:val="off" />
								<w:jc w:val="center" />
								<w:textAlignment w:val="center" />
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:ascii="Arial Unicode MS"
										w:h-ansi="Arial Unicode MS" w:fareast="Arial Unicode MS"
										w:cs="Arial Unicode MS" w:hint="fareast" />
									<w:i w:val="off" />
									<w:color w:val="000000" />
									<w:kern w:val="0" />
									<w:sz w:val="20" />
									<w:sz-cs w:val="20" />
									<w:u w:val="none" />
									<w:bdr w:val="nil" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA" />
								</w:rPr>
								<w:t>${table.carportArea?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				</#list>
			</w:tbl>
			</#if>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:br w:type="page" />
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:pStyle w:val="5" />
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:fldChar w:fldCharType="begin" />
				</w:r>
				<w:r>
					<w:instrText> HYPERLINK &quot;http://localhost:9999/jrzh/pages/reptile/newHTML/pages/marketReport/marketReport.html?repotrId=297e26906ad52983016ad53872cc0000&quot; \l &quot;jingpinqingkuangbiao&quot; </w:instrText>
				</w:r>
				<w:r>
					<w:fldChar w:fldCharType="separate" />
				</w:r>
				<w:r>
					<w:rPr>
						<w:rStyle w:val="a9" />
						<w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑"
							w:cs="微软雅黑" w:hint="fareast" />
						<w:i w:val="off" />
						<w:caps w:val="off" />
						<w:color w:val="000000" />
						<w:spacing w:val="0" />
						<w:sz-cs w:val="18" />
						<w:u w:val="none" />
						<w:shd w:val="clear" w:color="auto" w:fill="FFFFFF" />
					</w:rPr>
					<w:t>竞品情况表</w:t>
				</w:r>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:fldChar w:fldCharType="end" />
				</w:r>
			</w:p>
			<w:tbl>
				<w:tblPr>
					<w:tblStyle w:val="a7" />
					<w:tblW w:w="10682" w:type="dxa" />
					<w:tblInd w:w="0" w:type="dxa" />
					<w:tblBorders>
						<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
						<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
							w:color="auto" />
					</w:tblBorders>
					<w:tblLayout w:type="Fixed" />
					<w:tblCellMar>
						<w:left w:w="108" w:type="dxa" />
						<w:right w:w="108" w:type="dxa" />
					</w:tblCellMar>
				</w:tblPr>
				<w:tblGrid>
					<w:gridCol w:w="1780" />
					<w:gridCol w:w="1780" />
					<w:gridCol w:w="1780" />
					<w:gridCol w:w="1780" />
					<w:gridCol w:w="1781" />
					<w:gridCol w:w="1781" />
				</w:tblGrid>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:left w:w="108" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr />
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="10682" w:type="dxa" />
							<w:gridSpan w:val="6" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${report11.title?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:left w:w="108" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr />
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1780" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>项目</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1780" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>占地面积（万㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1780" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>容积率</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1780" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>主打户型</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1781" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>成交均价（元/㎡）</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1781" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="fareast" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>备注</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				<#list report11.table as table>
				<w:tr>
					<w:tblPrEx>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0"
								w:color="auto" />
						</w:tblBorders>
						<w:tblCellMar>
							<w:left w:w="108" w:type="dxa" />
							<w:right w:w="108" w:type="dxa" />
						</w:tblCellMar>
					</w:tblPrEx>
					<w:trPr />
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1780" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.houseName?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1780" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.areaCovered?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1780" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.plotRatio?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1780" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.mainUnit?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1781" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.housePrice?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="1781" w:type="dxa" />
							<w:shd w:val="clear" w:color="auto" w:fill="auto" />
							<w:vAlign w:val="center" />
						</w:tcPr>
						<w:p>
							<w:pPr>
								<w:jc w:val="center" />
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:rFonts w:hint="default" />
									<w:vertAlign w:val="baseline" />
									<w:lang w:val="EN-US" w:fareast="ZH-CN" />
								</w:rPr>
								<w:t>${table.comment?if_exists}</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
				</#list>
			</w:tbl>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="default" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
					<w:br w:type="page" />
				</w:r>
				<w:r>
					<w:rPr>
						<w:rStyle w:val="a10" />
						<w:rFonts w:hint="fareast" />
						<w:lang w:val="EN-US" w:fareast="ZH-CN" />
					</w:rPr>
					<w:t>竞品地图</w:t>
				</w:r>
			</w:p>
			<w:p>
				<w:pPr>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:hint="fareast" />
					</w:rPr>
					<w:pict>
						<w:binData w:name="wordml://12.png">${(report12.base64)!}</w:binData>
						<v:shape id="_x0000_s1032" o:spt="75" alt="7" type="#_x0000_t75"
							style="height:300pt;width:465pt;" filled="f" o:preferrelative="t"
							stroked="f" coordsize="21600,21600">
							<v:path />
							<v:fill on="f" focussize="0,0" />
							<v:stroke on="f" />
							<v:imagedata src="wordml://12.png" o:title="7" />
							<o:lock v:ext="edit" aspectratio="t" />
							<w10:wrap type="none" />
							<w10:anchorlock />
						</v:shape>
					</w:pict>
				</w:r>
			</w:p>
			<w:sectPr>
				<w:pgSz w:w="11906" w:h="16838" />
				<w:pgMar w:top="720" w:right="720" w:bottom="720" w:left="720"
					w:header="851" w:footer="992" w:gutter="0" />
				<w:cols w:space="720" />
				<w:docGrid w:type="lines" w:line-pitch="312" />
			</w:sectPr>
		</wx:sect>
	</w:body>
</w:wordDocument>