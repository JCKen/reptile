package com.jrzh.contants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * 城市区域常量类
 * @author xsc
 *
 */
public interface CityAreaConstants {
	
	public static final String GZ = "gz";
	public static final String SZ = "sz";
	public static final String BJ = "bj";
	public static final String SH = "sh";
	public static final String CD = "cd";
	public static final String TJ = "tj";
	public static final String FS = "fs";
	public static final String ZH = "zh";
	public static final String KS = "ks";
	public static final String NB = "nb";
	public static final String XA = "xa";
	public static final String JM = "jm";
	public static final String HZ = "hz";
	
	
	//链家
	final static ArrayList<String> GZPartString = new  ArrayList<String>(Arrays.asList("nansha","liwan","yuexiu","haizhu","tianhe","baiyun","huangpugz","panyu","huadou","zengcheng","conghua"));
	final static ArrayList<String> SZPartString = new  ArrayList<String>(Arrays.asList("futianqu","nanshanqu","longgangqu","baoanqu","luohuqu","longhuaqu","yantianqu","guangmingxinqu","pingshanqu","dapengxinqu"));
	final static ArrayList<String> BJPartString = new  ArrayList<String>(Arrays.asList("dongcheng","xicheng","chaoyang","haidian","fengtai","shijingshan","tongzhou","changping","daxing","yizhuangkaifaqu","shunyi","fangshan","mentougou","pinggu","huairou","miyun","yanqing"));
	final static ArrayList<String> SHPartString = new  ArrayList<String>(Arrays.asList("jingan","xuhui","huangpu","changning","putuo","pudong","baoshan","zhabei","hongkou","yangpu","minhang","jiading","songjiang","qingpu","fengxian","jinshan","chongming"));
	final static ArrayList<String> CDPartString = new  ArrayList<String>(Arrays.asList("jinjiang","qingyang","wuhou","gaoxin","chenghua","jinniu","tianfuxinqu","gaoxinxi","shaungliu","wenjiang","pidou","longquanyi","xindu","qingbaijiang","jintang","dayi","pujiang","xinjin","pengzhou","qionglai","chongzhou","dujiangyan","jianyang","tianfuxinqunanqu"));
	final static ArrayList<String> TJPartString = new  ArrayList<String>(Arrays.asList("baodi","binhaixinqu","jinghai","jizhou","ninghe","wuqing","heping","nankai","hexi","hebei","hedong","hongqiao","xiqing","beichen","dongli","jinnan","tanggu","kaifaqu","haihejiaoyuyuanqu"));
	final static ArrayList<String> FSPartString = new  ArrayList<String>(Arrays.asList("shunde","nanhai","chancheng","sanshui","gaoming"));
	final static ArrayList<String> ZHPartString = new  ArrayList<String>(Arrays.asList("xiangzhouqu","jinwanqu","doumenqu"));
	final static ArrayList<String> XAPartString = new  ArrayList<String>(Arrays.asList("yanta","changan4","baqiao","weiyang","lianhu","beilin","xinchengqu","xixianxinquxian","gaoling1","lintong","yanliang","lantian","huyiqu","zhouzhi"));
	final static ArrayList<String> HZPartString = new  ArrayList<String>(Arrays.asList("huicheng","huiyang","dayawan","huidong","boluo","longmen"));
	//链家没有昆山新房数据
	/*final static ArrayList<String> KSPartString = new  ArrayList<String>(Arrays.asList(""));*/
	//链家没有宁波新房数据
	/*final static ArrayList<String> NBPartString = new  ArrayList<String>(Arrays.asList(""));*/
	//链家没有江门新房数据
	/*final static ArrayList<String> JMPartString = new  ArrayList<String>(Arrays.asList(""));*/

	//链家
	public abstract class NEW_HOUSE_CITY{
		public static HashMap<String, String> keyMap = new HashMap<String, String>();
		static {
			keyMap.put(GZPartString.get(0), "南沙");
			keyMap.put(GZPartString.get(1), "荔湾");
			keyMap.put(GZPartString.get(2), "越秀");
			keyMap.put(GZPartString.get(3), "海珠");
			keyMap.put(GZPartString.get(4), "天河");
			keyMap.put(GZPartString.get(5), "白云");
			keyMap.put(GZPartString.get(6), "黄埔");
			keyMap.put(GZPartString.get(7), "番禺");
			keyMap.put(GZPartString.get(8), "花都");
			keyMap.put(GZPartString.get(9), "增城");
			keyMap.put(GZPartString.get(10), "从化");
			
			
			keyMap.put(SZPartString.get(0), "福田区");
			keyMap.put(SZPartString.get(1), "南山区");
			keyMap.put(SZPartString.get(2), "龙岗区");
			keyMap.put(SZPartString.get(3), "宝安区");
			keyMap.put(SZPartString.get(4), "罗湖区");
			keyMap.put(SZPartString.get(5), "龙华区");
			keyMap.put(SZPartString.get(6), "盐田区");
			keyMap.put(SZPartString.get(7), "光明新区");
			keyMap.put(SZPartString.get(8), "坪山区");
			keyMap.put(SZPartString.get(9), "大鹏新区");
			
			
			keyMap.put(BJPartString.get(0), "东城");
			keyMap.put(BJPartString.get(1), "西城");
			keyMap.put(BJPartString.get(2), "朝阳");
			keyMap.put(BJPartString.get(3), "海淀");
			keyMap.put(BJPartString.get(4), "丰台");
			keyMap.put(BJPartString.get(5), "石景山");
			keyMap.put(BJPartString.get(6), "通州");
			keyMap.put(BJPartString.get(7), "昌平");
			keyMap.put(BJPartString.get(8), "大兴");
			keyMap.put(BJPartString.get(9), "亦庄开发区");
			keyMap.put(BJPartString.get(10), "顺义");
			keyMap.put(BJPartString.get(11), "房山");
			keyMap.put(BJPartString.get(12), "门头沟");
			keyMap.put(BJPartString.get(13), "平谷");
			keyMap.put(BJPartString.get(14), "怀柔");
			keyMap.put(BJPartString.get(15), "密云");
			keyMap.put(BJPartString.get(16), "延庆");
			
			keyMap.put(SHPartString.get(0), "静安");
			keyMap.put(SHPartString.get(1), "徐汇");
			keyMap.put(SHPartString.get(2), "黄浦");
			keyMap.put(SHPartString.get(3), "长宁");
			keyMap.put(SHPartString.get(4), "普陀");
			keyMap.put(SHPartString.get(5), "浦东");
			keyMap.put(SHPartString.get(6), "宝山");
			keyMap.put(SHPartString.get(7), "闸北");
			keyMap.put(SHPartString.get(8), "虹口");
			keyMap.put(SHPartString.get(9), "杨浦");
			keyMap.put(SHPartString.get(10), "闵行");
			keyMap.put(SHPartString.get(11), "嘉定");
			keyMap.put(SHPartString.get(12), "松江");
			keyMap.put(SHPartString.get(13), "青浦");
			keyMap.put(SHPartString.get(14), "奉贤");
			keyMap.put(SHPartString.get(15), "金山");
			keyMap.put(SHPartString.get(16), "崇明");
			
			keyMap.put(CDPartString.get(0), "锦江");
			keyMap.put(CDPartString.get(1), "青羊");
			keyMap.put(CDPartString.get(2), "武侯");
			keyMap.put(CDPartString.get(3), "高新");
			keyMap.put(CDPartString.get(4), "成华");
			keyMap.put(CDPartString.get(5), "金牛");
			keyMap.put(CDPartString.get(6), "天府新区");
			keyMap.put(CDPartString.get(7), "高新西");
			keyMap.put(CDPartString.get(8), "双流");
			keyMap.put(CDPartString.get(9), "温江");
			keyMap.put(CDPartString.get(10), "郫都");
			keyMap.put(CDPartString.get(11), "龙泉驿");
			keyMap.put(CDPartString.get(12), "新都");
			keyMap.put(CDPartString.get(13), "青白江");
			keyMap.put(CDPartString.get(14), "金堂");
			keyMap.put(CDPartString.get(15), "大邑");
			keyMap.put(CDPartString.get(16), "浦江");
			keyMap.put(CDPartString.get(17), "新津");
			keyMap.put(CDPartString.get(18), "彭州");
			keyMap.put(CDPartString.get(19), "邛崃");
			keyMap.put(CDPartString.get(20), "崇州");
			keyMap.put(CDPartString.get(21), "都江堰");
			keyMap.put(CDPartString.get(22), "简阳");
			keyMap.put(CDPartString.get(23), "天府新区南区");
			
			
			keyMap.put(TJPartString.get(0), "宝坻");
			keyMap.put(TJPartString.get(1), "滨海新区");
			keyMap.put(TJPartString.get(2), "静海");
			keyMap.put(TJPartString.get(3), "蓟州");
			keyMap.put(TJPartString.get(4), "宁河");
			keyMap.put(TJPartString.get(5), "武清");
			keyMap.put(TJPartString.get(6), "和平");
			keyMap.put(TJPartString.get(7), "南开");
			keyMap.put(TJPartString.get(8), "河西");
			keyMap.put(TJPartString.get(9), "河北");
			keyMap.put(TJPartString.get(10), "河东");
			keyMap.put(TJPartString.get(11), "红桥");
			keyMap.put(TJPartString.get(12), "西青");
			keyMap.put(TJPartString.get(13), "北辰");
			keyMap.put(TJPartString.get(14), "东丽");
			keyMap.put(TJPartString.get(15), "津南");
			keyMap.put(TJPartString.get(16), "塘沽");
			keyMap.put(TJPartString.get(17), "开发区");
			keyMap.put(TJPartString.get(18), "海河教育园区");
			
			
			keyMap.put(FSPartString.get(0), "顺德");
			keyMap.put(FSPartString.get(1), "南海");
			keyMap.put(FSPartString.get(2), "禅城");
			keyMap.put(FSPartString.get(3), "三水");
			keyMap.put(FSPartString.get(4), "高明");
			
			keyMap.put(ZHPartString.get(0), "香洲");
			keyMap.put(ZHPartString.get(1), "金湾");
			keyMap.put(ZHPartString.get(2), "斗门");
			
			
			keyMap.put(XAPartString.get(0), "雁塔");
			keyMap.put(XAPartString.get(1), "长安");
			keyMap.put(XAPartString.get(2), "灞桥");
			keyMap.put(XAPartString.get(3), "未央");
			keyMap.put(XAPartString.get(4), "莲湖");
			keyMap.put(XAPartString.get(5), "碑林");
			keyMap.put(XAPartString.get(6), "新城");
			keyMap.put(XAPartString.get(7), "西咸新区");
			keyMap.put(XAPartString.get(8), "高陵");
			keyMap.put(XAPartString.get(9), "临潼");
			keyMap.put(XAPartString.get(10), "阎良");
			keyMap.put(XAPartString.get(11), "蓝田");
			keyMap.put(XAPartString.get(12), "鄠邑");
			keyMap.put(XAPartString.get(13), "周至");
			
			keyMap.put(HZPartString.get(0), "惠城");
			keyMap.put(HZPartString.get(1), "惠阳");
			keyMap.put(HZPartString.get(2), "大亚湾");
			keyMap.put(HZPartString.get(3), "惠东");
			keyMap.put(HZPartString.get(4), "博罗");
			keyMap.put(HZPartString.get(5), "龙门");
			
		}
	}
	
	
	
	//链家
	public abstract class NEW_HOUSE_AREA{
		public static Map<String, ArrayList<String>> valueMap = new LinkedHashMap<>();
		public static String[] cityMap =  new String[] {GZ,SZ,BJ,SH,CD,TJ,FS,ZH,XA,HZ};
		static{
			valueMap.put(GZ, GZPartString);
			valueMap.put(SZ, SZPartString);
			valueMap.put(BJ, BJPartString);
			valueMap.put(SH, SHPartString);
			valueMap.put(CD, CDPartString);
			valueMap.put(TJ, TJPartString);
			valueMap.put(FS, FSPartString);
			valueMap.put(ZH, ZHPartString);
			valueMap.put(XA, XAPartString);
			valueMap.put(HZ, HZPartString);
		}	
	}
	
	//房天下
	final static ArrayList<String> HworldGZPartString = new  ArrayList<String>(Arrays.asList("nansha","liwan","yuexiu","haizhu","tianhe","baiyun","huangpu","fanyu","huadou","zengcheng","conghua"));
	final static ArrayList<String> HworldSZPartString = new  ArrayList<String>(Arrays.asList("longgang","longhuaqu1","baoan","nanshan","futian","luohu","pingshanqu","guangmingqu","yantian","dapengxinqu"));
	final static ArrayList<String> HworldBJPartString = new  ArrayList<String>(Arrays.asList("chaoyang","haidian","fengtai","xicheng","dongcheng","changping","daxing","tongzhou","fangshan","shunyi","shijingshan","miyun","mentougou","huairou","yanqing","pinggu"));
	final static ArrayList<String> HworldSHPartString = new  ArrayList<String>(Arrays.asList("pudong","jiading","baoshan","minhang","qingpu","songjiang","putuo","xuhui","yangpu","hongkou","fengxian","jinshan","huangpu","jingan","luwan","changning","chongming"));
	final static ArrayList<String> HworldCDPartString = new  ArrayList<String>(Arrays.asList("qingyang","jinjiang","wuhou","chenghua","jinniu","gaoxinqu","gaoxinxiqu","tianfuxinqu","shuangliu","longquanyi","piduqu","wenjiang","xindou","qingbaijiang21"));
	final static ArrayList<String> HworldTJPartString = new  ArrayList<String>(Arrays.asList("baodi","binhaixinqu","jinghai","jizhou","ninghe","wuqing","heping","nankai","hexi","hebei","hedong","hongqiao","xiqing","beichen","dongli","jinnan","tanggu","kaifaqu","haihejiaoyuyuanqu"));
	final static ArrayList<String> HworldFSPartString = new  ArrayList<String>(Arrays.asList("shunde","nanhai","chancheng","sanshui","gaoming"));
	final static ArrayList<String> HworldZHPartString = new  ArrayList<String>(Arrays.asList("xiangzhou","jinwan","doumen","hengqin","gaolangang"));
	final static ArrayList<String> HworldKSPartString = new  ArrayList<String>(Arrays.asList("yushanzhen","huaqiaozhen","zhangpuzhen","zhoushizhen","lujiazhen","bachengzhen","qiandengzhen","zhouzhuangzhen","dianshanhuzhen","jinxizhen"));
	final static ArrayList<String> HworldNBPartString = new  ArrayList<String>(Arrays.asList("fenghua"));
	final static ArrayList<String> HworldXAPartString = new  ArrayList<String>(Arrays.asList("chengnei","chengbei11","chengxi11","chengnan11","gaoxin11","chengdong11","qujiang11","changan11","chanba21","jingkai11","gaoling1"));
	final static ArrayList<String> HworldJMPartString = new  ArrayList<String>(Arrays.asList("pengjiangqu","jianghaiqu","xinhuiqu","heshanshi1","enpingshi1","taishanshi1","kaipingshi"));
	final static ArrayList<String> HworldHZPartString = new  ArrayList<String>(Arrays.asList("huicheng","zhongkai","huiyang","dayawan","boluo","huidong","longmen"));

	//房天下
	public abstract class HOUSE_WORLD_CITY{
		public static HashMap<String, String> keyMap = new HashMap<String, String>();
		static{
			keyMap.put(HworldGZPartString.get(0), "南沙");
			keyMap.put(HworldGZPartString.get(1), "荔湾");
			keyMap.put(HworldGZPartString.get(2), "越秀");
			keyMap.put(HworldGZPartString.get(3), "海珠");
			keyMap.put(HworldGZPartString.get(4), "天河");
			keyMap.put(HworldGZPartString.get(5), "白云");
			keyMap.put(HworldGZPartString.get(6), "黄埔");
			keyMap.put(HworldGZPartString.get(7), "番禺");
			keyMap.put(HworldGZPartString.get(8), "花都");
			keyMap.put(HworldGZPartString.get(9), "增城");
			keyMap.put(HworldGZPartString.get(10), "从化");
			
			keyMap.put(HworldSZPartString.get(0), "龙岗区");
			keyMap.put(HworldSZPartString.get(1), "龙华区");
			keyMap.put(HworldSZPartString.get(2), "宝安区");
			keyMap.put(HworldSZPartString.get(3), "南山区");
			keyMap.put(HworldSZPartString.get(4), "福田区");
			keyMap.put(HworldSZPartString.get(5), "罗湖区");
			keyMap.put(HworldSZPartString.get(6), "坪山区");
			keyMap.put(HworldSZPartString.get(7), "光明新区");
			keyMap.put(HworldSZPartString.get(8), "盐田区");
			keyMap.put(HworldSZPartString.get(9), "大鹏新区");
			
			keyMap.put(HworldBJPartString.get(0), "朝阳");
			keyMap.put(HworldBJPartString.get(1), "海淀");
			keyMap.put(HworldBJPartString.get(2), "丰台");
			keyMap.put(HworldBJPartString.get(3), "西城");
			keyMap.put(HworldBJPartString.get(4), "东城");
			keyMap.put(HworldBJPartString.get(5), "昌平");
			keyMap.put(HworldBJPartString.get(6), "大兴");
			keyMap.put(HworldBJPartString.get(7), "通州");
			keyMap.put(HworldBJPartString.get(8), "房山");
			keyMap.put(HworldBJPartString.get(9), "顺义");
			keyMap.put(HworldBJPartString.get(10), "石景山");
			keyMap.put(HworldBJPartString.get(11), "密云");
			keyMap.put(HworldBJPartString.get(12), "门头沟");
			keyMap.put(HworldBJPartString.get(13), "怀柔");
			keyMap.put(HworldBJPartString.get(14), "延庆");
			keyMap.put(HworldBJPartString.get(15), "平谷");
			
			keyMap.put(HworldSHPartString.get(0), "浦东");
			keyMap.put(HworldSHPartString.get(1), "嘉定");
			keyMap.put(HworldSHPartString.get(2), "宝山");
			keyMap.put(HworldSHPartString.get(3), "闵行");
			keyMap.put(HworldSHPartString.get(4), "青浦");
			keyMap.put(HworldSHPartString.get(5), "松江");
			keyMap.put(HworldSHPartString.get(6), "普陀");
			keyMap.put(HworldSHPartString.get(7), "徐汇");
			keyMap.put(HworldSHPartString.get(8), "杨浦");
			keyMap.put(HworldSHPartString.get(9), "虹口");
			keyMap.put(HworldSHPartString.get(10), "奉贤");
			keyMap.put(HworldSHPartString.get(11), "金山");
			keyMap.put(HworldSHPartString.get(12), "黄浦");
			keyMap.put(HworldSHPartString.get(13), "静安");
			keyMap.put(HworldSHPartString.get(14), "卢湾");
			keyMap.put(HworldSHPartString.get(15), "长宁");
			keyMap.put(HworldSHPartString.get(16), "崇明");
			
			keyMap.put(HworldCDPartString.get(0), "青羊");
			keyMap.put(HworldCDPartString.get(1), "锦江");
			keyMap.put(HworldCDPartString.get(2), "武侯");
			keyMap.put(HworldCDPartString.get(3), "成华");
			keyMap.put(HworldCDPartString.get(4), "金牛");
			keyMap.put(HworldCDPartString.get(5), "高新");
			keyMap.put(HworldCDPartString.get(6), "高新西");
			keyMap.put(HworldCDPartString.get(7), "天府新区");
			keyMap.put(HworldCDPartString.get(8), "双流");
			keyMap.put(HworldCDPartString.get(9), "龙泉驿");
			keyMap.put(HworldCDPartString.get(10), "郫都");
			keyMap.put(HworldCDPartString.get(11), "温江");
			keyMap.put(HworldCDPartString.get(12), "新都");
			keyMap.put(HworldCDPartString.get(13), "青白江");
			
			
			keyMap.put(HworldTJPartString.get(0), "宝坻");
			keyMap.put(HworldTJPartString.get(1), "滨海新区");
			keyMap.put(HworldTJPartString.get(2), "静海");
			keyMap.put(HworldTJPartString.get(3), "蓟州");
			keyMap.put(HworldTJPartString.get(4), "宁河");
			keyMap.put(HworldTJPartString.get(5), "武清");
			keyMap.put(HworldTJPartString.get(6), "和平");
			keyMap.put(HworldTJPartString.get(7), "南开");
			keyMap.put(HworldTJPartString.get(8), "河西");
			keyMap.put(HworldTJPartString.get(9), "河北");
			keyMap.put(HworldTJPartString.get(10), "河东");
			keyMap.put(HworldTJPartString.get(11), "红桥");
			keyMap.put(HworldTJPartString.get(12), "西青");
			keyMap.put(HworldTJPartString.get(13), "北辰");
			keyMap.put(HworldTJPartString.get(14), "东丽");
			keyMap.put(HworldTJPartString.get(15), "津南");
			keyMap.put(HworldTJPartString.get(16), "塘沽");
			keyMap.put(HworldTJPartString.get(17), "开发区");
			keyMap.put(HworldTJPartString.get(18), "海河教育园区");
			
			keyMap.put(HworldFSPartString.get(0), "顺德");
			keyMap.put(HworldFSPartString.get(1), "南海");
			keyMap.put(HworldFSPartString.get(2), "禅城");
			keyMap.put(HworldFSPartString.get(3), "三水");
			keyMap.put(HworldFSPartString.get(4), "高明");
			
			keyMap.put(HworldZHPartString.get(0), "香洲");
			keyMap.put(HworldZHPartString.get(1), "金湾");
			keyMap.put(HworldZHPartString.get(2), "斗门");
			keyMap.put(HworldZHPartString.get(3), "横琴");
			keyMap.put(HworldZHPartString.get(4), "高栏港");
			
			keyMap.put(HworldKSPartString.get(0), "玉山");
			keyMap.put(HworldKSPartString.get(1), "花桥");
			keyMap.put(HworldKSPartString.get(2), "张浦");
			keyMap.put(HworldKSPartString.get(3), "周市");
			keyMap.put(HworldKSPartString.get(4), "陆家");
			keyMap.put(HworldKSPartString.get(5), "巴城");
			keyMap.put(HworldKSPartString.get(6), "千灯");
			keyMap.put(HworldKSPartString.get(7), "周庄");
			keyMap.put(HworldKSPartString.get(8), "淀山湖");
			keyMap.put(HworldKSPartString.get(9), "锦溪");
			
			keyMap.put(HworldNBPartString.get(0), "奉化");
			
			keyMap.put(HworldXAPartString.get(0), "城内");
			keyMap.put(HworldXAPartString.get(1), "城北");
			keyMap.put(HworldXAPartString.get(2), "城西");
			keyMap.put(HworldXAPartString.get(3), "城南");
			keyMap.put(HworldXAPartString.get(4), "高新");
			keyMap.put(HworldXAPartString.get(5), "城东");
			keyMap.put(HworldXAPartString.get(6), "曲江");
			keyMap.put(HworldXAPartString.get(7), "长安");
			keyMap.put(HworldXAPartString.get(8), "浐灞");
			keyMap.put(HworldXAPartString.get(9), "经开");
			keyMap.put(HworldXAPartString.get(10), "高陵");
			
			keyMap.put(HworldJMPartString.get(0), "蓬江");
			keyMap.put(HworldJMPartString.get(1), "江海");
			keyMap.put(HworldJMPartString.get(2), "新会");
			keyMap.put(HworldJMPartString.get(3), "鹤山");
			keyMap.put(HworldJMPartString.get(4), "恩平");
			keyMap.put(HworldJMPartString.get(5), "台山");
			keyMap.put(HworldJMPartString.get(6), "开平");
			
			keyMap.put(HworldHZPartString.get(0), "惠城");
			keyMap.put(HworldHZPartString.get(1), "仲恺");
			keyMap.put(HworldHZPartString.get(2), "惠阳");
			keyMap.put(HworldHZPartString.get(3), "大亚湾");
			keyMap.put(HworldHZPartString.get(4), "博罗");
			keyMap.put(HworldHZPartString.get(5), "惠东");
			keyMap.put(HworldHZPartString.get(6), "龙门");
		}
	}
	
	//房天下
	public abstract class HOUSE_WORLD_AREA{
		public static Map<String, ArrayList<String>> valueMap = new LinkedHashMap<>();
		public static String[] cityMap =  new String[] {GZ,SZ,BJ,SH,CD,TJ,FS,ZH,KS,NB,XA,JM,HZ};
		static{
			valueMap.put(GZ, HworldGZPartString);
			valueMap.put(SZ, HworldSZPartString);
			valueMap.put(BJ, HworldBJPartString);
			valueMap.put(SH, HworldSHPartString);
			valueMap.put(CD, HworldCDPartString);
			valueMap.put(TJ, HworldTJPartString);
			valueMap.put(FS, HworldFSPartString);
			valueMap.put(ZH, HworldZHPartString);
			valueMap.put(KS, HworldKSPartString);
			valueMap.put(NB, HworldNBPartString);
			valueMap.put(XA, HworldXAPartString);
			valueMap.put(JM, HworldJMPartString);
			valueMap.put(HZ, HworldHZPartString);
		}	
	}
	
}
