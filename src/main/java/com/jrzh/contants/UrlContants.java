package com.jrzh.contants;

import java.util.HashMap;
import java.util.Map;

public abstract class UrlContants {

	/** 土地 地址 **/
	public static String LAND_BJ_ALL_URL = "http://ghgtw.beijing.gov.cn/sjzy/front/landsold/dataproxy.do"; // 北京土地
	public static String LAND_BJ_DETAIL_URL = "http://ghgtw.beijing.gov.cn/sjzy/front/landsold";// 北京土地详情
	public static String LAND_GZ_LIST_URL = "https://www.gzlpc.gov.cn/gzlpc/ywpd_tdgl_tdjy_cjgs/list";// 广州土地地址
	public static String LAND_SZ_LIST_URL = "http://www.sz68.com/land/?s=";// 深圳土地地址
	public static String LAND_TJ_LIST_URL = "http://www.tjlandmarket.com/notice/sell_notice/eed8a001717f40858fb5fff7e367e551/eaff75e4453d416a9ca53e8349627877/";// 天津土地地址
	public static String LAND_ZH_LIST_URL = "http://ggzy.zhuhai.gov.cn/exchangeinfo/landexchange/tdjggs"; //珠海土地
	public static String LAND_FS_LIST_URL = "http://www.fsggzy.cn/jyxx/fss/tdkc_1108554/jggg_1108585"; // 佛山土地
	public static String LAND_DG_DATA_URL = "https://list.dgzb.com.cn/dggt/HomePage/GTPublishInfo/getPublishInfo?id="; // 东莞土地
	public static String LAND_QY_LIST_URL = "http://gtzyjy.qyggzy.cn/_public/tjggg.jsp?pageNo="; // 清远土地
	public static String LAND_JM_URL  ="http://zyjy.jiangmen.gov.cn/sztdjggs/index_"; // 江门土地
	public static String LAND_FH_URL  ="http://ztb.fh.gov.cn/Transaction_list.aspx?cls=22"; // 奉化土地
	
	
	
	/** 土地 结束 **/
	
	/**宏观数据  国民经济核算  相关地址*/
	public static Map<String, String> getUrl(){
		Map<String, String> map = new HashMap<>();
		map.put("国内生产总值", "http://data.stats.gov.cn/easyquery.htm?m=QueryData&dbcode=hgnd&rowcode=zb&colcode=sj&wds=%5B%5D&dfwds=%5B%7B%22wdcode%22%3A%22zb%22%2C%22valuecode%22%3A%22A0201%22%7D%5D&k1=1557387261924");
		map.put("国内生产总值指数(上年=100)", "http://data.stats.gov.cn/easyquery.htm?m=QueryData&dbcode=hgnd&rowcode=zb&colcode=sj&wds=%5B%5D&dfwds=%5B%7B%22wdcode%22%3A%22zb%22%2C%22valuecode%22%3A%22A020201%22%7D%5D&k1=1557387734898");
	    map.put("居民消费水平", "http://data.stats.gov.cn/easyquery.htm?m=QueryData&dbcode=hgnd&rowcode=zb&colcode=sj&wds=%5B%5D&dfwds=%5B%7B%22wdcode%22%3A%22zb%22%2C%22valuecode%22%3A%22A020B%22%7D%5D&k1=1557388077320");
		return map;
	}
	// end 
	
	/** 宏观数据    城市连接 */
	public static String LAND_SH_MacroscopicSpider_URL="http://www.stats-sh.gov.cn/html/sjfb/tjnj/"; // 上海
	
}
