package com.jrzh.contants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * 城市区域常量类
 * @author xsc
 *
 */
public interface EsfCityAreaConstants {
	
	public static final String GZ = "gz";
	public static final String SZ = "sz";
	public static final String BJ = "bj";
	public static final String SH = "sh";
	public static final String CD = "cd";
	public static final String TJ = "tj";
	public static final String FS = "fs";
	public static final String ZH = "zh";
	public static final String XA = "xa";
	public static final String KS = "ks";
	public static final String HZ = "hz";
	public static final String JM = "jm";
	public static final String NB = "nb";
	
	//链家
	final static ArrayList<String> GZPartString = new  ArrayList<String>(Arrays.asList("tianhe","yuexiu","liwan","haizhu","panyu","baiyun","huangpu","conghua","zengcheng","huadu","nansha"));
	final static ArrayList<String> SZPartString = new  ArrayList<String>(Arrays.asList("futianqu","nanshanqu","longgangqu","baoanqu","luohuqu","longhuaqu","yantianqu","guangmingxinqu","pingshanqu","dapengxinqu"));
	final static ArrayList<String> BJPartString = new  ArrayList<String>(Arrays.asList("dongcheng","xicheng","chaoyang","haidian","fengtai","shijingshan","tongzhou","changping","daxing","yizhuangkaifaqu","shunyi","fangshan","mentougou","pinggu","huairou","miyun","yanqing"));
	final static ArrayList<String> SHPartString = new  ArrayList<String>(Arrays.asList("jingan","xuhui","huangpu","changning","putuo","pudong","baoshan","zhabei","hongkou","yangpu","minhang","jiading","songjiang","qingpu","fengxian","jinshan","chongming","shanghaizhoubian"));
	final static ArrayList<String> CDPartString = new  ArrayList<String>(Arrays.asList("jinjiang","qingyang","wuhou","gaoxin","chenghua","jinniu","tianfuxinqu","gaoxinxi","shuangliu","wenjiang","pidou","longquanyi","xindu","tianfuxinqunanqu","qingbaijiang","dujiangyan","pengzhou","jianyang"));
	final static ArrayList<String> TJPartString = new  ArrayList<String>(Arrays.asList("heping","nankai","hexi","hebei","hedong","hongqiao","xiqing","beichen","dongli","jinnan","tanggu","kaifaqu","wuqing","binhaixinqu","baodi","jizhou","haihejiaoyuyuanqu"));
	final static ArrayList<String> FSPartString = new  ArrayList<String>(Arrays.asList("chancheng","nanhai","shunde","sanshui","gaoming"));
	final static ArrayList<String> ZHPartString = new  ArrayList<String>(Arrays.asList("xiangzhouqu","jinwanqu","doumenqu"));
	final static ArrayList<String> HZPartString = new  ArrayList<String>(Arrays.asList("huicheng","huiyang","dayawan","huidong"));
	final static ArrayList<String> NBPartString = new  ArrayList<String>(Arrays.asList("fenghuaqu"));
	final static ArrayList<String> XAPartString = new  ArrayList<String>(Arrays.asList("beilin","weiyang","baqiao","xinchengqu","lintong","yanliang","changan4","lianhu","yanta","lantian","huyiqu","zhouzhi","gaoling1xixianxinquxian"));
	final static ArrayList<String> KSPartString = new  ArrayList<String>(Arrays.asList("bacheng","dianshanhu","huaqiao","jinxi","kaifaqu5","lujia","qiandeng","yushan1","zhoushi","zhangpu","zhouzhuang"));
	final static ArrayList<String> JMPartString = new  ArrayList<String>(Arrays.asList("taishanshi","kaipingshi","enpingshi","xinhuiqu","jianghaiqu","pengjiangqu","heshanshi"));

	
	//链家
	public abstract class ESF_HOUSE_CITY{
		public static HashMap<String, String> keyMap = new HashMap<String, String>();
		static {
			keyMap.put(GZPartString.get(0), "天河");
			keyMap.put(GZPartString.get(1), "越秀");
			keyMap.put(GZPartString.get(2), "荔湾");
			keyMap.put(GZPartString.get(3), "海珠");
			keyMap.put(GZPartString.get(4), "番禺");
			keyMap.put(GZPartString.get(5), "白云");
			keyMap.put(GZPartString.get(6), "黄浦");
			keyMap.put(GZPartString.get(7), "从化");
			keyMap.put(GZPartString.get(8), "增城");
			keyMap.put(GZPartString.get(9), "花都");
			keyMap.put(GZPartString.get(10),"南沙");
			
			
			keyMap.put(SZPartString.get(0), "福田区");
			keyMap.put(SZPartString.get(1), "南山区");
			keyMap.put(SZPartString.get(2), "龙岗区");
			keyMap.put(SZPartString.get(3), "宝安区");
			keyMap.put(SZPartString.get(4), "罗湖区");
			keyMap.put(SZPartString.get(5), "龙华区");
			keyMap.put(SZPartString.get(6), "盐田区");
			keyMap.put(SZPartString.get(7), "光明新区");
			keyMap.put(SZPartString.get(8), "坪山区");
			keyMap.put(SZPartString.get(9), "大鹏新区");
			
			
			keyMap.put(BJPartString.get(0), "东城");
			keyMap.put(BJPartString.get(1), "西城");
			keyMap.put(BJPartString.get(2), "朝阳");
			keyMap.put(BJPartString.get(3), "海淀");
			keyMap.put(BJPartString.get(4), "丰台");
			keyMap.put(BJPartString.get(5), "石景山");
			keyMap.put(BJPartString.get(6), "通州");
			keyMap.put(BJPartString.get(7), "昌平");
			keyMap.put(BJPartString.get(8), "大兴");
			keyMap.put(BJPartString.get(9), "亦庄开发区");
			keyMap.put(BJPartString.get(10), "顺义");
			keyMap.put(BJPartString.get(11), "房山");
			keyMap.put(BJPartString.get(12), "门头沟");
			keyMap.put(BJPartString.get(13), "平谷");
			keyMap.put(BJPartString.get(14), "怀柔");
			keyMap.put(BJPartString.get(15), "密云");
			keyMap.put(BJPartString.get(16), "延庆");
			
			
			keyMap.put(SHPartString.get(0), "静安");
			keyMap.put(SHPartString.get(1), "徐汇");
			keyMap.put(SHPartString.get(2), "黄浦");
			keyMap.put(SHPartString.get(3), "长宁");
			keyMap.put(SHPartString.get(4), "普陀");
			keyMap.put(SHPartString.get(5), "浦东");
			keyMap.put(SHPartString.get(6), "宝山");
			keyMap.put(SHPartString.get(7), "闸北");
			keyMap.put(SHPartString.get(8), "虹口");
			keyMap.put(SHPartString.get(9), "杨浦");
			keyMap.put(SHPartString.get(10), "闵行");
			keyMap.put(SHPartString.get(11), "嘉定");
			keyMap.put(SHPartString.get(12), "松江");
			keyMap.put(SHPartString.get(13), "青浦");
			keyMap.put(SHPartString.get(14), "奉贤");
			keyMap.put(SHPartString.get(15), "金山");
			keyMap.put(SHPartString.get(16), "崇明");
			keyMap.put(SHPartString.get(17), "上海周边");
			
			
			keyMap.put(CDPartString.get(0), "锦江");
			keyMap.put(CDPartString.get(1), "青羊");
			keyMap.put(CDPartString.get(2), "武侯");
			keyMap.put(CDPartString.get(3), "高新");
			keyMap.put(CDPartString.get(4), "成华");
			keyMap.put(CDPartString.get(5), "金牛");
			keyMap.put(CDPartString.get(6), "天府新区");
			keyMap.put(CDPartString.get(7), "高新西");
			keyMap.put(CDPartString.get(8), "双流");
			keyMap.put(CDPartString.get(9), "温江");
			keyMap.put(CDPartString.get(10), "郫都");
			keyMap.put(CDPartString.get(11), "龙泉驿");
			keyMap.put(CDPartString.get(12), "新都");
			keyMap.put(CDPartString.get(13), "天府新区南区");
			keyMap.put(CDPartString.get(14), "青白江");
			keyMap.put(CDPartString.get(15), "都江堰");
			keyMap.put(CDPartString.get(16), "彭州");
			keyMap.put(CDPartString.get(17), "简阳");
			
			
			keyMap.put(TJPartString.get(0), "和平");
			keyMap.put(TJPartString.get(1), "南开");
			keyMap.put(TJPartString.get(2), "河西");
			keyMap.put(TJPartString.get(3), "河北");
			keyMap.put(TJPartString.get(4), "河东");
			keyMap.put(TJPartString.get(5), "红桥");
			keyMap.put(TJPartString.get(6), "西青");
			keyMap.put(TJPartString.get(7), "北辰");
			keyMap.put(TJPartString.get(8), "东丽");
			keyMap.put(TJPartString.get(9), "津南");
			keyMap.put(TJPartString.get(10), "塘沽");
			keyMap.put(TJPartString.get(11), "开发区");
			keyMap.put(TJPartString.get(12), "武清");
			keyMap.put(TJPartString.get(13), "滨海新区");
			keyMap.put(TJPartString.get(14), "宝坻");
			keyMap.put(TJPartString.get(15), "蓟州");
			keyMap.put(TJPartString.get(16), "海河教育园区");
			
			keyMap.put(FSPartString.get(0), "禅城");
			keyMap.put(FSPartString.get(1), "南海");
			keyMap.put(FSPartString.get(2), "顺德");
			keyMap.put(FSPartString.get(3), "三水");
			keyMap.put(FSPartString.get(4), "高明");
			keyMap.put(ZHPartString.get(0), "香洲");
			keyMap.put(ZHPartString.get(1), "金湾");
			keyMap.put(ZHPartString.get(2), "斗门");
			
			keyMap.put(HZPartString.get(0), "惠城");
			keyMap.put(HZPartString.get(1), "惠阳");
			keyMap.put(HZPartString.get(2), "大亚湾");
			keyMap.put(HZPartString.get(3), "惠东");
			
			keyMap.put(NBPartString.get(0), "奉化");
			
			keyMap.put(XAPartString.get(0), "碑林");
			keyMap.put(XAPartString.get(1), "未央");
			keyMap.put(XAPartString.get(2), "灞桥");
			keyMap.put(XAPartString.get(3), "新城区");
			keyMap.put(XAPartString.get(4), "临潼");
			keyMap.put(XAPartString.get(5), "阎良");
			keyMap.put(XAPartString.get(6), "长安");
			keyMap.put(XAPartString.get(7), "莲湖");
			keyMap.put(XAPartString.get(8), "雁塔");
			keyMap.put(XAPartString.get(9), "蓝田");
			keyMap.put(XAPartString.get(10), "鄠邑");
			keyMap.put(XAPartString.get(11), "周至");
			keyMap.put(XAPartString.get(12), "高陵");
			keyMap.put(XAPartString.get(13), "西咸新区");
			
			keyMap.put(KSPartString.get(0), "巴城");
			keyMap.put(KSPartString.get(1), "淀山湖");
			keyMap.put(KSPartString.get(2), "花桥");
			keyMap.put(KSPartString.get(3), "锦溪");
			keyMap.put(KSPartString.get(4), "开发区");
			keyMap.put(KSPartString.get(5), "陆家");
			keyMap.put(KSPartString.get(6), "千灯");
			keyMap.put(KSPartString.get(7), "玉山");
			keyMap.put(KSPartString.get(8), "周市");
			keyMap.put(KSPartString.get(9), "张浦");
			keyMap.put(KSPartString.get(10), "周庄");
			
			keyMap.put(JMPartString.get(0), "台山");
			keyMap.put(JMPartString.get(1), "开平");
			keyMap.put(JMPartString.get(2), "恩平");
			keyMap.put(JMPartString.get(3), "新会");
			keyMap.put(JMPartString.get(4), "江海");
			keyMap.put(JMPartString.get(5), "蓬江");
			keyMap.put(JMPartString.get(6), "鹤山");
			
		}
	}
	
	
	
	public abstract class ESF_HOUSE_AREA{
		//链家
		public static Map<String, ArrayList<String>> valueMap = new LinkedHashMap<>();
		public static String[] cityMap =  new String[] {GZ,BJ,SZ,SH,CD,TJ,FS,ZH,HZ,NB,XA,KS,JM};
		static{
			valueMap.put(GZ, GZPartString);
			valueMap.put(BJ, BJPartString);
			valueMap.put(SZ, SZPartString);
			valueMap.put(SH, SHPartString);
			valueMap.put(CD, CDPartString);
			valueMap.put(TJ, TJPartString);
			valueMap.put(FS, FSPartString);
			valueMap.put(ZH, ZHPartString);
			valueMap.put(HZ, HZPartString);
			valueMap.put(NB, NBPartString);
			valueMap.put(XA, XAPartString);
			valueMap.put(KS, KSPartString);
			valueMap.put(JM, JMPartString);
		}	
	}
	
	//房天下二手房 小区编号
	final static ArrayList<String> GZhouseWorldPartString = new ArrayList<String>(Arrays.asList("80","78","84","639","76","74","72","71","73","79","75","15882"));
	final static ArrayList<String> SZhouseWorldPartString = new  ArrayList<String>(Arrays.asList("90","13080","89","87","85","86","13081","13079","88","13082"));
	final static ArrayList<String> BJhouseWorldPartString = new  ArrayList<String>(Arrays.asList("1","0","6","3","2","12","585","10","8","11","7","13","9","14","15","16"));
	final static ArrayList<String> SHhouseWorldPartString = new  ArrayList<String>(Arrays.asList("25","29","30","18","31","586","28","19","26","23","27","32","35","24","21","22","20","996","1046"));
	final static ArrayList<String> CDhouseWorldPartString = new  ArrayList<String>(Arrays.asList("129","130","132","133","131","136","1156","16418","13066","134","140","939","138","13064","135","137","142","148","145","146","147","143","16624","1157"));
	final static ArrayList<String> TJhouseWorldPartString = new  ArrayList<String>(Arrays.asList("37","42","43","41","44","46","49","38","45","39","47","55","52","615","51","53","54","40","614","14634"));
	final static ArrayList<String> FShouseWorldPartString = new  ArrayList<String>(Arrays.asList("617","616","620","618","619","15883"));
	final static ArrayList<String> ZHhouseWorldPartString = new  ArrayList<String>(Arrays.asList("671","730","733","1107","12906","13099","13117","16523","13383"));
	final static ArrayList<String> XAhouseWorldPartString = new  ArrayList<String>(Arrays.asList("480","477","623","478","482","479","483","14681","11881"));
	final static ArrayList<String> KShouseWorldPartString = new  ArrayList<String>(Arrays.asList("13260","13261","13262","13263","13264","13265","13266","13267","13268","12952","13269"));
	final static ArrayList<String> HZhouseWorldPartString = new  ArrayList<String>(Arrays.asList("638","1181","91","96","93","92","94"));
	final static ArrayList<String> JMhouseWorldPartString = new  ArrayList<String>(Arrays.asList("11746","11747","11745","11750","11749","11748","11751"));
	final static ArrayList<String> NBhouseWorldPartString = new  ArrayList<String>(Arrays.asList("168"));


	//房天下-成交二手房
	public abstract class ESF_HOUSE_WORLD_CITY{
		public static HashMap<String, String> keyMap = new HashMap<String, String>();
		static {
			keyMap.put(GZhouseWorldPartString.get(0), "增城");
			keyMap.put(GZhouseWorldPartString.get(1), "番禺");
			keyMap.put(GZhouseWorldPartString.get(2), "南沙");
			keyMap.put(GZhouseWorldPartString.get(3), "花都");
			keyMap.put(GZhouseWorldPartString.get(4), "白云");
			keyMap.put(GZhouseWorldPartString.get(5), "海珠");
			keyMap.put(GZhouseWorldPartString.get(6), "越秀");
			keyMap.put(GZhouseWorldPartString.get(7), "荔湾");
			keyMap.put(GZhouseWorldPartString.get(8), "天河");
			keyMap.put(GZhouseWorldPartString.get(9), "从化");
			keyMap.put(GZhouseWorldPartString.get(10), "黄埔");
			keyMap.put(GZhouseWorldPartString.get(11), "广州周边");
			
			
			keyMap.put(SZhouseWorldPartString.get(0), "龙岗区");
			keyMap.put(SZhouseWorldPartString.get(1), "龙华区");
			keyMap.put(SZhouseWorldPartString.get(2), "宝安区");
			keyMap.put(SZhouseWorldPartString.get(3), "南山区");
			keyMap.put(SZhouseWorldPartString.get(4), "福田区");
			keyMap.put(SZhouseWorldPartString.get(5), "罗湖区");
			keyMap.put(SZhouseWorldPartString.get(6), "坪山区");
			keyMap.put(SZhouseWorldPartString.get(7), "光明新区");
			keyMap.put(SZhouseWorldPartString.get(8), "盐田区");
			keyMap.put(SZhouseWorldPartString.get(9), "大鹏新区");
			
			
			keyMap.put(BJhouseWorldPartString.get(0), "朝阳");
			keyMap.put(BJhouseWorldPartString.get(1), "海淀");
			keyMap.put(BJhouseWorldPartString.get(2), "丰台");
			keyMap.put(BJhouseWorldPartString.get(3), "西城");
			keyMap.put(BJhouseWorldPartString.get(4), "东城");
			keyMap.put(BJhouseWorldPartString.get(5), "昌平");
			keyMap.put(BJhouseWorldPartString.get(6), "大兴");
			keyMap.put(BJhouseWorldPartString.get(7), "通州");
			keyMap.put(BJhouseWorldPartString.get(8), "房山");
			keyMap.put(BJhouseWorldPartString.get(9), "顺义");
			keyMap.put(BJhouseWorldPartString.get(10), "石景山");
			keyMap.put(BJhouseWorldPartString.get(11), "密云");
			keyMap.put(BJhouseWorldPartString.get(12), "门头沟");
			keyMap.put(BJhouseWorldPartString.get(13), "怀柔");
			keyMap.put(BJhouseWorldPartString.get(14), "延庆");
			keyMap.put(BJhouseWorldPartString.get(15), "平谷");
			
			
			keyMap.put(SHhouseWorldPartString.get(0), "浦东");
			keyMap.put(SHhouseWorldPartString.get(1), "嘉定");
			keyMap.put(SHhouseWorldPartString.get(2), "宝山");
			keyMap.put(SHhouseWorldPartString.get(3), "闵行");
			keyMap.put(SHhouseWorldPartString.get(4), "青浦");
			keyMap.put(SHhouseWorldPartString.get(5), "松江");
			keyMap.put(SHhouseWorldPartString.get(6), "普陀");
			keyMap.put(SHhouseWorldPartString.get(7), "徐汇");
			keyMap.put(SHhouseWorldPartString.get(8), "杨浦");
			keyMap.put(SHhouseWorldPartString.get(9), "虹口");
			keyMap.put(SHhouseWorldPartString.get(10), "闸北");
			keyMap.put(SHhouseWorldPartString.get(11), "奉贤");
			keyMap.put(SHhouseWorldPartString.get(12), "金山");
			keyMap.put(SHhouseWorldPartString.get(13), "黄浦");
			keyMap.put(SHhouseWorldPartString.get(14), "静安");
			keyMap.put(SHhouseWorldPartString.get(15), "卢湾");
			keyMap.put(SHhouseWorldPartString.get(16), "长宁");
			keyMap.put(SHhouseWorldPartString.get(17), "崇明");
			keyMap.put(SHhouseWorldPartString.get(18), "上海周边");
			
			
			keyMap.put(CDhouseWorldPartString.get(0), "青羊");
			keyMap.put(CDhouseWorldPartString.get(1), "锦江");
			keyMap.put(CDhouseWorldPartString.get(2), "武侯");
			keyMap.put(CDhouseWorldPartString.get(3), "成华");
			keyMap.put(CDhouseWorldPartString.get(4), "金牛");
			keyMap.put(CDhouseWorldPartString.get(5), "高新区");
			keyMap.put(CDhouseWorldPartString.get(6), "高新西区");
			keyMap.put(CDhouseWorldPartString.get(7), "天府新区");
			keyMap.put(CDhouseWorldPartString.get(8), "双流");
			keyMap.put(CDhouseWorldPartString.get(9), "龙泉驿");
			keyMap.put(CDhouseWorldPartString.get(10), "郫都");
			keyMap.put(CDhouseWorldPartString.get(11), "温江");
			keyMap.put(CDhouseWorldPartString.get(12), "新都");
			keyMap.put(CDhouseWorldPartString.get(13), "都江堰");
			keyMap.put(CDhouseWorldPartString.get(14), "青白江");
			keyMap.put(CDhouseWorldPartString.get(15), "金堂");
			keyMap.put(CDhouseWorldPartString.get(16), "新津");
			keyMap.put(CDhouseWorldPartString.get(17), "彭州");
			keyMap.put(CDhouseWorldPartString.get(18), "崇州");
			keyMap.put(CDhouseWorldPartString.get(19), "邛崃");
			keyMap.put(CDhouseWorldPartString.get(20), "浦江");
			keyMap.put(CDhouseWorldPartString.get(21), "大邑");
			keyMap.put(CDhouseWorldPartString.get(22), "简阳");
			keyMap.put(CDhouseWorldPartString.get(23), "成都周边");
			
			
			keyMap.put(TJhouseWorldPartString.get(0), "和平");
			keyMap.put(TJhouseWorldPartString.get(1), "河东");
			keyMap.put(TJhouseWorldPartString.get(2), "河西");
			keyMap.put(TJhouseWorldPartString.get(3), "南开");
			keyMap.put(TJhouseWorldPartString.get(4), "河北");
			keyMap.put(TJhouseWorldPartString.get(5), "红桥");
			keyMap.put(TJhouseWorldPartString.get(6), "东丽");
			keyMap.put(TJhouseWorldPartString.get(7), "西青");
			keyMap.put(TJhouseWorldPartString.get(8), "津南");
			keyMap.put(TJhouseWorldPartString.get(9), "北辰");
			keyMap.put(TJhouseWorldPartString.get(10), "塘沽");
			keyMap.put(TJhouseWorldPartString.get(11), "开发区");
			keyMap.put(TJhouseWorldPartString.get(12), "武清");
			keyMap.put(TJhouseWorldPartString.get(13), "宝坻");
			keyMap.put(TJhouseWorldPartString.get(14), "蓟州");
			keyMap.put(TJhouseWorldPartString.get(15), "宁河");
			keyMap.put(TJhouseWorldPartString.get(16), "静海");
			keyMap.put(TJhouseWorldPartString.get(17), "大港");
			keyMap.put(TJhouseWorldPartString.get(18), "汉沽");
			keyMap.put(TJhouseWorldPartString.get(19), "天津周边");
			
			keyMap.put(FShouseWorldPartString.get(0), "顺德");
			keyMap.put(FShouseWorldPartString.get(1), "南海");
			keyMap.put(FShouseWorldPartString.get(2), "禅城");
			keyMap.put(FShouseWorldPartString.get(3), "三水");
			keyMap.put(FShouseWorldPartString.get(4), "高明");
			keyMap.put(FShouseWorldPartString.get(5), "佛山周边");
			
			keyMap.put(ZHhouseWorldPartString.get(0), "香洲");
			keyMap.put(ZHhouseWorldPartString.get(1), "金湾");
			keyMap.put(ZHhouseWorldPartString.get(2), "斗门");
			keyMap.put(ZHhouseWorldPartString.get(3), "横琴");
			keyMap.put(ZHhouseWorldPartString.get(4), "坦洲");
			keyMap.put(ZHhouseWorldPartString.get(5), "高新区");
			keyMap.put(ZHhouseWorldPartString.get(6), "南朗");
			keyMap.put(ZHhouseWorldPartString.get(7), "高栏港");
			keyMap.put(ZHhouseWorldPartString.get(8), "珠海周边");
			
			keyMap.put(XAhouseWorldPartString.get(0), "城内");
			keyMap.put(XAhouseWorldPartString.get(1), "城北");
			keyMap.put(XAhouseWorldPartString.get(2), "城西");
			keyMap.put(XAhouseWorldPartString.get(3), "城南");
			keyMap.put(XAhouseWorldPartString.get(4), "高新");
			keyMap.put(XAhouseWorldPartString.get(5), "城东");
			keyMap.put(XAhouseWorldPartString.get(6), "长安");
			keyMap.put(XAhouseWorldPartString.get(7), "曲江新区");
			keyMap.put(XAhouseWorldPartString.get(8), "西咸新区");
			
			keyMap.put(KShouseWorldPartString.get(0), "玉山");
			keyMap.put(KShouseWorldPartString.get(1), "花桥");
			keyMap.put(KShouseWorldPartString.get(2), "张浦");
			keyMap.put(KShouseWorldPartString.get(3), "巴城");
			keyMap.put(KShouseWorldPartString.get(4), "锦溪");
			keyMap.put(KShouseWorldPartString.get(5), "淀山湖");
			keyMap.put(KShouseWorldPartString.get(6), "千灯");
			keyMap.put(KShouseWorldPartString.get(7), "陆家");
			keyMap.put(KShouseWorldPartString.get(8), "周市");
			keyMap.put(KShouseWorldPartString.get(9), "开发区");
			keyMap.put(KShouseWorldPartString.get(10), "周庄");
			
			keyMap.put(HZhouseWorldPartString.get(0), "惠城");
			keyMap.put(HZhouseWorldPartString.get(1), "仲恺");
			keyMap.put(HZhouseWorldPartString.get(2), "惠阳");
			keyMap.put(HZhouseWorldPartString.get(3), "大亚湾");
			keyMap.put(HZhouseWorldPartString.get(4), "博罗");
			keyMap.put(HZhouseWorldPartString.get(5), "惠东");
			keyMap.put(HZhouseWorldPartString.get(6), "龙门");
			
			
			keyMap.put(JMhouseWorldPartString.get(0), "蓬江");
			keyMap.put(JMhouseWorldPartString.get(0), "江海");
			keyMap.put(JMhouseWorldPartString.get(0), "新会");
			keyMap.put(JMhouseWorldPartString.get(0), "鹤山");
			keyMap.put(JMhouseWorldPartString.get(0), "恩平");
			keyMap.put(JMhouseWorldPartString.get(0), "台山");
			keyMap.put(JMhouseWorldPartString.get(0), "开平");
			
			keyMap.put(NBhouseWorldPartString.get(0), "奉化");
		}
	}
	
	public abstract class ESF_HOUSE_WORLD_AREA{
		//成交
		//房天下
		public static Map<String, ArrayList<String>> valueMap = new LinkedHashMap<>();
		public static String[] cityMap =  new String[] {GZ,SZ,BJ,SH,CD,TJ,FS,ZH,XA,KS,HZ,JM,NB};
		static{
			valueMap.put(GZ, GZhouseWorldPartString);
			valueMap.put(SZ, SZhouseWorldPartString);
			valueMap.put(BJ, BJhouseWorldPartString);
			valueMap.put(SH, SHhouseWorldPartString);
			valueMap.put(CD, CDhouseWorldPartString);
			valueMap.put(TJ, TJhouseWorldPartString);
			valueMap.put(FS, FShouseWorldPartString);
			valueMap.put(ZH, ZHhouseWorldPartString);
			valueMap.put(XA, XAhouseWorldPartString);
			valueMap.put(KS, KShouseWorldPartString);
			valueMap.put(HZ, HZhouseWorldPartString);
			valueMap.put(JM, JMhouseWorldPartString);
			valueMap.put(NB, NBhouseWorldPartString);
		}	
	}
	
}
