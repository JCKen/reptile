package com.jrzh.contants;

import java.util.HashMap;
import java.util.Map;

public interface Contants {
	
	public static final  String REPTILE_SESSION_KEY="REPTILE_SESSION_KEY";
	public static final String MODULE_NAME = "reptile";
	public static final String USER_NAME = "test";
	public static final String USER_PASSWORD = "test";
	
	
	public abstract class USER_IS_STOP {
		public static String STOP = "0";
		public static String NO_STOP = "1";
	}
	public static final String WING="W";
	public static final String SUCCESS="S";
	public abstract class YGJY_SALE_TYPE{
		public static Map<String, String> valueMap = new HashMap<String, String>();
		static{
		}
	}
	
	public abstract class YGJY_GET_DATA_SALE_TYPE{
		public static Map<String, String> paraMap = new HashMap<String, String>();
		static{
			paraMap.put("modeID", "1");		
			paraMap.put("hfID", "0");	
			paraMap.put("unitType", "0");	
			paraMap.put("houseStatusID", "0");	
			paraMap.put("totalAreaID", "0");	
			paraMap.put("inAreaID", "0");
		}
	}
	
	//房间销售情况
	public abstract class SALE_TYPE{
		public static Map<String, String> SALEMAP = new HashMap<String, String>();
		static{
			SALEMAP.put("不可销售", "0");		
			SALEMAP.put("预售可售", "1");	
			SALEMAP.put("确权不可售", "2");	
			SALEMAP.put("确权可售", "3");	
			SALEMAP.put("已过户", "4");	
			SALEMAP.put("强制冻结", "5");
		}
	}
	
	//试调类型
	public abstract class TEMPLATE_TYPE{
		public static final  String TUDIINFO="tudiInfo";
		public static final  String ESFHOUSE="esfHouse";
		public static final  String NEWHOUSE="newHouse";
		public static Map<String, String> valueMap = new HashMap<String, String>();
		static{
			valueMap.put(NEWHOUSE, "新房试调");		
			valueMap.put(ESFHOUSE, "二手房试调");	
			valueMap.put(TUDIINFO, "土地试调");	
		}
	}
	
}
