package com.jrzh.util;


import java.util.ArrayList;
import java.util.List;

import com.jrzh.common.utils.ReflectUtils;

public class ObjectConvert {
	
	public static <T> T  columnsToObj(Class<T> clazz, Object[] columns ,String[] fields) throws Exception{
		T t = clazz.newInstance();
		for(int i = 0; i<columns.length; i++){
			if(columns[i] == null) continue;
			ReflectUtils.setValue(fields[i], t, ReflectUtils.stringConvertByFieldType(ReflectUtils.getField(clazz, fields[i]), columns[i].toString()));
		}
		return t;
	}
	
	public static <T> List<T> dataToList(Class<T> clazz, List<Object[]> data, String[] fields) throws Exception{
		List<T> result = new ArrayList<T>();
		for(Object[] columns : data){
			result.add(columnsToObj(clazz, columns, fields));
		}
		return result;
	}
}
