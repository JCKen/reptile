package com.jrzh.util;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpPostJsonUtils {
	
	final static String url = "http://ris.szpl.gov.cn:8080/api/marketInfoShow/getFjzsInfoData";
    final static String params = "{\"startDate\":\"2019-01-1\",\"endDate\":\"2019-03-1\",\"dateType\":\"\"}";
    
    
    public static String post(String strURL, String params) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		String responseContent = null;
		try {
			HttpPost httpPost = new HttpPost(strURL);
			httpPost.addHeader("Accept", "application/json");
			httpPost.addHeader("Accept-Encoding", "gzip, deflate");
			httpPost.addHeader("Accept-Language", "zh-CN,zh;q=0.9");
			httpPost.addHeader("Cache-Control", "no-cache");
			httpPost.addHeader("Connection", "keep-alive");
			httpPost.addHeader("Content-Type", "application/json");
			httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36");
			httpPost.addHeader("Cookie", "ASP.NET_SessionId=22mlrd2q1qxcf355mwjirunq; ASP.NET_SessionId_NS_Sig=oenCV6mdmzx9-Av7; zlpt-session-id=9af0684e-a02e-4bd0-8da7-a8aab0b7d700");
			httpPost.addHeader("X-Requested-With", "XMLHttpRequest");
			httpPost.setEntity(new StringEntity(params));
			CloseableHttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			responseContent = EntityUtils.toString(entity, "UTF-8"); 
			response.close();
			httpClient.close();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return responseContent;
    }

}
