package com.jrzh.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

import sun.misc.BASE64Decoder;

public class ImgBase64Utils {

	/**
	 * 解析base64，返回图片所在路径
	 * 
	 * @param base64Info
	 * @return
	 */
	public static String decodeBase64(String base64Info, File filePath) {
		if (StringUtils.isEmpty(base64Info)) {
			return null;
		}
		BASE64Decoder decoder = new BASE64Decoder();
		String[] arr = base64Info.split("base64,");
		try {
			byte[] buffer = decoder.decodeBuffer(arr[1]);
			OutputStream os = new FileOutputStream(filePath.getPath());
			os.write(buffer);
			os.close();
		} catch (IOException e) {
			throw new RuntimeException();
		}
		return filePath.getPath();
	}
}
