package com.jrzh.util;

import java.io.File;

import org.apache.log4j.Logger;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeDriverService.Builder;

import com.jrzh.framework.contants.GeneralConstant;

public class ChromeOptionsUtils {

	static Logger log = Logger.getLogger(ChromeOptionsUtils.class);
	
	private static Builder builder = new ChromeDriverService.Builder();
	
	static String WINDOWS_FILE = "C:\\chromedriver.exe";
	
	static String LINUX_FILE = "/usr/bin/chromedriver";
	
	private static ChromeDriverService chromeDriverService = "Linux".equals(GeneralConstant.RUN_ENVIR)
			? builder.usingDriverExecutable(new File(LINUX_FILE)).usingAnyFreePort().build()
			: builder.usingDriverExecutable(new File(WINDOWS_FILE)).usingAnyFreePort().build();

	public synchronized static ChromeDriverService getChromeDriverService() {
		if (chromeDriverService == null) {
			if ("Linux".equals(GeneralConstant.RUN_ENVIR)) {
				chromeDriverService = builder.usingDriverExecutable(new File(LINUX_FILE)).usingAnyFreePort().build();
			} else {
				chromeDriverService = builder.usingDriverExecutable(new File(WINDOWS_FILE)).usingAnyFreePort().build();
			}
		}
		return chromeDriverService;
	}
}
