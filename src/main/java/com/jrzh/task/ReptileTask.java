package com.jrzh.task;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.contants.Contants;
import com.jrzh.factory.gz.GZNewhouseDealEveryday;
import com.jrzh.factory.sz.SZGQBSpider;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseFloorInfoModel;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseFloorInfoSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.EsfHouseWoeldThread;
import com.jrzh.thread.EsfLatitudeLongitudeThread;
import com.jrzh.thread.EsfLianJiaThread;
import com.jrzh.thread.GTJLatLntInfoThread;
import com.jrzh.thread.LatitudeLongitudeThread;
import com.jrzh.thread.NewHouseFangtxThread;
import com.jrzh.thread.NewHouseLianjiaThread;
import com.jrzh.thread.UserThread;
import com.jrzh.thread.bj.BjLandThread;
import com.jrzh.thread.cd.CdLandThread;
import com.jrzh.thread.dg.DgLandThread;
import com.jrzh.thread.fh.FhThread;
import com.jrzh.thread.fs.FsLandThread;
import com.jrzh.thread.gj.DomestictradeThread;
import com.jrzh.thread.gj.FinanceThread;
import com.jrzh.thread.gj.FixedInvestment;
import com.jrzh.thread.gj.PopulationTheard;
import com.jrzh.thread.gj.gmjj.EconomicsThread;
import com.jrzh.thread.gz.GzLandThread;
import com.jrzh.thread.gz.GzMacroscopicThread;
import com.jrzh.thread.gz.GzYgjyHouseInfoThread;
import com.jrzh.thread.hz.HzLandThread;
import com.jrzh.thread.jm.JmLandThread;
import com.jrzh.thread.ks.KsLandThread;
import com.jrzh.thread.sh.ShLandThread;
import com.jrzh.thread.sh.ShMacroscopicThread;
import com.jrzh.thread.sz.SzLandThread;
import com.jrzh.thread.tj.TjLandThread;
import com.jrzh.thread.xa.XaLandThread;
import com.jrzh.thread.zh.ZhLandThread;

/**
 ** @author lee
 ** 
 *         0 0 10,14,16 * * ? 每天上午10点，下午2点，4点 0 0/30 9-17 * * ? 朝九晚五工作时间内每半小时 0
 *         0 12 ? * WED 表示每个星期三中午12点 "0 0 12 * * ?" 每天中午12点触发 "0 15 10 * * ?"
 *         每天上午10:15触发 "0 15 10 * * ? 2005" 2005年的每天上午10:15触发 "0 * 14 * * ?"
 *         在每天下午2点到下午2:59期间的每1分钟触发 "0 0/5 14 * * ?" 在每天下午2点到下午2:55期间的每5分钟触发 "0
 *         0/5 14,18 * * ?" 在每天下午2点到2:55期间和下午6点到6:55期间的每5分钟触发 "0 0-5 14 * * ?"
 *         在每天下午2点到下午2:05期间的每1分钟触发 "0 10,44 14 ? 3 WED" 每年三月的星期三的下午2:10和2:44触发
 *         "0 15 10 ? * MON-FRI" 周一至周五的上午10:15触发 "0 15 10 15 * ?" 每月15日上午10:15触发
 *         "0 15 10 L * ?" 每月最后一日的上午10:15触发 "0 15 10 ? * 6L" 每月的最后一个星期五上午10:15触发
 *         "0 15 10 ? * 6L 2002-2005" 2002年至2005年的每月的最后一个星期五上午10:15触发 "0 15 10 ?
 *         * 6#3" 每月的第三个星期五上午10:15触发
 **/
@EnableScheduling
@Component
public class ReptileTask {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private ReptileServiceManage reptileServiceManage;

	/**
	 * 1.土地数据任务 
	 * 每月1号 凌晨1点 获取国土局数据
	 **/
	@Scheduled(cron = "0 0 01 1 * ?")
	public void Land() throws ProjectException  {
		log.info("#########【执行凌晨定时任务】###############");
		log.info("土地数据执行定时器 starting");
		new ShLandThread(reptileServiceManage).start();
		new BjLandThread(reptileServiceManage).start();
		new SzLandThread(reptileServiceManage).start();
		new GzLandThread(reptileServiceManage).start();
		new CdLandThread(reptileServiceManage).start();
		new TjLandThread(reptileServiceManage).start();
		new FsLandThread(reptileServiceManage).start();
		new ZhLandThread(reptileServiceManage).start();
		new JmLandThread(reptileServiceManage).start();
		new FhThread(reptileServiceManage).start();
		new HzLandThread(reptileServiceManage).start();
		new XaLandThread(reptileServiceManage).start();
		new KsLandThread(reptileServiceManage).start();
		log.info("土地数据定时器 ending");
	}

	/**
	 * 获取OA用户数据
	 * 每天凌晨0点01分获取
	 */
	@Scheduled(cron = "0 01 0 * * ?")
	public void loadUser() throws ProjectException{
		// 更新用户数据
		log.info("用户数据执行定时器 starting");
		new UserThread(reptileServiceManage).start();
		log.info("用户数据执行定时器 end");
	}

	/**
	 * 链家和房天下新房定时器
	 * 每周三 凌晨 0点 01分 自动获取一手房
	 */
	@Scheduled(cron = "0 01 21 ? * FRI")
	//@Scheduled(cron = "30 04 20 * * ?")
	public void checkNewH() throws ProjectException {
		log.info("新房数据执行定时器 starting");
		new NewHouseLianjiaThread(reptileServiceManage).start();
		new NewHouseFangtxThread(reptileServiceManage).start();
		log.info("新房数据执行定时器 end");
	}

	/**
	 * 链家和房天下二手房定时器 
	 * 每周三 凌晨 0点 01分 30秒 自动获取 二手房
	 **/
	@Scheduled(cron = "30 01 21 ? * FRI")
	//@Scheduled(cron = "0 40 14 * * ?")
	public void checkEsf() throws ProjectException {
		log.info("二手房定时器 starting");
		new EsfHouseWoeldThread(reptileServiceManage).start();
		new EsfLianJiaThread(reptileServiceManage).start();
		log.info("二手房定时器 ending");
	}


	/**
	 * 阳光家缘每天销售信息
	 **/
	 @Scheduled(cron = "0 0 08 * * ?")
	public void saleDataTask() throws ProjectException {
		log.info("阳光家缘每天销售信息 starting");
		reptileServiceManage.reptileYgjySaleInfoService.addYgjySaleData(SessionUser.getSystemUser());
		log.info("阳光家缘每天销售信息 ending");
	}

	/**
	 * 房管局数据获取
	 **/
	 @Scheduled(cron = "10 0 01 * * ?")
	public void houseDataTask() throws ProjectException {
		log.info("阳光家缘-房屋信息执行定时器 starting");
		reptileServiceManage.reptileYgjyHouseInfoService.addBJFangGuanJuData(SessionUser.getSystemUser());
		reptileServiceManage.reptileYgjyHouseInfoService.addSZFangGuanJuData(SessionUser.getSystemUser());
		reptileServiceManage.reptileYgjyHouseInfoService.addYgjyHouseData(SessionUser.getSystemUser());
		reptileServiceManage.reptileHouseProjectNameService.addSHFangGuanJuHouseData(SessionUser.getSystemUser());
		log.info("阳光家缘-房屋信息执行定时器 ending");
	}

	/**
	 * 北京房管局销售表
	 **/
	 @Scheduled(cron = "0 00 01 * * ?")
	public void checkSaleInfo() throws ProjectException {
		log.info("阳光家缘-房屋信息执行定时器 starting");
		reptileServiceManage.reptileSaleInfoService.BjSaleInfo(SessionUser.getSystemUser());
		log.info("阳光家缘-房屋信息执行定时器 ending");
	}

	/**
	 * 房管局售控表楼盘与楼层信息数据获取（售控表）
	 **/
	@Scheduled(cron = "0 30 01 * * ?")
	public void checkHouseFloorData() throws ProjectException {
		log.info("执行定时器售控表 starting" + DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
		ReptileYgjyHouseFloorInfoSearch search = new ReptileYgjyHouseFloorInfoSearch();
		search.setEqStatus(Contants.WING);
		ReptileYgjyHouseFloorInfoModel model = reptileServiceManage.reptileYgjyHouseFloorInfoService.first(search);
		if (model == null)
			return;
		reptileServiceManage.reptileYgjyHouseFloorInfoService.checkFloorTask(model);
		reptileServiceManage.reptileYgjyHouseFloorInfoService.SZCheckFloorTask(model);
		reptileServiceManage.reptileYgjyHouseFloorInfoService.BJCheckFloorTask(model);
		reptileServiceManage.reptileYgjyHouseFloorInfoService.SHCheckFloorTask(model);
		log.info("执行定时器售控表 ending" + DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
	}

	/**
	 * 获取阳光家缘获取 1.基本信息 2.售控表 3.预售证信息
	 **/
	 @Scheduled(cron = "30 05 0 * * ?")
	public void ygjyDataTask() throws ProjectException {
		log.info("阳光家缘-房屋信息执行定时器 starting");
		new GzYgjyHouseInfoThread("广州", reptileServiceManage).start();
		log.info("阳光家缘-房屋信息执行定时器 ending");
	}

	/**
	 * 经纬度获取
	 * 每周四凌晨0点02分自动获取 经纬度
	 **/
	//@Scheduled(cron = "0 02 0 ? * THU")
	@Scheduled(cron = "0 0 02 ? * SAT")
	public void checkLatitudeLongitude() throws ProjectException {
		log.info("执行定时器经纬度 starting" + DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
		new LatitudeLongitudeThread(reptileServiceManage).start();
		new EsfLatitudeLongitudeThread(reptileServiceManage).start();
		new GTJLatLntInfoThread(reptileServiceManage).start();
		log.info("执行定时器经纬度 ending" + DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
	}

	/**
	 * 深圳房管局的供求比
	 **/
	@Scheduled(cron = "0 33 01 * * ?")
	public void szGqbDataTask() throws ProjectException {
		log.info("执行深圳房管局供求比定时器 starting" + DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
		SZGQBSpider.saveInfo(reptileServiceManage, SessionUser.getSystemUser());
		log.info("执行深圳房管局供求比定时器 ending" + DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
	}

	/**
	 * 国家宏观数据的房地产指标数据 , 每一年 三月 拉取一次
	 */
	@Scheduled(cron = "0 10 01 ? 3 WED")
	public void estateDataTask() throws ProjectException {
		//-------------------  房地产
//		new EstateInvestThread(reptileServiceManage).start();
//		new EstateInvestSjThread(reptileServiceManage).start();
//		new EstateLandThread(reptileServiceManage).start();
//		new EstateCompleteThread(reptileServiceManage).start();
//		new EstateHousingThread(reptileServiceManage).start();
//		new EstateOfficeThread(reptileServiceManage).start();
//		new EstateBusinessThread(reptileServiceManage).start();
//		new EstateSpfAreaThread(reptileServiceManage).start();
//		new EstateSpfSaleThread(reptileServiceManage).start();
//		new EstateHousingAreaThread(reptileServiceManage).start();
//		new EstateHousingSaleThread(reptileServiceManage).start();
//		new EstateOfficeAreaThread(reptileServiceManage).start();
//		new EstateOfficeSaleThread(reptileServiceManage).start();
//		new EstateBusinessAreaThread(reptileServiceManage).start();
//		new EstateBusinessSaleThread(reptileServiceManage).start();
		//---------------------  国民经济核算
		new EconomicsThread(reptileServiceManage).start();
		//---------------------  人口线程
		new PopulationTheard(reptileServiceManage).start();
		//---------------------  财政线程
		new FinanceThread(reptileServiceManage).start();
		//---------------------  国内贸易
		new DomestictradeThread(reptileServiceManage).start();
		//---------------------  固定投资额和房地产
		new FixedInvestment(reptileServiceManage).start();
		//---------------------  上海宏观数据
		new ShMacroscopicThread(reptileServiceManage).start();
		//---------------------  广州宏观数据
		new GzMacroscopicThread(reptileServiceManage).start();
		log.info("------执行宏观数据线程-------");
	}

}
