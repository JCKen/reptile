package com.jrzh.listener;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.jrzh.common.utils.DateUtil;
import com.jrzh.config.ProjectConfigration;
import com.jrzh.contants.Contants;
import com.jrzh.db.data.execute.reptile.ExecuteReptileConfig;
import com.jrzh.db.data.execute.reptile.ExecuteReptileI18n;
import com.jrzh.db.data.execute.reptile.ExecuteReptileResources;
import com.jrzh.framework.migration.MigrationHelper;
import com.jrzh.mvc.constants.SysConstant;
import com.jrzh.mvc.service.sys.ConfigServiceI;

@Component("reptileStartUpListener")
@Order(99)
public class ReptileStartUpListener implements ApplicationListener<ContextRefreshedEvent> {

	Log log = LogFactory.getLog(getClass());

	@Autowired
	private ProjectConfigration projectConfigration;

	@Resource(name = "configService")
	private ConfigServiceI configService;

	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (event.getApplicationContext().getParent() == null) {
			log.info(DateUtil.getMark() + "reptile启动事件触发.......");
			SysConstant.MODULE_LIST.add(Contants.MODULE_NAME);
			executeDataBase();
			executeDbData(event.getApplicationContext());
			initCacheData();
		}
	}

	/**
	 * 执行数据库脚本
	 */
	private void executeDataBase() {
		MigrationHelper.updateDataBase(projectConfigration, "reptile");
	}

	private void executeDbData(ApplicationContext context) {
		new ExecuteReptileI18n().execute(context);
		new ExecuteReptileResources().execute(context);
		new ExecuteReptileConfig().execute(context);
	}


	/**
	 * 初始化缓存数据
	 */
	private void initCacheData() {
		try {
			configService.initCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
