package com.jrzh.thread;

import java.net.URLEncoder;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.alibaba.fastjson.JSONObject;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;
import com.jrzh.mvc.search.reptile.ReptileGtjAreaInfoSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.tools.MyPayUtils;

/**
 * 获取土地经纬度线程
 * @author admin
 *
 */
public class GTJLatLntInfoThread extends Thread {

	private ReptileServiceManage reptileServiceManage;

	private static Log log = LogFactory.getLog(GTJLatLntInfoThread.class);

	public GTJLatLntInfoThread(ReptileServiceManage reptileServiceManage) {
		super();
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setLat("lat");
		search.setLnt("lnt");
		try {
			List<ReptileGtjAreaInfoModel> list = reptileServiceManage.reptileGtjAreaInfoService
					.findListBySearch(search);
			String[] appKeyArr = { "Zute9dUseygWr8g10lqn8O4nYfeWhb5G", "3B5pgekUc6OYvUGoQcLHnPjumemNlXKP",
					"sXgxtgqipGdLR91g2WPEHec5db3BMrXe" };
			for (ReptileGtjAreaInfoModel model : list) {

				Integer index = 0;
				String getAddress = model.getAddress();
				String address = model.getCity() + getAddress;
				address = URLEncoder.encode(address, "utf-8");
				String appkey = appKeyArr[index];
				String url = "http://api.map.baidu.com/geocoder/v2/?address=" + address + "&output=json&ak=" + appkey;
				String requestContent;
				try {
					requestContent = MyPayUtils.sendHttpByGet(url, 2000000, 20000000);
				} catch (Exception e) {
					try {
						requestContent = MyPayUtils.sendHttpByGet(url, 2000000, 20000000);
					} catch (Exception ex) {
						continue;
					}
				}
				JSONObject obj;
				try {
					obj = JSONObject.parseObject(requestContent);
				} catch (Exception e) {
					try {
						obj = JSONObject.parseObject(requestContent);
					} catch (Exception ex) {
						continue;
					}
				}
				// 今日数据获取量已到上限
				if (StringUtils.equals(String.valueOf(obj.get("status")), "302")) {
					++index;
					if (index == appKeyArr.length) {
						index = 0;
					}
					appkey = appKeyArr[index];
					url = "http://api.map.baidu.com/geocoder/v2/?address=" + address + "&output=json&ak=" + appkey;
					requestContent = MyPayUtils.sendHttpByGet(url, 2000000, 20000000);
					obj = JSONObject.parseObject(requestContent);
				}
				// 地址名长度过长
				if (StringUtils.equals(String.valueOf(obj.get("status")), "2")) {
					if (getAddress.contains("，")) {
						getAddress = getAddress.substring(0, getAddress.indexOf("，"));
					}
					if (getAddress.contains("、")) {
						getAddress = getAddress.substring(0, getAddress.indexOf("、"));
					}

					if (getAddress.contains("(")) {
						String temp = getAddress.substring(getAddress.indexOf("("), getAddress.indexOf(")") + 1);
						getAddress = getAddress.replace(temp, "");
					}
					if (getAddress.contains("（")) {
						String temp;
						if (getAddress.contains(")")) {
							temp = getAddress.substring(getAddress.indexOf("（"), getAddress.indexOf(")") + 1);
						} else {
							temp = getAddress.substring(getAddress.indexOf("（"), getAddress.indexOf("）") + 1);
						}
						getAddress = getAddress.replace(temp, "");
					}
					address = model.getCity() + getAddress;
					address = URLEncoder.encode(address, "utf-8");
					url = "http://api.map.baidu.com/geocoder/v2/?address=" + address + "&output=json&ak=" + appkey;
					requestContent = MyPayUtils.sendHttpByGet(url, 2000000, 20000000);
					obj = JSONObject.parseObject(requestContent);
				}
				JSONObject result = JSONObject.parseObject(String.valueOf(obj.get("result")));
				// 找不到地块
				if (result == null) {
					continue;
				}
				JSONObject location = JSONObject.parseObject(String.valueOf(result.get("location")));
				String lng = String.valueOf(location.get("lng"));
				String lat = String.valueOf(location.get("lat"));
				log.info(lng + lat);
				model.setLat(lat);
				model.setLnt(lng);
				reptileServiceManage.reptileGtjAreaInfoService.edit(model, SessionUser.getSystemUser());
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
