package com.jrzh.thread.gz;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

/***
 *  广州土地线程
 **/
public class GzLandThread extends Thread {

	public static Log log = LogFactory.getLog(GzLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public GzLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveGzLandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.info("###获取广州土地数据异常####");
		}
	}

}
