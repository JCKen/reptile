package com.jrzh.thread.gz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.contants.Contants;
import com.jrzh.factory.gz.GzYgjyHouseInfoSpider;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseFloorInfoModel;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseInfoModel;
import com.jrzh.mvc.model.reptile.ReptileYgjyHousePreSaleCertificateModel;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseFloorInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHousePreSaleCertificateSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.tools.UrlTools;

public class GzYgjyHouseInfoThread extends Thread {
	
	Log log = LogFactory.getLog(getClass());
	
	private String city;
	private ReptileServiceManage reptileServiceManage;

	public GzYgjyHouseInfoThread(String city, ReptileServiceManage reptileServiceManage) {
		this.city = city;
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			ReptileYgjyHouseInfoSearch search = new ReptileYgjyHouseInfoSearch();
			search.setEqualCity(city);
			List<ReptileYgjyHouseInfoModel> models = reptileServiceManage.reptileYgjyHouseInfoService.findListBySearch(search);
			//阳光佳缘预售证信息表
			for (ReptileYgjyHouseInfoModel model : models) {
				try {
					String baseUrl =model.getUrl();
					Document doc;
					try {
						doc = Jsoup.connect(baseUrl).timeout(500000).get();
					} catch (Exception e) {
						continue;
					}
					Element frame = doc.getElementById("frame");
					if(frame==null) continue;
					String url =  GzYgjyHouseInfoSpider.DOMIN+frame.attr("src");
					Document dataDoc = Jsoup.connect(url).timeout(50000).get();
					Elements elements =  dataDoc.getElementsByClass("tab_style03");
					if(elements==null || elements.size() <= 0) continue;
					Element table = elements.get(0);
					if (table == null){
						continue;
					}
					Elements trs = table.getElementsByTag("tr");
					if(trs ==null || trs.size()<0){
						continue;
					}
					Map<String, String> valueMap = new HashMap<>();
					for(int i =0 ;i< trs.size(); i++){
						Elements tds = trs.get(i).getElementsByTag("td");
						for(int j =0 ;j< tds.size(); j++){
							if(tds.size()>2){
								valueMap.put(StringUtils.replace(tds.get(0).text(), "：", ""), tds.get(1).text());
								valueMap.put(StringUtils.replace(tds.get(2).text(), "：", ""), tds.get(3).text());
							}else if(tds.size() == 2){
								valueMap.put(StringUtils.replace(tds.get(0).text(), "：", ""), tds.get(1).text());
							}else {
								continue;
							}
						}
					}
					model.setCity("广州");
					model.setAdministrativeArea(valueMap.get("行政区划"));
					model.setLandArea(StringUtils.isBlank(valueMap.get("占地面积")) ? 0.0 : Double.valueOf(valueMap.get("占地面积")) );
					model.setConstructionArea(StringUtils.isBlank(valueMap.get("总建筑面积")) ? 0.0 : Double.valueOf(valueMap.get("总建筑面积")));
					model.setSoldArea(StringUtils.isBlank(valueMap.get("已售总面积")) ? 0.0 : Double.valueOf(valueMap.get("已售总面积")));
					model.setUnsoldArea(StringUtils.isBlank(valueMap.get("未售总面积")) ? 0.0 : Double.valueOf(valueMap.get("未售总面积")));
					model.setSumSold(StringUtils.isBlank(valueMap.get("已售总套数")) ? 0.0 : Double.valueOf(valueMap.get("已售总套数")));
					model.setSumUnsold(StringUtils.isBlank(valueMap.get("未售总套数")) ? 0.0 : Double.valueOf(valueMap.get("未售总套数")));
					reptileServiceManage.reptileYgjyHouseInfoService.edit(model, SessionUser.getSystemUser());
					Thread.sleep(1500);
					log.info("###"+model.getProductName()+"###结束");
					log.info("###"+model.getId()+"###结束");
					//抓取售控表code
					Map<String, String> urlParams = UrlTools.getUrlParams(baseUrl);
					String name = urlParams.get("name");
					String pjID = urlParams.get("pjID");
					Document preDoc;
					try {
						preDoc = Jsoup.connect("http://www.gzcc.gov.cn/data/laho/sellForm.aspx?pjID=" + pjID + "&presell="
								+ model.getPreSalePermit() + "&chnlname=" + name).timeout(50000).get();
					} catch (Exception e) {
						continue;
					}
					Elements elems = preDoc.select("#buildingID");
					for(Element input : elems){
						ReptileYgjyHouseFloorInfoSearch reptileYgjyHouseFloorInfoSearch = new ReptileYgjyHouseFloorInfoSearch();
						reptileYgjyHouseFloorInfoSearch.setEqualHouseId(model.getId());
						reptileYgjyHouseFloorInfoSearch.setEqualBuildingId(input.attr("value"));
						ReptileYgjyHouseFloorInfoModel floor = reptileServiceManage.reptileYgjyHouseFloorInfoService.first(reptileYgjyHouseFloorInfoSearch);
						ReptileYgjyHouseFloorInfoModel floorModel = new ReptileYgjyHouseFloorInfoModel();
						if(null == floor) {
							floorModel.setHouseId(model.getId());
							floorModel.setGzBuildingId(input.attr("value"));
							floorModel.setFloorName(input.parent().text());
							floorModel.setStatus(Contants.WING);
							reptileServiceManage.reptileYgjyHouseFloorInfoService.add(floorModel, SessionUser.getSystemUser());
						}else {
							floor.setFloorName(input.parent().text());
							reptileServiceManage.reptileYgjyHouseFloorInfoService.edit(floor, SessionUser.getSystemUser());
						}
					}
					Document preSellDoc;
					try {
						preSellDoc = Jsoup.connect("http://www.gzcc.gov.cn/data/laho/preSell.aspx?pjID="+pjID+"&presell="+model.getPreSalePermit()).timeout(50000).get();
					} catch (Exception e) {
						continue;
					}
					Element preSellElemsFirst = preSellDoc.select(".tab_style03").get(0);
					Element preSellElems = preSellDoc.select(".tab_style03").get(1);
					ReptileYgjyHousePreSaleCertificateModel reptileYgjyHousePreSaleCertificateModel = new ReptileYgjyHousePreSaleCertificateModel();
					//不用断言的原因是即使不存在也还要执行后面的代码
					if(null != preSellElemsFirst) {
						Elements setNumEles = preSellElemsFirst.select("tr");
						if(setNumEles.size() < 5) {
							continue;
						}
						if(null != setNumEles) {
							if(null != setNumEles.get(3)) {
								Elements secend = setNumEles.get(3).select("td");
								if(secend.size() < 6) {
									reptileYgjyHousePreSaleCertificateModel.setPreWord(secend.get(1).text() == null ? "" : secend.get(1).text());
									reptileYgjyHousePreSaleCertificateModel.setPreSaleNumber(secend.get(3).text() == null ? "" : secend.get(3).text());
									reptileYgjyHousePreSaleCertificateModel.setHouseBuiltNumber(secend.get(5).text() == null ? "" : secend.get(5).text());
								}
							}
							if(null != setNumEles.get(4)) {
								Elements third = setNumEles.get(4).select("td");
								if(third.size() < 6) {
									reptileYgjyHousePreSaleCertificateModel.setLayerBuiltNumber(third.get(1).text() == null ? "" : third.get(1).text());
									reptileYgjyHousePreSaleCertificateModel.setTotalAreaReport(third.get(3).text() == null ? "" : third.get(3).text());
									reptileYgjyHousePreSaleCertificateModel.setAboveGroundArea(third.get(5).text() == null ? "" : third.get(5).text());
								}
							}
							if(null != setNumEles.get(5)) {
								Elements four = setNumEles.get(5).select("td");
								if(four.size() < 6) {
									reptileYgjyHousePreSaleCertificateModel.setUndergroundArea(four.get(1).text() == null ? "" : four.get(1).text());
									reptileYgjyHousePreSaleCertificateModel.setPreSaleGrossFloorArea(four.get(3).text() == null ? "" : four.get(3).text());
									reptileYgjyHousePreSaleCertificateModel.setPreSaleHouseIsMortgaged(four.get(5).text() == null ? "" : four.get(5).text());
								}
							}
							if(null != setNumEles.get(6)) {
								Elements five = setNumEles.get(6).select("td");
								if(five.size() < 6) {
									reptileYgjyHousePreSaleCertificateModel.setPreSaleBuildingSupportingArea(five.get(1).text() == null ? "" : five.get(1).text());
								}
							}
						}
					}
					if(null != preSellElems) {
						Elements setNumEles = preSellElems.select("tr");
						if(null != setNumEles) {
							Element setNumModelEles = setNumEles.select("tr").get(2);
							Element setNumAreaEles = setNumEles.select("tr").get(3);
							if(null != setNumModelEles) {
								Elements setNumModel = setNumModelEles.select("td");
								reptileYgjyHousePreSaleCertificateModel.setHouseId(model.getId()); 
								reptileYgjyHousePreSaleCertificateModel.setHouseNumberSet(setNumModel.get(1).text() == null ? "":setNumModel.get(1).text());
								reptileYgjyHousePreSaleCertificateModel.setBusinessNumberSet(setNumModel.get(2).text() == null ? "":setNumModel.get(2).text());
								reptileYgjyHousePreSaleCertificateModel.setOfficeNumberSet(setNumModel.get(3).text() == null ? "":setNumModel.get(3).text());
								reptileYgjyHousePreSaleCertificateModel.setParkingNumberSet(setNumModel.get(4).text() == null ? "":setNumModel.get(4).text());
								reptileYgjyHousePreSaleCertificateModel.setOtherNumberSet(setNumModel.get(5).text() == null ? "":setNumModel.get(5).text());
							}
							if(null != setNumAreaEles) {
								Elements setNumArea = setNumModelEles.select("td");
								reptileYgjyHousePreSaleCertificateModel.setHouseArea(setNumArea.get(1).text() == null ? "":setNumArea.get(1).text());
								reptileYgjyHousePreSaleCertificateModel.setBusinessArea(setNumArea.get(2).text() == null ? "":setNumArea.get(2).text());
								reptileYgjyHousePreSaleCertificateModel.setOfficeArea(setNumArea.get(3).text() == null ? "":setNumArea.get(3).text());
								reptileYgjyHousePreSaleCertificateModel.setParkingArea(setNumArea.get(4).text() == null ? "":setNumArea.get(4).text());
								reptileYgjyHousePreSaleCertificateModel.setOtherArea(setNumArea.get(5).text() == null ? "":setNumArea.get(5).text());
							}
							ReptileYgjyHousePreSaleCertificateSearch reptileYgjyHousePreSaleCertificateSearch = new ReptileYgjyHousePreSaleCertificateSearch();
							reptileYgjyHousePreSaleCertificateSearch.setEqualHouseId(model.getId());
							ReptileYgjyHousePreSaleCertificateModel newReptileYgjyHousePreSaleCertificateModel = reptileServiceManage.reptileYgjyHousePreSaleCertificateService.first(reptileYgjyHousePreSaleCertificateSearch);
							if(null == newReptileYgjyHousePreSaleCertificateModel) {
								reptileServiceManage.reptileYgjyHousePreSaleCertificateService.add(reptileYgjyHousePreSaleCertificateModel, SessionUser.getSystemUser());
							}else {
								ReflectUtils.copySameFieldToTargetFilterNull(reptileYgjyHousePreSaleCertificateModel,newReptileYgjyHousePreSaleCertificateModel);
								reptileServiceManage.reptileYgjyHousePreSaleCertificateService.edit(newReptileYgjyHousePreSaleCertificateModel, SessionUser.getSystemUser());
							}
						}
					}
					
					Thread.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
			}
		} catch (ProjectException e1) {
			e1.printStackTrace();
		}
	}
}
