package com.jrzh.thread.gz;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.sh.ShLandThread;

/**
 * 获取广州宏观数据
 * @author admin
 *
 */
public class GzMacroscopicThread extends Thread {
	public static Log log = LogFactory.getLog(ShLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public GzMacroscopicThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}
	@Override
	public void run() {
		try {
			reptileServiceManage.reptileEstateDatumService.saveGzMacroscopicData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.info("###获取广州 宏观数据异常####");
		}
	}
}
