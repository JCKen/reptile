package com.jrzh.thread.qy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.gz.GzLandThread;

/**
 * 清远土地线程
 *
 */
public class QyLandThread extends Thread {
	public static Log log = LogFactory.getLog(GzLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public QyLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveQyLandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.info("###获取清远土地数据异常####");
		}
	}

}
