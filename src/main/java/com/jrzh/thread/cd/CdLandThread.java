package com.jrzh.thread.cd;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

/**
 * 成都土地线程
 */
public class CdLandThread extends Thread {
	
	public static Log log = LogFactory.getLog(CdLandThread.class);
	
	private ReptileServiceManage reptileServiceManage;
	
	public CdLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}
	
	@Override
	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveCdLandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取成都土地数据异常####");
		}
	}
}
