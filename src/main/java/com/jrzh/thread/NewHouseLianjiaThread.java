package com.jrzh.thread;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;



/**
 * 链家新房线程
 * */
public class NewHouseLianjiaThread extends Thread {

	static Log log = LogFactory.getLog(NewHouseLianjiaThread.class);

	private ReptileServiceManage reptileServiceManage;

	public NewHouseLianjiaThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			reptileServiceManage.reptileNewHouseService.findNewHouseFromLianjiaInGZ(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("获取链家数据异常");
		}
	}
}
