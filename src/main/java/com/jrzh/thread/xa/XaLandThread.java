package com.jrzh.thread.xa;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

/**
 * 西安土地线程
 * @author admin
 *
 */
public class XaLandThread extends Thread {
	public static Log log = LogFactory.getLog(XaLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public XaLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveXALandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.info("###获取西安土地数据异常####");
		}
	}
}
