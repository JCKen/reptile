package com.jrzh.thread.tj;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.gz.GzLandThread;

/**
 * 天津土地线程
 *
 */
public class TjLandThread extends Thread {

	public static Log log = LogFactory.getLog(GzLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public TjLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	public void run() {
		//synchronized (this) {
			try {
				reptileServiceManage.reptileGtjAreaInfoService.saveTJLandData(SessionUser.getSystemUser());
				;
			} catch (ProjectException e) {
				e.printStackTrace();
				log.info("###获取天津土地数据异常####");
			}
		//}
	}
}
