package com.jrzh.thread;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.bean.jwd.JwdInfo;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileEsfHousesXiaoquModel;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public class EsfLatitudeLongitudeThread extends Thread {

	static Log log = LogFactory.getLog(EsfLatitudeLongitudeThread.class);

	private ReptileServiceManage reptileServiceManage;

	public EsfLatitudeLongitudeThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		// 更新坐标方法
		Object object = new Object();
		synchronized (object) {
			try {
				List<ReptileEsfHousesXiaoquModel> reptileEsfHouseModelList = reptileServiceManage.reptileEsfHousesXiaoquService
						.findAll();
				for (ReptileEsfHousesXiaoquModel model : reptileEsfHouseModelList) {
					if (model.getLatitude() != null && model.getLongitude() != null) {
						continue;
					}
					try {
						String lat = null;
						String lng = null;
						List<JwdInfo> list = LatitudeLongitudeThread.getJwd(model.getHousesCity(),
								model.getHouseName());
						if (list == null) {
							continue;
						}
						String[] strs = LatitudeLongitudeThread.getJwd(list, model.getHousesPart(),
								model.getHouseName());
						lat = strs[0];
						lng = strs[1];
						log.info(lat + "----" + lng);
						if (lat != null && lng != null) {
							model.setLatitude(new BigDecimal(lat));
							model.setLongitude(new BigDecimal(lng));
							reptileServiceManage.reptileEsfHousesXiaoquService.edit(model, SessionUser.getSystemUser());
						}
						Thread.sleep(100);
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
				}
				log.info("二手房经纬度获取结束");
			} catch (Exception e) {
				log.error("【获取二手房经纬度异常】");
				e.printStackTrace();
			}
		}
	}

}
