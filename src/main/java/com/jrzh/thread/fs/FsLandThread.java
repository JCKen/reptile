package com.jrzh.thread.fs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.cd.CdLandThread;

/**
 * 佛山土地线程
 *
 */
public class FsLandThread extends Thread {
public static Log log = LogFactory.getLog(CdLandThread.class);
	
	private ReptileServiceManage reptileServiceManage;
	
	public FsLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}
	
	@Override
	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveFsLandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取佛山土地数据异常####");
		}
	}
}
