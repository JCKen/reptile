package com.jrzh.thread.sz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileYgjyHousePreSaleCertificateConvert;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseInfoModel;
import com.jrzh.mvc.model.reptile.ReptileYgjyHousePreSaleCertificateModel;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHousePreSaleCertificateSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public class SZFangGuanJuHouseInfoThread extends Thread {
	
	private final static String PRE_SALE_URL = "http://ris.szpl.gov.cn/bol/certdetail.aspx";
	
	Log log = LogFactory.getLog(getClass());
	
	private String city;
	private ReptileServiceManage reptileServiceManage;

	public SZFangGuanJuHouseInfoThread(String city, ReptileServiceManage reptileServiceManage) {
		this.city = city;
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			ReptileYgjyHouseInfoSearch search = new ReptileYgjyHouseInfoSearch();
			search.setEqualCity(city);
			List<ReptileYgjyHouseInfoModel> models = reptileServiceManage.reptileYgjyHouseInfoService.findListBySearch(search);
			for (ReptileYgjyHouseInfoModel model : models) {
				try {
					String projectId = model.getProjectId();
					String houseId = model.getId();
					String preSaleUrl = PRE_SALE_URL + "?id=" + projectId;
					Document preSaleDoc = Jsoup.connect(preSaleUrl).timeout(50000).get();
	        		if(null == preSaleDoc) {
	        			continue;
	        		}
					//预售证信息
	        		Elements preEle = preSaleDoc.select(".a1");
	        		if(null == preEle) {
	        			continue;
	        		}
	        		Map<String,String> map = new HashMap<>();
	        		for(Element e : preEle) {
	        			Elements preSaleTd = e.select("td");
	            		if(null == preSaleTd) {
	            			continue;
	            		}
	            		if(preSaleTd.size() == 2) {
	            			map.put(preSaleTd.get(0).text(), preSaleTd.get(1).text());
	            		}else if(preSaleTd.size() == 4) {
	            			map.put(preSaleTd.get(0).text(), preSaleTd.get(1).text());
	            			map.put(preSaleTd.get(2).text(), preSaleTd.get(3).text());
	            		}else if(preSaleTd.size() == 6) {
	            			map.put(preSaleTd.get(1).text() + "面积", preSaleTd.get(3).text());
	            			map.put(preSaleTd.get(1).text() + "套数", preSaleTd.get(5).text());
	            		}
	        		}
	        		ReptileYgjyHousePreSaleCertificateModel reptileYgjyHousePreSaleCertificateModel = ReptileYgjyHousePreSaleCertificateConvert.houseWorldMapToHouseModel(map);
	        		reptileYgjyHousePreSaleCertificateModel.setHouseId(houseId);
	        		ReptileYgjyHousePreSaleCertificateSearch preSearch = new ReptileYgjyHousePreSaleCertificateSearch();
	        		preSearch.setEqualHouseId(houseId);
	        		ReptileYgjyHousePreSaleCertificateModel newReptileYgjyHousePreSaleCertificateModel = reptileServiceManage.reptileYgjyHousePreSaleCertificateService.first(preSearch);
	        		if(null == newReptileYgjyHousePreSaleCertificateModel) {
	        			log.info("开始====================开始" );
	        			reptileServiceManage.reptileYgjyHousePreSaleCertificateService.add(reptileYgjyHousePreSaleCertificateModel, SessionUser.getSystemUser());
	        		}else {
	        			ReflectUtils.copySameFieldToTargetFilterNull(reptileYgjyHousePreSaleCertificateModel,newReptileYgjyHousePreSaleCertificateModel);
	    				reptileServiceManage.reptileYgjyHousePreSaleCertificateService.edit(newReptileYgjyHousePreSaleCertificateModel, SessionUser.getSystemUser());
	        		}
					log.info("结束====================结束");
					Thread.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
			}
		} catch (ProjectException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
}
