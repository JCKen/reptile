package com.jrzh.thread.sz;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

/***
 *  深圳土地线程
 **/
public class SzLandThread extends Thread {

	public static Log log = LogFactory.getLog(SzLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public SzLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveSZLandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.info("####获取深圳土地数据异常####");
		}
	}

}
