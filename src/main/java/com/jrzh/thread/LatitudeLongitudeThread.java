package com.jrzh.thread;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.alibaba.fastjson.JSONObject;
import com.jrzh.bean.jwd.JwdBean;
import com.jrzh.bean.jwd.JwdInfo;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileNewHouseModel;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.tools.MyPayUtils;

public class LatitudeLongitudeThread extends Thread {

	static Log log = LogFactory.getLog(LatitudeLongitudeThread.class);

	private ReptileServiceManage reptileServiceManage;

	public LatitudeLongitudeThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		// 更新坐标方法
		Object object = new Object();
		synchronized (object) {
			List<ReptileNewHouseModel> reptileNewHouseModelList;
			try {
				reptileNewHouseModelList = reptileServiceManage.reptileNewHouseService.findAll();
				for (ReptileNewHouseModel reptileNewHouseModel : reptileNewHouseModelList) {
					if (reptileNewHouseModel.getLatitude() != null && reptileNewHouseModel.getLongitude() != null) {
						continue;
					}
					try {// 具体获取坐标方法
						String lat = null;
						String lng = null;
						List<JwdInfo> list = getJwd(reptileNewHouseModel.getHouseCity(),
								reptileNewHouseModel.getHouseName());
						if (list == null) {
							if(reptileNewHouseModel.getHouseOtherName()==null) continue;
							list = getJwd(reptileNewHouseModel.getHouseCity(),reptileNewHouseModel.getHouseOtherName());
							if(list==null) continue;
						}
						String[] strs = getJwd(list, reptileNewHouseModel.getHousePart(),
								reptileNewHouseModel.getHouseName());
						lat = strs[0];
						lng = strs[1];
						log.info(lat + "----" + lng);
						if (lat != null && lng != null) {
							reptileNewHouseModel.setLatitude(new BigDecimal(lat));
							reptileNewHouseModel.setLongitude(new BigDecimal(lng));
							reptileServiceManage.reptileNewHouseService.edit(reptileNewHouseModel,
									SessionUser.getSystemUser());
						}
						Thread.sleep(300);
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
				}
				log.info("一手房经纬度结束");
			} catch (ProjectException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}

	public static List<JwdInfo> getJwd(String city, String position) {
		String[] appKeyArr = { "Zute9dUseygWr8g10lqn8O4nYfeWhb5G", "3B5pgekUc6OYvUGoQcLHnPjumemNlXKP",
				"sXgxtgqipGdLR91g2WPEHec5db3BMrXe" };
		Integer index = 0;
		String requestContent = null;
		try {
			// 具体获取坐标方法
			position = URLEncoder.encode(position, "utf-8");
			String appkey = appKeyArr[index];
			String url = "http://api.map.baidu.com/place/v2/suggestion?query=" + position + "&region=" + city
					+ "&city_limit=true&output=json&ak=" + appkey;
			requestContent = MyPayUtils.sendHttpByGet(url, 2000000, 20000000);
			JwdBean jwdBean = JSONObject.parseObject(requestContent, JwdBean.class); // 获取返回信息
			if (StringUtils.equals(jwdBean.getMessage(), "302")) {
				appkey = appKeyArr[index++];
				url = "http://api.map.baidu.com/place/v2/suggestion?query=" + position + "&region=" + city
						+ "&city_limit=true&output=json&ak=" + appkey;
				requestContent = MyPayUtils.sendHttpByGet(url, 2000000, 20000000);
				jwdBean = JSONObject.parseObject(requestContent, JwdBean.class);
			}
			if (StringUtils.equals(jwdBean.getMessage(), "ok")) {
				List<JwdInfo> info = jwdBean.getResult();
				return info;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String[] getJwd(List<JwdInfo> list, String part, String houserName) {
		String lat = null;
		String lng = null;
		for (JwdInfo info : list) {
			if (info.getDistrict().contains(part)) {
				if (StringUtils.equals(info.getName(), houserName)) {
					lat = info.getLocation().getLat();
					lng = info.getLocation().getLng();
					return new String[] { lat, lng };
				}
			}
		}
		for (JwdInfo info : list) {
			if (info.getDistrict().contains(part)) {
				if (info.getName().contains(houserName)) {
					lat = info.getLocation().getLat();
					lng = info.getLocation().getLng();
					return new String[] { lat, lng };
				}
			}
		}
		for (JwdInfo info : list) {
			lat = info.getLocation().getLat();
			lng = info.getLocation().getLng();
			return new String[] { lat, lng };
		}
		return null;
	}
}
