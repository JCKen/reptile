package com.jrzh.thread.ks;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

/**
 * 昆山土地线程
 * @author admin
 *
 */
public class KsLandThread extends Thread {
	
	public static Log log = LogFactory.getLog(KsLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public KsLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}
	
	@Override
	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveKsLandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取昆山土地数据异常####");
		}
	}
}
