package com.jrzh.thread.dg;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.cd.CdLandThread;

/**
 * 东莞土地线程
 * @author admin
 *
 */
public class DgLandThread extends Thread {
	
	public static Log log = LogFactory.getLog(CdLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public DgLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveDgLandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取东莞土地数据异常####");
		}
	}
}
