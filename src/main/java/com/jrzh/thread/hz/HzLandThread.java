package com.jrzh.thread.hz;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

/**
 * 惠州土地线程
 * 
 * @author admin
 *
 */
public class HzLandThread extends Thread {
	public static Log log = LogFactory.getLog(HzLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public HzLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveHzLandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.info("###获取惠州土地数据异常####");
		}
	}
}
