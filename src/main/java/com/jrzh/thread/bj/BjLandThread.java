package com.jrzh.thread.bj;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

/***
 *  北京土地线程
 **/
public class BjLandThread extends Thread {

	public static Log log = LogFactory.getLog(BjLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public BjLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveBjLandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取北京土地数据异常####");
		}
	}

}
