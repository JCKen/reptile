package com.jrzh.thread.bj;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.contants.Contants;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseFloorInfoModel;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseInfoModel;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseFloorInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseInfoSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public class BJFangGuanJuHouseInfoThread extends Thread {

	Log log = LogFactory.getLog(getClass());

	private String city;
	private ReptileServiceManage reptileServiceManage;

	public BJFangGuanJuHouseInfoThread(String city, ReptileServiceManage reptileServiceManage) {
		this.city = city;
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			ReptileYgjyHouseInfoSearch search = new ReptileYgjyHouseInfoSearch();
			search.setEqualCity(city);
			List<ReptileYgjyHouseInfoModel> models = reptileServiceManage.reptileYgjyHouseInfoService
					.findListBySearch(search);
			for (ReptileYgjyHouseInfoModel model : models) {
				try {
					Document document = Jsoup.connect(model.getUrl()).timeout(50000).get();
					if (null == document)
						continue;
					//楼盘表
					Element spanEle = document.selectFirst("#Span1");
					if (null == spanEle)
						continue;
					Elements houseFloorUrlTr = spanEle.select("tr");
					if (null == houseFloorUrlTr)
						continue;
					for (Integer i = 1; i < houseFloorUrlTr.size(); i++) {
						Elements houseFloorUrlTd = houseFloorUrlTr.get(i).select("td");
						if (null == houseFloorUrlTd)
							continue;
						//楼层信息
						ReptileYgjyHouseFloorInfoModel reptileYgjyHouseFloorInfoModel = new ReptileYgjyHouseFloorInfoModel();
						String houseName = houseFloorUrlTd.get(0).text();
						Element houseFloorUrlEle = houseFloorUrlTd.last().selectFirst("a");
						if (null == houseFloorUrlEle) {
							continue;
						}
						// 楼层信息入库
						String houseFloorUrl = houseFloorUrlEle.attr("href");
						reptileYgjyHouseFloorInfoModel.setHouseId(model.getId());
						reptileYgjyHouseFloorInfoModel.setFloorName(houseName);
						reptileYgjyHouseFloorInfoModel.setBjStartId(houseFloorUrl);
						log.info("========" + houseFloorUrl);
						reptileYgjyHouseFloorInfoModel.setStatus(Contants.WING);
						ReptileYgjyHouseFloorInfoSearch reptileYgjyHouseFloorInfoSearch = new ReptileYgjyHouseFloorInfoSearch();
						reptileYgjyHouseFloorInfoSearch.setEqualBjStartId(houseFloorUrl);
						reptileYgjyHouseFloorInfoSearch.setEqualHouseId(model.getId());
						ReptileYgjyHouseFloorInfoModel newModel = reptileServiceManage.reptileYgjyHouseFloorInfoService
								.first(reptileYgjyHouseFloorInfoSearch);
						if (null == newModel) {
							reptileServiceManage.reptileYgjyHouseFloorInfoService.add(reptileYgjyHouseFloorInfoModel,
									SessionUser.getSystemUser());
						} else {
							ReflectUtils.copySameFieldToTarget(reptileYgjyHouseFloorInfoModel, newModel);
							reptileServiceManage.reptileYgjyHouseFloorInfoService.edit(newModel,
									SessionUser.getSystemUser());
						}
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
