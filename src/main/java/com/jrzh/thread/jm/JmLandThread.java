package com.jrzh.thread.jm;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public class JmLandThread extends Thread {
	public static Log log = LogFactory.getLog(JmLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public JmLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}
	
	@Override
	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveJmLandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取江门土地数据异常####");
		}
	}
}
