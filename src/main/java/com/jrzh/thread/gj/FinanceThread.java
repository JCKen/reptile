package com.jrzh.thread.gj;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.cd.CdLandThread;

/**
 * 国家数据 财政 线程
 * @author admin
 *
 */
public class FinanceThread extends Thread {
	
	private ReptileServiceManage reptileServiceManage;
	
	public static Log log = LogFactory.getLog(CdLandThread.class);
	
	public FinanceThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}
	
	@Override
	public void run() {
		try {
			String menu = "财政";
			String classify = "国家财政收支总额及增长速度";
			String url = "http://data.stats.gov.cn/easyquery.htm?m=QueryData&dbcode=hgnd&rowcode=zb&colcode=sj&wds=%5B%5D&dfwds=%5B%7B%22wdcode%22%3A%22zb%22%2C%22valuecode%22%3A%22A0801%22%7D%5D&k1=1557395121888";
			reptileServiceManage.reptileEstateDatumService.saveEstateInvestData(classify, menu ,url,SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取宏观数据  财政  数据异常####");
		}
	}
}
