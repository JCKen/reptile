package com.jrzh.thread.gj;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.cd.CdLandThread;


/**
 *   房地产土地开发与销售情况 线程
 *
 */
public class EstateLandThread extends Thread {
	
	private ReptileServiceManage reptileServiceManage;
	public static Log log = LogFactory.getLog(CdLandThread.class);

	public EstateLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			String classify = "房地产土地开发与销售情况";
			String url = "http://data.stats.gov.cn/easyquery.htm?m=QueryData&dbcode=hgyd&rowcode=zb&colcode=sj&wds=%5B%5D&dfwds=%5B%7B%22wdcode%22%3A%22zb%22%2C%22valuecode%22%3A%22A0603%22%7D%5D&k1=1553934344923";
			String menu = "房地产";
			reptileServiceManage.reptileEstateDatumService.saveEstateInvestData(classify, menu ,url,SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取宏观数据房地产板块房地产土地开发与销售情况数据异常####");
		}
	}
}
