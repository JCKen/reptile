package com.jrzh.thread.gj;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.cd.CdLandThread;

/**
 *    固定投资额和房地产
 */  
public class FixedInvestment extends Thread {
	
	private ReptileServiceManage reptileServiceManage;
	public static Log log = LogFactory.getLog(CdLandThread.class);
	
	public FixedInvestment(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}
	
	@Override
	public void run() {
		try {
			String menu = "固定投资额和房地产";
			String classify = "房地产开发企业投资总规模及完成情况";
			String url = "http://data.stats.gov.cn/easyquery.htm?m=QueryData&dbcode=hgnd&rowcode=zb&colcode=sj&wds=%5B%5D&dfwds=%5B%7B%22wdcode%22%3A%22zb%22%2C%22valuecode%22%3A%22A051E%22%7D%5D&k1=1557458452676";
			reptileServiceManage.reptileEstateDatumService.saveEstateInvestData(classify, menu ,url,SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取宏观数据  固定投资额和房地产  数据异常####");
		}
	}
}
