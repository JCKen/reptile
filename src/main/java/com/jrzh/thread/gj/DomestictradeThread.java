package com.jrzh.thread.gj;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.cd.CdLandThread;

/**
 *  国内贸易 社会消费品零售总额  线程
 * @author admin
 *
 */
public class DomestictradeThread extends Thread {
	
	private ReptileServiceManage reptileServiceManage;
	
	public static Log log = LogFactory.getLog(CdLandThread.class);
	
	public DomestictradeThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}
	
	@Override
	public void run() {
		try {
			String menu = "国内贸易";
			String classify = "社会消费品零售总额";
			String url = "http://data.stats.gov.cn/easyquery.htm?m=QueryData&dbcode=hgnd&rowcode=zb&colcode=sj&wds=%5B%5D&dfwds=%5B%7B%22wdcode%22%3A%22zb%22%2C%22valuecode%22%3A%22A0H%22%7D%5D&k1=1557396199139";
			reptileServiceManage.reptileEstateDatumService.saveEstateInvestData(classify, menu ,url,SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取宏观数据 社会消费品零售总额  数据异常####");
		}
	}
}
