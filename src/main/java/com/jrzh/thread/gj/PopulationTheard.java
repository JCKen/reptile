package com.jrzh.thread.gj;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.cd.CdLandThread;

public class PopulationTheard extends Thread {
	
	public static Log log = LogFactory.getLog(CdLandThread.class);
	
	private ReptileServiceManage reptileServiceManage;
	
	public PopulationTheard(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}
	
	@Override
	public void run() {
		try {
			String menu = "总人口";
			String classify = "总人口";
			String url = "http://data.stats.gov.cn/easyquery.htm?m=QueryData&dbcode=hgnd&rowcode=zb&colcode=sj&wds=%5B%5D&dfwds=%5B%7B%22wdcode%22%3A%22zb%22%2C%22valuecode%22%3A%22A0301%22%7D%5D&k1=1557391084360";
			reptileServiceManage.reptileEstateDatumService.saveEstateInvestData(classify, menu ,url,SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取宏观数据人口总数数据异常####");
		}
	}
}
