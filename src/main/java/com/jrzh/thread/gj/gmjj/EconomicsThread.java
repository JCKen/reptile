package com.jrzh.thread.gj.gmjj;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.contants.UrlContants;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.cd.CdLandThread;

/**
 *  宏观数据 国民经济核算线程
 */
public class EconomicsThread extends Thread {
	
	public static Log log = LogFactory.getLog(CdLandThread.class);
	
	private ReptileServiceManage reptileServiceManage;
	
	public EconomicsThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}
	
	@Override
	public void run() {
		try {
			String menu = "国民经济";
			Map<String, String> map = UrlContants.getUrl();
			for (String classify : map.keySet()) {
				reptileServiceManage.reptileEstateDatumService.saveEstateInvestData(classify, menu ,map.get(classify),SessionUser.getSystemUser());
			}
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取宏观数据国民经济数据异常####");
		}
	}
}
