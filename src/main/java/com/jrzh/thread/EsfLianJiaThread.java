package com.jrzh.thread;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public class EsfLianJiaThread extends Thread{

	static Log log = LogFactory.getLog(NewHouseLianjiaThread.class);
	
	private ReptileServiceManage reptileServiceManage;

	public EsfLianJiaThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			reptileServiceManage.reptileEsfHouseService.findEsfHouseFromLianjiaInGZ(SessionUser.getSystemUser());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取链家数据异常");
		}
	}
}
