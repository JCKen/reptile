package com.jrzh.thread.sh;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jrzh.bean.SHFloorProject.JsonRootBean;
import com.jrzh.bean.SHFloorProject.PriceInformationList;
import com.jrzh.bean.SHFloorProject.StartUnitList;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.contants.Contants;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileHouseProjectNameModel;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseFloorInfoModel;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseInfoModel;
import com.jrzh.mvc.model.reptile.ReptileYgjyHousePreSaleCertificateModel;
import com.jrzh.mvc.search.reptile.ReptileHouseProjectNameSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseFloorInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHousePreSaleCertificateSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.tools.MyPayUtils;

public class SHFangGuanJuHouseInfoThread extends Thread {
	
	private final static String STATT_UNIT_URL = "http://www.fangdi.com.cn/service/freshHouse/queryStartUnit.action";
	Log log = LogFactory.getLog(getClass());
	
	private String city;
	private ReptileServiceManage reptileServiceManage;

	public SHFangGuanJuHouseInfoThread(String city, ReptileServiceManage reptileServiceManage) {
		this.city = city;
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			ReptileHouseProjectNameSearch proSearch = new ReptileHouseProjectNameSearch();
			proSearch.setEqualCityName(city);
			List<ReptileHouseProjectNameModel> models = reptileServiceManage.reptileHouseProjectNameService.findListBySearch(proSearch);
			for (ReptileHouseProjectNameModel model : models) {
				try {
					log.info("开始====================开始" );
					String projectId = model.getProjectId();
					String startRequestContent = MyPayUtils.sendHttp(projectId,null,null, STATT_UNIT_URL, 20000, 50000);
					Object status = JSONObject.parseObject(startRequestContent).get("result");
					if(null != status) continue;
					JsonRootBean jsonRootBean = JSON.parseObject(startRequestContent, JsonRootBean.class);
					List<StartUnitList> startUnitList = jsonRootBean.getStartUnitList();
					List<List<PriceInformationList>> priceInformationList = jsonRootBean.getPriceInformationList();
					if(CollectionUtils.isEmpty(startUnitList)) {
	    				continue;
	    			}
					for(Integer i = 0 ;i < startUnitList.size();i++) {
						ReptileYgjyHouseInfoSearch reptileYgjyHouseInfoSearch = new ReptileYgjyHouseInfoSearch();
						reptileYgjyHouseInfoSearch.setEqualAdministrativeArea(model.getAreaName());
						reptileYgjyHouseInfoSearch.setEqualProjectId(model.getProjectId());
						reptileYgjyHouseInfoSearch.setEqualCity("上海");
						reptileYgjyHouseInfoSearch.setEqualPreSalePermit(startUnitList.get(i).getPresell_desc());
						ReptileYgjyHouseInfoModel reptileYgjyHouseInfoModel = reptileServiceManage.reptileYgjyHouseInfoService.first(reptileYgjyHouseInfoSearch);
						ReptileYgjyHouseInfoModel newReptileYgjyHouseInfoModel = new ReptileYgjyHouseInfoModel();
						//新增
						newReptileYgjyHouseInfoModel.setDeveloper(model.getDeveloper());
						newReptileYgjyHouseInfoModel.setCity("上海");
						newReptileYgjyHouseInfoModel.setAdministrativeArea(model.getAreaName());
						newReptileYgjyHouseInfoModel.setProductName(model.getProjectName());
						newReptileYgjyHouseInfoModel.setProjectId(model.getProjectId());
						newReptileYgjyHouseInfoModel.setPreSalePermit(startUnitList.get(i).getPresell_desc());
						newReptileYgjyHouseInfoModel.setLandArea(startUnitList.get(i).getArea());
						newReptileYgjyHouseInfoModel.setHouseUnsold(startUnitList.get(i).getZ_num());
						String houseId = null;
						if(null == reptileYgjyHouseInfoModel) {
							reptileServiceManage.reptileYgjyHouseInfoService.add(newReptileYgjyHouseInfoModel, SessionUser.getSystemUser());
							houseId = newReptileYgjyHouseInfoModel.getId();
						}else {
							ReflectUtils.copySameFieldToTargetFilterNull(newReptileYgjyHouseInfoModel,reptileYgjyHouseInfoModel);
							reptileServiceManage.reptileYgjyHouseInfoService.edit(reptileYgjyHouseInfoModel, SessionUser.getSystemUser());
							houseId = reptileYgjyHouseInfoModel.getId();
						}
						log.info("============priceInformationList" + priceInformationList);
						log.info("============priceInformationList.size()" + priceInformationList.size());
						log.info("============priceInformationList.get(i)" + priceInformationList.get(i));
						if(CollectionUtils.isEmpty(priceInformationList) || CollectionUtils.isEmpty(priceInformationList.get(i))) {
		    				continue;
		    			}
						
						for(PriceInformationList priceInformation : priceInformationList.get(i)) {
	    					ReptileYgjyHouseFloorInfoModel newReptileYgjyHouseFloorInfoModel = new ReptileYgjyHouseFloorInfoModel();
	    					ReptileYgjyHousePreSaleCertificateModel newReptileYgjyHousePreSaleCertificateModel = new ReptileYgjyHousePreSaleCertificateModel();
	    					ReptileYgjyHouseFloorInfoSearch search = new ReptileYgjyHouseFloorInfoSearch();
	    					ReptileYgjyHousePreSaleCertificateSearch reptileYgjyHousePreSaleCertificateSearch = new ReptileYgjyHousePreSaleCertificateSearch();
	    					reptileYgjyHousePreSaleCertificateSearch.setEqualHouseId(houseId);
	    					//查询
	    					search.setEqualHouseId(houseId);
	    					search.setEqualShStartId(priceInformation.getStart_id());
	    					search.setEqualBuildingId(priceInformation.getBuilding_id());
	    					ReptileYgjyHouseFloorInfoModel reptileYgjyHouseFloorInfoModel = reptileServiceManage.reptileYgjyHouseFloorInfoService.first(search);
	    					ReptileYgjyHousePreSaleCertificateModel reptileYgjyHousePreSaleCertificateModel = reptileServiceManage.reptileYgjyHousePreSaleCertificateService.first(reptileYgjyHousePreSaleCertificateSearch);
	    					//上海房管局项目信息
	    					newReptileYgjyHouseFloorInfoModel.setBuildingId(priceInformation.getBuilding_id());
	    					newReptileYgjyHouseFloorInfoModel.setHouseId(houseId);
	    					newReptileYgjyHouseFloorInfoModel.setFloorName(priceInformation.getBuilding_name());
	    					newReptileYgjyHouseFloorInfoModel.setStatus(Contants.WING);
	    					newReptileYgjyHouseFloorInfoModel.setShStartId(priceInformation.getStart_id());
	    					if(null == reptileYgjyHouseFloorInfoModel) {
	    						reptileServiceManage.reptileYgjyHouseFloorInfoService.add(newReptileYgjyHouseFloorInfoModel, SessionUser.getSystemUser());
	    					}else {
	    						ReflectUtils.copySameFieldToTarget(reptileYgjyHouseFloorInfoModel,newReptileYgjyHouseFloorInfoModel);
	    						reptileServiceManage.reptileYgjyHouseFloorInfoService.edit(newReptileYgjyHouseFloorInfoModel, SessionUser.getSystemUser());
	    					}
	    					//上海售控表信息
	    					newReptileYgjyHousePreSaleCertificateModel.setHouseId(houseId);
	    					newReptileYgjyHousePreSaleCertificateModel.setTotalAreaReport(String.valueOf(model.getProjectTotalArea()));
	    					newReptileYgjyHousePreSaleCertificateModel.setHouseArea(String.valueOf(model.getProjectHouseArea()));
	    					newReptileYgjyHousePreSaleCertificateModel.setHouseNumberSet(String.valueOf(model.getProjectHouseNumber()));
	    					if(null == reptileYgjyHousePreSaleCertificateModel) {
	    						reptileServiceManage.reptileYgjyHousePreSaleCertificateService.add(newReptileYgjyHousePreSaleCertificateModel, SessionUser.getSystemUser());
	    					}else {
	    						ReflectUtils.copySameFieldToTargetFilterNull(reptileYgjyHousePreSaleCertificateModel,newReptileYgjyHousePreSaleCertificateModel);
	    						reptileServiceManage.reptileYgjyHousePreSaleCertificateService.edit(newReptileYgjyHousePreSaleCertificateModel, SessionUser.getSystemUser());
	    					}
	    				}
					}
					
					log.info("结束====================结束" );
					Thread.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
			}
		} catch (ProjectException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
