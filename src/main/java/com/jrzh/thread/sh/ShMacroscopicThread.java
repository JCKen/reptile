package com.jrzh.thread.sh;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public class ShMacroscopicThread extends  Thread{

	public static Log log = LogFactory.getLog(ShLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public ShMacroscopicThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}
	@Override
	public void run() {
		try {
			reptileServiceManage.reptileEstateDatumService.saveSHMacroscopicData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.info("###获取上海 宏观数据异常####");
		}
	}
}
