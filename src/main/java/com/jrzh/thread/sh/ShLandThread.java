package com.jrzh.thread.sh;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

/***
 *  上海土地线程
 **/
public class ShLandThread extends Thread {

	public static Log log = LogFactory.getLog(ShLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public ShLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveSHLandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.info("###获取上海土地数据异常####");
		}
	}

}
