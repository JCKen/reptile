package com.jrzh.thread;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

/**
 * 房天下线程
 * */
public class NewHouseFangtxThread extends Thread {

	static Log log = LogFactory.getLog(NewHouseFangtxThread.class);

	private ReptileServiceManage reptileServiceManage;

	public NewHouseFangtxThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			reptileServiceManage.reptileNewHouseService.findNewHouseFromHouseWorldInGZ(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("获取人事系统数据");
		}
	}
}
