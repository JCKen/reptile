package com.jrzh.thread.zh;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.thread.cd.CdLandThread;

/**
 * 珠海土地线程
 *
 */
public class ZhLandThread extends Thread {
	public static Log log = LogFactory.getLog(CdLandThread.class);

	private ReptileServiceManage reptileServiceManage;

	public ZhLandThread(ReptileServiceManage reptileServiceManage) {
		this.reptileServiceManage = reptileServiceManage;
	}

	@Override
	public void run() {
		try {
			reptileServiceManage.reptileGtjAreaInfoService.saveZhLandData(SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
			log.error("###获取珠海土地数据异常####");
		}
	}
}
