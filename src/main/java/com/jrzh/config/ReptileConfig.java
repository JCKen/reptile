package com.jrzh.config;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.support.ErrorPageFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.jrzh.filter.ReptileSSOFilter;
import com.jrzh.framework.convert.CustomDateConverter;
import com.jrzh.framework.interceptor.FrameworkInterceptor;

@Configuration
public class ReptileConfig extends WebMvcConfigurerAdapter{

	@Bean
	public ErrorPageFilter errorPageFilter() {
		return new ErrorPageFilter();
	}

	@Bean
	public FilterRegistrationBean disableSpringBootErrorFilter(ErrorPageFilter filter) {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		filterRegistrationBean.setFilter(filter);
		filterRegistrationBean.setEnabled(false);
		return filterRegistrationBean;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/files/**").addResourceLocations("file:///C:/projects/");
		registry.addResourceHandler("/static/projects/**").addResourceLocations("file:///C:/projects/");
	}

	@Bean
	public CustomDateConverter customDateConverter() {
		CustomDateConverter customDateConverter = new CustomDateConverter();
		return customDateConverter;
	}

	@Bean
	public FormattingConversionServiceFactoryBean formattingConversionServiceFactoryBean() {
		FormattingConversionServiceFactoryBean fcserviceFactoryBean = new FormattingConversionServiceFactoryBean();
		Set<Object> converterSets = new HashSet<Object>();
		converterSets.add(customDateConverter());
		fcserviceFactoryBean.setConverters(converterSets);
		return fcserviceFactoryBean;
	}

	@Bean
	public RequestMappingHandlerMapping requestMappingHandlerMapping() {
		return new RequestMappingHandlerMapping();
	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize("200MB");
		factory.setMaxRequestSize("200MB");
		return factory.createMultipartConfig();
	}

	@Bean
	public FrameworkInterceptor frameworkInterceptor() {
		return new FrameworkInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(frameworkInterceptor());
	}

	@Bean
	 public FilterRegistrationBean ssoFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new ReptileSSOFilter());
        registration.addUrlPatterns("/sso/*");
        registration.setName("ssoFilter");
        registration.setOrder(3);
        return registration;
    }

}
