package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_estate_zb")
public class ReptileEstateZbModel extends GeneralModel {
    
    /**
     * 指标名称
     */
    @Column(name = "_zb")
    private String zb;
    /**
     * 单位
     */
    @Column(name = "_unit")
    private String unit;
    /**
     * 指标编号
     */
    @Column(name = "_code")
    private String code;
    
    /**
     * 指标f分类
     */
    @Column(name = "_classify")
    private String classify;
    
    /**
     * 指标城市
     */
    @Column(name = "_city")
    private String city;

    /**
     * 所属菜单
     */
    @Column(name = "_menu")
    private String menu;
    
    public void setZb(String zb) {
        this.zb = zb;
    }
    
    public String getZb() {
        return this.zb;
    }
    public void setUnit(String unit) {
        this.unit = unit;
    }
    
    public String getUnit() {
        return this.unit;
    }
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCode() {
        return this.code;
    }

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}
	

}