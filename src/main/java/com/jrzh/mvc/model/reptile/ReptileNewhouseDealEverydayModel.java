package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_newhouse_deal_everydays")
public class ReptileNewhouseDealEverydayModel extends GeneralModel {
    
    /**
     * 城市
     */
    @Column(name = "_city")
    private String city;
    /**
     * 区域
     */
    @Column(name = "_part")
    private String part;
    /**
     * 成交日期
     */
    @Column(name = "_deal_date")
    private String dealDate;
    /**
     * 住宅预售套数
     */
    @Column(name = "_house_count")
    private String houseCount;
    /**
     * 住宅预售面积（平米）
     */
    @Column(name = "_house_area")
    private String houseArea;
    /**
     * 商业预售套数
     */
    @Column(name = "_business_count")
    private String businessCount;
    /**
     * 商业预售面积（平米）
     */
    @Column(name = "_business_area")
    private String businessArea;
    /**
     * 办公预售套数
     */
    @Column(name = "_office_count")
    private String officeCount;
    /**
     * 办公预售面积（平米）
     */
    @Column(name = "_office_area")
    private String officeArea;
    /**
     * 车位预售套数
     */
    @Column(name = "_carport_count")
    private String carportCount;
    /**
     * 车位预售面积（平米）
     */
    @Column(name = "_carport_area")
    private String carportArea;
    /**
     * 数据来源地址
     */
    @Column(name = "_url")
    private String url;
    /**
     * 数据来源
     */
    @Column(name = "_information_sources")
    private String informationSources;

    public void setCity(String city) {
        this.city = city;
    }
    
    public String getCity() {
        return this.city;
    }
    public void setPart(String part) {
        this.part = part;
    }
    
    public String getPart() {
        return this.part;
    }
    public void setDealDate(String dealDate) {
        this.dealDate = dealDate;
    }
    
    public String getDealDate() {
        return this.dealDate;
    }
    public void setHouseCount(String houseCount) {
        this.houseCount = houseCount;
    }
    
    public String getHouseCount() {
        return this.houseCount;
    }
    public void setHouseArea(String houseArea) {
        this.houseArea = houseArea;
    }
    
    public String getHouseArea() {
        return this.houseArea;
    }
    public void setBusinessCount(String businessCount) {
        this.businessCount = businessCount;
    }
    
    public String getBusinessCount() {
        return this.businessCount;
    }
    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea;
    }
    
    public String getBusinessArea() {
        return this.businessArea;
    }
    public void setOfficeCount(String officeCount) {
        this.officeCount = officeCount;
    }
    
    public String getOfficeCount() {
        return this.officeCount;
    }
    public void setOfficeArea(String officeArea) {
        this.officeArea = officeArea;
    }
    
    public String getOfficeArea() {
        return this.officeArea;
    }
    public void setCarportCount(String carportCount) {
        this.carportCount = carportCount;
    }
    
    public String getCarportCount() {
        return this.carportCount;
    }
    public void setCarportArea(String carportArea) {
        this.carportArea = carportArea;
    }
    
    public String getCarportArea() {
        return this.carportArea;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getUrl() {
        return this.url;
    }
    public void setInformationSources(String informationSources) {
        this.informationSources = informationSources;
    }
    
    public String getInformationSources() {
        return this.informationSources;
    }

}