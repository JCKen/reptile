package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_user")
public class ReptileUserModel extends GeneralModel {

	/**
	 * 员工ID
	 */
	@Column(name = "_user_id")
	private String userId;
	/**
	 * 是否停用（0：停用；1：启用）
	 */
	@Column(name = "_is_stop")
	private String isStop;
	/**
	 * 员工手机
	 */
	@Column(name = "_mobile")
	private String mobile;
	/**
	 * 员工姓名
	 */
	@Column(name = "_user_name")
	private String userName;
	/**
	 * 部门名称
	 */
	@Column(name = "_dept_name")
	private String deptName;
	/**
	 * 密码
	 */
	@Column(name = "_pwd")
	private String pwd;
	
	/**
	 * 是否是管理员
	 * @param isAdmin
	 */
	@Column(name="_is_admin")
	private String isAdmin;

	public String getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(String isAdmin) {
		this.isAdmin = isAdmin;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPwd() {
		return this.pwd;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setIsStop(String isStop) {
		this.isStop = isStop;
	}

	public String getIsStop() {
		return this.isStop;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptName() {
		return this.deptName;
	}

}