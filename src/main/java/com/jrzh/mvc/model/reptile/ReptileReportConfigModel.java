package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_report_config")
public class ReptileReportConfigModel extends GeneralModel {
    
    /**
     * 用户ID
     */
    @Column(name = "_user_id")
    private String userId;
    /**
     * 父节点ID
     */
    @Column(name = "_pid")
    private String pid;
    /**
     * 页面名称
     */
    @Column(name = "_page_name")
    private String pageName;
    /**
     * 页面URL
     */
    @Column(name = "_page_url")
    private String pageUrl;
    /**
     * 页面是否显示(0.隐藏，1.显示)
     */
    @Column(name = "_is_check")
    private Integer isCheck;
    /**
     * 排序顺序
     */
    @Column(name = "_order_by")
    private Integer orderBy;

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getUserId() {
        return this.userId;
    }
   
    public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public void setPageName(String pageName) {
        this.pageName = pageName;
    }
    
    public String getPageName() {
        return this.pageName;
    }
    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }
    
    public String getPageUrl() {
        return this.pageUrl;
    }
    public void setIsCheck(Integer isCheck) {
        this.isCheck = isCheck;
    }
    
    public Integer getIsCheck() {
        return this.isCheck;
    }
    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }
    
    public Integer getOrderBy() {
        return this.orderBy;
    }

}