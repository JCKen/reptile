package com.jrzh.mvc.model.reptile;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_house_project_name")
public class ReptileHouseProjectNameModel extends GeneralModel {
    
    /**
     * 区名
     */
    @Column(name = "_area_name")
    private String areaName;
    /**
     * 城市名
     */
    @Column(name = "_city_name")
    private String cityName;
    /**
     * 项目名称
     */
    @Column(name = "_project_name")
    private String projectName;
    /**
     * 总套数
     */
    @Column(name = "_project_total_number")
    private Double projectTotalNumber;
    /**
     * 总面积
     */
    @Column(name = "_project_total_area")
    private Double projectTotalArea;
    /**
     * 住宅套数
     */
    @Column(name = "_project_house_number")
    private Double projectHouseNumber;
    /**
     * 住宅面积
     */
    @Column(name = "_project_house_area")
    private Double projectHouseArea;
    /**
     * 可售总套数(未售)
     */
    @Column(name = "_project_total_house_unsold_number")
    private Double projectTotalHouseUnsoldNumber;
    /**
     * 可售总面积(未售)
     */
    @Column(name = "_project_total_house_unsold_area")
    private Double projectTotalHouseUnsoldArea;
    /**
     * 可售住宅套数
     */
    @Column(name = "_project_house_unsold_number")
    private Double projectHouseUnsoldNumber;
    /**
     * 可售住宅面积
     */
    @Column(name = "_project_house_unsold_area")
    private Double projectHouseUnsoldArea;
    /**
     * 已售总套数
     */
    @Column(name = "_project_total_house_sold_number")
    private Double projectTotalHouseSoldNumber;
    /**
     * 已售总面积
     */
    @Column(name = "_project_total_house_sold_area")
    private Double projectTotalHouseSoldArea;
    /**
     * 已售住宅套数
     */
    @Column(name = "_project_house_sold_number")
    private Double projectHouseSoldNumber;
    /**
     * 已售住宅面积
     */
    @Column(name = "_project_house_sold_area")
    private Double projectHouseSoldArea;
    /**
     * 已登记总套数
     */
    @Column(name = "_project_register_total_house_number")
    private Double projectRegisterTotalHouseNumber;
    /**
     * 已登记总面积
     */
    @Column(name = "_project_register_total_house_area")
    private Double projectRegisterTotalHouseArea;
    /**
     * 已登记住宅套数
     */
    @Column(name = "_project_register_house_number")
    private Double projectRegisterHouseNumber;
    /**
     * 已登记住宅面积
     */
    @Column(name = "_project_register_house_area")
    private Double projectRegisterHouseArea;
    /**
     * 合同撤消总套数
     */
    @Column(name = "_project_cancellations_house_area")
    private Double projectCancellationsHouseArea;
    /**
     * 定价逾期总次数
     */
    @Column(name = "_project_total_price_overdue_number")
    private Double projectTotalPriceOverdueNumber;
    /**
     * 信息更新时间
     */
    @Column(name = "_info_update_time")
    private Date infoUpdateTime;
    /**
     * 房管局项目ID
     */
    @Column(name = "_project_id")
    private String projectId;
    /**
     * 开发商
     */
    @Column(name = "_developer")
    private String developer;


	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public Date getInfoUpdateTime() {
		return infoUpdateTime;
	}

	public void setInfoUpdateTime(Date infoUpdateTime) {
		this.infoUpdateTime = infoUpdateTime;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

    public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public void setProjectTotalNumber(Double projectTotalNumber) {
        this.projectTotalNumber = projectTotalNumber;
    }
    
    public Double getProjectTotalNumber() {
        return this.projectTotalNumber;
    }
    public void setProjectTotalArea(Double projectTotalArea) {
        this.projectTotalArea = projectTotalArea;
    }
    
    public Double getProjectTotalArea() {
        return this.projectTotalArea;
    }
    public void setProjectHouseNumber(Double projectHouseNumber) {
        this.projectHouseNumber = projectHouseNumber;
    }
    
    public Double getProjectHouseNumber() {
        return this.projectHouseNumber;
    }
    public void setProjectHouseArea(Double projectHouseArea) {
        this.projectHouseArea = projectHouseArea;
    }
    
    public Double getProjectHouseArea() {
        return this.projectHouseArea;
    }
    public void setProjectTotalHouseUnsoldNumber(Double projectTotalHouseUnsoldNumber) {
        this.projectTotalHouseUnsoldNumber = projectTotalHouseUnsoldNumber;
    }
    
    public Double getProjectTotalHouseUnsoldNumber() {
        return this.projectTotalHouseUnsoldNumber;
    }
    public void setProjectTotalHouseUnsoldArea(Double projectTotalHouseUnsoldArea) {
        this.projectTotalHouseUnsoldArea = projectTotalHouseUnsoldArea;
    }
    
    public Double getProjectTotalHouseUnsoldArea() {
        return this.projectTotalHouseUnsoldArea;
    }
    public void setProjectHouseUnsoldNumber(Double projectHouseUnsoldNumber) {
        this.projectHouseUnsoldNumber = projectHouseUnsoldNumber;
    }
    
    public Double getProjectHouseUnsoldNumber() {
        return this.projectHouseUnsoldNumber;
    }
    public void setProjectHouseUnsoldArea(Double projectHouseUnsoldArea) {
        this.projectHouseUnsoldArea = projectHouseUnsoldArea;
    }
    
    public Double getProjectHouseUnsoldArea() {
        return this.projectHouseUnsoldArea;
    }
    public void setProjectTotalHouseSoldNumber(Double projectTotalHouseSoldNumber) {
        this.projectTotalHouseSoldNumber = projectTotalHouseSoldNumber;
    }
    
    public Double getProjectTotalHouseSoldNumber() {
        return this.projectTotalHouseSoldNumber;
    }
    public void setProjectTotalHouseSoldArea(Double projectTotalHouseSoldArea) {
        this.projectTotalHouseSoldArea = projectTotalHouseSoldArea;
    }
    
    public Double getProjectTotalHouseSoldArea() {
        return this.projectTotalHouseSoldArea;
    }
    public void setProjectHouseSoldNumber(Double projectHouseSoldNumber) {
        this.projectHouseSoldNumber = projectHouseSoldNumber;
    }
    
    public Double getProjectHouseSoldNumber() {
        return this.projectHouseSoldNumber;
    }
    public void setProjectHouseSoldArea(Double projectHouseSoldArea) {
        this.projectHouseSoldArea = projectHouseSoldArea;
    }
    
    public Double getProjectHouseSoldArea() {
        return this.projectHouseSoldArea;
    }
    public void setProjectRegisterTotalHouseNumber(Double projectRegisterTotalHouseNumber) {
        this.projectRegisterTotalHouseNumber = projectRegisterTotalHouseNumber;
    }
    
    public Double getProjectRegisterTotalHouseNumber() {
        return this.projectRegisterTotalHouseNumber;
    }
    public void setProjectRegisterTotalHouseArea(Double projectRegisterTotalHouseArea) {
        this.projectRegisterTotalHouseArea = projectRegisterTotalHouseArea;
    }
    
    public Double getProjectRegisterTotalHouseArea() {
        return this.projectRegisterTotalHouseArea;
    }
    public void setProjectRegisterHouseNumber(Double projectRegisterHouseNumber) {
        this.projectRegisterHouseNumber = projectRegisterHouseNumber;
    }
    
    public Double getProjectRegisterHouseNumber() {
        return this.projectRegisterHouseNumber;
    }
    public void setProjectRegisterHouseArea(Double projectRegisterHouseArea) {
        this.projectRegisterHouseArea = projectRegisterHouseArea;
    }
    
    public Double getProjectRegisterHouseArea() {
        return this.projectRegisterHouseArea;
    }
    public void setProjectCancellationsHouseArea(Double projectCancellationsHouseArea) {
        this.projectCancellationsHouseArea = projectCancellationsHouseArea;
    }
    
    public Double getProjectCancellationsHouseArea() {
        return this.projectCancellationsHouseArea;
    }
    public void setProjectTotalPriceOverdueNumber(Double projectTotalPriceOverdueNumber) {
        this.projectTotalPriceOverdueNumber = projectTotalPriceOverdueNumber;
    }
    
    public Double getProjectTotalPriceOverdueNumber() {
        return this.projectTotalPriceOverdueNumber;
    }

}