package com.jrzh.mvc.model.reptile;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_ygjy_house_room_infos")
public class ReptileYgjyHouseRoomInfoModel extends GeneralModel {
    
    /**
     * 关联房源id
     */
    @Column(name = "_house_id")
    private String houseId;
    /**
     * 关联楼层栋id
     */
    @Column(name = "_floor_id")
    private String floorId;
    /**
     * 房号
     */
    @Column(name = "_room_number")
    private String roomNumber;
    /**
     * 房型
     */
    @Column(name = "_room_type")
    private String roomType;
    /**
     * 房间总面积
     */
    @Column(name = "_room_total_area")
    private String roomTotalArea;
    /**
     * 户型
     */
    @Column(name = "_room_apartment")
    private String roomApartment;
    /**
     * 房号
     */
    @Column(name = "_room_status")
    private String roomStatus;
    /**
     * 抵押状态
     */
    @Column(name = "_room_mortgage")
    private String roomMortgage;
    /**
     * 查封状态
     */
    @Column(name = "_room_sealing_state")
    private String roomSealingState;
    /**
     * 所在楼层
     */
    @Column(name = "_lou_ceng")
    private String louCeng;
    /**
     * 套内面积
     */
    @Column(name = "_room_real_area")
    private String roomRealArea;
    /**
     * 按建筑面积拟售单价
     */
    @Column(name = "_room_price")
    private String roomPrice;
    /**
     * 按套内面积拟售单价
     */
    @Column(name = "_room_real_price")
    private String roomRealPrice;
    /**
     * 更新时间
     */
    @Column(name = "_info_update_time")
    private Date infoUpdateTime;

	public Date getInfoUpdateTime() {
		return infoUpdateTime;
	}

	public void setInfoUpdateTime(Date infoUpdateTime) {
		this.infoUpdateTime = infoUpdateTime;
	}

	public String getRoomPrice() {
		return roomPrice;
	}

	public void setRoomPrice(String roomPrice) {
		this.roomPrice = roomPrice;
	}

	public String getRoomRealPrice() {
		return roomRealPrice;
	}

	public void setRoomRealPrice(String roomRealPrice) {
		this.roomRealPrice = roomRealPrice;
	}

	public String getRoomRealArea() {
		return roomRealArea;
	}

	public void setRoomRealArea(String roomRealArea) {
		this.roomRealArea = roomRealArea;
	}

	public String getLouCeng() {
		return louCeng;
	}

	public void setLouCeng(String louCeng) {
		this.louCeng = louCeng;
	}

	public void setHouseId(String houseId) {
        this.houseId = houseId;
    }
    
    public String getHouseId() {
        return this.houseId;
    }
    public void setFloorId(String floorId) {
        this.floorId = floorId;
    }
    
    public String getFloorId() {
        return this.floorId;
    }
    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }
    
    public String getRoomNumber() {
        return this.roomNumber;
    }
    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }
    
    public String getRoomType() {
        return this.roomType;
    }
    public void setRoomTotalArea(String roomTotalArea) {
        this.roomTotalArea = roomTotalArea;
    }
    
    public String getRoomTotalArea() {
        return this.roomTotalArea;
    }
    public void setRoomApartment(String roomApartment) {
        this.roomApartment = roomApartment;
    }
    
    public String getRoomApartment() {
        return this.roomApartment;
    }
    public void setRoomStatus(String roomStatus) {
        this.roomStatus = roomStatus;
    }
    
    public String getRoomStatus() {
        return this.roomStatus;
    }
    public void setRoomMortgage(String roomMortgage) {
        this.roomMortgage = roomMortgage;
    }
    
    public String getRoomMortgage() {
        return this.roomMortgage;
    }
    public void setRoomSealingState(String roomSealingState) {
        this.roomSealingState = roomSealingState;
    }
    
    public String getRoomSealingState() {
        return this.roomSealingState;
    }

}