package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_estate_datum")
public class ReptileEstateDatumModel extends GeneralModel {
    
    /**
     * 时间
     */
    @Column(name = "_time")
    private String time;
    /**
     * 指标编号
     */
    @Column(name = "_code")
    private String code;
    /**
     * 指标值
     */
    @Column(name = "_data")
    private String data;

    public void setTime(String time) {
        this.time = time;
    }
    
    public String getTime() {
        return this.time;
    }
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCode() {
        return this.code;
    }
    public void setData(String data) {
    	if(StringUtils.isNotBlank(data)){
        	Double dou = Double.parseDouble(data);
    		dou = (double)Math.round(dou*100)/100;
    		this.data = dou.toString();
    	}else{
    		this.data = data;
    	}
        
    }
    
    public String getData() {
        return this.data;
    }

}