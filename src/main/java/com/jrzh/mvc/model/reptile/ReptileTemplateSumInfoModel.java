package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_template_sum_info")
public class ReptileTemplateSumInfoModel extends GeneralModel {
    
    /**
     * 用户ID
     */
    @Column(name = "_user_id")
    private String userId;
    /**
     * 模板ID
     */
    @Column(name = "_template_id")
    private String templateId;
    /**
     * 一手房占比
     */
    @Column(name = "_one_house_ratio")
    private String oneHouseRatio;
    /**
     * 二手房占比
     */
    @Column(name = "_two_house_ratio")
    private String twoHouseRatio;
    /**
     * 土地占比
     */
    @Column(name = "_land_ratio")
    private String landRatio;
    
    /**
     * 一手房金额
     */
    @Column(name = "_one_house_amt")
    private String oneHouseAmt;
    /**
     * 二手房金额
     */
    @Column(name = "_two_house_amt")
    private String twoHouseAmt;
    /**
     * 土地金额
     */
    @Column(name = "_land_amt")
    private String landAmt;
    /**
     * 参考金额
     */
    @Column(name = "_refer_amt")
    private String referAmt;

    
    
    public String getOneHouseAmt() {
		return oneHouseAmt;
	}

	public void setOneHouseAmt(String oneHouseAmt) {
		this.oneHouseAmt = oneHouseAmt;
	}

	public String getTwoHouseAmt() {
		return twoHouseAmt;
	}

	public void setTwoHouseAmt(String twoHouseAmt) {
		this.twoHouseAmt = twoHouseAmt;
	}

	public String getLandAmt() {
		return landAmt;
	}

	public void setLandAmt(String landAmt) {
		this.landAmt = landAmt;
	}

	public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getUserId() {
        return this.userId;
    }
    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }
    
    public String getTemplateId() {
        return this.templateId;
    }
    public void setOneHouseRatio(String oneHouseRatio) {
        this.oneHouseRatio = oneHouseRatio;
    }
    
    public String getOneHouseRatio() {
        return this.oneHouseRatio;
    }
    public void setTwoHouseRatio(String twoHouseRatio) {
        this.twoHouseRatio = twoHouseRatio;
    }
    
    public String getTwoHouseRatio() {
        return this.twoHouseRatio;
    }
    public void setLandRatio(String landRatio) {
        this.landRatio = landRatio;
    }
    
    public String getLandRatio() {
        return this.landRatio;
    }
    public void setReferAmt(String referAmt) {
        this.referAmt = referAmt;
    }
    
    public String getReferAmt() {
        return this.referAmt;
    }

}