package com.jrzh.mvc.model.reptile;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_ygjy_sale_infos")
public class ReptileYgjySaleInfoModel extends GeneralModel {
    
    /**
     * 区域名称
     */
    @Column(name = "_area")
    private String area;
    /**
     * 住宅套数
     */
    @Column(name = "_house_set_number")
    private Double houseSetNumber;
    /**
     * 住宅面积
     */
    @Column(name = "_house_acreage")
    private Double houseAcreage;
    /**
     * 商业套数
     */
    @Column(name = "_biz_set_number")
    private Double bizSetNumber;
    /**
     * 商业面积
     */
    @Column(name = "_biz_acreage")
    private Double bizAcreage;
    /**
     * 办公套数
     */
    @Column(name = "_office_set_number")
    private Double officeSetNumber;
    /**
     * 办公面积
     */
    @Column(name = "_office_acreage")
    private Double officeAcreage;
    /**
     * 车位套数
     */
    @Column(name = "_position_set_number")
    private Double positionSetNumber;
    /**
     * 车位面积
     */
    @Column(name = "_position_acreage")
    private Double positionAcreage;
    /**
     * 销售类型
     */
    @Column(name = "sale_type")
    private String saleType;
    /**
     * 数据源
     */
    @Column(name = "_source")
    private String source;
    /**
     * 数据更新时间
     */
    @Column(name = "_data_update_time")
    private Date dataUpdateTime;

    public void setArea(String area) {
        this.area = area;
    }
    
    public String getArea() {
        return this.area;
    }
    public void setHouseSetNumber(Double houseSetNumber) {
        this.houseSetNumber = houseSetNumber;
    }
    
    public Double getHouseSetNumber() {
        return this.houseSetNumber;
    }
    public void setHouseAcreage(Double houseAcreage) {
        this.houseAcreage = houseAcreage;
    }
    
    public Double getHouseAcreage() {
        return this.houseAcreage;
    }
    public void setBizSetNumber(Double bizSetNumber) {
        this.bizSetNumber = bizSetNumber;
    }
    
    public Double getBizSetNumber() {
        return this.bizSetNumber;
    }
    public void setBizAcreage(Double bizAcreage) {
        this.bizAcreage = bizAcreage;
    }
    
    public Double getBizAcreage() {
        return this.bizAcreage;
    }
    public void setOfficeSetNumber(Double officeSetNumber) {
        this.officeSetNumber = officeSetNumber;
    }
    
    public Double getOfficeSetNumber() {
        return this.officeSetNumber;
    }
    public void setOfficeAcreage(Double officeAcreage) {
        this.officeAcreage = officeAcreage;
    }
    
    public Double getOfficeAcreage() {
        return this.officeAcreage;
    }
    public void setPositionSetNumber(Double positionSetNumber) {
        this.positionSetNumber = positionSetNumber;
    }
    
    public Double getPositionSetNumber() {
        return this.positionSetNumber;
    }
    public void setPositionAcreage(Double positionAcreage) {
        this.positionAcreage = positionAcreage;
    }
    
    public Double getPositionAcreage() {
        return this.positionAcreage;
    }
    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }
    
    public String getSaleType() {
        return this.saleType;
    }
    public void setSource(String source) {
        this.source = source;
    }
    
    public String getSource() {
        return this.source;
    }
    public void setDataUpdateTime(Date dataUpdateTime) {
        this.dataUpdateTime = dataUpdateTime;
    }
    
    public Date getDataUpdateTime() {
        return this.dataUpdateTime;
    }

}