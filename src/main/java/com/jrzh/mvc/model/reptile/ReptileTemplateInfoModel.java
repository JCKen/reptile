package com.jrzh.mvc.model.reptile;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_template_info")
public class ReptileTemplateInfoModel extends GeneralModel {

	/**
	 * 用户ID
	 */
	@Column(name = "_user_id")
	private String userId;
	/**
	 * 楼盘ID
	 */
	@Column(name = "_house_id")
	private String houseId;
	/**
	 * 楼盘价格
	 */
	@Column(name = "_house_price")
	private String housePrice;
	/**
	 * 楼盘权重
	 */
	@Column(name = "_house_weight")
	private String houseWeight;
	/**
	 * 楼盘类型（一手房或者二手房或者土地）
	 */
	@Column(name = "_house_type")
	private String houseType;
	/**
	 * 信息生成时间
	 */
	@Column(name = "_info_update_time")
	private Date infoUpdateTime;
	/**
	 * 修正值
	 */
	@Column(name = "_correction")
	private String correction;
	/**
	 * 模板ID
	 */
	@Column(name = "_template_id")
	private String templateId;
	/**
	 * 成交量
	 */
	@Column(name = "_volume")
	private String volume;
	/**
	 * 楼盘名
	 */
	@Column(name = "_house_name")
	private String houseName;
	/**
	 * 备注信息
	 */
	@Column(name = "_desc")
	private String desc;

	/**
	 * 修正金額
	 */
	@Column(name = "_edit_amt")
	private String editAmt;

	/**
	 * 综合修正系数
	 */
	@Column(name = "_total_correction_value")
	private String totalCorrectionValue;
	/**
	 * 对比价格
	 */
	@Column(name = "_compare_price")
	private String comparePrice;
	/**
	 * 二手交易税费系数
	 */
	@Column(name = "_trade_tax")
	private String tradeTax;
	/**
	 * 二手房实际成交价
	 */
	@Column(name = "_transaction_price")
	private String transactionPrice;
	/**
	 * 市场加权价
	 */
	@Column(name = "_sub_price")
	private String subPrice;
	/**
	 * 土地预估售价
	 */
	@Column(name = "_estimate_price")
	private String estimatePrice;
	/**
	 * 楼面价占比
	 */
	@Column(name = "_floor_price_proportion")
	private String floorPriceProportion;

	/**
	 * 土地面积
	 */
	@Column(name = "_tudi_acreage")
	private String tudiAcreage;

	/**
	 * 土地成交时间
	 */
	@Column(name = "_tudi_time")
	private String tudiTime;
	/**
	 * 土地用途
	 */
	@Column(name = "_tudi_use")
	private String tudiUse;

	public String getTudiAcreage() {
		return tudiAcreage;
	}

	public void setTudiAcreage(String tudiAcreage) {
		this.tudiAcreage = tudiAcreage;
	}

	public String getTudiTime() {
		return tudiTime;
	}

	public void setTudiTime(String tudiTime) {
		this.tudiTime = tudiTime;
	}

	public String getTudiUse() {
		return tudiUse;
	}

	public void setTudiUse(String tudiUse) {
		this.tudiUse = tudiUse;
	}

	public String getTotalCorrectionValue() {
		return totalCorrectionValue;
	}

	public void setTotalCorrectionValue(String totalCorrectionValue) {
		this.totalCorrectionValue = totalCorrectionValue;
	}

	public String getComparePrice() {
		return comparePrice;
	}

	public void setComparePrice(String comparePrice) {
		this.comparePrice = comparePrice;
	}

	public String getTradeTax() {
		return tradeTax;
	}

	public void setTradeTax(String tradeTax) {
		this.tradeTax = tradeTax;
	}

	public String getTransactionPrice() {
		return transactionPrice;
	}

	public void setTransactionPrice(String transactionPrice) {
		this.transactionPrice = transactionPrice;
	}

	public String getSubPrice() {
		return subPrice;
	}

	public void setSubPrice(String subPrice) {
		this.subPrice = subPrice;
	}

	public String getEstimatePrice() {
		return estimatePrice;
	}

	public void setEstimatePrice(String estimatePrice) {
		this.estimatePrice = estimatePrice;
	}

	public String getFloorPriceProportion() {
		return floorPriceProportion;
	}

	public void setFloorPriceProportion(String floorPriceProportion) {
		this.floorPriceProportion = floorPriceProportion;
	}

	public String getEditAmt() {
		return editAmt;
	}

	public void setEditAmt(String editAmt) {
		this.editAmt = editAmt;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getCorrection() {
		return correction;
	}

	public void setCorrection(String correction) {
		this.correction = correction;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setHouseId(String houseId) {
		this.houseId = houseId;
	}

	public String getHouseId() {
		return this.houseId;
	}

	public void setHousePrice(String housePrice) {
		this.housePrice = housePrice;
	}

	public String getHousePrice() {
		return this.housePrice;
	}

	public void setHouseWeight(String houseWeight) {
		this.houseWeight = houseWeight;
	}

	public String getHouseWeight() {
		return this.houseWeight;
	}

	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}

	public String getHouseType() {
		return this.houseType;
	}

	public void setInfoUpdateTime(Date infoUpdateTime) {
		this.infoUpdateTime = infoUpdateTime;
	}

	public Date getInfoUpdateTime() {
		return this.infoUpdateTime;
	}

}