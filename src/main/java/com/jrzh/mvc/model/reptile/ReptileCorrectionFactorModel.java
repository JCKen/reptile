package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_correction_factor")
public class ReptileCorrectionFactorModel extends GeneralModel {
    
    /**
     * 父ID
     */
    @Column(name = "_pid")
    private String pid;
    /**
     * 修正项
     */
    @Column(name = "_correction_name")
    private String correctionName;
    /**
     * 修正系数最大值
     */
    @Column(name = "_correction_max")
    private String correctionMax;
    /**
     * 修正系数最小值
     */
    @Column(name = "_correction_min")
    private String correctionMin;

    public void setPid(String pid) {
        this.pid = pid;
    }
    
    public String getPid() {
        return this.pid;
    }
    public void setCorrectionName(String correctionName) {
        this.correctionName = correctionName;
    }
    
    public String getCorrectionName() {
        return this.correctionName;
    }
    public void setCorrectionMax(String correctionMax) {
        this.correctionMax = correctionMax;
    }
    
    public String getCorrectionMax() {
        return this.correctionMax;
    }
    public void setCorrectionMin(String correctionMin) {
        this.correctionMin = correctionMin;
    }
    
    public String getCorrectionMin() {
        return this.correctionMin;
    }

}