package com.jrzh.mvc.model.reptile;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

//每日全市交易销售记录表(每日交易)

@Entity
@Table(name = "reptile_sale_info")
public class ReptileSaleInfoModel extends GeneralModel {
    
    /**
     * 成交套数
     */
    @Column(name = "_all_number")
    private String allNumber;
    /**
     * 成交面积
     */
    @Column(name = "_all_area")
    private String allArea;
    /**
     * 城市(
     */
    @Column(name = "_city")
    private String city;
    /**
     * 成交价格
     */
    @Column(name = "_all_price")
    private String allPrice;
    /**
     * 时间
     */
    @Column(name = "_info_updata_time")
    private Date infoUpdataTime;

    public void setAllNumber(String allNumber) {
        this.allNumber = allNumber;
    }
    
    public String getAllNumber() {
        return this.allNumber;
    }
    public void setAllArea(String allArea) {
        this.allArea = allArea;
    }
    
    public String getAllArea() {
        return this.allArea;
    }
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getCity() {
        return this.city;
    }
    public void setAllPrice(String allPrice) {
        this.allPrice = allPrice;
    }
    
    public String getAllPrice() {
        return this.allPrice;
    }
    public void setInfoUpdataTime(Date infoUpdataTime) {
        this.infoUpdataTime = infoUpdataTime;
    }
    
    public Date getInfoUpdataTime() {
        return this.infoUpdataTime;
    }

}