package com.jrzh.mvc.model.reptile;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_new_house")
public class ReptileSearchResultModel extends GeneralModel {
    
    private String houseCity;
    
    private String housePart;
    
    private Double buildingArea;
   
    private Double areaCovered;
    
    private String houseName;

	public String getHouseName() {
		return houseName;
	}
	
	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public String getHouseCity() {
		return houseCity;
	}
	
	public void setHouseCity(String houseCity) {
		this.houseCity = houseCity;
	}
	
	public String getHousePart() {
		return housePart;
	}

	public void setHousePart(String housePart) {
		this.housePart = housePart;
	}

	public Double getBuildingArea() {
		return buildingArea;
	}

	public void setBuildingArea(Double buildingArea) {
		this.buildingArea = buildingArea;
	}

	public Double getAreaCovered() {
		return areaCovered;
	}

	public void setAreaCovered(Double areaCovered) {
		this.areaCovered = areaCovered;
	}

}