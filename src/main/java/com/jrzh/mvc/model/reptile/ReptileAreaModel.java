package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_area")
public class ReptileAreaModel extends GeneralModel {
    
    /**
     * 区域名称
     */
    @Column(name = "_area_name")
    private String areaName;
    
    /**
     * 所属城市
     */
    @Column(name = "_own_city")
    private String ownCity;
    
    /**
     * 经纬度
     */
    @Column(name = "_latitude_longitude")
    private String latitudeLongitude;

    public String getLatitudeLongitude() {
		return latitudeLongitude;
	}

	public void setLatitudeLongitude(String latitudeLongitude) {
		this.latitudeLongitude = latitudeLongitude;
	}

	public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
    
    public String getAreaName() {
        return this.areaName;
    }

	public String getOwnCity() {
		return ownCity;
	}

	public void setOwnCity(String ownCity) {
		this.ownCity = ownCity;
	}

}