package com.jrzh.mvc.model.reptile;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_house_price")
public class ReptileHousePriceModel extends GeneralModel {
    
    /**
     * 房价
     */
    @Column(name = "_house_price")
    private String housePrice;
    /**
     * 房子ID
     */
    @Column(name = "_house_id")
    private String houseId;
    /**
     * 房价单位(1. 元/㎡ 2. 万/套)
     */
    @Column(name = "_house_price_unit")
    private String housePriceUnit;
    /**
     * 房价来源
     */
    @Column(name = "_info_source")
    private String infoSource;
    /**
     * 信息更新时间
     */
    @Column(name = "_info_update_time")
    private Date infoUpdateTime;

    public Date getInfoUpdateTime() {
		return infoUpdateTime;
	}

	public void setInfoUpdateTime(Date infoUpdateTime) {
		this.infoUpdateTime = infoUpdateTime;
	}

	public String getInfoSource() {
		return infoSource;
	}

	public void setInfoSource(String infoSource) {
		this.infoSource = infoSource;
	}

	public void setHousePrice(String housePrice) {
        this.housePrice = housePrice;
    }
    
    public String getHousePrice() {
        return this.housePrice;
    }
    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }
    
    public String getHouseId() {
        return this.houseId;
    }
    public void setHousePriceUnit(String housePriceUnit) {
        this.housePriceUnit = housePriceUnit;
    }
    
    public String getHousePriceUnit() {
        return this.housePriceUnit;
    }

}