package com.jrzh.mvc.model.reptile;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_commodity_house_info")
public class ReptileCommodityHouseInfoModel extends GeneralModel {
    
    /**
     * 户型
     */
    @Column(name = "_house_type")
    private String houseType;
    /**
     * 成交套数
     */
    @Column(name = "_transaction_number")
    private String transactionNumber;
    /**
     * 成交面积
     */
    @Column(name = "_transaction_area")
    private String transactionArea;
    /**
     * 城市
     */
    @Column(name = "_city")
    private String city;
    /**
     * 区域
     */
    @Column(name = "_part")
    private String part;
    /**
     * 成交均价
     */
    @Column(name = "_transaction_price")
    private String transactionPrice;
    /**
     * 可售面积
     */
    @Column(name = "_saleable_area")
    private String saleableArea;
    /**
     * 可售套数
     */
    @Column(name = "_available_sets")
    private String availableSets;
    /**
     * 时间
     */
    @Column(name = "_info_updata_time")
    private Date infoUpdataTime;

    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }
    
    public String getHouseType() {
        return this.houseType;
    }
    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }
    
    public String getTransactionNumber() {
        return this.transactionNumber;
    }
    public void setTransactionArea(String transactionArea) {
        this.transactionArea = transactionArea;
    }
    
    public String getTransactionArea() {
        return this.transactionArea;
    }
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getCity() {
        return this.city;
    }
    public void setPart(String part) {
        this.part = part;
    }
    
    public String getPart() {
        return this.part;
    }
    public void setTransactionPrice(String transactionPrice) {
        this.transactionPrice = transactionPrice;
    }
    
    public String getTransactionPrice() {
        return this.transactionPrice;
    }
    public void setSaleableArea(String saleableArea) {
        this.saleableArea = saleableArea;
    }
    
    public String getSaleableArea() {
        return this.saleableArea;
    }
    public void setAvailableSets(String availableSets) {
        this.availableSets = availableSets;
    }
    
    public String getAvailableSets() {
        return this.availableSets;
    }
    public void setInfoUpdataTime(Date infoUpdataTime) {
        this.infoUpdataTime = infoUpdataTime;
    }
    
    public Date getInfoUpdataTime() {
        return this.infoUpdataTime;
    }

}