package com.jrzh.mvc.model.reptile;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_esf_houses_xiaoqu")
public class ReptileEsfHousesXiaoquModel extends GeneralModel {
    
    /**
     * 小区名
     */
    @Column(name = "_house_name")
    private String houseName;
    /**
     * 小区别名
     */
    @Column(name = "_houses_other_name")
    private String housesOtherName;
    /**
     * 小区所属城市
     */
    @Column(name = "_houses_city")
    private String housesCity;
    /**
     * 小区所属区域
     */
    @Column(name = "_houses_part")
    private String housesPart;
    /**
     * 所在地址
     */
    @Column(name = "_houses_address")
    private String housesAddress;
    /**
     * 小区建筑年代
     */
    @Column(name = "_houses_building_year")
    private String housesBuildingYear;
    /**
     * 房屋类型
     */
    @Column(name = "_houses_type")
    private String housesType;
    /**
     * 房屋均价
     */
    @Column(name = "_houses_price")
    private String housesPrice;
    /**
     * 产权年限
     */
    @Column(name = "_year_of_property_rights")
    private String yearOfPropertyRights;
    /**
     * 信息来源
     */
    @Column(name = "_information_sources")
    private String informationSources;
    /**
     * 信息更新时间
     */
    @Column(name = "_info_update_time")
    private String infoUpdateTime;
    /**
     * 纬度
     */
    @Column(name = "_latitude")
    private BigDecimal latitude;
    /**
     *  经度
     */
    @Column(name = "_longitude")
    private BigDecimal longitude;
    /**
     * 开发商
     */
    @Column(name = "_developers")
    private String developers;
    /**
     * 绿化率
     */
    @Column(name = "_afforestation_rate")
    private String afforestationRate;
    /**
     * 容积率
     */
    @Column(name = "_plot_ratio")
    private String plotRatio;
    /**
     * 建筑类型
     */
    @Column(name = "_building_type")
    private String buildingType;
    /**
     * 物业公司
     */
    @Column(name = "_property_company")
    private String propertyCompany;
    /**
     * 房屋总数
     */
    @Column(name = "_total_households")
    private String totalHouseholds;
    /**
     * 物业费
     */
    @Column(name = "_property_costs")
    private String propertyCosts;
    /**
     * 附近交通
     */
    @Column(name = "_traffic")
    private String traffic;
    /**
     * 幼儿园
     */
    @Column(name = "_kindergarten")
    private String kindergarten;
    /**
     * 中小学
     */
    @Column(name = "_school")
    private String school;
    /**
     * 综合商场
     */
    @Column(name = "_power_center")
    private String powerCenter;
    /**
     * 医院
     */
    @Column(name = "_hospital")
    private String hospital;
    /**
     * 银行
     */
    @Column(name = "_bank")
    private String bank;
    /**
     * 邮政
     */
    @Column(name = "_post")
    private String post;
    /**
     * 其他
     */
    @Column(name = "_other")
    private String other;
    /**
     * 占地面积
     */
    @Column(name = "_floor_area")
    private String floorArea;
    /**
     * 建筑面积
     */
    @Column(name = "_building_area")
    private String buildingArea;
    /**
     * 数据访问地址
     */
    @Column(name = "_url")
    private String url;
    
    /**
     * 参考价月份
     */
    @Column(name = "_rp_month")
    private String rpMonth;

    public String getRpMonth() {
		return rpMonth;
	}

	public void setRpMonth(String rpMonth) {
		this.rpMonth = rpMonth;
	}

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public void setHousesOtherName(String housesOtherName) {
        this.housesOtherName = housesOtherName;
    }
    
    public String getHousesOtherName() {
        return this.housesOtherName;
    }
    public void setHousesCity(String housesCity) {
        this.housesCity = housesCity;
    }
    
    public String getHousesCity() {
        return this.housesCity;
    }
    public void setHousesPart(String housesPart) {
        this.housesPart = housesPart;
    }
    
    public String getHousesPart() {
        return this.housesPart;
    }
    public void setHousesAddress(String housesAddress) {
        this.housesAddress = housesAddress;
    }
    
    public String getHousesAddress() {
        return this.housesAddress;
    }
    public void setHousesBuildingYear(String housesBuildingYear) {
        this.housesBuildingYear = housesBuildingYear;
    }
    
    public String getHousesBuildingYear() {
        return this.housesBuildingYear;
    }
    public void setHousesType(String housesType) {
        this.housesType = housesType;
    }
    
    public String getHousesType() {
        return this.housesType;
    }
    public void setHousesPrice(String housesPrice) {
        this.housesPrice = housesPrice;
    }
    
    public String getHousesPrice() {
        return this.housesPrice;
    }
    public void setYearOfPropertyRights(String yearOfPropertyRights) {
        this.yearOfPropertyRights = yearOfPropertyRights;
    }
    
    public String getYearOfPropertyRights() {
        return this.yearOfPropertyRights;
    }
    public void setInformationSources(String informationSources) {
        this.informationSources = informationSources;
    }
    
    public String getInformationSources() {
        return this.informationSources;
    }
    public void setInfoUpdateTime(String infoUpdateTime) {
        this.infoUpdateTime = infoUpdateTime;
    }
    
    public String getInfoUpdateTime() {
        return this.infoUpdateTime;
    }
    
    public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public void setDevelopers(String developers) {
        this.developers = developers;
    }
    
    public String getDevelopers() {
        return this.developers;
    }
    public void setAfforestationRate(String afforestationRate) {
        this.afforestationRate = afforestationRate;
    }
    
    public String getAfforestationRate() {
        return this.afforestationRate;
    }
    public void setPlotRatio(String plotRatio) {
        this.plotRatio = plotRatio;
    }
    
    public String getPlotRatio() {
        return this.plotRatio;
    }
    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }
    
    public String getBuildingType() {
        return this.buildingType;
    }
    public void setPropertyCompany(String propertyCompany) {
        this.propertyCompany = propertyCompany;
    }
    
    public String getPropertyCompany() {
        return this.propertyCompany;
    }
    public void setTotalHouseholds(String totalHouseholds) {
        this.totalHouseholds = totalHouseholds;
    }
    
    public String getTotalHouseholds() {
        return this.totalHouseholds;
    }
    public void setPropertyCosts(String propertyCosts) {
        this.propertyCosts = propertyCosts;
    }
    
    public String getPropertyCosts() {
        return this.propertyCosts;
    }
    public void setTraffic(String traffic) {
        this.traffic = traffic;
    }
    
    public String getTraffic() {
        return this.traffic;
    }
    public void setKindergarten(String kindergarten) {
        this.kindergarten = kindergarten;
    }
    
    public String getKindergarten() {
        return this.kindergarten;
    }
    public void setSchool(String school) {
        this.school = school;
    }
    
    public String getSchool() {
        return this.school;
    }
    public void setPowerCenter(String powerCenter) {
        this.powerCenter = powerCenter;
    }
    
    public String getPowerCenter() {
        return this.powerCenter;
    }
    public void setHospital(String hospital) {
        this.hospital = hospital;
    }
    
    public String getHospital() {
        return this.hospital;
    }
    public void setBank(String bank) {
        this.bank = bank;
    }
    
    public String getBank() {
        return this.bank;
    }
    public void setPost(String post) {
        this.post = post;
    }
    
    public String getPost() {
        return this.post;
    }
    public void setOther(String other) {
        this.other = other;
    }
    
    public String getOther() {
        return this.other;
    }
    public void setFloorArea(String floorArea) {
        this.floorArea = floorArea;
    }
    
    public String getFloorArea() {
        return this.floorArea;
    }
    public void setBuildingArea(String buildingArea) {
        this.buildingArea = buildingArea;
    }
    
    public String getBuildingArea() {
        return this.buildingArea;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getUrl() {
        return this.url;
    }

}