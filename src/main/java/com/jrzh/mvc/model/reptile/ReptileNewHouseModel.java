package com.jrzh.mvc.model.reptile;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_new_house")
public class ReptileNewHouseModel extends GeneralModel {
    
    /**
     * 小区名
     */
    @Column(name = "_house_name")
    private String houseName;
    /**
     * 小区别名
     */
    @Column(name = "_house_other_name")
    private String houseOtherName;
    /**
     * 小区所在市
     */
    @Column(name = "_house_city")
    private String houseCity;
    /**
     * 小区所在区
     */
    @Column(name = "_house_part")
    private String housePart;
    /**
     * 小区详细地址
     */
    @Column(name = "_house_address")
    private String houseAddress;
    /**
     * 楼盘均价
     */
    @Column(name = "_house_price")
    private String housePrice;
    /**
     * 楼盘类型（商业、住宅、别墅、写字楼）
     */
    @Column(name = "_house_type")
    private String houseType;
    /**
     * 楼盘销售状态
     */
    @Column(name = "_house_sale_status")
    private String houseSaleStatus;
    /**
     * 楼盘是否新开（根据网址新开楼盘进行判断）
     */
    @Column(name = "_is_new_house")
    private String isNewHouse;
    /**
     * 总价（多少钱一套）
     */
    @Column(name = "_house_all_price")
    private String houseAllPrice;
    /**
     * 楼盘标签
     */
    @Column(name = "_house_tab")
    private String houseTab;
    /**
     * 楼盘评分
     */
    @Column(name = "_house_grade")
    private String houseGrade;
    /**
     * 最新开盘时间
     */
    @Column(name = "_latest_sale_time")
    private String latestSaleTime;
    /**
     * 主力户型
     */
    @Column(name = "_main_unit")
    private String mainUnit;
    /**
     * 售楼处地址
     */
    @Column(name = "_sales_office_address")
    private String salesOfficeAddress;
    /**
     * 开发商
     */
    @Column(name = "_developers")
    private String developers;
    /**
     * 物业类别
     */
    @Column(name = "_property_category")
    private String propertyCategory;
    /**
     * 建筑面积
     */
    @Column(name = "_building_area")
    private String buildingArea;
    /**
     * 占地面积
     */
    @Column(name = "_area_covered")
    private String areaCovered;
    /**
     * 绿化率
     */
    @Column(name = "_afforestation_rate")
    private String afforestationRate;
    /**
     * 容积率
     */
    @Column(name = "_plot_ratio")
    private String plotRatio;
    /**
     * 车位
     */
    @Column(name = "_parking_lot")
    private String parkingLot;
    /**
     * 供水方式
     */
    @Column(name = "_water_supply_mode")
    private String waterSupplyMode;
    /**
     * 供电方式
     */
    @Column(name = "_power_supply_mode")
    private String powerSupplyMode;
    /**
     * 产权年限
     */
    @Column(name = "_property_right_years")
    private String propertyRightYears;
    /**
     * 物业公司
     */
    @Column(name = "_property_company")
    private String propertyCompany;
    /**
     * 规划户数
     */
    @Column(name = "_planning_households")
    private String planningHouseholds;
    /**
     * 交房时间
     */
    @Column(name = "_time_of_delivery")
    private String timeOfDelivery;
    /**
     * 物业费用
     */
    @Column(name = "_property_costs")
    private String propertyCosts;
    /**
     * 信息来源
     */
    @Column(name = "_information_sources")
    private String informationSources;
    /**
     * 信息更新时间
     */
    @Column(name = "_info_update_time")
    private Date infoUpdateTime;
    /**
     * 
     */
    @Column(name = "_latitude")
    private BigDecimal latitude;
    /**
     * 
     */
    @Column(name = "_longitude")
    private BigDecimal longitude;
    /**
     * 附近交通
     */
    @Column(name = "_traffic")
    private String traffic;
    /**
     * 幼儿园
     */
    @Column(name = "_kindergarten")
    private String kindergarten;
    /**
     * 中小学
     */
    @Column(name = "_school")
    private String school;
    /**
     * 综合商场
     */
    @Column(name = "_power_center")
    private String powerCenter;
    /**
     * 医院
     */
    @Column(name = "_hospital")
    private String hospital;
    /**
     * 银行
     */
    @Column(name = "_bank")
    private String bank;
    /**
     * 邮政
     */
    @Column(name = "_post")
    private String post;
    /**
     * 其他
     */
    @Column(name = "_other")
    private String other;
    /**
     * 价格单位
     */
    @Column(name = "_peice_unit")
    private String peiceUnit;
    /**
     * 房价单位
     */
    @Column(name = "_house_price_unit")
    private String housePriceUnit;
    /**
     * 交易权属
     */
    @Column(name = "_house_trading_authority")
    private String houseTradingAuthority;
    /**
     * 数据来源楼房链接
     */
    @Column(name = "_house_url")
    private String houseUrl;
    /**
     * 楼盘建面
     */
    @Column(name = "_house_building_area")
    private String houseBuildingArea;


	public String getHouseBuildingArea() {
		return houseBuildingArea;
	}

	public void setHouseBuildingArea(String houseBuildingArea) {
		this.houseBuildingArea = houseBuildingArea;
	}

	public String getHouseUrl() {
		return houseUrl;
	}

	public void setHouseUrl(String houseUrl) {
		this.houseUrl = houseUrl;
	}

	public String getHouseTradingAuthority() {
		return houseTradingAuthority;
	}

	public void setHouseTradingAuthority(String houseTradingAuthority) {
		this.houseTradingAuthority = houseTradingAuthority;
	}

	public void setHouseName(String houseName) {
        this.houseName = houseName;
    }
    
    public String getHouseName() {
        return this.houseName;
    }
    public void setHouseOtherName(String houseOtherName) {
        this.houseOtherName = houseOtherName;
    }
    
    public String getHouseOtherName() {
        return this.houseOtherName;
    }
    public void setHouseCity(String houseCity) {
        this.houseCity = houseCity;
    }
    
    public String getHouseCity() {
        return this.houseCity;
    }
    public void setHousePart(String housePart) {
        this.housePart = housePart;
    }
    
    public String getHousePart() {
        return this.housePart;
    }
    public void setHouseAddress(String houseAddress) {
        this.houseAddress = houseAddress;
    }
    
    public String getHouseAddress() {
        return this.houseAddress;
    }
    public void setHousePrice(String housePrice) {
        this.housePrice = housePrice;
    }
    
    public String getHousePrice() {
        return this.housePrice;
    }
    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }
    
    public String getHouseType() {
        return this.houseType;
    }
    public void setHouseSaleStatus(String houseSaleStatus) {
        this.houseSaleStatus = houseSaleStatus;
    }
    
    public String getHouseSaleStatus() {
        return this.houseSaleStatus;
    }
    public void setIsNewHouse(String isNewHouse) {
        this.isNewHouse = isNewHouse;
    }
    
    public String getIsNewHouse() {
        return this.isNewHouse;
    }
    public void setHouseAllPrice(String houseAllPrice) {
        this.houseAllPrice = houseAllPrice;
    }
    
    public String getHouseAllPrice() {
        return this.houseAllPrice;
    }
    public void setHouseTab(String houseTab) {
        this.houseTab = houseTab;
    }
    
    public String getHouseTab() {
        return this.houseTab;
    }
    public void setHouseGrade(String houseGrade) {
        this.houseGrade = houseGrade;
    }
    
    public String getHouseGrade() {
        return this.houseGrade;
    }
    public void setLatestSaleTime(String latestSaleTime) {
        this.latestSaleTime = latestSaleTime;
    }
    
    public String getLatestSaleTime() {
        return this.latestSaleTime;
    }
    public void setMainUnit(String mainUnit) {
        this.mainUnit = mainUnit;
    }
    
    public String getMainUnit() {
        return this.mainUnit;
    }
    public void setSalesOfficeAddress(String salesOfficeAddress) {
        this.salesOfficeAddress = salesOfficeAddress;
    }
    
    public String getSalesOfficeAddress() {
        return this.salesOfficeAddress;
    }
    public void setDevelopers(String developers) {
        this.developers = developers;
    }
    
    public String getDevelopers() {
        return this.developers;
    }
    public void setPropertyCategory(String propertyCategory) {
        this.propertyCategory = propertyCategory;
    }
    
    public String getPropertyCategory() {
        return this.propertyCategory;
    }
    public void setBuildingArea(String buildingArea) {
        this.buildingArea = buildingArea;
    }
    
    public String getBuildingArea() {
        return this.buildingArea;
    }
    public void setAreaCovered(String areaCovered) {
        this.areaCovered = areaCovered;
    }
    
    public String getAreaCovered() {
        return this.areaCovered;
    }
    public void setAfforestationRate(String afforestationRate) {
        this.afforestationRate = afforestationRate;
    }
    
    public String getAfforestationRate() {
        return this.afforestationRate;
    }
    public void setPlotRatio(String plotRatio) {
        this.plotRatio = plotRatio;
    }
    
    public String getPlotRatio() {
        return this.plotRatio;
    }
    public void setParkingLot(String parkingLot) {
        this.parkingLot = parkingLot;
    }
    
    public String getParkingLot() {
        return this.parkingLot;
    }
    public void setWaterSupplyMode(String waterSupplyMode) {
        this.waterSupplyMode = waterSupplyMode;
    }
    
    public String getWaterSupplyMode() {
        return this.waterSupplyMode;
    }
    public void setPowerSupplyMode(String powerSupplyMode) {
        this.powerSupplyMode = powerSupplyMode;
    }
    
    public String getPowerSupplyMode() {
        return this.powerSupplyMode;
    }
    public void setPropertyRightYears(String propertyRightYears) {
        this.propertyRightYears = propertyRightYears;
    }
    
    public String getPropertyRightYears() {
        return this.propertyRightYears;
    }
    public void setPropertyCompany(String propertyCompany) {
        this.propertyCompany = propertyCompany;
    }
    
    public String getPropertyCompany() {
        return this.propertyCompany;
    }
    public void setPlanningHouseholds(String planningHouseholds) {
        this.planningHouseholds = planningHouseholds;
    }
    
    public String getPlanningHouseholds() {
        return this.planningHouseholds;
    }
    public void setTimeOfDelivery(String timeOfDelivery) {
        this.timeOfDelivery = timeOfDelivery;
    }
    
    public String getTimeOfDelivery() {
        return this.timeOfDelivery;
    }
    public void setPropertyCosts(String propertyCosts) {
        this.propertyCosts = propertyCosts;
    }
    
    public String getPropertyCosts() {
        return this.propertyCosts;
    }
    public void setInformationSources(String informationSources) {
        this.informationSources = informationSources;
    }
    
    public String getInformationSources() {
        return this.informationSources;
    }
    public void setInfoUpdateTime(Date infoUpdateTime) {
        this.infoUpdateTime = infoUpdateTime;
    }
    
    public Date getInfoUpdateTime() {
        return this.infoUpdateTime;
    }
    
    public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public void setTraffic(String traffic) {
        this.traffic = traffic;
    }
    
    public String getTraffic() {
        return this.traffic;
    }
    public void setKindergarten(String kindergarten) {
        this.kindergarten = kindergarten;
    }
    
    public String getKindergarten() {
        return this.kindergarten;
    }
    public void setSchool(String school) {
        this.school = school;
    }
    
    public String getSchool() {
        return this.school;
    }
    public void setPowerCenter(String powerCenter) {
        this.powerCenter = powerCenter;
    }
    
    public String getPowerCenter() {
        return this.powerCenter;
    }
    public void setHospital(String hospital) {
        this.hospital = hospital;
    }
    
    public String getHospital() {
        return this.hospital;
    }
    public void setBank(String bank) {
        this.bank = bank;
    }
    
    public String getBank() {
        return this.bank;
    }
    public void setPost(String post) {
        this.post = post;
    }
    
    public String getPost() {
        return this.post;
    }
    public void setOther(String other) {
        this.other = other;
    }
    
    public String getOther() {
        return this.other;
    }
    public void setPeiceUnit(String peiceUnit) {
        this.peiceUnit = peiceUnit;
    }
    
    public String getPeiceUnit() {
        return this.peiceUnit;
    }
    public void setHousePriceUnit(String housePriceUnit) {
        this.housePriceUnit = housePriceUnit;
    }
    
    public String getHousePriceUnit() {
        return this.housePriceUnit;
    }

}