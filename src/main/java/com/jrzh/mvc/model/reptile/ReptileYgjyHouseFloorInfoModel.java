package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

//阳光家缘楼层信息

@Entity
@Table(name = "reptile_ygjy_house_floor_infos")
public class ReptileYgjyHouseFloorInfoModel extends GeneralModel {
    
    /**
     * 关联房屋信息id
     */
    @Column(name = "_house_id")
    private String houseId;
    /**
     * 楼层名称
     */
    @Column(name = "_floor_name")
    private String floorName;
    /**
     * 阳光家缘id（用于查询楼层具体信息）
     */
    @Column(name = "_building_id")
    private String buildingId;
    /**
     * 爬取状态
     */
    @Column(name = "_status")
    private String status;
    /**
     * 上海房管局开盘单位ID
     */
    @Column(name = "_sh_start_id")
    private String shStartId;
    /**
     * 深圳房管局开盘单位ID
     */
    @Column(name = "_sz_start_id")
    private String szStartId;
    /**
     * 北京房管局开盘单位ID
     */
    @Column(name = "_bj_start_id")
    private String bjStartId;
    /**
     * 广州房管局开盘单位ID
     */
    @Column(name = "_gz_building_id")
    private String gzBuildingId;

	public String getGzBuildingId() {
		return gzBuildingId;
	}

	public void setGzBuildingId(String gzBuildingId) {
		this.gzBuildingId = gzBuildingId;
	}

	public String getShStartId() {
		return shStartId;
	}

	public void setShStartId(String shStartId) {
		this.shStartId = shStartId;
	}

	public String getSzStartId() {
		return szStartId;
	}

	public void setSzStartId(String szStartId) {
		this.szStartId = szStartId;
	}

	public String getBjStartId() {
		return bjStartId;
	}

	public void setBjStartId(String bjStartId) {
		this.bjStartId = bjStartId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setHouseId(String houseId) {
        this.houseId = houseId;
    }
    
    public String getHouseId() {
        return this.houseId;
    }
    public void setFloorName(String floorName) {
        this.floorName = floorName;
    }
    
    public String getFloorName() {
        return this.floorName;
    }
    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }
    
    public String getBuildingId() {
        return this.buildingId;
    }

}