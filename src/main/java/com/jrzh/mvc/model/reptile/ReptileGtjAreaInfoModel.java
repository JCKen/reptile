package com.jrzh.mvc.model.reptile;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_gtj_area_info")
public class ReptileGtjAreaInfoModel extends GeneralModel {
    
    /**
     * 地块编号
     */
    @Column(name = "_area_no")
    private String areaNo;
    /**
     * 地块位置
     */
    @Column(name = "_address")
    private String address;
    /**
     * 地块用途
     */
    @Column(name = "_use")
    private String use;
    /**
     * 地块面积
     */
    @Column(name = "_acreage")
    private String acreage;
    /**
     * 成交价
     */
    @Column(name = "_price")
    private String price;
    /**
     * 竞得人
     */
    @Column(name = "_buyer")
    private String buyer;

    /**
     * 来源
     */
    @Column(name = "_from")
    private String from;
    
    /**
     * 数据链接
     */
    @Column(name = "_url")
    private String url;
    
    /**
     * 容积率
     */
    @Column(name = "_plot_ratio")
    private String plotRatio;
    
    /**
     * 经度
     */
    @Column(name = "_lnt")
    private String lnt;
    
    /**
     * 纬度
     */
    @Column(name = "_lat")
    private String lat;
    
    /**
     * 城市
     */
    @Column(name = "_city")
    private String city;
    
    /**
     * 出让时间
     */
    @Column(name = "_time")
    private Date time;
    

    
    
    
    public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getTime() {
		return time;
	}

	public String getPlotRatio() {
		return plotRatio;
	}

	public void setPlotRatio(String plotRatio) {
		this.plotRatio = plotRatio;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getLnt() {
		return lnt;
	}

	public void setLnt(String lnt) {
		this.lnt = lnt;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public void setAreaNo(String areaNo) {
        this.areaNo = areaNo;
    }
    
    public String getAreaNo() {
        return this.areaNo;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getAddress() {
        return this.address;
    }
    public void setUse(String use) {
        this.use = use;
    }
    
    public String getUse() {
        return this.use;
    }
    public void setAcreage(String acreage) {
        this.acreage = acreage;
    }
    
    public String getAcreage() {
        return this.acreage;
    }
    public void setPrice(String price) {
        this.price = price;
    }
    
    public String getPrice() {
        return this.price;
    }
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }
    
    public String getBuyer() {
        return this.buyer;
    }

	@Override
	public String toString() {
		return "ReptileGtjAreaInfoModel [areaNo=" + areaNo + ", address="
				+ address + ", use=" + use + ", acreage=" + acreage
				+ ", price=" + price + ", buyer=" + buyer + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((areaNo == null) ? 0 : areaNo.hashCode());
		return result;
	}
	

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReptileGtjAreaInfoModel other = (ReptileGtjAreaInfoModel) obj;
		if (areaNo == null) {
			if (other.areaNo != null)
				return false;
		} else if (!areaNo.equals(other.areaNo))
			return false;
		return true;
	}

	
}