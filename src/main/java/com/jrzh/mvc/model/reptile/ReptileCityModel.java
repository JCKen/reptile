package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_city")
public class ReptileCityModel extends GeneralModel {
    
    /**
     * 城市名称
     */
    @Column(name = "_city_name")
    private String cityName;
    
    /**
     * 经纬度
     */
    @Column(name = "_latitude_longitude")
    private String latitudeLongitude;

    public String getLatitudeLongitude() {
		return latitudeLongitude;
	}

	public void setLatitudeLongitude(String latitudeLongitude) {
		this.latitudeLongitude = latitudeLongitude;
	}

	public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    
    public String getCityName() {
        return this.cityName;
    }

}