package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_correction_set")
public class ReptileCorrectionSetModel extends GeneralModel {
    
    /**
     * 竞品id
     */
    @Column(name = "_house_id")
    private String houseId;
    /**
     * 设置人
     */
    @Column(name = "_user_id")
    private String userId;
    /**
     * 修正项id
     */
    @Column(name = "_correction_id")
    private String correctionId;
    /**
     * 修正值
     */
    @Column(name = "_correction_value")
    private String correctionValue;

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }
    
    public String getHouseId() {
        return this.houseId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getUserId() {
        return this.userId;
    }
    public void setCorrectionId(String correctionId) {
        this.correctionId = correctionId;
    }
    
    public String getCorrectionId() {
        return this.correctionId;
    }
    public void setCorrectionValue(String correctionValue) {
        this.correctionValue = correctionValue;
    }
    
    public String getCorrectionValue() {
        return this.correctionValue;
    }

}