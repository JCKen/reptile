package com.jrzh.mvc.model.reptile;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_ygjy_house_infos")
public class ReptileYgjyHouseInfoModel extends GeneralModel {
    
    /**
     * 项目名称
     */
    @Column(name = "_product_name")
    private String productName;
    /**
     * 详情地址
     */
    @Column(name = "_url")
    private String url;
    /**
     * 详情地址
     */
    @Column(name = "_sale_url")
    private String saleUrl;
    /**
     * 开发商
     */
    @Column(name = "_developer")
    private String developer;
    /**
     * 预售证
     */
    @Column(name = "_pre_sale_permit")
    private String preSalePermit;
    /**
     * 项目地址
     */
    @Column(name = "_project_address")
    private String projectAddress;
    /**
     * 行政区
     */
    @Column(name = "_administrative_area")
    private String administrativeArea;
    /**
     * 占地面积
     */
    @Column(name = "_land_area")
    private Double landArea;
    /**
     * 建筑面积
     */
    @Column(name = "_construction_area")
    private Double constructionArea;
    /**
     * 已售面积
     */
    @Column(name = "_sold_area")
    private Double soldArea;
    /**
     * 未售面积
     */
    @Column(name = "_unsold_area")
    private Double unsoldArea;
    /**
     * 住宅已售
     */
    @Column(name = "_house_sold")
    private Double houseSold;
    /**
     * 住宅未售
     */
    @Column(name = "_house_unsold")
    private Double houseUnsold;
    /**
     * 已售总数
     */
    @Column(name = "_sum_sold")
    private Double sumSold;
    /**
     * 未售总数
     */
    @Column(name = "_sum_unsold")
    private Double sumUnsold;
    /**
     * 数据更新时间
     */
    @Column(name = "_data_update_time")
    private Date dataUpdateTime;
    /**
     * 数据源
     */
    @Column(name = "_source")
    private String source;
    /**
     * 房管局项目ID
     */
    @Column(name = "_project_id")
    private String projectId;
    /**
     * 城市
     */
    @Column(name = "_city")
    private String city;

    public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public void setProductName(String productName) {
        this.productName = productName;
    }
    
    public String getProductName() {
        return this.productName;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getUrl() {
        return this.url;
    }
    public void setSaleUrl(String saleUrl) {
        this.saleUrl = saleUrl;
    }
    
    public String getSaleUrl() {
        return this.saleUrl;
    }
    public void setDeveloper(String developer) {
        this.developer = developer;
    }
    
    public String getDeveloper() {
        return this.developer;
    }
    public void setPreSalePermit(String preSalePermit) {
        this.preSalePermit = preSalePermit;
    }
    
    public String getPreSalePermit() {
        return this.preSalePermit;
    }
    public void setProjectAddress(String projectAddress) {
        this.projectAddress = projectAddress;
    }
    
    public String getProjectAddress() {
        return this.projectAddress;
    }
    public void setAdministrativeArea(String administrativeArea) {
        this.administrativeArea = administrativeArea;
    }
    
    public String getAdministrativeArea() {
        return this.administrativeArea;
    }
    public void setLandArea(Double landArea) {
        this.landArea = landArea;
    }
    
    public Double getLandArea() {
        return this.landArea;
    }
    public void setConstructionArea(Double constructionArea) {
        this.constructionArea = constructionArea;
    }
    
    public Double getConstructionArea() {
        return this.constructionArea;
    }
    public void setSoldArea(Double soldArea) {
        this.soldArea = soldArea;
    }
    
    public Double getSoldArea() {
        return this.soldArea;
    }
    public void setUnsoldArea(Double unsoldArea) {
        this.unsoldArea = unsoldArea;
    }
    
    public Double getUnsoldArea() {
        return this.unsoldArea;
    }
    public void setHouseSold(Double houseSold) {
        this.houseSold = houseSold;
    }
    
    public Double getHouseSold() {
        return this.houseSold;
    }
    public void setHouseUnsold(Double houseUnsold) {
        this.houseUnsold = houseUnsold;
    }
    
    public Double getHouseUnsold() {
        return this.houseUnsold;
    }
    public void setSumSold(Double sumSold) {
        this.sumSold = sumSold;
    }
    
    public Double getSumSold() {
        return this.sumSold;
    }
    public void setSumUnsold(Double sumUnsold) {
        this.sumUnsold = sumUnsold;
    }
    
    public Double getSumUnsold() {
        return this.sumUnsold;
    }
    public void setDataUpdateTime(Date dataUpdateTime) {
        this.dataUpdateTime = dataUpdateTime;
    }
    
    public Date getDataUpdateTime() {
        return this.dataUpdateTime;
    }
    public void setSource(String source) {
        this.source = source;
    }
    
    public String getSource() {
        return this.source;
    }

}