package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_ygjy_house_pre_sale_certificate")
public class ReptileYgjyHousePreSaleCertificateModel extends GeneralModel {
    
    /**
     * 房子ID
     */
    @Column(name = "_house_id")
    private String houseId;
    /**
     * 住宅套数
     */
    @Column(name = "_house_number_set")
    private String houseNumberSet;
    /**
     * 住宅面积
     */
    @Column(name = "_house_area")
    private String houseArea;
    /**
     * 商业套数
     */
    @Column(name = "_business_number_set")
    private String businessNumberSet;
    /**
     * 商业面积
     */
    @Column(name = "_business_area")
    private String businessArea;
    /**
     * 办公套数
     */
    @Column(name = "_office_number_set")
    private String officeNumberSet;
    /**
     * 办公面积
     */
    @Column(name = "_office_area")
    private String officeArea;
    /**
     * 车位套数
     */
    @Column(name = "_parking_number_set")
    private String parkingNumberSet;
    /**
     * 车位面积
     */
    @Column(name = "_parking_area")
    private String parkingArea;
    /**
     * 其他套数
     */
    @Column(name = "_other_number_set")
    private String otherNumberSet;
    /**
     * 其他面积
     */
    @Column(name = "_other_area")
    private String otherArea;
    /**
     * 预字第
     */
    @Column(name = "_pre_word")
    private String preWord;
    /**
     * 预售幢数
     */
    @Column(name = "_pre_sale_number")
    private String preSaleNumber;
    /**
     * 报建屋数
     */
    @Column(name = "_house_built_number")
    private String houseBuiltNumber;
    /**
     * 已建层数
     */
    @Column(name = "_layer_built_number")
    private String layerBuiltNumber;
    /**
     * 本期报建总面积
     */
    @Column(name = "_total_area_report")
    private String totalAreaReport;
    /**
     * 地上面积
     */
    @Column(name = "_above_ground_area")
    private String aboveGroundArea;
    /**
     * 地下面积
     */
    @Column(name = "_underground_area")
    private String undergroundArea;
    /**
     * 本期预售总建筑面积
     */
    @Column(name = "_pre_sale_gross_floor_area")
    private String preSaleGrossFloorArea;
    /**
     * 预售房屋占用土地是否抵押
     */
    @Column(name = "_pre_sale_house_is_mortgaged")
    private String preSaleHouseIsMortgaged;
    /**
     * 预售楼宇配套面积情况
     */
    @Column(name = "_pre_sale_building_supporting_area")
    private String preSaleBuildingSupportingArea;

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }
    
    public String getHouseId() {
        return this.houseId;
    }
    public void setHouseNumberSet(String houseNumberSet) {
        this.houseNumberSet = houseNumberSet;
    }
    
    public String getHouseNumberSet() {
        return this.houseNumberSet;
    }
    public void setHouseArea(String houseArea) {
        this.houseArea = houseArea;
    }
    
    public String getHouseArea() {
        return this.houseArea;
    }
    public void setBusinessNumberSet(String businessNumberSet) {
        this.businessNumberSet = businessNumberSet;
    }
    
    public String getBusinessNumberSet() {
        return this.businessNumberSet;
    }
    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea;
    }
    
    public String getBusinessArea() {
        return this.businessArea;
    }
    public void setOfficeNumberSet(String officeNumberSet) {
        this.officeNumberSet = officeNumberSet;
    }
    
    public String getOfficeNumberSet() {
        return this.officeNumberSet;
    }
    public void setOfficeArea(String officeArea) {
        this.officeArea = officeArea;
    }
    
    public String getOfficeArea() {
        return this.officeArea;
    }
    public void setParkingNumberSet(String parkingNumberSet) {
        this.parkingNumberSet = parkingNumberSet;
    }
    
    public String getParkingNumberSet() {
        return this.parkingNumberSet;
    }
    public void setParkingArea(String parkingArea) {
        this.parkingArea = parkingArea;
    }
    
    public String getParkingArea() {
        return this.parkingArea;
    }
    public void setOtherNumberSet(String otherNumberSet) {
        this.otherNumberSet = otherNumberSet;
    }
    
    public String getOtherNumberSet() {
        return this.otherNumberSet;
    }
    public void setOtherArea(String otherArea) {
        this.otherArea = otherArea;
    }
    
    public String getOtherArea() {
        return this.otherArea;
    }
    public void setPreWord(String preWord) {
        this.preWord = preWord;
    }
    
    public String getPreWord() {
        return this.preWord;
    }
    public void setPreSaleNumber(String preSaleNumber) {
        this.preSaleNumber = preSaleNumber;
    }
    
    public String getPreSaleNumber() {
        return this.preSaleNumber;
    }
    public void setHouseBuiltNumber(String houseBuiltNumber) {
        this.houseBuiltNumber = houseBuiltNumber;
    }
    
    public String getHouseBuiltNumber() {
        return this.houseBuiltNumber;
    }
    public void setLayerBuiltNumber(String layerBuiltNumber) {
        this.layerBuiltNumber = layerBuiltNumber;
    }
    
    public String getLayerBuiltNumber() {
        return this.layerBuiltNumber;
    }
    public void setTotalAreaReport(String totalAreaReport) {
        this.totalAreaReport = totalAreaReport;
    }
    
    public String getTotalAreaReport() {
        return this.totalAreaReport;
    }
    public void setAboveGroundArea(String aboveGroundArea) {
        this.aboveGroundArea = aboveGroundArea;
    }
    
    public String getAboveGroundArea() {
        return this.aboveGroundArea;
    }
    public void setUndergroundArea(String undergroundArea) {
        this.undergroundArea = undergroundArea;
    }
    
    public String getUndergroundArea() {
        return this.undergroundArea;
    }
    public void setPreSaleGrossFloorArea(String preSaleGrossFloorArea) {
        this.preSaleGrossFloorArea = preSaleGrossFloorArea;
    }
    
    public String getPreSaleGrossFloorArea() {
        return this.preSaleGrossFloorArea;
    }
    public void setPreSaleHouseIsMortgaged(String preSaleHouseIsMortgaged) {
        this.preSaleHouseIsMortgaged = preSaleHouseIsMortgaged;
    }
    
    public String getPreSaleHouseIsMortgaged() {
        return this.preSaleHouseIsMortgaged;
    }
    public void setPreSaleBuildingSupportingArea(String preSaleBuildingSupportingArea) {
        this.preSaleBuildingSupportingArea = preSaleBuildingSupportingArea;
    }
    
    public String getPreSaleBuildingSupportingArea() {
        return this.preSaleBuildingSupportingArea;
    }

}