package com.jrzh.mvc.model.reptile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jrzh.framework.base.model.GeneralModel;

@Entity
@Table(name = "reptile_template")
public class ReptileTemplateModel extends GeneralModel {
    
    /**
     * 类型（市调或者模板，生成pdf模板）
     */
    @Column(name = "_type")
    private String type;
    /**
     * 模板名称
     */
    @Column(name = "_name")
    private String name;
    /**
     * 用户ID
     */
    @Column(name = "_user_id")
    private String userId;
    /**
     * 房屋类型
     */
    @Column(name = "_house_type")
    private String houseType;
    
    /**
     * 文件地址
     */
    @Column(name = "_file_url")
    private String fileUrl;
    
    /**
     * 竞品地图base64编码
     * @return
     */
    @Column(name = "_img_base64")
    private String imgBase64;

    public String getImgBase64() {
		return imgBase64;
	}

	public void setImgBase64(String imgBase64) {
		this.imgBase64 = imgBase64;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getHouseType() {
		return houseType;
	}

	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setType(String type) {
        this.type = type;
    }
    
    public String getType() {
        return this.type;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }

}