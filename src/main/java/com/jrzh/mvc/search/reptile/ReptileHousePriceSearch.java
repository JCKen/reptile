package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileHousePriceSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 房价
     */
    private String equalHousePrice;
    
    /**
     * Like 房价
     */
    private String likeHousePrice;
    
     /**
     * In 房价 英文逗号分隔
     */
    private String inHousePrices;
    
    /**
     * Equal 房子ID
     */
    private String equalHouseId;
    
    /**
     * Like 房子ID
     */
    private String likeHouseId;
    
     /**
     * In 房子ID 英文逗号分隔
     */
    private String inHouseIds;
    
    /**
     * Equal 房价单位(1. 元/㎡ 2. 万/套)
     */
    private String equalHousePriceUnit;
    
    /**
     * Like 房价单位(1. 元/㎡ 2. 万/套)
     */
    private String likeHousePriceUnit;
    
     /**
     * In 房价单位(1. 元/㎡ 2. 万/套) 英文逗号分隔
     */
    private String inHousePriceUnits;

    /**
    * 分组字段
    */
   private String groupField;
   

    public String getGroupField() {
		return groupField;
	}

	public void setGroupField(String groupField) {
		this.groupField = groupField;
	}

	public void setEqualHousePrice(String equalHousePrice) {
        this.equalHousePrice = equalHousePrice;
    }
    
    public String getEqualHousePrice() {
        return this.equalHousePrice;
    }
    
    public void setLikeHousePrice(String likeHousePrice) {
        this.likeHousePrice = likeHousePrice;
    }
    
    public String getLikeHousePrice() {
        return this.likeHousePrice;
    }
    
    public void setInHousePrices(String inHousePrices) {
        this.inHousePrices = inHousePrices;
    }
    
    public String getInHousePrices() {
        return this.inHousePrices;
    }
    public void setEqualHouseId(String equalHouseId) {
        this.equalHouseId = equalHouseId;
    }
    
    public String getEqualHouseId() {
        return this.equalHouseId;
    }
    
    public void setLikeHouseId(String likeHouseId) {
        this.likeHouseId = likeHouseId;
    }
    
    public String getLikeHouseId() {
        return this.likeHouseId;
    }
    
    public void setInHouseIds(String inHouseIds) {
        this.inHouseIds = inHouseIds;
    }
    
    public String getInHouseIds() {
        return this.inHouseIds;
    }
    public void setEqualHousePriceUnit(String equalHousePriceUnit) {
        this.equalHousePriceUnit = equalHousePriceUnit;
    }
    
    public String getEqualHousePriceUnit() {
        return this.equalHousePriceUnit;
    }
    
    public void setLikeHousePriceUnit(String likeHousePriceUnit) {
        this.likeHousePriceUnit = likeHousePriceUnit;
    }
    
    public String getLikeHousePriceUnit() {
        return this.likeHousePriceUnit;
    }
    
    public void setInHousePriceUnits(String inHousePriceUnits) {
        this.inHousePriceUnits = inHousePriceUnits;
    }
    
    public String getInHousePriceUnits() {
        return this.inHousePriceUnits;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalHousePrice)){
			criterias.add(Restrictions.eq("housePrice", equalHousePrice));
		}
	    if(StringUtils.isNotBlank(likeHousePrice)){
			criterias.add(Restrictions.allLike("housePrice", likeHousePrice));
		}
		 if(StringUtils.isNotBlank(inHousePrices)){
			criterias.add(Restrictions.in("housePrice", StringUtils.split(inHousePrices, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseId)){
			criterias.add(Restrictions.eq("houseId", equalHouseId));
		}
	    if(StringUtils.isNotBlank(likeHouseId)){
			criterias.add(Restrictions.allLike("houseId", likeHouseId));
		}
		 if(StringUtils.isNotBlank(inHouseIds)){
			criterias.add(Restrictions.in("houseId", StringUtils.split(inHouseIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalHousePriceUnit)){
			criterias.add(Restrictions.eq("housePriceUnit", equalHousePriceUnit));
		}
	    if(StringUtils.isNotBlank(likeHousePriceUnit)){
			criterias.add(Restrictions.allLike("housePriceUnit", likeHousePriceUnit));
		}
		 if(StringUtils.isNotBlank(inHousePriceUnits)){
			criterias.add(Restrictions.in("housePriceUnit", StringUtils.split(inHousePriceUnits, ",")));
		}
		 if(StringUtils.isNotBlank(groupField)){
			 criterias.addGroup(groupField);
		 }
	}
}