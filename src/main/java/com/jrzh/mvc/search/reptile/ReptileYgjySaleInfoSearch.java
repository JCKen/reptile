package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileYgjySaleInfoSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 区域名称
     */
    private String equalArea;
    
    /**
     * Like 区域名称
     */
    private String likeArea;
    
     /**
     * In 区域名称 英文逗号分隔
     */
    private String inAreas;
    
    /**
     * Equal 区域名称
     */
    private String equalSetNumber;
    
    /**
     * Like 区域名称
     */
    private String likeSetNumber;
    
     /**
     * In 区域名称 英文逗号分隔
     */
    private String inSetNumbers;
    
    /**
     * Equal 面积
     */
    private String equalAcreage;
    
    /**
     * Like 面积
     */
    private String likeAcreage;
    
     /**
     * In 面积 英文逗号分隔
     */
    private String inAcreages;
    
    /**
     * Equal 类型
     */
    private String equalType;
    
    /**
     * Like 类型
     */
    private String likeType;
    
     /**
     * In 类型 英文逗号分隔
     */
    private String inTypes;
    
    /**
     * Equal 数据源
     */
    private String equalSource;
    
    /**
     * Like 数据源
     */
    private String likeSource;
    
     /**
     * In 数据源 英文逗号分隔
     */
    private String inSources;
    
    /**
     * Equal 数据更新时间
     */
    private String equalDataUpdateTime;
    
    /**
     * Like 数据更新时间
     */
    private String likeDataUpdateTime;
    
     /**
     * In 数据更新时间 英文逗号分隔
     */
    private String inDataUpdateTimes;
    /**
     * eq 销售状态
     */
    private String eqSaleType;
    

	public String getEqSaleType() {
		return eqSaleType;
	}

	public void setEqSaleType(String eqSaleType) {
		this.eqSaleType = eqSaleType;
	}

	public void setEqualArea(String equalArea) {
        this.equalArea = equalArea;
    }
    
    public String getEqualArea() {
        return this.equalArea;
    }
    
    public void setLikeArea(String likeArea) {
        this.likeArea = likeArea;
    }
    
    public String getLikeArea() {
        return this.likeArea;
    }
    
    public void setInAreas(String inAreas) {
        this.inAreas = inAreas;
    }
    
    public String getInAreas() {
        return this.inAreas;
    }
    public void setEqualSetNumber(String equalSetNumber) {
        this.equalSetNumber = equalSetNumber;
    }
    
    public String getEqualSetNumber() {
        return this.equalSetNumber;
    }
    
    public void setLikeSetNumber(String likeSetNumber) {
        this.likeSetNumber = likeSetNumber;
    }
    
    public String getLikeSetNumber() {
        return this.likeSetNumber;
    }
    
    public void setInSetNumbers(String inSetNumbers) {
        this.inSetNumbers = inSetNumbers;
    }
    
    public String getInSetNumbers() {
        return this.inSetNumbers;
    }
    public void setEqualAcreage(String equalAcreage) {
        this.equalAcreage = equalAcreage;
    }
    
    public String getEqualAcreage() {
        return this.equalAcreage;
    }
    
    public void setLikeAcreage(String likeAcreage) {
        this.likeAcreage = likeAcreage;
    }
    
    public String getLikeAcreage() {
        return this.likeAcreage;
    }
    
    public void setInAcreages(String inAcreages) {
        this.inAcreages = inAcreages;
    }
    
    public String getInAcreages() {
        return this.inAcreages;
    }
    public void setEqualType(String equalType) {
        this.equalType = equalType;
    }
    
    public String getEqualType() {
        return this.equalType;
    }
    
    public void setLikeType(String likeType) {
        this.likeType = likeType;
    }
    
    public String getLikeType() {
        return this.likeType;
    }
    
    public void setInTypes(String inTypes) {
        this.inTypes = inTypes;
    }
    
    public String getInTypes() {
        return this.inTypes;
    }
    public void setEqualSource(String equalSource) {
        this.equalSource = equalSource;
    }
    
    public String getEqualSource() {
        return this.equalSource;
    }
    
    public void setLikeSource(String likeSource) {
        this.likeSource = likeSource;
    }
    
    public String getLikeSource() {
        return this.likeSource;
    }
    
    public void setInSources(String inSources) {
        this.inSources = inSources;
    }
    
    public String getInSources() {
        return this.inSources;
    }
    public void setEqualDataUpdateTime(String equalDataUpdateTime) {
        this.equalDataUpdateTime = equalDataUpdateTime;
    }
    
    public String getEqualDataUpdateTime() {
        return this.equalDataUpdateTime;
    }
    
    public void setLikeDataUpdateTime(String likeDataUpdateTime) {
        this.likeDataUpdateTime = likeDataUpdateTime;
    }
    
    public String getLikeDataUpdateTime() {
        return this.likeDataUpdateTime;
    }
    
    public void setInDataUpdateTimes(String inDataUpdateTimes) {
        this.inDataUpdateTimes = inDataUpdateTimes;
    }
    
    public String getInDataUpdateTimes() {
        return this.inDataUpdateTimes;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
		
		if(StringUtils.isNotBlank(eqSaleType)){
			criterias.add(Restrictions.eq("saleType", eqSaleType));
		}
		
	    if(StringUtils.isNotBlank(equalArea)){
			criterias.add(Restrictions.eq("area", equalArea));
		}
	    if(StringUtils.isNotBlank(likeArea)){
			criterias.add(Restrictions.allLike("area", likeArea));
		}
		 if(StringUtils.isNotBlank(inAreas)){
			criterias.add(Restrictions.in("area", StringUtils.split(inAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalSetNumber)){
			criterias.add(Restrictions.eq("setNumber", equalSetNumber));
		}
	    if(StringUtils.isNotBlank(likeSetNumber)){
			criterias.add(Restrictions.allLike("setNumber", likeSetNumber));
		}
		 if(StringUtils.isNotBlank(inSetNumbers)){
			criterias.add(Restrictions.in("setNumber", StringUtils.split(inSetNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalAcreage)){
			criterias.add(Restrictions.eq("acreage", equalAcreage));
		}
	    if(StringUtils.isNotBlank(likeAcreage)){
			criterias.add(Restrictions.allLike("acreage", likeAcreage));
		}
		 if(StringUtils.isNotBlank(inAcreages)){
			criterias.add(Restrictions.in("acreage", StringUtils.split(inAcreages, ",")));
		}
	    if(StringUtils.isNotBlank(equalType)){
			criterias.add(Restrictions.eq("type", equalType));
		}
	    if(StringUtils.isNotBlank(likeType)){
			criterias.add(Restrictions.allLike("type", likeType));
		}
		 if(StringUtils.isNotBlank(inTypes)){
			criterias.add(Restrictions.in("type", StringUtils.split(inTypes, ",")));
		}
	    if(StringUtils.isNotBlank(equalSource)){
			criterias.add(Restrictions.eq("source", equalSource));
		}
	    if(StringUtils.isNotBlank(likeSource)){
			criterias.add(Restrictions.allLike("source", likeSource));
		}
		 if(StringUtils.isNotBlank(inSources)){
			criterias.add(Restrictions.in("source", StringUtils.split(inSources, ",")));
		}
	    if(StringUtils.isNotBlank(equalDataUpdateTime)){
			criterias.add(Restrictions.eq("dataUpdateTime", equalDataUpdateTime));
		}
	    if(StringUtils.isNotBlank(likeDataUpdateTime)){
			criterias.add(Restrictions.allLike("dataUpdateTime", likeDataUpdateTime));
		}
		 if(StringUtils.isNotBlank(inDataUpdateTimes)){
			criterias.add(Restrictions.in("dataUpdateTime", StringUtils.split(inDataUpdateTimes, ",")));
		}
	}
}