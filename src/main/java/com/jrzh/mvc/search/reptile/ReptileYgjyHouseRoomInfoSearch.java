package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileYgjyHouseRoomInfoSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 关联房源id
     */
    private String equalHouseId;
    
    /**
     * Like 关联房源id
     */
    private String likeHouseId;
    
     /**
     * In 关联房源id 英文逗号分隔
     */
    private String inHouseIds;
    
    /**
     * Equal 关联楼层栋id
     */
    private String equalFloorId;
    
    /**
     * Like 关联楼层栋id
     */
    private String likeFloorId;
    
     /**
     * In 关联楼层栋id 英文逗号分隔
     */
    private String inFloorIds;
    
    /**
     * Equal 房号
     */
    private String equalRoomNumber;
    
    /**
     * Like 房号
     */
    private String likeRoomNumber;
    
     /**
     * In 房号 英文逗号分隔
     */
    private String inRoomNumbers;
    
    /**
     * Equal 房型
     */
    private String equalRoomType;
    
    /**
     * Like 房型
     */
    private String likeRoomType;
    
     /**
     * In 房型 英文逗号分隔
     */
    private String inRoomTypes;
    
    /**
     * Equal 房间总面积
     */
    private String equalRoomTotalArea;
    
    /**
     * Like 房间总面积
     */
    private String likeRoomTotalArea;
    
     /**
     * In 房间总面积 英文逗号分隔
     */
    private String inRoomTotalAreas;
    
    /**
     * Equal 户型
     */
    private String equalRoomApartment;
    
    /**
     * Like 户型
     */
    private String likeRoomApartment;
    
     /**
     * In 户型 英文逗号分隔
     */
    private String inRoomApartments;
    
    /**
     * Equal 房号
     */
    private String equalRoomStatus;
    
    /**
     * Like 房号
     */
    private String likeRoomStatus;
    
     /**
     * In 房号 英文逗号分隔
     */
    private String inRoomStatuss;
    
    /**
     * Equal 抵押状态
     */
    private String equalRoomMortgage;
    
    /**
     * Like 抵押状态
     */
    private String likeRoomMortgage;
    
     /**
     * In 抵押状态 英文逗号分隔
     */
    private String inRoomMortgages;
    
    /**
     * Equal 查封状态
     */
    private String equalRoomSealingState;
    
    /**
     * Like 查封状态
     */
    private String likeRoomSealingState;
    
     /**
     * In 查封状态 英文逗号分隔
     */
    private String inRoomSealingStates;
    
    private String[] houseFloorIds;
    
    public String[] getHouseFloorIds() {
		return houseFloorIds;
	}

	public void setHouseFloorIds(String[] houseFloorIds) {
		this.houseFloorIds = houseFloorIds;
	}

	public void setEqualHouseId(String equalHouseId) {
        this.equalHouseId = equalHouseId;
    }
    
    public String getEqualHouseId() {
        return this.equalHouseId;
    }
    
    public void setLikeHouseId(String likeHouseId) {
        this.likeHouseId = likeHouseId;
    }
    
    public String getLikeHouseId() {
        return this.likeHouseId;
    }
    
    public void setInHouseIds(String inHouseIds) {
        this.inHouseIds = inHouseIds;
    }
    
    public String getInHouseIds() {
        return this.inHouseIds;
    }
    public void setEqualFloorId(String equalFloorId) {
        this.equalFloorId = equalFloorId;
    }
    
    public String getEqualFloorId() {
        return this.equalFloorId;
    }
    
    public void setLikeFloorId(String likeFloorId) {
        this.likeFloorId = likeFloorId;
    }
    
    public String getLikeFloorId() {
        return this.likeFloorId;
    }
    
    public void setInFloorIds(String inFloorIds) {
        this.inFloorIds = inFloorIds;
    }
    
    public String getInFloorIds() {
        return this.inFloorIds;
    }
    public void setEqualRoomNumber(String equalRoomNumber) {
        this.equalRoomNumber = equalRoomNumber;
    }
    
    public String getEqualRoomNumber() {
        return this.equalRoomNumber;
    }
    
    public void setLikeRoomNumber(String likeRoomNumber) {
        this.likeRoomNumber = likeRoomNumber;
    }
    
    public String getLikeRoomNumber() {
        return this.likeRoomNumber;
    }
    
    public void setInRoomNumbers(String inRoomNumbers) {
        this.inRoomNumbers = inRoomNumbers;
    }
    
    public String getInRoomNumbers() {
        return this.inRoomNumbers;
    }
    public void setEqualRoomType(String equalRoomType) {
        this.equalRoomType = equalRoomType;
    }
    
    public String getEqualRoomType() {
        return this.equalRoomType;
    }
    
    public void setLikeRoomType(String likeRoomType) {
        this.likeRoomType = likeRoomType;
    }
    
    public String getLikeRoomType() {
        return this.likeRoomType;
    }
    
    public void setInRoomTypes(String inRoomTypes) {
        this.inRoomTypes = inRoomTypes;
    }
    
    public String getInRoomTypes() {
        return this.inRoomTypes;
    }
    public void setEqualRoomTotalArea(String equalRoomTotalArea) {
        this.equalRoomTotalArea = equalRoomTotalArea;
    }
    
    public String getEqualRoomTotalArea() {
        return this.equalRoomTotalArea;
    }
    
    public void setLikeRoomTotalArea(String likeRoomTotalArea) {
        this.likeRoomTotalArea = likeRoomTotalArea;
    }
    
    public String getLikeRoomTotalArea() {
        return this.likeRoomTotalArea;
    }
    
    public void setInRoomTotalAreas(String inRoomTotalAreas) {
        this.inRoomTotalAreas = inRoomTotalAreas;
    }
    
    public String getInRoomTotalAreas() {
        return this.inRoomTotalAreas;
    }
    public void setEqualRoomApartment(String equalRoomApartment) {
        this.equalRoomApartment = equalRoomApartment;
    }
    
    public String getEqualRoomApartment() {
        return this.equalRoomApartment;
    }
    
    public void setLikeRoomApartment(String likeRoomApartment) {
        this.likeRoomApartment = likeRoomApartment;
    }
    
    public String getLikeRoomApartment() {
        return this.likeRoomApartment;
    }
    
    public void setInRoomApartments(String inRoomApartments) {
        this.inRoomApartments = inRoomApartments;
    }
    
    public String getInRoomApartments() {
        return this.inRoomApartments;
    }
    public void setEqualRoomStatus(String equalRoomStatus) {
        this.equalRoomStatus = equalRoomStatus;
    }
    
    public String getEqualRoomStatus() {
        return this.equalRoomStatus;
    }
    
    public void setLikeRoomStatus(String likeRoomStatus) {
        this.likeRoomStatus = likeRoomStatus;
    }
    
    public String getLikeRoomStatus() {
        return this.likeRoomStatus;
    }
    
    public void setInRoomStatuss(String inRoomStatuss) {
        this.inRoomStatuss = inRoomStatuss;
    }
    
    public String getInRoomStatuss() {
        return this.inRoomStatuss;
    }
    public void setEqualRoomMortgage(String equalRoomMortgage) {
        this.equalRoomMortgage = equalRoomMortgage;
    }
    
    public String getEqualRoomMortgage() {
        return this.equalRoomMortgage;
    }
    
    public void setLikeRoomMortgage(String likeRoomMortgage) {
        this.likeRoomMortgage = likeRoomMortgage;
    }
    
    public String getLikeRoomMortgage() {
        return this.likeRoomMortgage;
    }
    
    public void setInRoomMortgages(String inRoomMortgages) {
        this.inRoomMortgages = inRoomMortgages;
    }
    
    public String getInRoomMortgages() {
        return this.inRoomMortgages;
    }
    public void setEqualRoomSealingState(String equalRoomSealingState) {
        this.equalRoomSealingState = equalRoomSealingState;
    }
    
    public String getEqualRoomSealingState() {
        return this.equalRoomSealingState;
    }
    
    public void setLikeRoomSealingState(String likeRoomSealingState) {
        this.likeRoomSealingState = likeRoomSealingState;
    }
    
    public String getLikeRoomSealingState() {
        return this.likeRoomSealingState;
    }
    
    public void setInRoomSealingStates(String inRoomSealingStates) {
        this.inRoomSealingStates = inRoomSealingStates;
    }
    
    public String getInRoomSealingStates() {
        return this.inRoomSealingStates;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalHouseId)){
			criterias.add(Restrictions.eq("houseId", equalHouseId));
		}
	    if(StringUtils.isNotBlank(likeHouseId)){
			criterias.add(Restrictions.allLike("houseId", likeHouseId));
		}
		 if(StringUtils.isNotBlank(inHouseIds)){
			criterias.add(Restrictions.in("houseId", StringUtils.split(inHouseIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalFloorId)){
			criterias.add(Restrictions.eq("floorId", equalFloorId));
		}
	    if(StringUtils.isNotBlank(likeFloorId)){
			criterias.add(Restrictions.allLike("floorId", likeFloorId));
		}
		 if(StringUtils.isNotBlank(inFloorIds)){
			criterias.add(Restrictions.in("floorId", StringUtils.split(inFloorIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalRoomNumber)){
			criterias.add(Restrictions.eq("roomNumber", equalRoomNumber));
		}
	    if(StringUtils.isNotBlank(likeRoomNumber)){
			criterias.add(Restrictions.allLike("roomNumber", likeRoomNumber));
		}
		 if(StringUtils.isNotBlank(inRoomNumbers)){
			criterias.add(Restrictions.in("roomNumber", StringUtils.split(inRoomNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalRoomType)){
			criterias.add(Restrictions.eq("roomType", equalRoomType));
		}
	    if(StringUtils.isNotBlank(likeRoomType)){
			criterias.add(Restrictions.allLike("roomType", likeRoomType));
		}
		 if(StringUtils.isNotBlank(inRoomTypes)){
			criterias.add(Restrictions.in("roomType", StringUtils.split(inRoomTypes, ",")));
		}
	    if(StringUtils.isNotBlank(equalRoomTotalArea)){
			criterias.add(Restrictions.eq("roomTotalArea", equalRoomTotalArea));
		}
	    if(StringUtils.isNotBlank(likeRoomTotalArea)){
			criterias.add(Restrictions.allLike("roomTotalArea", likeRoomTotalArea));
		}
		 if(StringUtils.isNotBlank(inRoomTotalAreas)){
			criterias.add(Restrictions.in("roomTotalArea", StringUtils.split(inRoomTotalAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalRoomApartment)){
			criterias.add(Restrictions.eq("roomApartment", equalRoomApartment));
		}
	    if(StringUtils.isNotBlank(likeRoomApartment)){
			criterias.add(Restrictions.allLike("roomApartment", likeRoomApartment));
		}
		 if(StringUtils.isNotBlank(inRoomApartments)){
			criterias.add(Restrictions.in("roomApartment", StringUtils.split(inRoomApartments, ",")));
		}
	    if(StringUtils.isNotBlank(equalRoomStatus)){
			criterias.add(Restrictions.eq("roomStatus", equalRoomStatus));
		}
	    if(StringUtils.isNotBlank(likeRoomStatus)){
			criterias.add(Restrictions.allLike("roomStatus", likeRoomStatus));
		}
		 if(StringUtils.isNotBlank(inRoomStatuss)){
			criterias.add(Restrictions.in("roomStatus", StringUtils.split(inRoomStatuss, ",")));
		}
	    if(StringUtils.isNotBlank(equalRoomMortgage)){
			criterias.add(Restrictions.eq("roomMortgage", equalRoomMortgage));
		}
	    if(StringUtils.isNotBlank(likeRoomMortgage)){
			criterias.add(Restrictions.allLike("roomMortgage", likeRoomMortgage));
		}
		 if(StringUtils.isNotBlank(inRoomMortgages)){
			criterias.add(Restrictions.in("roomMortgage", StringUtils.split(inRoomMortgages, ",")));
		}
	    if(StringUtils.isNotBlank(equalRoomSealingState)){
			criterias.add(Restrictions.eq("roomSealingState", equalRoomSealingState));
		}
	    if(StringUtils.isNotBlank(likeRoomSealingState)){
			criterias.add(Restrictions.allLike("roomSealingState", likeRoomSealingState));
		}
		if(StringUtils.isNotBlank(inRoomSealingStates)){
			criterias.add(Restrictions.in("roomSealingState", StringUtils.split(inRoomSealingStates, ",")));
		}
		if(null != houseFloorIds && houseFloorIds.length > 0 ){
			criterias.add(Restrictions.in("floorId",houseFloorIds));
		}
	}
}