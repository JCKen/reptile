package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileAreaSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 区域名称
     */
    private String equalAreaName;
    
    /**
     * Like 区域名称
     */
    private String likeAreaName;
    
     /**
     * In 区域名称 英文逗号分隔
     */
    private String inAreaNames;
    
    /**
     * Equal 所属城市
     */
    private String equalOwnCity;
    
    /**
     * Like 所属城市
     */
    private String likeOwnCity;
    
     /**
     * In 所属城市 英文逗号分隔
     */
    private String inOwnCitys;
    
    /**
     * Equal 经纬度
     */
    private String equalLatitudeLongitude;
    
    /**
     * Like 经纬度
     */
    private String likeLatitudeLongitude;
    
     /**
     * In 经纬度 英文逗号分隔
     */
    private String inLatitudeLongitudes;
    

    public void setEqualAreaName(String equalAreaName) {
        this.equalAreaName = equalAreaName;
    }
    
    public String getEqualAreaName() {
        return this.equalAreaName;
    }
    
    public void setLikeAreaName(String likeAreaName) {
        this.likeAreaName = likeAreaName;
    }
    
    public String getLikeAreaName() {
        return this.likeAreaName;
    }
    
    public void setInAreaNames(String inAreaNames) {
        this.inAreaNames = inAreaNames;
    }
    
    public String getInAreaNames() {
        return this.inAreaNames;
    }
    public void setEqualOwnCity(String equalOwnCity) {
        this.equalOwnCity = equalOwnCity;
    }
    
    public String getEqualOwnCity() {
        return this.equalOwnCity;
    }
    
    public void setLikeOwnCity(String likeOwnCity) {
        this.likeOwnCity = likeOwnCity;
    }
    
    public String getLikeOwnCity() {
        return this.likeOwnCity;
    }
    
    public void setInOwnCitys(String inOwnCitys) {
        this.inOwnCitys = inOwnCitys;
    }
    
    public String getInOwnCitys() {
        return this.inOwnCitys;
    }
    public void setEqualLatitudeLongitude(String equalLatitudeLongitude) {
        this.equalLatitudeLongitude = equalLatitudeLongitude;
    }
    
    public String getEqualLatitudeLongitude() {
        return this.equalLatitudeLongitude;
    }
    
    public void setLikeLatitudeLongitude(String likeLatitudeLongitude) {
        this.likeLatitudeLongitude = likeLatitudeLongitude;
    }
    
    public String getLikeLatitudeLongitude() {
        return this.likeLatitudeLongitude;
    }
    
    public void setInLatitudeLongitudes(String inLatitudeLongitudes) {
        this.inLatitudeLongitudes = inLatitudeLongitudes;
    }
    
    public String getInLatitudeLongitudes() {
        return this.inLatitudeLongitudes;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalAreaName)){
			criterias.add(Restrictions.eq("areaName", equalAreaName));
		}
	    if(StringUtils.isNotBlank(likeAreaName)){
			criterias.add(Restrictions.allLike("areaName", likeAreaName));
		}
		 if(StringUtils.isNotBlank(inAreaNames)){
			criterias.add(Restrictions.in("areaName", StringUtils.split(inAreaNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalOwnCity)){
			criterias.add(Restrictions.eq("ownCity", equalOwnCity));
		}
	    if(StringUtils.isNotBlank(likeOwnCity)){
			criterias.add(Restrictions.allLike("ownCity", likeOwnCity));
		}
		 if(StringUtils.isNotBlank(inOwnCitys)){
			criterias.add(Restrictions.in("ownCity", StringUtils.split(inOwnCitys, ",")));
		}
	    if(StringUtils.isNotBlank(equalLatitudeLongitude)){
			criterias.add(Restrictions.eq("latitudeLongitude", equalLatitudeLongitude));
		}
	    if(StringUtils.isNotBlank(likeLatitudeLongitude)){
			criterias.add(Restrictions.allLike("latitudeLongitude", likeLatitudeLongitude));
		}
		 if(StringUtils.isNotBlank(inLatitudeLongitudes)){
			criterias.add(Restrictions.in("latitudeLongitude", StringUtils.split(inLatitudeLongitudes, ",")));
		}
	}
}