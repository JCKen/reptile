package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileReportConfigSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 用户ID
     */
    private String equalUserId;
    
    /**
     * Like 用户ID
     */
    private String likeUserId;
    
     /**
     * In 用户ID 英文逗号分隔
     */
    private String inUserIds;
    
    /**
     * Equal 父节点ID
     */
    private String equalPid;
    
    /**
     * Like 父节点ID
     */
    private String likePid;
    
     /**
     * In 父节点ID 英文逗号分隔
     */
    private String inPids;
    
    /**
     * Equal 页面名称
     */
    private String equalPageName;
    
    /**
     * Like 页面名称
     */
    private String likePageName;
    
     /**
     * In 页面名称 英文逗号分隔
     */
    private String inPageNames;
    
    /**
     * Equal 页面URL
     */
    private String equalPageUrl;
    
    /**
     * Like 页面URL
     */
    private String likePageUrl;
    
     /**
     * In 页面URL 英文逗号分隔
     */
    private String inPageUrls;
    
    /**
     * Equal 页面是否显示(0.隐藏，1.显示)
     */
    private String equalIsCheck;
    
    /**
     * Like 页面是否显示(0.隐藏，1.显示)
     */
    private String likeIsCheck;
    
     /**
     * In 页面是否显示(0.隐藏，1.显示) 英文逗号分隔
     */
    private String inIsChecks;
    
    /**
     * Equal 排序顺序
     */
    private String equalOrderBy;
    
    /**
     * Like 排序顺序
     */
    private String likeOrderBy;
    
     /**
     * In 排序顺序 英文逗号分隔
     */
    private String inOrderBys;
    
    
    /**
     *为空
     */
    private String isNull;
    

    public String getIsNull() {
		return isNull;
	}

	public void setIsNull(String isNull) {
		this.isNull = isNull;
	}

	public void setEqualUserId(String equalUserId) {
        this.equalUserId = equalUserId;
    }
    
    public String getEqualUserId() {
        return this.equalUserId;
    }
    
    public void setLikeUserId(String likeUserId) {
        this.likeUserId = likeUserId;
    }
    
    public String getLikeUserId() {
        return this.likeUserId;
    }
    
    public void setInUserIds(String inUserIds) {
        this.inUserIds = inUserIds;
    }
    
    public String getInUserIds() {
        return this.inUserIds;
    }
    public void setEqualPid(String equalPid) {
        this.equalPid = equalPid;
    }
    
    public String getEqualPid() {
        return this.equalPid;
    }
    
    public void setLikePid(String likePid) {
        this.likePid = likePid;
    }
    
    public String getLikePid() {
        return this.likePid;
    }
    
    public void setInPids(String inPids) {
        this.inPids = inPids;
    }
    
    public String getInPids() {
        return this.inPids;
    }
    public void setEqualPageName(String equalPageName) {
        this.equalPageName = equalPageName;
    }
    
    public String getEqualPageName() {
        return this.equalPageName;
    }
    
    public void setLikePageName(String likePageName) {
        this.likePageName = likePageName;
    }
    
    public String getLikePageName() {
        return this.likePageName;
    }
    
    public void setInPageNames(String inPageNames) {
        this.inPageNames = inPageNames;
    }
    
    public String getInPageNames() {
        return this.inPageNames;
    }
    public void setEqualPageUrl(String equalPageUrl) {
        this.equalPageUrl = equalPageUrl;
    }
    
    public String getEqualPageUrl() {
        return this.equalPageUrl;
    }
    
    public void setLikePageUrl(String likePageUrl) {
        this.likePageUrl = likePageUrl;
    }
    
    public String getLikePageUrl() {
        return this.likePageUrl;
    }
    
    public void setInPageUrls(String inPageUrls) {
        this.inPageUrls = inPageUrls;
    }
    
    public String getInPageUrls() {
        return this.inPageUrls;
    }
    public void setEqualIsCheck(String equalIsCheck) {
        this.equalIsCheck = equalIsCheck;
    }
    
    public String getEqualIsCheck() {
        return this.equalIsCheck;
    }
    
    public void setLikeIsCheck(String likeIsCheck) {
        this.likeIsCheck = likeIsCheck;
    }
    
    public String getLikeIsCheck() {
        return this.likeIsCheck;
    }
    
    public void setInIsChecks(String inIsChecks) {
        this.inIsChecks = inIsChecks;
    }
    
    public String getInIsChecks() {
        return this.inIsChecks;
    }
    public void setEqualOrderBy(String equalOrderBy) {
        this.equalOrderBy = equalOrderBy;
    }
    
    public String getEqualOrderBy() {
        return this.equalOrderBy;
    }
    
    public void setLikeOrderBy(String likeOrderBy) {
        this.likeOrderBy = likeOrderBy;
    }
    
    public String getLikeOrderBy() {
        return this.likeOrderBy;
    }
    
    public void setInOrderBys(String inOrderBys) {
        this.inOrderBys = inOrderBys;
    }
    
    public String getInOrderBys() {
        return this.inOrderBys;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
		if(StringUtils.isNotBlank(isNull)){
			criterias.add(Restrictions.isNull(isNull));
		}
		
	    if(StringUtils.isNotBlank(equalUserId)){
			criterias.add(Restrictions.eq("userId", equalUserId));
		}
	    if(StringUtils.isNotBlank(likeUserId)){
			criterias.add(Restrictions.allLike("userId", likeUserId));
		}
		 if(StringUtils.isNotBlank(inUserIds)){
			criterias.add(Restrictions.in("userId", StringUtils.split(inUserIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalPid)){
			criterias.add(Restrictions.eq("pid", equalPid));
		}
	    if(StringUtils.isNotBlank(likePid)){
			criterias.add(Restrictions.allLike("pid", likePid));
		}
		 if(StringUtils.isNotBlank(inPids)){
			criterias.add(Restrictions.in("pid", StringUtils.split(inPids, ",")));
		}
	    if(StringUtils.isNotBlank(equalPageName)){
			criterias.add(Restrictions.eq("pageName", equalPageName));
		}
	    if(StringUtils.isNotBlank(likePageName)){
			criterias.add(Restrictions.allLike("pageName", likePageName));
		}
		 if(StringUtils.isNotBlank(inPageNames)){
			criterias.add(Restrictions.in("pageName", StringUtils.split(inPageNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalPageUrl)){
			criterias.add(Restrictions.eq("pageUrl", equalPageUrl));
		}
	    if(StringUtils.isNotBlank(likePageUrl)){
			criterias.add(Restrictions.allLike("pageUrl", likePageUrl));
		}
		 if(StringUtils.isNotBlank(inPageUrls)){
			criterias.add(Restrictions.in("pageUrl", StringUtils.split(inPageUrls, ",")));
		}
	    if(StringUtils.isNotBlank(equalIsCheck)){
			criterias.add(Restrictions.eq("isCheck", equalIsCheck));
		}
	    if(StringUtils.isNotBlank(likeIsCheck)){
			criterias.add(Restrictions.allLike("isCheck", likeIsCheck));
		}
		 if(StringUtils.isNotBlank(inIsChecks)){
			criterias.add(Restrictions.in("isCheck", StringUtils.split(inIsChecks, ",")));
		}
	    if(StringUtils.isNotBlank(equalOrderBy)){
			criterias.add(Restrictions.eq("orderBy", equalOrderBy));
		}
	    if(StringUtils.isNotBlank(likeOrderBy)){
			criterias.add(Restrictions.allLike("orderBy", likeOrderBy));
		}
		 if(StringUtils.isNotBlank(inOrderBys)){
			criterias.add(Restrictions.in("orderBy", StringUtils.split(inOrderBys, ",")));
		}
	}
}