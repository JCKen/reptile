package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;

import com.jrzh.framework.base.search.BaseSearch;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;

public class ReptileYgjyHousePreSaleCertificateSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 房子ID
     */
    private String equalHouseId;
    
    /**
     * Like 房子ID
     */
    private String likeHouseId;
    
     /**
     * In 房子ID 英文逗号分隔
     */
    private String inHouseIds;
    
    /**
     * Equal 住宅套数
     */
    private String equalHouseNumberSet;
    
    /**
     * Like 住宅套数
     */
    private String likeHouseNumberSet;
    
     /**
     * In 住宅套数 英文逗号分隔
     */
    private String inHouseNumberSets;
    
    /**
     * Equal 住宅面积
     */
    private String equalHouseArea;
    
    /**
     * Like 住宅面积
     */
    private String likeHouseArea;
    
     /**
     * In 住宅面积 英文逗号分隔
     */
    private String inHouseAreas;
    
    /**
     * Equal 商业套数
     */
    private String equalBusinessNumberSet;
    
    /**
     * Like 商业套数
     */
    private String likeBusinessNumberSet;
    
     /**
     * In 商业套数 英文逗号分隔
     */
    private String inBusinessNumberSets;
    
    /**
     * Equal 商业面积
     */
    private String equalBusinessArea;
    
    /**
     * Like 商业面积
     */
    private String likeBusinessArea;
    
     /**
     * In 商业面积 英文逗号分隔
     */
    private String inBusinessAreas;
    
    /**
     * Equal 办公套数
     */
    private String equalOfficeNumberSet;
    
    /**
     * Like 办公套数
     */
    private String likeOfficeNumberSet;
    
     /**
     * In 办公套数 英文逗号分隔
     */
    private String inOfficeNumberSets;
    
    /**
     * Equal 办公面积
     */
    private String equalOfficeArea;
    
    /**
     * Like 办公面积
     */
    private String likeOfficeArea;
    
     /**
     * In 办公面积 英文逗号分隔
     */
    private String inOfficeAreas;
    
    /**
     * Equal 车位套数
     */
    private String equalParkingNumberSet;
    
    /**
     * Like 车位套数
     */
    private String likeParkingNumberSet;
    
     /**
     * In 车位套数 英文逗号分隔
     */
    private String inParkingNumberSets;
    
    /**
     * Equal 车位面积
     */
    private String equalParkingArea;
    
    /**
     * Like 车位面积
     */
    private String likeParkingArea;
    
     /**
     * In 车位面积 英文逗号分隔
     */
    private String inParkingAreas;
    
    /**
     * Equal 其他套数
     */
    private String equalOtherNumberSet;
    
    /**
     * Like 其他套数
     */
    private String likeOtherNumberSet;
    
     /**
     * In 其他套数 英文逗号分隔
     */
    private String inOtherNumberSets;
    
    /**
     * Equal 其他面积
     */
    private String equalOtherArea;
    
    /**
     * Like 其他面积
     */
    private String likeOtherArea;
    
     /**
     * In 其他面积 英文逗号分隔
     */
    private String inOtherAreas;
    
    /**
     * Equal 预字第
     */
    private String equalPreWord;
    
    /**
     * Like 预字第
     */
    private String likePreWord;
    
     /**
     * In 预字第 英文逗号分隔
     */
    private String inPreWords;
    
    /**
     * Equal 预售幢数
     */
    private String equalPreSaleNumber;
    
    /**
     * Like 预售幢数
     */
    private String likePreSaleNumber;
    
     /**
     * In 预售幢数 英文逗号分隔
     */
    private String inPreSaleNumbers;
    
    /**
     * Equal 报建屋数
     */
    private String equalHouseBuiltNumber;
    
    /**
     * Like 报建屋数
     */
    private String likeHouseBuiltNumber;
    
     /**
     * In 报建屋数 英文逗号分隔
     */
    private String inHouseBuiltNumbers;
    
    /**
     * Equal 已建层数
     */
    private String equalLayerBuiltNumber;
    
    /**
     * Like 已建层数
     */
    private String likeLayerBuiltNumber;
    
     /**
     * In 已建层数 英文逗号分隔
     */
    private String inLayerBuiltNumbers;
    
    /**
     * Equal 本期报建总面积
     */
    private String equalTotalAreaReport;
    
    /**
     * Like 本期报建总面积
     */
    private String likeTotalAreaReport;
    
     /**
     * In 本期报建总面积 英文逗号分隔
     */
    private String inTotalAreaReports;
    
    /**
     * Equal 地上面积
     */
    private String equalAboveGroundArea;
    
    /**
     * Like 地上面积
     */
    private String likeAboveGroundArea;
    
     /**
     * In 地上面积 英文逗号分隔
     */
    private String inAboveGroundAreas;
    
    /**
     * Equal 地下面积
     */
    private String equalUndergroundArea;
    
    /**
     * Like 地下面积
     */
    private String likeUndergroundArea;
    
     /**
     * In 地下面积 英文逗号分隔
     */
    private String inUndergroundAreas;
    
    /**
     * Equal 本期预售总建筑面积
     */
    private String equalPreSaleGrossFloorArea;
    
    /**
     * Like 本期预售总建筑面积
     */
    private String likePreSaleGrossFloorArea;
    
     /**
     * In 本期预售总建筑面积 英文逗号分隔
     */
    private String inPreSaleGrossFloorAreas;
    
    /**
     * Equal 预售房屋占用土地是否抵押
     */
    private String equalPreSaleHouseIsMortgaged;
    
    /**
     * Like 预售房屋占用土地是否抵押
     */
    private String likePreSaleHouseIsMortgaged;
    
     /**
     * In 预售房屋占用土地是否抵押 英文逗号分隔
     */
    private String inPreSaleHouseIsMortgageds;
    
    /**
     * Equal 预售楼宇配套面积情况
     */
    private String equalPreSaleBuildingSupportingArea;
    
    /**
     * Like 预售楼宇配套面积情况
     */
    private String likePreSaleBuildingSupportingArea;
    
     /**
     * In 预售楼宇配套面积情况 英文逗号分隔
     */
    private String inPreSaleBuildingSupportingAreas;
    

    public void setEqualHouseId(String equalHouseId) {
        this.equalHouseId = equalHouseId;
    }
    
    public String getEqualHouseId() {
        return this.equalHouseId;
    }
    
    public void setLikeHouseId(String likeHouseId) {
        this.likeHouseId = likeHouseId;
    }
    
    public String getLikeHouseId() {
        return this.likeHouseId;
    }
    
    public void setInHouseIds(String inHouseIds) {
        this.inHouseIds = inHouseIds;
    }
    
    public String getInHouseIds() {
        return this.inHouseIds;
    }
    public void setEqualHouseNumberSet(String equalHouseNumberSet) {
        this.equalHouseNumberSet = equalHouseNumberSet;
    }
    
    public String getEqualHouseNumberSet() {
        return this.equalHouseNumberSet;
    }
    
    public void setLikeHouseNumberSet(String likeHouseNumberSet) {
        this.likeHouseNumberSet = likeHouseNumberSet;
    }
    
    public String getLikeHouseNumberSet() {
        return this.likeHouseNumberSet;
    }
    
    public void setInHouseNumberSets(String inHouseNumberSets) {
        this.inHouseNumberSets = inHouseNumberSets;
    }
    
    public String getInHouseNumberSets() {
        return this.inHouseNumberSets;
    }
    public void setEqualHouseArea(String equalHouseArea) {
        this.equalHouseArea = equalHouseArea;
    }
    
    public String getEqualHouseArea() {
        return this.equalHouseArea;
    }
    
    public void setLikeHouseArea(String likeHouseArea) {
        this.likeHouseArea = likeHouseArea;
    }
    
    public String getLikeHouseArea() {
        return this.likeHouseArea;
    }
    
    public void setInHouseAreas(String inHouseAreas) {
        this.inHouseAreas = inHouseAreas;
    }
    
    public String getInHouseAreas() {
        return this.inHouseAreas;
    }
    public void setEqualBusinessNumberSet(String equalBusinessNumberSet) {
        this.equalBusinessNumberSet = equalBusinessNumberSet;
    }
    
    public String getEqualBusinessNumberSet() {
        return this.equalBusinessNumberSet;
    }
    
    public void setLikeBusinessNumberSet(String likeBusinessNumberSet) {
        this.likeBusinessNumberSet = likeBusinessNumberSet;
    }
    
    public String getLikeBusinessNumberSet() {
        return this.likeBusinessNumberSet;
    }
    
    public void setInBusinessNumberSets(String inBusinessNumberSets) {
        this.inBusinessNumberSets = inBusinessNumberSets;
    }
    
    public String getInBusinessNumberSets() {
        return this.inBusinessNumberSets;
    }
    public void setEqualBusinessArea(String equalBusinessArea) {
        this.equalBusinessArea = equalBusinessArea;
    }
    
    public String getEqualBusinessArea() {
        return this.equalBusinessArea;
    }
    
    public void setLikeBusinessArea(String likeBusinessArea) {
        this.likeBusinessArea = likeBusinessArea;
    }
    
    public String getLikeBusinessArea() {
        return this.likeBusinessArea;
    }
    
    public void setInBusinessAreas(String inBusinessAreas) {
        this.inBusinessAreas = inBusinessAreas;
    }
    
    public String getInBusinessAreas() {
        return this.inBusinessAreas;
    }
    public void setEqualOfficeNumberSet(String equalOfficeNumberSet) {
        this.equalOfficeNumberSet = equalOfficeNumberSet;
    }
    
    public String getEqualOfficeNumberSet() {
        return this.equalOfficeNumberSet;
    }
    
    public void setLikeOfficeNumberSet(String likeOfficeNumberSet) {
        this.likeOfficeNumberSet = likeOfficeNumberSet;
    }
    
    public String getLikeOfficeNumberSet() {
        return this.likeOfficeNumberSet;
    }
    
    public void setInOfficeNumberSets(String inOfficeNumberSets) {
        this.inOfficeNumberSets = inOfficeNumberSets;
    }
    
    public String getInOfficeNumberSets() {
        return this.inOfficeNumberSets;
    }
    public void setEqualOfficeArea(String equalOfficeArea) {
        this.equalOfficeArea = equalOfficeArea;
    }
    
    public String getEqualOfficeArea() {
        return this.equalOfficeArea;
    }
    
    public void setLikeOfficeArea(String likeOfficeArea) {
        this.likeOfficeArea = likeOfficeArea;
    }
    
    public String getLikeOfficeArea() {
        return this.likeOfficeArea;
    }
    
    public void setInOfficeAreas(String inOfficeAreas) {
        this.inOfficeAreas = inOfficeAreas;
    }
    
    public String getInOfficeAreas() {
        return this.inOfficeAreas;
    }
    public void setEqualParkingNumberSet(String equalParkingNumberSet) {
        this.equalParkingNumberSet = equalParkingNumberSet;
    }
    
    public String getEqualParkingNumberSet() {
        return this.equalParkingNumberSet;
    }
    
    public void setLikeParkingNumberSet(String likeParkingNumberSet) {
        this.likeParkingNumberSet = likeParkingNumberSet;
    }
    
    public String getLikeParkingNumberSet() {
        return this.likeParkingNumberSet;
    }
    
    public void setInParkingNumberSets(String inParkingNumberSets) {
        this.inParkingNumberSets = inParkingNumberSets;
    }
    
    public String getInParkingNumberSets() {
        return this.inParkingNumberSets;
    }
    public void setEqualParkingArea(String equalParkingArea) {
        this.equalParkingArea = equalParkingArea;
    }
    
    public String getEqualParkingArea() {
        return this.equalParkingArea;
    }
    
    public void setLikeParkingArea(String likeParkingArea) {
        this.likeParkingArea = likeParkingArea;
    }
    
    public String getLikeParkingArea() {
        return this.likeParkingArea;
    }
    
    public void setInParkingAreas(String inParkingAreas) {
        this.inParkingAreas = inParkingAreas;
    }
    
    public String getInParkingAreas() {
        return this.inParkingAreas;
    }
    public void setEqualOtherNumberSet(String equalOtherNumberSet) {
        this.equalOtherNumberSet = equalOtherNumberSet;
    }
    
    public String getEqualOtherNumberSet() {
        return this.equalOtherNumberSet;
    }
    
    public void setLikeOtherNumberSet(String likeOtherNumberSet) {
        this.likeOtherNumberSet = likeOtherNumberSet;
    }
    
    public String getLikeOtherNumberSet() {
        return this.likeOtherNumberSet;
    }
    
    public void setInOtherNumberSets(String inOtherNumberSets) {
        this.inOtherNumberSets = inOtherNumberSets;
    }
    
    public String getInOtherNumberSets() {
        return this.inOtherNumberSets;
    }
    public void setEqualOtherArea(String equalOtherArea) {
        this.equalOtherArea = equalOtherArea;
    }
    
    public String getEqualOtherArea() {
        return this.equalOtherArea;
    }
    
    public void setLikeOtherArea(String likeOtherArea) {
        this.likeOtherArea = likeOtherArea;
    }
    
    public String getLikeOtherArea() {
        return this.likeOtherArea;
    }
    
    public void setInOtherAreas(String inOtherAreas) {
        this.inOtherAreas = inOtherAreas;
    }
    
    public String getInOtherAreas() {
        return this.inOtherAreas;
    }
    public void setEqualPreWord(String equalPreWord) {
        this.equalPreWord = equalPreWord;
    }
    
    public String getEqualPreWord() {
        return this.equalPreWord;
    }
    
    public void setLikePreWord(String likePreWord) {
        this.likePreWord = likePreWord;
    }
    
    public String getLikePreWord() {
        return this.likePreWord;
    }
    
    public void setInPreWords(String inPreWords) {
        this.inPreWords = inPreWords;
    }
    
    public String getInPreWords() {
        return this.inPreWords;
    }
    public void setEqualPreSaleNumber(String equalPreSaleNumber) {
        this.equalPreSaleNumber = equalPreSaleNumber;
    }
    
    public String getEqualPreSaleNumber() {
        return this.equalPreSaleNumber;
    }
    
    public void setLikePreSaleNumber(String likePreSaleNumber) {
        this.likePreSaleNumber = likePreSaleNumber;
    }
    
    public String getLikePreSaleNumber() {
        return this.likePreSaleNumber;
    }
    
    public void setInPreSaleNumbers(String inPreSaleNumbers) {
        this.inPreSaleNumbers = inPreSaleNumbers;
    }
    
    public String getInPreSaleNumbers() {
        return this.inPreSaleNumbers;
    }
    public void setEqualHouseBuiltNumber(String equalHouseBuiltNumber) {
        this.equalHouseBuiltNumber = equalHouseBuiltNumber;
    }
    
    public String getEqualHouseBuiltNumber() {
        return this.equalHouseBuiltNumber;
    }
    
    public void setLikeHouseBuiltNumber(String likeHouseBuiltNumber) {
        this.likeHouseBuiltNumber = likeHouseBuiltNumber;
    }
    
    public String getLikeHouseBuiltNumber() {
        return this.likeHouseBuiltNumber;
    }
    
    public void setInHouseBuiltNumbers(String inHouseBuiltNumbers) {
        this.inHouseBuiltNumbers = inHouseBuiltNumbers;
    }
    
    public String getInHouseBuiltNumbers() {
        return this.inHouseBuiltNumbers;
    }
    public void setEqualLayerBuiltNumber(String equalLayerBuiltNumber) {
        this.equalLayerBuiltNumber = equalLayerBuiltNumber;
    }
    
    public String getEqualLayerBuiltNumber() {
        return this.equalLayerBuiltNumber;
    }
    
    public void setLikeLayerBuiltNumber(String likeLayerBuiltNumber) {
        this.likeLayerBuiltNumber = likeLayerBuiltNumber;
    }
    
    public String getLikeLayerBuiltNumber() {
        return this.likeLayerBuiltNumber;
    }
    
    public void setInLayerBuiltNumbers(String inLayerBuiltNumbers) {
        this.inLayerBuiltNumbers = inLayerBuiltNumbers;
    }
    
    public String getInLayerBuiltNumbers() {
        return this.inLayerBuiltNumbers;
    }
    public void setEqualTotalAreaReport(String equalTotalAreaReport) {
        this.equalTotalAreaReport = equalTotalAreaReport;
    }
    
    public String getEqualTotalAreaReport() {
        return this.equalTotalAreaReport;
    }
    
    public void setLikeTotalAreaReport(String likeTotalAreaReport) {
        this.likeTotalAreaReport = likeTotalAreaReport;
    }
    
    public String getLikeTotalAreaReport() {
        return this.likeTotalAreaReport;
    }
    
    public void setInTotalAreaReports(String inTotalAreaReports) {
        this.inTotalAreaReports = inTotalAreaReports;
    }
    
    public String getInTotalAreaReports() {
        return this.inTotalAreaReports;
    }
    public void setEqualAboveGroundArea(String equalAboveGroundArea) {
        this.equalAboveGroundArea = equalAboveGroundArea;
    }
    
    public String getEqualAboveGroundArea() {
        return this.equalAboveGroundArea;
    }
    
    public void setLikeAboveGroundArea(String likeAboveGroundArea) {
        this.likeAboveGroundArea = likeAboveGroundArea;
    }
    
    public String getLikeAboveGroundArea() {
        return this.likeAboveGroundArea;
    }
    
    public void setInAboveGroundAreas(String inAboveGroundAreas) {
        this.inAboveGroundAreas = inAboveGroundAreas;
    }
    
    public String getInAboveGroundAreas() {
        return this.inAboveGroundAreas;
    }
    public void setEqualUndergroundArea(String equalUndergroundArea) {
        this.equalUndergroundArea = equalUndergroundArea;
    }
    
    public String getEqualUndergroundArea() {
        return this.equalUndergroundArea;
    }
    
    public void setLikeUndergroundArea(String likeUndergroundArea) {
        this.likeUndergroundArea = likeUndergroundArea;
    }
    
    public String getLikeUndergroundArea() {
        return this.likeUndergroundArea;
    }
    
    public void setInUndergroundAreas(String inUndergroundAreas) {
        this.inUndergroundAreas = inUndergroundAreas;
    }
    
    public String getInUndergroundAreas() {
        return this.inUndergroundAreas;
    }
    public void setEqualPreSaleGrossFloorArea(String equalPreSaleGrossFloorArea) {
        this.equalPreSaleGrossFloorArea = equalPreSaleGrossFloorArea;
    }
    
    public String getEqualPreSaleGrossFloorArea() {
        return this.equalPreSaleGrossFloorArea;
    }
    
    public void setLikePreSaleGrossFloorArea(String likePreSaleGrossFloorArea) {
        this.likePreSaleGrossFloorArea = likePreSaleGrossFloorArea;
    }
    
    public String getLikePreSaleGrossFloorArea() {
        return this.likePreSaleGrossFloorArea;
    }
    
    public void setInPreSaleGrossFloorAreas(String inPreSaleGrossFloorAreas) {
        this.inPreSaleGrossFloorAreas = inPreSaleGrossFloorAreas;
    }
    
    public String getInPreSaleGrossFloorAreas() {
        return this.inPreSaleGrossFloorAreas;
    }
    public void setEqualPreSaleHouseIsMortgaged(String equalPreSaleHouseIsMortgaged) {
        this.equalPreSaleHouseIsMortgaged = equalPreSaleHouseIsMortgaged;
    }
    
    public String getEqualPreSaleHouseIsMortgaged() {
        return this.equalPreSaleHouseIsMortgaged;
    }
    
    public void setLikePreSaleHouseIsMortgaged(String likePreSaleHouseIsMortgaged) {
        this.likePreSaleHouseIsMortgaged = likePreSaleHouseIsMortgaged;
    }
    
    public String getLikePreSaleHouseIsMortgaged() {
        return this.likePreSaleHouseIsMortgaged;
    }
    
    public void setInPreSaleHouseIsMortgageds(String inPreSaleHouseIsMortgageds) {
        this.inPreSaleHouseIsMortgageds = inPreSaleHouseIsMortgageds;
    }
    
    public String getInPreSaleHouseIsMortgageds() {
        return this.inPreSaleHouseIsMortgageds;
    }
    public void setEqualPreSaleBuildingSupportingArea(String equalPreSaleBuildingSupportingArea) {
        this.equalPreSaleBuildingSupportingArea = equalPreSaleBuildingSupportingArea;
    }
    
    public String getEqualPreSaleBuildingSupportingArea() {
        return this.equalPreSaleBuildingSupportingArea;
    }
    
    public void setLikePreSaleBuildingSupportingArea(String likePreSaleBuildingSupportingArea) {
        this.likePreSaleBuildingSupportingArea = likePreSaleBuildingSupportingArea;
    }
    
    public String getLikePreSaleBuildingSupportingArea() {
        return this.likePreSaleBuildingSupportingArea;
    }
    
    public void setInPreSaleBuildingSupportingAreas(String inPreSaleBuildingSupportingAreas) {
        this.inPreSaleBuildingSupportingAreas = inPreSaleBuildingSupportingAreas;
    }
    
    public String getInPreSaleBuildingSupportingAreas() {
        return this.inPreSaleBuildingSupportingAreas;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalHouseId)){
			criterias.add(Restrictions.eq("houseId", equalHouseId));
		}
	    if(StringUtils.isNotBlank(likeHouseId)){
			criterias.add(Restrictions.allLike("houseId", likeHouseId));
		}
		 if(StringUtils.isNotBlank(inHouseIds)){
			criterias.add(Restrictions.in("houseId", StringUtils.split(inHouseIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseNumberSet)){
			criterias.add(Restrictions.eq("houseNumberSet", equalHouseNumberSet));
		}
	    if(StringUtils.isNotBlank(likeHouseNumberSet)){
			criterias.add(Restrictions.allLike("houseNumberSet", likeHouseNumberSet));
		}
		 if(StringUtils.isNotBlank(inHouseNumberSets)){
			criterias.add(Restrictions.in("houseNumberSet", StringUtils.split(inHouseNumberSets, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseArea)){
			criterias.add(Restrictions.eq("houseArea", equalHouseArea));
		}
	    if(StringUtils.isNotBlank(likeHouseArea)){
			criterias.add(Restrictions.allLike("houseArea", likeHouseArea));
		}
		 if(StringUtils.isNotBlank(inHouseAreas)){
			criterias.add(Restrictions.in("houseArea", StringUtils.split(inHouseAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalBusinessNumberSet)){
			criterias.add(Restrictions.eq("businessNumberSet", equalBusinessNumberSet));
		}
	    if(StringUtils.isNotBlank(likeBusinessNumberSet)){
			criterias.add(Restrictions.allLike("businessNumberSet", likeBusinessNumberSet));
		}
		 if(StringUtils.isNotBlank(inBusinessNumberSets)){
			criterias.add(Restrictions.in("businessNumberSet", StringUtils.split(inBusinessNumberSets, ",")));
		}
	    if(StringUtils.isNotBlank(equalBusinessArea)){
			criterias.add(Restrictions.eq("businessArea", equalBusinessArea));
		}
	    if(StringUtils.isNotBlank(likeBusinessArea)){
			criterias.add(Restrictions.allLike("businessArea", likeBusinessArea));
		}
		 if(StringUtils.isNotBlank(inBusinessAreas)){
			criterias.add(Restrictions.in("businessArea", StringUtils.split(inBusinessAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalOfficeNumberSet)){
			criterias.add(Restrictions.eq("officeNumberSet", equalOfficeNumberSet));
		}
	    if(StringUtils.isNotBlank(likeOfficeNumberSet)){
			criterias.add(Restrictions.allLike("officeNumberSet", likeOfficeNumberSet));
		}
		 if(StringUtils.isNotBlank(inOfficeNumberSets)){
			criterias.add(Restrictions.in("officeNumberSet", StringUtils.split(inOfficeNumberSets, ",")));
		}
	    if(StringUtils.isNotBlank(equalOfficeArea)){
			criterias.add(Restrictions.eq("officeArea", equalOfficeArea));
		}
	    if(StringUtils.isNotBlank(likeOfficeArea)){
			criterias.add(Restrictions.allLike("officeArea", likeOfficeArea));
		}
		 if(StringUtils.isNotBlank(inOfficeAreas)){
			criterias.add(Restrictions.in("officeArea", StringUtils.split(inOfficeAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalParkingNumberSet)){
			criterias.add(Restrictions.eq("parkingNumberSet", equalParkingNumberSet));
		}
	    if(StringUtils.isNotBlank(likeParkingNumberSet)){
			criterias.add(Restrictions.allLike("parkingNumberSet", likeParkingNumberSet));
		}
		 if(StringUtils.isNotBlank(inParkingNumberSets)){
			criterias.add(Restrictions.in("parkingNumberSet", StringUtils.split(inParkingNumberSets, ",")));
		}
	    if(StringUtils.isNotBlank(equalParkingArea)){
			criterias.add(Restrictions.eq("parkingArea", equalParkingArea));
		}
	    if(StringUtils.isNotBlank(likeParkingArea)){
			criterias.add(Restrictions.allLike("parkingArea", likeParkingArea));
		}
		 if(StringUtils.isNotBlank(inParkingAreas)){
			criterias.add(Restrictions.in("parkingArea", StringUtils.split(inParkingAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalOtherNumberSet)){
			criterias.add(Restrictions.eq("otherNumberSet", equalOtherNumberSet));
		}
	    if(StringUtils.isNotBlank(likeOtherNumberSet)){
			criterias.add(Restrictions.allLike("otherNumberSet", likeOtherNumberSet));
		}
		 if(StringUtils.isNotBlank(inOtherNumberSets)){
			criterias.add(Restrictions.in("otherNumberSet", StringUtils.split(inOtherNumberSets, ",")));
		}
	    if(StringUtils.isNotBlank(equalOtherArea)){
			criterias.add(Restrictions.eq("otherArea", equalOtherArea));
		}
	    if(StringUtils.isNotBlank(likeOtherArea)){
			criterias.add(Restrictions.allLike("otherArea", likeOtherArea));
		}
		 if(StringUtils.isNotBlank(inOtherAreas)){
			criterias.add(Restrictions.in("otherArea", StringUtils.split(inOtherAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalPreWord)){
			criterias.add(Restrictions.eq("preWord", equalPreWord));
		}
	    if(StringUtils.isNotBlank(likePreWord)){
			criterias.add(Restrictions.allLike("preWord", likePreWord));
		}
		 if(StringUtils.isNotBlank(inPreWords)){
			criterias.add(Restrictions.in("preWord", StringUtils.split(inPreWords, ",")));
		}
	    if(StringUtils.isNotBlank(equalPreSaleNumber)){
			criterias.add(Restrictions.eq("preSaleNumber", equalPreSaleNumber));
		}
	    if(StringUtils.isNotBlank(likePreSaleNumber)){
			criterias.add(Restrictions.allLike("preSaleNumber", likePreSaleNumber));
		}
		 if(StringUtils.isNotBlank(inPreSaleNumbers)){
			criterias.add(Restrictions.in("preSaleNumber", StringUtils.split(inPreSaleNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseBuiltNumber)){
			criterias.add(Restrictions.eq("houseBuiltNumber", equalHouseBuiltNumber));
		}
	    if(StringUtils.isNotBlank(likeHouseBuiltNumber)){
			criterias.add(Restrictions.allLike("houseBuiltNumber", likeHouseBuiltNumber));
		}
		 if(StringUtils.isNotBlank(inHouseBuiltNumbers)){
			criterias.add(Restrictions.in("houseBuiltNumber", StringUtils.split(inHouseBuiltNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalLayerBuiltNumber)){
			criterias.add(Restrictions.eq("layerBuiltNumber", equalLayerBuiltNumber));
		}
	    if(StringUtils.isNotBlank(likeLayerBuiltNumber)){
			criterias.add(Restrictions.allLike("layerBuiltNumber", likeLayerBuiltNumber));
		}
		 if(StringUtils.isNotBlank(inLayerBuiltNumbers)){
			criterias.add(Restrictions.in("layerBuiltNumber", StringUtils.split(inLayerBuiltNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalTotalAreaReport)){
			criterias.add(Restrictions.eq("totalAreaReport", equalTotalAreaReport));
		}
	    if(StringUtils.isNotBlank(likeTotalAreaReport)){
			criterias.add(Restrictions.allLike("totalAreaReport", likeTotalAreaReport));
		}
		 if(StringUtils.isNotBlank(inTotalAreaReports)){
			criterias.add(Restrictions.in("totalAreaReport", StringUtils.split(inTotalAreaReports, ",")));
		}
	    if(StringUtils.isNotBlank(equalAboveGroundArea)){
			criterias.add(Restrictions.eq("aboveGroundArea", equalAboveGroundArea));
		}
	    if(StringUtils.isNotBlank(likeAboveGroundArea)){
			criterias.add(Restrictions.allLike("aboveGroundArea", likeAboveGroundArea));
		}
		 if(StringUtils.isNotBlank(inAboveGroundAreas)){
			criterias.add(Restrictions.in("aboveGroundArea", StringUtils.split(inAboveGroundAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalUndergroundArea)){
			criterias.add(Restrictions.eq("undergroundArea", equalUndergroundArea));
		}
	    if(StringUtils.isNotBlank(likeUndergroundArea)){
			criterias.add(Restrictions.allLike("undergroundArea", likeUndergroundArea));
		}
		 if(StringUtils.isNotBlank(inUndergroundAreas)){
			criterias.add(Restrictions.in("undergroundArea", StringUtils.split(inUndergroundAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalPreSaleGrossFloorArea)){
			criterias.add(Restrictions.eq("preSaleGrossFloorArea", equalPreSaleGrossFloorArea));
		}
	    if(StringUtils.isNotBlank(likePreSaleGrossFloorArea)){
			criterias.add(Restrictions.allLike("preSaleGrossFloorArea", likePreSaleGrossFloorArea));
		}
		 if(StringUtils.isNotBlank(inPreSaleGrossFloorAreas)){
			criterias.add(Restrictions.in("preSaleGrossFloorArea", StringUtils.split(inPreSaleGrossFloorAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalPreSaleHouseIsMortgaged)){
			criterias.add(Restrictions.eq("preSaleHouseIsMortgaged", equalPreSaleHouseIsMortgaged));
		}
	    if(StringUtils.isNotBlank(likePreSaleHouseIsMortgaged)){
			criterias.add(Restrictions.allLike("preSaleHouseIsMortgaged", likePreSaleHouseIsMortgaged));
		}
		 if(StringUtils.isNotBlank(inPreSaleHouseIsMortgageds)){
			criterias.add(Restrictions.in("preSaleHouseIsMortgaged", StringUtils.split(inPreSaleHouseIsMortgageds, ",")));
		}
	    if(StringUtils.isNotBlank(equalPreSaleBuildingSupportingArea)){
			criterias.add(Restrictions.eq("preSaleBuildingSupportingArea", equalPreSaleBuildingSupportingArea));
		}
	    if(StringUtils.isNotBlank(likePreSaleBuildingSupportingArea)){
			criterias.add(Restrictions.allLike("preSaleBuildingSupportingArea", likePreSaleBuildingSupportingArea));
		}
		 if(StringUtils.isNotBlank(inPreSaleBuildingSupportingAreas)){
			criterias.add(Restrictions.in("preSaleBuildingSupportingArea", StringUtils.split(inPreSaleBuildingSupportingAreas, ",")));
		}
	}
}