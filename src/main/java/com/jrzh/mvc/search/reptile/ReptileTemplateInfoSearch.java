package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;

import com.jrzh.framework.base.search.BaseSearch;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;

public class ReptileTemplateInfoSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 用户ID
     */
    private String equalUserId;
    
    /**
     * Like 用户ID
     */
    private String likeUserId;
    
     /**
     * In 用户ID 英文逗号分隔
     */
    private String inUserIds;
    
    /**
     * Equal 楼盘ID
     */
    private String equalHouseId;
    
    /**
     * Like 楼盘ID
     */
    private String likeHouseId;
    
     /**
     * In 楼盘ID 英文逗号分隔
     */
    private String inHouseIds;
    
    /**
     * Equal 楼盘价格
     */
    private String equalHousePrice;
    
    /**
     * Like 楼盘价格
     */
    private String likeHousePrice;
    
     /**
     * In 楼盘价格 英文逗号分隔
     */
    private String inHousePrices;
    
    /**
     * Equal 楼盘权重
     */
    private String equalHouseWeight;
    
    /**
     * Like 楼盘权重
     */
    private String likeHouseWeight;
    
     /**
     * In 楼盘权重 英文逗号分隔
     */
    private String inHouseWeights;
    
    /**
     * Equal 楼盘类型（一手房或者二手房或者土地）
     */
    private String equalHouseType;
    
    /**
     * Like 楼盘类型（一手房或者二手房或者土地）
     */
    private String likeHouseType;
    
     /**
     * In 楼盘类型（一手房或者二手房或者土地） 英文逗号分隔
     */
    private String inHouseTypes;
    
    /**
     * Equal 信息生成时间
     */
    private String equalInfoUpdateTime;
    
    /**
     * Like 信息生成时间
     */
    private String likeInfoUpdateTime;
    
     /**
     * In 信息生成时间 英文逗号分隔
     */
    private String inInfoUpdateTimes;
    
    
    /**
     * Equal 模板ID
     */
    private String equalTemplateId;
    
    /**
     * Like 模板ID
     */
    private String likeTemplateId;
    
     /**
     * In 模板ID 英文逗号分隔
     */
    private String inTemplateIds;
    
    public String getEqualTemplateId() {
		return equalTemplateId;
	}

	public void setEqualTemplateId(String equalTemplateId) {
		this.equalTemplateId = equalTemplateId;
	}

	public String getLikeTemplateId() {
		return likeTemplateId;
	}

	public void setLikeTemplateId(String likeTemplateId) {
		this.likeTemplateId = likeTemplateId;
	}

	public String getInTemplateIds() {
		return inTemplateIds;
	}

	public void setInTemplateIds(String inTemplateIds) {
		this.inTemplateIds = inTemplateIds;
	}

	public void setEqualUserId(String equalUserId) {
        this.equalUserId = equalUserId;
    }
    
    public String getEqualUserId() {
        return this.equalUserId;
    }
    
    public void setLikeUserId(String likeUserId) {
        this.likeUserId = likeUserId;
    }
    
    public String getLikeUserId() {
        return this.likeUserId;
    }
    
    public void setInUserIds(String inUserIds) {
        this.inUserIds = inUserIds;
    }
    
    public String getInUserIds() {
        return this.inUserIds;
    }
    public void setEqualHouseId(String equalHouseId) {
        this.equalHouseId = equalHouseId;
    }
    
    public String getEqualHouseId() {
        return this.equalHouseId;
    }
    
    public void setLikeHouseId(String likeHouseId) {
        this.likeHouseId = likeHouseId;
    }
    
    public String getLikeHouseId() {
        return this.likeHouseId;
    }
    
    public void setInHouseIds(String inHouseIds) {
        this.inHouseIds = inHouseIds;
    }
    
    public String getInHouseIds() {
        return this.inHouseIds;
    }
    public void setEqualHousePrice(String equalHousePrice) {
        this.equalHousePrice = equalHousePrice;
    }
    
    public String getEqualHousePrice() {
        return this.equalHousePrice;
    }
    
    public void setLikeHousePrice(String likeHousePrice) {
        this.likeHousePrice = likeHousePrice;
    }
    
    public String getLikeHousePrice() {
        return this.likeHousePrice;
    }
    
    public void setInHousePrices(String inHousePrices) {
        this.inHousePrices = inHousePrices;
    }
    
    public String getInHousePrices() {
        return this.inHousePrices;
    }
    public void setEqualHouseWeight(String equalHouseWeight) {
        this.equalHouseWeight = equalHouseWeight;
    }
    
    public String getEqualHouseWeight() {
        return this.equalHouseWeight;
    }
    
    public void setLikeHouseWeight(String likeHouseWeight) {
        this.likeHouseWeight = likeHouseWeight;
    }
    
    public String getLikeHouseWeight() {
        return this.likeHouseWeight;
    }
    
    public void setInHouseWeights(String inHouseWeights) {
        this.inHouseWeights = inHouseWeights;
    }
    
    public String getInHouseWeights() {
        return this.inHouseWeights;
    }
    public void setEqualHouseType(String equalHouseType) {
        this.equalHouseType = equalHouseType;
    }
    
    public String getEqualHouseType() {
        return this.equalHouseType;
    }
    
    public void setLikeHouseType(String likeHouseType) {
        this.likeHouseType = likeHouseType;
    }
    
    public String getLikeHouseType() {
        return this.likeHouseType;
    }
    
    public void setInHouseTypes(String inHouseTypes) {
        this.inHouseTypes = inHouseTypes;
    }
    
    public String getInHouseTypes() {
        return this.inHouseTypes;
    }
    public void setEqualInfoUpdateTime(String equalInfoUpdateTime) {
        this.equalInfoUpdateTime = equalInfoUpdateTime;
    }
    
    public String getEqualInfoUpdateTime() {
        return this.equalInfoUpdateTime;
    }
    
    public void setLikeInfoUpdateTime(String likeInfoUpdateTime) {
        this.likeInfoUpdateTime = likeInfoUpdateTime;
    }
    
    public String getLikeInfoUpdateTime() {
        return this.likeInfoUpdateTime;
    }
    
    public void setInInfoUpdateTimes(String inInfoUpdateTimes) {
        this.inInfoUpdateTimes = inInfoUpdateTimes;
    }
    
    public String getInInfoUpdateTimes() {
        return this.inInfoUpdateTimes;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalUserId)){
			criterias.add(Restrictions.eq("userId", equalUserId));
		}
	    if(StringUtils.isNotBlank(likeUserId)){
			criterias.add(Restrictions.allLike("userId", likeUserId));
		}
		 if(StringUtils.isNotBlank(inUserIds)){
			criterias.add(Restrictions.in("userId", StringUtils.split(inUserIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseId)){
			criterias.add(Restrictions.eq("houseId", equalHouseId));
		}
	    if(StringUtils.isNotBlank(likeHouseId)){
			criterias.add(Restrictions.allLike("houseId", likeHouseId));
		}
		 if(StringUtils.isNotBlank(inHouseIds)){
			criterias.add(Restrictions.in("houseId", StringUtils.split(inHouseIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalHousePrice)){
			criterias.add(Restrictions.eq("housePrice", equalHousePrice));
		}
	    if(StringUtils.isNotBlank(likeHousePrice)){
			criterias.add(Restrictions.allLike("housePrice", likeHousePrice));
		}
		 if(StringUtils.isNotBlank(inHousePrices)){
			criterias.add(Restrictions.in("housePrice", StringUtils.split(inHousePrices, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseWeight)){
			criterias.add(Restrictions.eq("houseWeight", equalHouseWeight));
		}
	    if(StringUtils.isNotBlank(likeHouseWeight)){
			criterias.add(Restrictions.allLike("houseWeight", likeHouseWeight));
		}
		 if(StringUtils.isNotBlank(inHouseWeights)){
			criterias.add(Restrictions.in("houseWeight", StringUtils.split(inHouseWeights, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseType)){
			criterias.add(Restrictions.eq("houseType", equalHouseType));
		}
	    if(StringUtils.isNotBlank(likeHouseType)){
			criterias.add(Restrictions.allLike("houseType", likeHouseType));
		}
		 if(StringUtils.isNotBlank(inHouseTypes)){
			criterias.add(Restrictions.in("houseType", StringUtils.split(inHouseTypes, ",")));
		}
	    if(StringUtils.isNotBlank(equalInfoUpdateTime)){
			criterias.add(Restrictions.eq("infoUpdateTime", equalInfoUpdateTime));
		}
	    if(StringUtils.isNotBlank(likeInfoUpdateTime)){
			criterias.add(Restrictions.allLike("infoUpdateTime", likeInfoUpdateTime));
		}
		 if(StringUtils.isNotBlank(inInfoUpdateTimes)){
			criterias.add(Restrictions.in("infoUpdateTime", StringUtils.split(inInfoUpdateTimes, ",")));
		}
		 
		if(StringUtils.isNotBlank(equalTemplateId)){
			criterias.add(Restrictions.eq("templateId", equalTemplateId));
		}
	    if(StringUtils.isNotBlank(likeTemplateId)){
			criterias.add(Restrictions.allLike("templateId", likeTemplateId));
		}
		 if(StringUtils.isNotBlank(inTemplateIds)){
			criterias.add(Restrictions.in("templateId", StringUtils.split(inTemplateIds, ",")));
		}
	}
}