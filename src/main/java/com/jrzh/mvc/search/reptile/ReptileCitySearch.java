package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileCitySearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 城市名称
     */
    private String equalCityName;
    
    /**
     * Like 城市名称
     */
    private String likeCityName;
    
     /**
     * In 城市名称 英文逗号分隔
     */
    private String inCityNames;
    

    public void setEqualCityName(String equalCityName) {
        this.equalCityName = equalCityName;
    }
    
    public String getEqualCityName() {
        return this.equalCityName;
    }
    
    public void setLikeCityName(String likeCityName) {
        this.likeCityName = likeCityName;
    }
    
    public String getLikeCityName() {
        return this.likeCityName;
    }
    
    public void setInCityNames(String inCityNames) {
        this.inCityNames = inCityNames;
    }
    
    public String getInCityNames() {
        return this.inCityNames;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalCityName)){
			criterias.add(Restrictions.eq("cityName", equalCityName));
		}
	    if(StringUtils.isNotBlank(likeCityName)){
			criterias.add(Restrictions.allLike("cityName", likeCityName));
		}
		 if(StringUtils.isNotBlank(inCityNames)){
			criterias.add(Restrictions.in("cityName", StringUtils.split(inCityNames, ",")));
		}
	}
}