package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;

import com.jrzh.common.utils.DateUtil;
import com.jrzh.framework.base.search.BaseSearch;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;

public class ReptileEsfHouseSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 小区名
     */
    private String equalHouseName;
    
    /**
     * Like 小区名
     */
    private String likeHouseName;
    
     /**
     * In 小区名 英文逗号分隔
     */
    private String inHouseNames;
    
    /**
     * Equal 小区别名
     */
    private String equalHouseOtherName;
    
    /**
     * Like 小区别名
     */
    private String likeHouseOtherName;
    
     /**
     * In 小区别名 英文逗号分隔
     */
    private String inHouseOtherNames;
    
    /**
     * Equal 小区所在市
     */
    private String equalHouseCity;
    
    /**
     * Like 小区所在市
     */
    private String likeHouseCity;
    
     /**
     * In 小区所在市 英文逗号分隔
     */
    private String inHouseCitys;
    
    /**
     * Equal 小区所在区
     */
    private String equalHousePart;
    
    /**
     * Like 小区所在区
     */
    private String likeHousePart;
    
     /**
     * In 小区所在区 英文逗号分隔
     */
    private String inHouseParts;
    
    /**
     * Equal 小区详细地址
     */
    private String equalHouseAddress;
    
    /**
     * Like 小区详细地址
     */
    private String likeHouseAddress;
    
     /**
     * In 小区详细地址 英文逗号分隔
     */
    private String inHouseAddresss;
    
    /**
     * Equal 楼盘均价
     */
    private String equalHousePrice;
    
    /**
     * Like 楼盘均价
     */
    private String likeHousePrice;
    
     /**
     * In 楼盘均价 英文逗号分隔
     */
    private String inHousePrices;
    
    /**
     * Equal 楼盘类型（商业、住宅、别墅、写字楼）
     */
    private String equalHouseType;
    
    /**
     * Like 楼盘类型（商业、住宅、别墅、写字楼）
     */
    private String likeHouseType;
    
     /**
     * In 楼盘类型（商业、住宅、别墅、写字楼） 英文逗号分隔
     */
    private String inHouseTypes;
    
    /**
     * Equal 楼盘销售状态
     */
    private String equalHouseSaleStatus;
    
    /**
     * Like 楼盘销售状态
     */
    private String likeHouseSaleStatus;
    
     /**
     * In 楼盘销售状态 英文逗号分隔
     */
    private String inHouseSaleStatuss;
    
    /**
     * Equal 楼盘是否新开（根据网址新开楼盘进行判断）
     */
    private String equalIsNewHouse;
    
    /**
     * Like 楼盘是否新开（根据网址新开楼盘进行判断）
     */
    private String likeIsNewHouse;
    
    /**
     * like 装修类型
     */
    private String likeRenovationCondition;
     /**
     * In 楼盘是否新开（根据网址新开楼盘进行判断） 英文逗号分隔
     */
    private String inIsNewHouses;
    
    /**
     * Equal 总价（多少钱一套）
     */
    private String equalHouseAllPrice;
    
    /**
     * Like 总价（多少钱一套）
     */
    private String likeHouseAllPrice;
    
     /**
     * In 总价（多少钱一套） 英文逗号分隔
     */
    private String inHouseAllPrices;
    
    /**
     * Equal 楼盘标签
     */
    private String equalHouseTab;
    
    /**
     * Like 楼盘标签
     */
    private String likeHouseTab;
    
     /**
     * In 楼盘标签 英文逗号分隔
     */
    private String inHouseTabs;
    
    /**
     * Equal 楼盘评分
     */
    private String equalHouseGrade;
    
    /**
     * Like 楼盘评分
     */
    private String likeHouseGrade;
    
     /**
     * In 楼盘评分 英文逗号分隔
     */
    private String inHouseGrades;
    
    /**
     * Equal 最新开盘时间
     */
    private String equalLatestSaleTime;
    
    /**
     * Like 最新开盘时间
     */
    private String likeLatestSaleTime;
    
     /**
     * In 最新开盘时间 英文逗号分隔
     */
    private String inLatestSaleTimes;
    
    /**
     * Equal 主力户型
     */
    private String equalMainUnit;
    
    /**
     * Like 主力户型
     */
    private String likeMainUnit;
    
     /**
     * In 主力户型 英文逗号分隔
     */
    private String inMainUnits;
    
    /**
     * Equal 售楼处地址
     */
    private String equalSalesOfficeAddress;
    
    /**
     * Like 售楼处地址
     */
    private String likeSalesOfficeAddress;
    
     /**
     * In 售楼处地址 英文逗号分隔
     */
    private String inSalesOfficeAddresss;
    
    /**
     * Equal 开发商
     */
    private String equalDevelopers;
    
    /**
     * Like 开发商
     */
    private String likeDevelopers;
    
     /**
     * In 开发商 英文逗号分隔
     */
    private String inDeveloperss;
    
    /**
     * Equal 物业类别
     */
    private String equalPropertyCategory;
    
    /**
     * Like 物业类别
     */
    private String likePropertyCategory;
    
     /**
     * In 物业类别 英文逗号分隔
     */
    private String inPropertyCategorys;
    
    /**
     * Equal 建筑面积
     */
    private String equalBuildingArea;
    
    /**
     * Like 建筑面积
     */
    private String likeBuildingArea;
    
     /**
     * In 建筑面积 英文逗号分隔
     */
    private String inBuildingAreas;
    
    /**
     * Equal 占地面积
     */
    private String equalAreaCovered;
    
    /**
     * Like 占地面积
     */
    private String likeAreaCovered;
    
     /**
     * In 占地面积 英文逗号分隔
     */
    private String inAreaCovereds;
    
    /**
     * Equal 绿化率
     */
    private String equalAfforestationRate;
    
    /**
     * Like 绿化率
     */
    private String likeAfforestationRate;
    
     /**
     * In 绿化率 英文逗号分隔
     */
    private String inAfforestationRates;
    
    /**
     * Equal 容积率
     */
    private String equalPlotRatio;
    
    /**
     * Like 容积率
     */
    private String likePlotRatio;
    
     /**
     * In 容积率 英文逗号分隔
     */
    private String inPlotRatios;
    
    /**
     * Equal 车位
     */
    private String equalParkingLot;
    
    /**
     * Like 车位
     */
    private String likeParkingLot;
    
     /**
     * In 车位 英文逗号分隔
     */
    private String inParkingLots;
    
    /**
     * Equal 供水方式
     */
    private String equalWaterSupplyMode;
    
    /**
     * Like 供水方式
     */
    private String likeWaterSupplyMode;
    
     /**
     * In 供水方式 英文逗号分隔
     */
    private String inWaterSupplyModes;
    
    /**
     * Equal 供电方式
     */
    private String equalPowerSupplyMode;
    
    /**
     * Like 供电方式
     */
    private String likePowerSupplyMode;
    
     /**
     * In 供电方式 英文逗号分隔
     */
    private String inPowerSupplyModes;
    
    /**
     * Equal 产权年限
     */
    private String equalPropertyRightYears;
    
    /**
     * Like 产权年限
     */
    private String likePropertyRightYears;
    
     /**
     * In 产权年限 英文逗号分隔
     */
    private String inPropertyRightYearss;
    
    /**
     * Equal 物业公司
     */
    private String equalPropertyCompany;
    
    /**
     * Like 物业公司
     */
    private String likePropertyCompany;
    
     /**
     * In 物业公司 英文逗号分隔
     */
    private String inPropertyCompanys;
    
    /**
     * Equal 规划户数
     */
    private String equalPlanningHouseholds;
    
    /**
     * Like 规划户数
     */
    private String likePlanningHouseholds;
    
     /**
     * In 规划户数 英文逗号分隔
     */
    private String inPlanningHouseholdss;
    
    /**
     * Equal 交房时间
     */
    private String equalTimeOfDelivery;
    
    /**
     * Like 交房时间
     */
    private String likeTimeOfDelivery;
    
     /**
     * In 交房时间 英文逗号分隔
     */
    private String inTimeOfDeliverys;
    
    /**
     * Equal 物业费用
     */
    private String equalPropertyCosts;
    
    /**
     * Like 物业费用
     */
    private String likePropertyCosts;
    
     /**
     * In 物业费用 英文逗号分隔
     */
    private String inPropertyCostss;
    
    /**
     * Equal 信息来源
     */
    private String equalInformationSources;
    
    /**
     * Like 信息来源
     */
    private String likeInformationSources;
    
     /**
     * In 信息来源 英文逗号分隔
     */
    private String inInformationSourcess;
    
    /**
     * Equal 信息更新时间
     */
    private String equalInfoUpdateTime;
    
    /**
     * Like 信息更新时间
     */
    private String likeInfoUpdateTime;
    
     /**
     * In 信息更新时间 英文逗号分隔
     */
    private String inInfoUpdateTimes;
    
    /**
     * Equal 经度
     */
    private String equalLatitude;
    
    /**
     * Like 经度
     */
    private String likeLatitude;
    
     /**
     * In 经度 英文逗号分隔
     */
    private String inLatitudes;
    
    /**
     * Equal 纬度
     */
    private String equalLongitude;
    
    /**
     * Like 纬度
     */
    private String likeLongitude;
    
     /**
     * In 纬度 英文逗号分隔
     */
    private String inLongitudes;
    
    /**
     * Equal 附近交通
     */
    private String equalTraffic;
    
    /**
     * Like 附近交通
     */
    private String likeTraffic;
    
     /**
     * In 附近交通 英文逗号分隔
     */
    private String inTraffics;
    
    /**
     * Equal 幼儿园
     */
    private String equalKindergarten;
    
    /**
     * Like 幼儿园
     */
    private String likeKindergarten;
    
     /**
     * In 幼儿园 英文逗号分隔
     */
    private String inKindergartens;
    
    /**
     * Equal 中小学
     */
    private String equalSchool;
    
    /**
     * Like 中小学
     */
    private String likeSchool;
    
     /**
     * In 中小学 英文逗号分隔
     */
    private String inSchools;
    
    /**
     * Equal 综合商场
     */
    private String equalPowerCenter;
    
    /**
     * Like 综合商场
     */
    private String likePowerCenter;
    
     /**
     * In 综合商场 英文逗号分隔
     */
    private String inPowerCenters;
    
    /**
     * Equal 医院
     */
    private String equalHospital;
    
    /**
     * Like 医院
     */
    private String likeHospital;
    
     /**
     * In 医院 英文逗号分隔
     */
    private String inHospitals;
    
    /**
     * Equal 银行
     */
    private String equalBank;
    
    /**
     * Like 银行
     */
    private String likeBank;
    
     /**
     * In 银行 英文逗号分隔
     */
    private String inBanks;
    
    /**
     * Equal 邮政
     */
    private String equalPost;
    
    /**
     * Like 邮政
     */
    private String likePost;
    
     /**
     * In 邮政 英文逗号分隔
     */
    private String inPosts;
    
    /**
     * Equal 其他
     */
    private String equalOther;
    
    /**
     * Like 其他
     */
    private String likeOther;
    
     /**
     * In 其他 英文逗号分隔
     */
    private String inOthers;
    
    /**
     * Equal 房价单位(1. 元/㎡ 2. 万/套)
     */
    private String equalHousePriceUnit;
    
    /**
     * Like 房价单位(1. 元/㎡ 2. 万/套)
     */
    private String likeHousePriceUnit;
    
     /**
     * In 房价单位(1. 元/㎡ 2. 万/套) 英文逗号分隔
     */
    private String inHousePriceUnits;
    

    public void setEqualHouseName(String equalHouseName) {
        this.equalHouseName = equalHouseName;
    }
    
    public String getEqualHouseName() {
        return this.equalHouseName;
    }
    
    public void setLikeHouseName(String likeHouseName) {
        this.likeHouseName = likeHouseName;
    }
    
    public String getLikeHouseName() {
        return this.likeHouseName;
    }
    
    public void setInHouseNames(String inHouseNames) {
        this.inHouseNames = inHouseNames;
    }
    
    public String getInHouseNames() {
        return this.inHouseNames;
    }
    public void setEqualHouseOtherName(String equalHouseOtherName) {
        this.equalHouseOtherName = equalHouseOtherName;
    }
    
    public String getEqualHouseOtherName() {
        return this.equalHouseOtherName;
    }
    
    public void setLikeHouseOtherName(String likeHouseOtherName) {
        this.likeHouseOtherName = likeHouseOtherName;
    }
    
    public String getLikeHouseOtherName() {
        return this.likeHouseOtherName;
    }
    
    public void setInHouseOtherNames(String inHouseOtherNames) {
        this.inHouseOtherNames = inHouseOtherNames;
    }
    
    public String getInHouseOtherNames() {
        return this.inHouseOtherNames;
    }
    public void setEqualHouseCity(String equalHouseCity) {
        this.equalHouseCity = equalHouseCity;
    }
    
    public String getEqualHouseCity() {
        return this.equalHouseCity;
    }
    
    public void setLikeHouseCity(String likeHouseCity) {
        this.likeHouseCity = likeHouseCity;
    }
    
    public String getLikeHouseCity() {
        return this.likeHouseCity;
    }
    
    public void setInHouseCitys(String inHouseCitys) {
        this.inHouseCitys = inHouseCitys;
    }
    
    public String getInHouseCitys() {
        return this.inHouseCitys;
    }
    public void setEqualHousePart(String equalHousePart) {
        this.equalHousePart = equalHousePart;
    }
    
    public String getEqualHousePart() {
        return this.equalHousePart;
    }
    
    public void setLikeHousePart(String likeHousePart) {
        this.likeHousePart = likeHousePart;
    }
    
    public String getLikeHousePart() {
        return this.likeHousePart;
    }
    
    public void setInHouseParts(String inHouseParts) {
        this.inHouseParts = inHouseParts;
    }
    
    public String getInHouseParts() {
        return this.inHouseParts;
    }
    public void setEqualHouseAddress(String equalHouseAddress) {
        this.equalHouseAddress = equalHouseAddress;
    }
    
    public String getEqualHouseAddress() {
        return this.equalHouseAddress;
    }
    
    public void setLikeHouseAddress(String likeHouseAddress) {
        this.likeHouseAddress = likeHouseAddress;
    }
    
    public String getLikeHouseAddress() {
        return this.likeHouseAddress;
    }
    
    public void setInHouseAddresss(String inHouseAddresss) {
        this.inHouseAddresss = inHouseAddresss;
    }
    
    public String getInHouseAddresss() {
        return this.inHouseAddresss;
    }
    public void setEqualHousePrice(String equalHousePrice) {
        this.equalHousePrice = equalHousePrice;
    }
    
    public String getEqualHousePrice() {
        return this.equalHousePrice;
    }
    
    public void setLikeHousePrice(String likeHousePrice) {
        this.likeHousePrice = likeHousePrice;
    }
    
    public String getLikeHousePrice() {
        return this.likeHousePrice;
    }
    
    public void setInHousePrices(String inHousePrices) {
        this.inHousePrices = inHousePrices;
    }
    
    public String getInHousePrices() {
        return this.inHousePrices;
    }
    public void setEqualHouseType(String equalHouseType) {
        this.equalHouseType = equalHouseType;
    }
    
    public String getEqualHouseType() {
        return this.equalHouseType;
    }
    
    public void setLikeHouseType(String likeHouseType) {
        this.likeHouseType = likeHouseType;
    }
    
    public String getLikeHouseType() {
        return this.likeHouseType;
    }
    
    public void setInHouseTypes(String inHouseTypes) {
        this.inHouseTypes = inHouseTypes;
    }
    
    public String getInHouseTypes() {
        return this.inHouseTypes;
    }
    public void setEqualHouseSaleStatus(String equalHouseSaleStatus) {
        this.equalHouseSaleStatus = equalHouseSaleStatus;
    }
    
    public String getEqualHouseSaleStatus() {
        return this.equalHouseSaleStatus;
    }
    
    public void setLikeHouseSaleStatus(String likeHouseSaleStatus) {
        this.likeHouseSaleStatus = likeHouseSaleStatus;
    }
    
    public String getLikeHouseSaleStatus() {
        return this.likeHouseSaleStatus;
    }
    
    public void setInHouseSaleStatuss(String inHouseSaleStatuss) {
        this.inHouseSaleStatuss = inHouseSaleStatuss;
    }
    
    public String getInHouseSaleStatuss() {
        return this.inHouseSaleStatuss;
    }
    public void setEqualIsNewHouse(String equalIsNewHouse) {
        this.equalIsNewHouse = equalIsNewHouse;
    }
    
    public String getEqualIsNewHouse() {
        return this.equalIsNewHouse;
    }
    
    public void setLikeIsNewHouse(String likeIsNewHouse) {
        this.likeIsNewHouse = likeIsNewHouse;
    }
    
    public String getLikeIsNewHouse() {
        return this.likeIsNewHouse;
    }
    
    public void setInIsNewHouses(String inIsNewHouses) {
        this.inIsNewHouses = inIsNewHouses;
    }
    
    public String getInIsNewHouses() {
        return this.inIsNewHouses;
    }
    public void setEqualHouseAllPrice(String equalHouseAllPrice) {
        this.equalHouseAllPrice = equalHouseAllPrice;
    }
    
    public String getEqualHouseAllPrice() {
        return this.equalHouseAllPrice;
    }
    
    public void setLikeHouseAllPrice(String likeHouseAllPrice) {
        this.likeHouseAllPrice = likeHouseAllPrice;
    }
    
    public String getLikeHouseAllPrice() {
        return this.likeHouseAllPrice;
    }
    
    public void setInHouseAllPrices(String inHouseAllPrices) {
        this.inHouseAllPrices = inHouseAllPrices;
    }
    
    public String getInHouseAllPrices() {
        return this.inHouseAllPrices;
    }
    public void setEqualHouseTab(String equalHouseTab) {
        this.equalHouseTab = equalHouseTab;
    }
    
    public String getEqualHouseTab() {
        return this.equalHouseTab;
    }
    
    public void setLikeHouseTab(String likeHouseTab) {
        this.likeHouseTab = likeHouseTab;
    }
    
    public String getLikeHouseTab() {
        return this.likeHouseTab;
    }
    
    public void setInHouseTabs(String inHouseTabs) {
        this.inHouseTabs = inHouseTabs;
    }
    
    public String getInHouseTabs() {
        return this.inHouseTabs;
    }
    public void setEqualHouseGrade(String equalHouseGrade) {
        this.equalHouseGrade = equalHouseGrade;
    }
    
    public String getEqualHouseGrade() {
        return this.equalHouseGrade;
    }
    
    public void setLikeHouseGrade(String likeHouseGrade) {
        this.likeHouseGrade = likeHouseGrade;
    }
    
    public String getLikeHouseGrade() {
        return this.likeHouseGrade;
    }
    
    public void setInHouseGrades(String inHouseGrades) {
        this.inHouseGrades = inHouseGrades;
    }
    
    public String getInHouseGrades() {
        return this.inHouseGrades;
    }
    public void setEqualLatestSaleTime(String equalLatestSaleTime) {
        this.equalLatestSaleTime = equalLatestSaleTime;
    }
    
    public String getEqualLatestSaleTime() {
        return this.equalLatestSaleTime;
    }
    
    public void setLikeLatestSaleTime(String likeLatestSaleTime) {
        this.likeLatestSaleTime = likeLatestSaleTime;
    }
    
    public String getLikeLatestSaleTime() {
        return this.likeLatestSaleTime;
    }
    
    public void setInLatestSaleTimes(String inLatestSaleTimes) {
        this.inLatestSaleTimes = inLatestSaleTimes;
    }
    
    public String getInLatestSaleTimes() {
        return this.inLatestSaleTimes;
    }
    public void setEqualMainUnit(String equalMainUnit) {
        this.equalMainUnit = equalMainUnit;
    }
    
    public String getEqualMainUnit() {
        return this.equalMainUnit;
    }
    
    public void setLikeMainUnit(String likeMainUnit) {
        this.likeMainUnit = likeMainUnit;
    }
    
    public String getLikeMainUnit() {
        return this.likeMainUnit;
    }
    
    public void setInMainUnits(String inMainUnits) {
        this.inMainUnits = inMainUnits;
    }
    
    public String getInMainUnits() {
        return this.inMainUnits;
    }
    public void setEqualSalesOfficeAddress(String equalSalesOfficeAddress) {
        this.equalSalesOfficeAddress = equalSalesOfficeAddress;
    }
    
    public String getEqualSalesOfficeAddress() {
        return this.equalSalesOfficeAddress;
    }
    
    public void setLikeSalesOfficeAddress(String likeSalesOfficeAddress) {
        this.likeSalesOfficeAddress = likeSalesOfficeAddress;
    }
    
    public String getLikeSalesOfficeAddress() {
        return this.likeSalesOfficeAddress;
    }
    
    public void setInSalesOfficeAddresss(String inSalesOfficeAddresss) {
        this.inSalesOfficeAddresss = inSalesOfficeAddresss;
    }
    
    public String getInSalesOfficeAddresss() {
        return this.inSalesOfficeAddresss;
    }
    public void setEqualDevelopers(String equalDevelopers) {
        this.equalDevelopers = equalDevelopers;
    }
    
    public String getEqualDevelopers() {
        return this.equalDevelopers;
    }
    
    public void setLikeDevelopers(String likeDevelopers) {
        this.likeDevelopers = likeDevelopers;
    }
    
    public String getLikeDevelopers() {
        return this.likeDevelopers;
    }
    
    public void setInDeveloperss(String inDeveloperss) {
        this.inDeveloperss = inDeveloperss;
    }
    
    public String getInDeveloperss() {
        return this.inDeveloperss;
    }
    public void setEqualPropertyCategory(String equalPropertyCategory) {
        this.equalPropertyCategory = equalPropertyCategory;
    }
    
    public String getEqualPropertyCategory() {
        return this.equalPropertyCategory;
    }
    
    public void setLikePropertyCategory(String likePropertyCategory) {
        this.likePropertyCategory = likePropertyCategory;
    }
    
    public String getLikePropertyCategory() {
        return this.likePropertyCategory;
    }
    
    public void setInPropertyCategorys(String inPropertyCategorys) {
        this.inPropertyCategorys = inPropertyCategorys;
    }
    
    public String getInPropertyCategorys() {
        return this.inPropertyCategorys;
    }
    public void setEqualBuildingArea(String equalBuildingArea) {
        this.equalBuildingArea = equalBuildingArea;
    }
    
    public String getEqualBuildingArea() {
        return this.equalBuildingArea;
    }
    
    public void setLikeBuildingArea(String likeBuildingArea) {
        this.likeBuildingArea = likeBuildingArea;
    }
    
    public String getLikeBuildingArea() {
        return this.likeBuildingArea;
    }
    
    public void setInBuildingAreas(String inBuildingAreas) {
        this.inBuildingAreas = inBuildingAreas;
    }
    
    public String getInBuildingAreas() {
        return this.inBuildingAreas;
    }
    public void setEqualAreaCovered(String equalAreaCovered) {
        this.equalAreaCovered = equalAreaCovered;
    }
    
    public String getEqualAreaCovered() {
        return this.equalAreaCovered;
    }
    
    public void setLikeAreaCovered(String likeAreaCovered) {
        this.likeAreaCovered = likeAreaCovered;
    }
    
    public String getLikeAreaCovered() {
        return this.likeAreaCovered;
    }
    
    public void setInAreaCovereds(String inAreaCovereds) {
        this.inAreaCovereds = inAreaCovereds;
    }
    
    public String getInAreaCovereds() {
        return this.inAreaCovereds;
    }
    public void setEqualAfforestationRate(String equalAfforestationRate) {
        this.equalAfforestationRate = equalAfforestationRate;
    }
    
    public String getEqualAfforestationRate() {
        return this.equalAfforestationRate;
    }
    
    public void setLikeAfforestationRate(String likeAfforestationRate) {
        this.likeAfforestationRate = likeAfforestationRate;
    }
    
    public String getLikeAfforestationRate() {
        return this.likeAfforestationRate;
    }
    
    public void setInAfforestationRates(String inAfforestationRates) {
        this.inAfforestationRates = inAfforestationRates;
    }
    
    public String getInAfforestationRates() {
        return this.inAfforestationRates;
    }
    public void setEqualPlotRatio(String equalPlotRatio) {
        this.equalPlotRatio = equalPlotRatio;
    }
    
    public String getEqualPlotRatio() {
        return this.equalPlotRatio;
    }
    
    public void setLikePlotRatio(String likePlotRatio) {
        this.likePlotRatio = likePlotRatio;
    }
    
    public String getLikePlotRatio() {
        return this.likePlotRatio;
    }
    
    public void setInPlotRatios(String inPlotRatios) {
        this.inPlotRatios = inPlotRatios;
    }
    
    public String getInPlotRatios() {
        return this.inPlotRatios;
    }
    public void setEqualParkingLot(String equalParkingLot) {
        this.equalParkingLot = equalParkingLot;
    }
    
    public String getEqualParkingLot() {
        return this.equalParkingLot;
    }
    
    public void setLikeParkingLot(String likeParkingLot) {
        this.likeParkingLot = likeParkingLot;
    }
    
    public String getLikeParkingLot() {
        return this.likeParkingLot;
    }
    
    public void setInParkingLots(String inParkingLots) {
        this.inParkingLots = inParkingLots;
    }
    
    public String getInParkingLots() {
        return this.inParkingLots;
    }
    public void setEqualWaterSupplyMode(String equalWaterSupplyMode) {
        this.equalWaterSupplyMode = equalWaterSupplyMode;
    }
    
    public String getEqualWaterSupplyMode() {
        return this.equalWaterSupplyMode;
    }
    
    public void setLikeWaterSupplyMode(String likeWaterSupplyMode) {
        this.likeWaterSupplyMode = likeWaterSupplyMode;
    }
    
    public String getLikeWaterSupplyMode() {
        return this.likeWaterSupplyMode;
    }
    
    public void setInWaterSupplyModes(String inWaterSupplyModes) {
        this.inWaterSupplyModes = inWaterSupplyModes;
    }
    
    public String getInWaterSupplyModes() {
        return this.inWaterSupplyModes;
    }
    public void setEqualPowerSupplyMode(String equalPowerSupplyMode) {
        this.equalPowerSupplyMode = equalPowerSupplyMode;
    }
    
    public String getEqualPowerSupplyMode() {
        return this.equalPowerSupplyMode;
    }
    
    public void setLikePowerSupplyMode(String likePowerSupplyMode) {
        this.likePowerSupplyMode = likePowerSupplyMode;
    }
    
    public String getLikePowerSupplyMode() {
        return this.likePowerSupplyMode;
    }
    
    public void setInPowerSupplyModes(String inPowerSupplyModes) {
        this.inPowerSupplyModes = inPowerSupplyModes;
    }
    
    public String getInPowerSupplyModes() {
        return this.inPowerSupplyModes;
    }
    public void setEqualPropertyRightYears(String equalPropertyRightYears) {
        this.equalPropertyRightYears = equalPropertyRightYears;
    }
    
    public String getEqualPropertyRightYears() {
        return this.equalPropertyRightYears;
    }
    
    public void setLikePropertyRightYears(String likePropertyRightYears) {
        this.likePropertyRightYears = likePropertyRightYears;
    }
    
    public String getLikePropertyRightYears() {
        return this.likePropertyRightYears;
    }
    
    public void setInPropertyRightYearss(String inPropertyRightYearss) {
        this.inPropertyRightYearss = inPropertyRightYearss;
    }
    
    public String getInPropertyRightYearss() {
        return this.inPropertyRightYearss;
    }
    public void setEqualPropertyCompany(String equalPropertyCompany) {
        this.equalPropertyCompany = equalPropertyCompany;
    }
    
    public String getLikeRenovationCondition() {
		return likeRenovationCondition;
	}

	public void setLikeRenovationCondition(String likeRenovationCondition) {
		this.likeRenovationCondition = likeRenovationCondition;
	}

	public String getEqualPropertyCompany() {
        return this.equalPropertyCompany;
    }
    
    public void setLikePropertyCompany(String likePropertyCompany) {
        this.likePropertyCompany = likePropertyCompany;
    }
    
    public String getLikePropertyCompany() {
        return this.likePropertyCompany;
    }
    
    public void setInPropertyCompanys(String inPropertyCompanys) {
        this.inPropertyCompanys = inPropertyCompanys;
    }
    
    public String getInPropertyCompanys() {
        return this.inPropertyCompanys;
    }
    public void setEqualPlanningHouseholds(String equalPlanningHouseholds) {
        this.equalPlanningHouseholds = equalPlanningHouseholds;
    }
    
    public String getEqualPlanningHouseholds() {
        return this.equalPlanningHouseholds;
    }
    
    public void setLikePlanningHouseholds(String likePlanningHouseholds) {
        this.likePlanningHouseholds = likePlanningHouseholds;
    }
    
    public String getLikePlanningHouseholds() {
        return this.likePlanningHouseholds;
    }
    
    public void setInPlanningHouseholdss(String inPlanningHouseholdss) {
        this.inPlanningHouseholdss = inPlanningHouseholdss;
    }
    
    public String getInPlanningHouseholdss() {
        return this.inPlanningHouseholdss;
    }
    public void setEqualTimeOfDelivery(String equalTimeOfDelivery) {
        this.equalTimeOfDelivery = equalTimeOfDelivery;
    }
    
    public String getEqualTimeOfDelivery() {
        return this.equalTimeOfDelivery;
    }
    
    public void setLikeTimeOfDelivery(String likeTimeOfDelivery) {
        this.likeTimeOfDelivery = likeTimeOfDelivery;
    }
    
    public String getLikeTimeOfDelivery() {
        return this.likeTimeOfDelivery;
    }
    
    public void setInTimeOfDeliverys(String inTimeOfDeliverys) {
        this.inTimeOfDeliverys = inTimeOfDeliverys;
    }
    
    public String getInTimeOfDeliverys() {
        return this.inTimeOfDeliverys;
    }
    public void setEqualPropertyCosts(String equalPropertyCosts) {
        this.equalPropertyCosts = equalPropertyCosts;
    }
    
    public String getEqualPropertyCosts() {
        return this.equalPropertyCosts;
    }
    
    public void setLikePropertyCosts(String likePropertyCosts) {
        this.likePropertyCosts = likePropertyCosts;
    }
    
    public String getLikePropertyCosts() {
        return this.likePropertyCosts;
    }
    
    public void setInPropertyCostss(String inPropertyCostss) {
        this.inPropertyCostss = inPropertyCostss;
    }
    
    public String getInPropertyCostss() {
        return this.inPropertyCostss;
    }
    public void setEqualInformationSources(String equalInformationSources) {
        this.equalInformationSources = equalInformationSources;
    }
    
    public String getEqualInformationSources() {
        return this.equalInformationSources;
    }
    
    public void setLikeInformationSources(String likeInformationSources) {
        this.likeInformationSources = likeInformationSources;
    }
    
    public String getLikeInformationSources() {
        return this.likeInformationSources;
    }
    
    public void setInInformationSourcess(String inInformationSourcess) {
        this.inInformationSourcess = inInformationSourcess;
    }
    
    public String getInInformationSourcess() {
        return this.inInformationSourcess;
    }
    public void setEqualInfoUpdateTime(String equalInfoUpdateTime) {
        this.equalInfoUpdateTime = equalInfoUpdateTime;
    }
    
    public String getEqualInfoUpdateTime() {
        return this.equalInfoUpdateTime;
    }
    
    public void setLikeInfoUpdateTime(String likeInfoUpdateTime) {
        this.likeInfoUpdateTime = likeInfoUpdateTime;
    }
    
    public String getLikeInfoUpdateTime() {
        return this.likeInfoUpdateTime;
    }
    
    public void setInInfoUpdateTimes(String inInfoUpdateTimes) {
        this.inInfoUpdateTimes = inInfoUpdateTimes;
    }
    
    public String getInInfoUpdateTimes() {
        return this.inInfoUpdateTimes;
    }
    public void setEqualLatitude(String equalLatitude) {
        this.equalLatitude = equalLatitude;
    }
    
    public String getEqualLatitude() {
        return this.equalLatitude;
    }
    
    public void setLikeLatitude(String likeLatitude) {
        this.likeLatitude = likeLatitude;
    }
    
    public String getLikeLatitude() {
        return this.likeLatitude;
    }
    
    public void setInLatitudes(String inLatitudes) {
        this.inLatitudes = inLatitudes;
    }
    
    public String getInLatitudes() {
        return this.inLatitudes;
    }
    public void setEqualLongitude(String equalLongitude) {
        this.equalLongitude = equalLongitude;
    }
    
    public String getEqualLongitude() {
        return this.equalLongitude;
    }
    
    public void setLikeLongitude(String likeLongitude) {
        this.likeLongitude = likeLongitude;
    }
    
    public String getLikeLongitude() {
        return this.likeLongitude;
    }
    
    public void setInLongitudes(String inLongitudes) {
        this.inLongitudes = inLongitudes;
    }
    
    public String getInLongitudes() {
        return this.inLongitudes;
    }
    public void setEqualTraffic(String equalTraffic) {
        this.equalTraffic = equalTraffic;
    }
    
    public String getEqualTraffic() {
        return this.equalTraffic;
    }
    
    public void setLikeTraffic(String likeTraffic) {
        this.likeTraffic = likeTraffic;
    }
    
    public String getLikeTraffic() {
        return this.likeTraffic;
    }
    
    public void setInTraffics(String inTraffics) {
        this.inTraffics = inTraffics;
    }
    
    public String getInTraffics() {
        return this.inTraffics;
    }
    public void setEqualKindergarten(String equalKindergarten) {
        this.equalKindergarten = equalKindergarten;
    }
    
    public String getEqualKindergarten() {
        return this.equalKindergarten;
    }
    
    public void setLikeKindergarten(String likeKindergarten) {
        this.likeKindergarten = likeKindergarten;
    }
    
    public String getLikeKindergarten() {
        return this.likeKindergarten;
    }
    
    public void setInKindergartens(String inKindergartens) {
        this.inKindergartens = inKindergartens;
    }
    
    public String getInKindergartens() {
        return this.inKindergartens;
    }
    public void setEqualSchool(String equalSchool) {
        this.equalSchool = equalSchool;
    }
    
    public String getEqualSchool() {
        return this.equalSchool;
    }
    
    public void setLikeSchool(String likeSchool) {
        this.likeSchool = likeSchool;
    }
    
    public String getLikeSchool() {
        return this.likeSchool;
    }
    
    public void setInSchools(String inSchools) {
        this.inSchools = inSchools;
    }
    
    public String getInSchools() {
        return this.inSchools;
    }
    public void setEqualPowerCenter(String equalPowerCenter) {
        this.equalPowerCenter = equalPowerCenter;
    }
    
    public String getEqualPowerCenter() {
        return this.equalPowerCenter;
    }
    
    public void setLikePowerCenter(String likePowerCenter) {
        this.likePowerCenter = likePowerCenter;
    }
    
    public String getLikePowerCenter() {
        return this.likePowerCenter;
    }
    
    public void setInPowerCenters(String inPowerCenters) {
        this.inPowerCenters = inPowerCenters;
    }
    
    public String getInPowerCenters() {
        return this.inPowerCenters;
    }
    public void setEqualHospital(String equalHospital) {
        this.equalHospital = equalHospital;
    }
    
    public String getEqualHospital() {
        return this.equalHospital;
    }
    
    public void setLikeHospital(String likeHospital) {
        this.likeHospital = likeHospital;
    }
    
    public String getLikeHospital() {
        return this.likeHospital;
    }
    
    public void setInHospitals(String inHospitals) {
        this.inHospitals = inHospitals;
    }
    
    public String getInHospitals() {
        return this.inHospitals;
    }
    public void setEqualBank(String equalBank) {
        this.equalBank = equalBank;
    }
    
    public String getEqualBank() {
        return this.equalBank;
    }
    
    public void setLikeBank(String likeBank) {
        this.likeBank = likeBank;
    }
    
    public String getLikeBank() {
        return this.likeBank;
    }
    
    public void setInBanks(String inBanks) {
        this.inBanks = inBanks;
    }
    
    public String getInBanks() {
        return this.inBanks;
    }
    public void setEqualPost(String equalPost) {
        this.equalPost = equalPost;
    }
    
    public String getEqualPost() {
        return this.equalPost;
    }
    
    public void setLikePost(String likePost) {
        this.likePost = likePost;
    }
    
    public String getLikePost() {
        return this.likePost;
    }
    
    public void setInPosts(String inPosts) {
        this.inPosts = inPosts;
    }
    
    public String getInPosts() {
        return this.inPosts;
    }
    public void setEqualOther(String equalOther) {
        this.equalOther = equalOther;
    }
    
    public String getEqualOther() {
        return this.equalOther;
    }
    
    public void setLikeOther(String likeOther) {
        this.likeOther = likeOther;
    }
    
    public String getLikeOther() {
        return this.likeOther;
    }
    
    public void setInOthers(String inOthers) {
        this.inOthers = inOthers;
    }
    
    public String getInOthers() {
        return this.inOthers;
    }
    public void setEqualHousePriceUnit(String equalHousePriceUnit) {
        this.equalHousePriceUnit = equalHousePriceUnit;
    }
    
    public String getEqualHousePriceUnit() {
        return this.equalHousePriceUnit;
    }
    
    public void setLikeHousePriceUnit(String likeHousePriceUnit) {
        this.likeHousePriceUnit = likeHousePriceUnit;
    }
    
    public String getLikeHousePriceUnit() {
        return this.likeHousePriceUnit;
    }
    
    public void setInHousePriceUnits(String inHousePriceUnits) {
        this.inHousePriceUnits = inHousePriceUnits;
    }
    
    public String getInHousePriceUnits() {
        return this.inHousePriceUnits;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalHouseName)){
			criterias.add(Restrictions.eq("houseName", equalHouseName));
		}
	    if(StringUtils.isNotBlank(likeHouseName)){
			criterias.add(Restrictions.allLike("houseName", likeHouseName));
		}
		 if(StringUtils.isNotBlank(inHouseNames)){
			criterias.add(Restrictions.in("houseName", StringUtils.split(inHouseNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseOtherName)){
			criterias.add(Restrictions.eq("houseOtherName", equalHouseOtherName));
		}
	    if(StringUtils.isNotBlank(likeHouseOtherName)){
			criterias.add(Restrictions.allLike("houseOtherName", likeHouseOtherName));
		}
		 if(StringUtils.isNotBlank(inHouseOtherNames)){
			criterias.add(Restrictions.in("houseOtherName", StringUtils.split(inHouseOtherNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseCity)){
			criterias.add(Restrictions.eq("houseCity", equalHouseCity));
		}
	    if(StringUtils.isNotBlank(likeHouseCity)){
			criterias.add(Restrictions.allLike("houseCity", likeHouseCity));
		}
		 if(StringUtils.isNotBlank(inHouseCitys)){
			criterias.add(Restrictions.in("houseCity", StringUtils.split(inHouseCitys, ",")));
		}
	    if(StringUtils.isNotBlank(equalHousePart)){
			criterias.add(Restrictions.eq("housePart", equalHousePart));
		}
	    if(StringUtils.isNotBlank(likeHousePart)){
			criterias.add(Restrictions.allLike("housePart", likeHousePart));
		}
		 if(StringUtils.isNotBlank(inHouseParts)){
			criterias.add(Restrictions.in("housePart", StringUtils.split(inHouseParts, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseAddress)){
			criterias.add(Restrictions.eq("houseAddress", equalHouseAddress));
		}
	    if(StringUtils.isNotBlank(likeHouseAddress)){
			criterias.add(Restrictions.allLike("houseAddress", likeHouseAddress));
		}
		 if(StringUtils.isNotBlank(inHouseAddresss)){
			criterias.add(Restrictions.in("houseAddress", StringUtils.split(inHouseAddresss, ",")));
		}
	    if(StringUtils.isNotBlank(equalHousePrice)){
			criterias.add(Restrictions.eq("housePrice", equalHousePrice));
		}
	    if(StringUtils.isNotBlank(likeHousePrice)){
			criterias.add(Restrictions.allLike("housePrice", likeHousePrice));
		}
		 if(StringUtils.isNotBlank(inHousePrices)){
			criterias.add(Restrictions.in("housePrice", StringUtils.split(inHousePrices, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseType)){
			criterias.add(Restrictions.eq("houseType", equalHouseType));
		}
	    if(StringUtils.isNotBlank(likeHouseType)){
			criterias.add(Restrictions.allLike("houseType", likeHouseType));
		}
		 if(StringUtils.isNotBlank(inHouseTypes)){
			criterias.add(Restrictions.in("houseType", StringUtils.split(inHouseTypes, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseSaleStatus)){
			criterias.add(Restrictions.eq("houseSaleStatus", equalHouseSaleStatus));
		}
	    if(StringUtils.isNotBlank(likeHouseSaleStatus)){
			criterias.add(Restrictions.allLike("houseSaleStatus", likeHouseSaleStatus));
		}
		 if(StringUtils.isNotBlank(inHouseSaleStatuss)){
			criterias.add(Restrictions.in("houseSaleStatus", StringUtils.split(inHouseSaleStatuss, ",")));
		}
	    if(StringUtils.isNotBlank(equalIsNewHouse)){
			criterias.add(Restrictions.eq("isNewHouse", equalIsNewHouse));
		}
	    if(StringUtils.isNotBlank(likeIsNewHouse)){
			criterias.add(Restrictions.allLike("isNewHouse", likeIsNewHouse));
		}
		 if(StringUtils.isNotBlank(inIsNewHouses)){
			criterias.add(Restrictions.in("isNewHouse", StringUtils.split(inIsNewHouses, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseAllPrice)){
			criterias.add(Restrictions.eq("houseAllPrice", equalHouseAllPrice));
		}
	    if(StringUtils.isNotBlank(likeHouseAllPrice)){
			criterias.add(Restrictions.allLike("houseAllPrice", likeHouseAllPrice));
		}
		 if(StringUtils.isNotBlank(inHouseAllPrices)){
			criterias.add(Restrictions.in("houseAllPrice", StringUtils.split(inHouseAllPrices, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseTab)){
			criterias.add(Restrictions.eq("houseTab", equalHouseTab));
		}
	    if(StringUtils.isNotBlank(likeHouseTab)){
			criterias.add(Restrictions.allLike("houseTab", likeHouseTab));
		}
		 if(StringUtils.isNotBlank(inHouseTabs)){
			criterias.add(Restrictions.in("houseTab", StringUtils.split(inHouseTabs, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseGrade)){
			criterias.add(Restrictions.eq("houseGrade", equalHouseGrade));
		}
	    if(StringUtils.isNotBlank(likeHouseGrade)){
			criterias.add(Restrictions.allLike("houseGrade", likeHouseGrade));
		}
		 if(StringUtils.isNotBlank(inHouseGrades)){
			criterias.add(Restrictions.in("houseGrade", StringUtils.split(inHouseGrades, ",")));
		}
	    if(StringUtils.isNotBlank(equalLatestSaleTime)){
			criterias.add(Restrictions.eq("latestSaleTime", equalLatestSaleTime));
		}
	    if(StringUtils.isNotBlank(likeLatestSaleTime)){
			criterias.add(Restrictions.allLike("latestSaleTime", likeLatestSaleTime));
		}
		 if(StringUtils.isNotBlank(inLatestSaleTimes)){
			criterias.add(Restrictions.in("latestSaleTime", StringUtils.split(inLatestSaleTimes, ",")));
		}
	    if(StringUtils.isNotBlank(equalMainUnit)){
			criterias.add(Restrictions.eq("mainUnit", equalMainUnit));
		}
	    if(StringUtils.isNotBlank(likeMainUnit)){
			criterias.add(Restrictions.allLike("mainUnit", likeMainUnit));
		}
		 if(StringUtils.isNotBlank(inMainUnits)){
			criterias.add(Restrictions.in("mainUnit", StringUtils.split(inMainUnits, ",")));
		}
	    if(StringUtils.isNotBlank(equalSalesOfficeAddress)){
			criterias.add(Restrictions.eq("salesOfficeAddress", equalSalesOfficeAddress));
		}
	    if(StringUtils.isNotBlank(likeSalesOfficeAddress)){
			criterias.add(Restrictions.allLike("salesOfficeAddress", likeSalesOfficeAddress));
		}
		 if(StringUtils.isNotBlank(inSalesOfficeAddresss)){
			criterias.add(Restrictions.in("salesOfficeAddress", StringUtils.split(inSalesOfficeAddresss, ",")));
		}
	    if(StringUtils.isNotBlank(equalDevelopers)){
			criterias.add(Restrictions.eq("developers", equalDevelopers));
		}
	    if(StringUtils.isNotBlank(likeDevelopers)){
			criterias.add(Restrictions.allLike("developers", likeDevelopers));
		}
		 if(StringUtils.isNotBlank(inDeveloperss)){
			criterias.add(Restrictions.in("developers", StringUtils.split(inDeveloperss, ",")));
		}
	    if(StringUtils.isNotBlank(equalPropertyCategory)){
			criterias.add(Restrictions.eq("propertyCategory", equalPropertyCategory));
		}
	    if(StringUtils.isNotBlank(likePropertyCategory)){
			criterias.add(Restrictions.allLike("propertyCategory", likePropertyCategory));
		}
		 if(StringUtils.isNotBlank(inPropertyCategorys)){
			criterias.add(Restrictions.in("propertyCategory", StringUtils.split(inPropertyCategorys, ",")));
		}
	    if(StringUtils.isNotBlank(equalBuildingArea)){
			criterias.add(Restrictions.eq("buildingArea", equalBuildingArea));
		}
	    if(StringUtils.isNotBlank(likeBuildingArea)){
			criterias.add(Restrictions.allLike("buildingArea", likeBuildingArea));
		}
		 if(StringUtils.isNotBlank(inBuildingAreas)){
			criterias.add(Restrictions.in("buildingArea", StringUtils.split(inBuildingAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalAreaCovered)){
			criterias.add(Restrictions.eq("areaCovered", equalAreaCovered));
		}
	    if(StringUtils.isNotBlank(likeAreaCovered)){
			criterias.add(Restrictions.allLike("areaCovered", likeAreaCovered));
		}
		 if(StringUtils.isNotBlank(inAreaCovereds)){
			criterias.add(Restrictions.in("areaCovered", StringUtils.split(inAreaCovereds, ",")));
		}
	    if(StringUtils.isNotBlank(equalAfforestationRate)){
			criterias.add(Restrictions.eq("afforestationRate", equalAfforestationRate));
		}
	    if(StringUtils.isNotBlank(likeAfforestationRate)){
			criterias.add(Restrictions.allLike("afforestationRate", likeAfforestationRate));
		}
		 if(StringUtils.isNotBlank(inAfforestationRates)){
			criterias.add(Restrictions.in("afforestationRate", StringUtils.split(inAfforestationRates, ",")));
		}
	    if(StringUtils.isNotBlank(equalPlotRatio)){
			criterias.add(Restrictions.eq("plotRatio", equalPlotRatio));
		}
	    if(StringUtils.isNotBlank(likePlotRatio)){
			criterias.add(Restrictions.allLike("plotRatio", likePlotRatio));
		}
		 if(StringUtils.isNotBlank(inPlotRatios)){
			criterias.add(Restrictions.in("plotRatio", StringUtils.split(inPlotRatios, ",")));
		}
	    if(StringUtils.isNotBlank(equalParkingLot)){
			criterias.add(Restrictions.eq("parkingLot", equalParkingLot));
		}
	    if(StringUtils.isNotBlank(likeParkingLot)){
			criterias.add(Restrictions.allLike("parkingLot", likeParkingLot));
		}
		 if(StringUtils.isNotBlank(inParkingLots)){
			criterias.add(Restrictions.in("parkingLot", StringUtils.split(inParkingLots, ",")));
		}
	    if(StringUtils.isNotBlank(equalWaterSupplyMode)){
			criterias.add(Restrictions.eq("waterSupplyMode", equalWaterSupplyMode));
		}
	    if(StringUtils.isNotBlank(likeWaterSupplyMode)){
			criterias.add(Restrictions.allLike("waterSupplyMode", likeWaterSupplyMode));
		}
		 if(StringUtils.isNotBlank(inWaterSupplyModes)){
			criterias.add(Restrictions.in("waterSupplyMode", StringUtils.split(inWaterSupplyModes, ",")));
		}
	    if(StringUtils.isNotBlank(equalPowerSupplyMode)){
			criterias.add(Restrictions.eq("powerSupplyMode", equalPowerSupplyMode));
		}
	    if(StringUtils.isNotBlank(likePowerSupplyMode)){
			criterias.add(Restrictions.allLike("powerSupplyMode", likePowerSupplyMode));
		}
		 if(StringUtils.isNotBlank(inPowerSupplyModes)){
			criterias.add(Restrictions.in("powerSupplyMode", StringUtils.split(inPowerSupplyModes, ",")));
		}
	    if(StringUtils.isNotBlank(equalPropertyRightYears)){
			criterias.add(Restrictions.eq("propertyRightYears", equalPropertyRightYears));
		}
	    if(StringUtils.isNotBlank(likePropertyRightYears)){
			criterias.add(Restrictions.allLike("propertyRightYears", likePropertyRightYears));
		}
		 if(StringUtils.isNotBlank(inPropertyRightYearss)){
			criterias.add(Restrictions.in("propertyRightYears", StringUtils.split(inPropertyRightYearss, ",")));
		}
	    if(StringUtils.isNotBlank(equalPropertyCompany)){
			criterias.add(Restrictions.eq("propertyCompany", equalPropertyCompany));
		}
	    if(StringUtils.isNotBlank(likePropertyCompany)){
			criterias.add(Restrictions.allLike("propertyCompany", likePropertyCompany));
		}
		 if(StringUtils.isNotBlank(inPropertyCompanys)){
			criterias.add(Restrictions.in("propertyCompany", StringUtils.split(inPropertyCompanys, ",")));
		}
	    if(StringUtils.isNotBlank(equalPlanningHouseholds)){
			criterias.add(Restrictions.eq("planningHouseholds", equalPlanningHouseholds));
		}
	    if(StringUtils.isNotBlank(likePlanningHouseholds)){
			criterias.add(Restrictions.allLike("planningHouseholds", likePlanningHouseholds));
		}
		 if(StringUtils.isNotBlank(inPlanningHouseholdss)){
			criterias.add(Restrictions.in("planningHouseholds", StringUtils.split(inPlanningHouseholdss, ",")));
		}
	    if(StringUtils.isNotBlank(equalTimeOfDelivery)){
			criterias.add(Restrictions.eq("timeOfDelivery", equalTimeOfDelivery));
		}
	    if(StringUtils.isNotBlank(likeTimeOfDelivery)){
			criterias.add(Restrictions.allLike("timeOfDelivery", likeTimeOfDelivery));
		}
		 if(StringUtils.isNotBlank(inTimeOfDeliverys)){
			criterias.add(Restrictions.in("timeOfDelivery", StringUtils.split(inTimeOfDeliverys, ",")));
		}
	    if(StringUtils.isNotBlank(equalPropertyCosts)){
			criterias.add(Restrictions.eq("propertyCosts", equalPropertyCosts));
		}
	    if(StringUtils.isNotBlank(likePropertyCosts)){
			criterias.add(Restrictions.allLike("propertyCosts", likePropertyCosts));
		}
		 if(StringUtils.isNotBlank(inPropertyCostss)){
			criterias.add(Restrictions.in("propertyCosts", StringUtils.split(inPropertyCostss, ",")));
		}
	    if(StringUtils.isNotBlank(equalInformationSources)){
			criterias.add(Restrictions.eq("informationSources", equalInformationSources));
		}
	    if(StringUtils.isNotBlank(likeInformationSources)){
			criterias.add(Restrictions.allLike("informationSources", likeInformationSources));
		}
		 if(StringUtils.isNotBlank(inInformationSourcess)){
			criterias.add(Restrictions.in("informationSources", StringUtils.split(inInformationSourcess, ",")));
		}
	    if(StringUtils.isNotBlank(equalInfoUpdateTime)){
			criterias.add(Restrictions.eq("infoUpdateTime", DateUtil.format(equalInfoUpdateTime)));
		}
	    if(StringUtils.isNotBlank(likeInfoUpdateTime)){
			criterias.add(Restrictions.allLike("infoUpdateTime", likeInfoUpdateTime));
		}
		 if(StringUtils.isNotBlank(inInfoUpdateTimes)){
			criterias.add(Restrictions.in("infoUpdateTime", StringUtils.split(inInfoUpdateTimes, ",")));
		}
	    if(StringUtils.isNotBlank(equalLatitude)){
			criterias.add(Restrictions.eq("latitude", equalLatitude));
		}
	    if(StringUtils.isNotBlank(likeLatitude)){
			criterias.add(Restrictions.allLike("latitude", likeLatitude));
		}
		 if(StringUtils.isNotBlank(inLatitudes)){
			criterias.add(Restrictions.in("latitude", StringUtils.split(inLatitudes, ",")));
		}
	    if(StringUtils.isNotBlank(equalLongitude)){
			criterias.add(Restrictions.eq("longitude", equalLongitude));
		}
	    if(StringUtils.isNotBlank(likeLongitude)){
			criterias.add(Restrictions.allLike("longitude", likeLongitude));
		}
		 if(StringUtils.isNotBlank(inLongitudes)){
			criterias.add(Restrictions.in("longitude", StringUtils.split(inLongitudes, ",")));
		}
	    if(StringUtils.isNotBlank(equalTraffic)){
			criterias.add(Restrictions.eq("traffic", equalTraffic));
		}
	    if(StringUtils.isNotBlank(likeTraffic)){
			criterias.add(Restrictions.allLike("traffic", likeTraffic));
		}
		 if(StringUtils.isNotBlank(inTraffics)){
			criterias.add(Restrictions.in("traffic", StringUtils.split(inTraffics, ",")));
		}
	    if(StringUtils.isNotBlank(equalKindergarten)){
			criterias.add(Restrictions.eq("kindergarten", equalKindergarten));
		}
	    if(StringUtils.isNotBlank(likeKindergarten)){
			criterias.add(Restrictions.allLike("kindergarten", likeKindergarten));
		}
		 if(StringUtils.isNotBlank(inKindergartens)){
			criterias.add(Restrictions.in("kindergarten", StringUtils.split(inKindergartens, ",")));
		}
	    if(StringUtils.isNotBlank(equalSchool)){
			criterias.add(Restrictions.eq("school", equalSchool));
		}
	    if(StringUtils.isNotBlank(likeSchool)){
			criterias.add(Restrictions.allLike("school", likeSchool));
		}
		 if(StringUtils.isNotBlank(inSchools)){
			criterias.add(Restrictions.in("school", StringUtils.split(inSchools, ",")));
		}
	    if(StringUtils.isNotBlank(equalPowerCenter)){
			criterias.add(Restrictions.eq("powerCenter", equalPowerCenter));
		}
	    if(StringUtils.isNotBlank(likePowerCenter)){
			criterias.add(Restrictions.allLike("powerCenter", likePowerCenter));
		}
		 if(StringUtils.isNotBlank(inPowerCenters)){
			criterias.add(Restrictions.in("powerCenter", StringUtils.split(inPowerCenters, ",")));
		}
	    if(StringUtils.isNotBlank(equalHospital)){
			criterias.add(Restrictions.eq("hospital", equalHospital));
		}
	    if(StringUtils.isNotBlank(likeHospital)){
			criterias.add(Restrictions.allLike("hospital", likeHospital));
		}
		 if(StringUtils.isNotBlank(inHospitals)){
			criterias.add(Restrictions.in("hospital", StringUtils.split(inHospitals, ",")));
		}
	    if(StringUtils.isNotBlank(equalBank)){
			criterias.add(Restrictions.eq("bank", equalBank));
		}
	    if(StringUtils.isNotBlank(likeBank)){
			criterias.add(Restrictions.allLike("bank", likeBank));
		}
		 if(StringUtils.isNotBlank(inBanks)){
			criterias.add(Restrictions.in("bank", StringUtils.split(inBanks, ",")));
		}
	    if(StringUtils.isNotBlank(equalPost)){
			criterias.add(Restrictions.eq("post", equalPost));
		}
	    if(StringUtils.isNotBlank(likePost)){
			criterias.add(Restrictions.allLike("post", likePost));
		}
		 if(StringUtils.isNotBlank(inPosts)){
			criterias.add(Restrictions.in("post", StringUtils.split(inPosts, ",")));
		}
	    if(StringUtils.isNotBlank(equalOther)){
			criterias.add(Restrictions.eq("other", equalOther));
		}
	    if(StringUtils.isNotBlank(likeOther)){
			criterias.add(Restrictions.allLike("other", likeOther));
		}
		 if(StringUtils.isNotBlank(inOthers)){
			criterias.add(Restrictions.in("other", StringUtils.split(inOthers, ",")));
		}
	    if(StringUtils.isNotBlank(equalHousePriceUnit)){
			criterias.add(Restrictions.eq("housePriceUnit", equalHousePriceUnit));
		}
	    if(StringUtils.isNotBlank(likeHousePriceUnit)){
			criterias.add(Restrictions.allLike("housePriceUnit", likeHousePriceUnit));
		}
		if(StringUtils.isNotBlank(inHousePriceUnits)){
			criterias.add(Restrictions.in("housePriceUnit", StringUtils.split(inHousePriceUnits, ",")));
		}
		if(StringUtils.isNotBlank(likeRenovationCondition)){
			criterias.add(Restrictions.allLike("renovationCondition", likeRenovationCondition));
		}
	}
}