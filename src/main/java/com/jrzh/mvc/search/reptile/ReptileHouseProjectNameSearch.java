package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileHouseProjectNameSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 区名
     */
    private String equalAreaName;
    
    /**
     * Like 区名
     */
    private String likeAreaName;
    
     /**
     * In 区名 英文逗号分隔
     */
    private String inAreaNames;
    
    /**
     * Equal 城市名
     */
    private String equalCityName;
    
    /**
     * Like 城市名
     */
    private String likeCityName;
    
     /**
     * In 城市名 英文逗号分隔
     */
    private String inCityNames;
    
    /**
     * Equal 项目名称
     */
    private String equalProjectName;
    
    /**
     * Like 项目名称
     */
    private String likeProjectName;
    
     /**
     * In 项目名称 英文逗号分隔
     */
    private String inProjectNames;
    
    /**
     * Equal 总套数
     */
    private String equalProjectTotalNumber;
    
    /**
     * Like 总套数
     */
    private String likeProjectTotalNumber;
    
     /**
     * In 总套数 英文逗号分隔
     */
    private String inProjectTotalNumbers;
    
    /**
     * Equal 总面积
     */
    private String equalProjectTotalArea;
    
    /**
     * Like 总面积
     */
    private String likeProjectTotalArea;
    
     /**
     * In 总面积 英文逗号分隔
     */
    private String inProjectTotalAreas;
    
    /**
     * Equal 住宅套数
     */
    private String equalProjectHouseNumber;
    
    /**
     * Like 住宅套数
     */
    private String likeProjectHouseNumber;
    
     /**
     * In 住宅套数 英文逗号分隔
     */
    private String inProjectHouseNumbers;
    
    /**
     * Equal 住宅面积
     */
    private String equalProjectHouseArea;
    
    /**
     * Like 住宅面积
     */
    private String likeProjectHouseArea;
    
     /**
     * In 住宅面积 英文逗号分隔
     */
    private String inProjectHouseAreas;
    
    /**
     * Equal 可售总套数(未售)
     */
    private String equalProjectTotalHouseUnsoldNumber;
    
    /**
     * Like 可售总套数(未售)
     */
    private String likeProjectTotalHouseUnsoldNumber;
    
     /**
     * In 可售总套数(未售) 英文逗号分隔
     */
    private String inProjectTotalHouseUnsoldNumbers;
    
    /**
     * Equal 可售总面积(未售)
     */
    private String equalProjectTotalHouseUnsoldArea;
    
    /**
     * Like 可售总面积(未售)
     */
    private String likeProjectTotalHouseUnsoldArea;
    
     /**
     * In 可售总面积(未售) 英文逗号分隔
     */
    private String inProjectTotalHouseUnsoldAreas;
    
    /**
     * Equal 可售住宅套数
     */
    private String equalProjectHouseUnsoldNumber;
    
    /**
     * Like 可售住宅套数
     */
    private String likeProjectHouseUnsoldNumber;
    
     /**
     * In 可售住宅套数 英文逗号分隔
     */
    private String inProjectHouseUnsoldNumbers;
    
    /**
     * Equal 可售住宅面积
     */
    private String equalProjectHouseUnsoldArea;
    
    /**
     * Like 可售住宅面积
     */
    private String likeProjectHouseUnsoldArea;
    
     /**
     * In 可售住宅面积 英文逗号分隔
     */
    private String inProjectHouseUnsoldAreas;
    
    /**
     * Equal 已售总套数
     */
    private String equalProjectTotalHouseSoldNumber;
    
    /**
     * Like 已售总套数
     */
    private String likeProjectTotalHouseSoldNumber;
    
     /**
     * In 已售总套数 英文逗号分隔
     */
    private String inProjectTotalHouseSoldNumbers;
    
    /**
     * Equal 已售总面积
     */
    private String equalProjectTotalHouseSoldArea;
    
    /**
     * Like 已售总面积
     */
    private String likeProjectTotalHouseSoldArea;
    
     /**
     * In 已售总面积 英文逗号分隔
     */
    private String inProjectTotalHouseSoldAreas;
    
    /**
     * Equal 已售住宅套数
     */
    private String equalProjectHouseSoldNumber;
    
    /**
     * Like 已售住宅套数
     */
    private String likeProjectHouseSoldNumber;
    
     /**
     * In 已售住宅套数 英文逗号分隔
     */
    private String inProjectHouseSoldNumbers;
    
    /**
     * Equal 已售住宅面积
     */
    private String equalProjectHouseSoldArea;
    
    /**
     * Like 已售住宅面积
     */
    private String likeProjectHouseSoldArea;
    
     /**
     * In 已售住宅面积 英文逗号分隔
     */
    private String inProjectHouseSoldAreas;
    
    /**
     * Equal 已登记总套数
     */
    private String equalProjectRegisterTotalHouseNumber;
    
    /**
     * Like 已登记总套数
     */
    private String likeProjectRegisterTotalHouseNumber;
    
     /**
     * In 已登记总套数 英文逗号分隔
     */
    private String inProjectRegisterTotalHouseNumbers;
    
    /**
     * Equal 已登记总面积
     */
    private String equalProjectRegisterTotalHouseArea;
    
    /**
     * Like 已登记总面积
     */
    private String likeProjectRegisterTotalHouseArea;
    
     /**
     * In 已登记总面积 英文逗号分隔
     */
    private String inProjectRegisterTotalHouseAreas;
    
    /**
     * Equal 已登记住宅套数
     */
    private String equalProjectRegisterHouseNumber;
    
    /**
     * Like 已登记住宅套数
     */
    private String likeProjectRegisterHouseNumber;
    
     /**
     * In 已登记住宅套数 英文逗号分隔
     */
    private String inProjectRegisterHouseNumbers;
    
    /**
     * Equal 已登记住宅面积
     */
    private String equalProjectRegisterHouseArea;
    
    /**
     * Like 已登记住宅面积
     */
    private String likeProjectRegisterHouseArea;
    
     /**
     * In 已登记住宅面积 英文逗号分隔
     */
    private String inProjectRegisterHouseAreas;
    
    /**
     * Equal 合同撤消总套数
     */
    private String equalProjectCancellationsHouseArea;
    
    /**
     * Like 合同撤消总套数
     */
    private String likeProjectCancellationsHouseArea;
    
     /**
     * In 合同撤消总套数 英文逗号分隔
     */
    private String inProjectCancellationsHouseAreas;
    
    /**
     * Equal 定价逾期总次数
     */
    private String equalProjectTotalPriceOverdueNumber;
    
    /**
     * Like 定价逾期总次数
     */
    private String likeProjectTotalPriceOverdueNumber;
    
     /**
     * In 定价逾期总次数 英文逗号分隔
     */
    private String inProjectTotalPriceOverdueNumbers;
    
    /**
     * Equal 房管局项目ID
     */
    private String equalProjectId;
    
    /**
     * Like 房管局项目ID
     */
    private String likeProjectId;
    
     /**
     * In 房管局项目ID 英文逗号分隔
     */
    private String inProjectIds;
    
    /**
     * Equal 信息更新时间
     */
    private String equalInfoUpdateTime;
    
    /**
     * Like 信息更新时间
     */
    private String likeInfoUpdateTime;
    
     /**
     * In 信息更新时间 英文逗号分隔
     */
    private String inInfoUpdateTimes;
    

    public void setEqualAreaName(String equalAreaName) {
        this.equalAreaName = equalAreaName;
    }
    
    public String getEqualAreaName() {
        return this.equalAreaName;
    }
    
    public void setLikeAreaName(String likeAreaName) {
        this.likeAreaName = likeAreaName;
    }
    
    public String getLikeAreaName() {
        return this.likeAreaName;
    }
    
    public void setInAreaNames(String inAreaNames) {
        this.inAreaNames = inAreaNames;
    }
    
    public String getInAreaNames() {
        return this.inAreaNames;
    }
    public void setEqualCityName(String equalCityName) {
        this.equalCityName = equalCityName;
    }
    
    public String getEqualCityName() {
        return this.equalCityName;
    }
    
    public void setLikeCityName(String likeCityName) {
        this.likeCityName = likeCityName;
    }
    
    public String getLikeCityName() {
        return this.likeCityName;
    }
    
    public void setInCityNames(String inCityNames) {
        this.inCityNames = inCityNames;
    }
    
    public String getInCityNames() {
        return this.inCityNames;
    }
    public void setEqualProjectName(String equalProjectName) {
        this.equalProjectName = equalProjectName;
    }
    
    public String getEqualProjectName() {
        return this.equalProjectName;
    }
    
    public void setLikeProjectName(String likeProjectName) {
        this.likeProjectName = likeProjectName;
    }
    
    public String getLikeProjectName() {
        return this.likeProjectName;
    }
    
    public void setInProjectNames(String inProjectNames) {
        this.inProjectNames = inProjectNames;
    }
    
    public String getInProjectNames() {
        return this.inProjectNames;
    }
    public void setEqualProjectTotalNumber(String equalProjectTotalNumber) {
        this.equalProjectTotalNumber = equalProjectTotalNumber;
    }
    
    public String getEqualProjectTotalNumber() {
        return this.equalProjectTotalNumber;
    }
    
    public void setLikeProjectTotalNumber(String likeProjectTotalNumber) {
        this.likeProjectTotalNumber = likeProjectTotalNumber;
    }
    
    public String getLikeProjectTotalNumber() {
        return this.likeProjectTotalNumber;
    }
    
    public void setInProjectTotalNumbers(String inProjectTotalNumbers) {
        this.inProjectTotalNumbers = inProjectTotalNumbers;
    }
    
    public String getInProjectTotalNumbers() {
        return this.inProjectTotalNumbers;
    }
    public void setEqualProjectTotalArea(String equalProjectTotalArea) {
        this.equalProjectTotalArea = equalProjectTotalArea;
    }
    
    public String getEqualProjectTotalArea() {
        return this.equalProjectTotalArea;
    }
    
    public void setLikeProjectTotalArea(String likeProjectTotalArea) {
        this.likeProjectTotalArea = likeProjectTotalArea;
    }
    
    public String getLikeProjectTotalArea() {
        return this.likeProjectTotalArea;
    }
    
    public void setInProjectTotalAreas(String inProjectTotalAreas) {
        this.inProjectTotalAreas = inProjectTotalAreas;
    }
    
    public String getInProjectTotalAreas() {
        return this.inProjectTotalAreas;
    }
    public void setEqualProjectHouseNumber(String equalProjectHouseNumber) {
        this.equalProjectHouseNumber = equalProjectHouseNumber;
    }
    
    public String getEqualProjectHouseNumber() {
        return this.equalProjectHouseNumber;
    }
    
    public void setLikeProjectHouseNumber(String likeProjectHouseNumber) {
        this.likeProjectHouseNumber = likeProjectHouseNumber;
    }
    
    public String getLikeProjectHouseNumber() {
        return this.likeProjectHouseNumber;
    }
    
    public void setInProjectHouseNumbers(String inProjectHouseNumbers) {
        this.inProjectHouseNumbers = inProjectHouseNumbers;
    }
    
    public String getInProjectHouseNumbers() {
        return this.inProjectHouseNumbers;
    }
    public void setEqualProjectHouseArea(String equalProjectHouseArea) {
        this.equalProjectHouseArea = equalProjectHouseArea;
    }
    
    public String getEqualProjectHouseArea() {
        return this.equalProjectHouseArea;
    }
    
    public void setLikeProjectHouseArea(String likeProjectHouseArea) {
        this.likeProjectHouseArea = likeProjectHouseArea;
    }
    
    public String getLikeProjectHouseArea() {
        return this.likeProjectHouseArea;
    }
    
    public void setInProjectHouseAreas(String inProjectHouseAreas) {
        this.inProjectHouseAreas = inProjectHouseAreas;
    }
    
    public String getInProjectHouseAreas() {
        return this.inProjectHouseAreas;
    }
    public void setEqualProjectTotalHouseUnsoldNumber(String equalProjectTotalHouseUnsoldNumber) {
        this.equalProjectTotalHouseUnsoldNumber = equalProjectTotalHouseUnsoldNumber;
    }
    
    public String getEqualProjectTotalHouseUnsoldNumber() {
        return this.equalProjectTotalHouseUnsoldNumber;
    }
    
    public void setLikeProjectTotalHouseUnsoldNumber(String likeProjectTotalHouseUnsoldNumber) {
        this.likeProjectTotalHouseUnsoldNumber = likeProjectTotalHouseUnsoldNumber;
    }
    
    public String getLikeProjectTotalHouseUnsoldNumber() {
        return this.likeProjectTotalHouseUnsoldNumber;
    }
    
    public void setInProjectTotalHouseUnsoldNumbers(String inProjectTotalHouseUnsoldNumbers) {
        this.inProjectTotalHouseUnsoldNumbers = inProjectTotalHouseUnsoldNumbers;
    }
    
    public String getInProjectTotalHouseUnsoldNumbers() {
        return this.inProjectTotalHouseUnsoldNumbers;
    }
    public void setEqualProjectTotalHouseUnsoldArea(String equalProjectTotalHouseUnsoldArea) {
        this.equalProjectTotalHouseUnsoldArea = equalProjectTotalHouseUnsoldArea;
    }
    
    public String getEqualProjectTotalHouseUnsoldArea() {
        return this.equalProjectTotalHouseUnsoldArea;
    }
    
    public void setLikeProjectTotalHouseUnsoldArea(String likeProjectTotalHouseUnsoldArea) {
        this.likeProjectTotalHouseUnsoldArea = likeProjectTotalHouseUnsoldArea;
    }
    
    public String getLikeProjectTotalHouseUnsoldArea() {
        return this.likeProjectTotalHouseUnsoldArea;
    }
    
    public void setInProjectTotalHouseUnsoldAreas(String inProjectTotalHouseUnsoldAreas) {
        this.inProjectTotalHouseUnsoldAreas = inProjectTotalHouseUnsoldAreas;
    }
    
    public String getInProjectTotalHouseUnsoldAreas() {
        return this.inProjectTotalHouseUnsoldAreas;
    }
    public void setEqualProjectHouseUnsoldNumber(String equalProjectHouseUnsoldNumber) {
        this.equalProjectHouseUnsoldNumber = equalProjectHouseUnsoldNumber;
    }
    
    public String getEqualProjectHouseUnsoldNumber() {
        return this.equalProjectHouseUnsoldNumber;
    }
    
    public void setLikeProjectHouseUnsoldNumber(String likeProjectHouseUnsoldNumber) {
        this.likeProjectHouseUnsoldNumber = likeProjectHouseUnsoldNumber;
    }
    
    public String getLikeProjectHouseUnsoldNumber() {
        return this.likeProjectHouseUnsoldNumber;
    }
    
    public void setInProjectHouseUnsoldNumbers(String inProjectHouseUnsoldNumbers) {
        this.inProjectHouseUnsoldNumbers = inProjectHouseUnsoldNumbers;
    }
    
    public String getInProjectHouseUnsoldNumbers() {
        return this.inProjectHouseUnsoldNumbers;
    }
    public void setEqualProjectHouseUnsoldArea(String equalProjectHouseUnsoldArea) {
        this.equalProjectHouseUnsoldArea = equalProjectHouseUnsoldArea;
    }
    
    public String getEqualProjectHouseUnsoldArea() {
        return this.equalProjectHouseUnsoldArea;
    }
    
    public void setLikeProjectHouseUnsoldArea(String likeProjectHouseUnsoldArea) {
        this.likeProjectHouseUnsoldArea = likeProjectHouseUnsoldArea;
    }
    
    public String getLikeProjectHouseUnsoldArea() {
        return this.likeProjectHouseUnsoldArea;
    }
    
    public void setInProjectHouseUnsoldAreas(String inProjectHouseUnsoldAreas) {
        this.inProjectHouseUnsoldAreas = inProjectHouseUnsoldAreas;
    }
    
    public String getInProjectHouseUnsoldAreas() {
        return this.inProjectHouseUnsoldAreas;
    }
    public void setEqualProjectTotalHouseSoldNumber(String equalProjectTotalHouseSoldNumber) {
        this.equalProjectTotalHouseSoldNumber = equalProjectTotalHouseSoldNumber;
    }
    
    public String getEqualProjectTotalHouseSoldNumber() {
        return this.equalProjectTotalHouseSoldNumber;
    }
    
    public void setLikeProjectTotalHouseSoldNumber(String likeProjectTotalHouseSoldNumber) {
        this.likeProjectTotalHouseSoldNumber = likeProjectTotalHouseSoldNumber;
    }
    
    public String getLikeProjectTotalHouseSoldNumber() {
        return this.likeProjectTotalHouseSoldNumber;
    }
    
    public void setInProjectTotalHouseSoldNumbers(String inProjectTotalHouseSoldNumbers) {
        this.inProjectTotalHouseSoldNumbers = inProjectTotalHouseSoldNumbers;
    }
    
    public String getInProjectTotalHouseSoldNumbers() {
        return this.inProjectTotalHouseSoldNumbers;
    }
    public void setEqualProjectTotalHouseSoldArea(String equalProjectTotalHouseSoldArea) {
        this.equalProjectTotalHouseSoldArea = equalProjectTotalHouseSoldArea;
    }
    
    public String getEqualProjectTotalHouseSoldArea() {
        return this.equalProjectTotalHouseSoldArea;
    }
    
    public void setLikeProjectTotalHouseSoldArea(String likeProjectTotalHouseSoldArea) {
        this.likeProjectTotalHouseSoldArea = likeProjectTotalHouseSoldArea;
    }
    
    public String getLikeProjectTotalHouseSoldArea() {
        return this.likeProjectTotalHouseSoldArea;
    }
    
    public void setInProjectTotalHouseSoldAreas(String inProjectTotalHouseSoldAreas) {
        this.inProjectTotalHouseSoldAreas = inProjectTotalHouseSoldAreas;
    }
    
    public String getInProjectTotalHouseSoldAreas() {
        return this.inProjectTotalHouseSoldAreas;
    }
    public void setEqualProjectHouseSoldNumber(String equalProjectHouseSoldNumber) {
        this.equalProjectHouseSoldNumber = equalProjectHouseSoldNumber;
    }
    
    public String getEqualProjectHouseSoldNumber() {
        return this.equalProjectHouseSoldNumber;
    }
    
    public void setLikeProjectHouseSoldNumber(String likeProjectHouseSoldNumber) {
        this.likeProjectHouseSoldNumber = likeProjectHouseSoldNumber;
    }
    
    public String getLikeProjectHouseSoldNumber() {
        return this.likeProjectHouseSoldNumber;
    }
    
    public void setInProjectHouseSoldNumbers(String inProjectHouseSoldNumbers) {
        this.inProjectHouseSoldNumbers = inProjectHouseSoldNumbers;
    }
    
    public String getInProjectHouseSoldNumbers() {
        return this.inProjectHouseSoldNumbers;
    }
    public void setEqualProjectHouseSoldArea(String equalProjectHouseSoldArea) {
        this.equalProjectHouseSoldArea = equalProjectHouseSoldArea;
    }
    
    public String getEqualProjectHouseSoldArea() {
        return this.equalProjectHouseSoldArea;
    }
    
    public void setLikeProjectHouseSoldArea(String likeProjectHouseSoldArea) {
        this.likeProjectHouseSoldArea = likeProjectHouseSoldArea;
    }
    
    public String getLikeProjectHouseSoldArea() {
        return this.likeProjectHouseSoldArea;
    }
    
    public void setInProjectHouseSoldAreas(String inProjectHouseSoldAreas) {
        this.inProjectHouseSoldAreas = inProjectHouseSoldAreas;
    }
    
    public String getInProjectHouseSoldAreas() {
        return this.inProjectHouseSoldAreas;
    }
    public void setEqualProjectRegisterTotalHouseNumber(String equalProjectRegisterTotalHouseNumber) {
        this.equalProjectRegisterTotalHouseNumber = equalProjectRegisterTotalHouseNumber;
    }
    
    public String getEqualProjectRegisterTotalHouseNumber() {
        return this.equalProjectRegisterTotalHouseNumber;
    }
    
    public void setLikeProjectRegisterTotalHouseNumber(String likeProjectRegisterTotalHouseNumber) {
        this.likeProjectRegisterTotalHouseNumber = likeProjectRegisterTotalHouseNumber;
    }
    
    public String getLikeProjectRegisterTotalHouseNumber() {
        return this.likeProjectRegisterTotalHouseNumber;
    }
    
    public void setInProjectRegisterTotalHouseNumbers(String inProjectRegisterTotalHouseNumbers) {
        this.inProjectRegisterTotalHouseNumbers = inProjectRegisterTotalHouseNumbers;
    }
    
    public String getInProjectRegisterTotalHouseNumbers() {
        return this.inProjectRegisterTotalHouseNumbers;
    }
    public void setEqualProjectRegisterTotalHouseArea(String equalProjectRegisterTotalHouseArea) {
        this.equalProjectRegisterTotalHouseArea = equalProjectRegisterTotalHouseArea;
    }
    
    public String getEqualProjectRegisterTotalHouseArea() {
        return this.equalProjectRegisterTotalHouseArea;
    }
    
    public void setLikeProjectRegisterTotalHouseArea(String likeProjectRegisterTotalHouseArea) {
        this.likeProjectRegisterTotalHouseArea = likeProjectRegisterTotalHouseArea;
    }
    
    public String getLikeProjectRegisterTotalHouseArea() {
        return this.likeProjectRegisterTotalHouseArea;
    }
    
    public void setInProjectRegisterTotalHouseAreas(String inProjectRegisterTotalHouseAreas) {
        this.inProjectRegisterTotalHouseAreas = inProjectRegisterTotalHouseAreas;
    }
    
    public String getInProjectRegisterTotalHouseAreas() {
        return this.inProjectRegisterTotalHouseAreas;
    }
    public void setEqualProjectRegisterHouseNumber(String equalProjectRegisterHouseNumber) {
        this.equalProjectRegisterHouseNumber = equalProjectRegisterHouseNumber;
    }
    
    public String getEqualProjectRegisterHouseNumber() {
        return this.equalProjectRegisterHouseNumber;
    }
    
    public void setLikeProjectRegisterHouseNumber(String likeProjectRegisterHouseNumber) {
        this.likeProjectRegisterHouseNumber = likeProjectRegisterHouseNumber;
    }
    
    public String getLikeProjectRegisterHouseNumber() {
        return this.likeProjectRegisterHouseNumber;
    }
    
    public void setInProjectRegisterHouseNumbers(String inProjectRegisterHouseNumbers) {
        this.inProjectRegisterHouseNumbers = inProjectRegisterHouseNumbers;
    }
    
    public String getInProjectRegisterHouseNumbers() {
        return this.inProjectRegisterHouseNumbers;
    }
    public void setEqualProjectRegisterHouseArea(String equalProjectRegisterHouseArea) {
        this.equalProjectRegisterHouseArea = equalProjectRegisterHouseArea;
    }
    
    public String getEqualProjectRegisterHouseArea() {
        return this.equalProjectRegisterHouseArea;
    }
    
    public void setLikeProjectRegisterHouseArea(String likeProjectRegisterHouseArea) {
        this.likeProjectRegisterHouseArea = likeProjectRegisterHouseArea;
    }
    
    public String getLikeProjectRegisterHouseArea() {
        return this.likeProjectRegisterHouseArea;
    }
    
    public void setInProjectRegisterHouseAreas(String inProjectRegisterHouseAreas) {
        this.inProjectRegisterHouseAreas = inProjectRegisterHouseAreas;
    }
    
    public String getInProjectRegisterHouseAreas() {
        return this.inProjectRegisterHouseAreas;
    }
    public void setEqualProjectCancellationsHouseArea(String equalProjectCancellationsHouseArea) {
        this.equalProjectCancellationsHouseArea = equalProjectCancellationsHouseArea;
    }
    
    public String getEqualProjectCancellationsHouseArea() {
        return this.equalProjectCancellationsHouseArea;
    }
    
    public void setLikeProjectCancellationsHouseArea(String likeProjectCancellationsHouseArea) {
        this.likeProjectCancellationsHouseArea = likeProjectCancellationsHouseArea;
    }
    
    public String getLikeProjectCancellationsHouseArea() {
        return this.likeProjectCancellationsHouseArea;
    }
    
    public void setInProjectCancellationsHouseAreas(String inProjectCancellationsHouseAreas) {
        this.inProjectCancellationsHouseAreas = inProjectCancellationsHouseAreas;
    }
    
    public String getInProjectCancellationsHouseAreas() {
        return this.inProjectCancellationsHouseAreas;
    }
    public void setEqualProjectTotalPriceOverdueNumber(String equalProjectTotalPriceOverdueNumber) {
        this.equalProjectTotalPriceOverdueNumber = equalProjectTotalPriceOverdueNumber;
    }
    
    public String getEqualProjectTotalPriceOverdueNumber() {
        return this.equalProjectTotalPriceOverdueNumber;
    }
    
    public void setLikeProjectTotalPriceOverdueNumber(String likeProjectTotalPriceOverdueNumber) {
        this.likeProjectTotalPriceOverdueNumber = likeProjectTotalPriceOverdueNumber;
    }
    
    public String getLikeProjectTotalPriceOverdueNumber() {
        return this.likeProjectTotalPriceOverdueNumber;
    }
    
    public void setInProjectTotalPriceOverdueNumbers(String inProjectTotalPriceOverdueNumbers) {
        this.inProjectTotalPriceOverdueNumbers = inProjectTotalPriceOverdueNumbers;
    }
    
    public String getInProjectTotalPriceOverdueNumbers() {
        return this.inProjectTotalPriceOverdueNumbers;
    }
    public void setEqualProjectId(String equalProjectId) {
        this.equalProjectId = equalProjectId;
    }
    
    public String getEqualProjectId() {
        return this.equalProjectId;
    }
    
    public void setLikeProjectId(String likeProjectId) {
        this.likeProjectId = likeProjectId;
    }
    
    public String getLikeProjectId() {
        return this.likeProjectId;
    }
    
    public void setInProjectIds(String inProjectIds) {
        this.inProjectIds = inProjectIds;
    }
    
    public String getInProjectIds() {
        return this.inProjectIds;
    }
    public void setEqualInfoUpdateTime(String equalInfoUpdateTime) {
        this.equalInfoUpdateTime = equalInfoUpdateTime;
    }
    
    public String getEqualInfoUpdateTime() {
        return this.equalInfoUpdateTime;
    }
    
    public void setLikeInfoUpdateTime(String likeInfoUpdateTime) {
        this.likeInfoUpdateTime = likeInfoUpdateTime;
    }
    
    public String getLikeInfoUpdateTime() {
        return this.likeInfoUpdateTime;
    }
    
    public void setInInfoUpdateTimes(String inInfoUpdateTimes) {
        this.inInfoUpdateTimes = inInfoUpdateTimes;
    }
    
    public String getInInfoUpdateTimes() {
        return this.inInfoUpdateTimes;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalAreaName)){
			criterias.add(Restrictions.eq("areaName", equalAreaName));
		}
	    if(StringUtils.isNotBlank(likeAreaName)){
			criterias.add(Restrictions.allLike("areaName", likeAreaName));
		}
		 if(StringUtils.isNotBlank(inAreaNames)){
			criterias.add(Restrictions.in("areaName", StringUtils.split(inAreaNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalCityName)){
			criterias.add(Restrictions.eq("cityName", equalCityName));
		}
	    if(StringUtils.isNotBlank(likeCityName)){
			criterias.add(Restrictions.allLike("cityName", likeCityName));
		}
		 if(StringUtils.isNotBlank(inCityNames)){
			criterias.add(Restrictions.in("cityName", StringUtils.split(inCityNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectName)){
			criterias.add(Restrictions.eq("projectName", equalProjectName));
		}
	    if(StringUtils.isNotBlank(likeProjectName)){
			criterias.add(Restrictions.allLike("projectName", likeProjectName));
		}
		 if(StringUtils.isNotBlank(inProjectNames)){
			criterias.add(Restrictions.in("projectName", StringUtils.split(inProjectNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectTotalNumber)){
			criterias.add(Restrictions.eq("projectTotalNumber", equalProjectTotalNumber));
		}
	    if(StringUtils.isNotBlank(likeProjectTotalNumber)){
			criterias.add(Restrictions.allLike("projectTotalNumber", likeProjectTotalNumber));
		}
		 if(StringUtils.isNotBlank(inProjectTotalNumbers)){
			criterias.add(Restrictions.in("projectTotalNumber", StringUtils.split(inProjectTotalNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectTotalArea)){
			criterias.add(Restrictions.eq("projectTotalArea", equalProjectTotalArea));
		}
	    if(StringUtils.isNotBlank(likeProjectTotalArea)){
			criterias.add(Restrictions.allLike("projectTotalArea", likeProjectTotalArea));
		}
		 if(StringUtils.isNotBlank(inProjectTotalAreas)){
			criterias.add(Restrictions.in("projectTotalArea", StringUtils.split(inProjectTotalAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectHouseNumber)){
			criterias.add(Restrictions.eq("projectHouseNumber", equalProjectHouseNumber));
		}
	    if(StringUtils.isNotBlank(likeProjectHouseNumber)){
			criterias.add(Restrictions.allLike("projectHouseNumber", likeProjectHouseNumber));
		}
		 if(StringUtils.isNotBlank(inProjectHouseNumbers)){
			criterias.add(Restrictions.in("projectHouseNumber", StringUtils.split(inProjectHouseNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectHouseArea)){
			criterias.add(Restrictions.eq("projectHouseArea", equalProjectHouseArea));
		}
	    if(StringUtils.isNotBlank(likeProjectHouseArea)){
			criterias.add(Restrictions.allLike("projectHouseArea", likeProjectHouseArea));
		}
		 if(StringUtils.isNotBlank(inProjectHouseAreas)){
			criterias.add(Restrictions.in("projectHouseArea", StringUtils.split(inProjectHouseAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectTotalHouseUnsoldNumber)){
			criterias.add(Restrictions.eq("projectTotalHouseUnsoldNumber", equalProjectTotalHouseUnsoldNumber));
		}
	    if(StringUtils.isNotBlank(likeProjectTotalHouseUnsoldNumber)){
			criterias.add(Restrictions.allLike("projectTotalHouseUnsoldNumber", likeProjectTotalHouseUnsoldNumber));
		}
		 if(StringUtils.isNotBlank(inProjectTotalHouseUnsoldNumbers)){
			criterias.add(Restrictions.in("projectTotalHouseUnsoldNumber", StringUtils.split(inProjectTotalHouseUnsoldNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectTotalHouseUnsoldArea)){
			criterias.add(Restrictions.eq("projectTotalHouseUnsoldArea", equalProjectTotalHouseUnsoldArea));
		}
	    if(StringUtils.isNotBlank(likeProjectTotalHouseUnsoldArea)){
			criterias.add(Restrictions.allLike("projectTotalHouseUnsoldArea", likeProjectTotalHouseUnsoldArea));
		}
		 if(StringUtils.isNotBlank(inProjectTotalHouseUnsoldAreas)){
			criterias.add(Restrictions.in("projectTotalHouseUnsoldArea", StringUtils.split(inProjectTotalHouseUnsoldAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectHouseUnsoldNumber)){
			criterias.add(Restrictions.eq("projectHouseUnsoldNumber", equalProjectHouseUnsoldNumber));
		}
	    if(StringUtils.isNotBlank(likeProjectHouseUnsoldNumber)){
			criterias.add(Restrictions.allLike("projectHouseUnsoldNumber", likeProjectHouseUnsoldNumber));
		}
		 if(StringUtils.isNotBlank(inProjectHouseUnsoldNumbers)){
			criterias.add(Restrictions.in("projectHouseUnsoldNumber", StringUtils.split(inProjectHouseUnsoldNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectHouseUnsoldArea)){
			criterias.add(Restrictions.eq("projectHouseUnsoldArea", equalProjectHouseUnsoldArea));
		}
	    if(StringUtils.isNotBlank(likeProjectHouseUnsoldArea)){
			criterias.add(Restrictions.allLike("projectHouseUnsoldArea", likeProjectHouseUnsoldArea));
		}
		 if(StringUtils.isNotBlank(inProjectHouseUnsoldAreas)){
			criterias.add(Restrictions.in("projectHouseUnsoldArea", StringUtils.split(inProjectHouseUnsoldAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectTotalHouseSoldNumber)){
			criterias.add(Restrictions.eq("projectTotalHouseSoldNumber", equalProjectTotalHouseSoldNumber));
		}
	    if(StringUtils.isNotBlank(likeProjectTotalHouseSoldNumber)){
			criterias.add(Restrictions.allLike("projectTotalHouseSoldNumber", likeProjectTotalHouseSoldNumber));
		}
		 if(StringUtils.isNotBlank(inProjectTotalHouseSoldNumbers)){
			criterias.add(Restrictions.in("projectTotalHouseSoldNumber", StringUtils.split(inProjectTotalHouseSoldNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectTotalHouseSoldArea)){
			criterias.add(Restrictions.eq("projectTotalHouseSoldArea", equalProjectTotalHouseSoldArea));
		}
	    if(StringUtils.isNotBlank(likeProjectTotalHouseSoldArea)){
			criterias.add(Restrictions.allLike("projectTotalHouseSoldArea", likeProjectTotalHouseSoldArea));
		}
		 if(StringUtils.isNotBlank(inProjectTotalHouseSoldAreas)){
			criterias.add(Restrictions.in("projectTotalHouseSoldArea", StringUtils.split(inProjectTotalHouseSoldAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectHouseSoldNumber)){
			criterias.add(Restrictions.eq("projectHouseSoldNumber", equalProjectHouseSoldNumber));
		}
	    if(StringUtils.isNotBlank(likeProjectHouseSoldNumber)){
			criterias.add(Restrictions.allLike("projectHouseSoldNumber", likeProjectHouseSoldNumber));
		}
		 if(StringUtils.isNotBlank(inProjectHouseSoldNumbers)){
			criterias.add(Restrictions.in("projectHouseSoldNumber", StringUtils.split(inProjectHouseSoldNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectHouseSoldArea)){
			criterias.add(Restrictions.eq("projectHouseSoldArea", equalProjectHouseSoldArea));
		}
	    if(StringUtils.isNotBlank(likeProjectHouseSoldArea)){
			criterias.add(Restrictions.allLike("projectHouseSoldArea", likeProjectHouseSoldArea));
		}
		 if(StringUtils.isNotBlank(inProjectHouseSoldAreas)){
			criterias.add(Restrictions.in("projectHouseSoldArea", StringUtils.split(inProjectHouseSoldAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectRegisterTotalHouseNumber)){
			criterias.add(Restrictions.eq("projectRegisterTotalHouseNumber", equalProjectRegisterTotalHouseNumber));
		}
	    if(StringUtils.isNotBlank(likeProjectRegisterTotalHouseNumber)){
			criterias.add(Restrictions.allLike("projectRegisterTotalHouseNumber", likeProjectRegisterTotalHouseNumber));
		}
		 if(StringUtils.isNotBlank(inProjectRegisterTotalHouseNumbers)){
			criterias.add(Restrictions.in("projectRegisterTotalHouseNumber", StringUtils.split(inProjectRegisterTotalHouseNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectRegisterTotalHouseArea)){
			criterias.add(Restrictions.eq("projectRegisterTotalHouseArea", equalProjectRegisterTotalHouseArea));
		}
	    if(StringUtils.isNotBlank(likeProjectRegisterTotalHouseArea)){
			criterias.add(Restrictions.allLike("projectRegisterTotalHouseArea", likeProjectRegisterTotalHouseArea));
		}
		 if(StringUtils.isNotBlank(inProjectRegisterTotalHouseAreas)){
			criterias.add(Restrictions.in("projectRegisterTotalHouseArea", StringUtils.split(inProjectRegisterTotalHouseAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectRegisterHouseNumber)){
			criterias.add(Restrictions.eq("projectRegisterHouseNumber", equalProjectRegisterHouseNumber));
		}
	    if(StringUtils.isNotBlank(likeProjectRegisterHouseNumber)){
			criterias.add(Restrictions.allLike("projectRegisterHouseNumber", likeProjectRegisterHouseNumber));
		}
		 if(StringUtils.isNotBlank(inProjectRegisterHouseNumbers)){
			criterias.add(Restrictions.in("projectRegisterHouseNumber", StringUtils.split(inProjectRegisterHouseNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectRegisterHouseArea)){
			criterias.add(Restrictions.eq("projectRegisterHouseArea", equalProjectRegisterHouseArea));
		}
	    if(StringUtils.isNotBlank(likeProjectRegisterHouseArea)){
			criterias.add(Restrictions.allLike("projectRegisterHouseArea", likeProjectRegisterHouseArea));
		}
		 if(StringUtils.isNotBlank(inProjectRegisterHouseAreas)){
			criterias.add(Restrictions.in("projectRegisterHouseArea", StringUtils.split(inProjectRegisterHouseAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectCancellationsHouseArea)){
			criterias.add(Restrictions.eq("projectCancellationsHouseArea", equalProjectCancellationsHouseArea));
		}
	    if(StringUtils.isNotBlank(likeProjectCancellationsHouseArea)){
			criterias.add(Restrictions.allLike("projectCancellationsHouseArea", likeProjectCancellationsHouseArea));
		}
		 if(StringUtils.isNotBlank(inProjectCancellationsHouseAreas)){
			criterias.add(Restrictions.in("projectCancellationsHouseArea", StringUtils.split(inProjectCancellationsHouseAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectTotalPriceOverdueNumber)){
			criterias.add(Restrictions.eq("projectTotalPriceOverdueNumber", equalProjectTotalPriceOverdueNumber));
		}
	    if(StringUtils.isNotBlank(likeProjectTotalPriceOverdueNumber)){
			criterias.add(Restrictions.allLike("projectTotalPriceOverdueNumber", likeProjectTotalPriceOverdueNumber));
		}
		 if(StringUtils.isNotBlank(inProjectTotalPriceOverdueNumbers)){
			criterias.add(Restrictions.in("projectTotalPriceOverdueNumber", StringUtils.split(inProjectTotalPriceOverdueNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectId)){
			criterias.add(Restrictions.eq("projectId", equalProjectId));
		}
	    if(StringUtils.isNotBlank(likeProjectId)){
			criterias.add(Restrictions.allLike("projectId", likeProjectId));
		}
		 if(StringUtils.isNotBlank(inProjectIds)){
			criterias.add(Restrictions.in("projectId", StringUtils.split(inProjectIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalInfoUpdateTime)){
			criterias.add(Restrictions.eq("infoUpdateTime", equalInfoUpdateTime));
		}
	    if(StringUtils.isNotBlank(likeInfoUpdateTime)){
			criterias.add(Restrictions.allLike("infoUpdateTime", likeInfoUpdateTime));
		}
		 if(StringUtils.isNotBlank(inInfoUpdateTimes)){
			criterias.add(Restrictions.in("infoUpdateTime", StringUtils.split(inInfoUpdateTimes, ",")));
		}
	}
}