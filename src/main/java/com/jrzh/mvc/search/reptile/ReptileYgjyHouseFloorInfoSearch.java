package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileYgjyHouseFloorInfoSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 关联房屋信息id
     */
    private String equalHouseId;
    
    /**
     * Like 关联房屋信息id
     */
    private String likeHouseId;
    
     /**
     * In 关联房屋信息id 英文逗号分隔
     */
    private String inHouseIds;
    
    /**
     * Equal 楼层名称
     */
    private String equalFloorName;
    
    /**
     * Like 楼层名称
     */
    private String likeFloorName;
    
     /**
     * In 楼层名称 英文逗号分隔
     */
    private String inFloorNames;
    
    /**
     * Equal 阳光家缘id（用于查询楼层具体信息）
     */
    private String equalBuildingId;
    
    /**
     * Like 阳光家缘id（用于查询楼层具体信息）
     */
    private String likeBuildingId;
    
     /**
     * In 阳光家缘id（用于查询楼层具体信息） 英文逗号分隔
     */
    private String inBuildingIds;
    
    private String eqStatus;
    
    /**
     * Equal 楼层名称
     */
    private String equalSzStartId;
    
    /**
     * Like 楼层名称
     */
    private String likeSzStartId;
    
     /**
     * In 楼层名称 英文逗号分隔
     */
    private String inSzStartId;
    
    /**
     * Equal 楼层名称
     */
    private String equalShStartId;
    
    /**
     * Like 楼层名称
     */
    private String likeShStartId;
    
     /**
     * In 楼层名称 英文逗号分隔
     */
    private String inShStartId;
    
    /**
     * Equal 楼层名称
     */
    private String equalBjStartId;
    
    /**
     * Like 楼层名称
     */
    private String likeBjStartId;
    
     /**
     * In 楼层名称 英文逗号分隔
     */
    private String inBjStartId;

	public String getEqualSzStartId() {
		return equalSzStartId;
	}

	public void setEqualSzStartId(String equalSzStartId) {
		this.equalSzStartId = equalSzStartId;
	}

	public String getLikeSzStartId() {
		return likeSzStartId;
	}

	public void setLikeSzStartId(String likeSzStartId) {
		this.likeSzStartId = likeSzStartId;
	}

	public String getInSzStartId() {
		return inSzStartId;
	}

	public void setInSzStartId(String inSzStartId) {
		this.inSzStartId = inSzStartId;
	}

	public String getEqualShStartId() {
		return equalShStartId;
	}

	public void setEqualShStartId(String equalShStartId) {
		this.equalShStartId = equalShStartId;
	}

	public String getLikeShStartId() {
		return likeShStartId;
	}

	public void setLikeShStartId(String likeShStartId) {
		this.likeShStartId = likeShStartId;
	}

	public String getInShStartId() {
		return inShStartId;
	}

	public void setInShStartId(String inShStartId) {
		this.inShStartId = inShStartId;
	}

	public String getEqualBjStartId() {
		return equalBjStartId;
	}

	public void setEqualBjStartId(String equalBjStartId) {
		this.equalBjStartId = equalBjStartId;
	}

	public String getLikeBjStartId() {
		return likeBjStartId;
	}

	public void setLikeBjStartId(String likeBjStartId) {
		this.likeBjStartId = likeBjStartId;
	}

	public String getInBjStartId() {
		return inBjStartId;
	}

	public void setInBjStartId(String inBjStartId) {
		this.inBjStartId = inBjStartId;
	}

	public String getEqStatus() {
		return eqStatus;
	}

	public void setEqStatus(String eqStatus) {
		this.eqStatus = eqStatus;
	}

	public void setEqualHouseId(String equalHouseId) {
        this.equalHouseId = equalHouseId;
    }
    
    public String getEqualHouseId() {
        return this.equalHouseId;
    }
    
    public void setLikeHouseId(String likeHouseId) {
        this.likeHouseId = likeHouseId;
    }
    
    public String getLikeHouseId() {
        return this.likeHouseId;
    }
    
    public void setInHouseIds(String inHouseIds) {
        this.inHouseIds = inHouseIds;
    }
    
    public String getInHouseIds() {
        return this.inHouseIds;
    }
    public void setEqualFloorName(String equalFloorName) {
        this.equalFloorName = equalFloorName;
    }
    
    public String getEqualFloorName() {
        return this.equalFloorName;
    }
    
    public void setLikeFloorName(String likeFloorName) {
        this.likeFloorName = likeFloorName;
    }
    
    public String getLikeFloorName() {
        return this.likeFloorName;
    }
    
    public void setInFloorNames(String inFloorNames) {
        this.inFloorNames = inFloorNames;
    }
    
    public String getInFloorNames() {
        return this.inFloorNames;
    }
    public void setEqualBuildingId(String equalBuildingId) {
        this.equalBuildingId = equalBuildingId;
    }
    
    public String getEqualBuildingId() {
        return this.equalBuildingId;
    }
    
    public void setLikeBuildingId(String likeBuildingId) {
        this.likeBuildingId = likeBuildingId;
    }
    
    public String getLikeBuildingId() {
        return this.likeBuildingId;
    }
    
    public void setInBuildingIds(String inBuildingIds) {
        this.inBuildingIds = inBuildingIds;
    }
    
    public String getInBuildingIds() {
        return this.inBuildingIds;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalHouseId)){
			criterias.add(Restrictions.eq("houseId", equalHouseId));
		}
	    if(StringUtils.isNotBlank(likeHouseId)){
			criterias.add(Restrictions.allLike("houseId", likeHouseId));
		}
		 if(StringUtils.isNotBlank(inHouseIds)){
			criterias.add(Restrictions.in("houseId", StringUtils.split(inHouseIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalFloorName)){
			criterias.add(Restrictions.eq("floorName", equalFloorName));
		}
	    if(StringUtils.isNotBlank(likeFloorName)){
			criterias.add(Restrictions.allLike("floorName", likeFloorName));
		}
		 if(StringUtils.isNotBlank(inFloorNames)){
			criterias.add(Restrictions.in("floorName", StringUtils.split(inFloorNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalBuildingId)){
			criterias.add(Restrictions.eq("buildingId", equalBuildingId));
		}
	    if(StringUtils.isNotBlank(likeBuildingId)){
			criterias.add(Restrictions.allLike("buildingId", likeBuildingId));
		}
		if(StringUtils.isNotBlank(inBuildingIds)){
			criterias.add(Restrictions.in("buildingId", StringUtils.split(inBuildingIds, ",")));
		}
		if(StringUtils.isNotBlank(eqStatus)){
			criterias.add(Restrictions.eq("status", eqStatus));
		}
		
		if(StringUtils.isNotBlank(equalSzStartId)){
			criterias.add(Restrictions.eq("szStartId", equalSzStartId));
		}
	    if(StringUtils.isNotBlank(likeSzStartId)){
			criterias.add(Restrictions.allLike("szStartId", likeSzStartId));
		}
		if(StringUtils.isNotBlank(inSzStartId)){
			criterias.add(Restrictions.in("szStartId", StringUtils.split(inSzStartId, ",")));
		}
		
		if(StringUtils.isNotBlank(equalShStartId)){
			criterias.add(Restrictions.eq("shStartId", equalShStartId));
		}
	    if(StringUtils.isNotBlank(likeShStartId)){
			criterias.add(Restrictions.allLike("shStartId", likeShStartId));
		}
		if(StringUtils.isNotBlank(inShStartId)){
			criterias.add(Restrictions.in("shStartId", StringUtils.split(inShStartId, ",")));
		}
		
		if(StringUtils.isNotBlank(equalBjStartId)){
			criterias.add(Restrictions.eq("bjStartId", equalBjStartId));
		}
	    if(StringUtils.isNotBlank(likeBjStartId)){
			criterias.add(Restrictions.allLike("bjStartId", likeBjStartId));
		}
		if(StringUtils.isNotBlank(inBjStartId)){
			criterias.add(Restrictions.in("bjStartId", StringUtils.split(inBjStartId, ",")));
		}
	}
}