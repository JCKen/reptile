package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileCorrectionFactorSearch extends BaseSearch {
	private static final long serialVersionUID = 1L;

	/**
	 * Equal 父ID
	 */
	private String equalPid;

	/**
	 * Like 父ID
	 */
	private String likePid;

	/**
	 * In 父ID 英文逗号分隔
	 */
	private String inPids;

	/**
	 * Equal 修正项
	 */
	private String equalCorrectionName;

	/**
	 * Like 修正项
	 */
	private String likeCorrectionName;

	/**
	 * In 修正项 英文逗号分隔
	 */
	private String inCorrectionNames;

	/**
	 * Equal 修正系数最大值
	 */
	private String equalCorrectionMax;

	/**
	 * Like 修正系数最大值
	 */
	private String likeCorrectionMax;

	/**
	 * In 修正系数最大值 英文逗号分隔
	 */
	private String inCorrectionMaxs;

	/**
	 * Equal 修正系数最小值
	 */
	private String equalCorrectionMin;

	/**
	 * Like 修正系数最小值
	 */
	private String likeCorrectionMin;

	/**
	 * In 修正系数最小值 英文逗号分隔
	 */
	private String inCorrectionMins;

	/**
	 * 为空
	 */
	private String isNull;

	public String getIsNull() {
		return isNull;
	}

	public void setIsNull(String isNull) {
		this.isNull = isNull;
	}

	public void setEqualPid(String equalPid) {
		this.equalPid = equalPid;
	}

	public String getEqualPid() {
		return this.equalPid;
	}

	public void setLikePid(String likePid) {
		this.likePid = likePid;
	}

	public String getLikePid() {
		return this.likePid;
	}

	public void setInPids(String inPids) {
		this.inPids = inPids;
	}

	public String getInPids() {
		return this.inPids;
	}

	public void setEqualCorrectionName(String equalCorrectionName) {
		this.equalCorrectionName = equalCorrectionName;
	}

	public String getEqualCorrectionName() {
		return this.equalCorrectionName;
	}

	public void setLikeCorrectionName(String likeCorrectionName) {
		this.likeCorrectionName = likeCorrectionName;
	}

	public String getLikeCorrectionName() {
		return this.likeCorrectionName;
	}

	public void setInCorrectionNames(String inCorrectionNames) {
		this.inCorrectionNames = inCorrectionNames;
	}

	public String getInCorrectionNames() {
		return this.inCorrectionNames;
	}

	public void setEqualCorrectionMax(String equalCorrectionMax) {
		this.equalCorrectionMax = equalCorrectionMax;
	}

	public String getEqualCorrectionMax() {
		return this.equalCorrectionMax;
	}

	public void setLikeCorrectionMax(String likeCorrectionMax) {
		this.likeCorrectionMax = likeCorrectionMax;
	}

	public String getLikeCorrectionMax() {
		return this.likeCorrectionMax;
	}

	public void setInCorrectionMaxs(String inCorrectionMaxs) {
		this.inCorrectionMaxs = inCorrectionMaxs;
	}

	public String getInCorrectionMaxs() {
		return this.inCorrectionMaxs;
	}

	public void setEqualCorrectionMin(String equalCorrectionMin) {
		this.equalCorrectionMin = equalCorrectionMin;
	}

	public String getEqualCorrectionMin() {
		return this.equalCorrectionMin;
	}

	public void setLikeCorrectionMin(String likeCorrectionMin) {
		this.likeCorrectionMin = likeCorrectionMin;
	}

	public String getLikeCorrectionMin() {
		return this.likeCorrectionMin;
	}

	public void setInCorrectionMins(String inCorrectionMins) {
		this.inCorrectionMins = inCorrectionMins;
	}

	public String getInCorrectionMins() {
		return this.inCorrectionMins;
	}

	@Override
	public void setCriterias(Criteria<?> criterias) {
		if (StringUtils.isNotBlank(equalPid)) {
			criterias.add(Restrictions.eq("pid", equalPid));
		}
		if (StringUtils.isNotBlank(likePid)) {
			criterias.add(Restrictions.allLike("pid", likePid));
		}
		if (StringUtils.isNotBlank(inPids)) {
			criterias.add(Restrictions.in("pid", StringUtils.split(inPids, ",")));
		}
		if (StringUtils.isNotBlank(equalCorrectionName)) {
			criterias.add(Restrictions.eq("correctionName", equalCorrectionName));
		}
		if (StringUtils.isNotBlank(likeCorrectionName)) {
			criterias.add(Restrictions.allLike("correctionName", likeCorrectionName));
		}
		if (StringUtils.isNotBlank(inCorrectionNames)) {
			criterias.add(Restrictions.in("correctionName", StringUtils.split(inCorrectionNames, ",")));
		}
		if (StringUtils.isNotBlank(equalCorrectionMax)) {
			criterias.add(Restrictions.eq("correctionMax", equalCorrectionMax));
		}
		if (StringUtils.isNotBlank(likeCorrectionMax)) {
			criterias.add(Restrictions.allLike("correctionMax", likeCorrectionMax));
		}
		if (StringUtils.isNotBlank(inCorrectionMaxs)) {
			criterias.add(Restrictions.in("correctionMax", StringUtils.split(inCorrectionMaxs, ",")));
		}
		if (StringUtils.isNotBlank(equalCorrectionMin)) {
			criterias.add(Restrictions.eq("correctionMin", equalCorrectionMin));
		}
		if (StringUtils.isNotBlank(likeCorrectionMin)) {
			criterias.add(Restrictions.allLike("correctionMin", likeCorrectionMin));
		}
		if (StringUtils.isNotBlank(inCorrectionMins)) {
			criterias.add(Restrictions.in("correctionMin", StringUtils.split(inCorrectionMins, ",")));
		}
		if(StringUtils.isNotBlank(isNull)){
			criterias.add(Restrictions.isNull(isNull));
		}
		
	}
}