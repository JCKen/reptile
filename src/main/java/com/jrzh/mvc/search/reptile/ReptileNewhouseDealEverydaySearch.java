package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileNewhouseDealEverydaySearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 城市
     */
    private String equalCity;
    
    /**
     * Like 城市
     */
    private String likeCity;
    
     /**
     * In 城市 英文逗号分隔
     */
    private String inCitys;
    
    /**
     * Equal 区域
     */
    private String equalPart;
    
    /**
     * Like 区域
     */
    private String likePart;
    
     /**
     * In 区域 英文逗号分隔
     */
    private String inParts;
    
    /**
     * Equal 成交日期
     */
    private String equalDealDate;
    
    /**
     * Like 成交日期
     */
    private String likeDealDate;
    
     /**
     * In 成交日期 英文逗号分隔
     */
    private String inDealDates;
    
    /**
     * Equal 住宅预售套数
     */
    private String equalHouseCount;
    
    /**
     * Like 住宅预售套数
     */
    private String likeHouseCount;
    
     /**
     * In 住宅预售套数 英文逗号分隔
     */
    private String inHouseCounts;
    
    /**
     * Equal 住宅预售面积（平米）
     */
    private String equalHouseArea;
    
    /**
     * Like 住宅预售面积（平米）
     */
    private String likeHouseArea;
    
     /**
     * In 住宅预售面积（平米） 英文逗号分隔
     */
    private String inHouseAreas;
    
    /**
     * Equal 商业预售套数
     */
    private String equalBusinessCount;
    
    /**
     * Like 商业预售套数
     */
    private String likeBusinessCount;
    
     /**
     * In 商业预售套数 英文逗号分隔
     */
    private String inBusinessCounts;
    
    /**
     * Equal 商业预售面积（平米）
     */
    private String equalBusinessArea;
    
    /**
     * Like 商业预售面积（平米）
     */
    private String likeBusinessArea;
    
     /**
     * In 商业预售面积（平米） 英文逗号分隔
     */
    private String inBusinessAreas;
    
    /**
     * Equal 办公预售套数
     */
    private String equalOfficeCount;
    
    /**
     * Like 办公预售套数
     */
    private String likeOfficeCount;
    
     /**
     * In 办公预售套数 英文逗号分隔
     */
    private String inOfficeCounts;
    
    /**
     * Equal 办公预售面积（平米）
     */
    private String equalOfficeArea;
    
    /**
     * Like 办公预售面积（平米）
     */
    private String likeOfficeArea;
    
     /**
     * In 办公预售面积（平米） 英文逗号分隔
     */
    private String inOfficeAreas;
    
    /**
     * Equal 车位预售套数
     */
    private String equalCarportCount;
    
    /**
     * Like 车位预售套数
     */
    private String likeCarportCount;
    
     /**
     * In 车位预售套数 英文逗号分隔
     */
    private String inCarportCounts;
    
    /**
     * Equal 车位预售面积（平米）
     */
    private String equalCarportArea;
    
    /**
     * Like 车位预售面积（平米）
     */
    private String likeCarportArea;
    
     /**
     * In 车位预售面积（平米） 英文逗号分隔
     */
    private String inCarportAreas;
    
    /**
     * Equal 数据来源地址
     */
    private String equalUrl;
    
    /**
     * Like 数据来源地址
     */
    private String likeUrl;
    
     /**
     * In 数据来源地址 英文逗号分隔
     */
    private String inUrls;
    
    /**
     * Equal 数据来源
     */
    private String equalInformationSources;
    
    /**
     * Like 数据来源
     */
    private String likeInformationSources;
    
     /**
     * In 数据来源 英文逗号分隔
     */
    private String inInformationSourcess;
    

    public void setEqualCity(String equalCity) {
        this.equalCity = equalCity;
    }
    
    public String getEqualCity() {
        return this.equalCity;
    }
    
    public void setLikeCity(String likeCity) {
        this.likeCity = likeCity;
    }
    
    public String getLikeCity() {
        return this.likeCity;
    }
    
    public void setInCitys(String inCitys) {
        this.inCitys = inCitys;
    }
    
    public String getInCitys() {
        return this.inCitys;
    }
    public void setEqualPart(String equalPart) {
        this.equalPart = equalPart;
    }
    
    public String getEqualPart() {
        return this.equalPart;
    }
    
    public void setLikePart(String likePart) {
        this.likePart = likePart;
    }
    
    public String getLikePart() {
        return this.likePart;
    }
    
    public void setInParts(String inParts) {
        this.inParts = inParts;
    }
    
    public String getInParts() {
        return this.inParts;
    }
    public void setEqualDealDate(String equalDealDate) {
        this.equalDealDate = equalDealDate;
    }
    
    public String getEqualDealDate() {
        return this.equalDealDate;
    }
    
    public void setLikeDealDate(String likeDealDate) {
        this.likeDealDate = likeDealDate;
    }
    
    public String getLikeDealDate() {
        return this.likeDealDate;
    }
    
    public void setInDealDates(String inDealDates) {
        this.inDealDates = inDealDates;
    }
    
    public String getInDealDates() {
        return this.inDealDates;
    }
    public void setEqualHouseCount(String equalHouseCount) {
        this.equalHouseCount = equalHouseCount;
    }
    
    public String getEqualHouseCount() {
        return this.equalHouseCount;
    }
    
    public void setLikeHouseCount(String likeHouseCount) {
        this.likeHouseCount = likeHouseCount;
    }
    
    public String getLikeHouseCount() {
        return this.likeHouseCount;
    }
    
    public void setInHouseCounts(String inHouseCounts) {
        this.inHouseCounts = inHouseCounts;
    }
    
    public String getInHouseCounts() {
        return this.inHouseCounts;
    }
    public void setEqualHouseArea(String equalHouseArea) {
        this.equalHouseArea = equalHouseArea;
    }
    
    public String getEqualHouseArea() {
        return this.equalHouseArea;
    }
    
    public void setLikeHouseArea(String likeHouseArea) {
        this.likeHouseArea = likeHouseArea;
    }
    
    public String getLikeHouseArea() {
        return this.likeHouseArea;
    }
    
    public void setInHouseAreas(String inHouseAreas) {
        this.inHouseAreas = inHouseAreas;
    }
    
    public String getInHouseAreas() {
        return this.inHouseAreas;
    }
    public void setEqualBusinessCount(String equalBusinessCount) {
        this.equalBusinessCount = equalBusinessCount;
    }
    
    public String getEqualBusinessCount() {
        return this.equalBusinessCount;
    }
    
    public void setLikeBusinessCount(String likeBusinessCount) {
        this.likeBusinessCount = likeBusinessCount;
    }
    
    public String getLikeBusinessCount() {
        return this.likeBusinessCount;
    }
    
    public void setInBusinessCounts(String inBusinessCounts) {
        this.inBusinessCounts = inBusinessCounts;
    }
    
    public String getInBusinessCounts() {
        return this.inBusinessCounts;
    }
    public void setEqualBusinessArea(String equalBusinessArea) {
        this.equalBusinessArea = equalBusinessArea;
    }
    
    public String getEqualBusinessArea() {
        return this.equalBusinessArea;
    }
    
    public void setLikeBusinessArea(String likeBusinessArea) {
        this.likeBusinessArea = likeBusinessArea;
    }
    
    public String getLikeBusinessArea() {
        return this.likeBusinessArea;
    }
    
    public void setInBusinessAreas(String inBusinessAreas) {
        this.inBusinessAreas = inBusinessAreas;
    }
    
    public String getInBusinessAreas() {
        return this.inBusinessAreas;
    }
    public void setEqualOfficeCount(String equalOfficeCount) {
        this.equalOfficeCount = equalOfficeCount;
    }
    
    public String getEqualOfficeCount() {
        return this.equalOfficeCount;
    }
    
    public void setLikeOfficeCount(String likeOfficeCount) {
        this.likeOfficeCount = likeOfficeCount;
    }
    
    public String getLikeOfficeCount() {
        return this.likeOfficeCount;
    }
    
    public void setInOfficeCounts(String inOfficeCounts) {
        this.inOfficeCounts = inOfficeCounts;
    }
    
    public String getInOfficeCounts() {
        return this.inOfficeCounts;
    }
    public void setEqualOfficeArea(String equalOfficeArea) {
        this.equalOfficeArea = equalOfficeArea;
    }
    
    public String getEqualOfficeArea() {
        return this.equalOfficeArea;
    }
    
    public void setLikeOfficeArea(String likeOfficeArea) {
        this.likeOfficeArea = likeOfficeArea;
    }
    
    public String getLikeOfficeArea() {
        return this.likeOfficeArea;
    }
    
    public void setInOfficeAreas(String inOfficeAreas) {
        this.inOfficeAreas = inOfficeAreas;
    }
    
    public String getInOfficeAreas() {
        return this.inOfficeAreas;
    }
    public void setEqualCarportCount(String equalCarportCount) {
        this.equalCarportCount = equalCarportCount;
    }
    
    public String getEqualCarportCount() {
        return this.equalCarportCount;
    }
    
    public void setLikeCarportCount(String likeCarportCount) {
        this.likeCarportCount = likeCarportCount;
    }
    
    public String getLikeCarportCount() {
        return this.likeCarportCount;
    }
    
    public void setInCarportCounts(String inCarportCounts) {
        this.inCarportCounts = inCarportCounts;
    }
    
    public String getInCarportCounts() {
        return this.inCarportCounts;
    }
    public void setEqualCarportArea(String equalCarportArea) {
        this.equalCarportArea = equalCarportArea;
    }
    
    public String getEqualCarportArea() {
        return this.equalCarportArea;
    }
    
    public void setLikeCarportArea(String likeCarportArea) {
        this.likeCarportArea = likeCarportArea;
    }
    
    public String getLikeCarportArea() {
        return this.likeCarportArea;
    }
    
    public void setInCarportAreas(String inCarportAreas) {
        this.inCarportAreas = inCarportAreas;
    }
    
    public String getInCarportAreas() {
        return this.inCarportAreas;
    }
    public void setEqualUrl(String equalUrl) {
        this.equalUrl = equalUrl;
    }
    
    public String getEqualUrl() {
        return this.equalUrl;
    }
    
    public void setLikeUrl(String likeUrl) {
        this.likeUrl = likeUrl;
    }
    
    public String getLikeUrl() {
        return this.likeUrl;
    }
    
    public void setInUrls(String inUrls) {
        this.inUrls = inUrls;
    }
    
    public String getInUrls() {
        return this.inUrls;
    }
    public void setEqualInformationSources(String equalInformationSources) {
        this.equalInformationSources = equalInformationSources;
    }
    
    public String getEqualInformationSources() {
        return this.equalInformationSources;
    }
    
    public void setLikeInformationSources(String likeInformationSources) {
        this.likeInformationSources = likeInformationSources;
    }
    
    public String getLikeInformationSources() {
        return this.likeInformationSources;
    }
    
    public void setInInformationSourcess(String inInformationSourcess) {
        this.inInformationSourcess = inInformationSourcess;
    }
    
    public String getInInformationSourcess() {
        return this.inInformationSourcess;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalCity)){
			criterias.add(Restrictions.eq("city", equalCity));
		}
	    if(StringUtils.isNotBlank(likeCity)){
			criterias.add(Restrictions.allLike("city", likeCity));
		}
		 if(StringUtils.isNotBlank(inCitys)){
			criterias.add(Restrictions.in("city", StringUtils.split(inCitys, ",")));
		}
	    if(StringUtils.isNotBlank(equalPart)){
			criterias.add(Restrictions.eq("part", equalPart));
		}
	    if(StringUtils.isNotBlank(likePart)){
			criterias.add(Restrictions.allLike("part", likePart));
		}
		 if(StringUtils.isNotBlank(inParts)){
			criterias.add(Restrictions.in("part", StringUtils.split(inParts, ",")));
		}
	    if(StringUtils.isNotBlank(equalDealDate)){
			criterias.add(Restrictions.eq("dealDate", equalDealDate));
		}
	    if(StringUtils.isNotBlank(likeDealDate)){
			criterias.add(Restrictions.allLike("dealDate", likeDealDate));
		}
		 if(StringUtils.isNotBlank(inDealDates)){
			criterias.add(Restrictions.in("dealDate", StringUtils.split(inDealDates, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseCount)){
			criterias.add(Restrictions.eq("houseCount", equalHouseCount));
		}
	    if(StringUtils.isNotBlank(likeHouseCount)){
			criterias.add(Restrictions.allLike("houseCount", likeHouseCount));
		}
		 if(StringUtils.isNotBlank(inHouseCounts)){
			criterias.add(Restrictions.in("houseCount", StringUtils.split(inHouseCounts, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseArea)){
			criterias.add(Restrictions.eq("houseArea", equalHouseArea));
		}
	    if(StringUtils.isNotBlank(likeHouseArea)){
			criterias.add(Restrictions.allLike("houseArea", likeHouseArea));
		}
		 if(StringUtils.isNotBlank(inHouseAreas)){
			criterias.add(Restrictions.in("houseArea", StringUtils.split(inHouseAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalBusinessCount)){
			criterias.add(Restrictions.eq("businessCount", equalBusinessCount));
		}
	    if(StringUtils.isNotBlank(likeBusinessCount)){
			criterias.add(Restrictions.allLike("businessCount", likeBusinessCount));
		}
		 if(StringUtils.isNotBlank(inBusinessCounts)){
			criterias.add(Restrictions.in("businessCount", StringUtils.split(inBusinessCounts, ",")));
		}
	    if(StringUtils.isNotBlank(equalBusinessArea)){
			criterias.add(Restrictions.eq("businessArea", equalBusinessArea));
		}
	    if(StringUtils.isNotBlank(likeBusinessArea)){
			criterias.add(Restrictions.allLike("businessArea", likeBusinessArea));
		}
		 if(StringUtils.isNotBlank(inBusinessAreas)){
			criterias.add(Restrictions.in("businessArea", StringUtils.split(inBusinessAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalOfficeCount)){
			criterias.add(Restrictions.eq("officeCount", equalOfficeCount));
		}
	    if(StringUtils.isNotBlank(likeOfficeCount)){
			criterias.add(Restrictions.allLike("officeCount", likeOfficeCount));
		}
		 if(StringUtils.isNotBlank(inOfficeCounts)){
			criterias.add(Restrictions.in("officeCount", StringUtils.split(inOfficeCounts, ",")));
		}
	    if(StringUtils.isNotBlank(equalOfficeArea)){
			criterias.add(Restrictions.eq("officeArea", equalOfficeArea));
		}
	    if(StringUtils.isNotBlank(likeOfficeArea)){
			criterias.add(Restrictions.allLike("officeArea", likeOfficeArea));
		}
		 if(StringUtils.isNotBlank(inOfficeAreas)){
			criterias.add(Restrictions.in("officeArea", StringUtils.split(inOfficeAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalCarportCount)){
			criterias.add(Restrictions.eq("carportCount", equalCarportCount));
		}
	    if(StringUtils.isNotBlank(likeCarportCount)){
			criterias.add(Restrictions.allLike("carportCount", likeCarportCount));
		}
		 if(StringUtils.isNotBlank(inCarportCounts)){
			criterias.add(Restrictions.in("carportCount", StringUtils.split(inCarportCounts, ",")));
		}
	    if(StringUtils.isNotBlank(equalCarportArea)){
			criterias.add(Restrictions.eq("carportArea", equalCarportArea));
		}
	    if(StringUtils.isNotBlank(likeCarportArea)){
			criterias.add(Restrictions.allLike("carportArea", likeCarportArea));
		}
		 if(StringUtils.isNotBlank(inCarportAreas)){
			criterias.add(Restrictions.in("carportArea", StringUtils.split(inCarportAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalUrl)){
			criterias.add(Restrictions.eq("url", equalUrl));
		}
	    if(StringUtils.isNotBlank(likeUrl)){
			criterias.add(Restrictions.allLike("url", likeUrl));
		}
		 if(StringUtils.isNotBlank(inUrls)){
			criterias.add(Restrictions.in("url", StringUtils.split(inUrls, ",")));
		}
	    if(StringUtils.isNotBlank(equalInformationSources)){
			criterias.add(Restrictions.eq("informationSources", equalInformationSources));
		}
	    if(StringUtils.isNotBlank(likeInformationSources)){
			criterias.add(Restrictions.allLike("informationSources", likeInformationSources));
		}
		 if(StringUtils.isNotBlank(inInformationSourcess)){
			criterias.add(Restrictions.in("informationSources", StringUtils.split(inInformationSourcess, ",")));
		}
	}
}