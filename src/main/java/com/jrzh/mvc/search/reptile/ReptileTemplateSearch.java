package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;

import com.jrzh.framework.base.search.BaseSearch;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;

public class ReptileTemplateSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 类型（市调或者模板，生成pdf模板）
     */
    private String equalType;
    
    /**
     * Like 类型（市调或者模板，生成pdf模板）
     */
    private String likeType;
    
     /**
     * In 类型（市调或者模板，生成pdf模板） 英文逗号分隔
     */
    private String inTypes;
    
    /**
     * Equal 模板名称
     */
    private String equalName;
    
    /**
     * Like 模板名称
     */
    private String likeName;
    
     /**
     * In 模板名称 英文逗号分隔
     */
    private String inNames;
    
    /**
     * Equal 用户ID
     */
    private String equalUserId;
    
    /**
     * Like 用户ID
     */
    private String likeUserId;
    
     /**
     * In 用户ID
     */
    private String inUserIds;
    
    /**
     * Equal 房屋类型
     */
    private String equalHouseType;
    
    /**
     * Like 房屋类型
     */
    private String likeHouseType;
    
     /**
     * In 房屋类型
     */
    private String inHouseTypes;
    
    public String getEqualUserId() {
		return equalUserId;
	}

	public void setEqualUserId(String equalUserId) {
		this.equalUserId = equalUserId;
	}

	public String getLikeUserId() {
		return likeUserId;
	}

	public void setLikeUserId(String likeUserId) {
		this.likeUserId = likeUserId;
	}

	public String getInUserIds() {
		return inUserIds;
	}

	public void setInUserIds(String inUserIds) {
		this.inUserIds = inUserIds;
	}

	public void setEqualType(String equalType) {
        this.equalType = equalType;
    }
    
    public String getEqualType() {
        return this.equalType;
    }
    
    public void setLikeType(String likeType) {
        this.likeType = likeType;
    }
    
    public String getLikeType() {
        return this.likeType;
    }
    
    public void setInTypes(String inTypes) {
        this.inTypes = inTypes;
    }
    
    public String getInTypes() {
        return this.inTypes;
    }
    public void setEqualName(String equalName) {
        this.equalName = equalName;
    }
    
    public String getEqualName() {
        return this.equalName;
    }
    
    public void setLikeName(String likeName) {
        this.likeName = likeName;
    }
    
    public String getLikeName() {
        return this.likeName;
    }
    
    public void setInNames(String inNames) {
        this.inNames = inNames;
    }
    
    public String getInNames() {
        return this.inNames;
    }

	public String getEqualHouseType() {
		return equalHouseType;
	}

	public void setEqualHouseType(String equalHouseType) {
		this.equalHouseType = equalHouseType;
	}

	public String getLikeHouseType() {
		return likeHouseType;
	}

	public void setLikeHouseType(String likeHouseType) {
		this.likeHouseType = likeHouseType;
	}

	public String getInHouseTypes() {
		return inHouseTypes;
	}

	public void setInHouseTypes(String inHouseTypes) {
		this.inHouseTypes = inHouseTypes;
	}

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalType)){
			criterias.add(Restrictions.eq("type", equalType));
		}
	    if(StringUtils.isNotBlank(likeType)){
			criterias.add(Restrictions.allLike("type", likeType));
		}
		 if(StringUtils.isNotBlank(inTypes)){
			criterias.add(Restrictions.in("type", StringUtils.split(inTypes, ",")));
		}
	    if(StringUtils.isNotBlank(equalName)){
			criterias.add(Restrictions.eq("name", equalName));
		}
	    if(StringUtils.isNotBlank(likeName)){
			criterias.add(Restrictions.allLike("name", likeName));
		}
		 if(StringUtils.isNotBlank(inNames)){
			criterias.add(Restrictions.in("name", StringUtils.split(inNames, ",")));
		}
		 
		if(StringUtils.isNotBlank(equalUserId)){
			criterias.add(Restrictions.eq("userId", equalUserId));
		}
	    if(StringUtils.isNotBlank(likeUserId)){
			criterias.add(Restrictions.allLike("userId", likeUserId));
		}
		 if(StringUtils.isNotBlank(inUserIds)){
			criterias.add(Restrictions.in("userId", StringUtils.split(inUserIds, ",")));
		}
		 
		if(StringUtils.isNotBlank(equalHouseType)){
			criterias.add(Restrictions.eq("houseType", equalHouseType));
		}
	    if(StringUtils.isNotBlank(likeHouseType)){
			criterias.add(Restrictions.allLike("houseType", likeHouseType));
		}
		 if(StringUtils.isNotBlank(inHouseTypes)){
			criterias.add(Restrictions.in("houseType", StringUtils.split(inHouseTypes, ",")));
		}
	}
}