package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileUserSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 员工ID
     */
    private String equalUserId;
    
    /**
     * Like 员工ID
     */
    private String likeUserId;
    
     /**
     * In 员工ID 英文逗号分隔
     */
    private String inUserIds;
    
    /**
     * Equal 是否停用（0：停用；1：启用）
     */
    private String equalIsStop;
    
    /**
     * Like 是否停用（0：停用；1：启用）
     */
    private String likeIsStop;
    
     /**
     * In 是否停用（0：停用；1：启用） 英文逗号分隔
     */
    private String inIsStops;
    
    /**
     * Equal 员工手机
     */
    private String equalMobile;
    
    /**
     * Like 员工手机
     */
    private String likeMobile;
    
     /**
     * In 员工手机 英文逗号分隔
     */
    private String inMobiles;
    
    /**
     * Equal 员工姓名
     */
    private String equalUserName;
    
    /**
     * Like 员工姓名
     */
    private String likeUserName;
    
     /**
     * In 员工姓名 英文逗号分隔
     */
    private String inUserNames;
    
    /**
     * Equal 部门名称
     */
    private String equalDeptName;
    
    /**
     * Like 部门名称
     */
    private String likeDeptName;
    
     /**
     * In 部门名称 英文逗号分隔
     */
    private String inDeptNames;
    
    /**
     * Equal 密码
     */
    private String equalPwd;
    
    /**
     * Like 密码
     */
    private String likePwd;
    
     /**
     * In 密码 英文逗号分隔
     */
    private String inPwds;
    

    public void setEqualUserId(String equalUserId) {
        this.equalUserId = equalUserId;
    }
    
    public String getEqualUserId() {
        return this.equalUserId;
    }
    
    public void setLikeUserId(String likeUserId) {
        this.likeUserId = likeUserId;
    }
    
    public String getLikeUserId() {
        return this.likeUserId;
    }
    
    public void setInUserIds(String inUserIds) {
        this.inUserIds = inUserIds;
    }
    
    public String getInUserIds() {
        return this.inUserIds;
    }
    public void setEqualIsStop(String equalIsStop) {
        this.equalIsStop = equalIsStop;
    }
    
    public String getEqualIsStop() {
        return this.equalIsStop;
    }
    
    public void setLikeIsStop(String likeIsStop) {
        this.likeIsStop = likeIsStop;
    }
    
    public String getLikeIsStop() {
        return this.likeIsStop;
    }
    
    public void setInIsStops(String inIsStops) {
        this.inIsStops = inIsStops;
    }
    
    public String getInIsStops() {
        return this.inIsStops;
    }
    public void setEqualMobile(String equalMobile) {
        this.equalMobile = equalMobile;
    }
    
    public String getEqualMobile() {
        return this.equalMobile;
    }
    
    public void setLikeMobile(String likeMobile) {
        this.likeMobile = likeMobile;
    }
    
    public String getLikeMobile() {
        return this.likeMobile;
    }
    
    public void setInMobiles(String inMobiles) {
        this.inMobiles = inMobiles;
    }
    
    public String getInMobiles() {
        return this.inMobiles;
    }
    public void setEqualUserName(String equalUserName) {
        this.equalUserName = equalUserName;
    }
    
    public String getEqualUserName() {
        return this.equalUserName;
    }
    
    public void setLikeUserName(String likeUserName) {
        this.likeUserName = likeUserName;
    }
    
    public String getLikeUserName() {
        return this.likeUserName;
    }
    
    public void setInUserNames(String inUserNames) {
        this.inUserNames = inUserNames;
    }
    
    public String getInUserNames() {
        return this.inUserNames;
    }
    public void setEqualDeptName(String equalDeptName) {
        this.equalDeptName = equalDeptName;
    }
    
    public String getEqualDeptName() {
        return this.equalDeptName;
    }
    
    public void setLikeDeptName(String likeDeptName) {
        this.likeDeptName = likeDeptName;
    }
    
    public String getLikeDeptName() {
        return this.likeDeptName;
    }
    
    public void setInDeptNames(String inDeptNames) {
        this.inDeptNames = inDeptNames;
    }
    
    public String getInDeptNames() {
        return this.inDeptNames;
    }
    public void setEqualPwd(String equalPwd) {
        this.equalPwd = equalPwd;
    }
    
    public String getEqualPwd() {
        return this.equalPwd;
    }
    
    public void setLikePwd(String likePwd) {
        this.likePwd = likePwd;
    }
    
    public String getLikePwd() {
        return this.likePwd;
    }
    
    public void setInPwds(String inPwds) {
        this.inPwds = inPwds;
    }
    
    public String getInPwds() {
        return this.inPwds;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalUserId)){
			criterias.add(Restrictions.eq("userId", equalUserId));
		}
	    if(StringUtils.isNotBlank(likeUserId)){
			criterias.add(Restrictions.allLike("userId", likeUserId));
		}
		 if(StringUtils.isNotBlank(inUserIds)){
			criterias.add(Restrictions.in("userId", StringUtils.split(inUserIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalIsStop)){
			criterias.add(Restrictions.eq("isStop", equalIsStop));
		}
	    if(StringUtils.isNotBlank(likeIsStop)){
			criterias.add(Restrictions.allLike("isStop", likeIsStop));
		}
		 if(StringUtils.isNotBlank(inIsStops)){
			criterias.add(Restrictions.in("isStop", StringUtils.split(inIsStops, ",")));
		}
	    if(StringUtils.isNotBlank(equalMobile)){
			criterias.add(Restrictions.eq("mobile", equalMobile));
		}
	    if(StringUtils.isNotBlank(likeMobile)){
			criterias.add(Restrictions.allLike("mobile", likeMobile));
		}
		 if(StringUtils.isNotBlank(inMobiles)){
			criterias.add(Restrictions.in("mobile", StringUtils.split(inMobiles, ",")));
		}
	    if(StringUtils.isNotBlank(equalUserName)){
			criterias.add(Restrictions.eq("userName", equalUserName));
		}
	    if(StringUtils.isNotBlank(likeUserName)){
			criterias.add(Restrictions.allLike("userName", likeUserName));
		}
		 if(StringUtils.isNotBlank(inUserNames)){
			criterias.add(Restrictions.in("userName", StringUtils.split(inUserNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalDeptName)){
			criterias.add(Restrictions.eq("deptName", equalDeptName));
		}
	    if(StringUtils.isNotBlank(likeDeptName)){
			criterias.add(Restrictions.allLike("deptName", likeDeptName));
		}
		 if(StringUtils.isNotBlank(inDeptNames)){
			criterias.add(Restrictions.in("deptName", StringUtils.split(inDeptNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalPwd)){
			criterias.add(Restrictions.eq("pwd", equalPwd));
		}
	    if(StringUtils.isNotBlank(likePwd)){
			criterias.add(Restrictions.allLike("pwd", likePwd));
		}
		 if(StringUtils.isNotBlank(inPwds)){
			criterias.add(Restrictions.in("pwd", StringUtils.split(inPwds, ",")));
		}
	}
}