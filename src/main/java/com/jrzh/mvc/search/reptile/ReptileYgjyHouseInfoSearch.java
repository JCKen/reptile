package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileYgjyHouseInfoSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 项目名称
     */
    private String equalProductName;
    
    /**
     * Like 项目名称
     */
    private String likeProductName;
    
     /**
     * In 项目名称 英文逗号分隔
     */
    private String inProductNames;
    
    /**
     * Equal 开发商
     */
    private String equalDeveloper;
    
    /**
     * Like 开发商
     */
    private String likeDeveloper;
    
     /**
     * In 开发商 英文逗号分隔
     */
    private String inDevelopers;
    
    /**
     * Equal 预售证
     */
    private String equalPreSalePermit;
    
    /**
     * Like 预售证
     */
    private String likePreSalePermit;
    
     /**
     * In 预售证 英文逗号分隔
     */
    private String inPreSalePermits;
    
    /**
     * Equal 项目地址
     */
    private String equalProjectAddress;
    
    /**
     * Like 项目地址
     */
    private String likeProjectAddress;
    
     /**
     * In 项目地址 英文逗号分隔
     */
    private String inProjectAddresss;
    
    /**
     * Equal 行政区
     */
    private String equalAdministrativeArea;
    
    /**
     * Like 行政区
     */
    private String likeAdministrativeArea;
    
     /**
     * In 行政区 英文逗号分隔
     */
    private String inAdministrativeAreas;
    
    /**
     * Equal 占地面积
     */
    private String equalLandArea;
    
    /**
     * Like 占地面积
     */
    private String likeLandArea;
    
     /**
     * In 占地面积 英文逗号分隔
     */
    private String inLandAreas;
    
    /**
     * Equal 建筑面积
     */
    private String equalConstructionArea;
    
    /**
     * Like 建筑面积
     */
    private String likeConstructionArea;
    
     /**
     * In 建筑面积 英文逗号分隔
     */
    private String inConstructionAreas;
    
    /**
     * Equal 已售面积
     */
    private String equalSoldArea;
    
    /**
     * Like 已售面积
     */
    private String likeSoldArea;
    
     /**
     * In 已售面积 英文逗号分隔
     */
    private String inSoldAreas;
    
    /**
     * Equal 未售面积
     */
    private String equalUnsoldArea;
    
    /**
     * Like 未售面积
     */
    private String likeUnsoldArea;
    
     /**
     * In 未售面积 英文逗号分隔
     */
    private String inUnsoldAreas;
    
    /**
     * Equal 住宅已售
     */
    private String equalHouseSold;
    
    /**
     * Like 住宅已售
     */
    private String likeHouseSold;
    
     /**
     * In 住宅已售 英文逗号分隔
     */
    private String inHouseSolds;
    
    /**
     * Equal 住宅未售
     */
    private String equalHouseUnsold;
    
    /**
     * Like 住宅未售
     */
    private String likeHouseUnsold;
    
     /**
     * In 住宅未售 英文逗号分隔
     */
    private String inHouseUnsolds;
    
    /**
     * Equal 已售总数
     */
    private String equalSumSold;
    
    /**
     * Like 已售总数
     */
    private String likeSumSold;
    
     /**
     * In 已售总数 英文逗号分隔
     */
    private String inSumSolds;
    
    /**
     * Equal 未售总数
     */
    private String equalSumUnsold;
    
    /**
     * Like 未售总数
     */
    private String likeSumUnsold;
    
     /**
     * In 未售总数 英文逗号分隔
     */
    private String inSumUnsolds;
    
    /**
     * Equal 数据更新时间
     */
    private String equalDataUpdateTime;
    
    /**
     * Like 数据更新时间
     */
    private String likeDataUpdateTime;
    
     /**
     * In 数据更新时间 英文逗号分隔
     */
    private String inDataUpdateTimes;
    
    /**
     * Equal 数据源
     */
    private String equalSource;
    
    /**
     * Like 数据源
     */
    private String likeSource;
    
     /**
     * In 数据源 英文逗号分隔
     */
    private String inSources;
    
    
    /**
     * Equal 数据源
     */
    private String equalProjectId;
    
    /**
     * Like 数据源
     */
    private String likeProjectId;
    
     /**
     * In 数据源 英文逗号分隔
     */
    private String inProjectId;
    
    /**
     * Equal 数据源
     */
    private String equalCity;
    
    /**
     * Like 数据源
     */
    private String likeCity;
    
     /**
     * In 数据源 英文逗号分隔
     */
    private String inCity;
    
    
    public String getEqualCity() {
		return equalCity;
	}

	public void setEqualCity(String equalCity) {
		this.equalCity = equalCity;
	}

	public String getLikeCity() {
		return likeCity;
	}

	public void setLikeCity(String likeCity) {
		this.likeCity = likeCity;
	}

	public String getInCity() {
		return inCity;
	}

	public void setInCity(String inCity) {
		this.inCity = inCity;
	}

	public String getEqualProjectId() {
		return equalProjectId;
	}

	public void setEqualProjectId(String equalProjectId) {
		this.equalProjectId = equalProjectId;
	}

	public String getLikeProjectId() {
		return likeProjectId;
	}

	public void setLikeProjectId(String likeProjectId) {
		this.likeProjectId = likeProjectId;
	}

	public String getInProjectId() {
		return inProjectId;
	}

	public void setInProjectId(String inProjectId) {
		this.inProjectId = inProjectId;
	}

	private String isNull;
    
    public String getIsNull() {
		return isNull;
	}

	public void setIsNull(String isNull) {
		this.isNull = isNull;
	}

	public void setEqualProductName(String equalProductName) {
        this.equalProductName = equalProductName;
    }
    
    public String getEqualProductName() {
        return this.equalProductName;
    }
    
    public void setLikeProductName(String likeProductName) {
        this.likeProductName = likeProductName;
    }
    
    public String getLikeProductName() {
        return this.likeProductName;
    }
    
    public void setInProductNames(String inProductNames) {
        this.inProductNames = inProductNames;
    }
    
    public String getInProductNames() {
        return this.inProductNames;
    }
    public void setEqualDeveloper(String equalDeveloper) {
        this.equalDeveloper = equalDeveloper;
    }
    
    public String getEqualDeveloper() {
        return this.equalDeveloper;
    }
    
    public void setLikeDeveloper(String likeDeveloper) {
        this.likeDeveloper = likeDeveloper;
    }
    
    public String getLikeDeveloper() {
        return this.likeDeveloper;
    }
    
    public void setInDevelopers(String inDevelopers) {
        this.inDevelopers = inDevelopers;
    }
    
    public String getInDevelopers() {
        return this.inDevelopers;
    }
    public void setEqualPreSalePermit(String equalPreSalePermit) {
        this.equalPreSalePermit = equalPreSalePermit;
    }
    
    public String getEqualPreSalePermit() {
        return this.equalPreSalePermit;
    }
    
    public void setLikePreSalePermit(String likePreSalePermit) {
        this.likePreSalePermit = likePreSalePermit;
    }
    
    public String getLikePreSalePermit() {
        return this.likePreSalePermit;
    }
    
    public void setInPreSalePermits(String inPreSalePermits) {
        this.inPreSalePermits = inPreSalePermits;
    }
    
    public String getInPreSalePermits() {
        return this.inPreSalePermits;
    }
    public void setEqualProjectAddress(String equalProjectAddress) {
        this.equalProjectAddress = equalProjectAddress;
    }
    
    public String getEqualProjectAddress() {
        return this.equalProjectAddress;
    }
    
    public void setLikeProjectAddress(String likeProjectAddress) {
        this.likeProjectAddress = likeProjectAddress;
    }
    
    public String getLikeProjectAddress() {
        return this.likeProjectAddress;
    }
    
    public void setInProjectAddresss(String inProjectAddresss) {
        this.inProjectAddresss = inProjectAddresss;
    }
    
    public String getInProjectAddresss() {
        return this.inProjectAddresss;
    }
    public void setEqualAdministrativeArea(String equalAdministrativeArea) {
        this.equalAdministrativeArea = equalAdministrativeArea;
    }
    
    public String getEqualAdministrativeArea() {
        return this.equalAdministrativeArea;
    }
    
    public void setLikeAdministrativeArea(String likeAdministrativeArea) {
        this.likeAdministrativeArea = likeAdministrativeArea;
    }
    
    public String getLikeAdministrativeArea() {
        return this.likeAdministrativeArea;
    }
    
    public void setInAdministrativeAreas(String inAdministrativeAreas) {
        this.inAdministrativeAreas = inAdministrativeAreas;
    }
    
    public String getInAdministrativeAreas() {
        return this.inAdministrativeAreas;
    }
    public void setEqualLandArea(String equalLandArea) {
        this.equalLandArea = equalLandArea;
    }
    
    public String getEqualLandArea() {
        return this.equalLandArea;
    }
    
    public void setLikeLandArea(String likeLandArea) {
        this.likeLandArea = likeLandArea;
    }
    
    public String getLikeLandArea() {
        return this.likeLandArea;
    }
    
    public void setInLandAreas(String inLandAreas) {
        this.inLandAreas = inLandAreas;
    }
    
    public String getInLandAreas() {
        return this.inLandAreas;
    }
    public void setEqualConstructionArea(String equalConstructionArea) {
        this.equalConstructionArea = equalConstructionArea;
    }
    
    public String getEqualConstructionArea() {
        return this.equalConstructionArea;
    }
    
    public void setLikeConstructionArea(String likeConstructionArea) {
        this.likeConstructionArea = likeConstructionArea;
    }
    
    public String getLikeConstructionArea() {
        return this.likeConstructionArea;
    }
    
    public void setInConstructionAreas(String inConstructionAreas) {
        this.inConstructionAreas = inConstructionAreas;
    }
    
    public String getInConstructionAreas() {
        return this.inConstructionAreas;
    }
    public void setEqualSoldArea(String equalSoldArea) {
        this.equalSoldArea = equalSoldArea;
    }
    
    public String getEqualSoldArea() {
        return this.equalSoldArea;
    }
    
    public void setLikeSoldArea(String likeSoldArea) {
        this.likeSoldArea = likeSoldArea;
    }
    
    public String getLikeSoldArea() {
        return this.likeSoldArea;
    }
    
    public void setInSoldAreas(String inSoldAreas) {
        this.inSoldAreas = inSoldAreas;
    }
    
    public String getInSoldAreas() {
        return this.inSoldAreas;
    }
    public void setEqualUnsoldArea(String equalUnsoldArea) {
        this.equalUnsoldArea = equalUnsoldArea;
    }
    
    public String getEqualUnsoldArea() {
        return this.equalUnsoldArea;
    }
    
    public void setLikeUnsoldArea(String likeUnsoldArea) {
        this.likeUnsoldArea = likeUnsoldArea;
    }
    
    public String getLikeUnsoldArea() {
        return this.likeUnsoldArea;
    }
    
    public void setInUnsoldAreas(String inUnsoldAreas) {
        this.inUnsoldAreas = inUnsoldAreas;
    }
    
    public String getInUnsoldAreas() {
        return this.inUnsoldAreas;
    }
    public void setEqualHouseSold(String equalHouseSold) {
        this.equalHouseSold = equalHouseSold;
    }
    
    public String getEqualHouseSold() {
        return this.equalHouseSold;
    }
    
    public void setLikeHouseSold(String likeHouseSold) {
        this.likeHouseSold = likeHouseSold;
    }
    
    public String getLikeHouseSold() {
        return this.likeHouseSold;
    }
    
    public void setInHouseSolds(String inHouseSolds) {
        this.inHouseSolds = inHouseSolds;
    }
    
    public String getInHouseSolds() {
        return this.inHouseSolds;
    }
    public void setEqualHouseUnsold(String equalHouseUnsold) {
        this.equalHouseUnsold = equalHouseUnsold;
    }
    
    public String getEqualHouseUnsold() {
        return this.equalHouseUnsold;
    }
    
    public void setLikeHouseUnsold(String likeHouseUnsold) {
        this.likeHouseUnsold = likeHouseUnsold;
    }
    
    public String getLikeHouseUnsold() {
        return this.likeHouseUnsold;
    }
    
    public void setInHouseUnsolds(String inHouseUnsolds) {
        this.inHouseUnsolds = inHouseUnsolds;
    }
    
    public String getInHouseUnsolds() {
        return this.inHouseUnsolds;
    }
    public void setEqualSumSold(String equalSumSold) {
        this.equalSumSold = equalSumSold;
    }
    
    public String getEqualSumSold() {
        return this.equalSumSold;
    }
    
    public void setLikeSumSold(String likeSumSold) {
        this.likeSumSold = likeSumSold;
    }
    
    public String getLikeSumSold() {
        return this.likeSumSold;
    }
    
    public void setInSumSolds(String inSumSolds) {
        this.inSumSolds = inSumSolds;
    }
    
    public String getInSumSolds() {
        return this.inSumSolds;
    }
    public void setEqualSumUnsold(String equalSumUnsold) {
        this.equalSumUnsold = equalSumUnsold;
    }
    
    public String getEqualSumUnsold() {
        return this.equalSumUnsold;
    }
    
    public void setLikeSumUnsold(String likeSumUnsold) {
        this.likeSumUnsold = likeSumUnsold;
    }
    
    public String getLikeSumUnsold() {
        return this.likeSumUnsold;
    }
    
    public void setInSumUnsolds(String inSumUnsolds) {
        this.inSumUnsolds = inSumUnsolds;
    }
    
    public String getInSumUnsolds() {
        return this.inSumUnsolds;
    }
    public void setEqualDataUpdateTime(String equalDataUpdateTime) {
        this.equalDataUpdateTime = equalDataUpdateTime;
    }
    
    public String getEqualDataUpdateTime() {
        return this.equalDataUpdateTime;
    }
    
    public void setLikeDataUpdateTime(String likeDataUpdateTime) {
        this.likeDataUpdateTime = likeDataUpdateTime;
    }
    
    public String getLikeDataUpdateTime() {
        return this.likeDataUpdateTime;
    }
    
    public void setInDataUpdateTimes(String inDataUpdateTimes) {
        this.inDataUpdateTimes = inDataUpdateTimes;
    }
    
    public String getInDataUpdateTimes() {
        return this.inDataUpdateTimes;
    }
    public void setEqualSource(String equalSource) {
        this.equalSource = equalSource;
    }
    
    public String getEqualSource() {
        return this.equalSource;
    }
    
    public void setLikeSource(String likeSource) {
        this.likeSource = likeSource;
    }
    
    public String getLikeSource() {
        return this.likeSource;
    }
    
    public void setInSources(String inSources) {
        this.inSources = inSources;
    }
    
    public String getInSources() {
        return this.inSources;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalProductName)){
			criterias.add(Restrictions.eq("productName", equalProductName));
		}
	    if(StringUtils.isNotBlank(likeProductName)){
			criterias.add(Restrictions.allLike("productName", likeProductName));
		}
		 if(StringUtils.isNotBlank(inProductNames)){
			criterias.add(Restrictions.in("productName", StringUtils.split(inProductNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalDeveloper)){
			criterias.add(Restrictions.eq("developer", equalDeveloper));
		}
	    if(StringUtils.isNotBlank(likeDeveloper)){
			criterias.add(Restrictions.allLike("developer", likeDeveloper));
		}
		 if(StringUtils.isNotBlank(inDevelopers)){
			criterias.add(Restrictions.in("developer", StringUtils.split(inDevelopers, ",")));
		}
	    if(StringUtils.isNotBlank(equalPreSalePermit)){
			criterias.add(Restrictions.eq("preSalePermit", equalPreSalePermit));
		}
	    if(StringUtils.isNotBlank(likePreSalePermit)){
			criterias.add(Restrictions.allLike("preSalePermit", likePreSalePermit));
		}
		 if(StringUtils.isNotBlank(inPreSalePermits)){
			criterias.add(Restrictions.in("preSalePermit", StringUtils.split(inPreSalePermits, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectAddress)){
			criterias.add(Restrictions.eq("projectAddress", equalProjectAddress));
		}
	    if(StringUtils.isNotBlank(likeProjectAddress)){
			criterias.add(Restrictions.allLike("projectAddress", likeProjectAddress));
		}
		 if(StringUtils.isNotBlank(inProjectAddresss)){
			criterias.add(Restrictions.in("projectAddress", StringUtils.split(inProjectAddresss, ",")));
		}
	    if(StringUtils.isNotBlank(equalAdministrativeArea)){
			criterias.add(Restrictions.eq("administrativeArea", equalAdministrativeArea));
		}
	    if(StringUtils.isNotBlank(likeAdministrativeArea)){
			criterias.add(Restrictions.allLike("administrativeArea", likeAdministrativeArea));
		}
		 if(StringUtils.isNotBlank(inAdministrativeAreas)){
			criterias.add(Restrictions.in("administrativeArea", StringUtils.split(inAdministrativeAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalLandArea)){
			criterias.add(Restrictions.eq("landArea", equalLandArea));
		}
	    if(StringUtils.isNotBlank(likeLandArea)){
			criterias.add(Restrictions.allLike("landArea", likeLandArea));
		}
		 if(StringUtils.isNotBlank(inLandAreas)){
			criterias.add(Restrictions.in("landArea", StringUtils.split(inLandAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalConstructionArea)){
			criterias.add(Restrictions.eq("constructionArea", equalConstructionArea));
		}
	    if(StringUtils.isNotBlank(likeConstructionArea)){
			criterias.add(Restrictions.allLike("constructionArea", likeConstructionArea));
		}
		 if(StringUtils.isNotBlank(inConstructionAreas)){
			criterias.add(Restrictions.in("constructionArea", StringUtils.split(inConstructionAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalSoldArea)){
			criterias.add(Restrictions.eq("soldArea", equalSoldArea));
		}
	    if(StringUtils.isNotBlank(likeSoldArea)){
			criterias.add(Restrictions.allLike("soldArea", likeSoldArea));
		}
		 if(StringUtils.isNotBlank(inSoldAreas)){
			criterias.add(Restrictions.in("soldArea", StringUtils.split(inSoldAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalUnsoldArea)){
			criterias.add(Restrictions.eq("unsoldArea", equalUnsoldArea));
		}
	    if(StringUtils.isNotBlank(likeUnsoldArea)){
			criterias.add(Restrictions.allLike("unsoldArea", likeUnsoldArea));
		}
		 if(StringUtils.isNotBlank(inUnsoldAreas)){
			criterias.add(Restrictions.in("unsoldArea", StringUtils.split(inUnsoldAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseSold)){
			criterias.add(Restrictions.eq("houseSold", equalHouseSold));
		}
	    if(StringUtils.isNotBlank(likeHouseSold)){
			criterias.add(Restrictions.allLike("houseSold", likeHouseSold));
		}
		 if(StringUtils.isNotBlank(inHouseSolds)){
			criterias.add(Restrictions.in("houseSold", StringUtils.split(inHouseSolds, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseUnsold)){
			criterias.add(Restrictions.eq("houseUnsold", equalHouseUnsold));
		}
	    if(StringUtils.isNotBlank(likeHouseUnsold)){
			criterias.add(Restrictions.allLike("houseUnsold", likeHouseUnsold));
		}
		 if(StringUtils.isNotBlank(inHouseUnsolds)){
			criterias.add(Restrictions.in("houseUnsold", StringUtils.split(inHouseUnsolds, ",")));
		}
	    if(StringUtils.isNotBlank(equalSumSold)){
			criterias.add(Restrictions.eq("sumSold", equalSumSold));
		}
	    if(StringUtils.isNotBlank(likeSumSold)){
			criterias.add(Restrictions.allLike("sumSold", likeSumSold));
		}
		 if(StringUtils.isNotBlank(inSumSolds)){
			criterias.add(Restrictions.in("sumSold", StringUtils.split(inSumSolds, ",")));
		}
	    if(StringUtils.isNotBlank(equalSumUnsold)){
			criterias.add(Restrictions.eq("sumUnsold", equalSumUnsold));
		}
	    if(StringUtils.isNotBlank(isNull)){
			criterias.add(Restrictions.isNull(isNull));
		}
	    if(StringUtils.isNotBlank(likeSumUnsold)){
			criterias.add(Restrictions.allLike("sumUnsold", likeSumUnsold));
		}
		 if(StringUtils.isNotBlank(inSumUnsolds)){
			criterias.add(Restrictions.in("sumUnsold", StringUtils.split(inSumUnsolds, ",")));
		}
	    if(StringUtils.isNotBlank(equalDataUpdateTime)){
			criterias.add(Restrictions.eq("dataUpdateTime", equalDataUpdateTime));
		}
	    if(StringUtils.isNotBlank(likeDataUpdateTime)){
			criterias.add(Restrictions.allLike("dataUpdateTime", likeDataUpdateTime));
		}
		 if(StringUtils.isNotBlank(inDataUpdateTimes)){
			criterias.add(Restrictions.in("dataUpdateTime", StringUtils.split(inDataUpdateTimes, ",")));
		}
	    if(StringUtils.isNotBlank(equalSource)){
			criterias.add(Restrictions.eq("source", equalSource));
		}
	    if(StringUtils.isNotBlank(likeSource)){
			criterias.add(Restrictions.allLike("source", likeSource));
		}
		 if(StringUtils.isNotBlank(inSources)){
			criterias.add(Restrictions.in("source", StringUtils.split(inSources, ",")));
		}
		if(StringUtils.isNotBlank(equalProjectId)){
			criterias.add(Restrictions.eq("projectId", equalProjectId));
		}
	    if(StringUtils.isNotBlank(likeProjectId)){
			criterias.add(Restrictions.allLike("projectId", likeProjectId));
		}
		if(StringUtils.isNotBlank(inProjectId)){
			criterias.add(Restrictions.in("projectId", StringUtils.split(inProjectId, ",")));
		} 
		if(StringUtils.isNotBlank(equalCity)){
			criterias.add(Restrictions.eq("city", equalCity));
		}
	    if(StringUtils.isNotBlank(likeCity)){
			criterias.add(Restrictions.allLike("city", likeCity));
		}
		 if(StringUtils.isNotBlank(inCity)){
			criterias.add(Restrictions.in("city", StringUtils.split(inCity, ",")));
		} 
	}
}