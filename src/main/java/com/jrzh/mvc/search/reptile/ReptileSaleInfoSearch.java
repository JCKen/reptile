package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;

import com.jrzh.framework.base.search.BaseSearch;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;

public class ReptileSaleInfoSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 成交套数
     */
    private String equalAllNumber;
    
    /**
     * Like 成交套数
     */
    private String likeAllNumber;
    
     /**
     * In 成交套数 英文逗号分隔
     */
    private String inAllNumbers;
    
    /**
     * Equal 成交面积
     */
    private String equalAllArea;
    
    /**
     * Like 成交面积
     */
    private String likeAllArea;
    
     /**
     * In 成交面积 英文逗号分隔
     */
    private String inAllAreas;
    
    /**
     * Equal 城市(
     */
    private String equalCity;
    
    /**
     * Like 城市(
     */
    private String likeCity;
    
     /**
     * In 城市( 英文逗号分隔
     */
    private String inCitys;
    
    /**
     * Equal 成交价格
     */
    private String equalAllPrice;
    
    /**
     * Like 成交价格
     */
    private String likeAllPrice;
    
     /**
     * In 成交价格 英文逗号分隔
     */
    private String inAllPrices;
    
    /**
     * Equal 时间
     */
    private String equalInfoUpdataTime;
    
    /**
     * Like 时间
     */
    private String likeInfoUpdataTime;
    
     /**
     * In 时间 英文逗号分隔
     */
    private String inInfoUpdataTimes;
    

    public void setEqualAllNumber(String equalAllNumber) {
        this.equalAllNumber = equalAllNumber;
    }
    
    public String getEqualAllNumber() {
        return this.equalAllNumber;
    }
    
    public void setLikeAllNumber(String likeAllNumber) {
        this.likeAllNumber = likeAllNumber;
    }
    
    public String getLikeAllNumber() {
        return this.likeAllNumber;
    }
    
    public void setInAllNumbers(String inAllNumbers) {
        this.inAllNumbers = inAllNumbers;
    }
    
    public String getInAllNumbers() {
        return this.inAllNumbers;
    }
    public void setEqualAllArea(String equalAllArea) {
        this.equalAllArea = equalAllArea;
    }
    
    public String getEqualAllArea() {
        return this.equalAllArea;
    }
    
    public void setLikeAllArea(String likeAllArea) {
        this.likeAllArea = likeAllArea;
    }
    
    public String getLikeAllArea() {
        return this.likeAllArea;
    }
    
    public void setInAllAreas(String inAllAreas) {
        this.inAllAreas = inAllAreas;
    }
    
    public String getInAllAreas() {
        return this.inAllAreas;
    }
    public void setEqualCity(String equalCity) {
        this.equalCity = equalCity;
    }
    
    public String getEqualCity() {
        return this.equalCity;
    }
    
    public void setLikeCity(String likeCity) {
        this.likeCity = likeCity;
    }
    
    public String getLikeCity() {
        return this.likeCity;
    }
    
    public void setInCitys(String inCitys) {
        this.inCitys = inCitys;
    }
    
    public String getInCitys() {
        return this.inCitys;
    }
    public void setEqualAllPrice(String equalAllPrice) {
        this.equalAllPrice = equalAllPrice;
    }
    
    public String getEqualAllPrice() {
        return this.equalAllPrice;
    }
    
    public void setLikeAllPrice(String likeAllPrice) {
        this.likeAllPrice = likeAllPrice;
    }
    
    public String getLikeAllPrice() {
        return this.likeAllPrice;
    }
    
    public void setInAllPrices(String inAllPrices) {
        this.inAllPrices = inAllPrices;
    }
    
    public String getInAllPrices() {
        return this.inAllPrices;
    }
    public void setEqualInfoUpdataTime(String equalInfoUpdataTime) {
        this.equalInfoUpdataTime = equalInfoUpdataTime;
    }
    
    public String getEqualInfoUpdataTime() {
        return this.equalInfoUpdataTime;
    }
    
    public void setLikeInfoUpdataTime(String likeInfoUpdataTime) {
        this.likeInfoUpdataTime = likeInfoUpdataTime;
    }
    
    public String getLikeInfoUpdataTime() {
        return this.likeInfoUpdataTime;
    }
    
    public void setInInfoUpdataTimes(String inInfoUpdataTimes) {
        this.inInfoUpdataTimes = inInfoUpdataTimes;
    }
    
    public String getInInfoUpdataTimes() {
        return this.inInfoUpdataTimes;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalAllNumber)){
			criterias.add(Restrictions.eq("allNumber", equalAllNumber));
		}
	    if(StringUtils.isNotBlank(likeAllNumber)){
			criterias.add(Restrictions.allLike("allNumber", likeAllNumber));
		}
		 if(StringUtils.isNotBlank(inAllNumbers)){
			criterias.add(Restrictions.in("allNumber", StringUtils.split(inAllNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalAllArea)){
			criterias.add(Restrictions.eq("allArea", equalAllArea));
		}
	    if(StringUtils.isNotBlank(likeAllArea)){
			criterias.add(Restrictions.allLike("allArea", likeAllArea));
		}
		 if(StringUtils.isNotBlank(inAllAreas)){
			criterias.add(Restrictions.in("allArea", StringUtils.split(inAllAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalCity)){
			criterias.add(Restrictions.eq("city", equalCity));
		}
	    if(StringUtils.isNotBlank(likeCity)){
			criterias.add(Restrictions.allLike("city", likeCity));
		}
		 if(StringUtils.isNotBlank(inCitys)){
			criterias.add(Restrictions.in("city", StringUtils.split(inCitys, ",")));
		}
	    if(StringUtils.isNotBlank(equalAllPrice)){
			criterias.add(Restrictions.eq("allPrice", equalAllPrice));
		}
	    if(StringUtils.isNotBlank(likeAllPrice)){
			criterias.add(Restrictions.allLike("allPrice", likeAllPrice));
		}
		 if(StringUtils.isNotBlank(inAllPrices)){
			criterias.add(Restrictions.in("allPrice", StringUtils.split(inAllPrices, ",")));
		}
	    if(StringUtils.isNotBlank(equalInfoUpdataTime)){
			criterias.add(Restrictions.eq("infoUpdataTime", equalInfoUpdataTime));
		}
	    if(StringUtils.isNotBlank(likeInfoUpdataTime)){
			criterias.add(Restrictions.allLike("infoUpdataTime", likeInfoUpdataTime));
		}
		 if(StringUtils.isNotBlank(inInfoUpdataTimes)){
			criterias.add(Restrictions.in("infoUpdataTime", StringUtils.split(inInfoUpdataTimes, ",")));
		}
	}
}