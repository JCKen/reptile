package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileEsfHousesXiaoquSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 小区名
     */
    private String equalHouseName;
    
    /**
     * Like 小区名
     */
    private String likeHouseName;
    
     /**
     * In 小区名 英文逗号分隔
     */
    private String inHouseNames;
    
    /**
     * Equal 小区别名
     */
    private String equalHousesOtherName;
    
    /**
     * Like 小区别名
     */
    private String likeHousesOtherName;
    
     /**
     * In 小区别名 英文逗号分隔
     */
    private String inHousesOtherNames;
    
    /**
     * Equal 小区所属城市
     */
    private String equalHousesCity;
    
    /**
     * Like 小区所属城市
     */
    private String likeHousesCity;
    
     /**
     * In 小区所属城市 英文逗号分隔
     */
    private String inHousesCitys;
    
    /**
     * Equal 小区所属区域
     */
    private String equalHousesPart;
    
    /**
     * Like 小区所属区域
     */
    private String likeHousesPart;
    
     /**
     * In 小区所属区域 英文逗号分隔
     */
    private String inHousesParts;
    
    /**
     * Equal 所在地址
     */
    private String equalHousesAddress;
    
    /**
     * Like 所在地址
     */
    private String likeHousesAddress;
    
     /**
     * In 所在地址 英文逗号分隔
     */
    private String inHousesAddresss;
    
    /**
     * Equal 小区建筑年代
     */
    private String equalHousesBuildingYear;
    
    /**
     * Like 小区建筑年代
     */
    private String likeHousesBuildingYear;
    
    /**
     * 大于建成年代
     */
    private String gtHouseBuildingYear;
    
     /**
     * In 小区建筑年代 英文逗号分隔
     */
    private String inHousesBuildingYears;
    
    /**
     * Equal 房屋类型
     */
    private String equalHousesType;
    
    /**
     * Like 房屋类型
     */
    private String likeHousesType;
    
     /**
     * In 房屋类型 英文逗号分隔
     */
    private String inHousesTypes;
    
    /**
     * Equal 房屋均价
     */
    private String equalHousesPrice;
    
    /**
     * Like 房屋均价
     */
    private String likeHousesPrice;
    
     /**
     * In 房屋均价 英文逗号分隔
     */
    private String inHousesPrices;
    
    /**
     * Equal 产权年限
     */
    private String equalYearOfPropertyRights;
    
    /**
     * Like 产权年限
     */
    private String likeYearOfPropertyRights;
    
     /**
     * In 产权年限 英文逗号分隔
     */
    private String inYearOfPropertyRightss;
    
    /**
     * Equal 信息来源
     */
    private String equalInformationSources;
    
    /**
     * Like 信息来源
     */
    private String likeInformationSources;
    
     /**
     * In 信息来源 英文逗号分隔
     */
    private String inInformationSourcess;
    
    /**
     * Equal 信息更新时间
     */
    private String equalInfoUpdateTime;
    
    /**
     * Like 信息更新时间
     */
    private String likeInfoUpdateTime;
    
     /**
     * In 信息更新时间 英文逗号分隔
     */
    private String inInfoUpdateTimes;
    
    /**
     * Equal 
     */
    private String equalLatitude;
    
    /**
     * Like 
     */
    private String likeLatitude;
    
     /**
     * In  英文逗号分隔
     */
    private String inLatitudes;
    
    /**
     * Equal 
     */
    private String equalLongitude;
    
    /**
     * Like 
     */
    private String likeLongitude;
    
     /**
     * In  英文逗号分隔
     */
    private String inLongitudes;
    
    /**
     * Equal 开发商
     */
    private String equalDevelopers;
    
    /**
     * Like 开发商
     */
    private String likeDevelopers;
    
     /**
     * In 开发商 英文逗号分隔
     */
    private String inDeveloperss;
    
    /**
     * Equal 绿化率
     */
    private String equalAfforestationRate;
    
    /**
     * Like 绿化率
     */
    private String likeAfforestationRate;
    
     /**
     * In 绿化率 英文逗号分隔
     */
    private String inAfforestationRates;
    
    /**
     * Equal 容积率
     */
    private String equalPlotRatio;
    
    /**
     * Like 容积率
     */
    private String likePlotRatio;
    
     /**
     * In 容积率 英文逗号分隔
     */
    private String inPlotRatios;
    
    /**
     * Equal 建筑类型
     */
    private String equalBuildingType;
    
    /**
     * Like 建筑类型
     */
    private String likeBuildingType;
    
     /**
     * In 建筑类型 英文逗号分隔
     */
    private String inBuildingTypes;
    
    /**
     * Equal 物业公司
     */
    private String equalPropertyCompany;
    
    /**
     * Like 物业公司
     */
    private String likePropertyCompany;
    
     /**
     * In 物业公司 英文逗号分隔
     */
    private String inPropertyCompanys;
    
    /**
     * Equal 房屋总数
     */
    private String equalTotalHouseholds;
    
    /**
     * Like 房屋总数
     */
    private String likeTotalHouseholds;
    
     /**
     * In 房屋总数 英文逗号分隔
     */
    private String inTotalHouseholdss;
    
    /**
     * Equal 物业费
     */
    private String equalPropertyCosts;
    
    /**
     * Like 物业费
     */
    private String likePropertyCosts;
    
     /**
     * In 物业费 英文逗号分隔
     */
    private String inPropertyCostss;
    
    /**
     * Equal 附近交通
     */
    private String equalTraffic;
    
    /**
     * Like 附近交通
     */
    private String likeTraffic;
    
     /**
     * In 附近交通 英文逗号分隔
     */
    private String inTraffics;
    
    /**
     * Equal 幼儿园
     */
    private String equalKindergarten;
    
    /**
     * Like 幼儿园
     */
    private String likeKindergarten;
    
     /**
     * In 幼儿园 英文逗号分隔
     */
    private String inKindergartens;
    
    /**
     * Equal 中小学
     */
    private String equalSchool;
    
    /**
     * Like 中小学
     */
    private String likeSchool;
    
     /**
     * In 中小学 英文逗号分隔
     */
    private String inSchools;
    
    /**
     * Equal 综合商场
     */
    private String equalPowerCenter;
    
    /**
     * Like 综合商场
     */
    private String likePowerCenter;
    
     /**
     * In 综合商场 英文逗号分隔
     */
    private String inPowerCenters;
    
    /**
     * Equal 医院
     */
    private String equalHospital;
    
    /**
     * Like 医院
     */
    private String likeHospital;
    
     /**
     * In 医院 英文逗号分隔
     */
    private String inHospitals;
    
    /**
     * Equal 银行
     */
    private String equalBank;
    
    /**
     * Like 银行
     */
    private String likeBank;
    
     /**
     * In 银行 英文逗号分隔
     */
    private String inBanks;
    
    /**
     * Equal 邮政
     */
    private String equalPost;
    
    /**
     * Like 邮政
     */
    private String likePost;
    
     /**
     * In 邮政 英文逗号分隔
     */
    private String inPosts;
    
    /**
     * Equal 其他
     */
    private String equalOther;
    
    /**
     * Like 其他
     */
    private String likeOther;
    
     /**
     * In 其他 英文逗号分隔
     */
    private String inOthers;
    
    /**
     * Equal 占地面积
     */
    private String equalFloorArea;
    
    /**
     * Like 占地面积
     */
    private String likeFloorArea;
    
     /**
     * In 占地面积 英文逗号分隔
     */
    private String inFloorAreas;
    
    /**
     * Equal 建筑面积
     */
    private String equalBuildingArea;
    
    /**
     * Like 建筑面积
     */
    private String likeBuildingArea;
    
     /**
     * In 建筑面积 英文逗号分隔
     */
    private String inBuildingAreas;
    
    /**
     * Equal 数据访问地址
     */
    private String equalUrl;
    
    /**
     * Like 数据访问地址
     */
    private String likeUrl;
    
     /**
     * In 数据访问地址 英文逗号分隔
     */
    private String inUrls;
    
    
    public String getEqualHouseName() {
		return equalHouseName;
	}

    public void setequalHouseName(String equalHouseName) {
        this.equalHouseName = equalHouseName;
    }
    
    public String getequalHouseName() {
        return this.equalHouseName;
    }
    
    public void setlikeHouseName(String likeHouseName) {
        this.likeHouseName = likeHouseName;
    }
    
    public String getlikeHouseName() {
        return this.likeHouseName;
    }
    
    public void setinHouseNames(String inHouseNames) {
        this.inHouseNames = inHouseNames;
    }
    
   
	public void setEqualHouseName(String equalHouseName) {
		this.equalHouseName = equalHouseName;
	}

	public String getLikeHouseName() {
		return likeHouseName;
	}

	public void setLikeHouseName(String likeHouseName) {
		this.likeHouseName = likeHouseName;
	}

	public void setInHouseNames(String inHouseNames) {
		this.inHouseNames = inHouseNames;
	}

	public void setEqualHousesOtherName(String equalHousesOtherName) {
        this.equalHousesOtherName = equalHousesOtherName;
    }
    
    public String getEqualHousesOtherName() {
        return this.equalHousesOtherName;
    }
    
    public void setLikeHousesOtherName(String likeHousesOtherName) {
        this.likeHousesOtherName = likeHousesOtherName;
    }
    
    public String getLikeHousesOtherName() {
        return this.likeHousesOtherName;
    }
    
    public void setInHousesOtherNames(String inHousesOtherNames) {
        this.inHousesOtherNames = inHousesOtherNames;
    }
    
    public String getInHousesOtherNames() {
        return this.inHousesOtherNames;
    }
    public void setEqualHousesCity(String equalHousesCity) {
        this.equalHousesCity = equalHousesCity;
    }
    
    public String getEqualHousesCity() {
        return this.equalHousesCity;
    }
    
    public void setLikeHousesCity(String likeHousesCity) {
        this.likeHousesCity = likeHousesCity;
    }
    
    public String getLikeHousesCity() {
        return this.likeHousesCity;
    }
    
    public void setInHousesCitys(String inHousesCitys) {
        this.inHousesCitys = inHousesCitys;
    }
    
    public String getInHousesCitys() {
        return this.inHousesCitys;
    }
    public void setEqualHousesPart(String equalHousesPart) {
        this.equalHousesPart = equalHousesPart;
    }
    
    public String getEqualHousesPart() {
        return this.equalHousesPart;
    }
    
    public void setLikeHousesPart(String likeHousesPart) {
        this.likeHousesPart = likeHousesPart;
    }
    
    public String getLikeHousesPart() {
        return this.likeHousesPart;
    }
    
    public void setInHousesParts(String inHousesParts) {
        this.inHousesParts = inHousesParts;
    }
    
    public String getInHousesParts() {
        return this.inHousesParts;
    }
    public void setEqualHousesAddress(String equalHousesAddress) {
        this.equalHousesAddress = equalHousesAddress;
    }
    
    public String getEqualHousesAddress() {
        return this.equalHousesAddress;
    }
    
    public void setLikeHousesAddress(String likeHousesAddress) {
        this.likeHousesAddress = likeHousesAddress;
    }
    
    public String getLikeHousesAddress() {
        return this.likeHousesAddress;
    }
    
    public void setInHousesAddresss(String inHousesAddresss) {
        this.inHousesAddresss = inHousesAddresss;
    }
    
    public String getInHousesAddresss() {
        return this.inHousesAddresss;
    }
    public void setEqualHousesBuildingYear(String equalHousesBuildingYear) {
        this.equalHousesBuildingYear = equalHousesBuildingYear;
    }
    
    public String getEqualHousesBuildingYear() {
        return this.equalHousesBuildingYear;
    }
    
    public void setLikeHousesBuildingYear(String likeHousesBuildingYear) {
        this.likeHousesBuildingYear = likeHousesBuildingYear;
    }
    
    public String getLikeHousesBuildingYear() {
        return this.likeHousesBuildingYear;
    }
    
    public void setInHousesBuildingYears(String inHousesBuildingYears) {
        this.inHousesBuildingYears = inHousesBuildingYears;
    }
    
    public String getInHousesBuildingYears() {
        return this.inHousesBuildingYears;
    }
    public void setEqualHousesType(String equalHousesType) {
        this.equalHousesType = equalHousesType;
    }
    
    public String getEqualHousesType() {
        return this.equalHousesType;
    }
    
    public void setLikeHousesType(String likeHousesType) {
        this.likeHousesType = likeHousesType;
    }
    
    public String getLikeHousesType() {
        return this.likeHousesType;
    }
    
    public void setInHousesTypes(String inHousesTypes) {
        this.inHousesTypes = inHousesTypes;
    }
    
    public String getInHousesTypes() {
        return this.inHousesTypes;
    }
    public void setEqualHousesPrice(String equalHousesPrice) {
        this.equalHousesPrice = equalHousesPrice;
    }
    
    public String getEqualHousesPrice() {
        return this.equalHousesPrice;
    }
    
    public void setLikeHousesPrice(String likeHousesPrice) {
        this.likeHousesPrice = likeHousesPrice;
    }
    
    public String getLikeHousesPrice() {
        return this.likeHousesPrice;
    }
    
    public void setInHousesPrices(String inHousesPrices) {
        this.inHousesPrices = inHousesPrices;
    }
    
    public String getInHousesPrices() {
        return this.inHousesPrices;
    }
    public void setEqualYearOfPropertyRights(String equalYearOfPropertyRights) {
        this.equalYearOfPropertyRights = equalYearOfPropertyRights;
    }
    
    public String getEqualYearOfPropertyRights() {
        return this.equalYearOfPropertyRights;
    }
    
    public void setLikeYearOfPropertyRights(String likeYearOfPropertyRights) {
        this.likeYearOfPropertyRights = likeYearOfPropertyRights;
    }
    
    public String getLikeYearOfPropertyRights() {
        return this.likeYearOfPropertyRights;
    }
    
    public void setInYearOfPropertyRightss(String inYearOfPropertyRightss) {
        this.inYearOfPropertyRightss = inYearOfPropertyRightss;
    }
    
    public String getInYearOfPropertyRightss() {
        return this.inYearOfPropertyRightss;
    }
    public void setEqualInformationSources(String equalInformationSources) {
        this.equalInformationSources = equalInformationSources;
    }
    
    public String getEqualInformationSources() {
        return this.equalInformationSources;
    }
    
    public void setLikeInformationSources(String likeInformationSources) {
        this.likeInformationSources = likeInformationSources;
    }
    
    public String getLikeInformationSources() {
        return this.likeInformationSources;
    }
    
    public void setInInformationSourcess(String inInformationSourcess) {
        this.inInformationSourcess = inInformationSourcess;
    }
    
    public String getInInformationSourcess() {
        return this.inInformationSourcess;
    }
    public void setEqualInfoUpdateTime(String equalInfoUpdateTime) {
        this.equalInfoUpdateTime = equalInfoUpdateTime;
    }
    
    public String getEqualInfoUpdateTime() {
        return this.equalInfoUpdateTime;
    }
    
    public void setLikeInfoUpdateTime(String likeInfoUpdateTime) {
        this.likeInfoUpdateTime = likeInfoUpdateTime;
    }
    
    public String getLikeInfoUpdateTime() {
        return this.likeInfoUpdateTime;
    }
    
    public void setInInfoUpdateTimes(String inInfoUpdateTimes) {
        this.inInfoUpdateTimes = inInfoUpdateTimes;
    }
    
    public String getInInfoUpdateTimes() {
        return this.inInfoUpdateTimes;
    }
    public void setEqualLatitude(String equalLatitude) {
        this.equalLatitude = equalLatitude;
    }
    
    public String getEqualLatitude() {
        return this.equalLatitude;
    }
    
    public void setLikeLatitude(String likeLatitude) {
        this.likeLatitude = likeLatitude;
    }
    
    public String getLikeLatitude() {
        return this.likeLatitude;
    }
    
    public void setInLatitudes(String inLatitudes) {
        this.inLatitudes = inLatitudes;
    }
    
    public String getInLatitudes() {
        return this.inLatitudes;
    }
    public void setEqualLongitude(String equalLongitude) {
        this.equalLongitude = equalLongitude;
    }
    
    public String getEqualLongitude() {
        return this.equalLongitude;
    }
    
    public void setLikeLongitude(String likeLongitude) {
        this.likeLongitude = likeLongitude;
    }
    
    public String getLikeLongitude() {
        return this.likeLongitude;
    }
    
    public void setInLongitudes(String inLongitudes) {
        this.inLongitudes = inLongitudes;
    }
    
    public String getInLongitudes() {
        return this.inLongitudes;
    }
    public void setEqualDevelopers(String equalDevelopers) {
        this.equalDevelopers = equalDevelopers;
    }
    
    public String getEqualDevelopers() {
        return this.equalDevelopers;
    }
    
    public void setLikeDevelopers(String likeDevelopers) {
        this.likeDevelopers = likeDevelopers;
    }
    
    public String getLikeDevelopers() {
        return this.likeDevelopers;
    }
    
    public void setInDeveloperss(String inDeveloperss) {
        this.inDeveloperss = inDeveloperss;
    }
    
    public String getInDeveloperss() {
        return this.inDeveloperss;
    }
    public void setEqualAfforestationRate(String equalAfforestationRate) {
        this.equalAfforestationRate = equalAfforestationRate;
    }
    
    public String getEqualAfforestationRate() {
        return this.equalAfforestationRate;
    }
    
    public void setLikeAfforestationRate(String likeAfforestationRate) {
        this.likeAfforestationRate = likeAfforestationRate;
    }
    
    public String getLikeAfforestationRate() {
        return this.likeAfforestationRate;
    }
    
    public void setInAfforestationRates(String inAfforestationRates) {
        this.inAfforestationRates = inAfforestationRates;
    }
    
    public String getInAfforestationRates() {
        return this.inAfforestationRates;
    }
    public void setEqualPlotRatio(String equalPlotRatio) {
        this.equalPlotRatio = equalPlotRatio;
    }
    
    public String getEqualPlotRatio() {
        return this.equalPlotRatio;
    }
    
    public void setLikePlotRatio(String likePlotRatio) {
        this.likePlotRatio = likePlotRatio;
    }
    
    public String getLikePlotRatio() {
        return this.likePlotRatio;
    }
    
    public void setInPlotRatios(String inPlotRatios) {
        this.inPlotRatios = inPlotRatios;
    }
    
    public String getInPlotRatios() {
        return this.inPlotRatios;
    }
    public void setEqualBuildingType(String equalBuildingType) {
        this.equalBuildingType = equalBuildingType;
    }
    
    public String getEqualBuildingType() {
        return this.equalBuildingType;
    }
    
    public void setLikeBuildingType(String likeBuildingType) {
        this.likeBuildingType = likeBuildingType;
    }
    
    public String getLikeBuildingType() {
        return this.likeBuildingType;
    }
    
    public void setInBuildingTypes(String inBuildingTypes) {
        this.inBuildingTypes = inBuildingTypes;
    }
    
    public String getInBuildingTypes() {
        return this.inBuildingTypes;
    }
    public void setEqualPropertyCompany(String equalPropertyCompany) {
        this.equalPropertyCompany = equalPropertyCompany;
    }
    
    public String getEqualPropertyCompany() {
        return this.equalPropertyCompany;
    }
    
    public void setLikePropertyCompany(String likePropertyCompany) {
        this.likePropertyCompany = likePropertyCompany;
    }
    
    public String getLikePropertyCompany() {
        return this.likePropertyCompany;
    }
    
    public void setInPropertyCompanys(String inPropertyCompanys) {
        this.inPropertyCompanys = inPropertyCompanys;
    }
    
    public String getInPropertyCompanys() {
        return this.inPropertyCompanys;
    }
    public void setEqualTotalHouseholds(String equalTotalHouseholds) {
        this.equalTotalHouseholds = equalTotalHouseholds;
    }
    
    public String getEqualTotalHouseholds() {
        return this.equalTotalHouseholds;
    }
    
    public void setLikeTotalHouseholds(String likeTotalHouseholds) {
        this.likeTotalHouseholds = likeTotalHouseholds;
    }
    
    public String getLikeTotalHouseholds() {
        return this.likeTotalHouseholds;
    }
    
    public void setInTotalHouseholdss(String inTotalHouseholdss) {
        this.inTotalHouseholdss = inTotalHouseholdss;
    }
    
    public String getInTotalHouseholdss() {
        return this.inTotalHouseholdss;
    }
    public void setEqualPropertyCosts(String equalPropertyCosts) {
        this.equalPropertyCosts = equalPropertyCosts;
    }
    
    public String getEqualPropertyCosts() {
        return this.equalPropertyCosts;
    }
    
    public void setLikePropertyCosts(String likePropertyCosts) {
        this.likePropertyCosts = likePropertyCosts;
    }
    
    public String getLikePropertyCosts() {
        return this.likePropertyCosts;
    }
    
    public void setInPropertyCostss(String inPropertyCostss) {
        this.inPropertyCostss = inPropertyCostss;
    }
    
    public String getInPropertyCostss() {
        return this.inPropertyCostss;
    }
    public void setEqualTraffic(String equalTraffic) {
        this.equalTraffic = equalTraffic;
    }
    
    public String getEqualTraffic() {
        return this.equalTraffic;
    }
    
    public void setLikeTraffic(String likeTraffic) {
        this.likeTraffic = likeTraffic;
    }
    
    public String getLikeTraffic() {
        return this.likeTraffic;
    }
    
    public String getGtHouseBuildingYear() {
		return gtHouseBuildingYear;
	}

	public void setGtHouseBuildingYear(String gtHouseBuildingYear) {
		this.gtHouseBuildingYear = gtHouseBuildingYear;
	}

	public void setInTraffics(String inTraffics) {
        this.inTraffics = inTraffics;
    }
    
    public String getInTraffics() {
        return this.inTraffics;
    }
    public void setEqualKindergarten(String equalKindergarten) {
        this.equalKindergarten = equalKindergarten;
    }
    
    public String getEqualKindergarten() {
        return this.equalKindergarten;
    }
    
    public void setLikeKindergarten(String likeKindergarten) {
        this.likeKindergarten = likeKindergarten;
    }
    
    public String getLikeKindergarten() {
        return this.likeKindergarten;
    }
    
    public void setInKindergartens(String inKindergartens) {
        this.inKindergartens = inKindergartens;
    }
    
    public String getInKindergartens() {
        return this.inKindergartens;
    }
    public void setEqualSchool(String equalSchool) {
        this.equalSchool = equalSchool;
    }
    
    public String getEqualSchool() {
        return this.equalSchool;
    }
    
    public void setLikeSchool(String likeSchool) {
        this.likeSchool = likeSchool;
    }
    
    public String getLikeSchool() {
        return this.likeSchool;
    }
    
    public void setInSchools(String inSchools) {
        this.inSchools = inSchools;
    }
    
    public String getInSchools() {
        return this.inSchools;
    }
    public void setEqualPowerCenter(String equalPowerCenter) {
        this.equalPowerCenter = equalPowerCenter;
    }
    
    public String getEqualPowerCenter() {
        return this.equalPowerCenter;
    }
    
    public void setLikePowerCenter(String likePowerCenter) {
        this.likePowerCenter = likePowerCenter;
    }
    
    public String getLikePowerCenter() {
        return this.likePowerCenter;
    }
    
    public void setInPowerCenters(String inPowerCenters) {
        this.inPowerCenters = inPowerCenters;
    }
    
    public String getInPowerCenters() {
        return this.inPowerCenters;
    }
    public void setEqualHospital(String equalHospital) {
        this.equalHospital = equalHospital;
    }
    
    public String getEqualHospital() {
        return this.equalHospital;
    }
    
    public void setLikeHospital(String likeHospital) {
        this.likeHospital = likeHospital;
    }
    
    public String getLikeHospital() {
        return this.likeHospital;
    }
    
    public void setInHospitals(String inHospitals) {
        this.inHospitals = inHospitals;
    }
    
    public String getInHospitals() {
        return this.inHospitals;
    }
    public void setEqualBank(String equalBank) {
        this.equalBank = equalBank;
    }
    
    public String getEqualBank() {
        return this.equalBank;
    }
    
    public void setLikeBank(String likeBank) {
        this.likeBank = likeBank;
    }
    
    public String getLikeBank() {
        return this.likeBank;
    }
    
    public void setInBanks(String inBanks) {
        this.inBanks = inBanks;
    }
    
    public String getInBanks() {
        return this.inBanks;
    }
    public void setEqualPost(String equalPost) {
        this.equalPost = equalPost;
    }
    
    public String getEqualPost() {
        return this.equalPost;
    }
    
    public void setLikePost(String likePost) {
        this.likePost = likePost;
    }
    
    public String getLikePost() {
        return this.likePost;
    }
    
    public void setInPosts(String inPosts) {
        this.inPosts = inPosts;
    }
    
    public String getInPosts() {
        return this.inPosts;
    }
    public void setEqualOther(String equalOther) {
        this.equalOther = equalOther;
    }
    
    public String getEqualOther() {
        return this.equalOther;
    }
    
    public void setLikeOther(String likeOther) {
        this.likeOther = likeOther;
    }
    
    public String getLikeOther() {
        return this.likeOther;
    }
    
    public void setInOthers(String inOthers) {
        this.inOthers = inOthers;
    }
    
    public String getInOthers() {
        return this.inOthers;
    }
    public void setEqualFloorArea(String equalFloorArea) {
        this.equalFloorArea = equalFloorArea;
    }
    
    public String getEqualFloorArea() {
        return this.equalFloorArea;
    }
    
    public void setLikeFloorArea(String likeFloorArea) {
        this.likeFloorArea = likeFloorArea;
    }
    
    public String getLikeFloorArea() {
        return this.likeFloorArea;
    }
    
    public void setInFloorAreas(String inFloorAreas) {
        this.inFloorAreas = inFloorAreas;
    }
    
    public String getInFloorAreas() {
        return this.inFloorAreas;
    }
    public void setEqualBuildingArea(String equalBuildingArea) {
        this.equalBuildingArea = equalBuildingArea;
    }
    
    public String getEqualBuildingArea() {
        return this.equalBuildingArea;
    }
    
    public void setLikeBuildingArea(String likeBuildingArea) {
        this.likeBuildingArea = likeBuildingArea;
    }
    
    public String getLikeBuildingArea() {
        return this.likeBuildingArea;
    }
    
    public void setInBuildingAreas(String inBuildingAreas) {
        this.inBuildingAreas = inBuildingAreas;
    }
    
    public String getInBuildingAreas() {
        return this.inBuildingAreas;
    }
    public void setEqualUrl(String equalUrl) {
        this.equalUrl = equalUrl;
    }
    
    public String getEqualUrl() {
        return this.equalUrl;
    }
    
    public void setLikeUrl(String likeUrl) {
        this.likeUrl = likeUrl;
    }
    
    public String getLikeUrl() {
        return this.likeUrl;
    }
    
    public void setInUrls(String inUrls) {
        this.inUrls = inUrls;
    }
    
    public String getInUrls() {
        return this.inUrls;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalHouseName)){
			criterias.add(Restrictions.eq("houseName", equalHouseName));
		}
	    if(StringUtils.isNotBlank(likeHouseName)){
			criterias.add(Restrictions.allLike("houseName", likeHouseName));
		}
		 if(StringUtils.isNotBlank(inHouseNames)){
			criterias.add(Restrictions.in("houseName", StringUtils.split(inHouseNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalHousesOtherName)){
			criterias.add(Restrictions.eq("housesOtherName", equalHousesOtherName));
		}
	    if(StringUtils.isNotBlank(likeHousesOtherName)){
			criterias.add(Restrictions.allLike("housesOtherName", likeHousesOtherName));
		}
		 if(StringUtils.isNotBlank(inHousesOtherNames)){
			criterias.add(Restrictions.in("housesOtherName", StringUtils.split(inHousesOtherNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalHousesCity)){
			criterias.add(Restrictions.eq("housesCity", equalHousesCity));
		}
	    if(StringUtils.isNotBlank(likeHousesCity)){
			criterias.add(Restrictions.allLike("housesCity", likeHousesCity));
		}
		 if(StringUtils.isNotBlank(inHousesCitys)){
			criterias.add(Restrictions.in("housesCity", StringUtils.split(inHousesCitys, ",")));
		}
	    if(StringUtils.isNotBlank(equalHousesPart)){
			criterias.add(Restrictions.eq("housesPart", equalHousesPart));
		}
	    if(StringUtils.isNotBlank(likeHousesPart)){
			criterias.add(Restrictions.allLike("housesPart", likeHousesPart));
		}
		 if(StringUtils.isNotBlank(inHousesParts)){
			criterias.add(Restrictions.in("housesPart", StringUtils.split(inHousesParts, ",")));
		}
	    if(StringUtils.isNotBlank(equalHousesAddress)){
			criterias.add(Restrictions.eq("housesAddress", equalHousesAddress));
		}
	    if(StringUtils.isNotBlank(likeHousesAddress)){
			criterias.add(Restrictions.allLike("housesAddress", likeHousesAddress));
		}
		 if(StringUtils.isNotBlank(inHousesAddresss)){
			criterias.add(Restrictions.in("housesAddress", StringUtils.split(inHousesAddresss, ",")));
		}
	    if(StringUtils.isNotBlank(equalHousesBuildingYear)){
			criterias.add(Restrictions.eq("housesBuildingYear", equalHousesBuildingYear));
		}
	    if(StringUtils.isNotBlank(likeHousesBuildingYear)){
			criterias.add(Restrictions.allLike("housesBuildingYear", likeHousesBuildingYear));
		}
		 if(StringUtils.isNotBlank(inHousesBuildingYears)){
			criterias.add(Restrictions.in("housesBuildingYear", StringUtils.split(inHousesBuildingYears, ",")));
		}
	    if(StringUtils.isNotBlank(equalHousesType)){
			criterias.add(Restrictions.eq("housesType", equalHousesType));
		}
	    if(StringUtils.isNotBlank(likeHousesType)){
			criterias.add(Restrictions.allLike("housesType", likeHousesType));
		}
		 if(StringUtils.isNotBlank(inHousesTypes)){
			criterias.add(Restrictions.in("housesType", StringUtils.split(inHousesTypes, ",")));
		}
	    if(StringUtils.isNotBlank(equalHousesPrice)){
			criterias.add(Restrictions.eq("housesPrice", equalHousesPrice));
		}
	    if(StringUtils.isNotBlank(likeHousesPrice)){
			criterias.add(Restrictions.allLike("housesPrice", likeHousesPrice));
		}
		 if(StringUtils.isNotBlank(inHousesPrices)){
			criterias.add(Restrictions.in("housesPrice", StringUtils.split(inHousesPrices, ",")));
		}
	    if(StringUtils.isNotBlank(equalYearOfPropertyRights)){
			criterias.add(Restrictions.eq("yearOfPropertyRights", equalYearOfPropertyRights));
		}
	    if(StringUtils.isNotBlank(likeYearOfPropertyRights)){
			criterias.add(Restrictions.allLike("yearOfPropertyRights", likeYearOfPropertyRights));
		}
		 if(StringUtils.isNotBlank(inYearOfPropertyRightss)){
			criterias.add(Restrictions.in("yearOfPropertyRights", StringUtils.split(inYearOfPropertyRightss, ",")));
		}
	    if(StringUtils.isNotBlank(equalInformationSources)){
			criterias.add(Restrictions.eq("informationSources", equalInformationSources));
		}
	    if(StringUtils.isNotBlank(likeInformationSources)){
			criterias.add(Restrictions.allLike("informationSources", likeInformationSources));
		}
		 if(StringUtils.isNotBlank(inInformationSourcess)){
			criterias.add(Restrictions.in("informationSources", StringUtils.split(inInformationSourcess, ",")));
		}
	    if(StringUtils.isNotBlank(equalInfoUpdateTime)){
			criterias.add(Restrictions.eq("infoUpdateTime", equalInfoUpdateTime));
		}
	    if(StringUtils.isNotBlank(likeInfoUpdateTime)){
			criterias.add(Restrictions.allLike("infoUpdateTime", likeInfoUpdateTime));
		}
		 if(StringUtils.isNotBlank(inInfoUpdateTimes)){
			criterias.add(Restrictions.in("infoUpdateTime", StringUtils.split(inInfoUpdateTimes, ",")));
		}
	    if(StringUtils.isNotBlank(equalLatitude)){
			criterias.add(Restrictions.eq("latitude", equalLatitude));
		}
	    if(StringUtils.isNotBlank(likeLatitude)){
			criterias.add(Restrictions.allLike("latitude", likeLatitude));
		}
		 if(StringUtils.isNotBlank(inLatitudes)){
			criterias.add(Restrictions.in("latitude", StringUtils.split(inLatitudes, ",")));
		}
	    if(StringUtils.isNotBlank(equalLongitude)){
			criterias.add(Restrictions.eq("longitude", equalLongitude));
		}
	    if(StringUtils.isNotBlank(likeLongitude)){
			criterias.add(Restrictions.allLike("longitude", likeLongitude));
		}
		 if(StringUtils.isNotBlank(inLongitudes)){
			criterias.add(Restrictions.in("longitude", StringUtils.split(inLongitudes, ",")));
		}
	    if(StringUtils.isNotBlank(equalDevelopers)){
			criterias.add(Restrictions.eq("developers", equalDevelopers));
		}
	    if(StringUtils.isNotBlank(likeDevelopers)){
			criterias.add(Restrictions.allLike("developers", likeDevelopers));
		}
		 if(StringUtils.isNotBlank(inDeveloperss)){
			criterias.add(Restrictions.in("developers", StringUtils.split(inDeveloperss, ",")));
		}
	    if(StringUtils.isNotBlank(equalAfforestationRate)){
			criterias.add(Restrictions.eq("afforestationRate", equalAfforestationRate));
		}
	    if(StringUtils.isNotBlank(likeAfforestationRate)){
			criterias.add(Restrictions.allLike("afforestationRate", likeAfforestationRate));
		}
		 if(StringUtils.isNotBlank(inAfforestationRates)){
			criterias.add(Restrictions.in("afforestationRate", StringUtils.split(inAfforestationRates, ",")));
		}
	    if(StringUtils.isNotBlank(equalPlotRatio)){
			criterias.add(Restrictions.eq("plotRatio", equalPlotRatio));
		}
	    if(StringUtils.isNotBlank(likePlotRatio)){
			criterias.add(Restrictions.allLike("plotRatio", likePlotRatio));
		}
		 if(StringUtils.isNotBlank(inPlotRatios)){
			criterias.add(Restrictions.in("plotRatio", StringUtils.split(inPlotRatios, ",")));
		}
	    if(StringUtils.isNotBlank(equalBuildingType)){
			criterias.add(Restrictions.eq("buildingType", equalBuildingType));
		}
	    if(StringUtils.isNotBlank(likeBuildingType)){
			criterias.add(Restrictions.allLike("buildingType", likeBuildingType));
		}
		 if(StringUtils.isNotBlank(inBuildingTypes)){
			criterias.add(Restrictions.in("buildingType", StringUtils.split(inBuildingTypes, ",")));
		}
	    if(StringUtils.isNotBlank(equalPropertyCompany)){
			criterias.add(Restrictions.eq("propertyCompany", equalPropertyCompany));
		}
	    if(StringUtils.isNotBlank(likePropertyCompany)){
			criterias.add(Restrictions.allLike("propertyCompany", likePropertyCompany));
		}
		 if(StringUtils.isNotBlank(inPropertyCompanys)){
			criterias.add(Restrictions.in("propertyCompany", StringUtils.split(inPropertyCompanys, ",")));
		}
	    if(StringUtils.isNotBlank(equalTotalHouseholds)){
			criterias.add(Restrictions.eq("totalHouseholds", equalTotalHouseholds));
		}
	    if(StringUtils.isNotBlank(likeTotalHouseholds)){
			criterias.add(Restrictions.allLike("totalHouseholds", likeTotalHouseholds));
		}
		 if(StringUtils.isNotBlank(inTotalHouseholdss)){
			criterias.add(Restrictions.in("totalHouseholds", StringUtils.split(inTotalHouseholdss, ",")));
		}
	    if(StringUtils.isNotBlank(equalPropertyCosts)){
			criterias.add(Restrictions.eq("propertyCosts", equalPropertyCosts));
		}
	    if(StringUtils.isNotBlank(likePropertyCosts)){
			criterias.add(Restrictions.allLike("propertyCosts", likePropertyCosts));
		}
		 if(StringUtils.isNotBlank(inPropertyCostss)){
			criterias.add(Restrictions.in("propertyCosts", StringUtils.split(inPropertyCostss, ",")));
		}
	    if(StringUtils.isNotBlank(equalTraffic)){
			criterias.add(Restrictions.eq("traffic", equalTraffic));
		}
	    if(StringUtils.isNotBlank(likeTraffic)){
			criterias.add(Restrictions.allLike("traffic", likeTraffic));
		}
		 if(StringUtils.isNotBlank(inTraffics)){
			criterias.add(Restrictions.in("traffic", StringUtils.split(inTraffics, ",")));
		}
	    if(StringUtils.isNotBlank(equalKindergarten)){
			criterias.add(Restrictions.eq("kindergarten", equalKindergarten));
		}
	    if(StringUtils.isNotBlank(likeKindergarten)){
			criterias.add(Restrictions.allLike("kindergarten", likeKindergarten));
		}
		 if(StringUtils.isNotBlank(inKindergartens)){
			criterias.add(Restrictions.in("kindergarten", StringUtils.split(inKindergartens, ",")));
		}
	    if(StringUtils.isNotBlank(equalSchool)){
			criterias.add(Restrictions.eq("school", equalSchool));
		}
	    if(StringUtils.isNotBlank(likeSchool)){
			criterias.add(Restrictions.allLike("school", likeSchool));
		}
		 if(StringUtils.isNotBlank(inSchools)){
			criterias.add(Restrictions.in("school", StringUtils.split(inSchools, ",")));
		}
	    if(StringUtils.isNotBlank(equalPowerCenter)){
			criterias.add(Restrictions.eq("powerCenter", equalPowerCenter));
		}
	    if(StringUtils.isNotBlank(likePowerCenter)){
			criterias.add(Restrictions.allLike("powerCenter", likePowerCenter));
		}
		 if(StringUtils.isNotBlank(inPowerCenters)){
			criterias.add(Restrictions.in("powerCenter", StringUtils.split(inPowerCenters, ",")));
		}
	    if(StringUtils.isNotBlank(equalHospital)){
			criterias.add(Restrictions.eq("hospital", equalHospital));
		}
	    if(StringUtils.isNotBlank(likeHospital)){
			criterias.add(Restrictions.allLike("hospital", likeHospital));
		}
		 if(StringUtils.isNotBlank(inHospitals)){
			criterias.add(Restrictions.in("hospital", StringUtils.split(inHospitals, ",")));
		}
	    if(StringUtils.isNotBlank(equalBank)){
			criterias.add(Restrictions.eq("bank", equalBank));
		}
	    if(StringUtils.isNotBlank(likeBank)){
			criterias.add(Restrictions.allLike("bank", likeBank));
		}
		 if(StringUtils.isNotBlank(inBanks)){
			criterias.add(Restrictions.in("bank", StringUtils.split(inBanks, ",")));
		}
	    if(StringUtils.isNotBlank(equalPost)){
			criterias.add(Restrictions.eq("post", equalPost));
		}
	    if(StringUtils.isNotBlank(likePost)){
			criterias.add(Restrictions.allLike("post", likePost));
		}
		 if(StringUtils.isNotBlank(inPosts)){
			criterias.add(Restrictions.in("post", StringUtils.split(inPosts, ",")));
		}
	    if(StringUtils.isNotBlank(equalOther)){
			criterias.add(Restrictions.eq("other", equalOther));
		}
	    if(StringUtils.isNotBlank(likeOther)){
			criterias.add(Restrictions.allLike("other", likeOther));
		}
		 if(StringUtils.isNotBlank(inOthers)){
			criterias.add(Restrictions.in("other", StringUtils.split(inOthers, ",")));
		}
	    if(StringUtils.isNotBlank(equalFloorArea)){
			criterias.add(Restrictions.eq("floorArea", equalFloorArea));
		}
	    if(StringUtils.isNotBlank(likeFloorArea)){
			criterias.add(Restrictions.allLike("floorArea", likeFloorArea));
		}
		 if(StringUtils.isNotBlank(inFloorAreas)){
			criterias.add(Restrictions.in("floorArea", StringUtils.split(inFloorAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalBuildingArea)){
			criterias.add(Restrictions.eq("buildingArea", equalBuildingArea));
		}
	    if(StringUtils.isNotBlank(likeBuildingArea)){
			criterias.add(Restrictions.allLike("buildingArea", likeBuildingArea));
		}
		 if(StringUtils.isNotBlank(inBuildingAreas)){
			criterias.add(Restrictions.in("buildingArea", StringUtils.split(inBuildingAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalUrl)){
			criterias.add(Restrictions.eq("url", equalUrl));
		}
	    if(StringUtils.isNotBlank(likeUrl)){
			criterias.add(Restrictions.allLike("url", likeUrl));
		}
		 if(StringUtils.isNotBlank(inUrls)){
			criterias.add(Restrictions.in("url", StringUtils.split(inUrls, ",")));
		}
		 if(StringUtils.isNotBlank(gtHouseBuildingYear)){
			 criterias.add(Restrictions.gt("housesBuildingYear", gtHouseBuildingYear));
		 }
	}
}