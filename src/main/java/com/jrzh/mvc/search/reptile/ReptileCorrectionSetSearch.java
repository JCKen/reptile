package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileCorrectionSetSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 竞品id
     */
    private String equalHouseId;
    
    /**
     * Like 竞品id
     */
    private String likeHouseId;
    
     /**
     * In 竞品id 英文逗号分隔
     */
    private String inHouseIds;
    
    /**
     * Equal 设置人
     */
    private String equalUserId;
    
    /**
     * Like 设置人
     */
    private String likeUserId;
    
     /**
     * In 设置人 英文逗号分隔
     */
    private String inUserIds;
    
    /**
     * Equal 修正项id
     */
    private String equalCorrectionId;
    
    /**
     * Like 修正项id
     */
    private String likeCorrectionId;
    
     /**
     * In 修正项id 英文逗号分隔
     */
    private String inCorrectionIds;
    
    /**
     * Equal 修正值
     */
    private String equalCorrectionValue;
    
    /**
     * Like 修正值
     */
    private String likeCorrectionValue;
    
     /**
     * In 修正值 英文逗号分隔
     */
    private String inCorrectionValues;
    

    public void setEqualHouseId(String equalHouseId) {
        this.equalHouseId = equalHouseId;
    }
    
    public String getEqualHouseId() {
        return this.equalHouseId;
    }
    
    public void setLikeHouseId(String likeHouseId) {
        this.likeHouseId = likeHouseId;
    }
    
    public String getLikeHouseId() {
        return this.likeHouseId;
    }
    
    public void setInHouseIds(String inHouseIds) {
        this.inHouseIds = inHouseIds;
    }
    
    public String getInHouseIds() {
        return this.inHouseIds;
    }
    public void setEqualUserId(String equalUserId) {
        this.equalUserId = equalUserId;
    }
    
    public String getEqualUserId() {
        return this.equalUserId;
    }
    
    public void setLikeUserId(String likeUserId) {
        this.likeUserId = likeUserId;
    }
    
    public String getLikeUserId() {
        return this.likeUserId;
    }
    
    public void setInUserIds(String inUserIds) {
        this.inUserIds = inUserIds;
    }
    
    public String getInUserIds() {
        return this.inUserIds;
    }
    public void setEqualCorrectionId(String equalCorrectionId) {
        this.equalCorrectionId = equalCorrectionId;
    }
    
    public String getEqualCorrectionId() {
        return this.equalCorrectionId;
    }
    
    public void setLikeCorrectionId(String likeCorrectionId) {
        this.likeCorrectionId = likeCorrectionId;
    }
    
    public String getLikeCorrectionId() {
        return this.likeCorrectionId;
    }
    
    public void setInCorrectionIds(String inCorrectionIds) {
        this.inCorrectionIds = inCorrectionIds;
    }
    
    public String getInCorrectionIds() {
        return this.inCorrectionIds;
    }
    public void setEqualCorrectionValue(String equalCorrectionValue) {
        this.equalCorrectionValue = equalCorrectionValue;
    }
    
    public String getEqualCorrectionValue() {
        return this.equalCorrectionValue;
    }
    
    public void setLikeCorrectionValue(String likeCorrectionValue) {
        this.likeCorrectionValue = likeCorrectionValue;
    }
    
    public String getLikeCorrectionValue() {
        return this.likeCorrectionValue;
    }
    
    public void setInCorrectionValues(String inCorrectionValues) {
        this.inCorrectionValues = inCorrectionValues;
    }
    
    public String getInCorrectionValues() {
        return this.inCorrectionValues;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalHouseId)){
			criterias.add(Restrictions.eq("houseId", equalHouseId));
		}
	    if(StringUtils.isNotBlank(likeHouseId)){
			criterias.add(Restrictions.allLike("houseId", likeHouseId));
		}
		 if(StringUtils.isNotBlank(inHouseIds)){
			criterias.add(Restrictions.in("houseId", StringUtils.split(inHouseIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalUserId)){
			criterias.add(Restrictions.eq("userId", equalUserId));
		}
	    if(StringUtils.isNotBlank(likeUserId)){
			criterias.add(Restrictions.allLike("userId", likeUserId));
		}
		 if(StringUtils.isNotBlank(inUserIds)){
			criterias.add(Restrictions.in("userId", StringUtils.split(inUserIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalCorrectionId)){
			criterias.add(Restrictions.eq("correctionId", equalCorrectionId));
		}
	    if(StringUtils.isNotBlank(likeCorrectionId)){
			criterias.add(Restrictions.allLike("correctionId", likeCorrectionId));
		}
		 if(StringUtils.isNotBlank(inCorrectionIds)){
			criterias.add(Restrictions.in("correctionId", StringUtils.split(inCorrectionIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalCorrectionValue)){
			criterias.add(Restrictions.eq("correctionValue", equalCorrectionValue));
		}
	    if(StringUtils.isNotBlank(likeCorrectionValue)){
			criterias.add(Restrictions.allLike("correctionValue", likeCorrectionValue));
		}
		 if(StringUtils.isNotBlank(inCorrectionValues)){
			criterias.add(Restrictions.in("correctionValue", StringUtils.split(inCorrectionValues, ",")));
		}
	}
}