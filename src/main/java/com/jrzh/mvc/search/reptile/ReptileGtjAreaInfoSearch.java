package com.jrzh.mvc.search.reptile;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;

import com.jrzh.framework.base.search.BaseSearch;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;

public class ReptileGtjAreaInfoSearch extends BaseSearch {
	private static final long serialVersionUID = 1L;

	/**
	 * Equal 地块编号
	 */
	private String equalAreaNo;

	/**
	 * Like 地块编号
	 */
	private String likeAreaNo;

	/**
	 * In 地块编号 英文逗号分隔
	 */
	private String inAreaNos;

	/**
	 * Equal 地块位置
	 */
	private String equalAddress;

	/**
	 * not null 价格
	 */
	private String notNullPrice;

	/**
	 * Like 地块位置
	 */
	private String likeAddress;

	/**
	 * In 地块位置 英文逗号分隔
	 */
	private String inAddresss;

	/**
	 * Equal 地块用途
	 */
	private String equalUse;

	/**
	 * Like 地块用途
	 */
	private String likeUse;

	/**
	 * In 地块用途 英文逗号分隔
	 */
	private String inUses;

	/**
	 * Equal 地块面积
	 */
	private String equalAcreage;

	/**
	 * Like 地块面积
	 */
	private String likeAcreage;

	/**
	 * In 地块面积 英文逗号分隔
	 */
	private String inAcreages;

	/**
	 * Equal 成交价
	 */
	private String equalPrice;

	/**
	 * Like 成交价
	 */
	private String likePrice;

	/**
	 * In 成交价 英文逗号分隔
	 */
	private String inPrices;

	/**
	 * Equal 竞得人
	 */
	private String equalBuyer;

	/**
	 * Like 竞得人
	 */
	private String likeBuyer;

	/**
	 * In 竞得人 英文逗号分隔
	 */
	private String inBuyers;

	/**
	 * is not Buyer
	 */
	private String isNotBuyer;

	/**
	 * Equal 城市
	 */
	private String equalCity;

	/**
	 * Like 城市
	 */
	private String likeCity;

	/**
	 * In 城市
	 */
	private String inCity;

	/**
	 * 纬度
	 */
	private String lat;
	/**
	 * 经度
	 */
	private String lnt;
	
	private String equalLnt;
	private String equalLat;
	/**
	 * 成交时间
	 */
	private String likeTime;
	private String gtTime;
	private String ltTime;

	public String getGtTime() {
		return gtTime;
	}

	public void setGtTime(String gtTime) {
		this.gtTime = gtTime;
	}

	public String getLtTime() {
		return ltTime;
	}

	public void setLtTime(String ltTime) {
		this.ltTime = ltTime;
	}

	public String getLikeTime() {
		return likeTime;
	}

	public void setLikeTime(String likeTime) {
		this.likeTime = likeTime;
	}

	public String getEqualLnt() {
		return equalLnt;
	}

	public void setEqualLnt(String equalLnt) {
		this.equalLnt = equalLnt;
	}

	public String getEqualLat() {
		return equalLat;
	}

	public void setEqualLat(String equalLat) {
		this.equalLat = equalLat;
	}

	

	public String getIsNotBuyer() {
		return isNotBuyer;
	}

	public void setIsNotBuyer(String isNotBuyer) {
		this.isNotBuyer = isNotBuyer;
	}

	public String getNotNullPrice() {
		return notNullPrice;
	}

	public void setNotNullPrice(String notNullPrice) {
		this.notNullPrice = notNullPrice;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLnt() {
		return lnt;
	}

	public void setLnt(String lnt) {
		this.lnt = lnt;
	}

	public void setEqualAreaNo(String equalAreaNo) {
		this.equalAreaNo = equalAreaNo;
	}

	public String getEqualAreaNo() {
		return this.equalAreaNo;
	}

	public void setLikeAreaNo(String likeAreaNo) {
		this.likeAreaNo = likeAreaNo;
	}

	public String getLikeAreaNo() {
		return this.likeAreaNo;
	}

	public void setInAreaNos(String inAreaNos) {
		this.inAreaNos = inAreaNos;
	}

	public String getInAreaNos() {
		return this.inAreaNos;
	}

	public void setEqualAddress(String equalAddress) {
		this.equalAddress = equalAddress;
	}

	public String getEqualAddress() {
		return this.equalAddress;
	}

	public void setLikeAddress(String likeAddress) {
		this.likeAddress = likeAddress;
	}

	public String getLikeAddress() {
		return this.likeAddress;
	}

	public void setInAddresss(String inAddresss) {
		this.inAddresss = inAddresss;
	}

	public String getInAddresss() {
		return this.inAddresss;
	}

	public void setEqualUse(String equalUse) {
		this.equalUse = equalUse;
	}

	public String getEqualUse() {
		return this.equalUse;
	}

	public void setLikeUse(String likeUse) {
		this.likeUse = likeUse;
	}

	public String getLikeUse() {
		return this.likeUse;
	}

	public void setInUses(String inUses) {
		this.inUses = inUses;
	}

	public String getInUses() {
		return this.inUses;
	}

	public void setEqualAcreage(String equalAcreage) {
		this.equalAcreage = equalAcreage;
	}

	public String getEqualAcreage() {
		return this.equalAcreage;
	}

	public void setLikeAcreage(String likeAcreage) {
		this.likeAcreage = likeAcreage;
	}

	public String getLikeAcreage() {
		return this.likeAcreage;
	}

	public void setInAcreages(String inAcreages) {
		this.inAcreages = inAcreages;
	}

	public String getInAcreages() {
		return this.inAcreages;
	}

	public void setEqualPrice(String equalPrice) {
		this.equalPrice = equalPrice;
	}

	public String getEqualPrice() {
		return this.equalPrice;
	}

	public void setLikePrice(String likePrice) {
		this.likePrice = likePrice;
	}

	public String getLikePrice() {
		return this.likePrice;
	}

	public void setInPrices(String inPrices) {
		this.inPrices = inPrices;
	}

	public String getInPrices() {
		return this.inPrices;
	}

	public void setEqualBuyer(String equalBuyer) {
		this.equalBuyer = equalBuyer;
	}

	public String getEqualBuyer() {
		return this.equalBuyer;
	}

	public void setLikeBuyer(String likeBuyer) {
		this.likeBuyer = likeBuyer;
	}

	public String getLikeBuyer() {
		return this.likeBuyer;
	}

	public void setInBuyers(String inBuyers) {
		this.inBuyers = inBuyers;
	}

	public String getInBuyers() {
		return this.inBuyers;
	}

	public String getEqualCity() {
		return equalCity;
	}

	public void setEqualCity(String equalCity) {
		this.equalCity = equalCity;
	}

	public String getLikeCity() {
		return likeCity;
	}

	public void setLikeCity(String likeCity) {
		this.likeCity = likeCity;
	}

	public String getInCity() {
		return inCity;
	}

	public void setInCity(String inCity) {
		this.inCity = inCity;
	}

	@Override
	public void setCriterias(Criteria<?> criterias) {
		if(likeTime!=null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				criterias.add(Restrictions.gte("time",sdf.parse(gtTime)));
				criterias.add(Restrictions.lt("time",sdf.parseObject(ltTime)));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if(StringUtils.isNotBlank(equalLat)) {
			criterias.add(Restrictions.eq("lat", equalLat));
		}
		if(StringUtils.isNotBlank(equalLnt)) {
			criterias.add(Restrictions.eq("lnt", equalLnt));
		}
		if (StringUtils.isNotBlank(isNotBuyer)) {
			criterias.add(Restrictions.isNotNull(isNotBuyer));
		}

		if (StringUtils.isNotBlank(lat)) {
			criterias.add(Restrictions.isNull(lat));
		}
		if (StringUtils.isNotBlank(lnt)) {
			criterias.add(Restrictions.isNull(lnt));
		}

		if (StringUtils.isNotBlank(equalAreaNo)) {
			criterias.add(Restrictions.eq("areaNo", equalAreaNo));
		}
		if (StringUtils.isNotBlank(likeAreaNo)) {
			criterias.add(Restrictions.allLike("areaNo", likeAreaNo));
		}
		if (StringUtils.isNotBlank(inAreaNos)) {
			criterias.add(Restrictions.in("areaNo", StringUtils.split(inAreaNos, ",")));
		}
		if (StringUtils.isNotBlank(equalAddress)) {
			criterias.add(Restrictions.eq("address", equalAddress));
		}
		if (StringUtils.isNotBlank(likeAddress)) {
			criterias.add(Restrictions.allLike("address", likeAddress));
		}
		if (StringUtils.isNotBlank(inAddresss)) {
			criterias.add(Restrictions.in("address", StringUtils.split(inAddresss, ",")));
		}
		if (StringUtils.isNotBlank(equalUse)) {
			criterias.add(Restrictions.eq("use", equalUse));
		}
		if (StringUtils.isNotBlank(likeUse)) {
			criterias.add(Restrictions.allLike("use", likeUse));
		}
		if (StringUtils.isNotBlank(inUses)) {
			criterias.add(Restrictions.in("use", StringUtils.split(inUses, ",")));
		}
		if (StringUtils.isNotBlank(equalAcreage)) {
			criterias.add(Restrictions.eq("acreage", equalAcreage));
		}
		if (StringUtils.isNotBlank(likeAcreage)) {
			criterias.add(Restrictions.allLike("acreage", likeAcreage));
		}
		if (StringUtils.isNotBlank(inAcreages)) {
			criterias.add(Restrictions.in("acreage", StringUtils.split(inAcreages, ",")));
		}
		if (StringUtils.isNotBlank(equalPrice)) {
			criterias.add(Restrictions.eq("price", equalPrice));
		}
		if (StringUtils.isNotBlank(likePrice)) {
			criterias.add(Restrictions.allLike("price", likePrice));
		}
		if (StringUtils.isNotBlank(inPrices)) {
			criterias.add(Restrictions.in("price", StringUtils.split(inPrices, ",")));
		}
		if (StringUtils.isNotBlank(equalBuyer)) {
			criterias.add(Restrictions.eq("buyer", equalBuyer));
		}
		if (StringUtils.isNotBlank(likeBuyer)) {
			criterias.add(Restrictions.allLike("buyer", likeBuyer));
		}
		if (StringUtils.isNotBlank(inBuyers)) {
			criterias.add(Restrictions.in("buyer", StringUtils.split(inBuyers, ",")));
		}

		if (StringUtils.isNotBlank(equalCity)) {
			criterias.add(Restrictions.eq("city", equalCity));
		}
		if (StringUtils.isNotBlank(likeCity)) {
			criterias.add(Restrictions.allLike("city", likeCity));
		}
		if (StringUtils.isNotBlank(inCity)) {
			criterias.add(Restrictions.in("city", StringUtils.split(inCity, ",")));
		}

		if (StringUtils.isNotBlank(notNullPrice)) {
			criterias.add(Restrictions.isNotNull("price"));
		}
	}
}