package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileTemplateSumInfoSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 用户ID
     */
    private String equalUserId;
    
    /**
     * Like 用户ID
     */
    private String likeUserId;
    
     /**
     * In 用户ID 英文逗号分隔
     */
    private String inUserIds;
    
    /**
     * Equal 模板ID
     */
    private String equalTemplateId;
    
    /**
     * Like 模板ID
     */
    private String likeTemplateId;
    
     /**
     * In 模板ID 英文逗号分隔
     */
    private String inTemplateIds;
    
    /**
     * Equal 一手房占比
     */
    private String equalOneHouseRatio;
    
    /**
     * Like 一手房占比
     */
    private String likeOneHouseRatio;
    
     /**
     * In 一手房占比 英文逗号分隔
     */
    private String inOneHouseRatios;
    
    /**
     * Equal 二手房占比
     */
    private String equalTwoHouseRatio;
    
    /**
     * Like 二手房占比
     */
    private String likeTwoHouseRatio;
    
     /**
     * In 二手房占比 英文逗号分隔
     */
    private String inTwoHouseRatios;
    
    /**
     * Equal 土地占比
     */
    private String equalLandRatio;
    
    /**
     * Like 土地占比
     */
    private String likeLandRatio;
    
     /**
     * In 土地占比 英文逗号分隔
     */
    private String inLandRatios;
    
    /**
     * Equal 参考金额
     */
    private String equalReferAmt;
    
    /**
     * Like 参考金额
     */
    private String likeReferAmt;
    
     /**
     * In 参考金额 英文逗号分隔
     */
    private String inReferAmts;
    

    public void setEqualUserId(String equalUserId) {
        this.equalUserId = equalUserId;
    }
    
    public String getEqualUserId() {
        return this.equalUserId;
    }
    
    public void setLikeUserId(String likeUserId) {
        this.likeUserId = likeUserId;
    }
    
    public String getLikeUserId() {
        return this.likeUserId;
    }
    
    public void setInUserIds(String inUserIds) {
        this.inUserIds = inUserIds;
    }
    
    public String getInUserIds() {
        return this.inUserIds;
    }
    public void setEqualTemplateId(String equalTemplateId) {
        this.equalTemplateId = equalTemplateId;
    }
    
    public String getEqualTemplateId() {
        return this.equalTemplateId;
    }
    
    public void setLikeTemplateId(String likeTemplateId) {
        this.likeTemplateId = likeTemplateId;
    }
    
    public String getLikeTemplateId() {
        return this.likeTemplateId;
    }
    
    public void setInTemplateIds(String inTemplateIds) {
        this.inTemplateIds = inTemplateIds;
    }
    
    public String getInTemplateIds() {
        return this.inTemplateIds;
    }
    public void setEqualOneHouseRatio(String equalOneHouseRatio) {
        this.equalOneHouseRatio = equalOneHouseRatio;
    }
    
    public String getEqualOneHouseRatio() {
        return this.equalOneHouseRatio;
    }
    
    public void setLikeOneHouseRatio(String likeOneHouseRatio) {
        this.likeOneHouseRatio = likeOneHouseRatio;
    }
    
    public String getLikeOneHouseRatio() {
        return this.likeOneHouseRatio;
    }
    
    public void setInOneHouseRatios(String inOneHouseRatios) {
        this.inOneHouseRatios = inOneHouseRatios;
    }
    
    public String getInOneHouseRatios() {
        return this.inOneHouseRatios;
    }
    public void setEqualTwoHouseRatio(String equalTwoHouseRatio) {
        this.equalTwoHouseRatio = equalTwoHouseRatio;
    }
    
    public String getEqualTwoHouseRatio() {
        return this.equalTwoHouseRatio;
    }
    
    public void setLikeTwoHouseRatio(String likeTwoHouseRatio) {
        this.likeTwoHouseRatio = likeTwoHouseRatio;
    }
    
    public String getLikeTwoHouseRatio() {
        return this.likeTwoHouseRatio;
    }
    
    public void setInTwoHouseRatios(String inTwoHouseRatios) {
        this.inTwoHouseRatios = inTwoHouseRatios;
    }
    
    public String getInTwoHouseRatios() {
        return this.inTwoHouseRatios;
    }
    public void setEqualLandRatio(String equalLandRatio) {
        this.equalLandRatio = equalLandRatio;
    }
    
    public String getEqualLandRatio() {
        return this.equalLandRatio;
    }
    
    public void setLikeLandRatio(String likeLandRatio) {
        this.likeLandRatio = likeLandRatio;
    }
    
    public String getLikeLandRatio() {
        return this.likeLandRatio;
    }
    
    public void setInLandRatios(String inLandRatios) {
        this.inLandRatios = inLandRatios;
    }
    
    public String getInLandRatios() {
        return this.inLandRatios;
    }
    public void setEqualReferAmt(String equalReferAmt) {
        this.equalReferAmt = equalReferAmt;
    }
    
    public String getEqualReferAmt() {
        return this.equalReferAmt;
    }
    
    public void setLikeReferAmt(String likeReferAmt) {
        this.likeReferAmt = likeReferAmt;
    }
    
    public String getLikeReferAmt() {
        return this.likeReferAmt;
    }
    
    public void setInReferAmts(String inReferAmts) {
        this.inReferAmts = inReferAmts;
    }
    
    public String getInReferAmts() {
        return this.inReferAmts;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalUserId)){
			criterias.add(Restrictions.eq("userId", equalUserId));
		}
	    if(StringUtils.isNotBlank(likeUserId)){
			criterias.add(Restrictions.allLike("userId", likeUserId));
		}
		 if(StringUtils.isNotBlank(inUserIds)){
			criterias.add(Restrictions.in("userId", StringUtils.split(inUserIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalTemplateId)){
			criterias.add(Restrictions.eq("templateId", equalTemplateId));
		}
	    if(StringUtils.isNotBlank(likeTemplateId)){
			criterias.add(Restrictions.allLike("templateId", likeTemplateId));
		}
		 if(StringUtils.isNotBlank(inTemplateIds)){
			criterias.add(Restrictions.in("templateId", StringUtils.split(inTemplateIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalOneHouseRatio)){
			criterias.add(Restrictions.eq("oneHouseRatio", equalOneHouseRatio));
		}
	    if(StringUtils.isNotBlank(likeOneHouseRatio)){
			criterias.add(Restrictions.allLike("oneHouseRatio", likeOneHouseRatio));
		}
		 if(StringUtils.isNotBlank(inOneHouseRatios)){
			criterias.add(Restrictions.in("oneHouseRatio", StringUtils.split(inOneHouseRatios, ",")));
		}
	    if(StringUtils.isNotBlank(equalTwoHouseRatio)){
			criterias.add(Restrictions.eq("twoHouseRatio", equalTwoHouseRatio));
		}
	    if(StringUtils.isNotBlank(likeTwoHouseRatio)){
			criterias.add(Restrictions.allLike("twoHouseRatio", likeTwoHouseRatio));
		}
		 if(StringUtils.isNotBlank(inTwoHouseRatios)){
			criterias.add(Restrictions.in("twoHouseRatio", StringUtils.split(inTwoHouseRatios, ",")));
		}
	    if(StringUtils.isNotBlank(equalLandRatio)){
			criterias.add(Restrictions.eq("landRatio", equalLandRatio));
		}
	    if(StringUtils.isNotBlank(likeLandRatio)){
			criterias.add(Restrictions.allLike("landRatio", likeLandRatio));
		}
		 if(StringUtils.isNotBlank(inLandRatios)){
			criterias.add(Restrictions.in("landRatio", StringUtils.split(inLandRatios, ",")));
		}
	    if(StringUtils.isNotBlank(equalReferAmt)){
			criterias.add(Restrictions.eq("referAmt", equalReferAmt));
		}
	    if(StringUtils.isNotBlank(likeReferAmt)){
			criterias.add(Restrictions.allLike("referAmt", likeReferAmt));
		}
		 if(StringUtils.isNotBlank(inReferAmts)){
			criterias.add(Restrictions.in("referAmt", StringUtils.split(inReferAmts, ",")));
		}
	}
}