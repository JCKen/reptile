package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileEstateZbSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 指标名称
     */
    private String equalZb;
    
    private String notInMenu;
    
    /**
     * Like 指标名称
     */
    private String likeZb;
    
     /**
     * In 指标名称 英文逗号分隔
     */
    private String inZbs;
    
    /**
     * Equal 菜单
     */
    private String equalMenu;
    
    /**
     * Equal 单位
     */
    private String equalUnit;
    
    /**
     * Like 单位
     */
    private String likeUnit;
    
     /**
     * In 单位 英文逗号分隔
     */
    private String inUnits;
    
    /**
     * Equal 指标编号
     */
    private String equalCode;
    
    /**
     * Like 指标编号
     */
    private String likeCode;
    
     /**
     * In 指标编号 英文逗号分隔
     */
    private String inCodes;
    
    /**
     * 按分类进行分组
     */
    private String groupClassify;
    
    /**
     * 按菜单进行分组
     */
    private String groupMenu;
    
    /**
     * 查询某个分类的所有指标
     */
    private String eqClassify;
    
    /**
     * eq  城市
     */
    private String eqCity;
    
    public String getEqClassify() {
		return eqClassify;
	}

	public void setEqClassify(String eqClassify) {
		this.eqClassify = eqClassify;
	}

	public void setEqualZb(String equalZb) {
        this.equalZb = equalZb;
    }
    
    public String getEqualZb() {
        return this.equalZb;
    }
    
    public String getNotInMenu() {
		return notInMenu;
	}

	public void setNotInMenu(String notInMenu) {
		this.notInMenu = notInMenu;
	}

	public void setLikeZb(String likeZb) {
        this.likeZb = likeZb;
    }
    
    public String getLikeZb() {
        return this.likeZb;
    }
    
    public String getEqCity() {
		return eqCity;
	}

	public void setEqCity(String eqCity) {
		this.eqCity = eqCity;
	}

	public void setInZbs(String inZbs) {
        this.inZbs = inZbs;
    }
    
    public String getInZbs() {
        return this.inZbs;
    }
    public void setEqualUnit(String equalUnit) {
        this.equalUnit = equalUnit;
    }
    
    public String getEqualUnit() {
        return this.equalUnit;
    }
    
    public void setLikeUnit(String likeUnit) {
        this.likeUnit = likeUnit;
    }
    
    public String getLikeUnit() {
        return this.likeUnit;
    }
    
    public void setInUnits(String inUnits) {
        this.inUnits = inUnits;
    }
    
    public String getInUnits() {
        return this.inUnits;
    }
    public void setEqualCode(String equalCode) {
        this.equalCode = equalCode;
    }
    
    public String getEqualCode() {
        return this.equalCode;
    }
    
    public void setLikeCode(String likeCode) {
        this.likeCode = likeCode;
    }
    
    public String getLikeCode() {
        return this.likeCode;
    }
    
    public void setInCodes(String inCodes) {
        this.inCodes = inCodes;
    }
    
    public String getInCodes() {
        return this.inCodes;
    }

	public String getGroupClassify() {
		return groupClassify;
	}

	public void setGroupClassify(String groupClassify) {
		this.groupClassify = groupClassify;
	}

	
	public String getEqualMenu() {
		return equalMenu;
	}

	public void setEqualMenu(String equalMenu) {
		this.equalMenu = equalMenu;
	}
	
	public String getGroupMenu() {
		return groupMenu;
	}

	public void setGroupMenu(String groupMenu) {
		this.groupMenu = groupMenu;
	}

	@Override
	public void setCriterias(Criteria<?> criterias) {
		
		// 分组
		if(StringUtils.isNotBlank(groupClassify)){
			criterias.addGroup(groupClassify);
		}
		if(StringUtils.isNotBlank(groupMenu)){
			criterias.addGroup(groupMenu);
		}
		
		if(StringUtils.isNotBlank(equalMenu)){
			criterias.add((Restrictions.eq("menu", equalMenu)));
		}
		
		if(StringUtils.isNotBlank(eqClassify)){
			criterias.add((Restrictions.eq("classify", eqClassify)));
		}
		
	    if(StringUtils.isNotBlank(equalZb)){
			criterias.add(Restrictions.eq("zb", equalZb));
		}
	    if(StringUtils.isNotBlank(likeZb)){
			criterias.add(Restrictions.allLike("zb", likeZb));
		}
		 if(StringUtils.isNotBlank(inZbs)){
			criterias.add(Restrictions.in("zb", StringUtils.split(inZbs, ",")));
		}
	    if(StringUtils.isNotBlank(equalUnit)){
			criterias.add(Restrictions.eq("unit", equalUnit));
		}
	    if(StringUtils.isNotBlank(likeUnit)){
			criterias.add(Restrictions.allLike("unit", likeUnit));
		}
		 if(StringUtils.isNotBlank(inUnits)){
			criterias.add(Restrictions.in("unit", StringUtils.split(inUnits, ",")));
		}
	    if(StringUtils.isNotBlank(equalCode)){
			criterias.add(Restrictions.eq("code", equalCode));
		}
	    if(StringUtils.isNotBlank(likeCode)){
			criterias.add(Restrictions.allLike("code", likeCode));
		}
		 if(StringUtils.isNotBlank(inCodes)){
			criterias.add(Restrictions.in("code", StringUtils.split(inCodes, ",")));
		}
		String[] menus = {"房地产"};
		criterias.add(Restrictions.notIn("menu", menus));
		
		if(StringUtils.isNotBlank(eqCity)){
			criterias.add(Restrictions.eq("city", eqCity));
		}
	}
}