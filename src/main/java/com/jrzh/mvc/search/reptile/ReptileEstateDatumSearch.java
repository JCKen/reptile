package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;

import com.jrzh.framework.base.search.BaseSearch;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;

public class ReptileEstateDatumSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 时间
     */
    private String equalTime;
    
    /**
     * Like 时间
     */
    private String likeTime;
    
    /**
     * 大于等于指定时间
     */
    private String gtaTime;
    
    /**
     * 小于等于指定时间
     */
    private String ltaTime;
    
     /**
     * In 时间 英文逗号分隔
     */
    private String inTimes;
    
    /**
     * Equal 指标编号
     */
    private String equalCode;
    
    /**
     * Like 指标编号
     */
    private String likeCode;
    
     /**
     * In 指标编号 英文逗号分隔
     */
    private String inCodes;
    
    /**
     * Equal 指标值
     */
    private String equalData;
    
    /**
     * Like 指标值
     */
    private String likeData;
    
     /**
     * In 指标值 英文逗号分隔
     */
    private String inDatas;
    
    private String groupTime;
    
    
    public String getGroupTime() {
		return groupTime;
	}

	public void setGroupTime(String groupTime) {
		this.groupTime = groupTime;
	}

	public void setEqualTime(String equalTime) {
        this.equalTime = equalTime;
    }
    
    public String getEqualTime() {
        return this.equalTime;
    }
    
    public void setLikeTime(String likeTime) {
        this.likeTime = likeTime;
    }
    
    public String getLikeTime() {
        return this.likeTime;
    }
    
    public void setInTimes(String inTimes) {
        this.inTimes = inTimes;
    }
    
    public String getInTimes() {
        return this.inTimes;
    }
    public void setEqualCode(String equalCode) {
        this.equalCode = equalCode;
    }

	public String getGtaTime() {
		return gtaTime;
	}

	public void setGtaTime(String gtaTime) {
		this.gtaTime = gtaTime;
	}

	public String getLtaTime() {
		return ltaTime;
	}

	public void setLtaTime(String ltaTime) {
		this.ltaTime = ltaTime;
	}

	public String getEqualCode() {
        return this.equalCode;
    }
    
    public void setLikeCode(String likeCode) {
        this.likeCode = likeCode;
    }
    
    public String getLikeCode() {
        return this.likeCode;
    }
    
    public void setInCodes(String inCodes) {
        this.inCodes = inCodes;
    }
    
    public String getInCodes() {
        return this.inCodes;
    }
    public void setEqualData(String equalData) {
        this.equalData = equalData;
    }
    
    public String getEqualData() {
        return this.equalData;
    }
    
    public void setLikeData(String likeData) {
        this.likeData = likeData;
    }
    
    public String getLikeData() {
        return this.likeData;
    }
    
    public void setInDatas(String inDatas) {
        this.inDatas = inDatas;
    }
    
    public String getInDatas() {
        return this.inDatas;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
		
		if(StringUtils.isNotBlank(groupTime)){
			criterias.addGroup(groupTime);
		}
		
	    if(StringUtils.isNotBlank(equalTime)){
			criterias.add(Restrictions.eq("time", equalTime));
		}
	    if(StringUtils.isNotBlank(likeTime)){
			criterias.add(Restrictions.allLike("time", likeTime));
		}
		 if(StringUtils.isNotBlank(inTimes)){
			criterias.add(Restrictions.in("time", StringUtils.split(inTimes, ",")));
		}
	    if(StringUtils.isNotBlank(equalCode)){
			criterias.add(Restrictions.eq("code", equalCode));
		}
	    if(StringUtils.isNotBlank(likeCode)){
			criterias.add(Restrictions.allLike("code", likeCode));
		}
		 if(StringUtils.isNotBlank(inCodes)){
			criterias.add(Restrictions.in("code", StringUtils.split(inCodes, ",")));
		}
	    if(StringUtils.isNotBlank(equalData)){
			criterias.add(Restrictions.eq("data", equalData));
		}
	    if(StringUtils.isNotBlank(likeData)){
			criterias.add(Restrictions.allLike("data", likeData));
		}
		 if(StringUtils.isNotBlank(inDatas)){
			criterias.add(Restrictions.in("data", StringUtils.split(inDatas, ",")));
		}
		 if(StringUtils.isNotBlank(gtaTime)){
			 criterias.add(Restrictions.gte("time", gtaTime));
			 criterias.add(Restrictions.lte("time", ltaTime));
		 }
	}
}