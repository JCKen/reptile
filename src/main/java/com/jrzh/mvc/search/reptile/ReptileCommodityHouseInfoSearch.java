package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;

import com.jrzh.framework.base.search.BaseSearch;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;

public class ReptileCommodityHouseInfoSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 户型
     */
    private String equalHouseType;
    
    /**
     * Like 户型
     */
    private String likeHouseType;
    
     /**
     * In 户型 英文逗号分隔
     */
    private String inHouseTypes;
    
    /**
     * Equal 成交套数
     */
    private String equalTransactionNumber;
    
    /**
     * Like 成交套数
     */
    private String likeTransactionNumber;
    
     /**
     * In 成交套数 英文逗号分隔
     */
    private String inTransactionNumbers;
    
    /**
     * Equal 成交面积
     */
    private String equalTransactionArea;
    
    /**
     * Like 成交面积
     */
    private String likeTransactionArea;
    
     /**
     * In 成交面积 英文逗号分隔
     */
    private String inTransactionAreas;
    
    /**
     * Equal 城市
     */
    private String equalCity;
    
    /**
     * Like 城市
     */
    private String likeCity;
    
     /**
     * In 城市 英文逗号分隔
     */
    private String inCitys;
    
    /**
     * Equal 区域
     */
    private String equalPart;
    
    /**
     * Like 区域
     */
    private String likePart;
    
     /**
     * In 区域 英文逗号分隔
     */
    private String inParts;
    
    /**
     * Equal 成交均价
     */
    private String equalTransactionPrice;
    
    /**
     * Like 成交均价
     */
    private String likeTransactionPrice;
    
     /**
     * In 成交均价 英文逗号分隔
     */
    private String inTransactionPrices;
    
    /**
     * Equal 可售面积
     */
    private String equalSaleableArea;
    
    /**
     * Like 可售面积
     */
    private String likeSaleableArea;
    
     /**
     * In 可售面积 英文逗号分隔
     */
    private String inSaleableAreas;
    
    /**
     * Equal 可售套数
     */
    private String equalAvailableSets;
    
    /**
     * Like 可售套数
     */
    private String likeAvailableSets;
    
     /**
     * In 可售套数 英文逗号分隔
     */
    private String inAvailableSetss;
    
    /**
     * Equal 时间
     */
    private String equalInfoUpdataTime;
    
    /**
     * Like 时间
     */
    private String likeInfoUpdataTime;
    
     /**
     * In 时间 英文逗号分隔
     */
    private String inInfoUpdataTimes;
    

    public void setEqualHouseType(String equalHouseType) {
        this.equalHouseType = equalHouseType;
    }
    
    public String getEqualHouseType() {
        return this.equalHouseType;
    }
    
    public void setLikeHouseType(String likeHouseType) {
        this.likeHouseType = likeHouseType;
    }
    
    public String getLikeHouseType() {
        return this.likeHouseType;
    }
    
    public void setInHouseTypes(String inHouseTypes) {
        this.inHouseTypes = inHouseTypes;
    }
    
    public String getInHouseTypes() {
        return this.inHouseTypes;
    }
    public void setEqualTransactionNumber(String equalTransactionNumber) {
        this.equalTransactionNumber = equalTransactionNumber;
    }
    
    public String getEqualTransactionNumber() {
        return this.equalTransactionNumber;
    }
    
    public void setLikeTransactionNumber(String likeTransactionNumber) {
        this.likeTransactionNumber = likeTransactionNumber;
    }
    
    public String getLikeTransactionNumber() {
        return this.likeTransactionNumber;
    }
    
    public void setInTransactionNumbers(String inTransactionNumbers) {
        this.inTransactionNumbers = inTransactionNumbers;
    }
    
    public String getInTransactionNumbers() {
        return this.inTransactionNumbers;
    }
    public void setEqualTransactionArea(String equalTransactionArea) {
        this.equalTransactionArea = equalTransactionArea;
    }
    
    public String getEqualTransactionArea() {
        return this.equalTransactionArea;
    }
    
    public void setLikeTransactionArea(String likeTransactionArea) {
        this.likeTransactionArea = likeTransactionArea;
    }
    
    public String getLikeTransactionArea() {
        return this.likeTransactionArea;
    }
    
    public void setInTransactionAreas(String inTransactionAreas) {
        this.inTransactionAreas = inTransactionAreas;
    }
    
    public String getInTransactionAreas() {
        return this.inTransactionAreas;
    }
    public void setEqualCity(String equalCity) {
        this.equalCity = equalCity;
    }
    
    public String getEqualCity() {
        return this.equalCity;
    }
    
    public void setLikeCity(String likeCity) {
        this.likeCity = likeCity;
    }
    
    public String getLikeCity() {
        return this.likeCity;
    }
    
    public void setInCitys(String inCitys) {
        this.inCitys = inCitys;
    }
    
    public String getInCitys() {
        return this.inCitys;
    }
    public void setEqualPart(String equalPart) {
        this.equalPart = equalPart;
    }
    
    public String getEqualPart() {
        return this.equalPart;
    }
    
    public void setLikePart(String likePart) {
        this.likePart = likePart;
    }
    
    public String getLikePart() {
        return this.likePart;
    }
    
    public void setInParts(String inParts) {
        this.inParts = inParts;
    }
    
    public String getInParts() {
        return this.inParts;
    }
    public void setEqualTransactionPrice(String equalTransactionPrice) {
        this.equalTransactionPrice = equalTransactionPrice;
    }
    
    public String getEqualTransactionPrice() {
        return this.equalTransactionPrice;
    }
    
    public void setLikeTransactionPrice(String likeTransactionPrice) {
        this.likeTransactionPrice = likeTransactionPrice;
    }
    
    public String getLikeTransactionPrice() {
        return this.likeTransactionPrice;
    }
    
    public void setInTransactionPrices(String inTransactionPrices) {
        this.inTransactionPrices = inTransactionPrices;
    }
    
    public String getInTransactionPrices() {
        return this.inTransactionPrices;
    }
    public void setEqualSaleableArea(String equalSaleableArea) {
        this.equalSaleableArea = equalSaleableArea;
    }
    
    public String getEqualSaleableArea() {
        return this.equalSaleableArea;
    }
    
    public void setLikeSaleableArea(String likeSaleableArea) {
        this.likeSaleableArea = likeSaleableArea;
    }
    
    public String getLikeSaleableArea() {
        return this.likeSaleableArea;
    }
    
    public void setInSaleableAreas(String inSaleableAreas) {
        this.inSaleableAreas = inSaleableAreas;
    }
    
    public String getInSaleableAreas() {
        return this.inSaleableAreas;
    }
    public void setEqualAvailableSets(String equalAvailableSets) {
        this.equalAvailableSets = equalAvailableSets;
    }
    
    public String getEqualAvailableSets() {
        return this.equalAvailableSets;
    }
    
    public void setLikeAvailableSets(String likeAvailableSets) {
        this.likeAvailableSets = likeAvailableSets;
    }
    
    public String getLikeAvailableSets() {
        return this.likeAvailableSets;
    }
    
    public void setInAvailableSetss(String inAvailableSetss) {
        this.inAvailableSetss = inAvailableSetss;
    }
    
    public String getInAvailableSetss() {
        return this.inAvailableSetss;
    }
    public void setEqualInfoUpdataTime(String equalInfoUpdataTime) {
        this.equalInfoUpdataTime = equalInfoUpdataTime;
    }
    
    public String getEqualInfoUpdataTime() {
        return this.equalInfoUpdataTime;
    }
    
    public void setLikeInfoUpdataTime(String likeInfoUpdataTime) {
        this.likeInfoUpdataTime = likeInfoUpdataTime;
    }
    
    public String getLikeInfoUpdataTime() {
        return this.likeInfoUpdataTime;
    }
    
    public void setInInfoUpdataTimes(String inInfoUpdataTimes) {
        this.inInfoUpdataTimes = inInfoUpdataTimes;
    }
    
    public String getInInfoUpdataTimes() {
        return this.inInfoUpdataTimes;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalHouseType)){
			criterias.add(Restrictions.eq("houseType", equalHouseType));
		}
	    if(StringUtils.isNotBlank(likeHouseType)){
			criterias.add(Restrictions.allLike("houseType", likeHouseType));
		}
		 if(StringUtils.isNotBlank(inHouseTypes)){
			criterias.add(Restrictions.in("houseType", StringUtils.split(inHouseTypes, ",")));
		}
	    if(StringUtils.isNotBlank(equalTransactionNumber)){
			criterias.add(Restrictions.eq("transactionNumber", equalTransactionNumber));
		}
	    if(StringUtils.isNotBlank(likeTransactionNumber)){
			criterias.add(Restrictions.allLike("transactionNumber", likeTransactionNumber));
		}
		 if(StringUtils.isNotBlank(inTransactionNumbers)){
			criterias.add(Restrictions.in("transactionNumber", StringUtils.split(inTransactionNumbers, ",")));
		}
	    if(StringUtils.isNotBlank(equalTransactionArea)){
			criterias.add(Restrictions.eq("transactionArea", equalTransactionArea));
		}
	    if(StringUtils.isNotBlank(likeTransactionArea)){
			criterias.add(Restrictions.allLike("transactionArea", likeTransactionArea));
		}
		 if(StringUtils.isNotBlank(inTransactionAreas)){
			criterias.add(Restrictions.in("transactionArea", StringUtils.split(inTransactionAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalCity)){
			criterias.add(Restrictions.eq("city", equalCity));
		}
	    if(StringUtils.isNotBlank(likeCity)){
			criterias.add(Restrictions.allLike("city", likeCity));
		}
		 if(StringUtils.isNotBlank(inCitys)){
			criterias.add(Restrictions.in("city", StringUtils.split(inCitys, ",")));
		}
	    if(StringUtils.isNotBlank(equalPart)){
			criterias.add(Restrictions.eq("part", equalPart));
		}
	    if(StringUtils.isNotBlank(likePart)){
			criterias.add(Restrictions.allLike("part", likePart));
		}
		 if(StringUtils.isNotBlank(inParts)){
			criterias.add(Restrictions.in("part", StringUtils.split(inParts, ",")));
		}
	    if(StringUtils.isNotBlank(equalTransactionPrice)){
			criterias.add(Restrictions.eq("transactionPrice", equalTransactionPrice));
		}
	    if(StringUtils.isNotBlank(likeTransactionPrice)){
			criterias.add(Restrictions.allLike("transactionPrice", likeTransactionPrice));
		}
		 if(StringUtils.isNotBlank(inTransactionPrices)){
			criterias.add(Restrictions.in("transactionPrice", StringUtils.split(inTransactionPrices, ",")));
		}
	    if(StringUtils.isNotBlank(equalSaleableArea)){
			criterias.add(Restrictions.eq("saleableArea", equalSaleableArea));
		}
	    if(StringUtils.isNotBlank(likeSaleableArea)){
			criterias.add(Restrictions.allLike("saleableArea", likeSaleableArea));
		}
		 if(StringUtils.isNotBlank(inSaleableAreas)){
			criterias.add(Restrictions.in("saleableArea", StringUtils.split(inSaleableAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalAvailableSets)){
			criterias.add(Restrictions.eq("availableSets", equalAvailableSets));
		}
	    if(StringUtils.isNotBlank(likeAvailableSets)){
			criterias.add(Restrictions.allLike("availableSets", likeAvailableSets));
		}
		 if(StringUtils.isNotBlank(inAvailableSetss)){
			criterias.add(Restrictions.in("availableSets", StringUtils.split(inAvailableSetss, ",")));
		}
	    if(StringUtils.isNotBlank(equalInfoUpdataTime)){
			criterias.add(Restrictions.eq("infoUpdataTime", equalInfoUpdataTime));
		}
	    if(StringUtils.isNotBlank(likeInfoUpdataTime)){
			criterias.add(Restrictions.allLike("infoUpdataTime", likeInfoUpdataTime));
		}
		 if(StringUtils.isNotBlank(inInfoUpdataTimes)){
			criterias.add(Restrictions.in("infoUpdataTime", StringUtils.split(inInfoUpdataTimes, ",")));
		}
	}
}