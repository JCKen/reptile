package com.jrzh.mvc.search.reptile;

import org.apache.commons.lang.StringUtils;
import com.jrzh.framework.searchutils.Criteria;
import com.jrzh.framework.searchutils.Restrictions;
import com.jrzh.framework.base.search.BaseSearch;

public class ReptileNewhouseYushouzhengSearch extends BaseSearch{
	private static final long serialVersionUID = 1L;
	
    /**
     * Equal 城市
     */
    private String equalCity;
    
    /**
     * Like 城市
     */
    private String likeCity;
    
     /**
     * In 城市 英文逗号分隔
     */
    private String inCitys;
    
    /**
     * Equal 区域
     */
    private String equalPart;
    
    /**
     * Like 区域
     */
    private String likePart;
    
     /**
     * In 区域 英文逗号分隔
     */
    private String inParts;
    
    /**
     * Equal 关联新房的id
     */
    private String equalHouseId;
    
    /**
     * Like 关联新房的id
     */
    private String likeHouseId;
    
     /**
     * In 关联新房的id 英文逗号分隔
     */
    private String inHouseIds;
    
    /**
     * Equal 预售证号
     */
    private String equalNo;
    
    /**
     * Like 预售证号
     */
    private String likeNo;
    
     /**
     * In 预售证号 英文逗号分隔
     */
    private String inNos;
    
    /**
     * Equal 项目名
     */
    private String equalProjectName;
    
    /**
     * Like 项目名
     */
    private String likeProjectName;
    
     /**
     * In 项目名 英文逗号分隔
     */
    private String inProjectNames;
    
    /**
     * Equal 住宅预售套数
     */
    private String equalHouseCount;
    
    /**
     * Like 住宅预售套数
     */
    private String likeHouseCount;
    
     /**
     * In 住宅预售套数 英文逗号分隔
     */
    private String inHouseCounts;
    
    /**
     * Equal 住宅预售面积（平米）
     */
    private String equalHouseArea;
    
    /**
     * Like 住宅预售面积（平米）
     */
    private String likeHouseArea;
    
     /**
     * In 住宅预售面积（平米） 英文逗号分隔
     */
    private String inHouseAreas;
    
    /**
     * Equal 商业预售套数
     */
    private String equalBusinessCount;
    
    /**
     * Like 商业预售套数
     */
    private String likeBusinessCount;
    
     /**
     * In 商业预售套数 英文逗号分隔
     */
    private String inBusinessCounts;
    
    /**
     * Equal 商业预售面积（平米）
     */
    private String equalBusinessArea;
    
    /**
     * Like 商业预售面积（平米）
     */
    private String likeBusinessArea;
    
     /**
     * In 商业预售面积（平米） 英文逗号分隔
     */
    private String inBusinessAreas;
    
    /**
     * Equal 办公预售套数
     */
    private String equalOfficeCount;
    
    /**
     * Like 办公预售套数
     */
    private String likeOfficeCount;
    
     /**
     * In 办公预售套数 英文逗号分隔
     */
    private String inOfficeCounts;
    
    /**
     * Equal 办公预售面积（平米）
     */
    private String equalOfficeArea;
    
    /**
     * Like 办公预售面积（平米）
     */
    private String likeOfficeArea;
    
     /**
     * In 办公预售面积（平米） 英文逗号分隔
     */
    private String inOfficeAreas;
    
    /**
     * Equal 车位预售套数
     */
    private String equalCarportCount;
    
    /**
     * Like 车位预售套数
     */
    private String likeCarportCount;
    
     /**
     * In 车位预售套数 英文逗号分隔
     */
    private String inCarportCounts;
    
    /**
     * Equal 车位预售面积（平米）
     */
    private String equalCarportArea;
    
    /**
     * Like 车位预售面积（平米）
     */
    private String likeCarportArea;
    
     /**
     * In 车位预售面积（平米） 英文逗号分隔
     */
    private String inCarportAreas;
    
    /**
     * Equal 其他预售套数
     */
    private String equalOtherCount;
    
    /**
     * Like 其他预售套数
     */
    private String likeOtherCount;
    
     /**
     * In 其他预售套数 英文逗号分隔
     */
    private String inOtherCounts;
    
    /**
     * Equal 其他预售面积（平米）
     */
    private String equalOtherArea;
    
    /**
     * Like 其他预售面积（平米）
     */
    private String likeOtherArea;
    
     /**
     * In 其他预售面积（平米） 英文逗号分隔
     */
    private String inOtherAreas;
    
    /**
     * Equal 本期预售总套数
     */
    private String equalAllCount;
    
    /**
     * Like 本期预售总套数
     */
    private String likeAllCount;
    
     /**
     * In 本期预售总套数 英文逗号分隔
     */
    private String inAllCounts;
    
    /**
     * Equal 本期预售总套数
     */
    private String equalAllArea;
    
    /**
     * Like 本期预售总套数
     */
    private String likeAllArea;
    
     /**
     * In 本期预售总套数 英文逗号分隔
     */
    private String inAllAreas;
    
    /**
     * Equal 发证日期
     */
    private String equalOpeningDate;
    
    /**
     * Like 发证日期
     */
    private String likeOpeningDate;
    
     /**
     * In 发证日期 英文逗号分隔
     */
    private String inOpeningDates;
    
    /**
     * Equal 发证机关
     */
    private String equalOffice;
    
    /**
     * Like 发证机关
     */
    private String likeOffice;
    
     /**
     * In 发证机关 英文逗号分隔
     */
    private String inOffices;
    
    /**
     * Equal 是否抵押
     */
    private String equalHypothecate;
    
    /**
     * Like 是否抵押
     */
    private String likeHypothecate;
    
     /**
     * In 是否抵押 英文逗号分隔
     */
    private String inHypothecates;
    
    /**
     * Equal 地上面积(平米)
     */
    private String equalAboveArea;
    
    /**
     * Like 地上面积(平米)
     */
    private String likeAboveArea;
    
     /**
     * In 地上面积(平米) 英文逗号分隔
     */
    private String inAboveAreas;
    
    /**
     * Equal 地下面积(平米)
     */
    private String equalUnderArea;
    
    /**
     * Like 地下面积(平米)
     */
    private String likeUnderArea;
    
     /**
     * In 地下面积(平米) 英文逗号分隔
     */
    private String inUnderAreas;
    
    /**
     * Equal 数据来源地址
     */
    private String equalUrl;
    
    /**
     * Like 数据来源地址
     */
    private String likeUrl;
    
     /**
     * In 数据来源地址 英文逗号分隔
     */
    private String inUrls;
    
    /**
     * Equal 数据来源
     */
    private String equalInformationSources;
    
    /**
     * Like 数据来源
     */
    private String likeInformationSources;
    
     /**
     * In 数据来源 英文逗号分隔
     */
    private String inInformationSourcess;
    

    public void setEqualCity(String equalCity) {
        this.equalCity = equalCity;
    }
    
    public String getEqualCity() {
        return this.equalCity;
    }
    
    public void setLikeCity(String likeCity) {
        this.likeCity = likeCity;
    }
    
    public String getLikeCity() {
        return this.likeCity;
    }
    
    public void setInCitys(String inCitys) {
        this.inCitys = inCitys;
    }
    
    public String getInCitys() {
        return this.inCitys;
    }
    public void setEqualPart(String equalPart) {
        this.equalPart = equalPart;
    }
    
    public String getEqualPart() {
        return this.equalPart;
    }
    
    public void setLikePart(String likePart) {
        this.likePart = likePart;
    }
    
    public String getLikePart() {
        return this.likePart;
    }
    
    public void setInParts(String inParts) {
        this.inParts = inParts;
    }
    
    public String getInParts() {
        return this.inParts;
    }
    public void setEqualHouseId(String equalHouseId) {
        this.equalHouseId = equalHouseId;
    }
    
    public String getEqualHouseId() {
        return this.equalHouseId;
    }
    
    public void setLikeHouseId(String likeHouseId) {
        this.likeHouseId = likeHouseId;
    }
    
    public String getLikeHouseId() {
        return this.likeHouseId;
    }
    
    public void setInHouseIds(String inHouseIds) {
        this.inHouseIds = inHouseIds;
    }
    
    public String getInHouseIds() {
        return this.inHouseIds;
    }
    public void setEqualNo(String equalNo) {
        this.equalNo = equalNo;
    }
    
    public String getEqualNo() {
        return this.equalNo;
    }
    
    public void setLikeNo(String likeNo) {
        this.likeNo = likeNo;
    }
    
    public String getLikeNo() {
        return this.likeNo;
    }
    
    public void setInNos(String inNos) {
        this.inNos = inNos;
    }
    
    public String getInNos() {
        return this.inNos;
    }
    public void setEqualProjectName(String equalProjectName) {
        this.equalProjectName = equalProjectName;
    }
    
    public String getEqualProjectName() {
        return this.equalProjectName;
    }
    
    public void setLikeProjectName(String likeProjectName) {
        this.likeProjectName = likeProjectName;
    }
    
    public String getLikeProjectName() {
        return this.likeProjectName;
    }
    
    public void setInProjectNames(String inProjectNames) {
        this.inProjectNames = inProjectNames;
    }
    
    public String getInProjectNames() {
        return this.inProjectNames;
    }
    public void setEqualHouseCount(String equalHouseCount) {
        this.equalHouseCount = equalHouseCount;
    }
    
    public String getEqualHouseCount() {
        return this.equalHouseCount;
    }
    
    public void setLikeHouseCount(String likeHouseCount) {
        this.likeHouseCount = likeHouseCount;
    }
    
    public String getLikeHouseCount() {
        return this.likeHouseCount;
    }
    
    public void setInHouseCounts(String inHouseCounts) {
        this.inHouseCounts = inHouseCounts;
    }
    
    public String getInHouseCounts() {
        return this.inHouseCounts;
    }
    public void setEqualHouseArea(String equalHouseArea) {
        this.equalHouseArea = equalHouseArea;
    }
    
    public String getEqualHouseArea() {
        return this.equalHouseArea;
    }
    
    public void setLikeHouseArea(String likeHouseArea) {
        this.likeHouseArea = likeHouseArea;
    }
    
    public String getLikeHouseArea() {
        return this.likeHouseArea;
    }
    
    public void setInHouseAreas(String inHouseAreas) {
        this.inHouseAreas = inHouseAreas;
    }
    
    public String getInHouseAreas() {
        return this.inHouseAreas;
    }
    public void setEqualBusinessCount(String equalBusinessCount) {
        this.equalBusinessCount = equalBusinessCount;
    }
    
    public String getEqualBusinessCount() {
        return this.equalBusinessCount;
    }
    
    public void setLikeBusinessCount(String likeBusinessCount) {
        this.likeBusinessCount = likeBusinessCount;
    }
    
    public String getLikeBusinessCount() {
        return this.likeBusinessCount;
    }
    
    public void setInBusinessCounts(String inBusinessCounts) {
        this.inBusinessCounts = inBusinessCounts;
    }
    
    public String getInBusinessCounts() {
        return this.inBusinessCounts;
    }
    public void setEqualBusinessArea(String equalBusinessArea) {
        this.equalBusinessArea = equalBusinessArea;
    }
    
    public String getEqualBusinessArea() {
        return this.equalBusinessArea;
    }
    
    public void setLikeBusinessArea(String likeBusinessArea) {
        this.likeBusinessArea = likeBusinessArea;
    }
    
    public String getLikeBusinessArea() {
        return this.likeBusinessArea;
    }
    
    public void setInBusinessAreas(String inBusinessAreas) {
        this.inBusinessAreas = inBusinessAreas;
    }
    
    public String getInBusinessAreas() {
        return this.inBusinessAreas;
    }
    public void setEqualOfficeCount(String equalOfficeCount) {
        this.equalOfficeCount = equalOfficeCount;
    }
    
    public String getEqualOfficeCount() {
        return this.equalOfficeCount;
    }
    
    public void setLikeOfficeCount(String likeOfficeCount) {
        this.likeOfficeCount = likeOfficeCount;
    }
    
    public String getLikeOfficeCount() {
        return this.likeOfficeCount;
    }
    
    public void setInOfficeCounts(String inOfficeCounts) {
        this.inOfficeCounts = inOfficeCounts;
    }
    
    public String getInOfficeCounts() {
        return this.inOfficeCounts;
    }
    public void setEqualOfficeArea(String equalOfficeArea) {
        this.equalOfficeArea = equalOfficeArea;
    }
    
    public String getEqualOfficeArea() {
        return this.equalOfficeArea;
    }
    
    public void setLikeOfficeArea(String likeOfficeArea) {
        this.likeOfficeArea = likeOfficeArea;
    }
    
    public String getLikeOfficeArea() {
        return this.likeOfficeArea;
    }
    
    public void setInOfficeAreas(String inOfficeAreas) {
        this.inOfficeAreas = inOfficeAreas;
    }
    
    public String getInOfficeAreas() {
        return this.inOfficeAreas;
    }
    public void setEqualCarportCount(String equalCarportCount) {
        this.equalCarportCount = equalCarportCount;
    }
    
    public String getEqualCarportCount() {
        return this.equalCarportCount;
    }
    
    public void setLikeCarportCount(String likeCarportCount) {
        this.likeCarportCount = likeCarportCount;
    }
    
    public String getLikeCarportCount() {
        return this.likeCarportCount;
    }
    
    public void setInCarportCounts(String inCarportCounts) {
        this.inCarportCounts = inCarportCounts;
    }
    
    public String getInCarportCounts() {
        return this.inCarportCounts;
    }
    public void setEqualCarportArea(String equalCarportArea) {
        this.equalCarportArea = equalCarportArea;
    }
    
    public String getEqualCarportArea() {
        return this.equalCarportArea;
    }
    
    public void setLikeCarportArea(String likeCarportArea) {
        this.likeCarportArea = likeCarportArea;
    }
    
    public String getLikeCarportArea() {
        return this.likeCarportArea;
    }
    
    public void setInCarportAreas(String inCarportAreas) {
        this.inCarportAreas = inCarportAreas;
    }
    
    public String getInCarportAreas() {
        return this.inCarportAreas;
    }
    public void setEqualOtherCount(String equalOtherCount) {
        this.equalOtherCount = equalOtherCount;
    }
    
    public String getEqualOtherCount() {
        return this.equalOtherCount;
    }
    
    public void setLikeOtherCount(String likeOtherCount) {
        this.likeOtherCount = likeOtherCount;
    }
    
    public String getLikeOtherCount() {
        return this.likeOtherCount;
    }
    
    public void setInOtherCounts(String inOtherCounts) {
        this.inOtherCounts = inOtherCounts;
    }
    
    public String getInOtherCounts() {
        return this.inOtherCounts;
    }
    public void setEqualOtherArea(String equalOtherArea) {
        this.equalOtherArea = equalOtherArea;
    }
    
    public String getEqualOtherArea() {
        return this.equalOtherArea;
    }
    
    public void setLikeOtherArea(String likeOtherArea) {
        this.likeOtherArea = likeOtherArea;
    }
    
    public String getLikeOtherArea() {
        return this.likeOtherArea;
    }
    
    public void setInOtherAreas(String inOtherAreas) {
        this.inOtherAreas = inOtherAreas;
    }
    
    public String getInOtherAreas() {
        return this.inOtherAreas;
    }
    public void setEqualAllCount(String equalAllCount) {
        this.equalAllCount = equalAllCount;
    }
    
    public String getEqualAllCount() {
        return this.equalAllCount;
    }
    
    public void setLikeAllCount(String likeAllCount) {
        this.likeAllCount = likeAllCount;
    }
    
    public String getLikeAllCount() {
        return this.likeAllCount;
    }
    
    public void setInAllCounts(String inAllCounts) {
        this.inAllCounts = inAllCounts;
    }
    
    public String getInAllCounts() {
        return this.inAllCounts;
    }
    public void setEqualAllArea(String equalAllArea) {
        this.equalAllArea = equalAllArea;
    }
    
    public String getEqualAllArea() {
        return this.equalAllArea;
    }
    
    public void setLikeAllArea(String likeAllArea) {
        this.likeAllArea = likeAllArea;
    }
    
    public String getLikeAllArea() {
        return this.likeAllArea;
    }
    
    public void setInAllAreas(String inAllAreas) {
        this.inAllAreas = inAllAreas;
    }
    
    public String getInAllAreas() {
        return this.inAllAreas;
    }
    public void setEqualOpeningDate(String equalOpeningDate) {
        this.equalOpeningDate = equalOpeningDate;
    }
    
    public String getEqualOpeningDate() {
        return this.equalOpeningDate;
    }
    
    public void setLikeOpeningDate(String likeOpeningDate) {
        this.likeOpeningDate = likeOpeningDate;
    }
    
    public String getLikeOpeningDate() {
        return this.likeOpeningDate;
    }
    
    public void setInOpeningDates(String inOpeningDates) {
        this.inOpeningDates = inOpeningDates;
    }
    
    public String getInOpeningDates() {
        return this.inOpeningDates;
    }
    public void setEqualOffice(String equalOffice) {
        this.equalOffice = equalOffice;
    }
    
    public String getEqualOffice() {
        return this.equalOffice;
    }
    
    public void setLikeOffice(String likeOffice) {
        this.likeOffice = likeOffice;
    }
    
    public String getLikeOffice() {
        return this.likeOffice;
    }
    
    public void setInOffices(String inOffices) {
        this.inOffices = inOffices;
    }
    
    public String getInOffices() {
        return this.inOffices;
    }
    public void setEqualHypothecate(String equalHypothecate) {
        this.equalHypothecate = equalHypothecate;
    }
    
    public String getEqualHypothecate() {
        return this.equalHypothecate;
    }
    
    public void setLikeHypothecate(String likeHypothecate) {
        this.likeHypothecate = likeHypothecate;
    }
    
    public String getLikeHypothecate() {
        return this.likeHypothecate;
    }
    
    public void setInHypothecates(String inHypothecates) {
        this.inHypothecates = inHypothecates;
    }
    
    public String getInHypothecates() {
        return this.inHypothecates;
    }
    public void setEqualAboveArea(String equalAboveArea) {
        this.equalAboveArea = equalAboveArea;
    }
    
    public String getEqualAboveArea() {
        return this.equalAboveArea;
    }
    
    public void setLikeAboveArea(String likeAboveArea) {
        this.likeAboveArea = likeAboveArea;
    }
    
    public String getLikeAboveArea() {
        return this.likeAboveArea;
    }
    
    public void setInAboveAreas(String inAboveAreas) {
        this.inAboveAreas = inAboveAreas;
    }
    
    public String getInAboveAreas() {
        return this.inAboveAreas;
    }
    public void setEqualUnderArea(String equalUnderArea) {
        this.equalUnderArea = equalUnderArea;
    }
    
    public String getEqualUnderArea() {
        return this.equalUnderArea;
    }
    
    public void setLikeUnderArea(String likeUnderArea) {
        this.likeUnderArea = likeUnderArea;
    }
    
    public String getLikeUnderArea() {
        return this.likeUnderArea;
    }
    
    public void setInUnderAreas(String inUnderAreas) {
        this.inUnderAreas = inUnderAreas;
    }
    
    public String getInUnderAreas() {
        return this.inUnderAreas;
    }
    public void setEqualUrl(String equalUrl) {
        this.equalUrl = equalUrl;
    }
    
    public String getEqualUrl() {
        return this.equalUrl;
    }
    
    public void setLikeUrl(String likeUrl) {
        this.likeUrl = likeUrl;
    }
    
    public String getLikeUrl() {
        return this.likeUrl;
    }
    
    public void setInUrls(String inUrls) {
        this.inUrls = inUrls;
    }
    
    public String getInUrls() {
        return this.inUrls;
    }
    public void setEqualInformationSources(String equalInformationSources) {
        this.equalInformationSources = equalInformationSources;
    }
    
    public String getEqualInformationSources() {
        return this.equalInformationSources;
    }
    
    public void setLikeInformationSources(String likeInformationSources) {
        this.likeInformationSources = likeInformationSources;
    }
    
    public String getLikeInformationSources() {
        return this.likeInformationSources;
    }
    
    public void setInInformationSourcess(String inInformationSourcess) {
        this.inInformationSourcess = inInformationSourcess;
    }
    
    public String getInInformationSourcess() {
        return this.inInformationSourcess;
    }

	@Override
	public void setCriterias(Criteria<?> criterias) {
	    if(StringUtils.isNotBlank(equalCity)){
			criterias.add(Restrictions.eq("city", equalCity));
		}
	    if(StringUtils.isNotBlank(likeCity)){
			criterias.add(Restrictions.allLike("city", likeCity));
		}
		 if(StringUtils.isNotBlank(inCitys)){
			criterias.add(Restrictions.in("city", StringUtils.split(inCitys, ",")));
		}
	    if(StringUtils.isNotBlank(equalPart)){
			criterias.add(Restrictions.eq("part", equalPart));
		}
	    if(StringUtils.isNotBlank(likePart)){
			criterias.add(Restrictions.allLike("part", likePart));
		}
		 if(StringUtils.isNotBlank(inParts)){
			criterias.add(Restrictions.in("part", StringUtils.split(inParts, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseId)){
			criterias.add(Restrictions.eq("houseId", equalHouseId));
		}
	    if(StringUtils.isNotBlank(likeHouseId)){
			criterias.add(Restrictions.allLike("houseId", likeHouseId));
		}
		 if(StringUtils.isNotBlank(inHouseIds)){
			criterias.add(Restrictions.in("houseId", StringUtils.split(inHouseIds, ",")));
		}
	    if(StringUtils.isNotBlank(equalNo)){
			criterias.add(Restrictions.eq("no", equalNo));
		}
	    if(StringUtils.isNotBlank(likeNo)){
			criterias.add(Restrictions.allLike("no", likeNo));
		}
		 if(StringUtils.isNotBlank(inNos)){
			criterias.add(Restrictions.in("no", StringUtils.split(inNos, ",")));
		}
	    if(StringUtils.isNotBlank(equalProjectName)){
			criterias.add(Restrictions.eq("projectName", equalProjectName));
		}
	    if(StringUtils.isNotBlank(likeProjectName)){
			criterias.add(Restrictions.allLike("projectName", likeProjectName));
		}
		 if(StringUtils.isNotBlank(inProjectNames)){
			criterias.add(Restrictions.in("projectName", StringUtils.split(inProjectNames, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseCount)){
			criterias.add(Restrictions.eq("houseCount", equalHouseCount));
		}
	    if(StringUtils.isNotBlank(likeHouseCount)){
			criterias.add(Restrictions.allLike("houseCount", likeHouseCount));
		}
		 if(StringUtils.isNotBlank(inHouseCounts)){
			criterias.add(Restrictions.in("houseCount", StringUtils.split(inHouseCounts, ",")));
		}
	    if(StringUtils.isNotBlank(equalHouseArea)){
			criterias.add(Restrictions.eq("houseArea", equalHouseArea));
		}
	    if(StringUtils.isNotBlank(likeHouseArea)){
			criterias.add(Restrictions.allLike("houseArea", likeHouseArea));
		}
		 if(StringUtils.isNotBlank(inHouseAreas)){
			criterias.add(Restrictions.in("houseArea", StringUtils.split(inHouseAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalBusinessCount)){
			criterias.add(Restrictions.eq("businessCount", equalBusinessCount));
		}
	    if(StringUtils.isNotBlank(likeBusinessCount)){
			criterias.add(Restrictions.allLike("businessCount", likeBusinessCount));
		}
		 if(StringUtils.isNotBlank(inBusinessCounts)){
			criterias.add(Restrictions.in("businessCount", StringUtils.split(inBusinessCounts, ",")));
		}
	    if(StringUtils.isNotBlank(equalBusinessArea)){
			criterias.add(Restrictions.eq("businessArea", equalBusinessArea));
		}
	    if(StringUtils.isNotBlank(likeBusinessArea)){
			criterias.add(Restrictions.allLike("businessArea", likeBusinessArea));
		}
		 if(StringUtils.isNotBlank(inBusinessAreas)){
			criterias.add(Restrictions.in("businessArea", StringUtils.split(inBusinessAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalOfficeCount)){
			criterias.add(Restrictions.eq("officeCount", equalOfficeCount));
		}
	    if(StringUtils.isNotBlank(likeOfficeCount)){
			criterias.add(Restrictions.allLike("officeCount", likeOfficeCount));
		}
		 if(StringUtils.isNotBlank(inOfficeCounts)){
			criterias.add(Restrictions.in("officeCount", StringUtils.split(inOfficeCounts, ",")));
		}
	    if(StringUtils.isNotBlank(equalOfficeArea)){
			criterias.add(Restrictions.eq("officeArea", equalOfficeArea));
		}
	    if(StringUtils.isNotBlank(likeOfficeArea)){
			criterias.add(Restrictions.allLike("officeArea", likeOfficeArea));
		}
		 if(StringUtils.isNotBlank(inOfficeAreas)){
			criterias.add(Restrictions.in("officeArea", StringUtils.split(inOfficeAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalCarportCount)){
			criterias.add(Restrictions.eq("carportCount", equalCarportCount));
		}
	    if(StringUtils.isNotBlank(likeCarportCount)){
			criterias.add(Restrictions.allLike("carportCount", likeCarportCount));
		}
		 if(StringUtils.isNotBlank(inCarportCounts)){
			criterias.add(Restrictions.in("carportCount", StringUtils.split(inCarportCounts, ",")));
		}
	    if(StringUtils.isNotBlank(equalCarportArea)){
			criterias.add(Restrictions.eq("carportArea", equalCarportArea));
		}
	    if(StringUtils.isNotBlank(likeCarportArea)){
			criterias.add(Restrictions.allLike("carportArea", likeCarportArea));
		}
		 if(StringUtils.isNotBlank(inCarportAreas)){
			criterias.add(Restrictions.in("carportArea", StringUtils.split(inCarportAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalOtherCount)){
			criterias.add(Restrictions.eq("otherCount", equalOtherCount));
		}
	    if(StringUtils.isNotBlank(likeOtherCount)){
			criterias.add(Restrictions.allLike("otherCount", likeOtherCount));
		}
		 if(StringUtils.isNotBlank(inOtherCounts)){
			criterias.add(Restrictions.in("otherCount", StringUtils.split(inOtherCounts, ",")));
		}
	    if(StringUtils.isNotBlank(equalOtherArea)){
			criterias.add(Restrictions.eq("otherArea", equalOtherArea));
		}
	    if(StringUtils.isNotBlank(likeOtherArea)){
			criterias.add(Restrictions.allLike("otherArea", likeOtherArea));
		}
		 if(StringUtils.isNotBlank(inOtherAreas)){
			criterias.add(Restrictions.in("otherArea", StringUtils.split(inOtherAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalAllCount)){
			criterias.add(Restrictions.eq("allCount", equalAllCount));
		}
	    if(StringUtils.isNotBlank(likeAllCount)){
			criterias.add(Restrictions.allLike("allCount", likeAllCount));
		}
		 if(StringUtils.isNotBlank(inAllCounts)){
			criterias.add(Restrictions.in("allCount", StringUtils.split(inAllCounts, ",")));
		}
	    if(StringUtils.isNotBlank(equalAllArea)){
			criterias.add(Restrictions.eq("allArea", equalAllArea));
		}
	    if(StringUtils.isNotBlank(likeAllArea)){
			criterias.add(Restrictions.allLike("allArea", likeAllArea));
		}
		 if(StringUtils.isNotBlank(inAllAreas)){
			criterias.add(Restrictions.in("allArea", StringUtils.split(inAllAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalOpeningDate)){
			criterias.add(Restrictions.eq("openingDate", equalOpeningDate));
		}
	    if(StringUtils.isNotBlank(likeOpeningDate)){
			criterias.add(Restrictions.allLike("openingDate", likeOpeningDate));
		}
		 if(StringUtils.isNotBlank(inOpeningDates)){
			criterias.add(Restrictions.in("openingDate", StringUtils.split(inOpeningDates, ",")));
		}
	    if(StringUtils.isNotBlank(equalOffice)){
			criterias.add(Restrictions.eq("office", equalOffice));
		}
	    if(StringUtils.isNotBlank(likeOffice)){
			criterias.add(Restrictions.allLike("office", likeOffice));
		}
		 if(StringUtils.isNotBlank(inOffices)){
			criterias.add(Restrictions.in("office", StringUtils.split(inOffices, ",")));
		}
	    if(StringUtils.isNotBlank(equalHypothecate)){
			criterias.add(Restrictions.eq("hypothecate", equalHypothecate));
		}
	    if(StringUtils.isNotBlank(likeHypothecate)){
			criterias.add(Restrictions.allLike("hypothecate", likeHypothecate));
		}
		 if(StringUtils.isNotBlank(inHypothecates)){
			criterias.add(Restrictions.in("hypothecate", StringUtils.split(inHypothecates, ",")));
		}
	    if(StringUtils.isNotBlank(equalAboveArea)){
			criterias.add(Restrictions.eq("aboveArea", equalAboveArea));
		}
	    if(StringUtils.isNotBlank(likeAboveArea)){
			criterias.add(Restrictions.allLike("aboveArea", likeAboveArea));
		}
		 if(StringUtils.isNotBlank(inAboveAreas)){
			criterias.add(Restrictions.in("aboveArea", StringUtils.split(inAboveAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalUnderArea)){
			criterias.add(Restrictions.eq("underArea", equalUnderArea));
		}
	    if(StringUtils.isNotBlank(likeUnderArea)){
			criterias.add(Restrictions.allLike("underArea", likeUnderArea));
		}
		 if(StringUtils.isNotBlank(inUnderAreas)){
			criterias.add(Restrictions.in("underArea", StringUtils.split(inUnderAreas, ",")));
		}
	    if(StringUtils.isNotBlank(equalUrl)){
			criterias.add(Restrictions.eq("url", equalUrl));
		}
	    if(StringUtils.isNotBlank(likeUrl)){
			criterias.add(Restrictions.allLike("url", likeUrl));
		}
		 if(StringUtils.isNotBlank(inUrls)){
			criterias.add(Restrictions.in("url", StringUtils.split(inUrls, ",")));
		}
	    if(StringUtils.isNotBlank(equalInformationSources)){
			criterias.add(Restrictions.eq("informationSources", equalInformationSources));
		}
	    if(StringUtils.isNotBlank(likeInformationSources)){
			criterias.add(Restrictions.allLike("informationSources", likeInformationSources));
		}
		 if(StringUtils.isNotBlank(inInformationSourcess)){
			criterias.add(Restrictions.in("informationSources", StringUtils.split(inInformationSourcess, ",")));
		}
	}
}