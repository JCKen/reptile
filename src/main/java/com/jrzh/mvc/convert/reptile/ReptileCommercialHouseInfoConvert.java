package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileCommercialHouseInfoModel;
import com.jrzh.mvc.view.reptile.ReptileCommercialHouseInfoView;

@Component("reptileCommercialHouseInfoConvert")
public class ReptileCommercialHouseInfoConvert implements BaseConvertI<ReptileCommercialHouseInfoModel, ReptileCommercialHouseInfoView> {

	@Override
	public ReptileCommercialHouseInfoModel addConvert(ReptileCommercialHouseInfoView view) throws ProjectException {
		ReptileCommercialHouseInfoModel model = new ReptileCommercialHouseInfoModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileCommercialHouseInfoModel editConvert(ReptileCommercialHouseInfoView view, ReptileCommercialHouseInfoModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileCommercialHouseInfoView convertToView(ReptileCommercialHouseInfoModel model) throws ProjectException {
		ReptileCommercialHouseInfoView view = new ReptileCommercialHouseInfoView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
