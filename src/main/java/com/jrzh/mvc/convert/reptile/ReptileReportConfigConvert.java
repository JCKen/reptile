package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileReportConfigModel;
import com.jrzh.mvc.view.reptile.ReptileReportConfigView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileReportConfigConvert")
public class ReptileReportConfigConvert implements BaseConvertI<ReptileReportConfigModel, ReptileReportConfigView> {

	@Override
	public ReptileReportConfigModel addConvert(ReptileReportConfigView view) throws ProjectException {
		ReptileReportConfigModel model = new ReptileReportConfigModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileReportConfigModel editConvert(ReptileReportConfigView view, ReptileReportConfigModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileReportConfigView convertToView(ReptileReportConfigModel model) throws ProjectException {
		ReptileReportConfigView view = new ReptileReportConfigView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
