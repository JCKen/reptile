package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileTemplateModel;
import com.jrzh.mvc.view.reptile.ReptileTemplateView;

@Component("reptileTemplateConvert")
public class ReptileTemplateConvert implements BaseConvertI<ReptileTemplateModel, ReptileTemplateView> {

	@Override
	public ReptileTemplateModel addConvert(ReptileTemplateView view) throws ProjectException {
		ReptileTemplateModel model = new ReptileTemplateModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileTemplateModel editConvert(ReptileTemplateView view, ReptileTemplateModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileTemplateView convertToView(ReptileTemplateModel model) throws ProjectException {
		ReptileTemplateView view = new ReptileTemplateView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
