package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileUserModel;
import com.jrzh.mvc.view.reptile.ReptileUserView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileUserConvert")
public class ReptileUserConvert implements BaseConvertI<ReptileUserModel, ReptileUserView> {

	@Override
	public ReptileUserModel addConvert(ReptileUserView view) throws ProjectException {
		ReptileUserModel model = new ReptileUserModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileUserModel editConvert(ReptileUserView view, ReptileUserModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileUserView convertToView(ReptileUserModel model) throws ProjectException {
		ReptileUserView view = new ReptileUserView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
