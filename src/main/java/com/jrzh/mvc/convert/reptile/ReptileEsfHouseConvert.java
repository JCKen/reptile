package com.jrzh.mvc.convert.reptile;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileEsfHouseModel;
import com.jrzh.mvc.view.reptile.ReptileEsfHouseView;

@Component("reptileEsfHouseConvert")
public class ReptileEsfHouseConvert implements BaseConvertI<ReptileEsfHouseModel, ReptileEsfHouseView> {

	@Override
	public ReptileEsfHouseModel addConvert(ReptileEsfHouseView view) throws ProjectException {
		ReptileEsfHouseModel model = new ReptileEsfHouseModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileEsfHouseModel editConvert(ReptileEsfHouseView view, ReptileEsfHouseModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileEsfHouseView convertToView(ReptileEsfHouseModel model) throws ProjectException {
		ReptileEsfHouseView view = new ReptileEsfHouseView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

	public static ReptileEsfHouseModel mapToHouseModel(Map<String, String> dataMap){
		ReptileEsfHouseModel model = new ReptileEsfHouseModel();
		if(null != dataMap.get("房屋户型")){
			model.setHouseType(dataMap.get("房屋户型"));
		}
		if(null != dataMap.get("户型结构")){
			model.setUnitStructure(dataMap.get("户型结构"));
		}
		if(null != dataMap.get("所在楼层")){
			model.setHouseFloor(dataMap.get("所在楼层"));
		}
		if(null != dataMap.get("建筑面积")){
			String buildingArea = StringUtils.replace(dataMap.get("建筑面积"), "㎡", "");
			model.setBuildingArea(buildingArea);
		}
		if(null != dataMap.get("计租面积")){
			String buildingArea = StringUtils.replace(dataMap.get("计租面积"), "㎡", "");
			model.setBuildingArea(buildingArea);
		}
		if(null != dataMap.get("套内面积")){
			String areaCovered = StringUtils.replace(dataMap.get("套内面积"), "㎡", "");
			model.setAreaCovered(areaCovered);
		}
		if(null != dataMap.get("梯户比例")){
			model.setLadderRatio(dataMap.get("梯户比例"));
		}
		if(null != dataMap.get("房屋朝向")){
			model.setHouseOrientation(dataMap.get("房屋朝向"));
		}
		if(null != dataMap.get("建筑类型")){
			model.setBuildingType(dataMap.get("建筑类型"));
		}
		if(null != dataMap.get("建筑结构")){
			model.setBuildingStructure(dataMap.get("建筑结构"));
		}
		if(null != dataMap.get("装修情况")){
			model.setRenovationCondition(dataMap.get("装修情况"));
		}
		if(null != dataMap.get("配备电梯")){
			model.setEquippedWithSubway(dataMap.get("配备电梯"));
		}
		if(null != dataMap.get("产权年限")){
			model.setYearOfPropertyRights(dataMap.get("产权年限"));
		}
		if(null != dataMap.get("挂牌时间")){
			model.setListingTime(dataMap.get("挂牌时间"));
		}
		if(null != dataMap.get("房屋年限")){
			model.setYearOfHousing(dataMap.get("房屋年限"));
		}
		if(null != dataMap.get("抵押信息")){
			model.setMortgageInformation(dataMap.get("抵押信息"));
		}
		if(null != dataMap.get("上次交易")){
			model.setLastTradingTime(dataMap.get("上次交易"));
		}
		
		if(null != dataMap.get("房屋用途")){
			model.setUsageOfHouses(dataMap.get("房屋用途"));
		}
		if(null != dataMap.get("房本备件")){
			model.setHousingFiling(dataMap.get("房本备件"));
		}
		if(null != dataMap.get("产权所属")){
			model.setPropertyRights(dataMap.get("产权所属"));
		}
		if(null != dataMap.get("交易权属")){
			model.setHouseTradingAuthority(dataMap.get("交易权属"));
		}
		return model;
	}
	
	public static ReptileEsfHouseModel houseWorldMapToHouseModel(Map<String, String> dataMap){
		ReptileEsfHouseModel model = new ReptileEsfHouseModel();
		if(null != dataMap.get("售价")){
			if(dataMap.get("售价").contains("元/平米")) {
			model.setHouseAllPrice(StringUtils.replace(dataMap.get("售价"),"元/平米",""));
			}else {
				model.setHouseAllPrice(dataMap.get("售价"));
			}
		}
		if(null != dataMap.get("户型")){
			model.setHouseType(dataMap.get("户型"));
		}
		if(null != dataMap.get("建筑面积")){
			String buildingArea = StringUtils.replace(dataMap.get("建筑面积"), "㎡", "");
			model.setBuildingArea(buildingArea);
		}
		if(null != dataMap.get("计租面积")){
			String buildingArea = StringUtils.replace(dataMap.get("计租面积"), "㎡", "");
			model.setBuildingArea(buildingArea);
		}
		if(null != dataMap.get("单价")){
			String price = null;
			if (dataMap.get("单价").contains("元/平米")) {
				price = StringUtils.replace(dataMap.get("单价"), "元/平米", "");
			}else {
				price = dataMap.get("单价");
			}
			model.setHousePrice(price);
			String priceUnit = "1";
			if(StringUtils.contains(model.getHousePriceUnit(), "㎡") || StringUtils.contains(model.getHousePriceUnit(), "米") || StringUtils.contains(model.getHousePriceUnit(), "平")) {
				priceUnit = "1";
			}
			if(StringUtils.contains(model.getHousePriceUnit(), "套")) {
				priceUnit = "2";
			}
			model.setHousePriceUnit(priceUnit);
		}
		if(null != dataMap.get("楼层")){
			model.setHouseFloor(dataMap.get("楼层"));
		}
		if(null != dataMap.get("朝向")){
			model.setHouseOrientation(dataMap.get("朝向"));
		}
		if(null != dataMap.get("小区")){
			model.setHouseSmallPart(dataMap.get("小区"));
		}
		if(null != dataMap.get("楼型")){
			model.setUnitStructure(dataMap.get("楼型"));
		}
		if(null != dataMap.get("建筑结构")){
			model.setBuildingStructure(dataMap.get("建筑结构"));
		}
		if(null != dataMap.get("住宅类别")){
			model.setUsageOfHouses(dataMap.get("住宅类别"));
		}
		if(null != dataMap.get("装修")){
			model.setRenovationCondition(dataMap.get("装修"));
		}
		if(null != dataMap.get("发布时间")){
			model.setListingTime(dataMap.get("发布时间"));
		}
		if(null != dataMap.get("公交")){
			model.setTraffic(dataMap.get("公交"));
		}
		if(null != dataMap.get("银行")){
			model.setBank(dataMap.get("银行"));
		}
		if(null != dataMap.get("医院")){
			model.setHospital(dataMap.get("医院"));
		}
		if(null != dataMap.get("超市")){
			model.setPowerCenter(dataMap.get("超市"));
		}
		if(null != dataMap.get("邮局")){
			model.setPost(dataMap.get("邮局"));
		}
		if(null != dataMap.get("其他")){
			model.setOther(dataMap.get("其他"));
		}
		if(null != dataMap.get("建筑类别")){
			model.setBuildingType(dataMap.get("建筑类别"));
		}
		if(null != dataMap.get("产权年限")){
			model.setYearOfHousing(dataMap.get("产权年限"));
		}
		if(null != dataMap.get("产权性质")){
			model.setPropertyRights(dataMap.get("产权性质"));
		}
		if(null != dataMap.get("有无电梯")){
			model.setEquippedWithSubway(dataMap.get("有无电梯"));
		}
		return model;
	}
	
}
