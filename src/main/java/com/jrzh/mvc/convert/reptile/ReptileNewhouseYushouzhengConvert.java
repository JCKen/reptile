package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileNewhouseYushouzhengModel;
import com.jrzh.mvc.view.reptile.ReptileNewhouseYushouzhengView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileNewhouseYushouzhengConvert")
public class ReptileNewhouseYushouzhengConvert implements BaseConvertI<ReptileNewhouseYushouzhengModel, ReptileNewhouseYushouzhengView> {

	@Override
	public ReptileNewhouseYushouzhengModel addConvert(ReptileNewhouseYushouzhengView view) throws ProjectException {
		ReptileNewhouseYushouzhengModel model = new ReptileNewhouseYushouzhengModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileNewhouseYushouzhengModel editConvert(ReptileNewhouseYushouzhengView view, ReptileNewhouseYushouzhengModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileNewhouseYushouzhengView convertToView(ReptileNewhouseYushouzhengModel model) throws ProjectException {
		ReptileNewhouseYushouzhengView view = new ReptileNewhouseYushouzhengView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
