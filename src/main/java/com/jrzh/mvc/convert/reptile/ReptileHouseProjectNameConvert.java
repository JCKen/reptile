package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileHouseProjectNameModel;
import com.jrzh.mvc.view.reptile.ReptileHouseProjectNameView;

@Component("reptileHouseProjectNameConvert")
public class ReptileHouseProjectNameConvert implements BaseConvertI<ReptileHouseProjectNameModel, ReptileHouseProjectNameView> {

	@Override
	public ReptileHouseProjectNameModel addConvert(ReptileHouseProjectNameView view) throws ProjectException {
		ReptileHouseProjectNameModel model = new ReptileHouseProjectNameModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileHouseProjectNameModel editConvert(ReptileHouseProjectNameView view, ReptileHouseProjectNameModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileHouseProjectNameView convertToView(ReptileHouseProjectNameModel model) throws ProjectException {
		ReptileHouseProjectNameView view = new ReptileHouseProjectNameView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
