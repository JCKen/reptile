package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileNewhouseDealEverydayModel;
import com.jrzh.mvc.view.reptile.ReptileNewhouseDealEverydayView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileNewhouseDealEverydayConvert")
public class ReptileNewhouseDealEverydayConvert implements BaseConvertI<ReptileNewhouseDealEverydayModel, ReptileNewhouseDealEverydayView> {

	@Override
	public ReptileNewhouseDealEverydayModel addConvert(ReptileNewhouseDealEverydayView view) throws ProjectException {
		ReptileNewhouseDealEverydayModel model = new ReptileNewhouseDealEverydayModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileNewhouseDealEverydayModel editConvert(ReptileNewhouseDealEverydayView view, ReptileNewhouseDealEverydayModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileNewhouseDealEverydayView convertToView(ReptileNewhouseDealEverydayModel model) throws ProjectException {
		ReptileNewhouseDealEverydayView view = new ReptileNewhouseDealEverydayView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
