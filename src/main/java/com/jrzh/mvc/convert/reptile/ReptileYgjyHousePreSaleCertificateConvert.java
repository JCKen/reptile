package com.jrzh.mvc.convert.reptile;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileYgjyHousePreSaleCertificateModel;
import com.jrzh.mvc.view.reptile.ReptileYgjyHousePreSaleCertificateView;

@Component("reptileYgjyHousePreSaleCertificateConvert")
public class ReptileYgjyHousePreSaleCertificateConvert implements BaseConvertI<ReptileYgjyHousePreSaleCertificateModel, ReptileYgjyHousePreSaleCertificateView> {

	@Override
	public ReptileYgjyHousePreSaleCertificateModel addConvert(ReptileYgjyHousePreSaleCertificateView view) throws ProjectException {
		ReptileYgjyHousePreSaleCertificateModel model = new ReptileYgjyHousePreSaleCertificateModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileYgjyHousePreSaleCertificateModel editConvert(ReptileYgjyHousePreSaleCertificateView view, ReptileYgjyHousePreSaleCertificateModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileYgjyHousePreSaleCertificateView convertToView(ReptileYgjyHousePreSaleCertificateModel model) throws ProjectException {
		ReptileYgjyHousePreSaleCertificateView view = new ReptileYgjyHousePreSaleCertificateView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}
	
	public static ReptileYgjyHousePreSaleCertificateModel houseWorldMapToHouseModel(Map<String, String> dataMap){
		ReptileYgjyHousePreSaleCertificateModel model = new ReptileYgjyHousePreSaleCertificateModel();
		if(StringUtils.isNotBlank(dataMap.get("批准面积"))) {
			model.setTotalAreaReport(StringUtils.replace(dataMap.get("批准面积"), "平方米", ""));
		}
		if(StringUtils.isNotBlank(dataMap.get("商业套数"))) {
			model.setBusinessNumberSet(dataMap.get("商业套数"));
		}
		if(StringUtils.isNotBlank(dataMap.get("商业面积"))) {
			model.setBusinessArea(StringUtils.replace(dataMap.get("商业面积"), "平方米", ""));
		}
		if(StringUtils.isNotBlank(dataMap.get("住宅套数"))) {
			model.setHouseNumberSet(dataMap.get("住宅套数"));
		}
		if(StringUtils.isNotBlank(dataMap.get("住宅面积"))) {
			model.setHouseArea(StringUtils.replace(dataMap.get("住宅面积"), "平方米", ""));
		}
		return model;
	}

}
