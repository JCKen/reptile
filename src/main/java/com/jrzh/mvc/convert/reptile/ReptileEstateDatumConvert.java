package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileEstateDatumModel;
import com.jrzh.mvc.view.reptile.ReptileEstateDatumView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileEstateDatumConvert")
public class ReptileEstateDatumConvert implements BaseConvertI<ReptileEstateDatumModel, ReptileEstateDatumView> {

	@Override
	public ReptileEstateDatumModel addConvert(ReptileEstateDatumView view) throws ProjectException {
		ReptileEstateDatumModel model = new ReptileEstateDatumModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileEstateDatumModel editConvert(ReptileEstateDatumView view, ReptileEstateDatumModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileEstateDatumView convertToView(ReptileEstateDatumModel model) throws ProjectException {
		ReptileEstateDatumView view = new ReptileEstateDatumView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
