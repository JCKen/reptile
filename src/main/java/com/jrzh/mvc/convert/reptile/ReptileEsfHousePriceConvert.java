package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileEsfHousePriceModel;
import com.jrzh.mvc.view.reptile.ReptileEsfHousePriceView;

@Component("reptileEsfHousePriceConvert")
public class ReptileEsfHousePriceConvert implements BaseConvertI<ReptileEsfHousePriceModel, ReptileEsfHousePriceView> {

	@Override
	public ReptileEsfHousePriceModel addConvert(ReptileEsfHousePriceView view) throws ProjectException {
		ReptileEsfHousePriceModel model = new ReptileEsfHousePriceModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileEsfHousePriceModel editConvert(ReptileEsfHousePriceView view, ReptileEsfHousePriceModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileEsfHousePriceView convertToView(ReptileEsfHousePriceModel model) throws ProjectException {
		ReptileEsfHousePriceView view = new ReptileEsfHousePriceView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
