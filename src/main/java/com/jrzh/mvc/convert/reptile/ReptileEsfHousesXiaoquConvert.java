package com.jrzh.mvc.convert.reptile;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileEsfHousesXiaoquModel;
import com.jrzh.mvc.view.reptile.ReptileEsfHousesXiaoquView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileEsfHousesXiaoquConvert")
public class ReptileEsfHousesXiaoquConvert implements BaseConvertI<ReptileEsfHousesXiaoquModel, ReptileEsfHousesXiaoquView> {

	@Override
	public ReptileEsfHousesXiaoquModel addConvert(ReptileEsfHousesXiaoquView view) throws ProjectException {
		ReptileEsfHousesXiaoquModel model = new ReptileEsfHousesXiaoquModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileEsfHousesXiaoquModel editConvert(ReptileEsfHousesXiaoquView view, ReptileEsfHousesXiaoquModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileEsfHousesXiaoquView convertToView(ReptileEsfHousesXiaoquModel model) throws ProjectException {
		ReptileEsfHousesXiaoquView view = new ReptileEsfHousesXiaoquView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

	public static void convertBaseInfo(ReptileEsfHousesXiaoquModel model, Map<String, String> map) {
		for(String key : map.keySet()) {
			if(StringUtils.contains(key, " ")) {
				key = key.replace(" ", "");
			}
			if(StringUtils.contains(key,"别名")) {
				model.setHousesOtherName(map.get(key));
			}
			if(StringUtils.contains(key,"小区地址") || StringUtils.contains(key,"楼盘地址")) {
				model.setHousesAddress(map.get(key));
			}
			if(StringUtils.contains(key,"产权年限") || StringUtils.contains(key,"产权描述")) {
				model.setYearOfPropertyRights(map.get(key));
			}
			if(StringUtils.contains(key,"开发商")) {
				model.setDevelopers(map.get(key));
			}
			if(StringUtils.contains(key,"绿化率")) {
				model.setAfforestationRate(map.get(key));
			}
			if(StringUtils.contains(key,"容积率")) {
				model.setPlotRatio(map.get(key));
			}
			if(StringUtils.contains(key,"建筑类型") || StringUtils.contains(key,"建筑类别")) {
				if(!map.get(key).contains("无")) {
					model.setBuildingType(map.get(key));
				}
			}
			if(StringUtils.contains(key,"物业公司")) {
				model.setPropertyCompany(map.get(key));
			}
			if(StringUtils.contains(key,"房屋总数")) {
				model.setTotalHouseholds(map.get(key));
			}
			if(StringUtils.contains(key,"物业费")) {
				String costs = map.get(key);
				if(StringUtils.contains(costs," ")) {
					costs = costs.replace(" ", "");
				}
				if(StringUtils.contains(costs,"元")) {
					int index= costs.indexOf("元");
					costs = costs.substring(0,index);
				}
				model.setPropertyCosts(costs+"元/平米*月");
			}
			if(StringUtils.contains(key, "建筑面积")) {
				String value = map.get(key);
				if(StringUtils.contains(value, " ")) {
					value = value.replace(" ", "");
				}
				if(StringUtils.contains(value, "平")) {
					int index = value.indexOf("平");
					value = value.substring(0, index);
					model.setBuildingArea(value);
				}
			}
			if(StringUtils.contains(key, "占地面积")) {
				String value = map.get(key);
				if(StringUtils.contains(value, " ")) {
					value = value.replace(" ", "");
				}
				if(StringUtils.contains(value, "平")) {
					int index = value.indexOf("平");
					value = value.substring(0, index);
					model.setFloorArea(value);
				}
			}
			if(StringUtils.contains(key, "幼儿园")) {
				model.setKindergarten(map.get(key));
			}
			if(StringUtils.contains(key, "中小学")) {
				model.setSchool(map.get(key));
			}
			if(StringUtils.contains(key, "商场")) {
				model.setPowerCenter(map.get(key));
			}
			if(StringUtils.contains(key, "医院")) {
				model.setHospital(map.get(key));
			}
			if(StringUtils.contains(key, "银行")) {
				model.setBank(map.get(key));
			}
			if(StringUtils.contains(key, "邮局")) {
				model.setPost(map.get(key));
			}
			if(StringUtils.contains(key, "其他")) {
				model.setOther(map.get(key));
			}
			
		}
	}
}
