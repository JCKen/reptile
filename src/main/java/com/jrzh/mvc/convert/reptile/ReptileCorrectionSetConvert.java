package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileCorrectionSetModel;
import com.jrzh.mvc.view.reptile.ReptileCorrectionSetView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileCorrectionSetConvert")
public class ReptileCorrectionSetConvert implements BaseConvertI<ReptileCorrectionSetModel, ReptileCorrectionSetView> {

	@Override
	public ReptileCorrectionSetModel addConvert(ReptileCorrectionSetView view) throws ProjectException {
		ReptileCorrectionSetModel model = new ReptileCorrectionSetModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileCorrectionSetModel editConvert(ReptileCorrectionSetView view, ReptileCorrectionSetModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileCorrectionSetView convertToView(ReptileCorrectionSetModel model) throws ProjectException {
		ReptileCorrectionSetView view = new ReptileCorrectionSetView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
