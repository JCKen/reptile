package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileTemplateInfoModel;
import com.jrzh.mvc.view.reptile.ReptileTemplateInfoView;

@Component("reptileTemplateInfoConvert")
public class ReptileTemplateInfoConvert implements BaseConvertI<ReptileTemplateInfoModel, ReptileTemplateInfoView> {

	@Override
	public ReptileTemplateInfoModel addConvert(ReptileTemplateInfoView view) throws ProjectException {
		ReptileTemplateInfoModel model = new ReptileTemplateInfoModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileTemplateInfoModel editConvert(ReptileTemplateInfoView view, ReptileTemplateInfoModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileTemplateInfoView convertToView(ReptileTemplateInfoModel model) throws ProjectException {
		ReptileTemplateInfoView view = new ReptileTemplateInfoView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
