package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileHousePriceModel;
import com.jrzh.mvc.view.reptile.ReptileHousePriceView;

@Component("reptileHousePriceConvert")
public class ReptileHousePriceConvert implements BaseConvertI<ReptileHousePriceModel, ReptileHousePriceView> {

	@Override
	public ReptileHousePriceModel addConvert(ReptileHousePriceView view) throws ProjectException {
		ReptileHousePriceModel model = new ReptileHousePriceModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileHousePriceModel editConvert(ReptileHousePriceView view, ReptileHousePriceModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileHousePriceView convertToView(ReptileHousePriceModel model) throws ProjectException {
		ReptileHousePriceView view = new ReptileHousePriceView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
