package com.jrzh.mvc.convert.reptile;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseInfoModel;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseInfoView;

@Component("reptileYgjyHouseInfoConvert")
public class ReptileYgjyHouseInfoConvert implements BaseConvertI<ReptileYgjyHouseInfoModel, ReptileYgjyHouseInfoView> {

	@Override
	public ReptileYgjyHouseInfoModel addConvert(ReptileYgjyHouseInfoView view) throws ProjectException {
		ReptileYgjyHouseInfoModel model = new ReptileYgjyHouseInfoModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileYgjyHouseInfoModel editConvert(ReptileYgjyHouseInfoView view, ReptileYgjyHouseInfoModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileYgjyHouseInfoView convertToView(ReptileYgjyHouseInfoModel model) throws ProjectException {
		ReptileYgjyHouseInfoView view = new ReptileYgjyHouseInfoView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

	
	public static ReptileYgjyHouseInfoModel houseWorldMapToHouseModel(Map<String, String> dataMap){
		ReptileYgjyHouseInfoModel model = new ReptileYgjyHouseInfoModel();
		if(StringUtils.isNotBlank(dataMap.get("所在区域"))) {
			model.setAdministrativeArea(dataMap.get("所在区域"));
		}
		if(StringUtils.isNotBlank(dataMap.get("预售总面积"))) {
			model.setLandArea(Double.valueOf(dataMap.get("预售总面积")));
		}
		if(StringUtils.isNotBlank(dataMap.get("宗地位置"))) {
			model.setProjectAddress(dataMap.get("宗地位置"));
		}
		if(StringUtils.isNotBlank(dataMap.get("预售总套数"))) {
			model.setSumUnsold(Double.valueOf(dataMap.get("预售总套数")));
		}
		return model;
	}
}
