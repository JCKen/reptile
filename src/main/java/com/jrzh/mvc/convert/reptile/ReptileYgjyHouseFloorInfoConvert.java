package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseFloorInfoModel;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseFloorInfoView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileYgjyHouseFloorInfoConvert")
public class ReptileYgjyHouseFloorInfoConvert implements BaseConvertI<ReptileYgjyHouseFloorInfoModel, ReptileYgjyHouseFloorInfoView> {

	@Override
	public ReptileYgjyHouseFloorInfoModel addConvert(ReptileYgjyHouseFloorInfoView view) throws ProjectException {
		ReptileYgjyHouseFloorInfoModel model = new ReptileYgjyHouseFloorInfoModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileYgjyHouseFloorInfoModel editConvert(ReptileYgjyHouseFloorInfoView view, ReptileYgjyHouseFloorInfoModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileYgjyHouseFloorInfoView convertToView(ReptileYgjyHouseFloorInfoModel model) throws ProjectException {
		ReptileYgjyHouseFloorInfoView view = new ReptileYgjyHouseFloorInfoView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
