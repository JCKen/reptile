package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileSaleInfoModel;
import com.jrzh.mvc.view.reptile.ReptileSaleInfoView;

@Component("reptileSaleInfoConvert")
public class ReptileSaleInfoConvert implements BaseConvertI<ReptileSaleInfoModel, ReptileSaleInfoView> {

	@Override
	public ReptileSaleInfoModel addConvert(ReptileSaleInfoView view) throws ProjectException {
		ReptileSaleInfoModel model = new ReptileSaleInfoModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileSaleInfoModel editConvert(ReptileSaleInfoView view, ReptileSaleInfoModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileSaleInfoView convertToView(ReptileSaleInfoModel model) throws ProjectException {
		ReptileSaleInfoView view = new ReptileSaleInfoView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
