package com.jrzh.mvc.convert.reptile;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseInfoModel;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseRoomInfoModel;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseRoomInfoView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileYgjyHouseRoomInfoConvert")
public class ReptileYgjyHouseRoomInfoConvert implements BaseConvertI<ReptileYgjyHouseRoomInfoModel, ReptileYgjyHouseRoomInfoView> {

	@Override
	public ReptileYgjyHouseRoomInfoModel addConvert(ReptileYgjyHouseRoomInfoView view) throws ProjectException {
		ReptileYgjyHouseRoomInfoModel model = new ReptileYgjyHouseRoomInfoModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileYgjyHouseRoomInfoModel editConvert(ReptileYgjyHouseRoomInfoView view, ReptileYgjyHouseRoomInfoModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileYgjyHouseRoomInfoView convertToView(ReptileYgjyHouseRoomInfoModel model) throws ProjectException {
		ReptileYgjyHouseRoomInfoView view = new ReptileYgjyHouseRoomInfoView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

	public static ReptileYgjyHouseRoomInfoModel houseWorldMapToHouseModel(Map<String, String> dataMap){
		ReptileYgjyHouseRoomInfoModel model = new ReptileYgjyHouseRoomInfoModel();
		if(StringUtils.isNotBlank(dataMap.get("户型"))) {
			model.setRoomApartment(dataMap.get("户型"));
		}
		if(StringUtils.isNotBlank(dataMap.get("楼层"))) {
			String louCeng = StringUtils.replace(dataMap.get("楼层"), "地下", "");
			model.setLouCeng(louCeng);
		}
		if(StringUtils.isNotBlank(dataMap.get("建筑面积"))) {
			String area = StringUtils.replace(dataMap.get("建筑面积"), "平方米", "");
			model.setRoomTotalArea(area);
		}
		if(StringUtils.isNotBlank(dataMap.get("房号"))) {
			model.setRoomNumber(dataMap.get("房号"));
		}
		return model;
	}
	
	
	public static ReptileYgjyHouseRoomInfoModel SZhouseWorldMapToHouseModel(Map<String, String> dataMap){
		ReptileYgjyHouseRoomInfoModel model = new ReptileYgjyHouseRoomInfoModel();
		if(StringUtils.isNotBlank(dataMap.get("房 间 号"))) {
			model.setRoomApartment(dataMap.get("房 间 号"));
		}
		if(StringUtils.isNotBlank(dataMap.get("户　　型"))) {
			model.setLouCeng(dataMap.get("户　　型"));
		}
		if(StringUtils.isNotBlank(dataMap.get("建筑面积"))) {
			model.setRoomTotalArea(dataMap.get("建筑面积"));
		}
		if(StringUtils.isNotBlank(dataMap.get("套内面积"))) {
			model.setRoomRealArea(dataMap.get("套内面积"));
		}
		if(StringUtils.isNotBlank(dataMap.get("按建筑面积拟售单价"))) {
			model.setRoomPrice(dataMap.get("按建筑面积拟售单价"));
		}
		if(StringUtils.isNotBlank(dataMap.get("按套内面积拟售单价"))) {
			model.setRoomRealPrice(dataMap.get("按套内面积拟售单价"));
		}
		if(StringUtils.isNotBlank(dataMap.get("建筑面积(m2)"))) {
			model.setRoomTotalArea(dataMap.get("建筑面积(m2)"));
		}
		if(StringUtils.isNotBlank(dataMap.get("套内面积(m2)"))) {
			model.setRoomRealArea(dataMap.get("套内面积(m2)"));
		}
		if(StringUtils.isNotBlank(dataMap.get("住宅建筑面积"))) {
			model.setRoomTotalArea(dataMap.get("住宅建筑面积"));
		}
		if(StringUtils.isNotBlank(dataMap.get("住宅套内面积"))) {
			model.setRoomRealArea(dataMap.get("住宅套内面积"));
		}
		return model;
	}
}
