package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileCommodityHouseInfoModel;
import com.jrzh.mvc.view.reptile.ReptileCommodityHouseInfoView;

@Component("reptileCommodityHouseInfoConvert")
public class ReptileCommodityHouseInfoConvert implements BaseConvertI<ReptileCommodityHouseInfoModel, ReptileCommodityHouseInfoView> {

	@Override
	public ReptileCommodityHouseInfoModel addConvert(ReptileCommodityHouseInfoView view) throws ProjectException {
		ReptileCommodityHouseInfoModel model = new ReptileCommodityHouseInfoModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileCommodityHouseInfoModel editConvert(ReptileCommodityHouseInfoView view, ReptileCommodityHouseInfoModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileCommodityHouseInfoView convertToView(ReptileCommodityHouseInfoModel model) throws ProjectException {
		ReptileCommodityHouseInfoView view = new ReptileCommodityHouseInfoView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
