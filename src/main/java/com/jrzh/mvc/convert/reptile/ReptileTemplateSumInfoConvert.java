package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileTemplateSumInfoModel;
import com.jrzh.mvc.view.reptile.ReptileTemplateSumInfoView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileTemplateSumInfoConvert")
public class ReptileTemplateSumInfoConvert implements BaseConvertI<ReptileTemplateSumInfoModel, ReptileTemplateSumInfoView> {

	@Override
	public ReptileTemplateSumInfoModel addConvert(ReptileTemplateSumInfoView view) throws ProjectException {
		ReptileTemplateSumInfoModel model = new ReptileTemplateSumInfoModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileTemplateSumInfoModel editConvert(ReptileTemplateSumInfoView view, ReptileTemplateSumInfoModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileTemplateSumInfoView convertToView(ReptileTemplateSumInfoModel model) throws ProjectException {
		ReptileTemplateSumInfoView view = new ReptileTemplateSumInfoView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
