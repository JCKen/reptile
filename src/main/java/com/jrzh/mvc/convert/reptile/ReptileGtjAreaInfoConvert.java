package com.jrzh.mvc.convert.reptile;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;
import com.jrzh.mvc.view.reptile.ReptileGtjAreaInfoView;
import com.jrzh.mvc.view.reptile.shanghaiAreaBean.AreaInfoData;

@Component("reptileGtjAreaInfoConvert")
public class ReptileGtjAreaInfoConvert implements BaseConvertI<ReptileGtjAreaInfoModel, ReptileGtjAreaInfoView> {

	private final static Pattern ADDRESS_PATTEN = Pattern.compile("^[\u4e00-\u9fa5]+");

	private final static Pattern BEIJING_AREA_NO_PATTEN = Pattern
			.compile("[a-z,A-Z,0-9]{2,4}(-[a-z,A-Z,0-9]{1,4}){1,2}");

	@Override
	public ReptileGtjAreaInfoModel addConvert(ReptileGtjAreaInfoView view) throws ProjectException {
		ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileGtjAreaInfoModel editConvert(ReptileGtjAreaInfoView view, ReptileGtjAreaInfoModel model)
			throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[] { "createBy", "createTime" });
		return model;
	}

	@Override
	public ReptileGtjAreaInfoView convertToView(ReptileGtjAreaInfoModel model) throws ProjectException {
		ReptileGtjAreaInfoView view = new ReptileGtjAreaInfoView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

	public ReptileGtjAreaInfoModel mapToGtjAreaInfoModel(Map<String, String> dataMap) throws ProjectException {
		ReptileGtjAreaInfoModel infoModel = new ReptileGtjAreaInfoModel();
		if (null != dataMap.get("地块编号")) {
			infoModel.setAreaNo(dataMap.get("地块编号"));
		}
		if (null != dataMap.get("土地位置")) {
			infoModel.setAddress(dataMap.get("土地位置"));
			;
		}
		if (null != dataMap.get("土地用途")) {
			infoModel.setUse(dataMap.get("土地用途"));
		}
		if (null != dataMap.get("土地面积（平方米）")) {
			infoModel.setAcreage(dataMap.get("土地面积（平方米）"));
		}
		if (null != dataMap.get("成交价（万元）")) {
			infoModel.setPrice(dataMap.get("成交价（万元）"));
		}
		if (null != dataMap.get("竞得人")) {
			infoModel.setBuyer(dataMap.get("竞得人"));
		}
		return infoModel;
	}

	public ReptileGtjAreaInfoModel SHBeanToInfoModel(AreaInfoData bean) {
		ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
		model.setBuyer(bean.getZongbr());
		String price = bean.getZongbj();
		Double temp = Double.parseDouble(price)*10000;
		model.setPrice(new BigDecimal(temp).toPlainString());
		if (StringUtils.isNotBlank(bean.getDikmc())) {
			Matcher matcher = ADDRESS_PATTEN.matcher(bean.getDikmc());
			if (matcher.find()) {
				model.setAddress(matcher.group());
			}
		}
		model.setAreaNo(bean.getDikmc());
		model.setAcreage(bean.getMianj());
		model.setUse(bean.getGuihyt());
		return model;
	}

	public ReptileGtjAreaInfoModel BJmapToModel(Map<String, String> dataMap, String url) {
		ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
		if (StringUtils.isNotBlank(dataMap.get("宗地面积：")))
			model.setAcreage(dataMap.get("宗地面积："));
		if (StringUtils.isNotBlank(dataMap.get("土地位置："))) {
			String address = dataMap.get("土地位置：");
			Matcher m = ADDRESS_PATTEN.matcher(address);
			if (m.find())
				model.setAddress(m.group());
			m = BEIJING_AREA_NO_PATTEN.matcher(address);
			if (m.find())
				model.setAreaNo(m.group());
		}
		if (StringUtils.isNotBlank(dataMap.get("规划用途：")))
			model.setUse(dataMap.get("规划用途："));
		if (StringUtils.isNotBlank(dataMap.get("容积率（地上）：")))
			model.setPlotRatio(ReptileGtjAreaInfoConvert.changVal(dataMap.get("容积率（地上）：")));
		if (StringUtils.isNotBlank(dataMap.get("土地成交价："))){
			String price = dataMap.get("土地成交价：");
			String regEx = "[^0-9.]"; //提取数字和.
			Pattern result = Pattern.compile(regEx);
			model.setPrice(result.matcher(price).replaceAll("").trim());
		}
		if (StringUtils.isNotBlank(dataMap.get("受让方名称：")))
			model.setBuyer(dataMap.get("受让方名称："));
		if (StringUtils.isNotBlank(dataMap.get("签约时间："))) {
			Date time = DateUtil.format(dataMap.get("签约时间："), "YYYY年MM月DD日");
			model.setTime(time);
		}
		model.setUrl(url);
		model.setCity("北京");
		model.setFrom("北京市规划和自然资源委员会");
		return model;
	}

	public ReptileGtjAreaInfoModel gZmapToModel(Map<String, String> dataMap) {
		ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
		if (StringUtils.isBlank(dataMap.get("地块编号")))
			return null;
		model.setAreaNo(dataMap.get("地块编号"));
		if (StringUtils.isNotBlank(dataMap.get("土地位置"))) {
			String address = dataMap.get("土地位置");
			Matcher m = ADDRESS_PATTEN.matcher(address);
			if (m.find())
				model.setAddress(m.group());
		}
		if (StringUtils.isNotBlank(dataMap.get("土地用途"))) {
			model.setUse(dataMap.get("土地用途"));
		}
		if (StringUtils.isNotBlank(dataMap.get("土地面积（平方米）"))) {
			String areage = dataMap.get("土地面积（平方米）");
			if(areage.indexOf("（")>-1){
				areage = areage.substring(0, areage.indexOf("（"));
			}
			if(areage.indexOf("(")>-1){
				areage = areage.substring(0, areage.indexOf("("));
			}
			model.setAcreage(dataMap.get("土地面积（平方米）"));
		}
		if (StringUtils.isNotBlank(dataMap.get("成交价 （万元）"))) {
			if(StringUtils.isBlank(dataMap.get("成交价 （万元）"))||StringUtils.equals("无", dataMap.get("成交价 （万元）"))){
				model = null;
				return model;
			}
			model.setPrice(dataMap.get("成交价 （万元）"));
		}
		if (StringUtils.isNotBlank(dataMap.get("竞得人"))) {
			if(StringUtils.isBlank(dataMap.get("竞得人"))||StringUtils.equals("无", dataMap.get("竞得人"))){
				model = null;
				return model;
			}
			model.setBuyer(dataMap.get("竞得人"));
		}
		if (StringUtils.isNotBlank(dataMap.get("受让单位"))) {
			if(StringUtils.isBlank(dataMap.get("受让单位"))||StringUtils.equals("无", dataMap.get("受让单位"))){
				model = null;
				return model;
			}
			model.setBuyer(dataMap.get("受让单位"));
		}
		return model;
	}

	// 江门土地
	public ReptileGtjAreaInfoModel JMmapToModel(Map<String, String> dataMap) {
		ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
		if (StringUtils.isBlank(dataMap.get("地块编号"))&&StringUtils.isBlank(dataMap.get("宗地编号")))
			return null;
		String areaNo = dataMap.get("地块编号");
		if(areaNo==null||StringUtils.equals("",areaNo)){
			areaNo = dataMap.get("宗地编号");
		}
		if(areaNo==null||StringUtils.equals("",areaNo)) return null;
		model.setAreaNo(areaNo);
		
		if (StringUtils.isNotBlank(dataMap.get("地块位置"))) {
			String address = dataMap.get("地块位置");
			Matcher m = ADDRESS_PATTEN.matcher(address);
			if (m.find())
				model.setAddress(m.group());
		}
		if (StringUtils.isNotBlank(dataMap.get("土地用途"))) {
			model.setUse(dataMap.get("土地用途"));
		}
		if (StringUtils.isNotBlank(dataMap.get("租赁用途"))||StringUtils.isNotBlank(dataMap.get("租赁面积"))) {
			return null;
		}
		if (StringUtils.isNotBlank(dataMap.get("土地面积（㎡）"))) {
			String areage = dataMap.get("土地面积（㎡）");
			if(areage.indexOf("（")>-1){
				areage = areage.substring(0, areage.indexOf("（"));
			}
			if(areage.indexOf("(")>-1){
				areage = areage.substring(0, areage.indexOf("("));
			}
			model.setAcreage(areage);
		}
		if (StringUtils.isNotBlank(dataMap.get("成交价(万元)"))) {
			if(StringUtils.isBlank(dataMap.get("成交价(万元)"))||StringUtils.equals("无", dataMap.get("成交价(万元)"))||StringUtils.equals("", dataMap.get("成交价(万元)"))){
				model = null;
				return model;
			}
			model.setPrice(dataMap.get("成交价(万元)"));
		}
		if (StringUtils.isNotBlank(dataMap.get("成交价"))) {
			String price = dataMap.get("成交价");
			if(price.indexOf("万元")>0){
				price = price.substring(0,price.indexOf("万元"));
			}
			model.setPrice(price);
		}
		if (StringUtils.isNotBlank(dataMap.get("出让结果："))) {
			return null;
		}
		if (StringUtils.isBlank(dataMap.get("受让单位"))) {
			return null;
		}
		if (StringUtils.isNotBlank(dataMap.get("受让单位"))) {
			if(StringUtils.isBlank(dataMap.get("受让单位"))||StringUtils.equals("无", dataMap.get("受让单位"))||StringUtils.equals("", dataMap.get("受让单位"))){
				model = null;
				return model;
			}
			model.setBuyer(dataMap.get("受让单位"));
		}
		return model;
	}
	// 处理容积率
	public static String changVal(String str) {
		String regEx = "[^0-9.]"; // 提取数字和.
		Pattern result = Pattern.compile(regEx);
		String val = result.matcher(str).replaceAll("").trim();
		if (StringUtils.equals("0.0", val)) {
			return null;
		}
		String temp = val;
		int num = 0;
		while (temp.contains(".")) {
			temp = temp.substring(temp.indexOf(".") + ".".length());
			num++;
		}
		if (num == 2) {
			String str1 = val.substring(0, val.length() / 2);
			String str2 = val.substring(val.length() / 2);
			Double sug = (Double.parseDouble(str1) + Double.parseDouble(str2)) / 2;
			val = sug.toString();
			int index = val.indexOf(".");
			try {
				val = val.substring(0, index + 3);
			} catch (Exception e) {
				val = val.substring(0, index + 2);
			}
			return val;
		} else {
			return val;
		}

	}
	// 获取惠州土地数据
	public ReptileGtjAreaInfoModel HzmapToModel(Map<String, String> map) {
		
		ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
		if(StringUtils.isNotBlank(map.get("地块规划编号:"))){
			model.setAreaNo(map.get("地块规划编号:"));
		}
		if(StringUtils.isNotBlank(map.get("土地位置:"))){
			model.setAddress(map.get("土地位置:"));
		}
		if(StringUtils.isNotBlank(map.get("计算指标用地面积(m²):"))){
			model.setAcreage(map.get("计算指标用地面积(m²):"));
		}
		if(StringUtils.isNotBlank(map.get("土地用途:"))){
			model.setUse(map.get("土地用途:"));
		}
		if(StringUtils.isNotBlank(map.get("容积率:"))){
			model.setPlotRatio(map.get("容积率:"));
		}
		return model;
	}
	
	// 昆山
	public ReptileGtjAreaInfoModel getKsMap(Map<String, String> map) {
		ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
		if(StringUtils.isNotBlank(map.get("项目位置:"))){
			model.setAddress(map.get("项目位置:"));
		}
		if(StringUtils.isNotBlank(map.get("项目名称:"))){
			model.setBuyer(map.get("项目名称:"));
		}
		if(StringUtils.isNotBlank(map.get("土地用途:"))){
			model.setUse(map.get("土地用途:"));
		}
		if(StringUtils.isNotBlank(map.get("成交价格(万元):"))){
			model.setPrice(map.get("成交价格(万元):"));
		}else{
			return null;
		}
		if(StringUtils.isNotBlank(map.get("供应面积(公顷):"))){
			String areage = map.get("供应面积(公顷):");
			Double temp = Double.parseDouble(areage);
			temp = temp*10000;
			model.setAcreage(temp.toString());
		}
		model.setAreaNo("KS-"+map.get("合同签订日期:"));
		model.setCity("昆山");
		model.setFrom("中华人民共和国自然资源部");
		if(StringUtils.isNotBlank(map.get("下限:"))||StringUtils.isNotBlank(map.get("上限:"))){
			if(StringUtils.isNotBlank(map.get("下限:"))&&StringUtils.isNotBlank(map.get("上限:"))){
				Double gt = Double.parseDouble(map.get("下限:"));
				Double lt = Double.parseDouble(map.get("上限:"));
				gt = (gt + lt)/2;
				model.setPlotRatio(gt.toString());
				return model;
			}
			if(StringUtils.isNotBlank(map.get("下限:"))){
				model.setPlotRatio(map.get("下限:"));
			}
			if(StringUtils.isNotBlank(map.get("上限:"))){
				model.setPlotRatio(map.get("上限:"));
			}
			return model;
		}
		return model;
	}

}
