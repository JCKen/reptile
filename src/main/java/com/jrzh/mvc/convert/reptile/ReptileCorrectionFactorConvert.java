package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileCorrectionFactorModel;
import com.jrzh.mvc.view.reptile.ReptileCorrectionFactorView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileCorrectionFactorConvert")
public class ReptileCorrectionFactorConvert implements BaseConvertI<ReptileCorrectionFactorModel, ReptileCorrectionFactorView> {

	@Override
	public ReptileCorrectionFactorModel addConvert(ReptileCorrectionFactorView view) throws ProjectException {
		ReptileCorrectionFactorModel model = new ReptileCorrectionFactorModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileCorrectionFactorModel editConvert(ReptileCorrectionFactorView view, ReptileCorrectionFactorModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileCorrectionFactorView convertToView(ReptileCorrectionFactorModel model) throws ProjectException {
		ReptileCorrectionFactorView view = new ReptileCorrectionFactorView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
