package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileAreaModel;
import com.jrzh.mvc.view.reptile.ReptileAreaView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileAreaConvert")
public class ReptileAreaConvert implements BaseConvertI<ReptileAreaModel, ReptileAreaView> {

	@Override
	public ReptileAreaModel addConvert(ReptileAreaView view) throws ProjectException {
		ReptileAreaModel model = new ReptileAreaModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileAreaModel editConvert(ReptileAreaView view, ReptileAreaModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileAreaView convertToView(ReptileAreaModel model) throws ProjectException {
		ReptileAreaView view = new ReptileAreaView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

	
}
