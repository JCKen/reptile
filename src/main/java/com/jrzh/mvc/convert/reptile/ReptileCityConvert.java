package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileCityModel;
import com.jrzh.mvc.view.reptile.ReptileCityView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileCityConvert")
public class ReptileCityConvert implements BaseConvertI<ReptileCityModel, ReptileCityView> {

	@Override
	public ReptileCityModel addConvert(ReptileCityView view) throws ProjectException {
		ReptileCityModel model = new ReptileCityModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileCityModel editConvert(ReptileCityView view, ReptileCityModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileCityView convertToView(ReptileCityModel model) throws ProjectException {
		ReptileCityView view = new ReptileCityView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
