package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileYgjySaleInfoModel;
import com.jrzh.mvc.view.reptile.ReptileYgjySaleInfoView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileYgjySaleInfoConvert")
public class ReptileYgjySaleInfoConvert implements BaseConvertI<ReptileYgjySaleInfoModel, ReptileYgjySaleInfoView> {

	@Override
	public ReptileYgjySaleInfoModel addConvert(ReptileYgjySaleInfoView view) throws ProjectException {
		ReptileYgjySaleInfoModel model = new ReptileYgjySaleInfoModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileYgjySaleInfoModel editConvert(ReptileYgjySaleInfoView view, ReptileYgjySaleInfoModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileYgjySaleInfoView convertToView(ReptileYgjySaleInfoModel model) throws ProjectException {
		ReptileYgjySaleInfoView view = new ReptileYgjySaleInfoView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
