package com.jrzh.mvc.convert.reptile;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.mvc.model.reptile.ReptileNewHouseModel;
import com.jrzh.mvc.view.reptile.ReptileNewHouseView;
import com.jrzh.util.ChromeOptionsUtils;

public class ReptileNewHouseConvert implements BaseConvertI<ReptileNewHouseModel, ReptileNewHouseView> {

	@Override
	public ReptileNewHouseModel addConvert(ReptileNewHouseView view) throws ProjectException {
		ReptileNewHouseModel model = new ReptileNewHouseModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileNewHouseModel editConvert(ReptileNewHouseView view, ReptileNewHouseModel model)
			throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[] { "createBy", "createTime" });
		return model;
	}

	@Override
	public ReptileNewHouseView convertToView(ReptileNewHouseModel model) throws ProjectException {
		ReptileNewHouseView view = new ReptileNewHouseView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

	public static ReptileNewHouseModel mapToHouseModel(Map<String, String> map) throws ProjectException {
		ReptileNewHouseModel model = new ReptileNewHouseModel();
		for (String key : map.keySet()) {
			if (StringUtils.contains(key, " ")) {
				key = key.replace(" ", "");
			}
			if (StringUtils.contains(key, "项目地址") || StringUtils.contains(key, "楼盘地址")) {
				model.setHouseAddress(map.get(key));
			}
			if (StringUtils.contains(key, "售楼")) {
				model.setSalesOfficeAddress(map.get(key));
			}
			if (StringUtils.contains(key, "开发商")) {
				model.setDevelopers(map.get(key));
			}
			if (StringUtils.contains(key, "物业公司")) {
				model.setPropertyCompany(map.get(key));
			}
			if (StringUtils.contains(key, "最新开盘")) {
				model.setLatestSaleTime(map.get(key));
			}
			if (StringUtils.contains(key, "容积率")) {
				model.setPlotRatio(map.get(key));
			}
			if (StringUtils.contains(key, "产权年限")) {
				model.setPropertyRightYears(map.get(key));
			}
			if (StringUtils.contains(key, "绿化率")) {
				model.setAfforestationRate(map.get(key));
			}
			if (StringUtils.contains(key, "规划户数")) {
				int index = map.get(key).indexOf("户");
				if (index != -1) {
					model.setPlanningHouseholds(map.get(key).substring(0, index));
				} else {
					model.setPlanningHouseholds(map.get(key));
				}
			}
			if (StringUtils.contains(key, "物业费用")) {
				model.setPropertyCosts(map.get(key).split("元")[0]);
			}
			if (StringUtils.contains(key, "车位情况")) {
				model.setParkingLot(map.get(key));
			}
			if (StringUtils.contains(key, "供水方式")) {
				model.setWaterSupplyMode(map.get(key));
			}
			if (StringUtils.contains(key, "供电方式")) {
				model.setPowerSupplyMode(map.get(key));
			}
			if (StringUtils.contains(key, "占地面积")) {
				String area = map.get(key);
				if (StringUtils.contains(area, ",")) {
					area = area.replace(",", "");
				}
				if (StringUtils.contains(area, "㎡")) {
					area = area.replace("㎡", "");
				}
				if (StringUtils.contains(area, "平")) {
					int index = area.indexOf("平");
					area = area.substring(0, index);
				}
				model.setAreaCovered(area);
			}
			if (StringUtils.contains(key, "建筑面积")) {
				String area = map.get(key);
				if (StringUtils.contains(area, ",")) {
					area = area.replace(",", "");
				}
				if (StringUtils.contains(area, "㎡")) {
					area = area.replace("㎡", "");
				}
				if (StringUtils.contains(area, "平")) {
					int index = area.indexOf("平");
					area = area.substring(0, index);
				}
				model.setBuildingArea(area);
			}
			if (StringUtils.contains(key, "交房时间")) {
				model.setTimeOfDelivery(map.get(key));
			}
			if (StringUtils.contains(key, "物业类型") || StringUtils.contains(key, "物业类别")) {
				String type = map.get(key);
				String houseType = map.get(key);
				if (StringUtils.contains(type, "商")) {
					houseType = "商铺";
				}
				if (StringUtils.contains(type, "写")) {
					houseType = "写字楼";
				}
				if (StringUtils.contains(type, "公寓")) {
					houseType = "住宅";
				}
				if (StringUtils.contains(type, "限竞房")) {
					houseType = "住宅";
				}
				if (StringUtils.contains(type, "平层")) {
					houseType = "住宅";
				}
				if (StringUtils.contains(type, "公租房")) {
					houseType = "住宅";
				}
				if (StringUtils.contains(type, "共有产权房")) {
					houseType = "住宅";
				}
				if (StringUtils.contains(type, "住宅")) {
					houseType = "住宅";
				}
				if (StringUtils.contains(type, "别墅")) {
					houseType = "别墅";
				}
				if (StringUtils.contains(type, "洋房")) {
					houseType = "别墅";
				}
				if (StringUtils.contains(type, "联排")) {
					houseType = "别墅";
				}
				if (StringUtils.contains(type, "叠拼")) {
					houseType = "别墅";
				}
				if (StringUtils.contains(type, "双拼")) {
					houseType = "别墅";
				}
				model.setHouseType(houseType);
			}
			if (StringUtils.contains(key, "交易权属")) {
				model.setHouseTradingAuthority(map.get(key));
			}
			if (StringUtils.equals(key, "houseUrlDetail") && null != map.get(key)) {
				ChromeDriverService service = ChromeOptionsUtils.getChromeDriverService();
		        try {
					service.start();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		        WebDriver driver = new RemoteWebDriver(service.getUrl(), DesiredCapabilities.chrome());
				try {
					String aa = null;
					Document zhouBian;
					try {
						driver.get(map.get(key));
						Thread.sleep(2000);
						aa = driver.getPageSource();
						zhouBian = Jsoup.parse(aa);
					} catch (Exception e) {
						return model;
					}
					if (null == zhouBian)
						return model;

					// 周边信息
					Element ele = zhouBian.selectFirst("#around_txt");
					if (null == ele)
						return model;
					Elements sheShiEle = ele.select("div");
					if (null == sheShiEle)
						return model;
					for (Element ss : sheShiEle) {
						String info = ss.text();
						if (StringUtils.contains(info, "地铁：") && StringUtils.contains(info, "其他：")) {
							continue;
						}
						if (StringUtils.contains(info, "地铁：")) {
							Element s = ss.selectFirst(".ret");
							if (null == s)
								continue;
							model.setTraffic(s.text());
						}
						if (StringUtils.contains(info, "教育设施：")) {
							Element s = ss.selectFirst(".ret");
							if (null == s)
								continue;
							model.setSchool(s.text());
						}
						if (StringUtils.contains(info, "医院：")) {
							Element s = ss.selectFirst(".ret");
							if (null == s)
								continue;
							model.setHospital(s.text());
						}
						if (StringUtils.contains(info, "购物：")) {
							Element s = ss.selectFirst(".ret");
							if (null == s)
								continue;
							model.setPowerCenter(s.text());
						}
						if (StringUtils.contains(info, "公园：")) {
							Element s = ss.selectFirst(".ret");
							if (null == s)
								continue;
							model.setOther(s.text());
						}
						if (StringUtils.contains(info, "银行：")) {
							Element s = ss.selectFirst(".ret");
							if (null == s)
								continue;
							model.setBank(s.text());
						}
						if (StringUtils.contains(info, "其他：")) {
							Element s = ss.selectFirst(".ret");
							if (null == s)
								continue;
							model.setOther(s.text());
						}
					}
					System.out.println(driver);
					if (driver != null) {
						driver.quit();
						service.stop();
					}
				} catch (Exception e) {
					e.printStackTrace();
					if (driver != null) {
						driver.quit();
						service.stop();
					
					}
				}
			}
		}
		return model;
	}

	public static ReptileNewHouseModel houseWorldMapToHouseModel(Map<String, String> map) {
		ReptileNewHouseModel model = new ReptileNewHouseModel();
		for (String key : map.keySet()) {
			if (StringUtils.contains(key, " ")) {
				key = key.replace(" ", "");
			}
			if (StringUtils.contains(key, "楼盘地址") || StringUtils.contains(key, "项目地址")) {
				model.setHouseAddress(map.get(key));
			}
			if (StringUtils.contains(key, "售楼")) {
				model.setSalesOfficeAddress(map.get(key));
			}
			if (StringUtils.contains(key, "开发商")) {
				model.setDevelopers(map.get(key));
			}
			if (StringUtils.contains(key, "物业公司")) {
				model.setPropertyCompany(map.get(key));
			}
			if (StringUtils.contains(key, "开盘时间")) {
				model.setLatestSaleTime(map.get(key));
			}
			if (StringUtils.contains(key, "容积率")) {
				model.setPlotRatio(map.get(key));
			}
			if (StringUtils.contains(key, "产权年限")) {
				model.setPropertyRightYears(map.get(key));
			}
			if (StringUtils.contains(key, "绿化率")) {
				model.setAfforestationRate(map.get(key));
			}
			if (StringUtils.contains(key, "总户数")) {
				int index = map.get(key).indexOf("户");
				if (index != -1) {
					model.setPlanningHouseholds(map.get(key).substring(0, index));
				} else {
					model.setPlanningHouseholds(map.get(key));
				}
			}
			if (StringUtils.contains(key, "物业类型") || StringUtils.contains(key, "物业类别")) {
				String type = map.get(key);
				String houseType = map.get(key);
				if (StringUtils.contains(type, "商")) {
					houseType = "商铺";
				}
				if (StringUtils.contains(type, "写")) {
					houseType = "写字楼";
				}
				if (StringUtils.contains(type, "公寓")) {
					houseType = "住宅";
				}
				if (StringUtils.contains(type, "限竞房")) {
					houseType = "住宅";
				}
				if (StringUtils.contains(type, "平层")) {
					houseType = "住宅";
				}
				if (StringUtils.contains(type, "公租房")) {
					houseType = "住宅";
				}
				if (StringUtils.contains(type, "共有产权房")) {
					houseType = "住宅";
				}
				if (StringUtils.contains(type, "住宅")) {
					houseType = "住宅";
				}
				if (StringUtils.contains(type, "别墅")) {
					houseType = "别墅";
				}
				if (StringUtils.contains(type, "洋房")) {
					houseType = "别墅";
				}
				if (StringUtils.contains(type, "联排")) {
					houseType = "别墅";
				}
				if (StringUtils.contains(type, "叠拼")) {
					houseType = "别墅";
				}
				if (StringUtils.contains(type, "双拼")) {
					houseType = "别墅";
				}
				model.setHouseType(houseType);
			}
			if (StringUtils.contains(key, "销售状态")) {
				model.setHouseSaleStatus(map.get(key));
			}
			if (StringUtils.contains(key, "物业费用")) {
				String costs = map.get(key);
				if (StringUtils.contains(costs, "元")) {
					int index = costs.indexOf("元");
					model.setPropertyCosts(costs.substring(0, index));
				}
			}
			if (StringUtils.contains(key, "停车位")) {
				model.setParkingLot(map.get(key));
			}
			if (StringUtils.contains(key, "交房时间")) {
				model.setTimeOfDelivery(map.get(key));
			}
			if (StringUtils.contains(key, "占地面积")) {
				String area = map.get(key);
				if (StringUtils.contains(area, ",")) {
					area = area.replace(",", "");
				}
				if (StringUtils.contains(area, "㎡")) {
					area = area.replace("㎡", "");
				}
				if (StringUtils.contains(area, "平")) {
					int index = area.indexOf("平");
					area = area.substring(0, index);
				}
				model.setAreaCovered(area);
			}
			if (StringUtils.contains(key, "建筑面积")) {
				String area = map.get(key);
				if (StringUtils.contains(area, ",")) {
					area = area.replace(",", "");
				}
				if (StringUtils.contains(area, "㎡")) {
					area = area.replace("㎡", "");
				}
				if (StringUtils.contains(area, "平")) {
					int index = area.indexOf("平");
					area = area.substring(0, index);
				}
				model.setBuildingArea(area);
			}
		}
		return model;
	}

	public static ReptileNewHouseModel getZhoubianInfo(ReptileNewHouseModel model, Map<String, String> map) {
		for (String key : map.keySet()) {
			if (StringUtils.contains(key, " ")) {
				key = key.replace(" ", "");
			}
			if (StringUtils.contains(key, "交通")) {
				model.setTraffic(map.get(key));
			}
			if (StringUtils.contains(key, "幼儿园")) {
				model.setKindergarten(map.get(key));
			}
			if (StringUtils.contains(key, "中小学")) {
				model.setSchool(map.get(key));
			}
			if (StringUtils.contains(key, "商场")) {
				model.setPowerCenter(map.get(key));
			}
			if (StringUtils.contains(key, "医院")) {
				model.setHospital(map.get(key));
			}
			if (StringUtils.contains(key, "银行")) {
				model.setBank(map.get(key));
			}
			if (StringUtils.contains(key, "其他")) {
				model.setOther(map.get(key));
			}
		}
		return model;
	}

}
