package com.jrzh.mvc.convert.reptile;

import org.springframework.stereotype.Component;

import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileEstateZbModel;
import com.jrzh.mvc.view.reptile.ReptileEstateZbView;
import com.jrzh.common.utils.ReflectUtils;

@Component("reptileEstateZbConvert")
public class ReptileEstateZbConvert implements BaseConvertI<ReptileEstateZbModel, ReptileEstateZbView> {

	@Override
	public ReptileEstateZbModel addConvert(ReptileEstateZbView view) throws ProjectException {
		ReptileEstateZbModel model = new ReptileEstateZbModel();
		ReflectUtils.copySameFieldToTarget(view, model);
		return model;
	}

	@Override
	public ReptileEstateZbModel editConvert(ReptileEstateZbView view, ReptileEstateZbModel model) throws ProjectException {
		ReflectUtils.copySameFieldToTargetFilter(view, model, new String[]{"createBy", "createTime"});
		return model;
	}

	@Override
	public ReptileEstateZbView convertToView(ReptileEstateZbModel model) throws ProjectException {
		ReptileEstateZbView view = new ReptileEstateZbView();
		ReflectUtils.copySameFieldToTarget(model, view);
		return view;
	}

}
