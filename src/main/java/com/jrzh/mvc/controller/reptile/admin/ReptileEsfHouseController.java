package com.jrzh.mvc.controller.reptile.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.annotation.RequiredPermission;
import com.jrzh.framework.annotation.UserEvent;
import com.jrzh.framework.base.controller.BaseAdminController;
import com.jrzh.framework.bean.JqDataGrid;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.mvc.constants.SysActionConstants;
import com.jrzh.mvc.search.reptile.ReptileEsfHouseSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileEsfHouseView;

@RestController(ReptileEsfHouseController.LOCATION +"/ReptileEsfHouseController")
@RequestMapping(ReptileEsfHouseController.LOCATION)
public class ReptileEsfHouseController extends BaseAdminController{
	public static final String LOCATION = "sys/admin/reptileEsfHouse";
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	@PostMapping(value = "datagrid")
	@RequiredPermission(key =  SysActionConstants.INDEX)
	@UserEvent(desc = "ReptileEsfHouse列表")
	public JqDataGrid<ReptileEsfHouseView> datagrid(ReptileEsfHouseSearch search) {
		JqDataGrid<ReptileEsfHouseView> dg = new JqDataGrid<ReptileEsfHouseView>();
	    try{
	    	dg = reptileServiceManage.reptileEsfHouseService.datagrid(search);
	    } catch (Exception e){
	    	log.error(e);
	    }
		return dg;
	}
	
	@PostMapping(value = "add")
	@RequiredPermission(key =  SysActionConstants.ADD)
	@UserEvent(desc = "ReptileEsfHouse新增")
	public ResultBean add(ReptileEsfHouseView view){
		ResultBean result = new ResultBean();
		try{
			reptileServiceManage.reptileEsfHouseService.addByView(view, getSessionUser());
			result.setStatus(ResultBean.SUCCESS);
			result.setMsg(I18nHelper.getI18nByKey("message.operation_success", getSessionUser()));
		}catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
		}
		return result;	
	}
	
	@PostMapping(value = "edit/{id}")
	@RequiredPermission(key = SysActionConstants.EDIT)
	@UserEvent(desc = "ReptileEsfHouse编辑")
	public ResultBean edit(@PathVariable("id") String id, ReptileEsfHouseView view) {
		ResultBean result = new ResultBean();
		try {
			view.setId(id);
			reptileServiceManage.reptileEsfHouseService.editByView(view, getSessionUser());
			result.setStatus(ResultBean.SUCCESS);
			result.setMsg(I18nHelper.getI18nByKey("message.operation_success", getSessionUser()));
		}catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
		}
		return result;
	}
	
	@PostMapping({"loadData/{id}"})
  	@RequiredPermission(key=SysActionConstants.VIEW)
  	@UserEvent(desc = "ReptileEsfHouse读取数据")
  	public ResultBean loadData(@PathVariable("id") String id, ReptileEsfHouseView view, BindingResult errors){
	    ResultBean result = new ResultBean();
	    try{
	      result = validateErrors(errors);
	      if (result != null) {
	        return result;
	      }
	      result = new ResultBean();
	      result.setObjs(new Object[] {reptileServiceManage.reptileEsfHouseService.findViewById(id) });
	      result.setStatus(ResultBean.SUCCESS);
	      result.setMsg(I18nHelper.getI18nByKey("message.inquire_success", getSessionUser()));
	    }
	    catch (Exception e){
	      log.error(e.getMessage());
	      result.setMsg(e.getMessage());
	    }
	    return result;
  }
	
	@PostMapping(value = "delete/{id}")
	@RequiredPermission(key = SysActionConstants.DELETE)
	@UserEvent(desc = "ReptileEsfHouse删除")
	public ResultBean delete(@PathVariable("id") String id) {
		ResultBean result = new ResultBean();
		try {
			reptileServiceManage.reptileEsfHouseService.delete(id, getSessionUser());
			result.setStatus(ResultBean.SUCCESS);
			result.setMsg(I18nHelper.getI18nByKey("message.operation_success", getSessionUser()));
		} catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
		}
		return result;
	}
	
	@PostMapping(value = "changeStatus/{id}")
	@RequiredPermission(key = SysActionConstants.DISABLE_ENABLE)
	@UserEvent(desc = "ReptileEsfHouse启用/禁用")
	public ResultBean changeStatus(@PathVariable("id") String id, ReptileEsfHouseView view){
		ResultBean result = new ResultBean();
		try {
			result = reptileServiceManage.reptileEsfHouseService.changeStatus(id, getSessionUser());
		} catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
		}
		return result;
	}
	
	@Override
	protected void setData() {
		
	}

}
