package com.jrzh.mvc.controller.reptile.front;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.config.ProjectConfigration;
import com.jrzh.framework.annotation.UserEvent;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;
import com.jrzh.mvc.model.reptile.ReptileNewHouseModel;
import com.jrzh.mvc.model.reptile.ReptileNewhouseDealEverydayModel;
import com.jrzh.mvc.model.reptile.ReptileNewhouseYushouzhengModel;
import com.jrzh.mvc.model.reptile.ReptileTemplateModel;
import com.jrzh.mvc.search.reptile.ReptileGtjAreaInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileNewHouseSearch;
import com.jrzh.mvc.search.reptile.ReptileNewhouseDealEverydaySearch;
import com.jrzh.mvc.search.reptile.ReptileNewhouseYushouzhengSearch;
import com.jrzh.mvc.search.reptile.ReptileTemplateInfoSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileCommercialHouseInfoView;
import com.jrzh.mvc.view.reptile.ReptileCommodityHouseInfoView;
import com.jrzh.mvc.view.reptile.ReptileNewHouseView;
import com.jrzh.mvc.view.reptile.ReptileNewhouseYushouzhengView;
import com.thoughtworks.selenium.webdriven.commands.GetTitle;

@RestController(ReportController.LOCATION + "/ReportController")
@RequestMapping(ReportController.LOCATION)
public class ReportController extends BaseFrontReptileController {

	public static final String LOCATION = "/report/front/";

	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	
	@Autowired
	ProjectConfigration projectConfigration;

	// 查询城市区域
	@PostMapping("selectPart")
	public ResultBean selectPart(String city) {
		ResultBean resultBean = new ResultBean();
		ReptileNewhouseDealEverydaySearch search = new ReptileNewhouseDealEverydaySearch();
		search.setLikeCity(city);
		List<String> parts = reptileServiceManage.reptileReportConfigService.getParts(city);
		if (parts == null)
			return resultBean;
		resultBean.setObjs(new Object[] { parts });
		resultBean.setStatus(ResultBean.SUCCESS);
		return resultBean;
	}

	// 获得近几年的年份
	private static String[] getYears(int i) {
		String[] years = new String[i];
		Integer now_year = Integer.parseInt(DateUtil.format(new Date(), "yyyy"));
		for (int j = i - 1; j >= 0; j--) {
			int year = now_year - j;
			years[i - j - 1] = Integer.toString(year);
		}
		return years;
	}

	private static String[] getYears(Integer start_year, Integer end_year) {
		String[] years = null;
		int i = end_year - start_year;
		if (i == 0) {
			years = new String[1];
			years[0] = Integer.toString(end_year);
		} else if (i > 0) {
			years = new String[i + 1];
			for (int j = 0; j <= i; j++) {
				years[j] = Integer.toString(start_year + j);
			}
		}
		return years;
	}

	// 城市供求情况——办公
	@RequestMapping("cityGQ_office")
	public ResultBean cityGQ_office(String city, Integer start_year, Integer end_year) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			List<String> ids = new ArrayList<String>();
			StringBuffer id = new StringBuffer();
			ReptileNewHouseSearch newHouse_search = new ReptileNewHouseSearch();
			newHouse_search.setLikeHouseType("写字楼");
			List<ReptileNewHouseModel> newHouse_types = reptileServiceManage.reptileNewHouseService
					.findListBySearch(newHouse_search);
			for (ReptileNewHouseModel model : newHouse_types) {
				ids.add(model.getId());
				// System.out.print("'"+model.getId()+"'"+",");
				id.append(model.getId() + ",");
			}
			id = id.deleteCharAt(id.length() - 1);
			System.out.println("\n");
			if (ids.size() > 0) {
				String[] years = null;
				int num = 5;
				if (start_year == null || end_year == null) {
					// 获取近五年年份
					years = getYears(5);
				} else {
					// 获取用户输入的年份
					years = getYears(start_year, end_year);
					num = end_year - start_year + 1;
				}
				List<ReportTableView> list = new ArrayList<ReportTableView>();
				ReptileNewhouseYushouzhengSearch search_yushou = new ReptileNewhouseYushouzhengSearch();
				// 获取每一年的 供应面积 集合

				for (int i = 0; i < num; i++) {
					// 获取不同年份 城市办公 供应面积
					search_yushou.setLikeCity(city);
					search_yushou.setLikeOpeningDate(years[i]);
					search_yushou.setInHouseIds(id.toString());
					List<ReptileNewhouseYushouzhengModel> yushous = reptileServiceManage.reptileNewhouseYushouzhengService
							.findListBySearch(search_yushou);
					// 计算 供应面积
					double gongying_area = 0;
					for (ReptileNewhouseYushouzhengModel model : yushous) {
						gongying_area += Double.parseDouble(model.getPresellAllArea());
					}
					String area = Double.toString(gongying_area);
					if (StringUtils.isBlank(area))
						area = "0";
					if (area.contains("."))
						area = area.substring(0, area.indexOf("."));
					ReportTableView view = new ReportTableView();
					view.setTime(years[i]);
					view.setGongying(area);
					view.setTitle(years[i] + "年办面积城市供求情况（平米）");

					// 获取城市办公 销售面积
					String deal_office_area = reptileServiceManage.reptileReportConfigService
							.getCityGQ_office_dela(city, years[i]);
					if (StringUtils.isBlank(deal_office_area))
						deal_office_area = "0";
					if (deal_office_area.contains("."))
						deal_office_area = deal_office_area.substring(0, deal_office_area.indexOf("."));
					view.setChengjiao(deal_office_area);
					list.add(view);
				}

				/*
				 * for (ReportTableView model : list) { System.out.println(model.getTime() + "年"
				 * + model.getTitle() + ": " + model.getGongying() + " , " +
				 * model.getChengjiao()); }
				 */

				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setObjs(new Object[] { list });
			} else {
				return resultBean;
			}
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	// 城市供求情况——住宅
	@RequestMapping("cityGQ_house")
	public ResultBean cityGQ_house(String city, Integer start_year, Integer end_year) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			List<String> ids = new ArrayList<String>();
			StringBuffer id = new StringBuffer();
			ReptileNewHouseSearch newHouse_search = new ReptileNewHouseSearch();
			newHouse_search.setLikeHouseType("住");
			List<ReptileNewHouseModel> newHouse_types = reptileServiceManage.reptileNewHouseService
					.findListBySearch(newHouse_search);
			for (ReptileNewHouseModel model : newHouse_types) {
				ids.add(model.getId());
				// System.out.print("'"+model.getId()+"'"+",");
				id.append(model.getId() + ",");
			}
			id = id.deleteCharAt(id.length() - 1);
			System.out.println("\n");
			if (ids.size() > 0) {
				String[] years = null;
				int num = 5;
				if (start_year == null || end_year == null) {
					// 获取近五年年份
					years = getYears(5);
				} else {
					// 获取用户输入的年份
					years = getYears(start_year, end_year);
					num = end_year - start_year + 1;
				}
				List<ReportTableView> list = new ArrayList<ReportTableView>();
				ReptileNewhouseYushouzhengSearch search_yushou = new ReptileNewhouseYushouzhengSearch();
				// 获取每一年的 供应面积 集合
				for (int i = 0; i < num; i++) {
					// 获取不同年份 城市住宅 供应面积
					search_yushou.setLikeCity(city);
					search_yushou.setLikeOpeningDate(years[i]);
					search_yushou.setInHouseIds(id.toString());
					List<ReptileNewhouseYushouzhengModel> yushous = reptileServiceManage.reptileNewhouseYushouzhengService
							.findListBySearch(search_yushou);
					// 计算 供应面积
					double gongying_area = 0;
					for (ReptileNewhouseYushouzhengModel model : yushous) {
						gongying_area += Double.parseDouble(model.getPresellAllArea());
					}
					String area = Double.toString(gongying_area);
					if (area.contains("."))
						area = area.substring(0, area.indexOf("."));
					ReportTableView view = new ReportTableView();
					view.setTime(years[i]);
					view.setGongying(area);
					view.setTitle(years[i] + "年住宅面积城市供求情况（平米）");

					// 获取城市办公 销售面积
					String deal_office_area = reptileServiceManage.reptileReportConfigService.getCityGQ_house_dela(city,
							years[i]);
					if (StringUtils.isBlank(deal_office_area))
						deal_office_area = "0";
					if (deal_office_area.contains("."))
						deal_office_area = deal_office_area.substring(0, deal_office_area.indexOf("."));
					view.setChengjiao(deal_office_area);
					list.add(view);
				}

				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setObjs(new Object[] { list });

				/*
				 * for(ReportTableView model :list) {
				 * System.out.println(model.getTime()+"年"+model.getTitle()+": "+model.
				 * getGongying()+" , "+model.getChengjiao()); }
				 */
			} else {
				return resultBean;
			}
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	// 区域供求情况——住宅
	@RequestMapping("partGQ_house")
	public ResultBean partGQ_house(String city, String part, Integer start_year, Integer end_year) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			List<String> ids = new ArrayList<String>();
			StringBuffer id = new StringBuffer();
			ReptileNewHouseSearch newHouse_search = new ReptileNewHouseSearch();
			newHouse_search.setLikeHouseType("住");
			List<ReptileNewHouseModel> newHouse_types = reptileServiceManage.reptileNewHouseService
					.findListBySearch(newHouse_search);
			for (ReptileNewHouseModel model : newHouse_types) {
				ids.add(model.getId());
				// System.out.print("'"+model.getId()+"'"+",");
				id.append(model.getId() + ",");
			}
			id = id.deleteCharAt(id.length() - 1);
			System.out.println("\n");
			if (ids.size() > 0) {
				String[] years = null;
				int num = 5;
				if (start_year == null || end_year == null) {
					// 获取近五年年份
					years = getYears(5);
				} else {
					// 获取用户输入的年份
					years = getYears(start_year, end_year);
					num = end_year - start_year + 1;
				}
				List<ReportTableView> list = new ArrayList<ReportTableView>();
				ReptileNewhouseYushouzhengSearch search_yushou = new ReptileNewhouseYushouzhengSearch();
				// 获取每一年的 供应面积 集合
				for (int i = 0; i < num; i++) {
					// 获取不同年份 城市住宅 供应面积
					search_yushou.setLikeCity(city);
					search_yushou.setLikePart(part);
					search_yushou.setLikeOpeningDate(years[i]);
					search_yushou.setInHouseIds(id.toString());
					List<ReptileNewhouseYushouzhengModel> yushous = reptileServiceManage.reptileNewhouseYushouzhengService
							.findListBySearch(search_yushou);
					// 计算 供应面积
					double gongying_area = 0;
					for (ReptileNewhouseYushouzhengModel model : yushous) {
						gongying_area += Double.parseDouble(model.getPresellAllArea());
					}
					String area = Double.toString(gongying_area);
					if (area.contains("."))
						area = area.substring(0, area.indexOf("."));
					ReportTableView view = new ReportTableView();
					view.setTime(years[i]);
					view.setGongying(area);
					view.setTitle(years[i] + "年住宅面积城市供求情况（平米）");

					// 获取区域住宅 销售面积
					String deal_office_area = reptileServiceManage.reptileReportConfigService.getPartGQ_house_dela(city,
							years[i], part);
					if (StringUtils.isBlank(deal_office_area))
						deal_office_area = "0";
					if (deal_office_area.contains("."))
						deal_office_area = deal_office_area.substring(0, deal_office_area.indexOf("."));
					view.setChengjiao(deal_office_area);
					list.add(view);
				}

				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setObjs(new Object[] { list });

			} else {
				return resultBean;
			}
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	// 区域供求情况——办公
	@RequestMapping("partGQ_office")
	public ResultBean partGQ_office(String city, String part, Integer start_year, Integer end_year) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			List<String> ids = new ArrayList<String>();
			StringBuffer id = new StringBuffer();
			ReptileNewHouseSearch newHouse_search = new ReptileNewHouseSearch();
			newHouse_search.setLikeHouseType("写字楼");
			List<ReptileNewHouseModel> newHouse_types = reptileServiceManage.reptileNewHouseService
					.findListBySearch(newHouse_search);
			for (ReptileNewHouseModel model : newHouse_types) {
				ids.add(model.getId());
				// System.out.print("'"+model.getId()+"'"+",");
				id.append(model.getId() + ",");
			}
			id = id.deleteCharAt(id.length() - 1);
			System.out.println("\n");
			if (ids.size() > 0) {
				String[] years = null;
				int num = 5;
				if (start_year == null || end_year == null) {
					// 获取近五年年份
					years = getYears(5);
				} else {
					// 获取用户输入的年份
					years = getYears(start_year, end_year);
					num = end_year - start_year + 1;
				}
				List<ReportTableView> list = new ArrayList<ReportTableView>();
				ReptileNewhouseYushouzhengSearch search_yushou = new ReptileNewhouseYushouzhengSearch();
				// 获取每一年的 供应面积 集合
				for (int i = 0; i < num; i++) {
					// 获取不同年份 城市办公 供应面积
					search_yushou.setLikeCity(city);
					search_yushou.setLikePart(part);
					search_yushou.setLikeOpeningDate(years[i]);
					search_yushou.setInHouseIds(id.toString());
					List<ReptileNewhouseYushouzhengModel> yushous = reptileServiceManage.reptileNewhouseYushouzhengService
							.findListBySearch(search_yushou);
					// 计算 供应面积
					double gongying_area = 0;
					for (ReptileNewhouseYushouzhengModel model : yushous) {
						gongying_area += Double.parseDouble(model.getPresellAllArea());
					}
					String area = Double.toString(gongying_area);
					if (area.contains("."))
						area = area.substring(0, area.indexOf("."));
					ReportTableView view = new ReportTableView();
					view.setTime(years[i]);
					view.setGongying(area);
					view.setTitle(years[i] + "年办公面积城市供求情况（平米）");

					// 获取区域办公 销售面积
					String deal_office_area = reptileServiceManage.reptileReportConfigService
							.getPartGQ_office_dela(city, years[i], part);
					if (StringUtils.isBlank(deal_office_area))
						deal_office_area = "0";
					if (deal_office_area.contains("."))
						deal_office_area = deal_office_area.substring(0, deal_office_area.indexOf("."));
					view.setChengjiao(deal_office_area);
					list.add(view);
				}

				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setObjs(new Object[] { list });

			} else {
				return resultBean;
			}
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	// 商品房供销价情况——全部商品房
	@RequestMapping("shoppingHouse_all")
	public ResultBean shoppingHouse_all(String city, Integer start_year, Integer end_year) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			StringBuffer id = new StringBuffer();
			ReptileNewHouseSearch newHouse_search = new ReptileNewHouseSearch();
			newHouse_search.setLikeHouseType("住宅");
			List<ReptileNewHouseModel> newHouse_types = reptileServiceManage.reptileNewHouseService
					.findListBySearch(newHouse_search);
			for (ReptileNewHouseModel model : newHouse_types) {
				id.append(model.getId() + ",");
			}
			newHouse_search.setLikeHouseType("别墅");
			newHouse_types = reptileServiceManage.reptileNewHouseService.findListBySearch(newHouse_search);
			for (ReptileNewHouseModel model : newHouse_types) {
				id.append(model.getId() + ",");
			}
			newHouse_search.setLikeHouseType("公寓");
			newHouse_types = reptileServiceManage.reptileNewHouseService.findListBySearch(newHouse_search);
			for (ReptileNewHouseModel model : newHouse_types) {
				id.append(model.getId() + ",");
			}
			id = id.deleteCharAt(id.length() - 1);
			System.out.println("\n");
			if (id.length() > 0) {
				String[] years = null;
				int num = 5;
				if (start_year == null || end_year == null) {
					// 获取近五年年份
					years = getYears(5);
				} else {
					// 获取用户输入的年份
					years = getYears(start_year, end_year);
					num = end_year - start_year + 1;
				}
				List<ReportTableView> list = new ArrayList<ReportTableView>();
				ReptileNewhouseYushouzhengSearch search_yushou = new ReptileNewhouseYushouzhengSearch();
				// 获取每一年的 供应面积 集合
				for (int i = 0; i < num; i++) {
					// 获取不同年份 城市住宅 供应面积
					search_yushou.setLikeCity(city);
					search_yushou.setLikeOpeningDate(years[i]);
					search_yushou.setInHouseIds(id.toString());
					List<ReptileNewhouseYushouzhengModel> yushous = reptileServiceManage.reptileNewhouseYushouzhengService
							.findListBySearch(search_yushou);
					// 计算 供应面积
					double gongying_area = 0;
					for (ReptileNewhouseYushouzhengModel model : yushous) {
						gongying_area += Double.parseDouble(model.getPresellAllArea());
					}
					String area = Double.toString(gongying_area);
					if (area.contains("."))
						area = area.substring(0, area.indexOf("."));
					ReportTableView view = new ReportTableView();
					view.setTime(years[i]);
					view.setGongying(area);
					view.setTitle(years[i] + "年商品房供销价情况（平米）");

					// 获取城市办公 销售面积
					String deal_office_area = reptileServiceManage.reptileReportConfigService
							.getShoppingHouse_all_dela(city, years[i]);
					if (StringUtils.isBlank(deal_office_area))
						deal_office_area = "0";
					if (deal_office_area.contains("."))
						deal_office_area = deal_office_area.substring(0, deal_office_area.indexOf("."));
					view.setChengjiao(deal_office_area);
					list.add(view);
				}

				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setObjs(new Object[] { list });

				/*
				 * for(ReportTableView model :list) {
				 * System.out.println(model.getTime()+"年"+model.getTitle()+": "+model.
				 * getGongying()+" , "+model.getChengjiao()); }
				 */
			} else {
				return resultBean;
			}
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	// 商品房供销价情况——住宅
	@RequestMapping("shoppingHouse_house")
	public ResultBean shoppingHouse_house(String city, Integer start_year, Integer end_year) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			StringBuffer id = new StringBuffer();
			ReptileNewHouseSearch newHouse_search = new ReptileNewHouseSearch();
			newHouse_search.setLikeHouseType("住宅");
			List<ReptileNewHouseModel> newHouse_types = reptileServiceManage.reptileNewHouseService
					.findListBySearch(newHouse_search);
			for (ReptileNewHouseModel model : newHouse_types) {
				id.append(model.getId() + ",");
			}

			id = id.deleteCharAt(id.length() - 1);
			System.out.println("\n");
			if (id.length() > 0) {
				String[] years = null;
				int num = 5;
				if (start_year == null || end_year == null) {
					// 获取近五年年份
					years = getYears(5);
				} else {
					// 获取用户输入的年份
					years = getYears(start_year, end_year);
					num = end_year - start_year + 1;
				}
				List<ReportTableView> list = new ArrayList<ReportTableView>();
				ReptileNewhouseYushouzhengSearch search_yushou = new ReptileNewhouseYushouzhengSearch();
				// 获取每一年的 供应面积 集合
				for (int i = 0; i < num; i++) {
					// 获取不同年份 城市住宅 供应面积
					search_yushou.setLikeCity(city);
					search_yushou.setLikeOpeningDate(years[i]);
					search_yushou.setInHouseIds(id.toString());
					List<ReptileNewhouseYushouzhengModel> yushous = reptileServiceManage.reptileNewhouseYushouzhengService
							.findListBySearch(search_yushou);
					// 计算 供应面积
					double gongying_area = 0;
					for (ReptileNewhouseYushouzhengModel model : yushous) {
						gongying_area += Double.parseDouble(model.getPresellAllArea());
					}
					String area = Double.toString(gongying_area);
					if (area.contains("."))
						area = area.substring(0, area.indexOf("."));
					ReportTableView view = new ReportTableView();
					view.setTime(years[i]);
					view.setGongying(area);
					view.setTitle(years[i] + "年商品房住宅供销价情况（平米）");

					// 获取城市办公 销售面积
					String deal_office_area = reptileServiceManage.reptileReportConfigService
							.getShoppingHouse_all_dela(city, years[i]);
					if (StringUtils.isBlank(deal_office_area))
						deal_office_area = "0";
					if (deal_office_area.contains("."))
						deal_office_area = deal_office_area.substring(0, deal_office_area.indexOf("."));
					view.setChengjiao(deal_office_area);
					list.add(view);
				}

				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setObjs(new Object[] { list });

				/*
				 * for(ReportTableView model :list) {
				 * System.out.println(model.getTime()+"年"+model.getTitle()+": "+model.
				 * getGongying()+" , "+model.getChengjiao()); }
				 */
			} else {
				return resultBean;
			}
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	// 分业态销售量价

	@RequestMapping("fenyetaixiaoshou")
	public ResultBean fenyetaixiaoshou(String city, Integer start_year, Integer end_year) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			List<ReportTableView> list = new ArrayList<ReportTableView>();
			StringBuffer id = new StringBuffer();
			String[] types = { "住宅", "别墅", "商", "写字楼" };
			ReptileNewHouseSearch newHouse_search = new ReptileNewHouseSearch();
			String[] years = null;
			int num = 2;
			if (start_year == null || end_year == null) {
				// 获取近两年年份
				years = getYears(2);
			} else {
				// 获取用户输入的年份
				years = getYears(start_year, end_year);
				num = end_year - start_year + 1;
			}
			ReportTableView[] fenyetaiViews = new ReportTableView[num];
			for (String type : types) {
				newHouse_search.setLikeHouseType(type);
				List<ReptileNewHouseModel> newHouse_types = reptileServiceManage.reptileNewHouseService
						.findListBySearch(newHouse_search);
				for (ReptileNewHouseModel model : newHouse_types) {
					id.append(model.getId() + ",");
				}

				id = id.deleteCharAt(id.length() - 1);
				if (id.length() > 0) {
					ReptileNewhouseYushouzhengSearch search_yushou = new ReptileNewhouseYushouzhengSearch();
					// 获取每一年的 供应面积 集合
					for (int i = 0; i < num; i++) {
						// 获取不同年份 城市住宅 供应面积
						search_yushou.setLikeCity(city);
						search_yushou.setLikeOpeningDate(years[i]);
						search_yushou.setInHouseIds(id.toString());
						List<ReptileNewhouseYushouzhengModel> yushous = reptileServiceManage.reptileNewhouseYushouzhengService
								.findListBySearch(search_yushou);
						// 计算 供应面积
						double gongying_area = 0;
						for (ReptileNewhouseYushouzhengModel model : yushous) {
							gongying_area += Double.parseDouble(model.getPresellAllArea());
						}
						String area = Double.toString(gongying_area);
						if (area.contains("."))
							area = area.substring(0, area.indexOf("."));
						ReportTableView view = new ReportTableView();
						view.setTime(years[i]);
						view.setGongying(area);
						view.setTitle(years[i] + "年商品房住宅供销价情况（平米）");
						if (StringUtils.equals(type, "商"))
							type = "商业";
						view.setHouseType(type);

						// 获取城市办公 销售面积
						String deal_office_area = reptileServiceManage.reptileReportConfigService
								.getShoppingHouse_all_dela(city, years[i]);
						if (StringUtils.isBlank(deal_office_area))
							deal_office_area = "0";
						if (deal_office_area.contains("."))
							deal_office_area = deal_office_area.substring(0, deal_office_area.indexOf("."));
						view.setChengjiao(deal_office_area);
						list.add(view);
					}
				} else {
					return resultBean;
				}
				for (int i = 0; i < num; i++) {
					if (fenyetaiViews[i] == null) {
						fenyetaiViews[i] = new ReportTableView();
					}
					String list_type = list.get(i).getHouseType();
					if (fenyetaiViews[i].getTime() == null) {
						fenyetaiViews[i].setTime(list.get(i).getTime());
					}
					if (StringUtils.contains(list_type, "住宅")) {
						fenyetaiViews[i].setType_house(list_type);
						fenyetaiViews[i].setChengjiao_house(list.get(i).getChengjiao());
						fenyetaiViews[i].setGongying_house(list.get(i).getGongying());
						fenyetaiViews[i].setPrice_house(list.get(i).getPrice());
					}
					if (StringUtils.contains(list_type, "商")) {
						fenyetaiViews[i].setType_business(list_type);
						fenyetaiViews[i].setChengjiao_business(list.get(i).getChengjiao());
						fenyetaiViews[i].setGongying_business(list.get(i).getGongying());
						fenyetaiViews[i].setPrice_business(list.get(i).getPrice());
					}
					if (StringUtils.contains(list_type, "写")) {
						fenyetaiViews[i].setType_office(list_type);
						fenyetaiViews[i].setChengjiao_office(list.get(i).getChengjiao());
						fenyetaiViews[i].setGongying_office(list.get(i).getGongying());
						fenyetaiViews[i].setPrice_office(list.get(i).getPrice());
					}
					if (StringUtils.contains(list_type, "墅")) {
						fenyetaiViews[i].setType_villa(list_type);
						fenyetaiViews[i].setChengjiao_villa(list.get(i).getChengjiao());
						fenyetaiViews[i].setGongying_villa(list.get(i).getGongying());
						fenyetaiViews[i].setPrice_villa(list.get(i).getPrice());
					}
				}
				list = new ArrayList<ReportTableView>();
			}

			/*
			 * for (FenyetaiView fenyetaiView : fenyetaiViews) {
			 * System.out.println(fenyetaiView.getTime());
			 * System.out.println(fenyetaiView.getType_house());
			 * System.out.println(fenyetaiView.getChengjiao_house());
			 * System.out.println(fenyetaiView.getGongying_house());
			 * System.out.println(fenyetaiView.getPrice_house());
			 * System.out.println(fenyetaiView.getType_business());
			 * System.out.println(fenyetaiView.getChengjiao_business());
			 * System.out.println(fenyetaiView.getGongying_business());
			 * System.out.println(fenyetaiView.getPrice_business());
			 * System.out.println(fenyetaiView.getType_office());
			 * System.out.println(fenyetaiView.getChengjiao_office());
			 * System.out.println(fenyetaiView.getGongying_office());
			 * System.out.println(fenyetaiView.getPrice_office());
			 * System.out.println(fenyetaiView.getType_villa());
			 * System.out.println(fenyetaiView.getChengjiao_villa());
			 * System.out.println(fenyetaiView.getGongying_villa());
			 * System.out.println(fenyetaiView.getPrice_villa());
			 * System.out.println("\n\n"); }
			 */

			if (fenyetaiViews.length > 0) {
				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setObjs(new Object[] { fenyetaiViews });
			}

		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	// 公开招拍挂市场典型地块列表
	@RequestMapping("public_auction_tudi")
	public ResultBean public_auction_tudi(String city, Integer year) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			List<ReptileGtjAreaInfoModel> list = new ArrayList<ReptileGtjAreaInfoModel>();
			if (year == null) {
				year = Integer.parseInt(getYears(1)[0]);
			}
			List<String> ids = reptileServiceManage.reptileReportConfigService.getGJTIds(city, Integer.toString(year));
			if (ids == null || ids.size() == 0)
				return resultBean;
			list = reptileServiceManage.reptileGtjAreaInfoService.listInByField("id", ids.toArray());
			System.out.println("总共：" + list.size() + "条数据");

			if (list != null && list.size() > 0) {
				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setObjs(new Object[] { list });
			}
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	// 区域土地成交情况
	@PostMapping("tudi_deal")
	public ResultBean tudi_deal(String city, String part, Integer year) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			ReptileNewhouseDealEverydaySearch search = new ReptileNewhouseDealEverydaySearch();
			search.setLikeCity(city);
			search.setLikePart(part);
			year = year == null ? Integer.parseInt(getYears(1)[0]) : year;
			search.setLikeDealDate(Integer.toString(year));
			List<ReptileNewhouseDealEverydayModel> models = reptileServiceManage.reptileNewhouseDealEverydayService
					.findListBySearch(search);
			long business = 0;
			long office = 0;
			long house = 0;
			long carport = 0;
			ReptileNewhouseDealEverydayModel deal = new ReptileNewhouseDealEverydayModel();
			for (ReptileNewhouseDealEverydayModel model : models) {
				business += Long.parseLong(model.getBusinessArea());
				office += Long.parseLong(model.getOfficeArea());
				house += Long.parseLong(model.getHouseArea());
				carport += Long.parseLong(model.getCarportArea());
			}
			deal.setBusinessArea(Long.toString(business));
			deal.setHouseArea(Long.toString(house));
			deal.setCarportArea(Long.toString(carport));
			deal.setOfficeArea(Long.toString(office));
			resultBean.setObjs(new Object[] { deal });
			resultBean.setStatus(ResultBean.SUCCESS);

			// System.out.println("商业："+deal.getBusinessArea());
			// System.out.println("住宅："+deal.getHouseArea());
			// System.out.println("办公："+deal.getOfficeArea());
			// System.out.println("车位："+deal.getCarportArea());
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	// 获取竞品地图
	@PostMapping("getJPMap")
	public ResultBean getJPMap(String repotrId) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			ReptileTemplateModel model = reptileServiceManage.reptileTemplateService.findById(repotrId);
			if (StringUtils.isNotBlank(model.getImgBase64())) {
				String imgURL = model.getImgBase64();
				resultBean.setObjs(new Object[] { imgURL });
				resultBean.setStatus(ResultBean.SUCCESS);
			}
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	// 竞品情况表
	@PostMapping("jingpinqingkuang")
	public ResultBean jingpinqingkuang(String id) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			List<String> houseIds = reptileServiceManage.reptileReportConfigService.getHouseIds(id);
			if (houseIds == null || houseIds.size() == 0)
				return resultBean;
			List<ReptileNewHouseModel> models = reptileServiceManage.reptileNewHouseService.listInByField("id",
					houseIds.toArray());
			resultBean.setObjs(new Object[] { models });
			resultBean.setStatus(ResultBean.SUCCESS);
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	@Override
	protected void setData() {

	}

}
