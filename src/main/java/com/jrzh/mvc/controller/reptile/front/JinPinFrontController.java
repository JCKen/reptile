package com.jrzh.mvc.controller.reptile.front;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrzh.bean.ReptileTemplateBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.config.ProjectConfigration;
import com.jrzh.framework.annotation.UserEvent;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.mvc.model.reptile.ReptileTemplateInfoModel;
import com.jrzh.mvc.model.reptile.ReptileTemplateModel;
import com.jrzh.mvc.search.reptile.ReptileGtjAreaInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileTemplateSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileEsfHouseView;
import com.jrzh.mvc.view.reptile.ReptileEsfHousesXiaoquView;
import com.jrzh.mvc.view.reptile.ReptileGtjAreaInfoView;
import com.jrzh.mvc.view.reptile.ReptileNewHouseView;
import com.jrzh.mvc.view.reptile.ReptileSaleInfoView;
import com.jrzh.mvc.view.reptile.ReptileTemplateInfoView;
import com.jrzh.mvc.view.reptile.ReptileTemplateView;

@RestController(JinPinFrontController.LOCATION + "/JinPinFrontController")
@RequestMapping(JinPinFrontController.LOCATION)
public class JinPinFrontController extends BaseFrontReptileController {

	public static final String LOCATION = "/jinpin/front/house/";

	@Autowired
	private ReptileServiceManage reptileServiceManage;
	@Autowired
	private ProjectConfigration projectConfigration;

	// 根据名字查询出竞品信息，同时展示周边信息,一手房和二手房都展示
	@PostMapping("findHouseSurroundingInfo")
	@UserEvent(desc = "")
	public ResultBean findHouseSurroundingInfo(ReptileNewHouseView view, String surrounding) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			String id = view.getId();
			ReptileNewHouseView newHouseView = reptileServiceManage.reptileNewHouseService.findViewById(id);
			BigDecimal longitude = newHouseView.getLongitude();
			BigDecimal latitude = newHouseView.getLatitude();
			if (StringUtils.isEmpty(surrounding)) {
				surrounding = "5";
			}
			List<ReptileNewHouseView> newHouseList = reptileServiceManage.reptileNewHouseService
					.findSurrounding(longitude, latitude, surrounding);
			List<ReptileEsfHouseView> esfHouseList = reptileServiceManage.reptileEsfHouseService
					.findSurrounding(longitude, latitude, surrounding);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] { newHouseList, esfHouseList });
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 删除试调
	@PostMapping("delTemplate")
	@UserEvent(desc = "")
	public ResultBean delTemplate(String id) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			reptileServiceManage.reptileTemplateService.deleteTemplate(id, getSessionUser());
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	

	// 查询城市地块列表
	@PostMapping("findAreaInfoByCity")
	@UserEvent(desc = "")
	public ResultBean findAreaInfoByCity(String city) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
			search.setEqualCity(city);
			List<ReptileGtjAreaInfoView> viewList = reptileServiceManage.reptileGtjAreaInfoService.viewList(search);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] { viewList });
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 根据id数组查询楼盘列表，用在竞品页面，一手房的还有二手房的数据。
	@PostMapping("findByIds")
	@UserEvent(desc = "")
	public ResultBean findByIds(String time, String[] ids) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			List<ReptileNewHouseView> newHouseList = reptileServiceManage.reptileNewHouseService.findByIds(time, ids);
			List<ReptileEsfHouseView> esfHouseList = reptileServiceManage.reptileEsfHouseService.findByIds(time, ids);
			List<ReptileGtjAreaInfoView> tudiList = reptileServiceManage.reptileGtjAreaInfoService.findByIds(ids);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] { newHouseList, esfHouseList, tudiList });
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 查询土地
	@PostMapping("findTudiByIds")
	@UserEvent(desc = "")
	public ResultBean findTudiByIds(String[] ids) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			List<ReptileGtjAreaInfoView> tudiList = reptileServiceManage.reptileGtjAreaInfoService.findByIds(ids);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] { tudiList });
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 查询新房
	@PostMapping("findNewHouseByIds")
	@UserEvent(desc = "")
	public ResultBean findNewHouseByIds(String time, String[] ids) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			List<ReptileNewHouseView> newHouseList = reptileServiceManage.reptileNewHouseService.findByIds(time, ids);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] { newHouseList });
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 查询二手房
	@PostMapping("findEsfByIds")
	@UserEvent(desc = "")
	public ResultBean findEsfByIds(String time, String[] ids) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			List<ReptileEsfHousesXiaoquView> esfHouseList = reptileServiceManage.reptileEsfHousesXiaoquService.findByIds(time, ids);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] { esfHouseList });
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 保存竞品模板
	@PostMapping("saveTemplate")
	@UserEvent(desc = "")
	public ResultBean saveTemplate(@RequestBody ReptileTemplateBean bean) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			ReptileTemplateModel model = reptileServiceManage.reptileTemplateService.saveTemplate(bean.getView(), bean.getViews(), getSessionUser());
			resultBean.setObjs(new Object[]{model});
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setMsg("保存成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 保存页面上所有的竞品模板
	@PostMapping("saveAllTemplate")
	@UserEvent(desc = "")
	public ResultBean saveAllTemplate(@RequestBody ReptileTemplateBean bean) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			ReptileTemplateModel model =reptileServiceManage.reptileTemplateService.saveAllTemplate(bean.getView(), bean.getSumView(), bean.getNewViews(), bean.getEsfViews(), bean.getTudiViews(), getSessionUser(),projectConfigration);
			resultBean.setObjs(new Object[]{model});
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setMsg("保存成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 导出竞品模板
	@PostMapping("transferTemplate")
	@UserEvent(desc = "")
	public ResultBean transferTemplate(String houseType, String type) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			List<ReptileTemplateModel> modelList = reptileServiceManage.reptileTemplateService.transferTemplate(houseType,
					type, getSessionUser());
			resultBean.setObjs(new Object[] { modelList });
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setMsg("保存成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	
	// 导出竞品模板信息
	@PostMapping("transferTemplateInfo")
	@UserEvent(desc = "")
	public ResultBean transferTemplateInfo(String[] templateIds) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			List<ReptileTemplateInfoModel> modelList = reptileServiceManage.reptileTemplateService.transferTemplateInfo(templateIds, getSessionUser());
			resultBean.setObjs(new Object[] {modelList});
			resultBean.setStatus(ResultBean.SUCCESS);
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 查询城市在时间段内的总成交面积，成交金额，成交套数
	@PostMapping("selectSaleInfo")
	@UserEvent(desc = "")
	public ResultBean selectSaleInfo(ReptileSaleInfoView view) {
		ResultBean resultBean = new ResultBean();
		try {
			// if(!isLogin()) {
			// resultBean.setStatus("noLogin");
			// resultBean.setMsg("未登录");
			// return resultBean;
			// }
			Object[] viewList = reptileServiceManage.reptileSaleInfoService.selectSaleInfo(view, getSessionUser());
			resultBean.setObjs(new Object[] { viewList[0] });
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setMsg("保存成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	
	//根据试调名查询已保存的试调
	@PostMapping("selectTemplateByName")
	public ResultBean selectTemplateByName(String name,String type,String houseType) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			List<ReptileTemplateModel> modelList = reptileServiceManage.reptileTemplateService.selectTemplateByName(name,type,houseType,getSessionUser());
			resultBean.setObjs(new Object[] { modelList });
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setMsg("保存成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	
	
	@Override
	protected void setData() {
	}

}
