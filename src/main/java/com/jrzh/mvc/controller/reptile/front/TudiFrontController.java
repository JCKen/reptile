package com.jrzh.mvc.controller.reptile.front;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.annotation.UserEvent;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileGtjAreaInfoView;


@RestController(TudiFrontController.LOCATION + "/TudiFrontController")
@RequestMapping(TudiFrontController.LOCATION)
public class TudiFrontController extends BaseFrontReptileController {

	public static final String LOCATION = "/tudi/front/house/";
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	//查询所有楼盘信息还有相关数据源价格，或者模糊查询相关关键字楼盘
	@RequestMapping("findAllHouseInfo")
	@UserEvent(desc = "")
	public ResultBean findAllHouseInfo(ReptileGtjAreaInfoView view,Integer page) {
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			List<ReptileGtjAreaInfoView> reptileGtjAreaInfoViewList = reptileServiceManage.reptileGtjAreaInfoService.findAllInfo(view,page,getSessionUser());
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {reptileGtjAreaInfoViewList});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	//查询指定条件下的总记录数
	@RequestMapping("selectTotalCount")
	@UserEvent(desc = "")
	public ResultBean selectTotalCount(ReptileGtjAreaInfoView view) {
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			Integer count = reptileServiceManage.reptileGtjAreaInfoService.totalCount(view);//总记录数
			Integer pageCount = (count + 6 - 1) / 6;//总页数
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {count , pageCount});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	// 查询指定距离的竞品数
	@RequestMapping("selectJpTotal")
	@UserEvent(desc = "")
	public ResultBean selectJpTotalCount(ReptileGtjAreaInfoView view,Integer distance){
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			Integer count = reptileServiceManage.reptileGtjAreaInfoService.findJpCount(view, distance);
			Integer pageCount = (count + 6 - 1) / 6;//总页数
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {count , pageCount});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	// 查询指定距离的竞品
	@RequestMapping("selectJp")
	@UserEvent(desc = "")
	public ResultBean selectJp(ReptileGtjAreaInfoView view, Integer distance, Integer page){
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			List<ReptileGtjAreaInfoView> views = reptileServiceManage.reptileGtjAreaInfoService.selectJp(view, distance, page);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {views});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	@Override
	protected void setData() {

	}

}
