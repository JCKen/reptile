package com.jrzh.mvc.controller.reptile.front;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("单个请求对象")
public class ReportContentRequest {

	@ApiModelProperty(value = "序号", name = "serial")
	private Integer serial;

	@ApiModelProperty(value = "标题", name = "title")
	private String title;

	@ApiModelProperty(value = "图表base64编码值", name = "base64")
	private String base64;

	@ApiModelProperty(value="表格信息" , name="table")
	private List<ReportTableView> table;

	public Integer getSerial() {
		return serial;
	}

	public void setSerial(Integer serial) {
		this.serial = serial;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public List<ReportTableView> getTable() {
		return table;
	}

	public void setTable(List<ReportTableView> table) {
		this.table = table;
	}

	
	
}
