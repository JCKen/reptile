package com.jrzh.mvc.controller.reptile.front;

public class CityGQ_OfficeView {
	private String gongying ="0";
	private String chengjiao ="0";
	private String title;
	private String time;
	private String price = "0";
	private String houseType;
	public String getHouseType() {
		return houseType;
	}
	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getGongying() {
		return gongying;
	}
	public void setGongying(String gongying) {
		this.gongying = gongying;
	}
	public String getChengjiao() {
		return chengjiao;
	}
	public void setChengjiao(String chengjiao) {
		this.chengjiao = chengjiao;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
}
