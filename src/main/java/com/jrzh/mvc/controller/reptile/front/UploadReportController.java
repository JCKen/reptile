package com.jrzh.mvc.controller.reptile.front;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrzh.bean.LayuiResultBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.framework.annotation.UserEvent;
import com.jrzh.framework.base.search.BaseSearch;
import com.jrzh.mvc.model.reptile.ReptileUserModel;
import com.jrzh.mvc.search.reptile.ReptileUserSearch;
import com.jrzh.mvc.search.sys.FileSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.service.sys.manage.SysServiceManage;
import com.jrzh.mvc.view.sys.FileView;

@RestController(UploadReportController.LOCATION + "/UploadReportController")
@RequestMapping(UploadReportController.LOCATION)
public class UploadReportController  extends BaseFrontReptileController{
	
	public static final String LOCATION = "/uploadReport/front/marketReport/";
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	@Autowired
	private SysServiceManage sysServiceManage;
	
	@PostMapping("getMarketReportDatas")
	@UserEvent(desc = "")
	public LayuiResultBean getMarketReportDatas() {
		LayuiResultBean resultBean = new LayuiResultBean();
		if(!isLogin()) {
			resultBean.setCode("-1");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			String strPage= request.getParameter("page");
			String strLimit = request.getParameter("limit");
			String eqTime = request.getParameter("eqTime");
			String eqFileName = request.getParameter("eqFileName");
			String eqUserName = request.getParameter("eqUserName");
			
			Integer page=1;
			Integer rows=10;
			if(StringUtils.isNotBlank(strPage)){
				page = Integer.parseInt(strPage);
			}
			if(StringUtils.isNotBlank(strLimit)){
				rows = Integer.parseInt(strLimit);
			}
			String memberId= "";
			Date beginTime=null;
			Date endTime=null;
			if(StringUtils.isNotBlank(eqUserName)){
				ReptileUserSearch  reptileUserSearch= new ReptileUserSearch();
				reptileUserSearch.setLikeUserName(eqUserName);
				ReptileUserModel member = 	reptileServiceManage.reptileUserService.first(reptileUserSearch);
				if(member!=null){
					memberId = member.getId();
				}else{
					memberId="-1";
				}
			}
			if(StringUtils.isNotBlank(eqTime)){
				String[] timeArray = eqTime.split(" - ");
				if(timeArray.length>0){
					String strBeginTime= timeArray[0];
					String strEndTime = timeArray[1];
					beginTime = DateUtil.format(strBeginTime+" 00:00:00", "yyyy-MM-dd HH:mm:ss");
					endTime = DateUtil.format(strEndTime+" 59:59:59", "yyyy-MM-dd HH:mm:ss");
				}
			}
			FileSearch search= new FileSearch();
			search.setLikeName(eqFileName);
			search.setGeCreateTime(beginTime);
			search.setLeCreateTime(endTime);
			search.setEqualMemberId(memberId);
			search.setEqKey("marketReport");
			search.setSort("createTime");
			search.setOrder("desc");
			
			Long count = sysServiceManage.fileService.count(search);
			search.setPage(page);
			search.setRows(rows);
			List<FileView> files = sysServiceManage.fileService.viewList(search);
			List<MarketReportView> views= new ArrayList<>();
			for(FileView  file :  files){
				MarketReportView view= new MarketReportView();
				ReptileUserModel member= reptileServiceManage.reptileUserService.findById(file.getMemberId());
				if(member!=null){
					view.setFileName(file.getName());
					view.setFileType(file.getType());
					view.setUserName(member.getUserName());
					view.setFileUrl(file.getUrl());
					view.setTime(DateUtil.format(file.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
					view.setPower(file.getPower());
					views.add(view);
				}
			}
			resultBean.setData(views);
			resultBean.setCode("0");
			resultBean.setCount(count+"");
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
}
