package com.jrzh.mvc.controller.reptile.front;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.annotation.UserEvent;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.mvc.model.reptile.ReptileEsfHouseModel;
import com.jrzh.mvc.search.reptile.ReptileEsfHousePriceSearch;
import com.jrzh.mvc.search.reptile.ReptileHousePriceSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileEsfHousePriceView;
import com.jrzh.mvc.view.reptile.ReptileEsfHouseView;
import com.jrzh.mvc.view.reptile.ReptileEsfHousesXiaoquView;
import com.jrzh.mvc.view.reptile.ReptileHousePriceView;
import com.jrzh.mvc.view.reptile.ReptileNewHouseView;

@RestController(EsfFrontController.LOCATION + "/EsfFrontController")
@RequestMapping(EsfFrontController.LOCATION)
public class EsfFrontController extends BaseFrontReptileController {

	public static final String LOCATION = "/esf/front/house/2";

	@Autowired
	private ReptileServiceManage reptileServiceManage;

	// 查询所有楼盘信息还有相关数据源价格，或者模糊查询相关关键字楼盘
	@PostMapping("findAllHouseInfo")
	@UserEvent(desc = "")
	public ResultBean findAllHouseInfo(ReptileEsfHouseView view, Integer page) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			List<ReptileEsfHouseView> reptileEsfHouseViewList = reptileServiceManage.reptileEsfHouseService
					.findAllHouseInfo(view, page, getSessionUser());
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] { reptileEsfHouseViewList });
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 根据id查询楼盘信息
	@PostMapping("selectEsfHouseInfoById")
	@UserEvent(desc = "")
	public ResultBean selectEsfHouseInfoById(String id) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			ReptileEsfHouseModel esfModel = reptileServiceManage.reptileEsfHouseService.findById(id);
			if (null == esfModel) {
				ReptileNewHouseView newView = reptileServiceManage.reptileNewHouseService.findViewById(id);
				ReptileHousePriceSearch newSearch = new ReptileHousePriceSearch();
				newSearch.setEqualHouseId(newView.getId());
				List<ReptileHousePriceView> priceViewList = reptileServiceManage.reptileHousePriceService
						.viewList(newSearch);
				List<String> priceList = new ArrayList<>();
				for (ReptileHousePriceView priceView : priceViewList) {
					if (StringUtils.equals(priceView.getHouseId(), id)) {
						if (StringUtils.isEmpty(priceView.getHousePrice()))
							continue;
						priceList.add(priceView.getHousePrice());
					}
					newView.setPriceList(priceList);
				}
				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setObjs(new Object[] { newView });
				resultBean.setMsg("查询成功");
				return resultBean;
			}
			ReptileEsfHouseView view = reptileServiceManage.reptileEsfHouseService.findViewById(id);
			ReptileEsfHousePriceSearch esfSearch = new ReptileEsfHousePriceSearch();
			esfSearch.setEqualHouseId(view.getId());
			List<ReptileEsfHousePriceView> priceViewList = reptileServiceManage.reptileEsfHousePriceService
					.viewList(esfSearch);
			List<String> priceList = new ArrayList<>();
			for (ReptileEsfHousePriceView priceView : priceViewList) {
				if (StringUtils.equals(priceView.getHouseId(), id)) {
					if (StringUtils.isEmpty(priceView.getHousePrice())) {
						priceList.add("待定," + priceView.getInfoSource());
					}
					priceList.add(priceView.getHousePrice());
				}
				view.setPriceList(priceList);
				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setObjs(new Object[] { view });
				resultBean.setMsg("查询成功");
			}
			return resultBean;
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 根据名字查询出竞品信息，同时展示周边信息
	@PostMapping("findHouseSurroundingInfo")
	@UserEvent(desc = "")
	public ResultBean findHouseSurroundingInfo(ReptileEsfHouseView view, String surrounding) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			String id = view.getId();
			ReptileEsfHouseView esfHouseView = reptileServiceManage.reptileEsfHouseService.findViewById(id);
			BigDecimal longitude = esfHouseView.getLongitude();
			BigDecimal latitude = esfHouseView.getLatitude();
			if (StringUtils.isEmpty(surrounding)) {
				surrounding = "5";
			}
			List<ReptileEsfHouseView> esfHouseList = reptileServiceManage.reptileEsfHouseService
					.findSurrounding(longitude, latitude, surrounding);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] { esfHouseList });
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 查询当前二手房楼盘成交量
	@PostMapping("selectThisHouseVolume")
	@UserEvent(desc = "")
	public ResultBean selectThisHouseVolume(String time, ReptileEsfHouseView view) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			String id = view.getId();
			ReptileEsfHouseView esfView = reptileServiceManage.reptileEsfHouseService.findViewById(id);
			String oherName = esfView.getHouseOtherName();
			Integer count = reptileServiceManage.reptileEsfHouseService.getVolume(time, oherName);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] { count });
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 查询指定条件下的总记录数
	@PostMapping("selectTotalCount")
	@UserEvent(desc = "")
	public ResultBean selectTotalCount(ReptileEsfHousesXiaoquView view) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			Integer count = reptileServiceManage.reptileEsfHousesXiaoquService.totalCount(view);// 总记录数
			Integer pageCount = (count + 6 - 1) / 6;// 总页数
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] { count, pageCount });
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	public ResultBean selectTotalCount(ReptileEsfHouseView view) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			Integer count = reptileServiceManage.reptileEsfHouseService.totalCount(view);// 总记录数
			Integer pageCount = (count + 6 - 1) / 6;// 总页数
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] { count, pageCount });
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	// 查询城市各个区域的均价还有楼盘数量
	@PostMapping("selectPart")
	@UserEvent(desc = "")
	public ResultBean selectPart(String houseCity, String housePart) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			List<Long> longList = reptileServiceManage.reptileEsfHouseService.selectPart(houseCity, housePart);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] { longList });
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	// 查询二手房竞品数
	@RequestMapping("selectJpTotal")
	@UserEvent(desc = "")
	public ResultBean findJpTotal(ReptileEsfHouseView view , Integer distance){
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			Integer total = reptileServiceManage.reptileEsfHouseService.selectJpTotal(view,distance);
			Integer pageCount = (total + 6 - 1) / 6;//总页数
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {total , pageCount});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	@RequestMapping("selectJp")
	@UserEvent(desc = "")
	public ResultBean findJp(ReptileEsfHouseView view , Integer distance,Integer page) {
		ResultBean resultBean = new ResultBean();
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			List<ReptileEsfHouseView> views = reptileServiceManage.reptileEsfHouseService.selectJp(view,distance,page);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {views});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
			resultBean.setMsg("查询失败");
		}
		return resultBean;
	}
	
	@Override
	protected void setData() {

	}

}
