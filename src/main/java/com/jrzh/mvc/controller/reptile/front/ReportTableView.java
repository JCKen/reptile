package com.jrzh.mvc.controller.reptile.front;

import java.util.List;

public class ReportTableView {
	// 供应面积
	private String gongying = "0";
	// 成交面积
	private String chengjiao = "0";
	private String title;
	// 时间
	private String time;
	// 价格
	private String price = "0";
	// 房屋类型
	private String houseType;

	// 一下字段用于分业态表格
	// 住宅类别
	private String type_house; 
	// 商业类别
	private String type_business;
	// 别墅类别
	private String type_villa;
	// 办公类别
	private String type_office;
	// 住宅供应面积
	private String gongying_house;
	// 商业供应面积
	private String gongying_business;
	// 别墅供应面积
	private String gongying_villa;
	// 办公供应面积
	private String gongying_office;
	// 住宅成交面积
	private String chengjiao_house;
	// 商业成交面积
	private String chengjiao_business;
	// 别墅成交面积
	private String chengjiao_villa;
	// 办公成交面积
	private String chengjiao_office;
	// 住宅成交价格
	private String price_house;
	// 商业成交价格
	private String price_business;
	// 别墅成交价格
	private String price_villa;
	// 办公成交价格
	private String price_office;
	// 供求比
	private String gqb_house;
	private String gqb_business;
	private String gqb_office;
	private String gqb_villa;

	// 以下字段用于土地
	// 土地编号
	private String areaNo;
	// 土地地址
	private String address;
	// 土地用途
	private String use;
	// 土地面积
	private String acreage;
	// 土地成交价
	private String tudi_price;
	// 竞得人
	private String buyer;

	// 以下字段用于土地成交情况
	// 住宅成交面积
	private String houseArea;
	// 商业成交面积
	private String businessArea;
	// 办公成交面积
	private String officeArea;
	// 车位成交面积
	private String carportArea;

	// 以下字段用于竞品情况表
	// 竞品名
	private String houseName;
	// 占地面积
	private String areaCovered;
	// 容积率
	private String plotRatio;
	// 主打项目
	private String mainUnit;
	// 成交价
	private String housePrice;
	// 备注
	private String comment;

	// 以下字段用于成交价格变化趋势
	private String price_chengjiao;
	// 年复合增长率
	private String cagr;
	// 增长率
	private String growth;
	private List<ReportTableView> children;

	public String getPrice_chengjiao() {
		return price_chengjiao;
	}

	public String getGqb_house() {
		return gqb_house;
	}

	public void setGqb_house(String gqb_house) {
		this.gqb_house = gqb_house;
	}

	public String getGqb_business() {
		return gqb_business;
	}

	public void setGqb_business(String gqb_business) {
		this.gqb_business = gqb_business;
	}

	public String getGqb_office() {
		return gqb_office;
	}

	public void setGqb_office(String gqb_office) {
		this.gqb_office = gqb_office;
	}

	public String getGqb_villa() {
		return gqb_villa;
	}

	public void setGqb_villa(String gqb_villa) {
		this.gqb_villa = gqb_villa;
	}

	public void setPrice_chengjiao(String price_chengjiao) {
		this.price_chengjiao = price_chengjiao;
	}

	public String getCagr() {
		return cagr;
	}

	public void setCagr(String cagr) {
		this.cagr = cagr;
	}

	public String getGrowth() {
		return growth;
	}

	public void setGrowth(String growth) {
		this.growth = growth;
	}

	public List<ReportTableView> getChildren() {
		return children;
	}

	public void setChildren(List<ReportTableView> children) {
		this.children = children;
	}

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public String getAreaCovered() {
		return areaCovered;
	}

	public void setAreaCovered(String areaCovered) {
		this.areaCovered = areaCovered;
	}

	public String getPlotRatio() {
		return plotRatio;
	}

	public void setPlotRatio(String plotRatio) {
		this.plotRatio = plotRatio;
	}

	public String getMainUnit() {
		return mainUnit;
	}

	public void setMainUnit(String mainUnit) {
		this.mainUnit = mainUnit;
	}

	public String getHousePrice() {
		return housePrice;
	}

	public void setHousePrice(String housePrice) {
		this.housePrice = housePrice;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getHouseArea() {
		return houseArea;
	}

	public void setHouseArea(String houseArea) {
		this.houseArea = houseArea;
	}

	public String getBusinessArea() {
		return businessArea;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	public String getOfficeArea() {
		return officeArea;
	}

	public void setOfficeArea(String officeArea) {
		this.officeArea = officeArea;
	}

	public String getCarportArea() {
		return carportArea;
	}

	public void setCarportArea(String carportArea) {
		this.carportArea = carportArea;
	}

	public String getAreaNo() {
		return areaNo;
	}

	public void setAreaNo(String areaNo) {
		this.areaNo = areaNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUse() {
		return use;
	}

	public void setUse(String use) {
		this.use = use;
	}

	public String getAcreage() {
		return acreage;
	}

	public void setAcreage(String acreage) {
		this.acreage = acreage;
	}

	public String getTudi_price() {
		return tudi_price;
	}

	public void setTudi_price(String tudi_price) {
		this.tudi_price = tudi_price;
	}

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public String getType_house() {
		return type_house;
	}

	public void setType_house(String type_house) {
		this.type_house = type_house;
	}

	public String getType_business() {
		return type_business;
	}

	public void setType_business(String type_business) {
		this.type_business = type_business;
	}

	public String getType_villa() {
		return type_villa;
	}

	public void setType_villa(String type_villa) {
		this.type_villa = type_villa;
	}

	public String getType_office() {
		return type_office;
	}

	public void setType_office(String type_office) {
		this.type_office = type_office;
	}

	public String getGongying_house() {
		return gongying_house;
	}

	public void setGongying_house(String gongying_house) {
		this.gongying_house = gongying_house;
	}

	public String getGongying_business() {
		return gongying_business;
	}

	public void setGongying_business(String gongying_business) {
		this.gongying_business = gongying_business;
	}

	public String getGongying_villa() {
		return gongying_villa;
	}

	public void setGongying_villa(String gongying_villa) {
		this.gongying_villa = gongying_villa;
	}

	public String getGongying_office() {
		return gongying_office;
	}

	public void setGongying_office(String gongying_office) {
		this.gongying_office = gongying_office;
	}

	public String getChengjiao_house() {
		return chengjiao_house;
	}

	public void setChengjiao_house(String chengjiao_house) {
		this.chengjiao_house = chengjiao_house;
	}

	public String getChengjiao_business() {
		return chengjiao_business;
	}

	public void setChengjiao_business(String chengjiao_business) {
		this.chengjiao_business = chengjiao_business;
	}

	public String getChengjiao_villa() {
		return chengjiao_villa;
	}

	public void setChengjiao_villa(String chengjiao_villa) {
		this.chengjiao_villa = chengjiao_villa;
	}

	public String getChengjiao_office() {
		return chengjiao_office;
	}

	public void setChengjiao_office(String chengjiao_office) {
		this.chengjiao_office = chengjiao_office;
	}

	public String getPrice_house() {
		return price_house;
	}

	public void setPrice_house(String price_house) {
		this.price_house = price_house;
	}

	public String getPrice_business() {
		return price_business;
	}

	public void setPrice_business(String price_business) {
		this.price_business = price_business;
	}

	public String getPrice_villa() {
		return price_villa;
	}

	public void setPrice_villa(String price_villa) {
		this.price_villa = price_villa;
	}

	public String getPrice_office() {
		return price_office;
	}

	public void setPrice_office(String price_office) {
		this.price_office = price_office;
	}

	public String getHouseType() {
		return houseType;
	}

	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getGongying() {
		return gongying;
	}

	public void setGongying(String gongying) {
		this.gongying = gongying;
	}

	public String getChengjiao() {
		return chengjiao;
	}

	public void setChengjiao(String chengjiao) {
		this.chengjiao = chengjiao;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
