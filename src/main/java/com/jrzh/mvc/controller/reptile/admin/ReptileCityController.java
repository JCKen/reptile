package com.jrzh.mvc.controller.reptile.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.annotation.RequiredPermission;
import com.jrzh.framework.annotation.UserEvent;
import com.jrzh.framework.base.controller.BaseAdminController;
import com.jrzh.framework.bean.JqDataGrid;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.mvc.constants.SysActionConstants;
import com.jrzh.mvc.search.reptile.ReptileCitySearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileCityView;

@RestController(ReptileCityController.LOCATION +"/ReptileCityController")
@RequestMapping(ReptileCityController.LOCATION)
public class ReptileCityController extends BaseAdminController{
	public static final String LOCATION = "reptile/admin/reptileCity";
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	@PostMapping(value = "datagrid")
	@RequiredPermission(key =  SysActionConstants.INDEX)
	@UserEvent(desc = "ReptileCity列表")
	public JqDataGrid<ReptileCityView> datagrid(ReptileCitySearch search) {
		JqDataGrid<ReptileCityView> dg = new JqDataGrid<ReptileCityView>();
	    try{
	    	dg = reptileServiceManage.reptileCityService.datagrid(search);
	    } catch (Exception e){
	    	log.error(e);
	    }
		return dg;
	}
	
	@PostMapping(value = "add")
	@RequiredPermission(key =  SysActionConstants.ADD)
	@UserEvent(desc = "ReptileCity新增")
	public ResultBean add(ReptileCityView view){
		ResultBean result = new ResultBean();
		try{
			reptileServiceManage.reptileCityService.addByView(view, getSessionUser());
			result.setStatus(ResultBean.SUCCESS);
			result.setMsg(I18nHelper.getI18nByKey("message.operation_success", getSessionUser()));
		}catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
		}
		return result;	
	}
	
	@PostMapping(value = "edit/{id}")
	@RequiredPermission(key = SysActionConstants.EDIT)
	@UserEvent(desc = "ReptileCity编辑")
	public ResultBean edit(@PathVariable("id") String id, ReptileCityView view) {
		ResultBean result = new ResultBean();
		try {
			view.setId(id);
			reptileServiceManage.reptileCityService.editByView(view, getSessionUser());
			result.setStatus(ResultBean.SUCCESS);
			result.setMsg(I18nHelper.getI18nByKey("message.operation_success", getSessionUser()));
		}catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
		}
		return result;
	}
	
	@PostMapping(value = "delete/{id}")
	@RequiredPermission(key = SysActionConstants.DELETE)
	@UserEvent(desc = "ReptileCity删除")
	public ResultBean delete(@PathVariable("id") String id) {
		ResultBean result = new ResultBean();
		try {
			reptileServiceManage.reptileCityService.delete(id, getSessionUser());
			result.setStatus(ResultBean.SUCCESS);
			result.setMsg(I18nHelper.getI18nByKey("message.operation_success", getSessionUser()));
		} catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
		}
		return result;
	}
	
	@PostMapping(value = "changeStatus/{id}")
	@RequiredPermission(key = SysActionConstants.DISABLE_ENABLE)
	@UserEvent(desc = "ReptileCity启用/禁用")
	public ResultBean changeStatus(@PathVariable("id") String id, ReptileCityView view){
		ResultBean result = new ResultBean();
		try {
			view.setId(id);
			reptileServiceManage.reptileCityService.editByFields(view, new String[]{"isDisable"}, getSessionUser());
			result.setStatus(ResultBean.SUCCESS);
			result.setMsg(I18nHelper.getI18nByKey("message.operation_success", getSessionUser()));
		} catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
		}
		return result;
	}
	
	@Override
	protected void setData() {
		
	}

}
