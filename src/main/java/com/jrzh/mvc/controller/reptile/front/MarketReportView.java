package com.jrzh.mvc.controller.reptile.front;

import com.jrzh.framework.base.BaseBean;

public class MarketReportView extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7365420209347376501L;
	private String fileName;
	private String userName;
	private String time;
	private String fileUrl;
	private String fileType;
	private String power;
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUserName() {
		return userName;
	}

	public String getPower() {
		return power;
	}

	public void setPower(String power) {
		this.power = power;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

}
