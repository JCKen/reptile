package com.jrzh.mvc.controller.reptile.front;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.jrzh.bean.LayuiResultBean;
import com.jrzh.bean.PDFTool.WordToPDF;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.tools.FileDownLoad;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.config.ProjectConfigration;
import com.jrzh.framework.annotation.UserEvent;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.sys.FileModel;
import com.jrzh.mvc.search.sys.FileSearch;
import com.jrzh.mvc.service.sys.manage.SysServiceManage;
import com.jrzh.util.FreeMarkerUtil;

@Controller(DownFileController.LOCATION + "/DownReportController")
@RequestMapping(DownFileController.LOCATION)
public class DownFileController extends BaseFrontReptileController {
	public static final String LOCATION = "/downReport/down/";

	public static final String POWER_PRIVATE = "否"; // 仅自己可见
	public static final String POWER_PUBLIC = "是"; // 公共
	@Autowired
	private ProjectConfigration projectConfigration;

	@Autowired
	private SysServiceManage sysServiceManage;

	@PostMapping("uploadReprot")
	@ResponseBody
	@UserEvent(desc = "")
	public ResultBean uploadReprot() {
		ResultBean resultBean = new ResultBean();
		String array[] = request.getParameterValues("imageArray");
		if (array == null || array.length <= 0) {
			resultBean.setStatus(ResultBean.ERROR);
			resultBean.setMsg("图表还未生成，请稍后重试");
			System.out.println("图表还未生成，请稍后重试");
			return resultBean;
		}
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			Map<String, Object> data = reptileServiceManage.reptileCommercialHouseInfoService.getReportData(array,
					SessionUser.getSystemUser());
			String dateDir = DateUtil.format(new Date(), "yyyy-MM-dd");
			String ctxPath = projectConfigration.getFileRootUrl() + "file/report/" + dateDir + "/";
			String mapPath = projectConfigration.mappingUrl + "file/report/" + dateDir + "/";
			File file = new File(ctxPath);
			if (!file.exists())
				file.mkdirs();
			String uuid = UUID.randomUUID().toString().replaceAll("\\-", "");// 返回一个随机UUID。
			String suffix = ".doc";
			String newFileName = "report-" + uuid + (suffix != null ? suffix : "");// 构成新文件名。
			String fileUrl = file.getPath() + "/" + newFileName;
			File uploadFile = FreeMarkerUtil.crateFile(data, "model.ftl", fileUrl);
			if (uploadFile.exists()) {
				FileModel fileModel = new FileModel();
				fileModel.setFormId("");
				fileModel.setMemberId("");
				fileModel.setKey("report");
				fileModel.setUrl(mapPath + newFileName);
				fileModel.setName(newFileName);
				fileModel.setType(suffix);
				fileModel.setModel("fileRule");
				sysServiceManage.fileService.add(fileModel, SessionUser.getSystemUser());
				resultBean.setObjs(new Object[] { fileModel.getId() });
				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setMsg("查询成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	@GetMapping("downReport")
	@ResponseBody
	@UserEvent(desc = "")
	public void downReport() {
		try {
			String reportId = request.getParameter("reportId");
			FileModel file = sysServiceManage.fileService.findById(reportId);
			if (file != null) {
				String fileUlr = file.getUrl().replace(projectConfigration.mappingUrl,
						projectConfigration.getFileRootUrl());
				new FileDownLoad().downLoad(fileUlr, file.getName(), response, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 下载模板
	@GetMapping("downTemplateFile")
	@ResponseBody
	@UserEvent(desc = "")
	public void downTemplateFile() {
		String id = request.getParameter("id");
		try {
			FileSearch fileSearch = new FileSearch();
			fileSearch.setEqualFormId(id);
			fileSearch.setEqKey("jingping");
			FileModel file = sysServiceManage.fileService.first(fileSearch);
			if (file != null) {
				String fileUlr = file.getUrl().replace(projectConfigration.mappingUrl,
						projectConfigration.getFileRootUrl());
				new FileDownLoad().downLoad(fileUlr, file.getName(), response, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@PostMapping(value = "upload")
	@ResponseBody
	public LayuiResultBean upload(MultipartFile file) throws IOException {
		LayuiResultBean result = new LayuiResultBean();
		if (!isLogin()) {
			result.setCode("-1");
			result.setMsg("未登录");
			return result;
		}
		SessionUser user = getSessionUser();
		String fileName = file.getOriginalFilename();
		String dateDir = DateUtil.format(new Date(), "yyyy-MM-dd");
		String ctxPath = projectConfigration.getFileRootUrl() + "file/marketReport/" + dateDir + "/";
		String mapPath = projectConfigration.mappingUrl + "file/marketReport/" + dateDir + "/";
		File hoistFile = new File(ctxPath);
		if (!hoistFile.exists())
			hoistFile.mkdirs();
		String uuid = UUID.randomUUID().toString().replaceAll("\\-", "");// 返回一个随机UUID。
		String suffix = fileName.indexOf(".") != -1 ? fileName.substring(fileName.lastIndexOf("."), fileName.length())
				: null;
		String repaceFileName = StringUtils.replace(fileName, suffix, "");
		String newFileName = repaceFileName + "-" + uuid + (suffix != null ? suffix : "");// 构成新文件名。
		File uploadFile = new File(hoistFile.getPath() + "/" + newFileName);
		FileCopyUtils.copy(file.getBytes(), uploadFile);
		FileModel fileModel = new FileModel();
		fileModel.setFormId("");
		fileModel.setMemberId(user.getId());
		fileModel.setKey("marketReport");
		fileModel.setUrl(mapPath + newFileName);
		fileModel.setName(newFileName);
		fileModel.setType(suffix);
		fileModel.setPower(POWER_PRIVATE);
		fileModel.setModel("marketReport");
		try {
			sysServiceManage.fileService.add(fileModel, SessionUser.getSystemUser());
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		result.setCode("0");
		result.setMsg("上传成功");
		return result;
	}

	@GetMapping("downMarketReport")
	@ResponseBody
	@UserEvent(desc = "")
	public void downMarketReport() {
		try {
			String fileUrl = request.getParameter("fileUrl");
			List<FileModel> files = sysServiceManage.fileService.selectByField("url", fileUrl);
			if (files != null && files.size() > 0) {
				FileModel file = files.get(0);
				String fileUlr = file.getUrl().replace(projectConfigration.mappingUrl,
						projectConfigration.getFileRootUrl());
				new FileDownLoad().downLoad(fileUlr, file.getName(), response, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private  ResultBean getPdfURL(ResultBean resultBean) {
		if (StringUtils.equals(resultBean.getStatus(), "success")) {
			resultBean.setStatus(ResultBean.ERROR);
			try {
				String reportId = (String) resultBean.getObjs()[0];
				FileModel file = sysServiceManage.fileService.findById(reportId);
				if (file != null) {
					String fileUlr = file.getUrl().replace(projectConfigration.mappingUrl,
							projectConfigration.getFileRootUrl());
					
					String overFile = fileUlr.replace(".doc", ".pdf");
					// doc转PDF
					WordToPDF.doc2pdf(fileUlr, overFile);
					overFile = overFile.replace(projectConfigration.getFileRootUrl(),projectConfigration.mappingUrl);
					resultBean.setObjs(new Object[] { overFile });
					resultBean.setStatus(ResultBean.SUCCESS);
				}
			} catch (Exception e) {
				e.printStackTrace();
				resultBean.setStatus(ResultBean.ERROR);
			}
		}
		return resultBean;
	}

	@PostMapping("printPDF")
	@ResponseBody
	@UserEvent(desc = "")
	public ResultBean printPDF() {
		ResultBean resultBean = new ResultBean();
		String array[] = request.getParameterValues("imageArray");
		if (array == null || array.length <= 0) {
			resultBean.setStatus(ResultBean.ERROR);
			resultBean.setMsg("图表还未生成，请稍后重试");
			System.out.println("图表还未生成，请稍后重试");
			return resultBean;
		}
		if (!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			Map<String, Object> data = reptileServiceManage.reptileCommercialHouseInfoService.getReportData(array,
					SessionUser.getSystemUser());
			String dateDir = DateUtil.format(new Date(), "yyyy-MM-dd");
			String ctxPath = projectConfigration.getFileRootUrl() + "file/report/" + dateDir + "/";
			String mapPath = projectConfigration.mappingUrl + "file/report/" + dateDir + "/";
			File file = new File(ctxPath);
			if (!file.exists())
				file.mkdirs();
			String uuid = UUID.randomUUID().toString().replaceAll("\\-", "");// 返回一个随机UUID。
			String suffix = ".doc";
			String newFileName = "report-" + uuid + (suffix != null ? suffix : "");// 构成新文件名。
			String fileUrl = file.getPath() + "/" + newFileName;
			File uploadFile = FreeMarkerUtil.crateFile(data, "model.ftl", fileUrl);
			if (uploadFile.exists()) {
				FileModel fileModel = new FileModel();
				fileModel.setFormId("");
				fileModel.setMemberId("");
				fileModel.setKey("report");
				fileModel.setUrl(mapPath + newFileName);
				fileModel.setName(newFileName);
				fileModel.setType(suffix);
				fileModel.setModel("fileRule");
				sysServiceManage.fileService.add(fileModel, SessionUser.getSystemUser());
				resultBean.setObjs(new Object[] { fileModel.getId() });
				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setMsg("查询成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		resultBean = getPdfURL(resultBean);
		return resultBean;
	}
}
