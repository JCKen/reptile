package com.jrzh.mvc.controller.reptile.front;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.mvc.model.reptile.ReptileCorrectionFactorModel;
import com.jrzh.mvc.model.reptile.ReptileCorrectionSetModel;
import com.jrzh.mvc.search.reptile.ReptileCorrectionFactorSearch;
import com.jrzh.mvc.search.reptile.ReptileCorrectionSetSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileCorrectionFactorView;

@RestController(ReptileCorrectionFactorController.LOCATION + "/ReptileCorrectionFactorController")
@RequestMapping(ReptileCorrectionFactorController.LOCATION)
public class ReptileCorrectionFactorController extends BaseFrontReptileController {
	public static final String LOCATION = "/front/reptileCorrectionFactor/";

	@Autowired
	private ReptileServiceManage reptileServiceManage;

	@RequestMapping("select")
	public ResultBean select() {
		ResultBean resultBean = new ResultBean();
		try {
			ReptileCorrectionFactorSearch search = new ReptileCorrectionFactorSearch();
			Map<String, List<ReptileCorrectionFactorModel>> models = new LinkedHashMap<>();
			search.setIsNull("pid");
			search.setSort("createTime");
			search.setOrder("asc");
			List<ReptileCorrectionFactorModel> parents = reptileServiceManage.reptileCorrectionFactorService
					.findListBySearch(search);
			search.setIsNull("");
			for (ReptileCorrectionFactorModel parent : parents) {
				search.setEqualPid(parent.getId());
				models.put(JSON.toJSONString(parent),
						reptileServiceManage.reptileCorrectionFactorService.findListBySearch(search));
			}
			resultBean.setObjs(new Object[] { models });
			resultBean.setStatus(ResultBean.SUCCESS);
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}

		return resultBean;
	}

	@RequestMapping("addNewCorr")
	public ResultBean addNewCorr(ReptileCorrectionFactorView view) {
		ResultBean resultBean = new ResultBean();
		try {
			if (StringUtils.isBlank(view.getPid()))
				view.setPid(null);
			if (StringUtils.isBlank(view.getCorrectionName()))
				view.setCorrectionName(null);
			if (StringUtils.isBlank(view.getCorrectionMax()))
				view.setCorrectionMax(null);
			if (StringUtils.isBlank(view.getCorrectionMin()))
				view.setCorrectionMin(null);
			if (StringUtils.isNotBlank(view.getCorrectionName())) {
				reptileServiceManage.reptileCorrectionFactorService.addByView(view, getSessionUser());
				resultBean.setStatus(ResultBean.SUCCESS);
			} else {
				resultBean.setStatus(ResultBean.ERROR);
				resultBean.setMsg("请确认输入无误");
			}
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	@RequestMapping("edit")
	public ResultBean edit(ReptileCorrectionFactorView view) {
		ResultBean resultBean = new ResultBean();
		try {
			ReptileCorrectionFactorView checkView = reptileServiceManage.reptileCorrectionFactorService
					.findViewById(view.getId());
			checkView.setCorrectionName(view.getCorrectionName());
			checkView.setCorrectionMax(view.getCorrectionMax());
			checkView.setCorrectionMin(view.getCorrectionMin());
			reptileServiceManage.reptileCorrectionFactorService.editByView(checkView, getSessionUser());
			resultBean.setStatus(ResultBean.SUCCESS);
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	@RequestMapping("delete")
	public ResultBean delete(String id) {
		ResultBean resultBean = new ResultBean();
		try {
			// 删除修正项数据设置
			ReptileCorrectionSetSearch search_set = new ReptileCorrectionSetSearch();
			search_set.setEqualCorrectionId(id);
			List<ReptileCorrectionSetModel> list = reptileServiceManage.reptileCorrectionSetService
					.findListBySearch(search_set);
			Set models = new HashSet(list);
			reptileServiceManage.reptileCorrectionSetService.deletes(models, getSessionUser());

			ReptileCorrectionFactorView view_factor = reptileServiceManage.reptileCorrectionFactorService
					.findViewById(id);
			// 删除修正项分类
			reptileServiceManage.reptileCorrectionFactorService.delete(id, getSessionUser());

			// 查询修正分类中是否还有修正项
			ReptileCorrectionFactorSearch search_Factor = new ReptileCorrectionFactorSearch();
			search_Factor.setEqualPid(view_factor.getPid());
			List<ReptileCorrectionFactorModel> list_factor = reptileServiceManage.reptileCorrectionFactorService
					.findListBySearch(search_Factor);
			// 如果子分类为空，则父分类自动删除
			if (CollectionUtils.isEmpty(list_factor)) {
				ReptileCorrectionFactorView view_parent = reptileServiceManage.reptileCorrectionFactorService
						.findViewById(view_factor.getPid());
				reptileServiceManage.reptileCorrectionFactorService.delete(view_parent.getId(), getSessionUser());
			}

			resultBean.setStatus(ResultBean.SUCCESS);
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}

	@Override
	protected void setData() {

	}

}
