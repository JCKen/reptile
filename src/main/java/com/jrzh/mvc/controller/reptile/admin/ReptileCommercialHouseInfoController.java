package com.jrzh.mvc.controller.reptile.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.annotation.RequiredPermission;
import com.jrzh.framework.annotation.UserEvent;
import com.jrzh.framework.base.controller.BaseAdminController;
import com.jrzh.framework.bean.JqDataGrid;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.mvc.constants.SysActionConstants;
import com.jrzh.mvc.search.reptile.ReptileCommercialHouseInfoSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileCommercialHouseInfoView;

@RestController(ReptileCommercialHouseInfoController.LOCATION +"/ReptileCommercialHouseInfoController")
@RequestMapping(ReptileCommercialHouseInfoController.LOCATION)
public class ReptileCommercialHouseInfoController extends BaseAdminController{
	public static final String LOCATION = "sys/admin/reptileCommercialHouseInfo";
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	@PostMapping(value = "datagrid")
	@RequiredPermission(key =  SysActionConstants.INDEX)
	@UserEvent(desc = "ReptileCommercialHouseInfo列表")
	public JqDataGrid<ReptileCommercialHouseInfoView> datagrid(ReptileCommercialHouseInfoSearch search) {
		JqDataGrid<ReptileCommercialHouseInfoView> dg = new JqDataGrid<ReptileCommercialHouseInfoView>();
	    try{
	    	dg = reptileServiceManage.reptileCommercialHouseInfoService.datagrid(search);
	    } catch (Exception e){
	    	log.error(e);
	    }
		return dg;
	}
	
	@PostMapping(value = "add")
	@RequiredPermission(key =  SysActionConstants.ADD)
	@UserEvent(desc = "ReptileCommercialHouseInfo新增")
	public ResultBean add(ReptileCommercialHouseInfoView view){
		ResultBean result = new ResultBean();
		try{
			reptileServiceManage.reptileCommercialHouseInfoService.addByView(view, getSessionUser());
			result.setStatus(ResultBean.SUCCESS);
			result.setMsg(I18nHelper.getI18nByKey("message.operation_success", getSessionUser()));
		}catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
		}
		return result;	
	}
	
	@PostMapping(value = "edit/{id}")
	@RequiredPermission(key = SysActionConstants.EDIT)
	@UserEvent(desc = "ReptileCommercialHouseInfo编辑")
	public ResultBean edit(@PathVariable("id") String id, ReptileCommercialHouseInfoView view) {
		ResultBean result = new ResultBean();
		try {
			view.setId(id);
			reptileServiceManage.reptileCommercialHouseInfoService.editByView(view, getSessionUser());
			result.setStatus(ResultBean.SUCCESS);
			result.setMsg(I18nHelper.getI18nByKey("message.operation_success", getSessionUser()));
		}catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
		}
		return result;
	}
	
	@PostMapping({"loadData/{id}"})
  	@RequiredPermission(key=SysActionConstants.VIEW)
  	@UserEvent(desc = "ReptileCommercialHouseInfo读取数据")
  	public ResultBean loadData(@PathVariable("id") String id, ReptileCommercialHouseInfoView view, BindingResult errors){
	    ResultBean result = new ResultBean();
	    try{
	      result = validateErrors(errors);
	      if (result != null) {
	        return result;
	      }
	      result = new ResultBean();
	      result.setObjs(new Object[] {reptileServiceManage.reptileCommercialHouseInfoService.findViewById(id) });
	      result.setStatus(ResultBean.SUCCESS);
	      result.setMsg(I18nHelper.getI18nByKey("message.inquire_success", getSessionUser()));
	    }
	    catch (Exception e){
	      log.error(e.getMessage());
	      result.setMsg(e.getMessage());
	    }
	    return result;
  }
	
	@PostMapping(value = "delete/{id}")
	@RequiredPermission(key = SysActionConstants.DELETE)
	@UserEvent(desc = "ReptileCommercialHouseInfo删除")
	public ResultBean delete(@PathVariable("id") String id) {
		ResultBean result = new ResultBean();
		try {
			reptileServiceManage.reptileCommercialHouseInfoService.delete(id, getSessionUser());
			result.setStatus(ResultBean.SUCCESS);
			result.setMsg(I18nHelper.getI18nByKey("message.operation_success", getSessionUser()));
		} catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
		}
		return result;
	}
	
	@PostMapping(value = "changeStatus/{id}")
	@RequiredPermission(key = SysActionConstants.DISABLE_ENABLE)
	@UserEvent(desc = "ReptileCommercialHouseInfo启用/禁用")
	public ResultBean changeStatus(@PathVariable("id") String id, ReptileCommercialHouseInfoView view){
		ResultBean result = new ResultBean();
		try {
			result = reptileServiceManage.reptileCommercialHouseInfoService.changeStatus(id, getSessionUser());
		} catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
		}
		return result;
	}
	
	@Override
	protected void setData() {
		
	}

}
