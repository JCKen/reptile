package com.jrzh.mvc.controller.reptile.front;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileReportConfigModel;
import com.jrzh.mvc.search.reptile.ReptileReportConfigSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileReportConfigView;

import netscape.javascript.JSObject;

@RestController(ReptileReportConfigController.LOCATION + "/ReptileReportConfigController")
@RequestMapping(ReptileReportConfigController.LOCATION)
public class ReptileReportConfigController extends BaseFrontReptileController {
	public static final String LOCATION = "/front/reportConfig/";

	@Autowired
	private ReptileServiceManage reptileServiceManage;


	@RequestMapping(method = RequestMethod.POST, value = "select")
	public ResultBean select() {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			SessionUser user = getSessionUser();
			Map<String,List<ReptileReportConfigModel>> models = new LinkedHashMap<>();
			ReptileReportConfigSearch search = new ReptileReportConfigSearch();
			search.setEqualUserId(user.getId());
			search.setIsNull("pid");
			search.setSort("orderBy");	
			search.setOrder("asc");
			List<ReptileReportConfigModel> parents = reptileServiceManage.reptileReportConfigService.findListBySearch(search);
			search.setIsNull("");
			for(ReptileReportConfigModel parent : parents) {
				search.setEqualPid(parent.getId());
				models.put(JSON.toJSONString(parent), reptileServiceManage.reptileReportConfigService.findListBySearch(search));
			}
			if (models.size() > 0) {
				// 存在用户模版数据
				log.info("存在用户模版数据");
				resultBean.setObjs(new Object[]{models});
				resultBean.setStatus(ResultBean.SUCCESS);
			} else {
				// 不存在用户模版数据
				log.info("不存在用户模版数据");
				resultBean.setStatus(ResultBean.SUCCESS);
			}
		} catch (Exception e) {
			resultBean.setMsg(e.getMessage());
			resultBean.setStatus(ResultBean.ERROR);
			e.printStackTrace();
		}
		return resultBean;
	}

	@RequestMapping(value = "add")
	public ResultBean add(ReptileReportConfigView view) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			SessionUser user = getSessionUser();
			view.setUserId(user.getId());
			reptileServiceManage.reptileReportConfigService.addByView(view, getSessionUser());
			resultBean.setStatus(ResultBean.SUCCESS);
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			resultBean.setMsg(e.getMessage());
			log.error(e);
			e.printStackTrace();
		}
		return resultBean;
	}
	
	
	@PostMapping(value = "edit")
	public ResultBean edit(String id,Integer orderBy,Integer isCheck,String pid) {
		ResultBean result = new ResultBean();
		try {
			log.info("the id is :"+id);
			ReptileReportConfigView view = reptileServiceManage.reptileReportConfigService.findViewById(id);
			view.setIsCheck(isCheck);
			view.setOrderBy(orderBy);
			if (StringUtils.isNotBlank(pid)) {
				view.setPid(pid);
			}
			reptileServiceManage.reptileReportConfigService.editByView(view, getSessionUser());
			result.setStatus(ResultBean.SUCCESS);
		}catch (Exception e) {
			log.error(e);
			result.setMsg(e.getMessage());
			result.setStatus(ResultBean.ERROR);
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(method = RequestMethod.POST, value = "findPid")
	public ResultBean findPid(ReptileReportConfigView view) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			SessionUser user = getSessionUser();
			ReptileReportConfigSearch search = new ReptileReportConfigSearch();
			search.setEqualOrderBy(view.getOrderBy().toString());
			search.setEqualPageUrl(view.getPageUrl());
			search.setEqualPageName(view.getPageName());
			search.setEqualUserId(user.getId());
			String id = reptileServiceManage.reptileReportConfigService.findViewBySearch(search).getId();
			log.info("the pid is : "+id);
			resultBean.setObjs(new Object[] { id });
			resultBean.setStatus(ResultBean.SUCCESS);
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			resultBean.setMsg(e.getMessage());
			log.error(e);
			e.printStackTrace();
		}
		return resultBean;
	}

	@Override
	protected void setData() {

	}

}
