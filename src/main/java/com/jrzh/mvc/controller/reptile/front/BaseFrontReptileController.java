package com.jrzh.mvc.controller.reptile.front;

import org.springframework.beans.factory.annotation.Autowired;

import com.jrzh.contants.Contants;
import com.jrzh.framework.base.controller.BaseFrontController;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public abstract class BaseFrontReptileController  extends BaseFrontController {
	
	@Autowired
	public ReptileServiceManage reptileServiceManage;
	
	/**
	 * 获取当前登录用户的信息
	 */
	@Override
	protected SessionUser getSessionUser() {
		SessionUser sessionUser = (SessionUser) request.getSession().getAttribute(Contants.REPTILE_SESSION_KEY);
		return sessionUser;
	}
	
	protected Boolean isLogin(){
		if(getSessionUser() != null){
			return  true;
		}
		return false;
	}
	
	@Override
	protected void setData() {
		
	}

}
