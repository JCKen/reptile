package com.jrzh.mvc.controller.reptile.front;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrzh.bean.ReptileSearchFloorResultBean;
import com.jrzh.bean.ReptileSearchPriceResultBean;
import com.jrzh.bean.ReptileSearchResultBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.framework.annotation.UserEvent;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.mvc.model.reptile.ReptileNewHouseModel;
import com.jrzh.mvc.search.reptile.ReptileAreaSearch;
import com.jrzh.mvc.search.reptile.ReptileHousePriceSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHousePreSaleCertificateSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseRoomInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjySaleInfoSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileAreaView;
import com.jrzh.mvc.view.reptile.ReptileHousePriceView;
import com.jrzh.mvc.view.reptile.ReptileNewHouseView;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseInfoView;
import com.jrzh.mvc.view.reptile.ReptileYgjyHousePreSaleCertificateView;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseRoomInfoView;
import com.jrzh.mvc.view.reptile.ReptileYgjySaleInfoView;


@RestController(FrontController.LOCATION + "/FrontController")
@RequestMapping(FrontController.LOCATION)
public class FrontController extends BaseFrontReptileController {

	public static final String LOCATION = "/front/house/";

	@Autowired
	private ReptileServiceManage reptileServiceManage;

	//根据城市查询城市所有区的新房,分组展示统计数据。
	@PostMapping("findHouseInfo")
	@UserEvent(desc = "")
	public ResultBean findHouseInfo(ReptileNewHouseView view) {
		ResultBean resultBean = new ResultBean();
//		if(!isLogin()) {
//			resultBean.setStatus("noLogin");
//			resultBean.setMsg("未登录");
//			return resultBean;
//		}
		try {
			view.setHouseCity(view.getHouseCity());
			List<ReptileSearchResultBean> reptileSearchResultBean = reptileServiceManage.reptileNewHouseService.findHouseInfo(view);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {reptileSearchResultBean.get(0)});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	//查询已有城市和所有区
	@PostMapping("findCity")
	@UserEvent(desc = "")
	public ResultBean findCity() {
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			Map<String,List<ReptileAreaView>> map = reptileServiceManage.reptileCityService.findCityArea();
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {map});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	//查询所有楼盘信息还有相关数据源价格，或者模糊查询相关关键字楼盘
	@PostMapping("findAllHouseInfo")
	@UserEvent(desc = "")
	public ResultBean findAllHouseInfo(ReptileNewHouseView view,Integer page) {
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			List<ReptileNewHouseView> reptileNewHouseViewList = reptileServiceManage.reptileNewHouseService.findAllHouseInfo(view,page,getSessionUser());
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {reptileNewHouseViewList});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	
	//根据id查询楼盘信息和该地区楼盘不同销售状态的总面积和均价
	@PostMapping("selectHouseInfoSaleStatus")
	@UserEvent(desc = "")
	public ResultBean selectHouseInfoSaleStatus(String id) {
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			ReptileNewHouseView view = reptileServiceManage.reptileNewHouseService.findViewById(id);
			List<String> priceList = new ArrayList<>();
 			ReptileHousePriceSearch priceSearch = new ReptileHousePriceSearch();
 			priceSearch.setEqualHouseId(id);
			List<ReptileHousePriceView> priceViewList = reptileServiceManage.reptileHousePriceService.viewList(priceSearch);
			for(ReptileHousePriceView priceView : priceViewList) {
				if(StringUtils.equals(priceView.getHouseId(), id)) {
					if(StringUtils.isEmpty(priceView.getHousePrice())) continue;
					if(StringUtils.equals(priceView.getHousePriceUnit(), "1")) {
						priceList.add(priceView.getHousePrice());
					}
					if(StringUtils.equals(priceView.getHousePriceUnit(), "2")) {
						priceList.add(priceView.getHousePrice() );
					}
				}
			}
			view.setPriceList(priceList);
			String housePart = view.getHousePart();
			if(StringUtils.isBlank(housePart)) {
				throw new ProjectException("出错");
			}
			ReptileAreaSearch search = new ReptileAreaSearch();
			search.setEqualOwnCity(view.getHouseCity());
			List<ReptileAreaView> reptileAreaViewList =  reptileServiceManage.reptileAreaService.viewList(search);
			//这里暂时查询当天时间的价格
			List<ReptileSearchPriceResultBean> reptileSearchPriceResultBeanList = reptileServiceManage.reptileNewHouseService.findHouseAVG(view);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {view,reptileAreaViewList,reptileSearchPriceResultBeanList});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	//查询指定条件下的总记录数
	@PostMapping("selectTotalCount")
	@UserEvent(desc = "")
	public ResultBean selectTotalCount(ReptileNewHouseView view) {
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			Integer count = reptileServiceManage.reptileNewHouseService.totalCount(view);//总记录数
			Integer pageCount = (count + 6 - 1) / 6;//总页数
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {count , pageCount});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	@PostMapping("selectHouseFloorInfoById")
	@UserEvent(desc = "")
	public ResultBean selectHouseFloorInfoById(String id) {
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			ReptileSearchFloorResultBean floorList = reptileServiceManage.reptileNewHouseService.selectById(id);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {floorList});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	@PostMapping("selectHouseRoomInfoById")
	@UserEvent(desc = "")
	public ResultBean selectHouseRoomInfoById(String houseId,String floorId) {
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			ReptileYgjyHouseRoomInfoSearch reptileYgjyHouseRoomInfoSearch = new ReptileYgjyHouseRoomInfoSearch();
			if(StringUtils.isEmpty(houseId) && StringUtils.isEmpty(floorId)) {
				resultBean.setStatus(ResultBean.SUCCESS);
				resultBean.setObjs(new Object[] {});
				resultBean.setMsg("查询成功");
				return resultBean;
			}
			reptileYgjyHouseRoomInfoSearch.setEqualHouseId(houseId);
			reptileYgjyHouseRoomInfoSearch.setEqualFloorId(floorId);
			//查询楼层信息
			List<ReptileYgjyHouseRoomInfoView> viewList = reptileServiceManage.reptileYgjyHouseRoomInfoService.viewList(reptileYgjyHouseRoomInfoSearch);
			Integer max = reptileServiceManage.reptileYgjyHouseRoomInfoService.selectMaxLouCeng(houseId,floorId);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {viewList,max});
			resultBean.setMsg("查询成功");
			
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	
	
	@PostMapping("findSalleInfo")
	@UserEvent(desc = "")
	public ResultBean findSalleInfo(ReptileYgjySaleInfoSearch search) {
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			search.setGeCreateTime(DateUtil.getBegin(new Date()));
			List<ReptileYgjySaleInfoView> saleInfoViews =  reptileServiceManage.reptileYgjySaleInfoService.viewList(search);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {saleInfoViews});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}

	//查询预售证信息
	@PostMapping("findCertificate")
	@UserEvent(desc = "")
	public ResultBean findCertificate(String id) {
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			ReptileNewHouseModel newHouseModel = reptileServiceManage.reptileNewHouseService.findById(id);
			if(null == newHouseModel) {
				throw new ProjectException("无此找到相关楼盘信息");
			}
			String selectInfo = newHouseModel.getHouseOtherName();
			if(StringUtils.isNotBlank(selectInfo)) {
				String houseName = selectInfo.trim();
				String[]  hoseNames = StringUtils.split(houseName, ",");
				System.out.println(hoseNames[0]);
				selectInfo = hoseNames[0];
			}else{
				 selectInfo = newHouseModel.getHouseName();
			}
			ReptileYgjyHouseInfoSearch reptileYgjyHouseInfoSearch = new ReptileYgjyHouseInfoSearch();
			reptileYgjyHouseInfoSearch.setLikeProductName(selectInfo);
			List<ReptileYgjyHouseInfoView> reptileYgjyHouseInfoViewList = reptileServiceManage.reptileYgjyHouseInfoService.viewList(reptileYgjyHouseInfoSearch);
			Map<String,List<ReptileYgjyHousePreSaleCertificateView>> map = new HashMap<>();
			for(ReptileYgjyHouseInfoView reptileYgjyHouseInfoView : reptileYgjyHouseInfoViewList) {
				String houseInfoId = reptileYgjyHouseInfoView.getId();
				ReptileYgjyHousePreSaleCertificateSearch reptileYgjyHousePreSaleCertificateSearch = new ReptileYgjyHousePreSaleCertificateSearch();
				reptileYgjyHousePreSaleCertificateSearch.setEqualHouseId(houseInfoId);
				List<ReptileYgjyHousePreSaleCertificateView> certificateList = reptileServiceManage.reptileYgjyHousePreSaleCertificateService.viewList(reptileYgjyHousePreSaleCertificateSearch);
				map.put(reptileYgjyHouseInfoView.getPreSalePermit(),certificateList);
			}
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {map});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	//查询城市各个区域的均价还有楼盘数量
	@PostMapping("selectPart")
	@UserEvent(desc = "")
	public ResultBean selectPart(String houseCity,String housePart) {
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			List<Long> longList = reptileServiceManage.reptileNewHouseService.selectPart(houseCity,housePart);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {longList});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	//根据id查询楼盘价格趋势
	@PostMapping("findNewHouseAllPrice")
	@UserEvent(desc = "")
	public ResultBean findNewHouseAllPrice(String id) {
		ResultBean resultBean = new ResultBean();
		try {
			if(!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {reptileServiceManage.reptileNewHouseService.findNewHouseAllPrice(id)});
			resultBean.setMsg("查询成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	
	// 查询指定距离的竞品
	@PostMapping("selectDistance")
	public ResultBean findDistance(ReptileNewHouseView view,Integer distance,Integer page){
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			List<ReptileNewHouseView> views = reptileServiceManage.reptileNewHouseService.findByDistance(view, distance,page);
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {views});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
			resultBean.setMsg("查询失败");
		}
		return resultBean;
	}
	
	// 查询指定距离的竞品数
	@PostMapping("selectCompetitorTotal")
	public ResultBean findCompetitorTotal(ReptileNewHouseView view,Integer distance){
		ResultBean resultBean = new ResultBean();
		if(!isLogin()) {
			resultBean.setStatus("noLogin");
			resultBean.setMsg("未登录");
			return resultBean;
		}
		try {
			Integer total = reptileServiceManage.reptileNewHouseService.totalCountByDistance(view, distance);
			Integer pageCount = (total + 6 - 1) / 6;//总页数
			resultBean.setStatus(ResultBean.SUCCESS);
			resultBean.setObjs(new Object[] {total , pageCount});
			resultBean.setMsg("查询成功");
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return resultBean;
	}
	
	@Override
	protected void setData() {

	}
}
