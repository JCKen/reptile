package com.jrzh.mvc.controller.reptile.front;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.contants.Contants;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.view.reptile.ReptileUserView;


@RestController(LoginController.LOCATION + "/LoginController")
@RequestMapping(LoginController.LOCATION)
public class LoginController extends BaseFrontReptileController {

	public static final String LOCATION = "/front/login/";
	
	/**
	 * 保存session
	 */
	@RequestMapping(method = RequestMethod.POST,value = "saveUserSession")
	public ResultBean saveUserSession(ReptileUserView view){
		ResultBean resultBean = new ResultBean();
		try {
			ReptileUserView checkView = reptileServiceManage.reptileUserService.saveSessionUser(view);
			SessionUser sessionUser = new SessionUser();
			sessionUser.setName(checkView.getUserName());
			sessionUser.setId(checkView.getId());
			request.getSession().setAttribute(Contants.REPTILE_SESSION_KEY, sessionUser);
			resultBean.setObjs(new Object[]{checkView});
			resultBean.setMsg("success");
			resultBean.setStatus(ResultBean.SUCCESS);
		} catch (ProjectException e) {
			resultBean.setStatus(ResultBean.ERROR);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}
	
	
	/**
	 * 退出
	 */
	@RequestMapping(method = RequestMethod.POST,value = "logOut")
	public ResultBean logOut(){
		ResultBean resultBean = new ResultBean();
		request.getSession().removeAttribute(Contants.REPTILE_SESSION_KEY);
		resultBean.setMsg("success");
		resultBean.setStatus(ResultBean.SUCCESS);
		return resultBean;
	}

	@Override
	protected void setData() {
	}
	
}
