package com.jrzh.mvc.controller.reptile.front;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileCorrectionSetModel;
import com.jrzh.mvc.search.reptile.ReptileCorrectionSetSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileCorrectionSetView;
import org.springframework.util.CollectionUtils;

@RestController(ReptileCorrectionSetController.LOCATION +"/ReptileCorrectionSetController")
@RequestMapping(ReptileCorrectionSetController.LOCATION)
public class ReptileCorrectionSetController extends BaseFrontReptileController{
	public static final String LOCATION = "/front/reptileCorrectionSet/";
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	
	@RequestMapping("saveCorrSet")
	public ResultBean saveCorrSet(ReptileCorrectionSetView view)   {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			SessionUser user = getSessionUser();
			if (user==null) {
				return resultBean;
			}
			view.setUserId(user.getId());
			ReptileCorrectionSetSearch search = new ReptileCorrectionSetSearch();
			if(StringUtils.isNotBlank(view.getHouseId())) search.setEqualHouseId(view.getHouseId());
			if(StringUtils.isNotBlank(view.getUserId())) search.setEqualUserId(view.getUserId());
			if(StringUtils.isNotBlank(view.getCorrectionId())) search.setEqualCorrectionId(view.getCorrectionId());
			ReptileCorrectionSetModel checkView = reptileServiceManage.reptileCorrectionSetService.findBySearch(search);
			if(checkView==null) {
				reptileServiceManage.reptileCorrectionSetService.addByView(view, user);
			}else {
				checkView.setCorrectionValue(view.getCorrectionValue());
				
				reptileServiceManage.reptileCorrectionSetService.edit(checkView, user);
			}
			resultBean.setStatus(ResultBean.SUCCESS);
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}
	
	@RequestMapping("selectCorrSet")
	public ResultBean selectCorrSet(String houseId) {
		ResultBean resultBean = new ResultBean();
		try {
			if (!isLogin()) {
				resultBean.setStatus("noLogin");
				resultBean.setMsg("未登录");
				return resultBean;
			}
			SessionUser user = getSessionUser();
			if (user==null) {
				return resultBean;
			}
			ReptileCorrectionSetSearch search = new ReptileCorrectionSetSearch();
			search.setEqualUserId(user.getId());
			search.setEqualHouseId(houseId);
			List<ReptileCorrectionSetModel> list = reptileServiceManage.reptileCorrectionSetService.findListBySearch(search);
			if (!CollectionUtils.isEmpty(list)) {
				resultBean.setObjs(new Object[] {list});
			}
			resultBean.setStatus(ResultBean.SUCCESS);
		} catch (Exception e) {
			resultBean.setStatus(ResultBean.ERROR);
			log.error(e);
			resultBean.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return resultBean;
	}
	
	@Override
	protected void setData() {
		
	}

}
