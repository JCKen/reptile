package com.jrzh.mvc.controller.reptile.front;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.mvc.search.reptile.ReptileEstateDatumSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileEstateDatumView;

@RestController(ReptileEstateDatumController.LOCATION +"/ReptileEstateDatumController")
@RequestMapping(ReptileEstateDatumController.LOCATION)
public class ReptileEstateDatumController extends BaseFrontReptileController{
	public static final String LOCATION = "reptile/front/reptileEstateDatum";
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	@PostMapping(value = "getTime")
	public ResultBean getTime() {
		ResultBean result = new ResultBean();
		ReptileEstateDatumSearch search = new ReptileEstateDatumSearch();
		search.setRows(13);
		search.setGroupTime("time");
		search.setSort("time");
		search.setOrder("desc");
		try {
			List<ReptileEstateDatumView> list = reptileServiceManage.reptileEstateDatumService.viewList(search);
			result.setObjs(new Object[] {list});
			result.setStatus(ResultBean.SUCCESS);
		} catch (ProjectException e) {
			result.setMsg(ResultBean.ERROR);
		}
		return result;
	}
	
	@PostMapping(value = "findByCode")
	public ResultBean findByCode(String code,String startYear, String endYeat){
		ResultBean result = new ResultBean();
		ReptileEstateDatumSearch search = new ReptileEstateDatumSearch();
		search.setRows(13);
		search.setSort("time");
		search.setOrder("desc");
		search.setGtaTime(startYear);
		search.setLtaTime(endYeat);
		search.setEqualCode(code);
		try {
			List<ReptileEstateDatumView> list = reptileServiceManage.reptileEstateDatumService.viewList(search);
			result.setObjs(new Object[]{list});
			result.setStatus(ResultBean.SUCCESS);
		} catch (ProjectException e) {
			result.setMsg(ResultBean.ERROR);
		}
		return result;
	}
	
	@Override
	protected void setData() {
		
	}

}
