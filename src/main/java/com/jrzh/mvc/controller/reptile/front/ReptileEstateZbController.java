package com.jrzh.mvc.controller.reptile.front;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.search.BaseSearch;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.mvc.search.reptile.ReptileEstateZbSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileEstateZbView;

@RestController(ReptileEstateZbController.LOCATION + "/ReptileEstateZbController")
@RequestMapping(ReptileEstateZbController.LOCATION)
public class ReptileEstateZbController extends BaseFrontReptileController {
	public static final String LOCATION = "reptile/front/reptileEstateZb";

	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	// 根据城市 获取所有菜单
	@PostMapping(value = "getMenu")
	public ResultBean getMenu(String city){
		
		ResultBean result = new ResultBean();
		ReptileEstateZbSearch search = new ReptileEstateZbSearch();
		search.setGroupMenu("menu");
		search.setEqCity(city);
		search.setOrder(BaseSearch.Order_Type_Desc);
		search.setSort("updateTime");
		try {
			List<ReptileEstateZbView> list = reptileServiceManage.reptileEstateZbService.viewList(search);
			result.setObjs(new Object[] { list });
			result.setStatus(ResultBean.SUCCESS);
		} catch (ProjectException e) {
			result.setMsg(ResultBean.ERROR);
		}
		return result;
	}
	
	// 获取所有分组
	@PostMapping(value = "getClassify")
	public ResultBean getClassify(String menu,String city) {
		ResultBean result = new ResultBean();
		ReptileEstateZbSearch search = new ReptileEstateZbSearch();
		search.setGroupClassify("classify");
		search.setOrder(BaseSearch.Order_Type_Desc);
		search.setSort("updateTime");
		search.setEqualMenu(menu);
		search.setEqCity(city);
		try {
			List<ReptileEstateZbView> list = reptileServiceManage.reptileEstateZbService.viewList(search);
			result.setObjs(new Object[] { list });
			result.setStatus(ResultBean.SUCCESS);
		} catch (ProjectException e) {
			result.setMsg(ResultBean.ERROR);
		}
		return result;
	}
	
	// 获取指定分组的所有指标
	@PostMapping(value = "getZb")
	public ResultBean getZb(String classify,String city){
		ResultBean result = new ResultBean();
		ReptileEstateZbSearch search = new ReptileEstateZbSearch();
		search.setEqClassify(classify);
		search.setOrder(BaseSearch.Order_Type_Desc);
		search.setSort("updateTime");
		search.setEqCity(city);
		try {
			List<ReptileEstateZbView> list = reptileServiceManage.reptileEstateZbService.viewList(search);
			result.setStatus(ResultBean.SUCCESS);
			result.setObjs(new Object[]{list});
		} catch (ProjectException e) {
			result.setMsg(ResultBean.ERROR);
		}
		return result;
	}
	
	@Override
	protected void setData() {

	}

}
