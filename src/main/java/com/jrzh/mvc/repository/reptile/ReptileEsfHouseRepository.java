package com.jrzh.mvc.repository.reptile;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.mvc.model.reptile.ReptileEsfHouseModel;

@Repository
public interface ReptileEsfHouseRepository extends BaseRepository<ReptileEsfHouseModel,Serializable>{

	 @Modifying
	 @Query(nativeQuery=true,value="select * from reptile_esf_house where sqrt((((?1-_longitude)*PI()*12656*cos(((?2+_latitude)/2)*PI()/180)/180)  *  ((?1-_longitude)*PI()*12656*cos (((?2+_latitude)/2)*PI()/180)/180)  )  +  (  ((?2-_latitude)*PI()*12656/180)  *  ((?2-_latitude)*PI()*12656/180)  )  ) < ?3 ")
	 public List<ReptileEsfHouseModel> findSurrounding(BigDecimal longitude,BigDecimal latitude,String surrounding);
	 
	 //查询本月的数据
	 @Query(nativeQuery=true,value=" SELECT count(*) FROM reptile_esf_house WHERE DATE_FORMAT(_info_update_time, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' )  AND _house_other_name = ? ")
	 public Integer findThisMonth(String houseOtherName);
	
	 //查询上一个月的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_house WHERE PERIOD_DIFF(date_format(now() , '%Y%m' ) , date_format( _info_update_time, '%Y%m' ) ) = 1  AND _house_other_name = ? ")
	 public Integer findLastMonth(String houseOtherName);
	 
	 //查询上三个月的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_house WHERE PERIOD_DIFF(date_format(now() , '%Y%m' ) , date_format( _info_update_time, '%Y%m' ) ) = 3  AND _house_other_name = ? ")
	 public Integer findThreeMonthCount(String houseOtherName);
	 
	 //查询上六个月的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_house WHERE PERIOD_DIFF(date_format(now() , '%Y%m' ) , date_format( _info_update_time, '%Y%m' ) ) = 6  AND _house_other_name = ? ")
	 public Integer findSixMonthCount(String houseOtherName);
	 
	 //查询当前这周的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_house WHERE YEARWEEK(date_format(_info_update_time,'%Y-%m-%d')) = YEARWEEK(now())  AND _house_other_name = ? ")
	 public Integer findThisWeekCount(String houseOtherName);
	 
	 //查询上一周的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_house WHERE YEARWEEK(date_format(_info_update_time,'%Y-%m-%d')) = YEARWEEK(now())-1  AND _house_other_name = ? ")
	 public Integer findLastWeekCount(String houseOtherName);
	 
	 //查询本年的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_house WHERE YEAR(_info_update_time)=YEAR(NOW()) AND  _house_other_name = ? ")
	 public Integer findThisYearCount(String houseOtherName);
	 
	 //查询上一年的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_house WHERE year(_info_update_time)=year(date_sub(now(),interval 1 year))  AND _house_other_name = ? ")
	 public Integer findLastYearCount(String houseOtherName);
	 
	 
	 @Query(nativeQuery=true,value="SELECT count(*) as count,AVG(b._house_price) as avg FROM reptile_esf_house a LEFT JOIN reptile_esf_house_price b ON a._id = b._house_id WHERE _house_city = ? AND _house_part = ? ")
	 public List<Long> selectPart(String houseCity,String housePart);
	 
	 @Query(nativeQuery=true,value="select count(*) from( select ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_esf_house HAVING distance BETWEEN 1 AND ?3) as a")
	 public Integer selectJpTotal(BigDecimal latitude, BigDecimal longitude, Integer distance);
	 
	 @Query(nativeQuery=true,value="select *,ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_esf_house HAVING distance BETWEEN 1 AND ?3 limit ?4,6")
	 public List<ReptileEsfHouseModel> selectJp(BigDecimal latitude, BigDecimal longitude, Integer distance, Integer page);

	 @Query(nativeQuery=true,value="select count(*) from( select ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_esf_house where _renovation_condition like concat('%',?3,'%') HAVING distance BETWEEN 1 AND ?4) as a")
	 public Integer selectJpTotalByRenovation(BigDecimal latitude, BigDecimal longitude, String renovationCondition,
			Integer distance);
	 @Query(nativeQuery=true,value="select *,ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_esf_house where _renovation_condition like concat('%',?3,'%')  HAVING distance BETWEEN 1 AND ?4 limit ?5,6")
	public List<ReptileEsfHouseModel> selectJpByRenovation(BigDecimal latitude, BigDecimal longitude,
			String renovationCondition, Integer distance, Integer page);
}
