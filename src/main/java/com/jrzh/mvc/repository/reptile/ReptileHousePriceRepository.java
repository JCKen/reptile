package com.jrzh.mvc.repository.reptile;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.mvc.model.reptile.ReptileHousePriceModel;

@Repository
public interface ReptileHousePriceRepository extends BaseRepository<ReptileHousePriceModel,Serializable>{

}
