package com.jrzh.mvc.repository.reptile;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.mvc.model.reptile.ReptileEsfHousesXiaoquModel;

@Repository
public interface ReptileEsfHousesXiaoquRepository extends BaseRepository<ReptileEsfHousesXiaoquModel, Serializable> {

	@Query(nativeQuery = true, value = "SELECT count(*) as count,AVG(b._house_price) as avg FROM reptile_esf_houses_xiaoqu a LEFT JOIN reptile_esf_house_price b ON a._id = b._house_id WHERE _houses_city = ? AND _houses_part = ? ")
	List<Long> selectPart(String houseCity, String housePart);

	@Query(nativeQuery=true,value="select count(*) from( select ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_esf_houses_xiaoqu where _houses_type like concat('%',?3,'%') and _houses_price is not null and _houses_price !=0 and  _houses_building_year>= ?4  HAVING distance BETWEEN 1 AND ?5) as a")
	Integer selectJpTotalByType(BigDecimal latitude, BigDecimal longitude, String houseType, String year,
			Integer distance);

	@Query(nativeQuery=true,value="select count(*) from( select ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_esf_houses_xiaoqu where _houses_building_year>= ?3 and _houses_price is not null and _houses_price !=0 HAVING distance BETWEEN 1 AND ?4) as a")
	Integer selectJpTotal(BigDecimal latitude, BigDecimal longitude, String year ,Integer distance);

	@Query(nativeQuery=true,value="select *,ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_esf_houses_xiaoqu where _houses_type like concat('%',?3,'%') and _houses_building_year>= ?4 and _houses_price is not null and _houses_price !=0 HAVING distance BETWEEN 1 AND ?5 limit ?6,6")
	List<ReptileEsfHousesXiaoquModel> selectJpByType(BigDecimal latitude, BigDecimal longitude, String houseType,String year,
			Integer distance, Integer page);

	@Query(nativeQuery=true,value="select *,ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_esf_houses_xiaoqu where _houses_building_year >= ?3 and _houses_price is not null and _houses_price !=0 HAVING distance BETWEEN 1 AND ?4 limit ?5,6")
	List<ReptileEsfHousesXiaoquModel> selectJp(BigDecimal latitude, BigDecimal longitude, String year ,Integer distance, Integer page);
	
	//��ѯ���µ�����
		 @Query(nativeQuery=true,value=" SELECT count(*) FROM reptile_esf_houses_xiaoqu WHERE DATE_FORMAT(_info_update_time, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' )  AND _houses_name = ? ")
		 public Integer findThisMonth(String houseOtherName);
		
		 //��ѯ��һ���µ�����
		 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_houses_xiaoqu WHERE PERIOD_DIFF(date_format(now() , '%Y%m' ) , date_format( _info_update_time, '%Y%m' ) ) = 1  AND _houses_name = ? ")
		 public Integer findLastMonth(String houseOtherName);
		 
		 //��ѯ�������µ�����
		 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_houses_xiaoqu WHERE PERIOD_DIFF(date_format(now() , '%Y%m' ) , date_format( _info_update_time, '%Y%m' ) ) = 3  AND _houses_name = ? ")
		 public Integer findThreeMonthCount(String houseOtherName);
		 
		 //��ѯ�������µ�����
		 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_houses_xiaoqu WHERE PERIOD_DIFF(date_format(now() , '%Y%m' ) , date_format( _info_update_time, '%Y%m' ) ) = 6  AND _houses_name = ? ")
		 public Integer findSixMonthCount(String houseOtherName);
		 
		 //��ѯ��ǰ���ܵ�����
		 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_houses_xiaoqu WHERE YEARWEEK(date_format(_info_update_time,'%Y-%m-%d')) = YEARWEEK(now())  AND _houses_name = ? ")
		 public Integer findThisWeekCount(String houseOtherName);
		 
		 //��ѯ��һ�ܵ�����
		 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_houses_xiaoqu WHERE YEARWEEK(date_format(_info_update_time,'%Y-%m-%d')) = YEARWEEK(now())-1  AND _houses_name = ? ")
		 public Integer findLastWeekCount(String houseOtherName);
		 
		 //��ѯ���������
		 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_houses_xiaoqu WHERE YEAR(_info_update_time)=YEAR(NOW()) AND  _houses_name = ? ")
		 public Integer findThisYearCount(String houseOtherName);
		 
		 //��ѯ��һ�������
		 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_esf_houses_xiaoqu WHERE year(_info_update_time)=year(date_sub(now(),interval 1 year))  AND _houses_name = ? ")
		 public Integer findLastYearCount(String houseOtherName);
}
