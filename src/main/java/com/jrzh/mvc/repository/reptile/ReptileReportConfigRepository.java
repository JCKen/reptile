package com.jrzh.mvc.repository.reptile;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;
import com.jrzh.mvc.model.reptile.ReptileNewhouseYushouzhengModel;
import com.jrzh.mvc.model.reptile.ReptileReportConfigModel;
import com.jrzh.mvc.view.reptile.ReptileNewhouseYushouzhengView;

@Repository
public interface ReptileReportConfigRepository extends BaseRepository<ReptileReportConfigModel,Serializable>{


	@Query(nativeQuery = true, value ="SELECT SUM(_office_area) from reptile_newhouse_deal_everydays where _city like concat('%',:city,'%') and _deal_date like concat('%',:years,'%') ")
	public String getCityGQ_office_dela(@Param("city")String city,@Param("years")String years);
	
	
	@Query(nativeQuery = true, value ="SELECT SUM(_house_area) from reptile_newhouse_deal_everydays where _city like concat('%',:city,'%') and _deal_date like concat('%',:years,'%') ")
	public String getCityGQ_house_dela(@Param("city")String city,@Param("years")String years);
	
	@Query(nativeQuery = true, value ="select _part from reptile_newhouse_deal_everydays where _city like concat('%',:city,'%') group by _city,_part")
	public List<String> getParts(@Param("city")String city);
	
	@Query(nativeQuery = true, value ="SELECT SUM(_house_area) from reptile_newhouse_deal_everydays where _city like concat('%',:city,'%') and _part like concat('%',:part,'%') and _deal_date like concat('%',:years,'%') ")
	public String getPartGQ_house_dela(@Param("city")String city,@Param("years")String years,@Param("part")String part);
	
	@Query(nativeQuery = true, value ="SELECT SUM(_office_area) from reptile_newhouse_deal_everydays where _city like concat('%',:city,'%') and _part like concat('%',:part,'%') and _deal_date like concat('%',:years,'%') ")
	public String getPartGQ_office_dela(@Param("city")String city,@Param("years")String years,@Param("part")String part);
	
	@Query(nativeQuery = true, value ="SELECT SUM(_house_area) from reptile_newhouse_deal_everydays where _city like concat('%',:city,'%') and _deal_date like concat('%',:years,'%')")
	public String getShoppingHouse_all_dela(@Param("city")String city,@Param("years")String years);
	
	@Query(nativeQuery = true, value="SELECT * from reptile_gtj_area_info where  _city like concat('%',:city,'%') and _time like concat('%',:time,'%') and _address!='' and _price!='' and _buyer !=''")
	public List<ReptileGtjAreaInfoModel> public_auction_tudi(@Param("city")String city,@Param("time")String time) ;
	
	@Query(nativeQuery = true, value="SELECT _id from reptile_gtj_area_info where _city=:city and _time like concat('%',:time,'%')")
	public List<String> getGJTIds(@Param("city")String city,@Param("time")String time);
	
	@Query(nativeQuery = true, value="SELECT _house_id from reptile_template_info where _template_id=:id  AND _house_type='newHouse'")
	public List<String> getHouseIds(@Param("id")String id);
	
}
