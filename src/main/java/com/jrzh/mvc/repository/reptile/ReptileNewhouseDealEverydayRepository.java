package com.jrzh.mvc.repository.reptile;

import java.io.Serializable;
import org.springframework.stereotype.Repository;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.mvc.model.reptile.ReptileNewhouseDealEverydayModel;

@Repository
public interface ReptileNewhouseDealEverydayRepository extends BaseRepository<ReptileNewhouseDealEverydayModel,Serializable>{

}
