package com.jrzh.mvc.repository.reptile;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

@Repository
public interface ReptileGtjAreaInfoRepository extends BaseRepository<ReptileGtjAreaInfoModel,Serializable>{

	@Query(nativeQuery=true,value="select count(*) from( select ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _lat * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _lat * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _lnt * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_gtj_area_info where  _city = ?3 and _price is not null and _price !=0 and _acreage is not null HAVING distance BETWEEN 1 AND ?4) as a")
	Integer selectJpCount(String lat, String lnt, String cityName, Integer distance);
	
	@Query(nativeQuery=true,value="select *,ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?2 * PI() / 180 - _lat * PI() / 180 ) / 2 ) , 2 )+ COS( ?2 * PI( ) / 180 ) * COS( _lat * PI( ) / 180 )* POW( SIN( ( ?3 * PI() / 180 - _lnt * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance from reptile_gtj_area_info where  _city = ?1 and _price !=0 and _price is not null and _acreage is not null HAVING distance BETWEEN 1 AND ?4 limit ?5,6")
	List<ReptileGtjAreaInfoModel> selectJp(String cityName, String lat, String lnt, Integer distance,Integer page);

	@Query(nativeQuery=true,value="select count(*) from( select ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _lat * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _lat * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _lnt * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_gtj_area_info where  _city = ?3 and _use like concat('%',?4,'%') and _price is not null and _price !=0 and _acreage is not null HAVING distance BETWEEN 1 AND ?5) as a")
	Integer selectJpCountByUse(String lat, String lnt, String city, String use, Integer distance);

	@Query(nativeQuery=true,value="select *,ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?2 * PI() / 180 - _lat * PI() / 180 ) / 2 ) , 2 )+ COS( ?2 * PI( ) / 180 ) * COS( _lat * PI( ) / 180 )* POW( SIN( ( ?3 * PI() / 180 - _lnt * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance from reptile_gtj_area_info where  _city = ?1 and _use like concat('%',?4,'%') and _price is not null and _price !=0 and _acreage is not null HAVING distance BETWEEN 1 AND ?5 limit ?6,6")
	List<ReptileGtjAreaInfoModel> selectJpByUse(String city, String lat, String lnt, String use, Integer distance,
			Integer page);

}
