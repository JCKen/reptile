package com.jrzh.mvc.repository.reptile;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.mvc.model.reptile.ReptileNewHouseModel;

@Repository
public interface ReptileNewHouseRepository extends BaseRepository<ReptileNewHouseModel,Serializable>{

	 @Modifying
	 @Query(nativeQuery=true,value="select * from reptile_new_house where sqrt((((?1-_longitude)*PI()*12656*cos(((?2+_latitude)/2)*PI()/180)/180)  *  ((?1-_longitude)*PI()*12656*cos (((?2+_latitude)/2)*PI()/180)/180)  )  +  (  ((?2-_latitude)*PI()*12656/180)  *  ((?2-_latitude)*PI()*12656/180)  )  ) < ?3 ")
	 public List<ReptileNewHouseModel> findSurrounding(BigDecimal longitude,BigDecimal latitude,String surrounding);

	 @Query(nativeQuery=true,value="SELECT count(*) as count,AVG(b._house_price) as avg FROM reptile_new_house a LEFT JOIN reptile_house_price b ON a._id = b._house_id WHERE _house_city = ? AND _house_part = ? ")
	 public List<Long> selectPart(String houseCity,String housePart);
	 
	 @Query(nativeQuery=true,value="select *,ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_new_house where  _house_city = ?3 and _house_price !=0 and _house_price is not null HAVING distance BETWEEN 1 AND ?4 limit ?5,6")
	 public List<ReptileNewHouseModel> findDistance(BigDecimal lat,BigDecimal log,String cityName,Integer distance,Integer page);
	 
	 @Query(nativeQuery=true,value="select count(*) from( select ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_new_house where  _house_city = ?3 and _house_price !=0 and _house_price is not null  HAVING distance BETWEEN 1 AND ?4) as a")
	 public Integer findCompetitorTotal(BigDecimal lat,BigDecimal log,String cityName,Integer distance);
	 
	 @Query(nativeQuery=true,value="select count(*) from( select ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_new_house where  _house_city = ?3  and _house_price !=0 and _house_price is not null and _house_type like concat('%',?4,'%') HAVING distance BETWEEN 1 AND ?5) as a")
	 public Integer findCompetitorTotalByType(BigDecimal lat,BigDecimal log,String cityName,String houseType,Integer distance);
	 
	 @Query(nativeQuery=true,value=" select _house_type from reptile_new_house where _house_type like concat('%',?1,'%') GROUP BY _house_type")
	 public List<String> selectTypes(String type);
	 
	 @Query(nativeQuery=true,value="select *,ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( ?1 * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( ?1 * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( ?2 * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_new_house where  _house_city = ?3 and _house_price is not null  and _house_type like concat('%',?4,'%') HAVING distance BETWEEN 1 AND ?5 limit ?6,6")
	 public List<ReptileNewHouseModel> findDistanceByHouseType(BigDecimal latitude, BigDecimal longitude,String houseCity, String houseType, Integer distance, Integer page);
	 
}
