package com.jrzh.mvc.repository.reptile;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseRoomInfoModel;

@Repository
public interface ReptileYgjyHouseRoomInfoRepository extends BaseRepository<ReptileYgjyHouseRoomInfoModel,Serializable>{

	 //查询本月的数据
	 @Query(nativeQuery=true,value=" SELECT count(*) FROM reptile_ygjy_house_room_infos WHERE DATE_FORMAT(_info_update_time, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' ) AND _room_status LIKE '%已售%' AND _house_id = ? ")
	 public Integer findThisMonth(String houseId);
	
	 //查询上一个月的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_ygjy_house_room_infos WHERE PERIOD_DIFF(date_format(now() , '%Y%m' ) , date_format( _info_update_time, '%Y%m' ) ) = 1 AND _room_status LIKE '%已售%' AND _house_id = ? ")
	 public Integer findLastMonth(String houseId);
	 
	 //查询上三个月的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_ygjy_house_room_infos WHERE PERIOD_DIFF(date_format(now() , '%Y%m' ) , date_format( _info_update_time, '%Y%m' ) ) = 3 AND _room_status LIKE '%已售%' AND _house_id = ? ")
	 public Integer findThreeMonthCount(String houseId);
	 
	 //查询上六个月的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_ygjy_house_room_infos WHERE PERIOD_DIFF(date_format(now() , '%Y%m' ) , date_format( _info_update_time, '%Y%m' ) ) = 6 AND _room_status LIKE '%已售%' AND _house_id = ? ")
	 public Integer findSixMonthCount(String houseId);
	 
	 //查询当前这周的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_ygjy_house_room_infos WHERE YEARWEEK(date_format(_info_update_time,'%Y-%m-%d')) = YEARWEEK(now()) AND _room_status LIKE '%已售%' AND _house_id = ? ")
	 public Integer findThisWeekCount(String houseId);
	 
	 //查询上一周的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_ygjy_house_room_infos WHERE YEARWEEK(date_format(_info_update_time,'%Y-%m-%d')) = YEARWEEK(now())-1 AND _room_status LIKE '%已售%' AND _house_id = ? ")
	 public Integer findLastWeekCount(String houseId);
	 
	 //查询本年的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_ygjy_house_room_infos WHERE YEAR(_info_update_time)=YEAR(NOW()) AND _room_status LIKE '%已售%' AND _house_id = ? ")
	 public Integer findThisYearCount(String houseId);
	 
	 //查询上一年的数据
	 @Query(nativeQuery=true,value="SELECT count(*) FROM reptile_ygjy_house_room_infos WHERE year(_info_update_time)=year(date_sub(now(),interval 1 year)) AND _room_status LIKE '%已售%' AND _house_id = ? ")
	 public Integer findLastYearCount(String houseId);
	 
	 //查询当前楼盘最高楼层数
	 @Query(nativeQuery=true,value="SELECT _lou_ceng FROM reptile_ygjy_house_room_infos WHERE _house_id = ? AND _floor_id = ? ORDER BY _lou_ceng DESC LIMIT 0,1")
	 public Integer selectMaxLouCeng(String houseId,String floorId);
}
