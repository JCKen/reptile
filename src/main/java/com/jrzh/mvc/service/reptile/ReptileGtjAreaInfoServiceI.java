package com.jrzh.mvc.service.reptile;

import java.util.List;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;
import com.jrzh.mvc.search.reptile.ReptileGtjAreaInfoSearch;
import com.jrzh.mvc.view.reptile.ReptileGtjAreaInfoView;

public interface ReptileGtjAreaInfoServiceI  extends BaseServiceI<ReptileGtjAreaInfoModel, ReptileGtjAreaInfoSearch, ReptileGtjAreaInfoView>{
	
	/**
	 * 保存深圳土地数据
	 * */
	void saveSZLandData(SessionUser user)throws ProjectException;
	/**
	 * 保存广州土地数据
	 * */
	void saveGzLandData(SessionUser user)throws ProjectException;
	/**
	 * 保存北京土地数据
	 * */
	void saveBjLandData(SessionUser user)throws ProjectException;
	/**
	 * 保存上海土地数据
	 * */
	void saveSHLandData(SessionUser user)throws ProjectException;
	/**
	 * 保存成都土地数据
	 */
	void saveCdLandData(SessionUser user)throws ProjectException;
	/**
	 * 保存天津土地数据
	 * */
	void saveTJLandData(SessionUser user)throws ProjectException;
	/**
	 * 保存珠海土地数据
	 */
	void saveZhLandData(SessionUser systemUser)throws ProjectException;
	/**
	 * 保存佛山土地数据
	 */
	void saveFsLandData(SessionUser systemUser)throws ProjectException;
	/**
	 *  保存东莞土地数据
	 */
	void saveDgLandData(SessionUser systemUser)throws ProjectException;
	/**
	 * 保存清远土地数据
	 */
	void saveQyLandData(SessionUser systemUser)throws ProjectException;
	/**
	 * 保存江门土地数据 
	 */
	void saveJmLandData(SessionUser systemUser)throws ProjectException;
	/**
	 * 保存奉化土地数据 
	 */
	void saveFhLandData(SessionUser systemUser)throws ProjectException;
	/**
	 *  保存惠州土地数据
	 */
	void saveHzLandData(SessionUser systemUser)throws ProjectException;
	/**
	 * 保存西安土地数据
	 */
	void saveXALandData(SessionUser systemUser)throws ProjectException;
	/**
	 * 保存昆山土地数据
	 */
	void saveKsLandData(SessionUser systemUser)throws ProjectException;
	
	public List<ReptileGtjAreaInfoView> findAllInfo(ReptileGtjAreaInfoView view,Integer page,SessionUser user) throws ProjectException;
	
	public Integer totalCount(ReptileGtjAreaInfoView view) throws ProjectException;
	
	public List<ReptileGtjAreaInfoView> findByIds(String ids[]) throws ProjectException;
	
	/**
	 * 获取土地竞品的数量
	 * @param lat 纬度
	 * @param log 经度
	 * @param cityName 城市
	 * @param distance 距离
	 * @return
	 */
	public Integer findJpCount(ReptileGtjAreaInfoView view, Integer distance)throws ProjectException;
	
	/**
	 * 获取土地竞品的数量
	 * @param lat 纬度
	 * @param log 经度
	 * @param cityName 城市
	 * @param distance 距离
	 * @param page 第几页
	 * @return List<ReptileGtjAreaInfoModel>
	 */
	public List<ReptileGtjAreaInfoView> selectJp(ReptileGtjAreaInfoView view, Integer distance,
			Integer page)throws ProjectException;
	
}