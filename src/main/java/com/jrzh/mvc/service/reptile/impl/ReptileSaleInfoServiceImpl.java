package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.factory.bj.BJHouseSaleInfoSpider;
import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileSaleInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileSaleInfoModel;
import com.jrzh.mvc.repository.reptile.ReptileSaleInfoRepository;
import com.jrzh.mvc.search.reptile.ReptileSaleInfoSearch;
import com.jrzh.mvc.service.reptile.ReptileSaleInfoServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileSaleInfoView;

@Service("reptileSaleInfoService")
public class ReptileSaleInfoServiceImpl extends
		BaseServiceImpl<ReptileSaleInfoModel, ReptileSaleInfoSearch, ReptileSaleInfoView> implements
		ReptileSaleInfoServiceI {

	@Autowired
	private ReptileSaleInfoRepository reptileSaleInfoRepository;
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	@PersistenceContext
	EntityManager em ;

	@Override
	public BaseRepository<ReptileSaleInfoModel,Serializable> getDao() {
		return reptileSaleInfoRepository;
	}

	@Override
	public BaseConvertI<ReptileSaleInfoModel, ReptileSaleInfoView> getConvert() {
		return new ReptileSaleInfoConvert();
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException{
		ResultBean result = new ResultBean();

		ReptileSaleInfoModel reptileSaleInfoModel = this.findById(id);
		reptileSaleInfoModel.setIsDisable(!reptileSaleInfoModel.getIsDisable());
		edit(reptileSaleInfoModel, sessionUser);

		result.setStatus(ResultBean.SUCCESS);
		result.setMsg(I18nHelper.getI18nByKey("message.adjustment_success", sessionUser));
		return result;
	}

	@Override
	public void BjSaleInfo(SessionUser user) throws ProjectException {
		new BJHouseSaleInfoSpider().saveBJHouseSaleInfo(reptileServiceManage, user);
	}

	@Override
	public Object[] selectSaleInfo(ReptileSaleInfoView view, SessionUser user) throws ProjectException {
		
		try {
			StringBuilder whereSql = new StringBuilder("SELECT SUM(CAST(_all_area AS SIGNED)) as area ,SUM(CAST(_all_number AS SIGNED)) as number FROM reptile_sale_info WHERE 1 = 1 ");
			if(StringUtils.isNotBlank(view.getCity())) {
				whereSql.append(" AND _house_name = " + view.getCity() + " ");
			}
			if(StringUtils.isNotBlank(view.getStartTime()) && StringUtils.isNotBlank(view.getEndTime())) {
				whereSql.append(" AND _info_updata_time between ' " + view.getStartTime() + " ' and ' " +  view.getEndTime() + "' " );
			}
			//近一周
			if(StringUtils.equals(view.getDateType(), "近一周")) {
				whereSql.append(" AND date_sub(CURDATE(),INTERVAL 7 DAY) <= DATE(_info_updata_time) ");
			}
			//本月
			if(StringUtils.equals(view.getDateType(), "本月")) {
				whereSql.append(" AND DATE_FORMAT( _info_updata_time, '%Y%m' ) = DATE_FORMAT( CURDATE() , '%Y%m' ) ");
			}
			//上一月
			if(StringUtils.equals(view.getDateType(), "上一月")) {
				whereSql.append(" AND PERIOD_DIFF( date_format( now( ) , '%Y%m' ) , date_format( _info_updata_time, '%Y%m' ) ) =1 ");
			}
			log.info("### ƴװsql ### begin");
			Query query =em.createNativeQuery(whereSql.toString());
			log.info("### ƴװsql ### end");
			List<Integer[]> intList = query.getResultList();
			Object[] obj = intList.toArray();
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


}
