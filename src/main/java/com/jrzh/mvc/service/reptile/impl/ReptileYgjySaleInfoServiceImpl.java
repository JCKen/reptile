package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.factory.gz.GZNewhouseDealEveryday;
import com.jrzh.factory.gz.GzYgjySaleInfoSpider;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileYgjySaleInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileYgjySaleInfoModel;
import com.jrzh.mvc.repository.reptile.ReptileYgjySaleInfoRepository;
import com.jrzh.mvc.search.reptile.ReptileYgjySaleInfoSearch;
import com.jrzh.mvc.service.reptile.ReptileYgjySaleInfoServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileYgjySaleInfoView;

@Service("reptileYgjySaleInfoService")
public class ReptileYgjySaleInfoServiceImpl extends
		BaseServiceImpl<ReptileYgjySaleInfoModel, ReptileYgjySaleInfoSearch, ReptileYgjySaleInfoView> implements
		ReptileYgjySaleInfoServiceI {

	@Autowired
	private ReptileYgjySaleInfoRepository reptileYgjySaleInfoRepository;
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	@Override
	public BaseRepository<ReptileYgjySaleInfoModel,Serializable> getDao() {
		return reptileYgjySaleInfoRepository;
	}

	@Override
	public BaseConvertI<ReptileYgjySaleInfoModel, ReptileYgjySaleInfoView> getConvert() {
		return new ReptileYgjySaleInfoConvert();
	}

	@Override
	public void addYgjySaleData(SessionUser user) throws ProjectException {
		//GzYgjySaleInfoSpider.saveYgjySaleInfo(reptileServiceManage, user);
		GZNewhouseDealEveryday.spider(reptileServiceManage, user);
	}

}
