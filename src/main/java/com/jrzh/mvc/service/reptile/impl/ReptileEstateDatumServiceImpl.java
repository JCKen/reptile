package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrzh.bean.macroscopic.estate.DataNodeBean;
import com.jrzh.bean.macroscopic.estate.NodesBean;
import com.jrzh.bean.macroscopic.estate.WdBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.factory.cd.CDAreaInfoSpider;
import com.jrzh.factory.gj.EstateBase;
import com.jrzh.factory.gz.GzMacroscopicSpider;
import com.jrzh.factory.sh.ShMacroscopicSpider;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileEstateDatumConvert;
import com.jrzh.mvc.model.reptile.ReptileEstateDatumModel;
import com.jrzh.mvc.model.reptile.ReptileEstateZbModel;
import com.jrzh.mvc.repository.reptile.ReptileEstateDatumRepository;
import com.jrzh.mvc.search.reptile.ReptileEstateDatumSearch;
import com.jrzh.mvc.search.reptile.ReptileEstateZbSearch;
import com.jrzh.mvc.service.reptile.ReptileEstateDatumServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileEstateDatumView;

@Service("reptileEstateDatumService")
public class ReptileEstateDatumServiceImpl
		extends BaseServiceImpl<ReptileEstateDatumModel, ReptileEstateDatumSearch, ReptileEstateDatumView>
		implements ReptileEstateDatumServiceI {

	@Autowired
	private ReptileServiceManage reptileServiceManage;

	@Autowired
	private ReptileEstateDatumRepository reptileEstateDatumRepository;

	@Override
	public BaseRepository<ReptileEstateDatumModel, Serializable> getDao() {
		return reptileEstateDatumRepository;
	}

	private static Log log = LogFactory.getLog(CDAreaInfoSpider.class);

	@Override
	public BaseConvertI<ReptileEstateDatumModel, ReptileEstateDatumView> getConvert() {
		return new ReptileEstateDatumConvert();
	}

	@Override
	public void saveEstateInvestData(String classify, String menu, String url, SessionUser systemUser)
			throws ProjectException {
		ReptileEstateZbSearch zbSearch = new ReptileEstateZbSearch();
		ReptileEstateDatumSearch search = new ReptileEstateDatumSearch();
		List<DataNodeBean> list = EstateBase.getData(url);
		List<NodesBean> zbs = EstateBase.getZb(url);
		if (zbs != null) {
			// 添加指标
			for (NodesBean zb : zbs) {
				zbSearch.setEqualCode(zb.getCode());
				ReptileEstateZbModel zbModel = reptileServiceManage.reptileEstateZbService.findBySearch(zbSearch);
				if (zbModel != null) {
					continue;
				}
				if (filterZb(zb.getCname()))
					continue;
				zbModel = new ReptileEstateZbModel();
				zbModel.setZb(zb.getCname());
				zbModel.setCode(zb.getCode());
				zbModel.setUnit(zb.getUnit());
				zbModel.setClassify(classify);
				zbModel.setCity("全国");
				zbModel.setMenu(menu);
				reptileServiceManage.reptileEstateZbService.add(zbModel, systemUser);
			}
		}
		if (list != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMM");
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.MONTH, -1);
			Date m = c.getTime();
			String mon = format.format(m);
			search.setEqualTime(mon);
			search.setEqualData("0");
			List<ReptileEstateDatumModel> times = reptileServiceManage.reptileEstateDatumService
					.findListBySearch(search);
			if (times.size() > 0) {
				Set<ReptileEstateDatumModel> models = new HashSet(times);
				reptileServiceManage.reptileEstateDatumService.deletes(models, systemUser); // 先删除上个月的数据(数据为0)
			}
			// 添加数据
			search = new ReptileEstateDatumSearch();
			for (DataNodeBean value : list) {
				List<WdBean> wds = value.getWds();
				search.setEqualCode(wds.get(0).getValuecode());
				search.setEqualTime(wds.get(1).getValuecode());
				ReptileEstateDatumModel model = reptileServiceManage.reptileEstateDatumService.findBySearch(search);
				if (model != null) {
					continue;
				}
				model = new ReptileEstateDatumModel();
				model.setTime(wds.get(1).getValuecode());
				model.setCode(wds.get(0).getValuecode());
				model.setData(value.getData().getData());
				reptileServiceManage.reptileEstateDatumService.add(model, systemUser);
			}
		}
	}

	// 过滤掉不需要的指标 直接往数组里加就行了
	public boolean filterZb(String zb) {
		String[] zbs = { "男性人口", "女性人口", "房地产开发企业自开始建设至本年底累计完成投资", "房地产开发企业建筑安装工程本年完成投资额", "房地产开发企业设备工器具购置本年完成投资额",
				"房地产开发企业其他费用本年完成投资额" };
		for (int i = 0; i < zbs.length; i++) {
			if (StringUtils.equals(zb, zbs[i])) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void saveSHMacroscopicData(SessionUser systemUser) throws ProjectException {
		List<ReptileEstateZbModel> zbList = ShMacroscopicSpider.getZB();
		ReptileEstateZbSearch zbSearch = new ReptileEstateZbSearch();
		for (ReptileEstateZbModel zbModel : zbList) {
			zbSearch.setEqualCode(zbModel.getCode());
			if (reptileServiceManage.reptileEstateZbService.findBySearch(zbSearch) == null) {
				reptileServiceManage.reptileEstateZbService.add(zbModel, systemUser);
			}
		}
		List<ReptileEstateDatumModel> datas = ShMacroscopicSpider.getDate();
		if (datas.size() > 0 || datas != null) {
			for (ReptileEstateDatumModel model : datas) {
				ReptileEstateDatumSearch search = new ReptileEstateDatumSearch();
				search.setEqualCode(model.getCode());
				search.setEqualTime(model.getTime());
				ReptileEstateDatumModel model2 = reptileServiceManage.reptileEstateDatumService.findBySearch(search);
				if (model2 != null) {
					continue;
				}
				reptileServiceManage.reptileEstateDatumService.add(model, systemUser);
			}
		}
	}

	@Override
	public void saveGzMacroscopicData(SessionUser systemUser) throws ProjectException {
		List<ReptileEstateZbModel> zbList = GzMacroscopicSpider.getAllZb();
		ReptileEstateZbSearch zbSearch = new ReptileEstateZbSearch();
		for (ReptileEstateZbModel zbModel : zbList) {
			zbSearch.setEqualCode(zbModel.getCode());
			if (reptileServiceManage.reptileEstateZbService.findBySearch(zbSearch) == null) {
				reptileServiceManage.reptileEstateZbService.add(zbModel, systemUser);
			}
		}
		GzMacroscopicSpider.getUrl();
		List<ReptileEstateDatumModel> datas = GzMacroscopicSpider.dataList;
		if (datas.size() > 0 || datas != null) {
			for (ReptileEstateDatumModel model : datas) {
				ReptileEstateDatumSearch search = new ReptileEstateDatumSearch();
				search.setEqualCode(model.getCode());
				search.setEqualTime(model.getTime());
				ReptileEstateDatumModel model2 = reptileServiceManage.reptileEstateDatumService.findBySearch(search);
				if (model2 != null) {
					continue;
				}
				reptileServiceManage.reptileEstateDatumService.add(model, systemUser);
			}
		}
	}
}
