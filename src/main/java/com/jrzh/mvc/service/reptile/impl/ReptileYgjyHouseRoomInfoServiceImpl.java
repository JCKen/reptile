package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.mvc.convert.reptile.ReptileYgjyHouseRoomInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseRoomInfoModel;
import com.jrzh.mvc.repository.reptile.ReptileYgjyHouseRoomInfoRepository;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseRoomInfoSearch;
import com.jrzh.mvc.service.reptile.ReptileYgjyHouseRoomInfoServiceI;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseRoomInfoView;

@Service("reptileYgjyHouseRoomInfoService")
public class ReptileYgjyHouseRoomInfoServiceImpl extends
		BaseServiceImpl<ReptileYgjyHouseRoomInfoModel, ReptileYgjyHouseRoomInfoSearch, ReptileYgjyHouseRoomInfoView> implements
		ReptileYgjyHouseRoomInfoServiceI {

	@Autowired
	private ReptileYgjyHouseRoomInfoRepository reptileYgjyHouseRoomInfoRepository;

	@Override
	public BaseRepository<ReptileYgjyHouseRoomInfoModel,Serializable> getDao() {
		return reptileYgjyHouseRoomInfoRepository;
	}

	@Override
	public BaseConvertI<ReptileYgjyHouseRoomInfoModel, ReptileYgjyHouseRoomInfoView> getConvert() {
		return new ReptileYgjyHouseRoomInfoConvert();
	}

	@Override
	public Integer getVolume(String time,String houseId) throws ProjectException {
		Integer count = null;
		if(StringUtils.equals(time, "近一周")) {
			Integer thisWeekCount  = reptileYgjyHouseRoomInfoRepository.findThisWeekCount(houseId);
			Integer lastWeekCount = reptileYgjyHouseRoomInfoRepository.findLastWeekCount(houseId);
			count = thisWeekCount - lastWeekCount;
		}
		if(StringUtils.equals(time, "近一个月")) {
			Integer thisMonthCount  = reptileYgjyHouseRoomInfoRepository.findThisMonth(houseId);
			Integer lastMonthCount = reptileYgjyHouseRoomInfoRepository.findLastMonth(houseId);
			count = thisMonthCount - lastMonthCount;	
		}
		if(StringUtils.equals(time, "近三个月")) {
			Integer thisMonthCount  = reptileYgjyHouseRoomInfoRepository.findThisMonth(houseId);
			Integer threeMonthCount = reptileYgjyHouseRoomInfoRepository.findThreeMonthCount(houseId);
			count = thisMonthCount - threeMonthCount;
		}
		if(StringUtils.equals(time, "近六个月")) {
			Integer thisMonthCount  = reptileYgjyHouseRoomInfoRepository.findThisMonth(houseId);
			Integer sixMonthCount = reptileYgjyHouseRoomInfoRepository.findSixMonthCount(houseId);
			count = thisMonthCount - sixMonthCount;
		}
		if(StringUtils.equals(time, "近一年")) {
			Integer thisYearCount  = reptileYgjyHouseRoomInfoRepository.findThisYearCount(houseId);
			Integer lastYearCount = reptileYgjyHouseRoomInfoRepository.findLastYearCount(houseId);
			count = thisYearCount - lastYearCount;
		}
		return count;
	}

	@Override
	public Integer selectMaxLouCeng(String houseId, String floorId) throws ProjectException {
		return reptileYgjyHouseRoomInfoRepository.selectMaxLouCeng(houseId,floorId);
	}

}
