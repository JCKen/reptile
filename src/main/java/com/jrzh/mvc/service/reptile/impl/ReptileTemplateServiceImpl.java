package com.jrzh.mvc.service.reptile.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.jrzh.bean.jpexcel.JpEsfHouseDataBean;
import com.jrzh.bean.jpexcel.JpExcelBean;
import com.jrzh.bean.jpexcel.JpNewHouseDataBean;
import com.jrzh.bean.jpexcel.JpTudiDataBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.tools.CollectionTools;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.config.ProjectConfigration;
import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.controller.reptile.front.ReptileCorrectionFactorController;
import com.jrzh.mvc.convert.reptile.ReptileTemplateConvert;
import com.jrzh.mvc.convert.reptile.ReptileTemplateSumInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileCorrectionFactorModel;
import com.jrzh.mvc.model.reptile.ReptileCorrectionSetModel;
import com.jrzh.mvc.model.reptile.ReptileTemplateInfoModel;
import com.jrzh.mvc.model.reptile.ReptileTemplateModel;
import com.jrzh.mvc.model.reptile.ReptileTemplateSumInfoModel;
import com.jrzh.mvc.model.sys.FileModel;
import com.jrzh.mvc.repository.reptile.ReptileTemplateRepository;
import com.jrzh.mvc.search.reptile.ReptileCorrectionFactorSearch;
import com.jrzh.mvc.search.reptile.ReptileCorrectionSetSearch;
import com.jrzh.mvc.search.reptile.ReptileTemplateInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileTemplateSearch;
import com.jrzh.mvc.search.reptile.ReptileTemplateSumInfoSearch;
import com.jrzh.mvc.service.reptile.ReptileTemplateServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.service.sys.manage.SysServiceManage;
import com.jrzh.mvc.view.reptile.ReptileCorrectionSetView;
import com.jrzh.mvc.view.reptile.ReptileTemplateInfoView;
import com.jrzh.mvc.view.reptile.ReptileTemplateSumInfoView;
import com.jrzh.mvc.view.reptile.ReptileTemplateView;
import com.jrzh.tools.Base64ToImg;

@Service("reptileTemplateService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ReptileTemplateServiceImpl
		extends BaseServiceImpl<ReptileTemplateModel, ReptileTemplateSearch, ReptileTemplateView>
		implements ReptileTemplateServiceI {

	@Autowired
	private ReptileTemplateRepository reptileTemplateRepository;

	@Autowired
	private ReptileServiceManage reptileServiceManage;

	@Autowired
	private ProjectConfigration projectConfigration;

	@Autowired
	private SysServiceManage sysServiceManage;

	@Override
	public BaseRepository<ReptileTemplateModel, Serializable> getDao() {
		return reptileTemplateRepository;
	}

	@Override
	public BaseConvertI<ReptileTemplateModel, ReptileTemplateView> getConvert() {
		return new ReptileTemplateConvert();
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	public ResultBean changeStatus(String id, SessionUser sessionUser) throws ProjectException {
		ResultBean result = new ResultBean();

		ReptileTemplateModel reptileTemplateModel = this.findById(id);
		reptileTemplateModel.setIsDisable(!reptileTemplateModel.getIsDisable());
		edit(reptileTemplateModel, sessionUser);

		result.setStatus(ResultBean.SUCCESS);
		result.setMsg(I18nHelper.getI18nByKey("message.adjustment_success", sessionUser));
		return result;
	}

	@Override
	public ReptileTemplateModel saveTemplate(ReptileTemplateView templateView, ReptileTemplateInfoView[] views,
			SessionUser user) throws ProjectException {
		ReptileTemplateModel model = new ReptileTemplateModel();
		model.setUserId(user.getId());
		model.setHouseType(templateView.getHouseType());
		model.setName(templateView.getName());
		model.setType(templateView.getType());
		reptileServiceManage.reptileTemplateService.add(model, user);
		for (ReptileTemplateInfoView view : views) {
			view.setUserId(user.getId());
			view.setTemplateId(model.getId());
			reptileServiceManage.reptileTemplateInfoService.addByView(view, user);
		}
		return model;
	}

	@Override
	public List<ReptileTemplateModel> transferTemplate(String houseType, String type, SessionUser user)
			throws ProjectException {
		String id = user.getId();
		ReptileTemplateSearch search = new ReptileTemplateSearch();
		search.setOrder("desc");
		search.setSort("updateTime");
		if (StringUtils.isNotBlank(houseType)) {
			search.setEqualHouseType(houseType);
		}
		search.setEqualUserId(id);
		if (StringUtils.isNotBlank(type)) {
			search.setEqualType(type);
		}
		List<ReptileTemplateModel> modelList = reptileServiceManage.reptileTemplateService.findListBySearch(search);
		return modelList;
	}

	@Override
	public List<ReptileTemplateInfoModel> transferTemplateInfo(String[] templateIds, SessionUser user)
			throws ProjectException {
		ReptileTemplateInfoSearch search = new ReptileTemplateInfoSearch();
		/*search.setOrder("desc");
		search.setSort("createTime");*/
		StringBuffer ids = new StringBuffer();
		if(templateIds!=null) {
			for (String id : templateIds) {
				ids.append(id+",");
			}
			
			ids = ids.deleteCharAt(ids.length()-1);
		}
		
		
		search.setInTemplateIds(ids.toString());
		List<ReptileTemplateInfoModel> modelList = reptileServiceManage.reptileTemplateInfoService
				.findListBySearch(search);
		return modelList;
	}

	@Override
	public void deleteTemplate(String id, SessionUser user) throws ProjectException {
		ReptileTemplateModel template = reptileServiceManage.reptileTemplateService.findById(id);
		if (template != null) {
			reptileServiceManage.reptileTemplateService.delete(id, user);
		}
		ReptileTemplateInfoSearch search = new ReptileTemplateInfoSearch();
		search.setEqualUserId(template.getUserId());
		search.setEqualTemplateId(template.getId());
		List<ReptileTemplateInfoModel> templateInfos = reptileServiceManage.reptileTemplateInfoService.list(search);
		if (CollectionTools.isNotBlank(templateInfos)) {
			for (ReptileTemplateInfoModel info : templateInfos) {
				reptileServiceManage.reptileTemplateInfoService.delete(info.getId(), user);
			}
		}

		ReptileTemplateSumInfoSearch sumInfoSearch = new ReptileTemplateSumInfoSearch();
		search.setEqualUserId(template.getUserId());
		search.setEqualTemplateId(template.getId());
		List<ReptileTemplateSumInfoModel> templateSumInfos = reptileServiceManage.reptileTemplateSumInfoService
				.list(sumInfoSearch);
		if (CollectionTools.isNotBlank(templateInfos)) {
			for (ReptileTemplateSumInfoModel sumiInfo : templateSumInfos) {
				reptileServiceManage.reptileTemplateSumInfoService.delete(sumiInfo.getId(), user);
			}
		}
	}

	@Override
	public ReptileTemplateModel saveAllTemplate(ReptileTemplateView templateView,
			ReptileTemplateSumInfoView templateSumInfoView, ReptileTemplateInfoView[] newViews,
			ReptileTemplateInfoView[] esfViews, ReptileTemplateInfoView[] tudiViews, SessionUser user,ProjectConfigration projectConfigration)
			throws ProjectException {
		// 添加模板
		ReptileTemplateModel model = new ReptileTemplateModel();
		model.setUserId(user.getId());
		model.setHouseType(templateView.getHouseType());
		model.setName(templateView.getName());
		model.setType(templateView.getType());
		// 创建文件
		JpExcelBean dataBean = createJpExcelBean(templateView, templateSumInfoView, newViews, esfViews, tudiViews,
				user);
		String fileKey = "jingping";
		String dateDir = DateUtil.format(new Date(), "yyyy-MM-dd");
		String ctxPath = projectConfigration.getFileRootUrl() + "file/" + fileKey + "/" + dateDir + "/";
		String mapPath = projectConfigration.mappingUrl + "file/" + fileKey + "/" + dateDir + "/";
		File file = new File(ctxPath);
		if (!file.exists())
			file.mkdirs();
		String uuid = UUID.randomUUID().toString().replaceAll("\\-", "");// 返回一个随机UUID。
		String suffix = ".xls";
		String newFileName = "竞品 -" + uuid + suffix;// 构成新文件名。
		String fileUrl = mapPath + "/" + newFileName;
		model.setFileUrl(fileUrl);
		
		//生成竞品地图
		String base64 = templateView.getImgBase64();
		model.setImgBase64(Base64ToImg.GenerateImage(projectConfigration,base64));
		
		// 添加模板
		reptileServiceManage.reptileTemplateService.add(model, user);

		FileModel fileModel = new FileModel();
		fileModel.setFormId(model.getId());
		fileModel.setKey(fileKey);
		fileModel.setUrl(mapPath + newFileName);
		fileModel.setName(newFileName);
		fileModel.setType(suffix);
		fileModel.setModel("ReptileTemplateModel");
		sysServiceManage.fileService.add(fileModel, user);
		// 写入数据
		createTemplateFile(model.getId(), ctxPath, newFileName, dataBean, user);

		// 添加计算汇总
		ReptileTemplateSumInfoModel sumInfo = new ReptileTemplateSumInfoConvert().addConvert(templateSumInfoView);
		sumInfo.setUserId(user.getId());
		sumInfo.setTemplateId(model.getId());
		reptileServiceManage.reptileTemplateSumInfoService.add(sumInfo, user);

		// 添加各类型
		for (ReptileTemplateInfoView view : newViews) {
			view.setUserId(user.getId());
			view.setTemplateId(model.getId());
			reptileServiceManage.reptileTemplateInfoService.addByView(view, user);
		}
		for (ReptileTemplateInfoView view : esfViews) {
			view.setUserId(user.getId());
			view.setTemplateId(model.getId());
			reptileServiceManage.reptileTemplateInfoService.addByView(view, user);
		}
		for (ReptileTemplateInfoView view : tudiViews) {
			view.setUserId(user.getId());
			view.setTemplateId(model.getId());
			reptileServiceManage.reptileTemplateInfoService.addByView(view, user);
		}

		return model;
	}

	@Override
	public JpExcelBean createJpExcelBean(ReptileTemplateView templateView, ReptileTemplateSumInfoView sumInfoView,
			ReptileTemplateInfoView[] newViews, ReptileTemplateInfoView[] esfViews, ReptileTemplateInfoView[] tudiViews,
			SessionUser user) throws ProjectException {
		// 一手房数据
		JpNewHouseDataBean newHouseData = new JpNewHouseDataBean();
		if (newViews != null && newViews.length > 0) {
			newHouseData.setNewViews(Arrays.asList(newViews));
			newHouseData.setNewHouseAmt(sumInfoView.getOneHouseAmt());
			newHouseData.setNewHouseRatio(sumInfoView.getOneHouseRatio());
		}
		// 二手房数据
		JpEsfHouseDataBean esfHouseData = new JpEsfHouseDataBean();
		if (esfViews != null && esfViews.length > 0) {
			esfHouseData.setEsfViews(Arrays.asList(esfViews));
			esfHouseData.setEsfHouseAmt(sumInfoView.getTwoHouseAmt());
			esfHouseData.setEsfHouseRatio(sumInfoView.getTwoHouseRatio());
		}
		// 土地数据
		JpTudiDataBean tudiData = new JpTudiDataBean();
		if (tudiViews != null && tudiViews.length > 0) {
			tudiData.setTudiViews(Arrays.asList(tudiViews));
			tudiData.setTudiHouseAmt(sumInfoView.getLandAmt());
			tudiData.setTudiHouseRatio(sumInfoView.getLandRatio());
		}
		JpExcelBean dataBean = new JpExcelBean();
		dataBean.setAmt(sumInfoView.getReferAmt());
		dataBean.setEsfHouseData(esfHouseData);
		dataBean.setNewHouseData(newHouseData);
		dataBean.setTudiData(tudiData);
		return dataBean;
	}

	// 查询修正类和修正项
	private Map<ReptileCorrectionFactorModel, List<ReptileCorrectionFactorModel>> selectCorr() {
		Map<ReptileCorrectionFactorModel, List<ReptileCorrectionFactorModel>> models = new LinkedHashMap<>();
		try {
			ReptileCorrectionFactorSearch search = new ReptileCorrectionFactorSearch();
			search.setIsNull("pid");
			search.setSort("createTime");
			search.setOrder("asc");
			List<ReptileCorrectionFactorModel> parents = reptileServiceManage.reptileCorrectionFactorService
					.findListBySearch(search);
			search.setIsNull("");
			for (ReptileCorrectionFactorModel parent : parents) {
				search.setEqualPid(parent.getId());
				models.put(parent, reptileServiceManage.reptileCorrectionFactorService.findListBySearch(search));
			}
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		}
		return models;

	}

	// excel 合并单元格 边框设置
	private void setRegionBorder(CellRangeAddress region, Sheet sheet) {
		RegionUtil.setBorderBottom(BorderStyle.THIN, region, sheet); // 下边框
		RegionUtil.setBorderLeft(BorderStyle.THIN, region, sheet); // 左边框
		RegionUtil.setBorderRight(BorderStyle.THIN, region, sheet); // 有边框
		RegionUtil.setBorderTop(BorderStyle.THIN, region, sheet); // 上边框
	}

	@Override
	public void createTemplateFile(String templateId, String ctxUrl, String fileName, JpExcelBean dataBean,
			SessionUser user) throws ProjectException {
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("竞品");

		// 单元格样式设置
		HSSFCellStyle baseStyle = wb.createCellStyle();

		// setBorder 设置要用于单元格边框的边框类型
		baseStyle.setBorderBottom(BorderStyle.THIN);
		baseStyle.setBorderLeft(BorderStyle.THIN);
		baseStyle.setBorderRight(BorderStyle.THIN);
		baseStyle.setBorderTop(BorderStyle.THIN);

		// 设置自动换行
		baseStyle.setWrapText(true);

		// setAlignment 设置单元格的水平对齐类型
		baseStyle.setAlignment(HorizontalAlignment.CENTER);// 水平居中
		baseStyle.setVerticalAlignment(VerticalAlignment.CENTER); // 垂直居中

		// 数据文本样式
		HSSFCellStyle style_text = baseStyle;

		// 标题类单元格字体设置
		HSSFCellStyle style = baseStyle;
		Font fontStyle = wb.createFont(); // 字体样式
		fontStyle.setBold(true); // 加粗
		fontStyle.setFontName("宋体"); // 字体
		fontStyle.setFontHeightInPoints((short) 11); // 大小
		// 将字体样式添加到单元格样式中
		// style.setFont(fontStyle);

		// 在工作表中创建一个新行并返回高级HSSFRow对象
		int rowsNum = 0;
		int index_no = 0;
		HSSFRow row = sheet.createRow(rowsNum);
		HSSFCell cell = row.createCell(0);
		cell.setCellValue("一手房对标价 （元/㎡）");
		cell.setCellStyle(style);

		// 数据
		JpNewHouseDataBean newHouse = dataBean.getNewHouseData();
		JpEsfHouseDataBean esfHouse = dataBean.getEsfHouseData();
		JpTudiDataBean tudi = dataBean.getTudiData();
		// 计算每个项目个数
		int currInde;
		if (newHouse.getNewViews() != null) {
			currInde = newHouse.getNewViews().size();
		} else {
			currInde = 0;
		}
		int esfCurrIndex;
		if (esfHouse.getEsfViews() != null) {
			esfCurrIndex = esfHouse.getEsfViews().size();
		} else {
			esfCurrIndex = 0;
		}
		int tudiCurrIndex;
		if (tudi.getTudiViews() != null) {
			tudiCurrIndex = tudi.getTudiViews().size();
		} else {
			tudiCurrIndex = 0;
		}

		// 查询修正类型 及 修正项个数 绘画合并单元格
		Map<ReptileCorrectionFactorModel, List<ReptileCorrectionFactorModel>> map_corr = selectCorr();
		// 创建新的单元格范围。
		CellRangeAddress cra = null;
		int firstRow = 0;
		int lastRow = 0;
		int firstCol = 0;
		int lastCol = 0;

		// 一手房表格
		rowsNum = newHouse_excel(map_corr, firstRow, lastRow, firstCol, lastCol, index_no, rowsNum, row, cell, cra,
				sheet, wb, style_text, style, currInde, newHouse);

		// 二手房表格
		rowsNum = rowsNum + 4;
		rowsNum = esfHouse_excel(map_corr, firstRow, lastRow, firstCol, lastCol, index_no, rowsNum, row, cell, cra,
				sheet, wb, style_text, style, esfCurrIndex, esfHouse);

		// 土地
		rowsNum = rowsNum + 4;
		tudi_excel(firstRow, lastRow, firstCol, lastCol, index_no, rowsNum, row, cell, cra, sheet, wb, style_text,
				style, tudiCurrIndex, tudi);

		// 生成文件
		try {
			FileOutputStream fout = new FileOutputStream(ctxUrl + "/" + fileName);
			wb.write(fout);
			fout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 一手房excel表格
	private int newHouse_excel(Map<ReptileCorrectionFactorModel, List<ReptileCorrectionFactorModel>> map_corr,
			int firstRow, int lastRow, int firstCol, int lastCol, int index_no, int rowsNum, HSSFRow row, HSSFCell cell,
			CellRangeAddress cra, HSSFSheet sheet, HSSFWorkbook wb, HSSFCellStyle style_text, HSSFCellStyle style,
			int currInde, JpNewHouseDataBean newHouse) throws ProjectException {
		index_no = 0;
		// 创建合并行 起始行号，终止行号， 起始列号，终止列号
		rowsNum++;
		row = sheet.createRow(rowsNum);
		// 表格第几列下标
		cell = row.createCell(index_no);
		cell.setCellValue("项目/系数");
		cell.setCellStyle(style);
		for (ReptileCorrectionFactorModel parent : map_corr.keySet()) {
			// 一手房不显示“二手房”相关的修正项
			if (!StringUtils.contains(parent.getCorrectionName(), "二手房")) {
				int columns = 1;
				for (int i = 1; i < map_corr.get(parent).size(); i++) {
					columns++;
				}
				firstRow = rowsNum;
				lastRow = rowsNum;
				firstCol = index_no + 1;
				lastCol = index_no + columns;
				if (columns > 1) {
					cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
					sheet.addMergedRegion(cra);
					cell = row.createCell(firstCol);
					cell.setCellValue(parent.getCorrectionName());
					cell.setCellStyle(style);
					// 使用RegionUtil类为合并后的单元格添加边框
					setRegionBorder(cra, sheet);

				} else {
					cell = row.createCell(firstCol);
					cell.setCellValue(parent.getCorrectionName());
					cell.setCellStyle(style);
				}
				index_no = lastCol;
			}
		}
		// 一手房 系数计算及价格显示
		// 综合系数
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("综合系数");
		cell.setCellStyle(style);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 市场价格
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("市场价格（元/㎡）");
		cell.setCellStyle(style);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 本项目对比定价
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("本项目对比定价 （元/㎡）");
		cell.setCellStyle(style);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 竞品权重
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("竞品权重");
		cell.setCellStyle(style);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 市场加权价
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("市场加权价（元/㎡）");
		cell.setCellStyle(style);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 市场价格测算
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("市场价格测算（元/㎡）");
		cell.setCellStyle(style);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 一手房市场价格权重
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("一手房市场价格权重");
		cell.setCellStyle(style);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 换行
		rowsNum++;

		index_no = 0;
		row = sheet.createRow(rowsNum);
		cell = row.createCell(index_no);
		cell.setCellValue("");
		cell.setCellStyle(style);

		// 展示一手房详细修正项名称
		for (ReptileCorrectionFactorModel parent : map_corr.keySet()) {
			// 一手房不显示“二手房”相关的修正项
			if (!StringUtils.contains(parent.getCorrectionName(), "二手房")) {
				int length = map_corr.get(parent).size();
				for (int i = 0; i < length; i++) {
					index_no++;
					cell = row.createCell(index_no);
					cell.setCellValue(map_corr.get(parent).get(i).getCorrectionName());
					cell.setCellStyle(style);
				}
			}
		}

		// 换行
		rowsNum++;

		index_no = 0;
		row = sheet.createRow(rowsNum);
		cell = row.createCell(index_no);
		cell.setCellValue("因素设定范围");
		cell.setCellStyle(style);

		// 展示一手房详细修正项范围
		for (ReptileCorrectionFactorModel parent : map_corr.keySet()) {
			// 一手房不显示“二手房”相关的修正项
			if (!StringUtils.contains(parent.getCorrectionName(), "二手房")) {
				int length = map_corr.get(parent).size();
				for (int i = 0; i < length; i++) {
					index_no++;
					cell = row.createCell(index_no);
					cell.setCellValue(map_corr.get(parent).get(i).getCorrectionMin() + "-"
							+ map_corr.get(parent).get(i).getCorrectionMax());
					cell.setCellStyle(style);
				}
			}
		}

		// 换行
		rowsNum++;

		index_no = 0;
		row = sheet.createRow(rowsNum);
		cell = row.createCell(index_no);
		cell.setCellValue("本项目");
		cell.setCellStyle(style);

		// “本项目行”默认值设定
		for (ReptileCorrectionFactorModel parent : map_corr.keySet()) {
			// 一手房不显示“二手房”相关的修正项
			if (!StringUtils.contains(parent.getCorrectionName(), "二手房")) {
				int length = map_corr.get(parent).size();
				for (int i = 0; i < length; i++) {
					index_no++;
					cell = row.createCell(index_no);
					cell.setCellValue(1);
					cell.setCellStyle(style_text);
				}
			}
		}
		// 本项目-综合系数
		index_no++;
		cell = row.createCell(index_no);
		cell.setCellValue("1");
		cell.setCellStyle(style_text);
		index_no++;

		// 本项目-市场价格
		cell = row.createCell(index_no);
		cell.setCellValue("-");
		cell.setCellStyle(style_text);
		index_no++;

		// 本项目-本项目对比定价
		cell = row.createCell(index_no);
		cell.setCellValue("-");
		cell.setCellStyle(style_text);
		index_no++;

		// 本项目-竞品权重
		cell = row.createCell(index_no);
		cell.setCellValue("-");
		cell.setCellStyle(style_text);
		index_no++;

		// 本项目-市场加权价
		cell = row.createCell(index_no);
		cell.setCellValue("-");
		cell.setCellStyle(style_text);
		index_no++;

		// 标记储存的对象是否为空
		String id;
		if (newHouse.getNewViews() != null) {
			id = newHouse.getNewViews().get(0).getHouseId();
		} else {
			id = null;
			newHouse.setNewHouseAmt("0.00元/㎡");
			newHouse.setNewHouseRatio("70");
		}

		// 市场价格测算值
		// 判断是否需要合并单元格
		if (currInde > 0 && StringUtils.isNotBlank(id)) {
			firstRow = rowsNum;
			lastRow = rowsNum + currInde;
			firstCol = index_no;
			lastCol = index_no;
			cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
			sheet.addMergedRegion(cra);
			cell = row.createCell(index_no);
			if (StringUtils.contains(newHouse.getNewHouseAmt(), "元")) {
				cell.setCellValue(newHouse.getNewHouseAmt().substring(0, newHouse.getNewHouseAmt().indexOf("元")));
			} else {
				cell.setCellValue(newHouse.getNewHouseAmt());
			}
			cell.setCellStyle(style_text);
			// 使用RegionUtil类为合并后的单元格添加边框
			setRegionBorder(cra, sheet);
		} else {
			cell = row.createCell(index_no);
			if (StringUtils.contains(newHouse.getNewHouseAmt(), "元")) {
				cell.setCellValue(newHouse.getNewHouseAmt().substring(0, newHouse.getNewHouseAmt().indexOf("元")));
			} else {
				cell.setCellValue(newHouse.getNewHouseAmt());
			}
			cell.setCellStyle(style_text);
		}
		index_no++;

		// 市场价格权重
		if (currInde > 0 && StringUtils.isNotBlank(id)) {
			firstRow = rowsNum;
			lastRow = rowsNum + currInde;
			firstCol = index_no;
			lastCol = index_no;
			cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
			sheet.addMergedRegion(cra);
			cell = row.createCell(index_no);
			cell.setCellValue(newHouse.getNewHouseRatio() + "%");
			cell.setCellStyle(style_text);
			// 使用RegionUtil类为合并后的单元格添加边框
			setRegionBorder(cra, sheet);
		} else {
			cell = row.createCell(index_no);
			cell.setCellValue(newHouse.getNewHouseRatio() + "%");
			cell.setCellStyle(style_text);
		}
		index_no++;

		// 如果没有任何楼盘信息 就不进行后续遍历了
		if (currInde < 1) {
			return rowsNum;
		} else if (StringUtils.isBlank(id)) {
			return rowsNum;
		}

		// 遍历一手竞品详细信息
		for (ReptileTemplateInfoView view : newHouse.getNewViews()) {
			rowsNum++;

			index_no = 0;
			row = sheet.createRow(rowsNum);
			cell = row.createCell(index_no);
			cell.setCellValue(view.getHouseName());
			cell.setCellStyle(style);
			index_no++;
			// 查询详细的修正项值
			ReptileCorrectionSetSearch search = new ReptileCorrectionSetSearch();
			search.setEqualHouseId(view.getHouseId());
			search.setEqualUserId(view.getUserId());
			List<ReptileCorrectionSetModel> model_set = reptileServiceManage.reptileCorrectionSetService
					.findListBySearch(search);
			for (ReptileCorrectionFactorModel parent : map_corr.keySet()) {
				// 一手房不显示“二手房”相关的修正项
				if (!StringUtils.contains(parent.getCorrectionName(), "二手房")) {
					int length = map_corr.get(parent).size();
					for (int i = 0; i < length; i++) {
						for (ReptileCorrectionSetModel model : model_set) {
							// 如果修正项的id和model的correctionId一致 则输出
							if (StringUtils.equals(map_corr.get(parent).get(i).getId(), model.getCorrectionId())) {
								cell = row.createCell(index_no);
								cell.setCellValue(model.getCorrectionValue());
								cell.setCellStyle(style_text);
								index_no++;
							}
						}
					}
				}
			}
			// 综合系数
			cell = row.createCell(index_no);
			cell.setCellValue(view.getTotalCorrectionValue());
			cell.setCellStyle(style_text);
			index_no++;

			// 市场价格
			cell = row.createCell(index_no);
			cell.setCellValue(view.getHousePrice());
			cell.setCellStyle(style_text);
			index_no++;

			// 本项目对比定价
			cell = row.createCell(index_no);
			cell.setCellValue(view.getComparePrice());
			cell.setCellStyle(style_text);
			index_no++;

			// 竞品权重
			cell = row.createCell(index_no);
			cell.setCellValue(view.getHouseWeight() + "%");
			cell.setCellStyle(style_text);
			index_no++;

			// 市场加权价
			cell = row.createCell(index_no);
			cell.setCellValue(view.getSubPrice());
			cell.setCellStyle(style_text);
			index_no++;
		}
		return rowsNum;
	}

	// 二手房excel表格
	private int esfHouse_excel(Map<ReptileCorrectionFactorModel, List<ReptileCorrectionFactorModel>> map_corr,
			int firstRow, int lastRow, int firstCol, int lastCol, int index_no, int rowsNum, HSSFRow row, HSSFCell cell,
			CellRangeAddress cra, HSSFSheet sheet, HSSFWorkbook wb, HSSFCellStyle style_text, HSSFCellStyle style,
			int esfCurrIndex, JpEsfHouseDataBean esfHouse) throws ProjectException {

		index_no = 0;
		row = sheet.createRow(rowsNum);
		cell = row.createCell(index_no);
		cell.setCellValue("二手房对标价 （元/㎡）");
		sheet.autoSizeColumn(index_no, true);
		cell.setCellStyle(style);

		// 换行
		rowsNum++;

		index_no = 0;
		row = sheet.createRow(rowsNum);
		cell = row.createCell(index_no);
		cell.setCellValue("项目/系数");
		cell.setCellStyle(style);
		for (ReptileCorrectionFactorModel parent : map_corr.keySet()) {
			int columns = 1;
			for (int i = 1; i < map_corr.get(parent).size(); i++) {
				columns++;
			}
			firstRow = rowsNum;
			lastRow = rowsNum;
			firstCol = index_no + 1;
			lastCol = index_no + columns;
			if (columns > 1) {
				cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
				sheet.addMergedRegion(cra);
				cell = row.createCell(firstCol);
				cell.setCellValue(parent.getCorrectionName());
				cell.setCellStyle(style);
				sheet.setColumnWidth(index_no, 6 * 512);
				// 使用RegionUtil类为合并后的单元格添加边框
				setRegionBorder(cra, sheet);
			} else {
				cell = row.createCell(firstCol);
				cell.setCellValue(parent.getCorrectionName());
				cell.setCellStyle(style);
				sheet.setColumnWidth(index_no, 6 * 512);
			}
			index_no = lastCol;
		}
		// 二手房综合系数
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("综合系数");
		cell.setCellStyle(style);
		sheet.autoSizeColumn(index_no, true);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 二手房 市场价格
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("市场价格（元/㎡）");
		cell.setCellStyle(style);
		sheet.autoSizeColumn(index_no, true);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 二手房 折算二手交易税费系数
		firstRow = rowsNum;
		lastRow = rowsNum + 1;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("折算二手交易税费系数");
		cell.setCellStyle(style);
		sheet.autoSizeColumn(index_no, true);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 二手房实际成交价
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("实际成交价 （元/㎡）");
		cell.setCellStyle(style);
		sheet.autoSizeColumn(index_no, true);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 二手房本项目对比定价
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("本项目对比定价 （元/㎡）");
		cell.setCellStyle(style);
		sheet.autoSizeColumn(index_no, true);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 二手房竞品权重
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("竞品权重");
		cell.setCellStyle(style);
		sheet.autoSizeColumn(index_no, true);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 二手房市场加权价
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("市场加权价（元/㎡）");
		cell.setCellStyle(style);
		sheet.autoSizeColumn(index_no, true);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 二手房市场价格测算
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("市场价格测算（元/㎡）");
		cell.setCellStyle(style);
		sheet.autoSizeColumn(index_no, true);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 二手房市场价格权重
		firstRow = rowsNum;
		lastRow = rowsNum + 2;
		firstCol = index_no + 1;
		lastCol = index_no + 1;
		cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		sheet.addMergedRegion(cra);
		cell = row.createCell(firstCol);
		cell.setCellValue("市场价格权重");
		cell.setCellStyle(style);
		// 使用RegionUtil类为合并后的单元格添加边框
		setRegionBorder(cra, sheet);
		index_no = lastCol;

		// 换行
		rowsNum++;

		index_no = 0;
		row = sheet.createRow(rowsNum);
		cell = row.createCell(index_no);
		cell.setCellValue("");
		cell.setCellStyle(style);

		// 展示二手房详细修正项名称
		for (ReptileCorrectionFactorModel parent : map_corr.keySet()) {
			int length = map_corr.get(parent).size();
			for (int i = 0; i < length; i++) {
				index_no++;
				cell = row.createCell(index_no);
				cell.setCellValue(map_corr.get(parent).get(i).getCorrectionName());
				cell.setCellStyle(style);
			}
		}

		// 换行
		rowsNum++;

		index_no = 0;
		row = sheet.createRow(rowsNum);
		cell = row.createCell(index_no);
		cell.setCellValue("因素设定范围");
		cell.setCellStyle(style);

		// 展示二手房详细修正项范围
		for (ReptileCorrectionFactorModel parent : map_corr.keySet()) {
			int length = map_corr.get(parent).size();
			for (int i = 0; i < length; i++) {
				index_no++;
				cell = row.createCell(index_no);
				cell.setCellValue(map_corr.get(parent).get(i).getCorrectionMin() + "-"
						+ map_corr.get(parent).get(i).getCorrectionMax());
				cell.setCellStyle(style);
			}
		}
		// 交易税费系数范围
		index_no = index_no + 3;
		cell = row.createCell(index_no);
		cell.setCellValue("1.01-1.2");
		cell.setCellStyle(style);

		// 换行
		rowsNum++;

		index_no = 0;
		row = sheet.createRow(rowsNum);
		cell = row.createCell(index_no);
		cell.setCellValue("本项目");
		cell.setCellStyle(style);

		// “本项目行”默认值设定
		for (ReptileCorrectionFactorModel parent : map_corr.keySet()) {
			int length = map_corr.get(parent).size();
			for (int i = 0; i < length; i++) {
				index_no++;
				cell = row.createCell(index_no);
				cell.setCellValue(1);
				cell.setCellStyle(style_text);
			}
		}
		// 本项目-综合系数
		index_no++;
		cell = row.createCell(index_no);
		cell.setCellValue("1");
		cell.setCellStyle(style_text);
		index_no++;

		// 本项目-市场价格
		cell = row.createCell(index_no);
		cell.setCellValue("-");
		cell.setCellStyle(style_text);
		index_no++;

		// 本项目-交易税费系数
		cell = row.createCell(index_no);
		cell.setCellValue("1");
		cell.setCellStyle(style_text);
		index_no++;

		// 本项目-实际成交价
		cell = row.createCell(index_no);
		cell.setCellValue("-");
		cell.setCellStyle(style_text);
		index_no++;

		// 本项目-对比定价
		cell = row.createCell(index_no);
		cell.setCellValue("-");
		cell.setCellStyle(style_text);
		index_no++;

		// 本项目-竞品权重
		cell = row.createCell(index_no);
		cell.setCellValue("-");
		cell.setCellStyle(style_text);
		index_no++;

		// 本项目-市场加权价
		cell = row.createCell(index_no);
		cell.setCellValue("-");
		cell.setCellStyle(style_text);
		index_no++;

		// 标记储存的信息中 是否为空
		String id;
		if (esfHouse.getEsfViews() != null) {
			id = esfHouse.getEsfViews().get(0).getHouseId();
		} else {
			id = null;
			esfHouse.setEsfHouseAmt("0.00元/㎡");
			esfHouse.setEsfHouseRatio("20");
		}

		// 二手房 — 市场价格测算值
		if (esfCurrIndex > 0 && StringUtils.isNotBlank(id)) {
			firstRow = rowsNum;
			lastRow = rowsNum + esfCurrIndex;
			firstCol = index_no;
			lastCol = index_no;
			cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
			sheet.addMergedRegion(cra);
			cell = row.createCell(index_no);
			if (StringUtils.contains(esfHouse.getEsfHouseAmt(), "元")) {
				cell.setCellValue(esfHouse.getEsfHouseAmt().substring(0, esfHouse.getEsfHouseAmt().indexOf("元")));
			} else {
				cell.setCellValue(esfHouse.getEsfHouseAmt());
			}
			cell.setCellStyle(style_text);
			// 使用RegionUtil类为合并后的单元格添加边框
			setRegionBorder(cra, sheet);
		} else {
			cell = row.createCell(index_no);
			if (StringUtils.contains(esfHouse.getEsfHouseAmt(), "元")) {
				cell.setCellValue(esfHouse.getEsfHouseAmt().substring(0, esfHouse.getEsfHouseAmt().indexOf("元")));
			} else {
				cell.setCellValue(esfHouse.getEsfHouseAmt());
			}
			cell.setCellStyle(style_text);
		}
		index_no++;

		// 二手房 — 市场价格权重
		if (esfCurrIndex > 0 && StringUtils.isNotBlank(id)) {
			firstRow = rowsNum;
			lastRow = rowsNum + esfCurrIndex;
			firstCol = index_no;
			lastCol = index_no;
			cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
			sheet.addMergedRegion(cra);
			cell = row.createCell(index_no);
			cell.setCellValue(esfHouse.getEsfHouseRatio() + "%");
			cell.setCellStyle(style_text);
			// 使用RegionUtil类为合并后的单元格添加边框
			setRegionBorder(cra, sheet);
		} else {
			cell = row.createCell(index_no);
			cell.setCellValue(esfHouse.getEsfHouseRatio() + "%");
			cell.setCellStyle(style_text);
		}
		index_no++;

		if (esfCurrIndex < 1) {
			return rowsNum;
		} else {
			if (StringUtils.isBlank(id)) {
				return rowsNum;
			}
		}

		// 遍历二手竞品详细信息
		for (ReptileTemplateInfoView view : esfHouse.getEsfViews()) {
			rowsNum++;

			index_no = 0;
			row = sheet.createRow(rowsNum);
			cell = row.createCell(index_no);
			cell.setCellValue(view.getHouseName());
			cell.setCellStyle(style);
			index_no++;
			// 查询详细的修正项值
			ReptileCorrectionSetSearch search = new ReptileCorrectionSetSearch();
			search.setEqualHouseId(view.getHouseId());
			search.setEqualUserId(view.getUserId());
			List<ReptileCorrectionSetModel> model_set = reptileServiceManage.reptileCorrectionSetService
					.findListBySearch(search);
			for (ReptileCorrectionFactorModel parent : map_corr.keySet()) {
				int length = map_corr.get(parent).size();
				for (int i = 0; i < length; i++) {
					for (ReptileCorrectionSetModel model : model_set) {
						// 如果修正项的id和model的correctionId一致 则输出
						if (StringUtils.equals(map_corr.get(parent).get(i).getId(), model.getCorrectionId())) {
							cell = row.createCell(index_no);
							cell.setCellValue(model.getCorrectionValue());
							cell.setCellStyle(style_text);
							index_no++;
						}
					}
				}
			}
			// 综合系数
			cell = row.createCell(index_no);
			cell.setCellValue(view.getTotalCorrectionValue());
			cell.setCellStyle(style_text);
			index_no++;

			// 市场价格
			cell = row.createCell(index_no);
			cell.setCellValue(view.getHousePrice());
			cell.setCellStyle(style_text);
			index_no++;

			// 二手交易税费系数
			cell = row.createCell(index_no);
			cell.setCellValue(view.getTradeTax());
			cell.setCellStyle(style_text);
			index_no++;

			// 实际成交价
			cell = row.createCell(index_no);
			cell.setCellValue(view.getTransactionPrice());
			cell.setCellStyle(style_text);
			index_no++;

			// 本项目对比定价
			cell = row.createCell(index_no);
			cell.setCellValue(view.getComparePrice());
			cell.setCellStyle(style_text);
			index_no++;

			// 竞品权重
			cell = row.createCell(index_no);
			cell.setCellValue(view.getHouseWeight() + "%");
			cell.setCellStyle(style_text);
			index_no++;

			// 市场加权价
			cell = row.createCell(index_no);
			cell.setCellValue(view.getSubPrice());
			cell.setCellStyle(style_text);
			index_no++;
		}
		return rowsNum;
	}

	// 土地excel表格
	private void tudi_excel(int firstRow, int lastRow, int firstCol, int lastCol, int index_no, int rowsNum,
			HSSFRow row, HSSFCell cell, CellRangeAddress cra, HSSFSheet sheet, HSSFWorkbook wb,
			HSSFCellStyle style_text, HSSFCellStyle style, int tudiCurrIndex, JpTudiDataBean tudi)
			throws ProjectException {
		index_no = 0;
		row = sheet.createRow(rowsNum);
		cell = row.createCell(index_no);
		cell.setCellValue("土地对标价");
		cell.setCellStyle(style);

		// 换行
		rowsNum++;

		// 地块名称
		index_no = 0;
		row = sheet.createRow(rowsNum);
		cell = row.createCell(index_no);
		cell.setCellValue("地块名称");
		cell.setCellStyle(style);
		index_no++;

		// 土地用途
		cell = row.createCell(index_no);
		cell.setCellValue("土地用途");
		cell.setCellStyle(style);
		index_no++;

		// 宗地面积
		cell = row.createCell(index_no);
		cell.setCellValue("宗地面积  /㎡");
		cell.setCellStyle(style);
		index_no++;

		// 计容建面
		/*
		 * cell = row.createCell(index_no); cell.setCellValue("计容建面  /㎡");
		 * cell.setCellStyle(style); index_no++;
		 */

		// 成交时间
		cell = row.createCell(index_no);
		cell.setCellValue("成交时间");
		cell.setCellStyle(style);
		index_no++;

		// 楼面地价
		cell = row.createCell(index_no);
		cell.setCellValue("楼面地价 （元/㎡）");
		cell.setCellStyle(style);
		index_no++;

		// 楼面地价在房价中占比
		cell = row.createCell(index_no);
		cell.setCellValue("楼面地价在房价中占比");
		cell.setCellStyle(style);
		index_no++;

		// 预估售价
		cell = row.createCell(index_no);
		cell.setCellValue("预估售价 （元/㎡）");
		cell.setCellStyle(style);
		index_no++;

		// 竞品土地权重
		cell = row.createCell(index_no);
		cell.setCellValue("权重占比");
		cell.setCellStyle(style);
		index_no++;

		// 市场加权价
		cell = row.createCell(index_no);
		cell.setCellValue("市场加权价 （元/㎡）");
		cell.setCellStyle(style);
		index_no++;

		// 综合价格
		cell = row.createCell(index_no);
		cell.setCellValue("综合价格 （元/㎡）");
		cell.setCellStyle(style);
		index_no++;

		// 土地价格权重
		cell = row.createCell(index_no);
		cell.setCellValue("土地价格权重");
		cell.setCellStyle(style);
		index_no++;

		// 标记土地详细数据起始行行号
		int row_index = rowsNum + 1;

		// 如果没有任何楼盘信息 就不进行后续遍历了
		if (tudiCurrIndex < 1) {
			return;
		} else {
			String id;
			if (tudi.getTudiViews() != null) {
				id = tudi.getTudiViews().get(0).getHouseId();
			} else {
				id = null;
				tudi.setTudiHouseAmt("0.00元/㎡");
				tudi.setTudiHouseRatio("10");
			}
			if (StringUtils.isBlank(id)) {
				return;
			}
		}

		// 遍历土地详细数据
		for (ReptileTemplateInfoView view : tudi.getTudiViews()) {
			rowsNum++;

			index_no = 0;

			// 土地名称
			row = sheet.createRow(rowsNum);
			cell = row.createCell(index_no);
			cell.setCellValue(view.getHouseName());
			cell.setCellStyle(style);
			index_no++;
			// 土地用途
			cell = row.createCell(index_no);
			cell.setCellValue(view.getTudiUse());
			cell.setCellStyle(style);
			index_no++;
			// 宗地面积
			cell = row.createCell(index_no);
			cell.setCellValue(view.getTudiAcreage());
			cell.setCellStyle(style);
			index_no++;
			// 计容建面
			/*
			 * cell = row.createCell(index_no); cell.setCellValue("计容建面_待补充");
			 * cell.setCellStyle(style); index_no++;
			 */
			// 成交时间
			cell = row.createCell(index_no);
			cell.setCellValue(view.getTudiTime());
			cell.setCellStyle(style);
			index_no++;
			// 楼面价
			cell = row.createCell(index_no);
			cell.setCellValue(view.getHousePrice());
			cell.setCellStyle(style);
			index_no++;
			// 楼面价占比
			cell = row.createCell(index_no);
			cell.setCellValue(view.getFloorPriceProportion() + "%");
			cell.setCellStyle(style);
			index_no++;
			// 预估售价
			cell = row.createCell(index_no);
			cell.setCellValue(view.getEstimatePrice());
			cell.setCellStyle(style);
			index_no++;
			// 竞品权重
			cell = row.createCell(index_no);
			cell.setCellValue(view.getHouseWeight() + "%");
			cell.setCellStyle(style);
			index_no++;
			// 市场加权价
			cell = row.createCell(index_no);
			cell.setCellValue(view.getSubPrice());
			cell.setCellStyle(style);
			index_no++;
			if (row_index == rowsNum) {
				// 综合价格
				if (tudiCurrIndex > 1) {
					firstRow = rowsNum;
					lastRow = rowsNum + tudiCurrIndex - 1;
					firstCol = index_no;
					lastCol = index_no;
					cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
					sheet.addMergedRegion(cra);
					cell = row.createCell(index_no);
					if (StringUtils.contains(tudi.getTudiHouseAmt(), "元")) {
						cell.setCellValue(tudi.getTudiHouseAmt().substring(0, tudi.getTudiHouseAmt().indexOf("元")));
					} else {
						cell.setCellValue(tudi.getTudiHouseAmt());
					}
					cell.setCellStyle(style);
					// 使用RegionUtil类为合并后的单元格添加边框
					setRegionBorder(cra, sheet);
				} else {
					cell = row.createCell(index_no);
					if (StringUtils.contains(tudi.getTudiHouseAmt(), "元")) {
						cell.setCellValue(tudi.getTudiHouseAmt().substring(0, tudi.getTudiHouseAmt().indexOf("元")));
					} else {
						cell.setCellValue(tudi.getTudiHouseAmt());
					}
					cell.setCellStyle(style);
				}
				index_no++;

				// 土地价格权重
				if (tudiCurrIndex > 1) {
					firstRow = rowsNum;
					lastRow = rowsNum + tudiCurrIndex - 1;
					firstCol = index_no;
					lastCol = index_no;
					cra = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
					sheet.addMergedRegion(cra);
					cell = row.createCell(index_no);
					cell.setCellValue(tudi.getTudiHouseRatio() + "%");
					cell.setCellStyle(style);
					// 使用RegionUtil类为合并后的单元格添加边框
					setRegionBorder(cra, sheet);
				} else {
					cell = row.createCell(index_no);
					cell.setCellValue(tudi.getTudiHouseRatio() + "%");
					cell.setCellStyle(style);
				}
				index_no++;
			}
		}
	}

	@Override
	public List<ReptileTemplateModel> selectTemplateByName(String name, String type, String houseType, SessionUser user)
			throws ProjectException {
		String id = user.getId();
		ReptileTemplateSearch search = new ReptileTemplateSearch();
		search.setOrder("desc");
		search.setSort("updateTime");
		if (StringUtils.isNotBlank(name)) {
			search.setLikeName(name);
		}
		if (StringUtils.isNotBlank(houseType)) {
			search.setEqualHouseType(houseType);
		}
		search.setEqualUserId(id);
		if (StringUtils.isNotBlank(type)) {
			search.setEqualType(type);
		}
		List<ReptileTemplateModel> modelList = reptileServiceManage.reptileTemplateService.findListBySearch(search);
		return modelList;
	}
}
