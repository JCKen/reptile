package com.jrzh.mvc.service.reptile;

import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.mvc.model.reptile.ReptileCorrectionFactorModel;
import com.jrzh.mvc.search.reptile.ReptileCorrectionFactorSearch;
import com.jrzh.mvc.view.reptile.ReptileCorrectionFactorView;

public interface ReptileCorrectionFactorServiceI  extends BaseServiceI<ReptileCorrectionFactorModel, ReptileCorrectionFactorSearch, ReptileCorrectionFactorView>{

}