package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileHouseProjectNameConvert;
import com.jrzh.mvc.model.reptile.ReptileHouseProjectNameModel;
import com.jrzh.mvc.repository.reptile.ReptileHouseProjectNameRepository;
import com.jrzh.mvc.search.reptile.ReptileHouseProjectNameSearch;
import com.jrzh.mvc.service.reptile.ReptileHouseProjectNameServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileHouseProjectNameView;
import com.jrzh.thread.sh.SHFangGuanJuHouseInfoThread;

@Service("reptileHouseProjectNameService")
public class ReptileHouseProjectNameServiceImpl extends
		BaseServiceImpl<ReptileHouseProjectNameModel, ReptileHouseProjectNameSearch, ReptileHouseProjectNameView> implements
		ReptileHouseProjectNameServiceI {

	@Autowired
	private ReptileHouseProjectNameRepository reptileHouseProjectNameRepository;
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;

	@Override
	public BaseRepository<ReptileHouseProjectNameModel,Serializable> getDao() {
		return reptileHouseProjectNameRepository;
	}

	@Override
	public BaseConvertI<ReptileHouseProjectNameModel, ReptileHouseProjectNameView> getConvert() {
		return new ReptileHouseProjectNameConvert();
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException{
		ResultBean result = new ResultBean();

		ReptileHouseProjectNameModel reptileHouseProjectNameModel = this.findById(id);
		reptileHouseProjectNameModel.setIsDisable(!reptileHouseProjectNameModel.getIsDisable());
		edit(reptileHouseProjectNameModel, sessionUser);

		result.setStatus(ResultBean.SUCCESS);
		result.setMsg(I18nHelper.getI18nByKey("message.adjustment_success", sessionUser));
		return result;
	}

	@Override
	public void addSHFangGuanJuHouseData(SessionUser user) throws ProjectException {
		new SHFangGuanJuHouseInfoThread("上海", reptileServiceManage).start();
	}
}
