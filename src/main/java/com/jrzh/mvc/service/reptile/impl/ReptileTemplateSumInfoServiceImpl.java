package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.mvc.convert.reptile.ReptileTemplateSumInfoConvert;
import com.jrzh.mvc.repository.reptile.ReptileTemplateSumInfoRepository;
import com.jrzh.mvc.model.reptile.ReptileTemplateSumInfoModel;
import com.jrzh.mvc.search.reptile.ReptileTemplateSumInfoSearch;
import com.jrzh.mvc.service.reptile.ReptileTemplateSumInfoServiceI;
import com.jrzh.mvc.view.reptile.ReptileTemplateSumInfoView;

@Service("reptileTemplateSumInfoService")
public class ReptileTemplateSumInfoServiceImpl extends
		BaseServiceImpl<ReptileTemplateSumInfoModel, ReptileTemplateSumInfoSearch, ReptileTemplateSumInfoView> implements
		ReptileTemplateSumInfoServiceI {

	@Autowired
	private ReptileTemplateSumInfoRepository reptileTemplateSumInfoRepository;

	@Override
	public BaseRepository<ReptileTemplateSumInfoModel,Serializable> getDao() {
		return reptileTemplateSumInfoRepository;
	}

	@Override
	public BaseConvertI<ReptileTemplateSumInfoModel, ReptileTemplateSumInfoView> getConvert() {
		return new ReptileTemplateSumInfoConvert();
	}

}
