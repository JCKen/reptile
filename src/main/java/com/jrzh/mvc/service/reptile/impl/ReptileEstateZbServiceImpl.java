package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.mvc.convert.reptile.ReptileEstateZbConvert;
import com.jrzh.mvc.repository.reptile.ReptileEstateZbRepository;
import com.jrzh.mvc.model.reptile.ReptileEstateZbModel;
import com.jrzh.mvc.search.reptile.ReptileEstateZbSearch;
import com.jrzh.mvc.service.reptile.ReptileEstateZbServiceI;
import com.jrzh.mvc.view.reptile.ReptileEstateZbView;

@Service("reptileEstateZbService")
public class ReptileEstateZbServiceImpl extends
		BaseServiceImpl<ReptileEstateZbModel, ReptileEstateZbSearch, ReptileEstateZbView> implements
		ReptileEstateZbServiceI {

	@Autowired
	private ReptileEstateZbRepository reptileEstateZbRepository;

	@Override
	public BaseRepository<ReptileEstateZbModel,Serializable> getDao() {
		return reptileEstateZbRepository;
	}

	@Override
	public BaseConvertI<ReptileEstateZbModel, ReptileEstateZbView> getConvert() {
		return new ReptileEstateZbConvert();
	}

}
