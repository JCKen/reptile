package com.jrzh.mvc.service.reptile;

import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.mvc.model.reptile.ReptileTemplateSumInfoModel;
import com.jrzh.mvc.search.reptile.ReptileTemplateSumInfoSearch;
import com.jrzh.mvc.view.reptile.ReptileTemplateSumInfoView;

public interface ReptileTemplateSumInfoServiceI  extends BaseServiceI<ReptileTemplateSumInfoModel, ReptileTemplateSumInfoSearch, ReptileTemplateSumInfoView>{

}