package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileYgjyHousePreSaleCertificateConvert;
import com.jrzh.mvc.model.reptile.ReptileYgjyHousePreSaleCertificateModel;
import com.jrzh.mvc.repository.reptile.ReptileYgjyHousePreSaleCertificateRepository;
import com.jrzh.mvc.search.reptile.ReptileYgjyHousePreSaleCertificateSearch;
import com.jrzh.mvc.service.reptile.ReptileYgjyHousePreSaleCertificateServiceI;
import com.jrzh.mvc.view.reptile.ReptileYgjyHousePreSaleCertificateView;

@Service("reptileYgjyHousePreSaleCertificateService")
public class ReptileYgjyHousePreSaleCertificateServiceImpl extends
		BaseServiceImpl<ReptileYgjyHousePreSaleCertificateModel, ReptileYgjyHousePreSaleCertificateSearch, ReptileYgjyHousePreSaleCertificateView> implements
		ReptileYgjyHousePreSaleCertificateServiceI {

	@Autowired
	private ReptileYgjyHousePreSaleCertificateRepository reptileYgjyHousePreSaleCertificateRepository;

	@Override
	public BaseRepository<ReptileYgjyHousePreSaleCertificateModel,Serializable> getDao() {
		return reptileYgjyHousePreSaleCertificateRepository;
	}

	@Override
	public BaseConvertI<ReptileYgjyHousePreSaleCertificateModel, ReptileYgjyHousePreSaleCertificateView> getConvert() {
		return new ReptileYgjyHousePreSaleCertificateConvert();
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException{
		ResultBean result = new ResultBean();

		ReptileYgjyHousePreSaleCertificateModel reptileHouseDistributionModel = this.findById(id);
		reptileHouseDistributionModel.setIsDisable(!reptileHouseDistributionModel.getIsDisable());
		edit(reptileHouseDistributionModel, sessionUser);

		result.setStatus(ResultBean.SUCCESS);
		result.setMsg(I18nHelper.getI18nByKey("message.adjustment_success", sessionUser));
		return result;
	}

}
