package com.jrzh.mvc.service.reptile;

import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.mvc.model.reptile.ReptileCorrectionSetModel;
import com.jrzh.mvc.search.reptile.ReptileCorrectionSetSearch;
import com.jrzh.mvc.view.reptile.ReptileCorrectionSetView;

public interface ReptileCorrectionSetServiceI  extends BaseServiceI<ReptileCorrectionSetModel, ReptileCorrectionSetSearch, ReptileCorrectionSetView>{

}