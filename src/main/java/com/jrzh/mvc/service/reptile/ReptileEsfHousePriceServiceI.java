package com.jrzh.mvc.service.reptile;

import java.util.List;

import com.jrzh.bean.ReptileSearchPriceResultBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileEsfHousePriceModel;
import com.jrzh.mvc.search.reptile.ReptileEsfHousePriceSearch;
import com.jrzh.mvc.view.reptile.ReptileEsfHousePriceView;
import com.jrzh.mvc.view.reptile.ReptileEsfHouseView;

public interface ReptileEsfHousePriceServiceI  extends BaseServiceI<ReptileEsfHousePriceModel, ReptileEsfHousePriceSearch, ReptileEsfHousePriceView>{
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException;
	
	public List<ReptileSearchPriceResultBean> findHouseAVG(ReptileEsfHouseView view ) throws ProjectException;
}