package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileTemplateInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileTemplateInfoModel;
import com.jrzh.mvc.repository.reptile.ReptileTemplateInfoRepository;
import com.jrzh.mvc.search.reptile.ReptileTemplateInfoSearch;
import com.jrzh.mvc.service.reptile.ReptileTemplateInfoServiceI;
import com.jrzh.mvc.view.reptile.ReptileTemplateInfoView;

@Service("reptileTemplateInfoService")
@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
public class ReptileTemplateInfoServiceImpl extends
		BaseServiceImpl<ReptileTemplateInfoModel, ReptileTemplateInfoSearch, ReptileTemplateInfoView> implements
		ReptileTemplateInfoServiceI {

	@Autowired
	private ReptileTemplateInfoRepository reptileTemplateRepository;

	@Override
	public BaseRepository<ReptileTemplateInfoModel,Serializable> getDao() {
		return reptileTemplateRepository;
	}

	@Override
	public BaseConvertI<ReptileTemplateInfoModel, ReptileTemplateInfoView> getConvert() {
		return new ReptileTemplateInfoConvert();
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException{
		ResultBean result = new ResultBean();

		ReptileTemplateInfoModel reptileTemplateModel = this.findById(id);
		reptileTemplateModel.setIsDisable(!reptileTemplateModel.getIsDisable());
		edit(reptileTemplateModel, sessionUser);

		result.setStatus(ResultBean.SUCCESS);
		result.setMsg(I18nHelper.getI18nByKey("message.adjustment_success", sessionUser));
		return result;
	}

	
	
}
