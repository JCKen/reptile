package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jrzh.bean.SHRoomProject.BigBean;
import com.jrzh.bean.SHRoomProject.JsonRoomBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.contants.Contants;
import com.jrzh.contants.Contants.YGJY_GET_DATA_SALE_TYPE;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileYgjyHouseFloorInfoConvert;
import com.jrzh.mvc.convert.reptile.ReptileYgjyHouseRoomInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseFloorInfoModel;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseRoomInfoModel;
import com.jrzh.mvc.repository.reptile.ReptileYgjyHouseFloorInfoRepository;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseFloorInfoSearch;
import com.jrzh.mvc.service.reptile.ReptileYgjyHouseFloorInfoServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseFloorInfoView;
import com.jrzh.tools.HttpUtils;
import com.jrzh.tools.MyPayUtils;

@Service("reptileYgjyHouseFloorInfoService")
public class ReptileYgjyHouseFloorInfoServiceImpl extends
		BaseServiceImpl<ReptileYgjyHouseFloorInfoModel, ReptileYgjyHouseFloorInfoSearch, ReptileYgjyHouseFloorInfoView>
		implements ReptileYgjyHouseFloorInfoServiceI {

	@Autowired
	private ReptileYgjyHouseFloorInfoRepository reptileYgjyHouseFloorInfoRepository;

	@Autowired
	private ReptileServiceManage reptileServiceManage;

	@Override
	public BaseRepository<ReptileYgjyHouseFloorInfoModel, Serializable> getDao() {
		return reptileYgjyHouseFloorInfoRepository;
	}

	@Override
	public BaseConvertI<ReptileYgjyHouseFloorInfoModel, ReptileYgjyHouseFloorInfoView> getConvert() {
		return new ReptileYgjyHouseFloorInfoConvert();
	}

	@Override
	public void checkFloorTask(ReptileYgjyHouseFloorInfoModel floor) throws ProjectException {
		if(StringUtils.isEmpty(floor.getGzBuildingId())) return;
		YGJY_GET_DATA_SALE_TYPE.paraMap.put("buildingID", floor.getGzBuildingId());
		Date infoUpdateTime = new Date();
		try {
			String res = HttpUtils.post("http://www.gzcc.gov.cn/data/laho/sellFormpic.aspx", YGJY_GET_DATA_SALE_TYPE.paraMap, null);
			Document doc = Jsoup.parse(res);
			Element tables= doc.selectFirst(".mt10");
			if(null == tables) return;
			Elements chilrenTables = tables.getElementsByAttributeValue("width","10%");
			if(null == chilrenTables) return;
			Elements chilrenTables2 = tables.getElementsByAttributeValue("width","885");
			if(null == chilrenTables2) return;
			for(int i = 0; i<chilrenTables.size(); i++) {
				if(i+1 > chilrenTables2.size()) continue;
				String roomStatus = null;
				String roomNumber = null;
				String louCeng = chilrenTables.get(i).text();
				Element tr = chilrenTables2.get(i).selectFirst("tr");
				if(null == tr) continue;
				Elements aEle = tr.select("a");
				if(null != aEle) {
					for(Element a : aEle) {
						ReptileYgjyHouseRoomInfoModel roomModel = new ReptileYgjyHouseRoomInfoModel();
						String info = a.attr("title");
						roomNumber = StringUtils.split(a.text()," ")[0];
						String[] str = StringUtils.split(info, "：");
						String roomType = StringUtils.replace(str[1],"户型" , "");//房型
						String roomApartment = StringUtils.replace(str[2],"套内面积" , "");//户型
						String roomRealArea = StringUtils.replace(str[3],"总面积" , "");//套内面积
						String roomTotalArea = StringUtils.replace(str[4],"当前状态" , "");//总面积
						roomStatus = StringUtils.replace(str[5],"查封" , "");//当前状态
						if(StringUtils.equals(roomStatus, "不可销售")) {
							roomStatus = "不可售";
						}
						if(StringUtils.equals(roomStatus, "预售可售")) {
							roomStatus = "可售";					
						}
						if(StringUtils.equals(roomStatus, "确权不可售")) {
							roomStatus = "不可售";
						}
						if(StringUtils.equals(roomStatus, "确权可售")) {
							roomStatus = "可售";
						}
						if(StringUtils.equals(roomStatus, "已过户")) {
							roomStatus = "已售";
						}
						if(StringUtils.equals(roomStatus, "强制冻结")) {
							roomStatus = "不可售";
						}
						String roomSealingState = StringUtils.replace(str[6],"抵押" , "");//查封状态
						String roomMortgage = StringUtils.replace(str[7],"详细信息请点击房号显示" , "");//抵押状态
						roomModel.setFloorId(floor.getId());
						roomModel.setHouseId(floor.getHouseId());
						louCeng = StringUtils.replace(louCeng, "地下", "-");
						roomModel.setLouCeng(louCeng);
						roomModel.setRoomApartment(roomApartment);
						roomModel.setRoomMortgage(roomMortgage);
						roomModel.setRoomNumber(roomNumber);
						roomModel.setRoomRealArea(roomRealArea);
						roomModel.setRoomTotalArea(roomTotalArea);
						roomModel.setRoomSealingState(roomSealingState);
						roomModel.setRoomType(roomType);
						Elements type = a.select("font");
						if(null != type) {
							for(Element t : type) {
								if(StringUtils.equals(t.text(), "★")) {
									roomStatus += ",未纳入预售";
								}
								if(StringUtils.equals(t.text(), "△")) {
									roomStatus += ",回迁房";
								}
								if(StringUtils.equals(t.text(), "□")) {
									roomStatus += ",自用房";
								}
								if(StringUtils.equals(t.text(), "☆")) {
									roomStatus += ",公建配套";
								}
								if(StringUtils.equals(t.text(), "▲")) {
									roomStatus += ",直管房";
								}
								if(StringUtils.equals(t.text(), "〓")) {
									roomStatus += ",分成";
								}
								if(StringUtils.equals(t.text(), "◆")) {
									roomStatus += ",抵押";
								}
								if(StringUtils.equals(t.text(), "●")) {
									roomStatus += ",查封";
								}
								if(StringUtils.equals(t.text(), "◎")) {
									roomStatus += ",已售";
								}
								if(StringUtils.equals(t.text(), "■")) {
									roomStatus += ",已备案";
								}
							}
						}
						roomModel.setInfoUpdateTime(DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd")+" 00:00:00"));
						roomModel.setRoomStatus(roomStatus);
						reptileServiceManage.reptileYgjyHouseRoomInfoService.add(roomModel, SessionUser.getSystemUser());
					}
				}
			}
			Thread.sleep(1000);
			floor.setStatus(Contants.SUCCESS);
			reptileServiceManage.reptileYgjyHouseFloorInfoService.edit(floor, SessionUser.getSystemUser());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void SHCheckFloorTask(ReptileYgjyHouseFloorInfoModel floor) throws ProjectException {
		try {
			String startId = floor.getShStartId();
			String buildingId= floor.getBuildingId();
			String roomRequestContent = MyPayUtils.sendHttp(null,buildingId,startId, "http://www.fangdi.com.cn/service/freshHouse/getMoreInfo.action", 20000, 50000);
			Object status = JSONObject.parseObject(roomRequestContent).get("result");
			if(null != status) return;
			BigBean bigBean = JSON.parseObject(roomRequestContent,BigBean.class);
			LinkedHashMap<String,List<JsonRoomBean>> link = bigBean.getMoreInfoList();
			if(null == link || link.size() <= 0) return;
			for(Integer j = 0; j < link.size();j++) {
				List<JsonRoomBean> jsonRoomBeanList = link.get(String.valueOf(j+1));
				if(CollectionUtils.isEmpty(jsonRoomBeanList)) continue;
				for(JsonRoomBean jr : jsonRoomBeanList) {
					ReptileYgjyHouseRoomInfoModel newReptileYgjyHouseRoomInfosModel = new ReptileYgjyHouseRoomInfoModel();
					newReptileYgjyHouseRoomInfosModel.setFloorId(floor.getId());
					newReptileYgjyHouseRoomInfosModel.setHouseId(floor.getHouseId());
					newReptileYgjyHouseRoomInfosModel.setRoomNumber(jr.getRoom_number());
					newReptileYgjyHouseRoomInfosModel.setRoomTotalArea(jr.getFlarea());
					reptileServiceManage.reptileYgjyHouseRoomInfoService.add(newReptileYgjyHouseRoomInfosModel, SessionUser.getSystemUser());
				}
			}
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void SZCheckFloorTask(ReptileYgjyHouseFloorInfoModel floor) throws ProjectException {
		Date infoUpdateTime = new Date();
		try {
			String id = floor.getBuildingId();
			String presellid = floor.getSzStartId();
			if(StringUtils.isEmpty(presellid)) return;
			String url = "http://ris.szpl.gov.cn/bol/building.aspx?id=" + id + "&presellid=" + presellid;
			Document preSaleDoc;
			try {
				preSaleDoc = Jsoup.connect(url).timeout(50000).get();
			} catch (Exception e2) {
				e2.printStackTrace();
				return;
			}
			if(null == preSaleDoc) {
				return;
			}
			Element updatepanel = preSaleDoc.selectFirst("#updatepanel1");
			if(null == updatepanel) {
				return ;
			}
			Element divShowBranch = updatepanel.selectFirst("#divShowBranch");
			if(null == divShowBranch) {
				return;
			}
			Elements tableEle = updatepanel.select("table");
			if(null == tableEle || tableEle.size() < 3) {
				return ;
			}
			Elements selectA = divShowBranch.select("a");
			//查询楼盘的座号的url
			if(null != selectA) {
				for(Element e : selectA) {
					String houseUrl = e.attr("href");
					String goHouseUrl = "http://ris.szpl.gov.cn/bol/" + houseUrl;
					if(StringUtils.isEmpty(houseUrl)) continue;
					Document preSaleDocOther = Jsoup.connect(goHouseUrl).timeout(50000).get();
					if(null == preSaleDocOther) {
						continue ;
					}
					Element updatepanelOther = preSaleDocOther.selectFirst("#updatepanel1");
					if(null == updatepanelOther) {
						continue ;
					}
					Element divShowBranchOther = updatepanelOther.selectFirst("#divShowBranch");
					if(null == divShowBranchOther) {
						continue;
					}
					Elements tableEleOther = updatepanelOther.select("table");
					if(null == tableEleOther || tableEleOther.size() < 3) {
						continue ;
					}
					Element houseInfoUrlEleOther = tableEleOther.last();
					if(null == houseInfoUrlEleOther) continue;
					Elements houseInfoUrls = houseInfoUrlEleOther.select(".a1");
					if(null == houseInfoUrls) continue;
					for(Element houseInfoUrl : houseInfoUrls) {
						Elements louCengEle = houseInfoUrl.select("td");
						if(null == louCengEle) continue;
						for(Element lc : louCengEle) {
							Element houseRoomInfoUrlEle = lc.selectFirst("a");
							if(null == houseRoomInfoUrlEle) continue;
							String houseType = lc.selectFirst("img").attr("src");
							if(StringUtils.contains(houseType, "b1_2.gif")) {
								houseType = "可售";
							}else if(StringUtils.contains(houseType, "b2.gif")) {
								houseType = "已售";
							}else if(StringUtils.contains(houseType, "b123.gif")) {
								houseType = "不可售";
							}else if(StringUtils.contains(houseType, "b10.gif")) {
								houseType = "已售";
							}else if(StringUtils.contains(houseType, "b3.gif")) {
								houseType = "已售";
							}else {
								houseType = "不可售";
							}
							String houserDetailUrl = "http://ris.szpl.gov.cn/bol/" + houseRoomInfoUrlEle.attr("href");
							Document houseDetailDoc;
							try {
								houseDetailDoc = Jsoup.connect(houserDetailUrl).timeout(50000).get();
							} catch (Exception e1) {
								e1.printStackTrace();
								continue;
							}
							Map<String,String> map = new HashedMap<>();
							Elements selectTr = houseDetailDoc.select(".a1");
							if(null == selectTr) continue;
							for(Element tabEle : selectTr) {
								if(StringUtils.equals(tabEle.text(), "竣工查丈")) break;
								Elements selectTd = tabEle.select("td");
								if(null == selectTd) continue;
								if(selectTd.size() == 4) {
									map.put(selectTd.get(0).text(), selectTd.get(1).text());
									map.put(selectTd.get(2).text(), selectTd.get(3).text());
								}else if(selectTd.size() == 6) {
									map.put(selectTd.get(0).text(), selectTd.get(1).text());
									map.put(selectTd.get(2).text(), selectTd.get(3).text());
									map.put(selectTd.get(4).text(), selectTd.get(5).text());
								}
							}
							ReptileYgjyHouseRoomInfoModel reptileYgjyHouseRoomInfoModel = ReptileYgjyHouseRoomInfoConvert.houseWorldMapToHouseModel(map);
							reptileYgjyHouseRoomInfoModel.setRoomStatus(houseType);
							reptileYgjyHouseRoomInfoModel.setHouseId(floor.getHouseId());
							reptileYgjyHouseRoomInfoModel.setFloorId(floor.getId());
							reptileYgjyHouseRoomInfoModel.setInfoUpdateTime(DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd")+" 00:00:00"));
							reptileServiceManage.reptileYgjyHouseRoomInfoService.add(reptileYgjyHouseRoomInfoModel, SessionUser.getSystemUser());
							Thread.sleep(1000);
						}
					}
					Thread.sleep(500);
				}
			}
			//获取当前页面的房间的url
			Element houseInfoUrlEle = tableEle.last();
			if(null == houseInfoUrlEle) return;
			Elements houseInfoUrls = houseInfoUrlEle.select(".a1");
			if(null == houseInfoUrls) return;
			for(Element houseInfoUrl : houseInfoUrls) {
				Elements louCengEle = houseInfoUrl.select("td");
				if(null == louCengEle) continue;
				for(Element lc : louCengEle) {
					Element houseRoomInfoUrlEle = lc.selectFirst("a");
					if(null == houseRoomInfoUrlEle) continue;
					String houseType = lc.selectFirst("img").attr("src");
					if(StringUtils.contains(houseType, "b1_2.gif")) {
						houseType = "可售";
					}else if(StringUtils.contains(houseType, "b2.gif")) {
						houseType = "已售";
					}else if(StringUtils.contains(houseType, "b123.gif")) {
						houseType = "不可售";
					}else if(StringUtils.contains(houseType, "b10.gif")) {
						houseType = "已售";
					}else if(StringUtils.contains(houseType, "b3.gif")) {
						houseType = "已售";
					}else {
						houseType = "不可售";
					}
					String houserDetailUrl = "http://ris.szpl.gov.cn/bol/" + houseRoomInfoUrlEle.attr("href");
					Document houseDetailDoc;
					try {
						houseDetailDoc = Jsoup.connect(houserDetailUrl).timeout(50000).get();
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
					if(null == houseDetailDoc) continue;
					Elements selectTr = houseDetailDoc.select(".a1");
					if(null == selectTr) continue;
					Map<String,String> map = new HashedMap<>();
					for(Element tabEle : selectTr) {
						if(StringUtils.equals(tabEle.text(), "竣工查丈")) break;
						Elements selectTd = tabEle.select("td");
						if(null == selectTd) continue;
						if(selectTd.size() == 4) {
							map.put(selectTd.get(0).text(), selectTd.get(1).text());
							map.put(selectTd.get(2).text(), selectTd.get(3).text());
						}else if(selectTd.size() == 6) {
							map.put(selectTd.get(0).text(), selectTd.get(1).text());
							map.put(selectTd.get(2).text(), selectTd.get(3).text());
							map.put(selectTd.get(4).text(), selectTd.get(5).text());
						}
					}
					ReptileYgjyHouseRoomInfoModel reptileYgjyHouseRoomInfoModel = ReptileYgjyHouseRoomInfoConvert.houseWorldMapToHouseModel(map);
					reptileYgjyHouseRoomInfoModel.setRoomStatus(houseType);
					reptileYgjyHouseRoomInfoModel.setHouseId(floor.getHouseId());
					reptileYgjyHouseRoomInfoModel.setFloorId(floor.getId());
					reptileYgjyHouseRoomInfoModel.setInfoUpdateTime(DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd")+" 00:00:00"));
					reptileServiceManage.reptileYgjyHouseRoomInfoService.add(reptileYgjyHouseRoomInfoModel, SessionUser.getSystemUser());
					Thread.sleep(1000);
				}
			}
			floor.setStatus(Contants.SUCCESS);
			reptileServiceManage.reptileYgjyHouseFloorInfoService.edit(floor, SessionUser.getSystemUser());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void BJCheckFloorTask(ReptileYgjyHouseFloorInfoModel floor) throws ProjectException {
		Date infoUpdateTime = new Date();
		try {
			if(StringUtils.isEmpty(floor.getBjStartId())) {
				return;
			}
			String url = "http://www.bjjs.gov.cn" + floor.getBjStartId();
			Document document = Jsoup.connect(url).timeout(50000).get();
			if(null == document) return;
			Element tableEle = document.selectFirst("#table_Buileing");
			if(null == tableEle) {
				return ;
			}
			String type = null;
			Elements tableTr = tableEle.select("tr");
			if(null == tableTr) {
				return ;
			}
			for(Element e : tableTr) {
				Elements tableTd = e.select("td");
				if(null == tableTd) continue;
				//这里要获取楼层，修改
				String louceng = tableTd.first().text();
				Elements roomUrlEle = tableTd.last().select("div");
				if(null == roomUrlEle) continue;
				for(Element ea : roomUrlEle) {
					String roomUrl = "http://www.bjjs.gov.cn" + ea.select("a").attr("href");
					String backgroundColor = ea.attr("style");
					if(StringUtils.contains(backgroundColor, "33CC00")) {
						type = "可售";
					}else if(StringUtils.contains(backgroundColor, "FFCC99")) {
						type = "已售";
					}else if(StringUtils.contains(backgroundColor, "d2691e")) {
						type = "已售";
					}else if(StringUtils.contains(backgroundColor, "CCCCCC")) {
						type = "不可售";
					}else if(StringUtils.contains(backgroundColor, "ffff00")) {
						type = "已售";
					}else if(StringUtils.contains(backgroundColor, "00FFFF")) {
						type = "已售";
					}else if(StringUtils.contains(backgroundColor, "FF0000")) {
						type = "已售";
					}else {
						type = "未确定状态";
					}
					Document roomDoc = Jsoup.connect(roomUrl).timeout(50000).get();
					Map<String,String> map = new HashMap<>();
					Element roomInfoEle = roomDoc.selectFirst("#showDiv");
					if(null == roomInfoEle) continue;
					Elements roomDetailInfoEle = roomInfoEle.select("tr");
					if(null == roomDetailInfoEle) continue;
					for(Element roomDetailInfo : roomDetailInfoEle) {
						Elements tds = roomDetailInfo.select("td");
						if(tds.size() < 2 ) continue;
						map.put(tds.get(0).text(), tds.get(1).text());
					}
					ReptileYgjyHouseRoomInfoModel roomModel = ReptileYgjyHouseRoomInfoConvert.SZhouseWorldMapToHouseModel(map);
					roomModel.setHouseId(floor.getHouseId());
					roomModel.setFloorId(floor.getId());
					louceng = StringUtils.replace(louceng, "地下", "-");
					roomModel.setLouCeng(louceng);
					roomModel.setRoomStatus(type);
					roomModel.setInfoUpdateTime(DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd")+" 00:00:00"));
					reptileServiceManage.reptileYgjyHouseRoomInfoService.add(roomModel, SessionUser.getSystemUser());
					Thread.sleep(500);
				}
				Thread.sleep(500);
			}
			floor.setStatus(Contants.SUCCESS);
			reptileServiceManage.reptileYgjyHouseFloorInfoService.edit(floor, SessionUser.getSystemUser());
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	
}
