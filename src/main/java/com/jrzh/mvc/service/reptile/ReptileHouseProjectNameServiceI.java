package com.jrzh.mvc.service.reptile;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileHouseProjectNameModel;
import com.jrzh.mvc.search.reptile.ReptileHouseProjectNameSearch;
import com.jrzh.mvc.view.reptile.ReptileHouseProjectNameView;

public interface ReptileHouseProjectNameServiceI  extends BaseServiceI<ReptileHouseProjectNameModel, ReptileHouseProjectNameSearch, ReptileHouseProjectNameView>{
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException;
	
	public void addSHFangGuanJuHouseData(SessionUser user) throws ProjectException;
}