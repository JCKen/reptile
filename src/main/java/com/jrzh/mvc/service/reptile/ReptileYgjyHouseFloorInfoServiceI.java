package com.jrzh.mvc.service.reptile;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseFloorInfoModel;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseFloorInfoSearch;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseFloorInfoView;

public interface ReptileYgjyHouseFloorInfoServiceI extends
		BaseServiceI<ReptileYgjyHouseFloorInfoModel, ReptileYgjyHouseFloorInfoSearch, ReptileYgjyHouseFloorInfoView> {

	void checkFloorTask(ReptileYgjyHouseFloorInfoModel floor) throws ProjectException;
	
	void SHCheckFloorTask(ReptileYgjyHouseFloorInfoModel floor) throws ProjectException;
	
	void SZCheckFloorTask(ReptileYgjyHouseFloorInfoModel floor) throws ProjectException;
	
	void BJCheckFloorTask(ReptileYgjyHouseFloorInfoModel floor) throws ProjectException;
}