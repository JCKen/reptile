package com.jrzh.mvc.service.reptile;

import java.util.List;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileEsfHousesXiaoquModel;
import com.jrzh.mvc.search.reptile.ReptileEsfHousesXiaoquSearch;
import com.jrzh.mvc.view.reptile.ReptileEsfHousesXiaoquView;

public interface ReptileEsfHousesXiaoquServiceI  extends BaseServiceI<ReptileEsfHousesXiaoquModel, ReptileEsfHousesXiaoquSearch, ReptileEsfHousesXiaoquView>{

	List<ReptileEsfHousesXiaoquView> findAllHouseInfo(ReptileEsfHousesXiaoquView view, Integer page, SessionUser sessionUser) throws ProjectException;

	Integer totalCount(ReptileEsfHousesXiaoquView view) throws ProjectException;

	List<Long> selectPart(String houseCity, String housePart) throws ProjectException;

	Integer selectJpTotal(ReptileEsfHousesXiaoquView view, Integer distance)throws ProjectException;

	List<ReptileEsfHousesXiaoquView> selectJp(ReptileEsfHousesXiaoquView view, Integer distance, Integer page)throws ProjectException;
	
	List<ReptileEsfHousesXiaoquView> findByIds(String time ,String ids[]) throws ProjectException;
	
	Integer getVolume(String time,String houseOtherName) throws ProjectException;
}
