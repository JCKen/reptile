package com.jrzh.mvc.service.reptile;

import java.util.List;
import java.util.Map;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileCommercialHouseInfoModel;
import com.jrzh.mvc.search.reptile.ReptileCommercialHouseInfoSearch;
import com.jrzh.mvc.view.reptile.ReptileCommercialHouseInfoView;

public interface ReptileCommercialHouseInfoServiceI extends
		BaseServiceI<ReptileCommercialHouseInfoModel, ReptileCommercialHouseInfoSearch, ReptileCommercialHouseInfoView> {
	public ResultBean changeStatus(String id, SessionUser sessionUser) throws ProjectException;

	public List<ReptileCommercialHouseInfoView> selectGqbInfo(ReptileCommercialHouseInfoView view, SessionUser user)
			throws ProjectException;

	public Map<String, Object> getReportData(String array[], SessionUser sessionUser) throws ProjectException;
}