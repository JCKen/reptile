package com.jrzh.mvc.service.reptile;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.jrzh.bean.ReptileSearchFloorResultBean;
import com.jrzh.bean.ReptileSearchPriceResultBean;
import com.jrzh.bean.ReptileSearchResultBean;
import com.jrzh.bean.priceTrend.PriceBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileNewHouseModel;
import com.jrzh.mvc.search.reptile.ReptileNewHouseSearch;
import com.jrzh.mvc.view.reptile.ReptileNewHouseView;

public interface ReptileNewHouseServiceI extends BaseServiceI<ReptileNewHouseModel, ReptileNewHouseSearch, ReptileNewHouseView> {

	public void findNewHouseFromLianjiaInGZ(SessionUser user) throws ProjectException;
	/**
	 * @param view
	 * @return
	 * @throws ProjectException
	 * 根据特定参数查询出同时间，同城市，同区域的数据，数据包含城市，区域，总建筑面积，总占地面积。
	 */
	public List<ReptileSearchResultBean> findHouseInfo(ReptileNewHouseView view) throws ProjectException;
	
	/**
	 * @param view
	 * @return
	 * @throws ProjectException
	 * 查询当前日期的楼盘数据列表
	 */
	public List<ReptileNewHouseView> findTodayHouseInfo() throws ProjectException;
	
	/**
	 * @param id
	 * @return
	 * @throws ProjectException
	 * 根据id查询具体信息
	 */
	public ReptileSearchFloorResultBean selectById(String id) throws ProjectException;
	
	public void findNewHouseFromHouseWorldInGZ(SessionUser user) throws ProjectException;
	
	/**
	 * @param user
	 * @return 查询所有楼盘信息，楼盘价格，也支持只查改指定城市
	 * @throws ProjectException
	 */
	public List<ReptileNewHouseView> findAllHouseInfo(ReptileNewHouseView view,Integer page,SessionUser user) throws ProjectException;

	public List<ReptileSearchPriceResultBean> findHouseAVG(ReptileNewHouseView view) throws ProjectException ;
	
	/**
	 * @param view
	 * @param user
	 * @return 查询周边信息
	 * @throws ProjectException
	 */
	public List<ReptileNewHouseView> findSurrounding(BigDecimal longitude,BigDecimal latitude,String math) throws ProjectException;
	
	/**
	 * @param view
	 * @param user
	 * @return 根据id数组查询相关信息
	 * @throws ProjectException
	 */
	public List<ReptileNewHouseView> findByIds(String time ,String ids[]) throws ProjectException;
	
	/**
	 * @param view
	 * @return 查询当前条件成立时的总记录数
	 * @throws ProjectException
	 */
	public Integer totalCount(ReptileNewHouseView view) throws ProjectException;
	
	/**
	 * @param view
	 * @return 查询城市各个区域的均价还有楼盘数量
	 * @throws ProjectException
	 */
	public List<Long> selectPart(String houseCity,String housePart) throws ProjectException;
	
	
	/**
	 * @param id
	 * @return 查询单个楼盘的最近价格趋势
	 * @throws ProjectException
	 */
	public PriceBean findNewHouseAllPrice(String id) throws ProjectException;
	
	/**
	 * @return 查询单个楼盘周围指定距离的楼盘
	 */
	public List<ReptileNewHouseView> findByDistance(ReptileNewHouseView view,Integer distance,Integer page )throws ProjectException;

	/**
	 * 查询 指定距离的楼盘数
	 */
	public Integer totalCountByDistance(ReptileNewHouseView view,Integer distance) throws ProjectException;
	
	/**
	 * 
	 * @param type:房屋类型
	 * @return 与该类型相似的集合
	 * @throws ProjectException
	 */
	public Map<Integer, List<String>> getHouseTypes(String[] type) throws ProjectException;
	
}
