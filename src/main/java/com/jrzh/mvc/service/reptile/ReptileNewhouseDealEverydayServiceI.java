package com.jrzh.mvc.service.reptile;

import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.mvc.model.reptile.ReptileNewhouseDealEverydayModel;
import com.jrzh.mvc.search.reptile.ReptileNewhouseDealEverydaySearch;
import com.jrzh.mvc.view.reptile.ReptileNewhouseDealEverydayView;

public interface ReptileNewhouseDealEverydayServiceI  extends BaseServiceI<ReptileNewhouseDealEverydayModel, ReptileNewhouseDealEverydaySearch, ReptileNewhouseDealEverydayView>{

}