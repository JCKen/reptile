package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.mvc.convert.reptile.ReptileReportConfigConvert;
import com.jrzh.mvc.repository.reptile.ReptileReportConfigRepository;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;
import com.jrzh.mvc.model.reptile.ReptileNewhouseYushouzhengModel;
import com.jrzh.mvc.model.reptile.ReptileReportConfigModel;
import com.jrzh.mvc.search.reptile.ReptileReportConfigSearch;
import com.jrzh.mvc.service.reptile.ReptileReportConfigServiceI;
import com.jrzh.mvc.view.reptile.ReptileNewhouseYushouzhengView;
import com.jrzh.mvc.view.reptile.ReptileReportConfigView;

@Service("reptileReportConfigService")
public class ReptileReportConfigServiceImpl extends
		BaseServiceImpl<ReptileReportConfigModel, ReptileReportConfigSearch, ReptileReportConfigView> implements
		ReptileReportConfigServiceI {

	@Autowired
	private ReptileReportConfigRepository reptileReportConfigRepository;

	@Override
	public BaseRepository<ReptileReportConfigModel,Serializable> getDao() {
		return reptileReportConfigRepository;
	}

	@Override
	public BaseConvertI<ReptileReportConfigModel, ReptileReportConfigView> getConvert() {
		return new ReptileReportConfigConvert();
	}


	@Override
	public String getCityGQ_office_dela(String city, String years) {
		return reptileReportConfigRepository.getCityGQ_office_dela(city, years);
	}

	@Override
	public String getCityGQ_house_dela(String city, String years) {
		return reptileReportConfigRepository.getCityGQ_house_dela(city, years);
	}

	@Override
	public List<String> getParts(String city) {
		return reptileReportConfigRepository.getParts(city);
	}

	@Override
	public String getPartGQ_office_dela(String city, String years, String part) {
		return reptileReportConfigRepository.getPartGQ_office_dela(city, years, part);
	}

	@Override
	public String getPartGQ_house_dela(String city, String years, String part) {
		return reptileReportConfigRepository.getPartGQ_house_dela(city, years, part);
	}

	@Override
	public String getShoppingHouse_all_dela(String city, String years) {
		return reptileReportConfigRepository.getShoppingHouse_all_dela(city, years);
	}

	@Override
	public List<ReptileGtjAreaInfoModel> public_auction_tudi(String city, String time) {
		return reptileReportConfigRepository.public_auction_tudi(city, time);
	}

	@Override
	public List<String> getGJTIds(String city, String time) {
		return reptileReportConfigRepository.getGJTIds(city, time);
	}

	@Override
	public List<String> getHouseIds(String id) {
		return reptileReportConfigRepository.getHouseIds(id);
	}

}
