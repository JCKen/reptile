package com.jrzh.mvc.service.reptile;

import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.mvc.model.reptile.ReptileNewhouseYushouzhengModel;
import com.jrzh.mvc.search.reptile.ReptileNewhouseYushouzhengSearch;
import com.jrzh.mvc.view.reptile.ReptileNewhouseYushouzhengView;

public interface ReptileNewhouseYushouzhengServiceI  extends BaseServiceI<ReptileNewhouseYushouzhengModel, ReptileNewhouseYushouzhengSearch, ReptileNewhouseYushouzhengView>{

}