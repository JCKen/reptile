package com.jrzh.mvc.service.reptile;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileYgjyHousePreSaleCertificateModel;
import com.jrzh.mvc.search.reptile.ReptileYgjyHousePreSaleCertificateSearch;
import com.jrzh.mvc.view.reptile.ReptileYgjyHousePreSaleCertificateView;

public interface ReptileYgjyHousePreSaleCertificateServiceI  extends BaseServiceI<ReptileYgjyHousePreSaleCertificateModel, ReptileYgjyHousePreSaleCertificateSearch, ReptileYgjyHousePreSaleCertificateView>{
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException;
}