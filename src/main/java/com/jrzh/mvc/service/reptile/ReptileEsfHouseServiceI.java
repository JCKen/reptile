package com.jrzh.mvc.service.reptile;

import java.math.BigDecimal;
import java.util.List;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileEsfHouseModel;
import com.jrzh.mvc.search.reptile.ReptileEsfHouseSearch;
import com.jrzh.mvc.view.reptile.ReptileEsfHouseView;

public interface ReptileEsfHouseServiceI  extends BaseServiceI<ReptileEsfHouseModel, ReptileEsfHouseSearch, ReptileEsfHouseView>{
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException;
	
	public void findEsfHouseFromLianjiaInGZ(SessionUser user) throws ProjectException;
	
	public void findEsfHouseFromHouseWorldInGZ(SessionUser user) throws ProjectException; 
	
	public List<ReptileEsfHouseView> findAllHouseInfo(ReptileEsfHouseView view,Integer page,SessionUser user) throws ProjectException ;

	public List<ReptileEsfHouseView> findSurrounding(BigDecimal longitude,BigDecimal latitude,String math) throws ProjectException;
	/**
	 * @param view
	 * @param user
	 * @return 根据id数组查询相关信息
	 * @throws ProjectException
	 */
	public List<ReptileEsfHouseView> findByIds(String time ,String ids[]) throws ProjectException;
	
	public Integer getVolume(String time,String houseOtherName) throws ProjectException;
	
	public Integer totalCount(ReptileEsfHouseView view ) throws ProjectException;
	
	public List<Long> selectPart(String houseCity, String housePart) throws ProjectException ;
	
	/**
	 * 查询二手房 指定距离的竞品数
	 * @param view
	 * @param distance(距离)
	 * @return
	 */
	public Integer selectJpTotal(ReptileEsfHouseView view, Integer distance) throws ProjectException ;
	
	/**
	 * 查询二手房 指定距离的竞品
	 * @param view
	 * @param page 
	 * @param distance(距离)
	 * @param page(当前页)
	 * @return
	 */
	public List<ReptileEsfHouseView> selectJp(ReptileEsfHouseView view, Integer distance, Integer page)throws ProjectException ;
	
}