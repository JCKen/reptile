package com.jrzh.mvc.service.reptile;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileUserModel;
import com.jrzh.mvc.search.reptile.ReptileUserSearch;
import com.jrzh.mvc.view.reptile.ReptileUserView;

public interface ReptileUserServiceI  extends BaseServiceI<ReptileUserModel, ReptileUserSearch, ReptileUserView>{

	public void saveUserInfo(SessionUser sessionUser) throws ProjectException;
	
	/**
	 * 用户登录
	 * */
	public ReptileUserView saveSessionUser(ReptileUserView view) throws ProjectException;

	public ResultBean changeStatus(String id, SessionUser sessionUser)throws ProjectException;
}