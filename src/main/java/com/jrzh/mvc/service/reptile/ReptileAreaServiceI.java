package com.jrzh.mvc.service.reptile;

import java.io.InputStream;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.mvc.model.reptile.ReptileAreaModel;
import com.jrzh.mvc.search.reptile.ReptileAreaSearch;
import com.jrzh.mvc.view.reptile.ReptileAreaView;

public interface ReptileAreaServiceI  extends BaseServiceI<ReptileAreaModel, ReptileAreaSearch, ReptileAreaView>{

	public void saveInit(String project, InputStream in) throws ProjectException;
}