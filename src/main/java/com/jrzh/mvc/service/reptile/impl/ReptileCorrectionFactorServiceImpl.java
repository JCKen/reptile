package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.mvc.convert.reptile.ReptileCorrectionFactorConvert;
import com.jrzh.mvc.repository.reptile.ReptileCorrectionFactorRepository;
import com.jrzh.mvc.model.reptile.ReptileCorrectionFactorModel;
import com.jrzh.mvc.search.reptile.ReptileCorrectionFactorSearch;
import com.jrzh.mvc.service.reptile.ReptileCorrectionFactorServiceI;
import com.jrzh.mvc.view.reptile.ReptileCorrectionFactorView;

@Service("reptileCorrectionFactorService")
public class ReptileCorrectionFactorServiceImpl extends
		BaseServiceImpl<ReptileCorrectionFactorModel, ReptileCorrectionFactorSearch, ReptileCorrectionFactorView> implements
		ReptileCorrectionFactorServiceI {

	@Autowired
	private ReptileCorrectionFactorRepository reptileCorrectionFactorRepository;

	@Override
	public BaseRepository<ReptileCorrectionFactorModel,Serializable> getDao() {
		return reptileCorrectionFactorRepository;
	}

	@Override
	public BaseConvertI<ReptileCorrectionFactorModel, ReptileCorrectionFactorView> getConvert() {
		return new ReptileCorrectionFactorConvert();
	}

}
