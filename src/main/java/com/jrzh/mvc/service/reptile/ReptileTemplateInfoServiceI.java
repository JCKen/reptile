package com.jrzh.mvc.service.reptile;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileTemplateInfoModel;
import com.jrzh.mvc.search.reptile.ReptileTemplateInfoSearch;
import com.jrzh.mvc.view.reptile.ReptileTemplateInfoView;

public interface ReptileTemplateInfoServiceI  extends BaseServiceI<ReptileTemplateInfoModel, ReptileTemplateInfoSearch, ReptileTemplateInfoView>{
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException;
}