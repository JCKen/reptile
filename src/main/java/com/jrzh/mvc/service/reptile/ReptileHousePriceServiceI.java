package com.jrzh.mvc.service.reptile;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileHousePriceModel;
import com.jrzh.mvc.search.reptile.ReptileHousePriceSearch;
import com.jrzh.mvc.view.reptile.ReptileHousePriceView;

public interface ReptileHousePriceServiceI  extends BaseServiceI<ReptileHousePriceModel, ReptileHousePriceSearch, ReptileHousePriceView>{
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException;
}