package com.jrzh.mvc.service.reptile;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileYgjySaleInfoModel;
import com.jrzh.mvc.search.reptile.ReptileYgjySaleInfoSearch;
import com.jrzh.mvc.view.reptile.ReptileYgjySaleInfoView;

public interface ReptileYgjySaleInfoServiceI  extends BaseServiceI<ReptileYgjySaleInfoModel, ReptileYgjySaleInfoSearch, ReptileYgjySaleInfoView>{
	
	void addYgjySaleData(SessionUser user) throws ProjectException;
}