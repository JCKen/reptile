package com.jrzh.mvc.service.reptile;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseRoomInfoModel;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseRoomInfoSearch;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseRoomInfoView;

/**
 * @author xsc
 *获取成交量
 */
public interface ReptileYgjyHouseRoomInfoServiceI  extends BaseServiceI<ReptileYgjyHouseRoomInfoModel, ReptileYgjyHouseRoomInfoSearch, ReptileYgjyHouseRoomInfoView>{
	
	public Integer getVolume(String time,String houseId) throws ProjectException;
	
	public Integer selectMaxLouCeng(String houseId,String floorId) throws ProjectException;
}