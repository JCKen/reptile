package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.mvc.convert.reptile.ReptileNewhouseYushouzhengConvert;
import com.jrzh.mvc.repository.reptile.ReptileNewhouseYushouzhengRepository;
import com.jrzh.mvc.model.reptile.ReptileNewhouseYushouzhengModel;
import com.jrzh.mvc.search.reptile.ReptileNewhouseYushouzhengSearch;
import com.jrzh.mvc.service.reptile.ReptileNewhouseYushouzhengServiceI;
import com.jrzh.mvc.view.reptile.ReptileNewhouseYushouzhengView;

@Service("reptileNewhouseYushouzhengService")
public class ReptileNewhouseYushouzhengServiceImpl extends
		BaseServiceImpl<ReptileNewhouseYushouzhengModel, ReptileNewhouseYushouzhengSearch, ReptileNewhouseYushouzhengView> implements
		ReptileNewhouseYushouzhengServiceI {

	@Autowired
	private ReptileNewhouseYushouzhengRepository reptileNewhouseYushouzhengRepository;

	@Override
	public BaseRepository<ReptileNewhouseYushouzhengModel,Serializable> getDao() {
		return reptileNewhouseYushouzhengRepository;
	}

	@Override
	public BaseConvertI<ReptileNewhouseYushouzhengModel, ReptileNewhouseYushouzhengView> getConvert() {
		return new ReptileNewhouseYushouzhengConvert();
	}

}
