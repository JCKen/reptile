package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jrzh.bean.ReptileSearchPriceResultBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileEsfHousePriceConvert;
import com.jrzh.mvc.model.reptile.ReptileEsfHousePriceModel;
import com.jrzh.mvc.repository.reptile.ReptileEsfHousePriceRepository;
import com.jrzh.mvc.search.reptile.ReptileEsfHousePriceSearch;
import com.jrzh.mvc.service.reptile.ReptileEsfHousePriceServiceI;
import com.jrzh.mvc.view.reptile.ReptileEsfHousePriceView;
import com.jrzh.mvc.view.reptile.ReptileEsfHouseView;
import com.jrzh.util.ObjectConvert;

@Service("reptileEsfHousePriceService")
public class ReptileEsfHousePriceServiceImpl extends
		BaseServiceImpl<ReptileEsfHousePriceModel, ReptileEsfHousePriceSearch, ReptileEsfHousePriceView> implements
		ReptileEsfHousePriceServiceI {

	@Autowired
	private ReptileEsfHousePriceRepository reptileEsfHousePriceRepository;
	
	@PersistenceContext
	EntityManager em ;

	@Override
	public BaseRepository<ReptileEsfHousePriceModel,Serializable> getDao() {
		return reptileEsfHousePriceRepository;
	}

	@Override
	public BaseConvertI<ReptileEsfHousePriceModel, ReptileEsfHousePriceView> getConvert() {
		return new ReptileEsfHousePriceConvert();
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException{
		ResultBean result = new ResultBean();

		ReptileEsfHousePriceModel reptileEsfHousePriceModel = this.findById(id);
		reptileEsfHousePriceModel.setIsDisable(!reptileEsfHousePriceModel.getIsDisable());
		edit(reptileEsfHousePriceModel, sessionUser);

		result.setStatus(ResultBean.SUCCESS);
		result.setMsg(I18nHelper.getI18nByKey("message.adjustment_success", sessionUser));
		return result;
	}

	
	@Override
	public List<ReptileSearchPriceResultBean> findHouseAVG(ReptileEsfHouseView view ) throws ProjectException {
		try {
			StringBuilder whereSql = new StringBuilder("SELECT AVG(price._house_price) AS priceAvg,SUM(price._house_price) AS priceSum,house._house_city AS houseCity,house._house_part AS housePart FROM reptile_esf_house AS house LEFT JOIN reptile_esf_house_price AS price ON house._id = price._house_id WHERE price._house_price_unit = '1' ");
			if(StringUtils.isNotBlank(view.getHouseCity())) {
				whereSql.append(" AND _house_city = :houseCity ");
			}
			whereSql.append(" GROUP BY house._house_city, house._house_part");
			Query query =em.createNativeQuery(whereSql.toString());
			if(StringUtils.isNotBlank(view.getHouseCity())) {
				query.setParameter("houseCity", view.getHouseCity());
			}
			log.info("### ƴװsql ### end");
			List<Object[]> objectList = query.getResultList();
			String[] fields = {"priceAvg","priceSum","houseCity","housePart","houseStatus"};
			List<ReptileSearchPriceResultBean> reptileSearchPriceResultBeanList = new ArrayList<>();
			reptileSearchPriceResultBeanList = ObjectConvert.dataToList(ReptileSearchPriceResultBean.class, objectList, fields);
			return reptileSearchPriceResultBeanList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
