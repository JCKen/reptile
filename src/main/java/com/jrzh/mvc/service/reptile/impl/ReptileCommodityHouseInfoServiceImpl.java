package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileCommodityHouseInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileCommodityHouseInfoModel;
import com.jrzh.mvc.repository.reptile.ReptileCommodityHouseInfoRepository;
import com.jrzh.mvc.search.reptile.ReptileCommodityHouseInfoSearch;
import com.jrzh.mvc.service.reptile.ReptileCommodityHouseInfoServiceI;
import com.jrzh.mvc.view.reptile.ReptileCommodityHouseInfoView;
import com.jrzh.util.ObjectConvert;

//商品住房
@Service("reptileCommodityHouseInfoService")
public class ReptileCommodityHouseInfoServiceImpl extends
		BaseServiceImpl<ReptileCommodityHouseInfoModel, ReptileCommodityHouseInfoSearch, ReptileCommodityHouseInfoView> implements
		ReptileCommodityHouseInfoServiceI {

	@Autowired
	private ReptileCommodityHouseInfoRepository reptileCommodityHouseInfoRepository;
	
	@PersistenceContext
	EntityManager em ;

	@Override
	public BaseRepository<ReptileCommodityHouseInfoModel,Serializable> getDao() {
		return reptileCommodityHouseInfoRepository;
	}

	@Override
	public BaseConvertI<ReptileCommodityHouseInfoModel, ReptileCommodityHouseInfoView> getConvert() {
		return new ReptileCommodityHouseInfoConvert();
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException{
		ResultBean result = new ResultBean();

		ReptileCommodityHouseInfoModel reptileCommodityHouseInfoModel = this.findById(id);
		reptileCommodityHouseInfoModel.setIsDisable(!reptileCommodityHouseInfoModel.getIsDisable());
		edit(reptileCommodityHouseInfoModel, sessionUser);

		result.setStatus(ResultBean.SUCCESS);
		result.setMsg(I18nHelper.getI18nByKey("message.adjustment_success", sessionUser));
		return result;
	}
	
	@Override
	public List<ReptileCommodityHouseInfoView> selectGqbInfo(ReptileCommodityHouseInfoView view, SessionUser user) throws ProjectException {
		
		try {
			//本月信息
			if(StringUtils.isBlank(view.getCity())) {
				throw new Exception("城市不能为空");
			}
			StringBuilder whereSql = new StringBuilder("SELECT _house_type as houseType,_city as city,_part as part,SUM(CAST(_transaction_number AS SIGNED)) as transactionAllNumber,SUM(CAST(_transaction_area AS SIGNED)) as transactionAllArea,SUM(CAST(_transaction_price AS SIGNED)) as transactionAllPrice,SUM(CAST(_saleable_area AS SIGNED)) as saleableAllArea,SUM(CAST(_available_sets AS SIGNED)) as availableAllSets,DATE_FORMAT(_info_updata_time, '%Y%m') AS months FROM reptile_commodity_house_info WHERE 1 = 1 AND ");
			if(StringUtils.isNotBlank(view.getCity())) {
				whereSql.append(" _city = '" + view.getCity() + "' GROUP BY months");
			}
			if(StringUtils.equals(view.getIsCity(), "0")) {
				whereSql.append(" ,_city ");
			}
			if(StringUtils.equals(view.getIsPart(), "0")) {
				whereSql.append(" ,_part ");
			}
			if(StringUtils.equals(view.getIsHouseType(), "0")) {
				whereSql.append(" ,_house_type ");
			}
			log.info("### ƴװsql ### begin");
			Query query =em.createNativeQuery(whereSql.toString());
			log.info("### ƴװsql ### end");
			List<Object[]> objectList = query.getResultList();
			String[] fields = {"houseType","city","part","transactionAllNumber","transactionAllArea","transactionAllPrice","saleableAllArea","availableAllSets","months"};
			List<ReptileCommodityHouseInfoView> reptileCommodityHouseInfoViewList = new ArrayList<>();
			reptileCommodityHouseInfoViewList = ObjectConvert.dataToList(ReptileCommodityHouseInfoView.class, objectList, fields);
			return reptileCommodityHouseInfoViewList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
