package com.jrzh.mvc.service.reptile;

import java.util.List;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;
import com.jrzh.mvc.model.reptile.ReptileReportConfigModel;
import com.jrzh.mvc.search.reptile.ReptileReportConfigSearch;
import com.jrzh.mvc.view.reptile.ReptileReportConfigView;

public interface ReptileReportConfigServiceI
		extends BaseServiceI<ReptileReportConfigModel, ReptileReportConfigSearch, ReptileReportConfigView> {

	// 城市 办公 成交面积
	public String getCityGQ_office_dela(String city, String years);

	// 城市 住宅成交面积
	public String getCityGQ_house_dela(String city, String years);

	// 区域 办公 成交面积
	public String getPartGQ_office_dela(String city, String years,String part);

	// 区域 住宅成交面积
	public String getPartGQ_house_dela(String city, String years,String part);

	// 查询城市区域
	public List<String> getParts(String city);
	
	//商品房供销价情况
	public String getShoppingHouse_all_dela(String city,String years);
	
	//公开招拍挂市场典型地块列表
	public List<ReptileGtjAreaInfoModel> public_auction_tudi(String city,String time) ;
	
	//查询特定条件的国土局数据
	public List<String> getGJTIds(String city,String time);
	
	//根据竞品号查房屋id
	public List<String> getHouseIds(String id);
}
