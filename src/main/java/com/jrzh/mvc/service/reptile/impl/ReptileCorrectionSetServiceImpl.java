package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.mvc.convert.reptile.ReptileCorrectionSetConvert;
import com.jrzh.mvc.repository.reptile.ReptileCorrectionSetRepository;
import com.jrzh.mvc.model.reptile.ReptileCorrectionSetModel;
import com.jrzh.mvc.search.reptile.ReptileCorrectionSetSearch;
import com.jrzh.mvc.service.reptile.ReptileCorrectionSetServiceI;
import com.jrzh.mvc.view.reptile.ReptileCorrectionSetView;

@Service("reptileCorrectionSetService")
public class ReptileCorrectionSetServiceImpl extends
		BaseServiceImpl<ReptileCorrectionSetModel, ReptileCorrectionSetSearch, ReptileCorrectionSetView> implements
		ReptileCorrectionSetServiceI {

	@Autowired
	private ReptileCorrectionSetRepository reptileCorrectionSetRepository;

	@Override
	public BaseRepository<ReptileCorrectionSetModel,Serializable> getDao() {
		return reptileCorrectionSetRepository;
	}

	@Override
	public BaseConvertI<ReptileCorrectionSetModel, ReptileCorrectionSetView> getConvert() {
		return new ReptileCorrectionSetConvert();
	}

}
