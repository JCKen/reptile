package com.jrzh.mvc.service.reptile;

import java.util.List;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileCommodityHouseInfoModel;
import com.jrzh.mvc.search.reptile.ReptileCommodityHouseInfoSearch;
import com.jrzh.mvc.view.reptile.ReptileCommodityHouseInfoView;

public interface ReptileCommodityHouseInfoServiceI  extends BaseServiceI<ReptileCommodityHouseInfoModel, ReptileCommodityHouseInfoSearch, ReptileCommodityHouseInfoView>{
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException;
	
	public List<ReptileCommodityHouseInfoView> selectGqbInfo(ReptileCommodityHouseInfoView view, SessionUser user) throws ProjectException ;
}