package com.jrzh.mvc.service.reptile.impl;

import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.tools.JsonTools;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileCityConvert;
import com.jrzh.mvc.model.reptile.ReptileCityModel;
import com.jrzh.mvc.repository.reptile.ReptileCityRepository;
import com.jrzh.mvc.search.reptile.ReptileAreaSearch;
import com.jrzh.mvc.search.reptile.ReptileCitySearch;
import com.jrzh.mvc.service.reptile.ReptileCityServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileAreaView;
import com.jrzh.mvc.view.reptile.ReptileCityView;

@Service("reptileCityService")
public class ReptileCityServiceImpl extends
		BaseServiceImpl<ReptileCityModel, ReptileCitySearch, ReptileCityView> implements
		ReptileCityServiceI {

	@Autowired
	private ReptileCityRepository reptileCityRepository;
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;

	@Override
	public BaseRepository<ReptileCityModel,Serializable> getDao() {
		return reptileCityRepository;
	}

	@Override
	public BaseConvertI<ReptileCityModel, ReptileCityView> getConvert() {
		return new ReptileCityConvert();
	}

	@Override
	public void saveInit(String project, InputStream in) throws ProjectException {
		log.info("###########同步"+ project  +"模块 配置表数据 Begin");
		JSONArray array = JsonTools.getFromFile(in);
		if(null != array && array.size() > 0){
			for(int i = 0; i < array.size(); i ++){
				JSONObject jsonObject = array.getJSONObject(i);
				ReptileCityModel reptileCityModel = new ReptileCityModel();
				String group = jsonObject.getString("group");
				String value = jsonObject.getString("value");
				reptileCityModel.setCityName(group);
				reptileCityModel.setLatitudeLongitude(value);
				ReptileCityModel dbmCity = this.findByField("cityName", group);
				if(null == dbmCity){
					log.info("######新增" + reptileCityModel.getCityName());
					this.add(reptileCityModel, SessionUser.getSystemUser());
				}else{
					if(!equals(dbmCity, reptileCityModel)){
						log.info("######编辑" + reptileCityModel.getCityName());
						ReflectUtils.copySameFieldToTargetFilterNull(reptileCityModel, dbmCity);
						this.edit(dbmCity, SessionUser.getSystemUser());
					}
				}
			}
		}
		log.info("###########同步"+project+"模块 配置表数据 End");
	}
	
	private Boolean equals(ReptileCityModel source, ReptileCityModel target) {
		return StringUtils.equals(source.getCityName(), target.getCityName());
	}

	@Override
	public Map<String,List<ReptileAreaView>> findCityArea() throws ProjectException {
		List<ReptileCityModel> modelList = this.findAll();
		Map<String,List<ReptileAreaView>> map = new HashedMap<>();
		if(CollectionUtils.isEmpty(modelList)) return null;
		for(ReptileCityModel model : modelList) {
			ReptileAreaSearch search = new ReptileAreaSearch();
			search.setEqualOwnCity(model.getCityName());
			List<ReptileAreaView> viewList = reptileServiceManage.reptileAreaService.viewList(search);
			map.put(model.getCityName(), viewList);
		}
		return map;
	}
	
	
	
	
}
