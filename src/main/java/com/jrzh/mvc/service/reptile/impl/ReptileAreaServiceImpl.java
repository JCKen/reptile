package com.jrzh.mvc.service.reptile.impl;

import java.io.InputStream;
import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.tools.JsonTools;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileAreaConvert;
import com.jrzh.mvc.model.reptile.ReptileAreaModel;
import com.jrzh.mvc.repository.reptile.ReptileAreaRepository;
import com.jrzh.mvc.search.reptile.ReptileAreaSearch;
import com.jrzh.mvc.service.reptile.ReptileAreaServiceI;
import com.jrzh.mvc.view.reptile.ReptileAreaView;

@Service("reptileAreaService")
public class ReptileAreaServiceImpl extends
		BaseServiceImpl<ReptileAreaModel, ReptileAreaSearch, ReptileAreaView> implements
		ReptileAreaServiceI {

	@Autowired
	private ReptileAreaRepository reptileAreaRepository;

	@Override
	public BaseRepository<ReptileAreaModel,Serializable> getDao() {
		return reptileAreaRepository;
	}

	@Override
	public BaseConvertI<ReptileAreaModel, ReptileAreaView> getConvert() {
		return new ReptileAreaConvert();
	}

	@Override
	public void saveInit(String project, InputStream in) throws ProjectException {
		log.info("###########同步"+ project  +"模块 配置表数据 Begin");
		JSONArray array = JsonTools.getFromFile(in);
		if(null != array && array.size() > 0){
			for(int i = 0; i < array.size(); i ++){
				JSONObject jsonObject = array.getJSONObject(i);
				ReptileAreaModel reptileAreaModel = new ReptileAreaModel();
				String name = jsonObject.getString("name");
				String value = jsonObject.getString("value");
				String group = jsonObject.getString("group");
				reptileAreaModel.setAreaName(name);
				reptileAreaModel.setOwnCity(group);
				reptileAreaModel.setLatitudeLongitude(value);
				ReptileAreaModel dbmArea = this.findByField("areaName", name);
				if(null == dbmArea){
					log.info("######新增" + reptileAreaModel.getOwnCity());
					this.add(reptileAreaModel, SessionUser.getSystemUser());
				}else{
					if(!equals(dbmArea, reptileAreaModel)){
						log.info("######编辑" + reptileAreaModel.getOwnCity());
						ReflectUtils.copySameFieldToTargetFilterNull(reptileAreaModel, dbmArea);
						this.edit(dbmArea, SessionUser.getSystemUser());
					}
				}
			}
		}
		log.info("###########同步"+project+"模块 配置表数据 End");
	}
	
	private Boolean equals(ReptileAreaModel source, ReptileAreaModel target) {
		return StringUtils.equals(source.getOwnCity(), target.getOwnCity());
	}

}
