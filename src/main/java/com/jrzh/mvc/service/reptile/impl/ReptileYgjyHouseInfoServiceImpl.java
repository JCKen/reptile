package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.factory.bj.BJFangGuanJuSpider;
import com.jrzh.factory.gz.GzYgjyHouseInfoSpider;
import com.jrzh.factory.sz.SZFangGuanJuSpider;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileYgjyHouseInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseInfoModel;
import com.jrzh.mvc.repository.reptile.ReptileYgjyHouseInfoRepository;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseInfoSearch;
import com.jrzh.mvc.service.reptile.ReptileYgjyHouseInfoServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseInfoView;
import com.jrzh.thread.bj.BJFangGuanJuHouseInfoThread;
import com.jrzh.thread.gz.GzYgjyHouseInfoThread;
import com.jrzh.thread.sz.SZFangGuanJuHouseInfoThread;

@Service("reptileYgjyHouseInfoService")
public class ReptileYgjyHouseInfoServiceImpl extends
		BaseServiceImpl<ReptileYgjyHouseInfoModel, ReptileYgjyHouseInfoSearch, ReptileYgjyHouseInfoView> implements
		ReptileYgjyHouseInfoServiceI {

	@Autowired
	private ReptileYgjyHouseInfoRepository reptileYgjyHouseInfoRepository;
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;

	@Override
	public BaseRepository<ReptileYgjyHouseInfoModel,Serializable> getDao() {
		return reptileYgjyHouseInfoRepository;
	}

	@Override
	public BaseConvertI<ReptileYgjyHouseInfoModel, ReptileYgjyHouseInfoView> getConvert() {
		return new ReptileYgjyHouseInfoConvert();
	}

	@Override
	public void addYgjyHouseData(SessionUser user) throws ProjectException {
		new GzYgjyHouseInfoSpider().saveYgjyHouseInfo(reptileServiceManage, user);
		new GzYgjyHouseInfoThread("广州", reptileServiceManage).start();
	}

	@Override
	public void addSZFangGuanJuData(SessionUser user) throws ProjectException {
		new SZFangGuanJuSpider().saveSHFangGuanJuHouseInfo(reptileServiceManage, user);
		new SZFangGuanJuHouseInfoThread("深圳",reptileServiceManage).start();	
	}

	@Override
	public void addBJFangGuanJuData(SessionUser user) throws ProjectException {
		new BJFangGuanJuSpider().saveBJFangGuanJuHouseInfo(reptileServiceManage, user);
		new BJFangGuanJuHouseInfoThread("北京",reptileServiceManage).start();
	}
	
	
}
