package com.jrzh.mvc.service.reptile;

import java.util.List;

import com.jrzh.bean.jpexcel.JpExcelBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.config.ProjectConfigration;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileTemplateInfoModel;
import com.jrzh.mvc.model.reptile.ReptileTemplateModel;
import com.jrzh.mvc.search.reptile.ReptileTemplateSearch;
import com.jrzh.mvc.view.reptile.ReptileTemplateInfoView;
import com.jrzh.mvc.view.reptile.ReptileTemplateSumInfoView;
import com.jrzh.mvc.view.reptile.ReptileTemplateView;

public interface ReptileTemplateServiceI
		extends BaseServiceI<ReptileTemplateModel, ReptileTemplateSearch, ReptileTemplateView> {
	public ResultBean changeStatus(String id, SessionUser sessionUser) throws ProjectException;

	public ReptileTemplateModel saveTemplate(ReptileTemplateView templateView, ReptileTemplateInfoView[] views, SessionUser user)
			throws ProjectException;

	public List<ReptileTemplateModel> transferTemplate(String houseType, String type, SessionUser user)
			throws ProjectException;
	
	public List<ReptileTemplateModel> selectTemplateByName(String name, String type,String houseType,SessionUser user)
			throws ProjectException;

	public List<ReptileTemplateInfoModel> transferTemplateInfo(String[] templateIds, SessionUser user)
			throws ProjectException;
	/**
	 * 保存模板
	 **/
	public ReptileTemplateModel saveAllTemplate(ReptileTemplateView templateView, ReptileTemplateSumInfoView templateSumInfoView,
			ReptileTemplateInfoView[] newViews, ReptileTemplateInfoView[] esfViews, ReptileTemplateInfoView[] tudiViews,
			SessionUser user,ProjectConfigration projectConfigration) throws ProjectException;
	/**
	 * 删除模板 试调
	 **/
	public void deleteTemplate(String id, SessionUser user) throws ProjectException;
	
	/**
	 * 根据数组创建导出excel bean
	 **/
	public JpExcelBean createJpExcelBean(ReptileTemplateView templateView, ReptileTemplateSumInfoView templateSumInfoView,
			ReptileTemplateInfoView[] newViews, ReptileTemplateInfoView[] esfViews, ReptileTemplateInfoView[] tudiViews,
			SessionUser user) throws ProjectException;
	
	/**
	 * 根生成文件
	 * @param templateId 模板id
	 * @param ctxUrl 文件地址
	 * @param mapUrl	映射地址
	 * @param dataBean	数据
	 **/
	public void createTemplateFile(String templateId, String ctxUrl, String fileName, JpExcelBean dataBean, SessionUser user) throws ProjectException;

}