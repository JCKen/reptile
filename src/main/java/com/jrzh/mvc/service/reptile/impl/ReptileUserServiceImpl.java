package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.contants.ConfigContants;
import com.jrzh.contants.Contants;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileUserConvert;
import com.jrzh.mvc.model.reptile.ReptileUserModel;
import com.jrzh.mvc.repository.reptile.ReptileUserRepository;
import com.jrzh.mvc.search.reptile.ReptileUserSearch;
import com.jrzh.mvc.service.reptile.ReptileUserServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.service.sys.manage.SysServiceManage;
import com.jrzh.mvc.view.reptile.ReptileUserView;
import com.jrzh.webservice.ReptileWebService;

@Service("reptileUserService")
@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
public class ReptileUserServiceImpl extends
		BaseServiceImpl<ReptileUserModel, ReptileUserSearch, ReptileUserView> implements
		ReptileUserServiceI {

	@Autowired
	private ReptileUserRepository reptileUserRepository;
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	@Autowired
	private SysServiceManage sysServiceManage;
	
	@Override
	public BaseRepository<ReptileUserModel,Serializable> getDao() {
		return reptileUserRepository;
	}

	@Override
	public BaseConvertI<ReptileUserModel, ReptileUserView> getConvert() {
		return new ReptileUserConvert();
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException{
		ResultBean result = new ResultBean();
		ReptileUserModel model = this.findById(id);
		
		model.setIsDisable(!model.getIsDisable());
		edit(model, sessionUser);
		result.setStatus(ResultBean.SUCCESS);
		return result;
	}

	@Override
	public void saveUserInfo(SessionUser sessionUser) throws ProjectException {
		try {
			StringBuffer xmlStr = ReptileWebService.reptileWebService(Contants.USER_NAME, Contants.USER_PASSWORD);
			String str = xmlStr.toString();
			str = StringUtils.replace(str, "&lt;", "<");
			str = StringUtils.replace(str, "&gt;", ">");
			Document doc = Jsoup.parse(str);
			Elements userEle = doc.select("ds");
			String initPwd = sysServiceManage.configService.getValue(ConfigContants.CONFIG_INIT_PWD);
			initPwd = StringUtils.isBlank(initPwd)? "123456": initPwd;
			ReptileUserSearch search = new ReptileUserSearch();
			for(Element e : userEle) {
				Element userNameEle = e.selectFirst("UserName");//姓名
				Element staffCodeEle = e.selectFirst("StaffCode");//员工号
				Element deptNameEle = e.selectFirst("Dept_Name");//部门
				Element isStopEle = e.selectFirst("IsStop");//是否停用（true:停用，false:启用）
				Element mobileEle = e.selectFirst("Mobile");//手机号
//				Element IDNumberLast6Ele = e.selectFirst("IDNumberLast6");//身份证后六位
				if(StringUtils.isBlank(staffCodeEle.text())) continue;
				search.setEqualUserId(staffCodeEle.text());
				ReptileUserModel model = reptileServiceManage.reptileUserService.first(search);
				if(model ==null ){
					model = new ReptileUserModel();
					model.setPwd(initPwd);
				}
				if(null != userNameEle) {
					model.setUserName(userNameEle.text());
				}
				if(null != staffCodeEle) {
					model.setUserId(staffCodeEle.text());			
				}
				if(null != deptNameEle) {
					model.setDeptName(deptNameEle.text());
				}
				if(null != isStopEle) {
					model.setIsStop(Contants.USER_IS_STOP.STOP);
					if(StringUtils.equals("false", isStopEle.text())) {
						model.setIsStop(Contants.USER_IS_STOP.NO_STOP);
					}
				}
				if(null != mobileEle) {
					model.setMobile(mobileEle.text());
				}
				reptileServiceManage.reptileUserService.edit(model, sessionUser);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public ReptileUserView saveSessionUser(ReptileUserView view) throws ProjectException {
		if(StringUtils.isBlank(view.getUserId())){
			throw new ProjectException("工号不能为空");
		}
		if(StringUtils.isBlank(view.getPwd())){
			throw new ProjectException("密码不能为空");
		}
		ReptileUserView checkView = reptileServiceManage.reptileUserService.findViewByField("userId", view.getUserId());
		if(null == checkView) {
			throw new ProjectException("该用户不存在");
		}
		if(!StringUtils.equals(checkView.getPwd(), view.getPwd())){
			throw new ProjectException("密码错误，如忘记密码请联系管理员");
		}
		return checkView;
	}

}
