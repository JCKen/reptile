package com.jrzh.mvc.service.reptile;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileEstateDatumModel;
import com.jrzh.mvc.search.reptile.ReptileEstateDatumSearch;
import com.jrzh.mvc.view.reptile.ReptileEstateDatumView;

public interface ReptileEstateDatumServiceI  extends BaseServiceI<ReptileEstateDatumModel, ReptileEstateDatumSearch, ReptileEstateDatumView>{

	void saveEstateInvestData(String classify,String menu,String url,SessionUser systemUser) throws ProjectException;

	// 保存上海宏观数据
	void saveSHMacroscopicData(SessionUser systemUser) throws ProjectException;

	void saveGzMacroscopicData(SessionUser systemUser)throws ProjectException;

}