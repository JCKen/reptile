package com.jrzh.mvc.service.reptile;

import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.mvc.model.reptile.ReptileEstateZbModel;
import com.jrzh.mvc.search.reptile.ReptileEstateZbSearch;
import com.jrzh.mvc.view.reptile.ReptileEstateZbView;

public interface ReptileEstateZbServiceI  extends BaseServiceI<ReptileEstateZbModel, ReptileEstateZbSearch, ReptileEstateZbView>{

}