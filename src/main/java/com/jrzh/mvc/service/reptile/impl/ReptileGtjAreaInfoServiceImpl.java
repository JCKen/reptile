package com.jrzh.mvc.service.reptile.impl;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.tools.CollectionTools;
import com.jrzh.factory.bj.BjAreaInfoSpider;
import com.jrzh.factory.cd.CDAreaInfoSpider;
import com.jrzh.factory.dg.DGAreaInfoSpider;
import com.jrzh.factory.fh.FhAreaInfoSpider;
import com.jrzh.factory.fs.FSAreaInfoSpider;
import com.jrzh.factory.gz.GzAreaInfoSpider;
import com.jrzh.factory.hz.HzAreaInfoSpider;
import com.jrzh.factory.jm.JMAreaInfoSpider;
import com.jrzh.factory.ks.KsAreaInfoSpider;
import com.jrzh.factory.qy.QYAreaInfoSpider;
import com.jrzh.factory.sh.SHAreaInfoSpider;
import com.jrzh.factory.sz.SZAreaSpider;
import com.jrzh.factory.tj.TJAreaSpider;
import com.jrzh.factory.xa.XaAreaInfoSider;
import com.jrzh.factory.zh.ZHAreaInfoSpider;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileGtjAreaInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;
import com.jrzh.mvc.repository.reptile.ReptileGtjAreaInfoRepository;
import com.jrzh.mvc.search.reptile.ReptileGtjAreaInfoSearch;
import com.jrzh.mvc.service.reptile.ReptileGtjAreaInfoServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileGtjAreaInfoView;

@Service("reptileGtjAreaInfoService")
public class ReptileGtjAreaInfoServiceImpl
		extends BaseServiceImpl<ReptileGtjAreaInfoModel, ReptileGtjAreaInfoSearch, ReptileGtjAreaInfoView>
		implements ReptileGtjAreaInfoServiceI {

	@Autowired
	private ReptileGtjAreaInfoRepository reptileGtjAreaInfoRepository;

	@Autowired
	private ReptileServiceManage reptileServiceManage;

	@PersistenceContext
	EntityManager em;

	@Override
	public BaseRepository<ReptileGtjAreaInfoModel, Serializable> getDao() {
		return reptileGtjAreaInfoRepository;
	}

	@Override
	public BaseConvertI<ReptileGtjAreaInfoModel, ReptileGtjAreaInfoView> getConvert() {
		return new ReptileGtjAreaInfoConvert();
	}

	@Override
	public void saveSZLandData(SessionUser user) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("深圳");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		log.info("#深圳目前的土地数据量#" + cont);
		// 判断是否第一次爬取数据 ，如果是第一次 就需要抓取更多的数据
		Integer maxPage = 2;
		if (cont <= 0L) {
			maxPage = 59;
		}
		log.info("#深圳本次抓取页数#" + maxPage);
		List<ReptileGtjAreaInfoModel> models = SZAreaSpider.getSZAreaInfo(maxPage);
		Set<ReptileGtjAreaInfoModel> modelssSet = new HashSet<ReptileGtjAreaInfoModel>(models);
		if (CollectionTools.isNotBlank(modelssSet)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : modelssSet) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, user);
				}
			}
		}
	}

	@Override
	public void saveGzLandData(SessionUser user) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("广州");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		log.info("#广州目前的土地数据量#" + cont);
		// 判断是否第一次爬取数据 ，如果是第一次 就需要抓取更多的数据
		Integer maxPage = 2;
		if (cont <= 0L) {
			maxPage = 60;
		}
		log.info("#广州本次抓取页数#" + maxPage);
		List<ReptileGtjAreaInfoModel> models = GzAreaInfoSpider.getGZAreaInfo(maxPage);
		Set<ReptileGtjAreaInfoModel> modelssSet = new HashSet<ReptileGtjAreaInfoModel>(models);
		if (CollectionTools.isNotBlank(modelssSet)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : modelssSet) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, user);
				}
			}
		}
	}

	@Override
	public void saveBjLandData(SessionUser user) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("北京");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		log.info("#北京目前的土地数据量#" + cont);
		// 判断是否第一次爬取数据 ，如果是第一次 就需要抓取更多的数据
		String endrecord = "50";
		if (cont <= 0L) {
			endrecord = "1500";
		}
		log.info("#北京本次抓取页数#" + endrecord);
		List<ReptileGtjAreaInfoModel> models = BjAreaInfoSpider.getBJAreaInfo(endrecord);
		Set<ReptileGtjAreaInfoModel> modelssSet = new HashSet<ReptileGtjAreaInfoModel>(models);
		if (CollectionTools.isNotBlank(modelssSet)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : modelssSet) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, user);
				}
			}
		}
	}

	@Override
	public void saveSHLandData(SessionUser user) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("上海");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		log.info("#上海目前的土地数据量#" + cont);
		// 判断是否第一次爬取数据 ，如果是第一次 就需要抓取更多的数据
		Integer maxPage = 1;
		if (cont <= 0L) {
			maxPage = 56;
		}
		log.info("#上海本次抓取页数#" + maxPage);
		List<ReptileGtjAreaInfoModel> models = SHAreaInfoSpider.getSHArea(maxPage);
		Set<ReptileGtjAreaInfoModel> modelssSet = new HashSet<ReptileGtjAreaInfoModel>(models);
		if (CollectionTools.isNotBlank(modelssSet)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : modelssSet) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, user);
				}
			}
		}
	}

	// 成都土地
	@Override
	public void saveCdLandData(SessionUser user) throws ProjectException {
		// TODO Auto-generated method stub
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("成都");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		log.info("#成都目前的土地数据量#" + cont);
		Integer maxPage = 1;
		if (cont <= 0L) {
			maxPage = 39;
		}
		log.info("#成都本次抓取页数#" + maxPage);
		List<ReptileGtjAreaInfoModel> models = CDAreaInfoSpider.getInfo(maxPage);
		Set<ReptileGtjAreaInfoModel> modelssSet = new HashSet<ReptileGtjAreaInfoModel>(models);
		if (CollectionTools.isNotBlank(modelssSet)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : modelssSet) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				// 判断是否有这块土地 没有就添加
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, user);
				}
			}
		}
	}

	// 天津土地
	@Override
	public void saveTJLandData(SessionUser user) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("天津");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		log.info("#天津目前的土地数据量#" + cont);
		// 判断是否第一次爬取数据 ，如果是第一次 就需要抓取更多的数据
		Integer maxPage = 5;
		if (cont <= 0L) {
			maxPage = 400;
		}
		log.info("#天津本次抓取页数#" + maxPage);
		List<ReptileGtjAreaInfoModel> models;
		try {
			models = TJAreaSpider.getTJAreaInfo(maxPage);
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
		if (CollectionTools.isNotBlank(models)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : models) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, user);
				}
			}
		}
	}

	// 珠海土地
	@Override
	public void saveZhLandData(SessionUser systemUser) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("珠海");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		log.info("#珠海目前的土地数据量#" + cont);
		// 判断是否第一次爬取数据 ，如果是第一次 就需要抓取更多的数据
		Integer maxPage = 1;
		if (cont <= 0L) {
			maxPage = 28;
		}
		log.info("#珠海本次抓取页数#" + maxPage);
		List<ReptileGtjAreaInfoModel> models = ZHAreaInfoSpider.getInfo(maxPage);
		if (CollectionTools.isNotBlank(models)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : models) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, systemUser);
				}
			}
		}
	}

	// 佛山
	@Override
	public void saveFsLandData(SessionUser systemUser) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("佛山");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		log.info("#佛山目前的土地数据量#" + cont);
		// 判断是否第一次爬取数据 ，如果是第一次 就需要抓取更多的数据
		Integer maxPage = 1;
		if (cont <= 0L) {
			maxPage = 4;
		}
		log.info("#佛山本次抓取页数#" + maxPage);
		List<ReptileGtjAreaInfoModel> models = FSAreaInfoSpider.getAreaInfo(maxPage);
		if (CollectionTools.isNotBlank(models)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : models) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, systemUser);
				}
			}
		}
	}

	// 东莞土地
	@Override
	public void saveDgLandData(SessionUser systemUser) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("东莞");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		log.info("#东莞目前的土地数据量#" + cont);
		// 判断是否第一次爬取数据 ，如果是第一次 就需要抓取更多的数据
		Integer number = 10;
		if (cont <= 0L) {
			number = 100;
		}
		log.info("#东莞本次抓取数据数量#" + number);
		List<ReptileGtjAreaInfoModel> models = DGAreaInfoSpider.getAreaInfo(number);
		if (CollectionTools.isNotBlank(models)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : models) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, systemUser);
				}
			}
		}
	}

	/**
	 * 清远土地
	 */
	@Override
	public void saveQyLandData(SessionUser systemUser) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("清远");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		log.info("#清远目前的土地数据量#" + cont);
		// 判断是否第一次爬取数据 ，如果是第一次 就需要抓取更多的数据
		Integer maxPage = 1;
		if (cont <= 0L) {
			maxPage = 32;
		}
		log.info("#清远本次抓取数据数量#" + maxPage);
		List<ReptileGtjAreaInfoModel> models = QYAreaInfoSpider.getInfo(maxPage);
		if (CollectionTools.isNotBlank(models)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : models) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, systemUser);
				}
			}
		}
	}

	@Override
	public List<ReptileGtjAreaInfoView> findAllInfo(ReptileGtjAreaInfoView view, Integer page, SessionUser user)
			throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity(view.getCity());
		search.setLikeAddress(view.getAddress());
		search.setLikeUse(view.getUse());
		search.setNotNullPrice("price");
		if (null != page) {
			search.setPage(page);
			search.setRows(6);
		}
		List<ReptileGtjAreaInfoView> viewList = reptileServiceManage.reptileGtjAreaInfoService.viewList(search);
		return viewList;
	}

	@Override
	public Integer totalCount(ReptileGtjAreaInfoView view) throws ProjectException {
		try {
			StringBuilder whereSql = new StringBuilder("select count(*) from reptile_gtj_area_info where 1 = 1 ");
			if (StringUtils.isNotBlank(view.getCity())) {
				whereSql.append(" AND _city = '" + view.getCity() + "'");
			}
			if (StringUtils.isNotBlank(view.getAddress())) {
				whereSql.append(" AND _address like '%" + view.getAddress() + "%' ");
			}
			if (StringUtils.isNotBlank(view.getUse())) {
				whereSql.append(" AND _use like '%" + view.getUse() + "%' ");
			}
			Query query = em.createNativeQuery(whereSql.toString());
			List<BigInteger[]> count = query.getResultList();
			Object[] c = count.toArray();
			if (CollectionUtils.isEmpty(count))
				return 0;
			return Integer.valueOf(c[0].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<ReptileGtjAreaInfoView> findByIds(String[] ids) throws ProjectException {
		List<ReptileGtjAreaInfoView> gtjAreaInfoViewList = reptileServiceManage.reptileGtjAreaInfoService
				.listViewInField("id", ids);
		if (CollectionUtils.isEmpty(gtjAreaInfoViewList)) {
			return null;
		}
		return gtjAreaInfoViewList;
	}

	@Override
	public Integer findJpCount(ReptileGtjAreaInfoView view, Integer distance) throws ProjectException {
		Integer count = 0;
		distance = distance == 0 ? 1000000000 : distance;
		if (StringUtils.isNotBlank(view.getUse())) {
			count = reptileGtjAreaInfoRepository.selectJpCountByUse(view.getLat(), view.getLnt(), view.getCity(),
					view.getUse(), distance);
		} else {
			count = reptileGtjAreaInfoRepository.selectJpCount(view.getLat(), view.getLnt(), view.getCity(), distance);
		}
		return count;
	}

	@Override
	public List<ReptileGtjAreaInfoView> selectJp(ReptileGtjAreaInfoView view, Integer distance, Integer page) {
		page = (page - 1) * 6;
		distance = distance == 0 ? 1000000000 : distance;
		List<ReptileGtjAreaInfoModel> models;
		if (StringUtils.isNotBlank(view.getUse())) {
			models = reptileGtjAreaInfoRepository.selectJpByUse(view.getCity(), view.getLat(), view.getLnt(),
					view.getUse(), distance, page);
		} else {
			models = reptileGtjAreaInfoRepository.selectJp(view.getCity(), view.getLat(), view.getLnt(), distance,
					page);
		}
		List<ReptileGtjAreaInfoView> views;
		try {
			views = reptileServiceManage.reptileGtjAreaInfoService.convertByModelList(models);
			return views;
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取江门土地数据
	 */
	@Override
	public void saveJmLandData(SessionUser systemUser) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("江门");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		Integer maxPage = 1;
		if (cont <= 0L) {
			maxPage = 10;
		}
		log.info("#江门本次抓取数据数量#" + maxPage);
		List<ReptileGtjAreaInfoModel> models = JMAreaInfoSpider.getInfo(maxPage);
		if (CollectionTools.isNotBlank(models)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : models) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, systemUser);
				}
			}
		}
	}

	/**
	 * 保存奉化土地
	 */
	@Override
	public void saveFhLandData(SessionUser systemUser) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("奉化");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		Integer maxPage = 1;
		if (cont <= 0L) {
			maxPage = 2;
		}
		log.info("#奉化本次抓取数据数量#" + maxPage);
		List<ReptileGtjAreaInfoModel> models = FhAreaInfoSpider.getData();
		if (CollectionTools.isNotBlank(models)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : models) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, systemUser);
				}
			}
		}
	}
	
	// 保存 惠州土地数据
	@Override
	public void saveHzLandData(SessionUser systemUser) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("惠州");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		Integer maxPage = 5;
		if (cont <= 0L) {
			maxPage = 210;
		}
		List<ReptileGtjAreaInfoModel> models = HzAreaInfoSpider.getData(maxPage);
		if (CollectionTools.isNotBlank(models)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : models) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, systemUser);
				}
			}
		}
	}

	// 保存西安土地数据
	@Override
	public void saveXALandData(SessionUser systemUser) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("西安");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		Integer maxPage = 1;
		if (cont <= 0L) {
			maxPage = 50;
		}
		List<ReptileGtjAreaInfoModel> models = XaAreaInfoSider.getInfo(maxPage);
		if (CollectionTools.isNotBlank(models)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : models) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, systemUser);
				}
			}
		}
	}

	// 保存k昆山土地数据
	@Override
	public void saveKsLandData(SessionUser systemUser) throws ProjectException {
		ReptileGtjAreaInfoSearch search = new ReptileGtjAreaInfoSearch();
		search.setEqualCity("昆山");
		Long cont = reptileServiceManage.reptileGtjAreaInfoService.count(search);
		Integer maxPage = 1;
		if (cont <= 0L) {
			maxPage = 25;
		}
		List<ReptileGtjAreaInfoModel> models = KsAreaInfoSpider.getInfo(maxPage);
		if (CollectionTools.isNotBlank(models)) {
			for (ReptileGtjAreaInfoModel reptileGtjAreaInfoModel : models) {
				search.setEqualAreaNo(reptileGtjAreaInfoModel.getAreaNo());
				ReptileGtjAreaInfoModel newModel = reptileServiceManage.reptileGtjAreaInfoService.first(search);
				if (null == newModel) {
					add(reptileGtjAreaInfoModel, systemUser);
				}
			}
		}
	}

}
