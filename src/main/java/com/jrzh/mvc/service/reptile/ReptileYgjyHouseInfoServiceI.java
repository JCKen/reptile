package com.jrzh.mvc.service.reptile;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseInfoModel;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseInfoSearch;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseInfoView;

public interface ReptileYgjyHouseInfoServiceI  extends BaseServiceI<ReptileYgjyHouseInfoModel, ReptileYgjyHouseInfoSearch, ReptileYgjyHouseInfoView>{
	
	/**
	 * @param user
	 * @throws ProjectException
	 * 阳光佳缘
	 */
	void addYgjyHouseData(SessionUser user) throws ProjectException;
	
	/**
	 * @param user
	 * @throws ProjectException
	 * 深圳房管局
	 */
	void addSZFangGuanJuData(SessionUser user) throws ProjectException;
	
	/**
	 * @param user
	 * @throws ProjectException
	 * 北京房管局
	 */
	void addBJFangGuanJuData(SessionUser user) throws ProjectException;
	
}