package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.mvc.convert.reptile.ReptileNewhouseDealEverydayConvert;
import com.jrzh.mvc.repository.reptile.ReptileNewhouseDealEverydayRepository;
import com.jrzh.mvc.model.reptile.ReptileNewhouseDealEverydayModel;
import com.jrzh.mvc.search.reptile.ReptileNewhouseDealEverydaySearch;
import com.jrzh.mvc.service.reptile.ReptileNewhouseDealEverydayServiceI;
import com.jrzh.mvc.view.reptile.ReptileNewhouseDealEverydayView;

@Service("reptileNewhouseDealEverydayService")
public class ReptileNewhouseDealEverydayServiceImpl extends
		BaseServiceImpl<ReptileNewhouseDealEverydayModel, ReptileNewhouseDealEverydaySearch, ReptileNewhouseDealEverydayView> implements
		ReptileNewhouseDealEverydayServiceI {

	@Autowired
	private ReptileNewhouseDealEverydayRepository reptileNewhouseDealEverydayRepository;

	@Override
	public BaseRepository<ReptileNewhouseDealEverydayModel,Serializable> getDao() {
		return reptileNewhouseDealEverydayRepository;
	}

	@Override
	public BaseConvertI<ReptileNewhouseDealEverydayModel, ReptileNewhouseDealEverydayView> getConvert() {
		return new ReptileNewhouseDealEverydayConvert();
	}

}
