package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrzh.bean.ReptileSearchFloorResultBean;
import com.jrzh.bean.ReptileSearchPriceResultBean;
import com.jrzh.bean.ReptileSearchResultBean;
import com.jrzh.bean.priceTrend.PriceBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.tools.CollectionTools;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.contants.HouseType;
import com.jrzh.factory.webSpider.HouseWorldSpider;
import com.jrzh.factory.webSpider.NewHouseWorldSpider;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileNewHouseConvert;
import com.jrzh.mvc.model.reptile.ReptileHousePriceModel;
import com.jrzh.mvc.model.reptile.ReptileNewHouseModel;
import com.jrzh.mvc.repository.reptile.ReptileNewHouseRepository;
import com.jrzh.mvc.search.reptile.ReptileHousePriceSearch;
import com.jrzh.mvc.search.reptile.ReptileNewHouseSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseFloorInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseInfoSearch;
import com.jrzh.mvc.service.reptile.ReptileNewHouseServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileNewHouseView;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseFloorInfoView;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseInfoView;
import com.jrzh.util.ObjectConvert;

@Service("reptileNewHouseService")
public class ReptileNewHouseServiceImpl
		extends BaseServiceImpl<ReptileNewHouseModel, ReptileNewHouseSearch, ReptileNewHouseView>
		implements ReptileNewHouseServiceI {

	@Autowired
	private ReptileNewHouseRepository reptileNewHouseRepository;

	@Autowired
	private ReptileServiceManage reptileServiceManage;

	@PersistenceContext
	EntityManager em;

	@Override
	public BaseRepository<ReptileNewHouseModel, Serializable> getDao() {
		return reptileNewHouseRepository;
	}

	@Override
	public BaseConvertI<ReptileNewHouseModel, ReptileNewHouseView> getConvert() {
		return new ReptileNewHouseConvert();
	}

	@Override
	public void findNewHouseFromLianjiaInGZ(SessionUser user) throws ProjectException {
		new NewHouseWorldSpider().saveNewHouseInfoFromInGZ(reptileServiceManage, user);
	}

	@Override
	public List<ReptileSearchResultBean> findHouseInfo(ReptileNewHouseView view) throws ProjectException {
		try {
			StringBuilder whereSql = new StringBuilder(
					"SELECT _house_name,_house_city AS houseCity,_house_part AS housePart,SUM(cast(_building_area AS SIGNED)) AS buildingArea,SUM(cast(_area_covered AS SIGNED)) AS areaCovered FROM reptile_new_house WHERE 1 = 1 ");
			if (StringUtils.isNotBlank(view.getHouseName())) {
				whereSql.append(" AND _house_name = :houseName ");
			}
			if (StringUtils.isNotBlank(view.getHouseSaleStatus())) {
				whereSql.append(" AND _house_sale_status = :houseSaleStatus ");
			}
			if (StringUtils.isNotBlank(view.getIsNewHouse())) {
				whereSql.append(" AND _is_new_house = :isNewHouse ");
			}
			if (StringUtils.isNotBlank(view.getHouseCity())) {
				whereSql.append(" AND _house_city = :houseCity ");
			}
			if (StringUtils.isNotBlank(view.getHousePart())) {
				whereSql.append(" AND _house_part = :housePart ");
			}
			if (StringUtils.isNotBlank(view.getHouseType())) {
				whereSql.append(" AND _house_type = :houseType ");
			}
			if (null != view.getInfoUpdateTime()) {
				whereSql.append(" AND _info_update_time BETWEEN :beginTime AND :endTime ");
			}
			whereSql.append(" GROUP BY _house_city,_house_part");
			log.info("### ƴװsql ### begin");
			Query query = em.createNativeQuery(whereSql.toString());
			if (StringUtils.isNotBlank(view.getHouseName())) {
				query.setParameter("houseName", view.getHouseName());
			}
			if (StringUtils.isNotBlank(view.getHouseSaleStatus())) {
				query.setParameter("houseSaleStatus", view.getHouseSaleStatus());
			}
			if (StringUtils.isNotBlank(view.getIsNewHouse())) {
				query.setParameter("isNewHouse", view.getIsNewHouse());
			}
			if (StringUtils.isNotBlank(view.getHouseCity())) {
				query.setParameter("houseCity", view.getHouseCity());
			}
			if (StringUtils.isNotBlank(view.getHousePart())) {
				query.setParameter("housePart", view.getHousePart());
			}
			if (StringUtils.isNotBlank(view.getHouseType())) {
				query.setParameter("houseType", view.getHouseType());
			}
			if (null != view.getBeginTime()) {
				query.setParameter("beginTime", view.getBeginTime());
			}
			if (null != view.getEndTime()) {
				query.setParameter("endTime", view.getEndTime());
			}
			log.info("### ƴװsql ### end");
			List<Object[]> objectList = query.getResultList();
			String[] fields = { "houseName", "houseCity", "housePart", "buildingArea", "areaCovered" };
			List<ReptileSearchResultBean> reptileSearchResultBeanList = new ArrayList<>();
			reptileSearchResultBeanList = ObjectConvert.dataToList(ReptileSearchResultBean.class, objectList, fields);
			return reptileSearchResultBeanList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<ReptileSearchPriceResultBean> findHouseAVG(ReptileNewHouseView view) throws ProjectException {
		try {
			StringBuilder whereSql = new StringBuilder(
					"SELECT AVG(price._house_price) AS priceAvg,SUM(price._house_price) AS priceSum,house._house_city AS houseCity,house._house_part AS housePart,house._house_sale_status houseStatus FROM reptile_new_house AS house LEFT JOIN reptile_house_price AS price ON house._id = price._house_id WHERE price._house_price_unit = '1' ");
			if (StringUtils.isNotBlank(view.getHouseCity())) {
				whereSql.append(" AND _house_city = :houseCity ");
			}
			whereSql.append(" GROUP BY house._house_city, house._house_part,house._house_sale_status");
			Query query = em.createNativeQuery(whereSql.toString());
			if (StringUtils.isNotBlank(view.getHouseCity())) {
				query.setParameter("houseCity", view.getHouseCity());
			}
			log.info("### ƴװsql ### end");
			List<Object[]> objectList = query.getResultList();
			String[] fields = { "priceAvg", "priceSum", "houseCity", "housePart", "houseStatus" };
			List<ReptileSearchPriceResultBean> reptileSearchPriceResultBeanList = new ArrayList<>();
			reptileSearchPriceResultBeanList = ObjectConvert.dataToList(ReptileSearchPriceResultBean.class, objectList,
					fields);
			return reptileSearchPriceResultBeanList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Integer totalCount(ReptileNewHouseView view) throws ProjectException {
		try {
			StringBuilder whereSql = new StringBuilder("select count(*) from reptile_new_house where 1 = 1 ");
			if (StringUtils.isNotBlank(view.getHouseCity())) {
				whereSql.append(" AND _house_city = '" + view.getHouseCity() + "'");
			}
			if (StringUtils.isNotBlank(view.getHouseName())) {
				whereSql.append(" AND _house_name like '%" + view.getHouseName() + "%' ");
			}
			if (StringUtils.isNotBlank(view.getHouseType())) {
				whereSql.append(" AND _house_type like '%" + view.getHouseType() + "%' ");
//				if(!StringUtils.equals(view.getHouseType(), "其它")){
//					whereSql.append(" AND _house_type like '%" + view.getHouseType() + "%' ");
//				}else{
//					whereSql.append(" AND _house_type not in ("+""+")");
//					
//				}
			}
			if (StringUtils.isNotBlank(view.getHousePart())) {
				whereSql.append(" AND _house_part like '%" + view.getHousePart() + "%'");
			}
			Query query = em.createNativeQuery(whereSql.toString());
			List<BigInteger[]> count = query.getResultList();
			Object[] c = count.toArray();
			if (CollectionUtils.isEmpty(count))
				return 0;
			return Integer.valueOf(c[0].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<ReptileNewHouseView> findTodayHouseInfo() throws ProjectException {
		ReptileNewHouseSearch search = new ReptileNewHouseSearch();
		// String strData = DateUtil.format(new Date() ,"yyyy-MM-dd");
		//
		// search.setEqualInfoUpdateTime(strData+" 00:00:00");
		List<ReptileNewHouseView> list = this.viewList(search);
		System.out.println(list.size());
		return list;
	}

	@Override
	public ReptileSearchFloorResultBean selectById(String id) throws ProjectException {
		ReptileNewHouseModel newHouseModel = reptileServiceManage.reptileNewHouseService.findById(id);
		if (null == newHouseModel) {
			throw new ProjectException("无此找到相关楼盘信息");
		}
		String selectInfo = newHouseModel.getHouseOtherName();
		if (StringUtils.isNotBlank(selectInfo)) {
			String houseName = selectInfo.trim();
			String[] hoseNames = StringUtils.split(houseName, ",");
			System.out.println(hoseNames[0]);
			selectInfo = hoseNames[0];
		} else {
			selectInfo = newHouseModel.getHouseName();
		}
		ReptileYgjyHouseInfoSearch reptileYgjyHouseInfoSearch = new ReptileYgjyHouseInfoSearch();
		reptileYgjyHouseInfoSearch.setEqualCity(newHouseModel.getHouseCity());
		reptileYgjyHouseInfoSearch.setLikeProductName(selectInfo);
		List<ReptileYgjyHouseInfoView> reptileYgjyHouseInfoViewList = reptileServiceManage.reptileYgjyHouseInfoService
				.viewList(reptileYgjyHouseInfoSearch);
		if (StringUtils.isNotBlank(newHouseModel.getHouseName())
				&& CollectionUtils.isEmpty(reptileYgjyHouseInfoViewList)) {
			reptileYgjyHouseInfoSearch.setLikeProductName(newHouseModel.getHouseName());
			reptileYgjyHouseInfoViewList = reptileServiceManage.reptileYgjyHouseInfoService
					.viewList(reptileYgjyHouseInfoSearch);
		}
		List<List<ReptileYgjyHouseFloorInfoView>> flooList = new ArrayList<>();
		List<String> floorNameList = new ArrayList<>();
		ReptileSearchFloorResultBean reptileSearchFloorResultBean = new ReptileSearchFloorResultBean();
		for (ReptileYgjyHouseInfoView reptileYgjyHouseInfoView : reptileYgjyHouseInfoViewList) {
			String houseInfoId = reptileYgjyHouseInfoView.getId();
			ReptileYgjyHouseFloorInfoSearch reptileYgjyHouseFloorInfoSearch = new ReptileYgjyHouseFloorInfoSearch();
			reptileYgjyHouseFloorInfoSearch.setEqualHouseId(houseInfoId);
			List<ReptileYgjyHouseFloorInfoView> flooViewList = reptileServiceManage.reptileYgjyHouseFloorInfoService
					.viewList(reptileYgjyHouseFloorInfoSearch);
			flooList.add(flooViewList);
			floorNameList.add(reptileYgjyHouseInfoView.getProductName() + reptileYgjyHouseInfoView.getPreSalePermit());
			reptileSearchFloorResultBean.setFloorList(flooList);
			reptileSearchFloorResultBean.setFloorNameList(floorNameList);
		}
		return reptileSearchFloorResultBean;
	}

	@Override
	public void findNewHouseFromHouseWorldInGZ(SessionUser user) throws ProjectException {
		new HouseWorldSpider().saveHouseInfoFromHouseWorldInGZ(reptileServiceManage, user);
	}

	@Override
	public List<ReptileNewHouseView> findAllHouseInfo(ReptileNewHouseView view, Integer page, SessionUser user)
			throws ProjectException {
		ReptileNewHouseSearch search = new ReptileNewHouseSearch();
		if (StringUtils.isNotBlank(view.getHouseCity())) {
			search.setEqualHouseCity(view.getHouseCity());
		}
		if (StringUtils.isNotBlank(view.getHousePart())) {
			search.setEqualHousePart(view.getHousePart());
		}
		if (StringUtils.isNotBlank(view.getHouseName())) {
			search.setLikeHouseName(view.getHouseName());
		}
		if (StringUtils.isNotBlank(view.getHouseType())) {
//			search.setLikeHouseType(view.getHouseType());
			if(!view.getHouseType().equals("其它")){
				search.setLikeHouseType(view.getHouseType());
			}else{
				Map<Integer, List<String>> map = getHouseTypes(HouseType.houseTypes);
				List<String> list = new ArrayList<>();
				for (List<String> val : map.values()) {
					for (String string : val) {
						list.add(string);
					}
				}
				String[] str = new String[list.size()];
				for (int i = 0; i < list.size(); i++) {
					str[i] = list.get(i);
				}
				StringBuilder ss = new StringBuilder();
				for (String string : str) { 
					ss.append("\""+string+"\""+",");
				}
				search.setNotInHouseTypes(ss.substring(0,ss.length()-1));
			}
		}
		if (null != page) {
			search.setPage(page);
			search.setRows(6);
		}
//		 if(StringUtils.equals(view.getIsToday(), "true")){
//		 	String strData = DateUtil.format(new Date(),"yyyy-MM-dd");
//			search.setEqualInfoUpdateTime(strData+" 00:00:00");
//		 }
		List<ReptileNewHouseView> houseViewList = reptileServiceManage.reptileNewHouseService.viewList(search);
		if (CollectionUtils.isEmpty(houseViewList)) {
			return null;
		}
		String[] ids = CollectionTools.collect(houseViewList, "id", new String[] {});
		List<ReptileHousePriceModel> priceModelList = reptileServiceManage.reptileHousePriceService
				.listInByField("houseId", ids);
		if (CollectionUtils.isEmpty(priceModelList)) {
			return houseViewList;
		}
		// 添加价格列表展示到前端
		for (ReptileNewHouseView houseView : houseViewList) {
			String id = houseView.getId();
			List<String> priceList = new ArrayList<>();
			for (ReptileHousePriceModel priceModel : priceModelList) {
				if (StringUtils.equals(priceModel.getHouseId(), id)) {
					if (StringUtils.isEmpty(priceModel.getHousePrice())) {
						priceList.add("待定," + priceModel.getInfoSource());
					} else {
						priceList.add(priceModel.getHousePrice());
					}
				}
				houseView.setPriceList(priceList);
			}
		}
		return houseViewList;
	}

	// 查询周边信息
	@Override
	public List<ReptileNewHouseView> findSurrounding(BigDecimal longitude, BigDecimal latitude, String math)
			throws ProjectException {
		try {
			if (null == longitude || null == latitude)
				return null;
			List<ReptileNewHouseModel> newList = reptileNewHouseRepository.findSurrounding(longitude, latitude, math);

			if (CollectionUtils.isEmpty(newList))
				return null;
			String[] ids = CollectionTools.collect(newList, "id", new String[] {});
			List<ReptileHousePriceModel> priceModelList = reptileServiceManage.reptileHousePriceService
					.listInByField("houseId", ids);
			List<ReptileNewHouseView> viewList = new ArrayList<>();
			for (ReptileNewHouseModel houseModel : newList) {
				ReptileNewHouseView houseView = new ReptileNewHouseConvert().convertToView(houseModel);
				String id = houseModel.getId();
				List<String> priceList = new ArrayList<>();
				if (CollectionUtils.isEmpty(priceModelList)) {
					continue;
				}
				for (ReptileHousePriceModel priceModel : priceModelList) {
					if (StringUtils.equals(priceModel.getHouseId(), id)) {
						if (StringUtils.isEmpty(priceModel.getHousePrice())) {
							priceList.add("待定," + priceModel.getInfoSource());
						} else {
							priceList.add(priceModel.getHousePrice());
						}
					}
					houseView.setPriceList(priceList);
				}
				viewList.add(houseView);
			}
			return viewList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<ReptileNewHouseView> findByIds(String time, String ids[]) throws ProjectException {
		List<ReptileNewHouseView> houseViewList = reptileServiceManage.reptileNewHouseService.listViewInField("id",
				ids);
		if (CollectionUtils.isEmpty(houseViewList)) {
			return null;
		}
		List<ReptileHousePriceModel> priceModelList = reptileServiceManage.reptileHousePriceService
				.listInByField("houseId", ids);
		if (CollectionUtils.isEmpty(priceModelList)) {
			return houseViewList;
		}
		for (ReptileNewHouseView houseView : houseViewList) {
			String id = houseView.getId();
			// 这里是获取当前所有竞品楼盘的成交量
			String otherName = houseView.getHouseOtherName();
			if (StringUtils.isNotBlank(otherName)) {
				String houseName = otherName.trim();
				String[] hoseNames = StringUtils.split(houseName, ",");
				otherName = hoseNames[0];
			} else {
				otherName = houseView.getHouseName();
			}
			// 先匹配楼盘在房管局里面的数据
			ReptileYgjyHouseInfoSearch reptileYgjyHouseInfoSearch = new ReptileYgjyHouseInfoSearch();
			reptileYgjyHouseInfoSearch.setLikeProductName(otherName);
			List<ReptileYgjyHouseInfoView> reptileYgjyHouseInfoViewList = reptileServiceManage.reptileYgjyHouseInfoService
					.viewList(reptileYgjyHouseInfoSearch);
			Integer count = 0;
			for (ReptileYgjyHouseInfoView view : reptileYgjyHouseInfoViewList) {
				String infoId = view.getId();
				count += reptileServiceManage.reptileYgjyHouseRoomInfoService.getVolume("近一周", infoId);
			}
			houseView.setCount(count);
			List<String> priceList = new ArrayList<>();
			for (ReptileHousePriceModel priceModel : priceModelList) {
				if (StringUtils.equals(priceModel.getHouseId(), id)) {
					if (StringUtils.isEmpty(priceModel.getHousePrice())) {
						priceList.add("待定," + priceModel.getInfoSource());
					} else {
						priceList.add(priceModel.getHousePrice() + "元/平方米," + priceModel.getInfoSource());
					}
				}
				houseView.setPriceList(priceList);
			}
		}
		return houseViewList;
	}

	// 查询城市各个区域的均价还有楼盘数量
	@Override
	public List<Long> selectPart(String houseCity, String housePart) throws ProjectException {
		List<Long> longList = reptileNewHouseRepository.selectPart(houseCity, housePart);
		return longList;
	}

	@Override
	public PriceBean findNewHouseAllPrice(String id) throws ProjectException {
		ReptileNewHouseModel model = reptileServiceManage.reptileNewHouseService.findById(id);
		String houseName = "";
		if (StringUtils.isNotBlank(model.getHouseName())) {
			houseName = model.getHouseName();
		} else {
			houseName = model.getHouseOtherName();
		}
		ReptileNewHouseSearch search = new ReptileNewHouseSearch();
		search.setEqualHouseName(houseName);
		search.setGroupField("informationSources");
		search.setEqualHousePart(model.getHousePart());
		List<ReptileNewHouseModel> newHouses = reptileServiceManage.reptileNewHouseService.list(search);
		PriceBean priceBean = new PriceBean();
		Map<String, List<String>> datas = new HashMap<String, List<String>>();
		ReptileHousePriceSearch priceSearch = new ReptileHousePriceSearch();
		Date now = new Date();
		List<String> priceList = null;
		List<String> times = new ArrayList<String>();
		// 近一个月的
		now = DateUtil.addNatureMonth(now, -1);
		Date jiluDate = now;
		for (int j = 0; j < newHouses.size(); j++) {
			priceList = new ArrayList<String>();
			for (int i = 0; i < 20; i++) {
				String formatDate = DateUtil.format(now, "yyyy-MM-dd");
				String beginTime = formatDate + " 00:00:00";
				String endTime = formatDate + " 59:59:59";
				priceSearch.setGeCreateTime(DateUtil.format(beginTime));
				priceSearch.setLeCreateTime(DateUtil.format(endTime));
				priceSearch.setEqualHouseId(newHouses.get(j).getId());
				ReptileHousePriceModel price = reptileServiceManage.reptileHousePriceService.first(priceSearch);
				if (price == null || StringUtils.isBlank(price.getHousePrice())
						|| StringUtils.equals(price.getHousePrice(), "待定")) {
					priceList.add("0");
				} else {
					priceList.add(price.getHousePrice());
				}
				if (j <= 0) {
					times.add(DateUtil.format(now, "MM-dd"));
				}
				Date newNow = DateUtil.addDay(now, 1);
				now = newNow;
			}
			now = jiluDate;
			datas.put(newHouses.get(j).getInformationSources(), priceList);
		}
		priceBean.setDatas(datas);
		priceBean.setTimes(times);
		return priceBean;
	}

	@Override
	public List<ReptileNewHouseView> findByDistance(ReptileNewHouseView view, Integer distance,Integer page) {
		page = (page-1)*6;
		distance = distance==0?1000000000:distance;
		List<ReptileNewHouseModel> modelList;
		if(StringUtils.isNotBlank(view.getHouseType())){
			modelList = reptileNewHouseRepository.findDistanceByHouseType(view.getLatitude(),view.getLongitude(),view.getHouseCity(),view.getHouseType(), distance,page);
		}else{
			modelList = reptileNewHouseRepository.findDistance(view.getLatitude(),view.getLongitude(),view.getHouseCity(), distance,page);
		}
		List<ReptileNewHouseView> newList;
		try {
			newList = reptileServiceManage.reptileNewHouseService.convertByModelList(modelList);
			return newList;
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public Integer totalCountByDistance(ReptileNewHouseView view, Integer distance)
			throws ProjectException {
		Integer total = 0;
		distance = distance==0?1000000000:distance;
		if(StringUtils.isNotBlank(view.getHouseType())){
			total = reptileNewHouseRepository.findCompetitorTotalByType(view.getLatitude(), view.getLongitude(), view.getHouseCity(),view.getHouseType(),distance);
		}else{
			total = reptileNewHouseRepository.findCompetitorTotal(view.getLatitude(), view.getLongitude(), view.getHouseCity() ,distance);
		}
		return total;
	}

	@Override
	public Map<Integer, List<String>> getHouseTypes(String[] types) throws ProjectException {
		Map<Integer, List<String>> map = new HashMap<>();
		for (int i = 0; i < types.length; i++) {
			map.put(i, reptileNewHouseRepository.selectTypes(types[i]));
		}
		return map;
	}
}
