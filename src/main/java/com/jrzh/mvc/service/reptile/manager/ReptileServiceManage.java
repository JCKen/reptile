package com.jrzh.mvc.service.reptile.manager;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.jrzh.mvc.service.reptile.ReptileAreaServiceI;
import com.jrzh.mvc.service.reptile.ReptileCityServiceI;
import com.jrzh.mvc.service.reptile.ReptileCommercialHouseInfoServiceI;
import com.jrzh.mvc.service.reptile.ReptileCommodityHouseInfoServiceI;
import com.jrzh.mvc.service.reptile.ReptileCorrectionFactorServiceI;
import com.jrzh.mvc.service.reptile.ReptileCorrectionSetServiceI;
import com.jrzh.mvc.service.reptile.ReptileEsfHousePriceServiceI;
import com.jrzh.mvc.service.reptile.ReptileEsfHouseServiceI;
import com.jrzh.mvc.service.reptile.ReptileEsfHousesXiaoquServiceI;
import com.jrzh.mvc.service.reptile.ReptileEstateDatumServiceI;
import com.jrzh.mvc.service.reptile.ReptileEstateZbServiceI;
import com.jrzh.mvc.service.reptile.ReptileGtjAreaInfoServiceI;
import com.jrzh.mvc.service.reptile.ReptileHousePriceServiceI;
import com.jrzh.mvc.service.reptile.ReptileHouseProjectNameServiceI;
import com.jrzh.mvc.service.reptile.ReptileNewHouseServiceI;
import com.jrzh.mvc.service.reptile.ReptileNewhouseDealEverydayServiceI;
import com.jrzh.mvc.service.reptile.ReptileNewhouseYushouzhengServiceI;
import com.jrzh.mvc.service.reptile.ReptileReportConfigServiceI;
import com.jrzh.mvc.service.reptile.ReptileSaleInfoServiceI;
import com.jrzh.mvc.service.reptile.ReptileTemplateInfoServiceI;
import com.jrzh.mvc.service.reptile.ReptileTemplateServiceI;
import com.jrzh.mvc.service.reptile.ReptileTemplateSumInfoServiceI;
import com.jrzh.mvc.service.reptile.ReptileUserServiceI;
import com.jrzh.mvc.service.reptile.ReptileYgjyHouseFloorInfoServiceI;
import com.jrzh.mvc.service.reptile.ReptileYgjyHouseInfoServiceI;
import com.jrzh.mvc.service.reptile.ReptileYgjyHousePreSaleCertificateServiceI;
import com.jrzh.mvc.service.reptile.ReptileYgjyHouseRoomInfoServiceI;
import com.jrzh.mvc.service.reptile.ReptileYgjySaleInfoServiceI;

@Service("reptileServiceManage")
public class ReptileServiceManage {
	
	@Resource(name = "reptileEstateDatumService")
	public ReptileEstateDatumServiceI reptileEstateDatumService;
	
	@Resource(name = "reptileEstateZbService")
	public ReptileEstateZbServiceI reptileEstateZbService;
	
	@Resource(name = "reptileNewHouseService")
	public ReptileNewHouseServiceI reptileNewHouseService;

	@Resource(name = "reptileYgjySaleInfoService")
	public ReptileYgjySaleInfoServiceI reptileYgjySaleInfoService;

	@Resource(name = "reptileAreaService")
	public ReptileAreaServiceI reptileAreaService;

	@Resource(name = "reptileCityService")
	public ReptileCityServiceI reptileCityService;

	@Resource(name = "reptileYgjyHouseInfoService")
	public ReptileYgjyHouseInfoServiceI reptileYgjyHouseInfoService;
	
	@Resource(name = "reptileYgjyHouseFloorInfoService")
	public ReptileYgjyHouseFloorInfoServiceI reptileYgjyHouseFloorInfoService;
	
	@Resource(name = "reptileYgjyHouseRoomInfoService")
	public ReptileYgjyHouseRoomInfoServiceI reptileYgjyHouseRoomInfoService;
	
	@Resource(name = "reptileHousePriceService")
	public ReptileHousePriceServiceI reptileHousePriceService;
	
	@Resource(name = "reptileEsfHouseService")
	public ReptileEsfHouseServiceI reptileEsfHouseService;
	
	@Resource(name = "reptileEsfHousePriceService")
	public ReptileEsfHousePriceServiceI reptileEsfHousePriceService;
	
	@Resource(name = "reptileYgjyHousePreSaleCertificateService")
	public ReptileYgjyHousePreSaleCertificateServiceI reptileYgjyHousePreSaleCertificateService;
	
	@Resource(name = "reptileHouseProjectNameService")
	public ReptileHouseProjectNameServiceI reptileHouseProjectNameService;
	
	@Resource(name = "reptileGtjAreaInfoService")
	public ReptileGtjAreaInfoServiceI reptileGtjAreaInfoService;
	
	@Resource(name = "reptileTemplateInfoService")
	public ReptileTemplateInfoServiceI reptileTemplateInfoService;
	
	@Resource(name = "reptileUserService")
	public ReptileUserServiceI reptileUserService;
	
	@Resource(name = "reptileTemplateService")
	public ReptileTemplateServiceI reptileTemplateService;
	
	@Resource(name = "reptileSaleInfoService")
	public ReptileSaleInfoServiceI reptileSaleInfoService;
	
	@Resource(name = "reptileCommodityHouseInfoService")
	public ReptileCommodityHouseInfoServiceI reptileCommodityHouseInfoService;
	
	@Resource(name = "reptileCommercialHouseInfoService")
	public ReptileCommercialHouseInfoServiceI reptileCommercialHouseInfoService;
	
	@Resource(name = "reptileTemplateSumInfoService")
	public ReptileTemplateSumInfoServiceI reptileTemplateSumInfoService;
	
	@Resource(name = "reptileReportConfigService")
	public ReptileReportConfigServiceI reptileReportConfigService;
	
	@Resource(name = "reptileCorrectionFactorService")
	public ReptileCorrectionFactorServiceI reptileCorrectionFactorService;
	
	@Resource(name = "reptileCorrectionSetService")
	public ReptileCorrectionSetServiceI reptileCorrectionSetService;
	
	@Resource(name = "reptileEsfHousesXiaoquService")
	public ReptileEsfHousesXiaoquServiceI reptileEsfHousesXiaoquService;
	
	@Resource(name = "reptileNewhouseYushouzhengService")
	public ReptileNewhouseYushouzhengServiceI reptileNewhouseYushouzhengService;
	
	@Resource(name = "reptileNewhouseDealEverydayService")
	public ReptileNewhouseDealEverydayServiceI reptileNewhouseDealEverydayService;
	
}
