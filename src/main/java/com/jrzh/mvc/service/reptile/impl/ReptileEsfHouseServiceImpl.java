package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.tools.CollectionTools;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.factory.webSpider.EsfHouseWorldSpider;
import com.jrzh.factory.webSpider.EsfLianJiaHouseSpider;
import com.jrzh.factory.webSpider.EsfXiaoqu_Fangtianxia;
import com.jrzh.factory.webSpider.EsfXiaoqu_Lianjia;
import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileEsfHouseConvert;
import com.jrzh.mvc.model.reptile.ReptileEsfHouseModel;
import com.jrzh.mvc.model.reptile.ReptileEsfHousePriceModel;
import com.jrzh.mvc.repository.reptile.ReptileEsfHouseRepository;
import com.jrzh.mvc.search.reptile.ReptileEsfHouseSearch;
import com.jrzh.mvc.service.reptile.ReptileEsfHouseServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileEsfHouseView;

@Service("reptileEsfHouseService")
public class ReptileEsfHouseServiceImpl extends
		BaseServiceImpl<ReptileEsfHouseModel, ReptileEsfHouseSearch, ReptileEsfHouseView> implements
		ReptileEsfHouseServiceI {

	@Autowired
	private ReptileEsfHouseRepository reptileEsfHouseRepository;
	
	@Autowired
	private ReptileServiceManage reptileServiceManage;
	
	@PersistenceContext
	EntityManager em ;

	@Override
	public BaseRepository<ReptileEsfHouseModel,Serializable> getDao() {
		return reptileEsfHouseRepository;
	}

	@Override
	public BaseConvertI<ReptileEsfHouseModel, ReptileEsfHouseView> getConvert() {
		return new ReptileEsfHouseConvert();
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException{
		ResultBean result = new ResultBean();
		ReptileEsfHouseModel reptileEsfHouseModel = this.findById(id);
		reptileEsfHouseModel.setIsDisable(!reptileEsfHouseModel.getIsDisable());
		edit(reptileEsfHouseModel, sessionUser);
		result.setStatus(ResultBean.SUCCESS);
		result.setMsg(I18nHelper.getI18nByKey("message.adjustment_success", sessionUser));
		return result;
	}
	
	//保存链家二手房信息
	@Override
	public void findEsfHouseFromLianjiaInGZ(SessionUser user) throws ProjectException {
		new EsfXiaoqu_Lianjia().saveEsfHouseInfoFromInGZ(reptileServiceManage, user);
	}

	//保存房天下二手房信息
	@Override
	public void findEsfHouseFromHouseWorldInGZ(SessionUser user) throws ProjectException {
		new EsfXiaoqu_Fangtianxia().saveHouseInfoFromHouseWorldInGZ(reptileServiceManage, user);
	}

	@Override
	public List<ReptileEsfHouseView> findAllHouseInfo(ReptileEsfHouseView view,Integer page, SessionUser user)
			throws ProjectException {
		ReptileEsfHouseSearch search = new ReptileEsfHouseSearch();
		String city = view.getHouseCity();
		search.setEqualHouseCity(city);
		if(StringUtils.isNotBlank(view.getHousePart())){
			search.setEqualHousePart(view.getHousePart());
		}
//		search.setLikeHouseName(view.getHouseName());
		search.setLikeHouseOtherName(view.getHouseOtherName());
		if(StringUtils.isNotBlank(view.getRenovationCondition())){
			search.setLikeRenovationCondition(view.getRenovationCondition());
		}
		if(null != page) {
			search.setPage(page);
			search.setRows(6);
		}
		if(StringUtils.equals(view.getIsToday(), "true")){
			String strData = DateUtil.format(new Date(),"yyyy-MM-dd");
			search.setEqualInfoUpdateTime(strData+" 00:00:00");
		}
		List<ReptileEsfHouseView> houseViewList = reptileServiceManage.reptileEsfHouseService.viewList(search);
		if(CollectionUtils.isEmpty(houseViewList)) {
			return null;
		}
		String[] ids = CollectionTools.collect(houseViewList, "id", new String[] {});
		List<ReptileEsfHousePriceModel> priceModelList = reptileServiceManage.reptileEsfHousePriceService.listInByField("houseId", ids);
		if(CollectionUtils.isEmpty(priceModelList)) {
			return houseViewList;
		}
		
		for(ReptileEsfHouseView houseView : houseViewList) {
			String id = houseView.getId();
			List<String> priceList = new ArrayList<>();
			for(ReptileEsfHousePriceModel priceModel : priceModelList) {
				if(StringUtils.equals(priceModel.getHouseId(), id)) {
					if(StringUtils.isEmpty(priceModel.getHousePrice())) {
						priceList.add( "待定," + priceModel.getInfoSource());
					}else {
						priceList.add(priceModel.getHousePrice() );
					}
				}
				houseView.setPriceList(priceList);
			}
		}
		
		return houseViewList;
	}

	@Override
	public List<ReptileEsfHouseView> findSurrounding(BigDecimal longitude,BigDecimal latitude,String math) throws ProjectException {
		try {
			if(null == longitude || null == latitude) return null;
			List<ReptileEsfHouseModel> esfList = reptileEsfHouseRepository.findSurrounding(longitude, latitude,math);
			if(CollectionUtils.isEmpty(esfList)) return null;
			String[] ids = CollectionTools.collect(esfList, "id", new String[] {});
			List<ReptileEsfHousePriceModel> priceModelList = reptileServiceManage.reptileEsfHousePriceService.listInByField("houseId", ids);
			List<ReptileEsfHouseView> viewList = new ArrayList<>();
			for(ReptileEsfHouseModel houseModel : esfList) {
				ReptileEsfHouseView houseView = new ReptileEsfHouseConvert().convertToView(houseModel);
				String id = houseModel.getId();
				List<String> priceList = new ArrayList<>();
				if(CollectionUtils.isEmpty(priceModelList)) {
					continue;
				}
				for(ReptileEsfHousePriceModel priceModel : priceModelList) {
					if(StringUtils.equals(priceModel.getHouseId(), id)) {
						if(StringUtils.isEmpty(priceModel.getHousePrice())) {
							priceList.add( "待定," + priceModel.getInfoSource());
						}else {
							priceList.add(priceModel.getHousePrice());
						}
					}
					houseView.setPriceList(priceList);
				}
				viewList.add(houseView);
			}
			return viewList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	@Override
	public List<ReptileEsfHouseView> findByIds(String time ,String ids[]) throws ProjectException {
		List<ReptileEsfHouseView> houseViewList = reptileServiceManage.reptileEsfHouseService.listViewInField("id", ids);
		if(CollectionUtils.isEmpty(houseViewList)) {
			return null;
		}
		List<ReptileEsfHousePriceModel> priceModelList = reptileServiceManage.reptileEsfHousePriceService.listInByField("houseId", ids);
		if(CollectionUtils.isEmpty(priceModelList)) {
			return houseViewList;
		}
		for(ReptileEsfHouseView houseView : houseViewList) {
			String id = houseView.getId();
			String oherName = houseView.getHouseOtherName();
			if(StringUtils.isEmpty(time)) {
				time = "近一周";
			}
			Integer count = reptileServiceManage.reptileEsfHouseService.getVolume("近一周",oherName);
			houseView.setCount(count);
			List<String> priceList = new ArrayList<>();
			for(ReptileEsfHousePriceModel priceModel : priceModelList) {
				if(StringUtils.equals(priceModel.getHouseId(), id)) {
					if(StringUtils.isEmpty(priceModel.getHousePrice())) {
						priceList.add( "待定," + priceModel.getInfoSource());
					}else {
						priceList.add(priceModel.getHousePrice());
					}
				}
				houseView.setPriceList(priceList);
			}
		}
		return houseViewList;
	}
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
	public Integer getVolume(String time,String houseOtherName) throws ProjectException {
		Integer count = null;
		if(StringUtils.equals(time, "近一周")) {
			Integer thisWeekCount  = reptileEsfHouseRepository.findThisWeekCount(houseOtherName);
			Integer lastWeekCount = reptileEsfHouseRepository.findLastWeekCount(houseOtherName);
			count = thisWeekCount - lastWeekCount;
		}
		if(StringUtils.equals(time, "近一个月")) {
			Integer thisMonthCount  = reptileEsfHouseRepository.findThisMonth(houseOtherName);
			Integer lastMonthCount = reptileEsfHouseRepository.findLastMonth(houseOtherName);
			count = thisMonthCount - lastMonthCount;	
		}
		if(StringUtils.equals(time, "近三个月")) {
			Integer thisMonthCount  = reptileEsfHouseRepository.findThisMonth(houseOtherName);
			Integer threeMonthCount = reptileEsfHouseRepository.findThreeMonthCount(houseOtherName);
			count = thisMonthCount - threeMonthCount;
		}
		if(StringUtils.equals(time, "近六个月")) {
			Integer thisMonthCount  = reptileEsfHouseRepository.findThisMonth(houseOtherName);
			Integer sixMonthCount = reptileEsfHouseRepository.findSixMonthCount(houseOtherName);
			count = thisMonthCount - sixMonthCount;
		}
		if(StringUtils.equals(time, "近一年")) {
			Integer thisYearCount  = reptileEsfHouseRepository.findThisYearCount(houseOtherName);
			Integer lastYearCount = reptileEsfHouseRepository.findLastYearCount(houseOtherName);
			count = thisYearCount - lastYearCount;
		}
		return count;
	}
	
	@Override
	public Integer totalCount(ReptileEsfHouseView view ) throws ProjectException {
		try {
			StringBuilder whereSql = new StringBuilder("select count(*) from reptile_esf_house where 1 = 1 ");
			if(StringUtils.isNotBlank(view.getHouseCity())) {
				whereSql.append(" AND _house_city = '"+view.getHouseCity() +"'");
			}
			if(StringUtils.isNotBlank(view.getHouseOtherName())) {
				whereSql.append(" AND _house_other_name like '%" + view.getHouseOtherName() + "%' ");
			}
			if(StringUtils.isNotBlank(view.getHousePart())) {
				whereSql.append(" AND _house_part = '" + view.getHousePart() +"'");
			}
			if(StringUtils.isNotBlank(view.getRenovationCondition())){
				whereSql.append(" AND _renovation_condition like '%" + view.getRenovationCondition() +"%'");
			}
			System.out.println("查询二手房的总数据："+whereSql);
			
			Query query = em.createNativeQuery(whereSql.toString());
			List<BigInteger[]> count = query.getResultList();
			Object[] c =  count.toArray();
			if(CollectionUtils.isEmpty(count)) return 0;
			return Integer.valueOf(c[0].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//查询城市各个区域的均价还有楼盘数量
	@Override
	public List<Long> selectPart(String houseCity, String housePart) throws ProjectException {
		List<Long> longList = reptileEsfHouseRepository.selectPart(houseCity, housePart);
		return longList;
	}

	@Override
	public Integer selectJpTotal(ReptileEsfHouseView view, Integer distance) throws ProjectException {
		System.out.println("查询二手房竞品数量sql语句：select count(*) from( select ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN( ( "+view.getLatitude()+" * PI() / 180 - _latitude * PI() / 180 ) / 2 ) , 2 )+ COS( "+view.getLatitude()+" * PI( ) / 180 ) * COS( _latitude * PI( ) / 180 )* POW( SIN( ( "+view.getLongitude()+" * PI() / 180 - _longitude * PI() / 180 ) / 2 ) , 2 ))) * 1000) AS distance  from reptile_esf_house HAVING distance BETWEEN 1 AND "+distance+") as a");
		Integer count = 0;
		if(StringUtils.isNotBlank(view.getRenovationCondition())){
		 	count = reptileEsfHouseRepository.selectJpTotalByRenovation(view.getLatitude(),view.getLongitude(),view.getRenovationCondition(),distance);
		}else{
			count = reptileEsfHouseRepository.selectJpTotal(view.getLatitude(),view.getLongitude(),distance);
		}
		return count;
	}

	@Override
	public List<ReptileEsfHouseView> selectJp(ReptileEsfHouseView view, Integer distance,Integer page) throws ProjectException {
		page = (page-1) * 6;
		List<ReptileEsfHouseModel> models;
		if(StringUtils.isNotBlank(view.getRenovationCondition())){
			models = reptileEsfHouseRepository.selectJpByRenovation(view.getLatitude(), view.getLongitude(), view.getRenovationCondition(), distance,page);
		}else{
			models = reptileEsfHouseRepository.selectJp(view.getLatitude(),view.getLongitude(),distance,page);
		}
		List<ReptileEsfHouseView> list = new ArrayList<>();
		list = reptileServiceManage.reptileEsfHouseService.convertByModelList(models);
		return list;
	}
	
}
