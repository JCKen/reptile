package com.jrzh.mvc.service.reptile;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileSaleInfoModel;
import com.jrzh.mvc.search.reptile.ReptileSaleInfoSearch;
import com.jrzh.mvc.view.reptile.ReptileSaleInfoView;

public interface ReptileSaleInfoServiceI  extends BaseServiceI<ReptileSaleInfoModel, ReptileSaleInfoSearch, ReptileSaleInfoView>{
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException;
	
	public void BjSaleInfo(SessionUser user) throws ProjectException;
	
	public Object[] selectSaleInfo(ReptileSaleInfoView view,SessionUser user) throws ProjectException;
}