package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.tools.CollectionTools;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileEsfHousesXiaoquConvert;
import com.jrzh.mvc.model.reptile.ReptileEsfHousePriceModel;
import com.jrzh.mvc.model.reptile.ReptileEsfHousesXiaoquModel;
import com.jrzh.mvc.repository.reptile.ReptileEsfHousesXiaoquRepository;
import com.jrzh.mvc.search.reptile.ReptileEsfHousesXiaoquSearch;
import com.jrzh.mvc.service.reptile.ReptileEsfHousesXiaoquServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileEsfHousesXiaoquView;

@Service("reptileEsfHousesXiaoquService")
public class ReptileEsfHousesXiaoquServiceImpl
		extends BaseServiceImpl<ReptileEsfHousesXiaoquModel, ReptileEsfHousesXiaoquSearch, ReptileEsfHousesXiaoquView>
		implements ReptileEsfHousesXiaoquServiceI {

	@Autowired
	private ReptileEsfHousesXiaoquRepository reptileEsfHousesXiaoquRepository;

	@Override
	public BaseRepository<ReptileEsfHousesXiaoquModel, Serializable> getDao() {
		return reptileEsfHousesXiaoquRepository;
	}

	@PersistenceContext
	EntityManager em;

	@Autowired
	private ReptileServiceManage reptileServiceManage;

	@Override
	public BaseConvertI<ReptileEsfHousesXiaoquModel, ReptileEsfHousesXiaoquView> getConvert() {
		return new ReptileEsfHousesXiaoquConvert();
	}

	@Override
	public List<ReptileEsfHousesXiaoquView> findAllHouseInfo(ReptileEsfHousesXiaoquView view, Integer page,
			SessionUser user) throws ProjectException {
		ReptileEsfHousesXiaoquSearch search = new ReptileEsfHousesXiaoquSearch();

		if(StringUtils.isNotBlank(view.getHousesCity())){
			search.setEqualHousesCity(view.getHousesCity());
		}
		if(StringUtils.isNotBlank(view.getHouseName())){
			search.setlikeHouseName(view.getHouseName());
			search.setLikeHouseName(view.getHouseName());
		}
		if (StringUtils.isNotBlank(view.getHousesPart())) {
			search.setEqualHousesPart(view.getHousesPart());
		}
		if (StringUtils.isNotBlank(view.getHousesType())) {
			search.setLikeHousesType(view.getHousesType());
		}
		if(StringUtils.isNotBlank(view.getHousesBuildingYear())){
			search.setGtHouseBuildingYear(view.getHousesBuildingYear());
		}
		if (null != page) {
			search.setPage(page);
			search.setRows(6);
		}
		if (StringUtils.equals(view.getIsToday(), "true")) {
			String strData = DateUtil.format(new Date(), "yyyy-MM-dd");
			search.setEqualInfoUpdateTime(strData + " 00:00:00");
		}
		List<ReptileEsfHousesXiaoquView> houseViewList = reptileServiceManage.reptileEsfHousesXiaoquService
				.viewList(search);
		if (CollectionUtils.isEmpty(houseViewList)) {
			return null;
		}
		String[] ids = CollectionTools.collect(houseViewList, "id", new String[] {});
		List<ReptileEsfHousePriceModel> priceModelList = reptileServiceManage.reptileEsfHousePriceService
				.listInByField("houseId", ids);
		if (CollectionUtils.isEmpty(priceModelList)) {
			return houseViewList;
		}

		for (ReptileEsfHousesXiaoquView houseView : houseViewList) {
			String id = houseView.getId();
			List<String> priceList = new ArrayList<>();
			for (ReptileEsfHousePriceModel priceModel : priceModelList) {
				if (StringUtils.equals(priceModel.getId(), id)) {
					if (StringUtils.isEmpty(priceModel.getHousePrice())) {
						priceList.add("待定," + priceModel.getInfoSource());
					} else {
						priceList.add(priceModel.getHousePrice());
					}
				}
				houseView.setPriceList(priceList);
			}
		}
		return houseViewList;
	}

	@Override
	public Integer totalCount(ReptileEsfHousesXiaoquView view) throws ProjectException {
		try {
			StringBuilder whereSql = new StringBuilder("select count(*) from reptile_esf_houses_xiaoqu where 1 = 1 ");
			if (StringUtils.isNotBlank(view.getHousesCity())) {
				whereSql.append(" AND _houses_city = '" + view.getHousesCity() + "'");
			}
			if (StringUtils.isNotBlank(view.getHouseName())) {
				whereSql.append(" AND _house_name like '%" + view.getHouseName() + "%' ");
			}
			if (StringUtils.isNotBlank(view.getHousesPart())) {
				whereSql.append(" AND _houses_part = '" + view.getHousesPart() + "'");
			}
			if (StringUtils.isNotBlank(view.getHousesType())) {
				whereSql.append(" AND _houses_type like '%" + view.getHousesType() + "%'");
			}
			if (StringUtils.isNotBlank(view.getHousesBuildingYear())) {
				whereSql.append(" AND _houses_building_year >= '" + view.getHousesBuildingYear() + "'");
			}
			Query query = em.createNativeQuery(whereSql.toString());
			List<BigInteger[]> count = query.getResultList();
			Object[] c = count.toArray();
			if (CollectionUtils.isEmpty(count))
				return 0;
			return Integer.valueOf(c[0].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// 查询城市各个区域的均价还有楼盘数量
	@Override
	public List<Long> selectPart(String houseCity, String housePart) throws ProjectException {
		List<Long> longList = reptileEsfHousesXiaoquRepository.selectPart(houseCity, housePart);
		return longList;
	}

	@Override
	public Integer selectJpTotal(ReptileEsfHousesXiaoquView view, Integer distance) throws ProjectException {
		Integer count = 0;
		distance = distance==0?1000000000:distance;
		if (StringUtils.isNotBlank(view.getHousesType())) {
			count = reptileEsfHousesXiaoquRepository.selectJpTotalByType(view.getLatitude(), view.getLongitude(),
					view.getHousesType(), view.getHousesBuildingYear(),distance);
		} else {
			count = reptileEsfHousesXiaoquRepository.selectJpTotal(view.getLatitude(), view.getLongitude(), view.getHousesBuildingYear() ,distance);
		}
		return count;
	}
	
	
	@Override
	public List<ReptileEsfHousesXiaoquView> selectJp(ReptileEsfHousesXiaoquView view, Integer distance,Integer page) throws ProjectException {
		page = (page-1) * 6;
		distance = distance==0?1000000000:distance;
		List<ReptileEsfHousesXiaoquModel> models;
		if(StringUtils.isNotBlank(view.getHousesType())){
			models = reptileEsfHousesXiaoquRepository.selectJpByType(view.getLatitude(), view.getLongitude(), view.getHousesType(), view.getHousesBuildingYear() ,distance,page);
		}else{
			models = reptileEsfHousesXiaoquRepository.selectJp(view.getLatitude(),view.getLongitude(), view.getHousesBuildingYear() ,distance,page);
		}
		List<ReptileEsfHousesXiaoquView> list = new ArrayList<>();
		list = reptileServiceManage.reptileEsfHousesXiaoquService.convertByModelList(models);
		return list;
	}

	@Override
	public List<ReptileEsfHousesXiaoquView> findByIds(String time, String[] ids) throws ProjectException {
		List<ReptileEsfHousesXiaoquView> houseViewList = reptileServiceManage.reptileEsfHousesXiaoquService.listViewInField("id", ids);
		if(CollectionUtils.isEmpty(houseViewList)) {
			return null;
		}
		List<ReptileEsfHousePriceModel> priceModelList = reptileServiceManage.reptileEsfHousePriceService.listInByField("houseId", ids);
		if(CollectionUtils.isEmpty(priceModelList)) {
			return houseViewList;
		}
		for(ReptileEsfHousesXiaoquView houseView : houseViewList) {
			String id = houseView.getId();
			String houseName = houseView.getHouseName();
			if(StringUtils.isEmpty(time)) {
				time = "近一周";
			}
			Integer count = reptileServiceManage.reptileEsfHouseService.getVolume("近一周",houseName);
			houseView.setCount(count);
			List<String> priceList = new ArrayList<>();
			for(ReptileEsfHousePriceModel priceModel : priceModelList) {
				if(StringUtils.equals(priceModel.getHouseId(), id)) {
					if(StringUtils.isEmpty(priceModel.getHousePrice())) {
						priceList.add( "待定," + priceModel.getInfoSource());
					}else {
						priceList.add(priceModel.getHousePrice());
					}
				}
				houseView.setPriceList(priceList);
			}
		}
		return houseViewList;
	}

	@Override
	public Integer getVolume(String time, String houseOtherName) throws ProjectException {
		Integer count = null;
		if(StringUtils.equals(time, "近一周")) {
			Integer thisWeekCount  = reptileEsfHousesXiaoquRepository.findThisWeekCount(houseOtherName);
			Integer lastWeekCount = reptileEsfHousesXiaoquRepository.findLastWeekCount(houseOtherName);
			count = thisWeekCount - lastWeekCount;
		}
		if(StringUtils.equals(time, "近一个月")) {
			Integer thisMonthCount  = reptileEsfHousesXiaoquRepository.findThisMonth(houseOtherName);
			Integer lastMonthCount = reptileEsfHousesXiaoquRepository.findLastMonth(houseOtherName);
			count = thisMonthCount - lastMonthCount;	
		}
		if(StringUtils.equals(time, "近三个月")) {
			Integer thisMonthCount  = reptileEsfHousesXiaoquRepository.findThisMonth(houseOtherName);
			Integer threeMonthCount = reptileEsfHousesXiaoquRepository.findThreeMonthCount(houseOtherName);
			count = thisMonthCount - threeMonthCount;
		}
		if(StringUtils.equals(time, "近六个月")) {
			Integer thisMonthCount  = reptileEsfHousesXiaoquRepository.findThisMonth(houseOtherName);
			Integer sixMonthCount = reptileEsfHousesXiaoquRepository.findSixMonthCount(houseOtherName);
			count = thisMonthCount - sixMonthCount;
		}
		if(StringUtils.equals(time, "近一年")) {
			Integer thisYearCount  = reptileEsfHousesXiaoquRepository.findThisYearCount(houseOtherName);
			Integer lastYearCount = reptileEsfHousesXiaoquRepository.findLastYearCount(houseOtherName);
			count = thisYearCount - lastYearCount;
		}
		return count;
	}

}
