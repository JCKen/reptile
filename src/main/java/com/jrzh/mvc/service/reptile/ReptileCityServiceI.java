package com.jrzh.mvc.service.reptile;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.base.service.BaseServiceI;
import com.jrzh.mvc.model.reptile.ReptileCityModel;
import com.jrzh.mvc.search.reptile.ReptileCitySearch;
import com.jrzh.mvc.view.reptile.ReptileAreaView;
import com.jrzh.mvc.view.reptile.ReptileCityView;

public interface ReptileCityServiceI  extends BaseServiceI<ReptileCityModel, ReptileCitySearch, ReptileCityView>{

	public void saveInit(String project, InputStream in) throws ProjectException;
	
	public Map<String,List<ReptileAreaView>> findCityArea() throws ProjectException;;
}