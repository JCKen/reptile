package com.jrzh.mvc.service.reptile.impl;

import java.io.Serializable;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileHousePriceConvert;
import com.jrzh.mvc.model.reptile.ReptileHousePriceModel;
import com.jrzh.mvc.repository.reptile.ReptileHousePriceRepository;
import com.jrzh.mvc.search.reptile.ReptileHousePriceSearch;
import com.jrzh.mvc.service.reptile.ReptileHousePriceServiceI;
import com.jrzh.mvc.view.reptile.ReptileHousePriceView;

@Service("reptileHousePriceService")
public class ReptileHousePriceServiceImpl extends
		BaseServiceImpl<ReptileHousePriceModel, ReptileHousePriceSearch, ReptileHousePriceView> implements
		ReptileHousePriceServiceI {

	@Autowired
	private ReptileHousePriceRepository reptileHousePriceRepository;

	@Override
	public BaseRepository<ReptileHousePriceModel,Serializable> getDao() {
		return reptileHousePriceRepository;
	}

	@Override
	public BaseConvertI<ReptileHousePriceModel, ReptileHousePriceView> getConvert() {
		return new ReptileHousePriceConvert();
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public ResultBean changeStatus(String id, SessionUser sessionUser)
			throws ProjectException{
		ResultBean result = new ResultBean();

		ReptileHousePriceModel reptileHousePriceModel = this.findById(id);
		reptileHousePriceModel.setIsDisable(!reptileHousePriceModel.getIsDisable());
		edit(reptileHousePriceModel, sessionUser);

		result.setStatus(ResultBean.SUCCESS);
		result.setMsg(I18nHelper.getI18nByKey("message.adjustment_success", sessionUser));
		return result;
	}

}
