package com.jrzh.mvc.service.reptile.impl;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.jrzh.bean.ImageBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.Arith;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.config.ProjectConfigration;
import com.jrzh.framework.I18nHelper;
import com.jrzh.framework.base.convert.BaseConvertI;
import com.jrzh.framework.base.repository.BaseRepository;
import com.jrzh.framework.base.service.impl.BaseServiceImpl;
import com.jrzh.framework.bean.ResultBean;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.controller.reptile.front.ReportContentRequest;
import com.jrzh.mvc.controller.reptile.front.ReportTableView;
import com.jrzh.mvc.convert.reptile.ReptileCommercialHouseInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileCommercialHouseInfoModel;
import com.jrzh.mvc.repository.reptile.ReptileCommercialHouseInfoRepository;
import com.jrzh.mvc.search.reptile.ReptileCommercialHouseInfoSearch;
import com.jrzh.mvc.service.reptile.ReptileCommercialHouseInfoServiceI;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.mvc.view.reptile.ReptileCommercialHouseInfoView;
import com.jrzh.mvc.view.reptile.ReptileCommodityHouseInfoView;
import com.jrzh.util.FreeMarkerUtil;
import com.jrzh.util.ImgBase64Utils;
import com.jrzh.util.ObjectConvert;

//商品房
@Service("reptileCommercialHouseInfoService")
public class ReptileCommercialHouseInfoServiceImpl extends
		BaseServiceImpl<ReptileCommercialHouseInfoModel, ReptileCommercialHouseInfoSearch, ReptileCommercialHouseInfoView>
		implements ReptileCommercialHouseInfoServiceI {

	@Autowired
	private ReptileCommercialHouseInfoRepository reptileCommercialHouseInfoRepository;
	@Autowired
	private ReptileServiceManage reptileServiceManage;

	@Autowired
	private ProjectConfigration projectConfigration;

	@PersistenceContext
	EntityManager em;

	@Override
	public BaseRepository<ReptileCommercialHouseInfoModel, Serializable> getDao() {
		return reptileCommercialHouseInfoRepository;
	}

	@Override
	public BaseConvertI<ReptileCommercialHouseInfoModel, ReptileCommercialHouseInfoView> getConvert() {
		return new ReptileCommercialHouseInfoConvert();
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	public ResultBean changeStatus(String id, SessionUser sessionUser) throws ProjectException {
		ResultBean result = new ResultBean();

		ReptileCommercialHouseInfoModel reptileCommercialHouseInfoModel = this.findById(id);
		reptileCommercialHouseInfoModel.setIsDisable(!reptileCommercialHouseInfoModel.getIsDisable());
		edit(reptileCommercialHouseInfoModel, sessionUser);

		result.setStatus(ResultBean.SUCCESS);
		result.setMsg(I18nHelper.getI18nByKey("message.adjustment_success", sessionUser));
		return result;
	}

	@Override
	public List<ReptileCommercialHouseInfoView> selectGqbInfo(ReptileCommercialHouseInfoView view, SessionUser user)
			throws ProjectException {
		try {
			// 本月信息
			if (StringUtils.isBlank(view.getCity())) {
				throw new Exception("城市不能为空");
			}
			StringBuilder whereSql = new StringBuilder(
					"SELECT _use as use,_city as city,_part as part,SUM(CAST(_transaction_number AS SIGNED)) as transactionAllNumber,SUM(CAST(_transaction_area AS SIGNED)) as transactionAllArea,SUM(CAST(_transaction_price AS SIGNED)) as transactionAllPrice,SUM(CAST(_saleable_area AS SIGNED)) as saleableAllArea,SUM(CAST(_available_sets AS SIGNED)) as availableAllSets,,DATE_FORMAT(_info_updata_time, '%Y%m') AS months FROM reptile_commercial_house_info WHERE 1 = 1 AND ");
			if (StringUtils.isNotBlank(view.getCity())) {
				whereSql.append(" AND _city = '" + view.getCity() + "' GROUP BY months ");
			}
			if (StringUtils.equals(view.getIsCity(), "0")) {
				whereSql.append(" ,_city ");
			}
			if (StringUtils.equals(view.getIsPart(), "0")) {
				whereSql.append(" ,_part ");
			}
			if (StringUtils.equals(view.getIsUse(), "0")) {
				whereSql.append(" ,_house_type ");
			}
			log.info("### ƴװsql ### begin");
			Query query = em.createNativeQuery(whereSql.toString());
			log.info("### ƴװsql ### end");
			List<Object[]> objectList = query.getResultList();
			System.out.println(objectList.get(0).length);
			String[] fields = { "use", "city", "part", "transactionAllNumber", "transactionAllArea",
					"transactionAllPrice", "saleableAllArea", "availableAllSets", "months" };
			List<ReptileCommercialHouseInfoView> reptileCommercialHouseInfoViewList = new ArrayList<>();
			reptileCommercialHouseInfoViewList = ObjectConvert.dataToList(ReptileCommercialHouseInfoView.class,
					objectList, fields);
			return reptileCommercialHouseInfoViewList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Map<String, Object> getReportData( String array[], SessionUser sessionUser)
			throws ProjectException {
		Map<String, Object> result = new HashMap<String, Object>();
		// 文件处理
		ReportContentRequest report =null;
		for (String reportObj : array) {
			report = JSON.parseObject(reportObj, ReportContentRequest.class);
			Integer serial = report.getSerial();
			switch (serial) {
			case 1:
				result.put("report1", report);
				break;
			case 2:
				result.put("report2", report);
				break;
			case 3:
				result.put("report3", report);
				break;
			case 4:
				result.put("report4", report);
				break;
			case 5:
				result.put("report5", report);
				break;
			case 6:
				result.put("report6", report);
				break;
			case 7:
				result.put("report7", report);
				break;
			case 8:
				result.put("report8", report);
				break;
			case 9:
				result.put("report9", report);
				break;
			case 10:
				result.put("report10", report);
				break;
			case 11:
				result.put("report11", report);
				break;
			case 12:
				result.put("report12", report);
				break;
			}
		}
		return result;
	}
}
