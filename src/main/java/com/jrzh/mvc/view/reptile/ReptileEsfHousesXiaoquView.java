package com.jrzh.mvc.view.reptile;

import java.math.BigDecimal;
import java.util.List;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileEsfHousesXiaoquView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 小区名
     */
    private String houseName;
    /**
     * 小区别名
     */
    private String housesOtherName;
    /**
     * 小区所属城市
     */
    private String housesCity;
    /**
     * 小区所属区域
     */
    private String housesPart;
    /**
     * 所在地址
     */
    private String housesAddress;
    /**
     * 小区建筑年代
     */
    private String housesBuildingYear;
    /**
     * 房屋类型
     */
    private String housesType;
    /**
     * 房屋均价
     */
    private String housesPrice;
    /**
     * 产权年限
     */
    private String yearOfPropertyRights;
    /**
     * 信息来源
     */
    private String informationSources;
    /**
     * 信息更新时间
     */
    private String infoUpdateTime;
    /**
     * 纬度
     */
    private BigDecimal latitude;
    /**
     * 经度
     */
    private BigDecimal longitude;
    /**
     * 开发商
     */
    private String developers;
    /**
     * 绿化率
     */
    private String afforestationRate;
    /**
     * 容积率
     */
    private String plotRatio;
    /**
     * 建筑类型
     */
    private String buildingType;
    /**
     * 物业公司
     */
    private String propertyCompany;
    /**
     * 房屋总数
     */
    private String totalHouseholds;
    /**
     * 物业费
     */
    private String propertyCosts;
    /**
     * 附近交通
     */
    private String traffic;
    /**
     * 幼儿园
     */
    private String kindergarten;
    /**
     * 中小学
     */
    private String school;
    /**
     * 综合商场
     */
    private String powerCenter;
    /**
     * 医院
     */
    private String hospital;
    /**
     * 银行
     */
    private String bank;
    /**
     * 邮政
     */
    private String post;
    /**
     * 其他
     */
    private String other;
    /**
     * 占地面积
     */
    private String floorArea;
    /**
     * 建筑面积
     */
    private String buildingArea;
    /**
     * 数据访问地址
     */
    private String url;
    
    private List<String> priceList;
    
    private String isToday;
    
    //成交总量
    private Integer count;
    
    //参考价月份
    private String rpMonth;
    
    public String getRpMonth() {
		return rpMonth;
	}

	public void setRpMonth(String rpMonth) {
		this.rpMonth = rpMonth;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void setHouseName(String houseName) {
        this.houseName = houseName;
    }
    
    public String getHouseName() {
        return this.houseName;
    }
    public void setHousesOtherName(String housesOtherName) {
        this.housesOtherName = housesOtherName;
    }
    
    public String getHousesOtherName() {
        return this.housesOtherName;
    }
    public void setHousesCity(String housesCity) {
        this.housesCity = housesCity;
    }
    
    public String getIsToday() {
		return isToday;
	}

	public List<String> getPriceList() {
		return priceList;
	}

	public void setPriceList(List<String> priceList) {
		this.priceList = priceList;
	}

	public void setIsToday(String isToday) {
		this.isToday = isToday;
	}

	public String getHousesCity() {
        return this.housesCity;
    }
    public void setHousesPart(String housesPart) {
        this.housesPart = housesPart;
    }
    
    public String getHousesPart() {
        return this.housesPart;
    }
    public void setHousesAddress(String housesAddress) {
        this.housesAddress = housesAddress;
    }
    
    public String getHousesAddress() {
        return this.housesAddress;
    }
    public void setHousesBuildingYear(String housesBuildingYear) {
        this.housesBuildingYear = housesBuildingYear;
    }
    
    public String getHousesBuildingYear() {
        return this.housesBuildingYear;
    }
    public void setHousesType(String housesType) {
        this.housesType = housesType;
    }
    
    public String getHousesType() {
        return this.housesType;
    }
    public void setHousesPrice(String housesPrice) {
        this.housesPrice = housesPrice;
    }
    
    public String getHousesPrice() {
        return this.housesPrice;
    }
    public void setYearOfPropertyRights(String yearOfPropertyRights) {
        this.yearOfPropertyRights = yearOfPropertyRights;
    }
    
    public String getYearOfPropertyRights() {
        return this.yearOfPropertyRights;
    }
    public void setInformationSources(String informationSources) {
        this.informationSources = informationSources;
    }
    
    public String getInformationSources() {
        return this.informationSources;
    }
    public void setInfoUpdateTime(String infoUpdateTime) {
        this.infoUpdateTime = infoUpdateTime;
    }
    
    public String getInfoUpdateTime() {
        return this.infoUpdateTime;
    }
 
    public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public void setDevelopers(String developers) {
        this.developers = developers;
    }
    
    public String getDevelopers() {
        return this.developers;
    }
    public void setAfforestationRate(String afforestationRate) {
        this.afforestationRate = afforestationRate;
    }
    
    public String getAfforestationRate() {
        return this.afforestationRate;
    }
    public void setPlotRatio(String plotRatio) {
        this.plotRatio = plotRatio;
    }
    
    public String getPlotRatio() {
        return this.plotRatio;
    }
    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }
    
    public String getBuildingType() {
        return this.buildingType;
    }
    public void setPropertyCompany(String propertyCompany) {
        this.propertyCompany = propertyCompany;
    }
    
    public String getPropertyCompany() {
        return this.propertyCompany;
    }
    public void setTotalHouseholds(String totalHouseholds) {
        this.totalHouseholds = totalHouseholds;
    }
    
    public String getTotalHouseholds() {
        return this.totalHouseholds;
    }
    public void setPropertyCosts(String propertyCosts) {
        this.propertyCosts = propertyCosts;
    }
    
    public String getPropertyCosts() {
        return this.propertyCosts;
    }
    public void setTraffic(String traffic) {
        this.traffic = traffic;
    }
    
    public String getTraffic() {
        return this.traffic;
    }
    public void setKindergarten(String kindergarten) {
        this.kindergarten = kindergarten;
    }
    
    public String getKindergarten() {
        return this.kindergarten;
    }
    public void setSchool(String school) {
        this.school = school;
    }
    
    public String getSchool() {
        return this.school;
    }
    public void setPowerCenter(String powerCenter) {
        this.powerCenter = powerCenter;
    }
    
    public String getPowerCenter() {
        return this.powerCenter;
    }
    public void setHospital(String hospital) {
        this.hospital = hospital;
    }
    
    public String getHospital() {
        return this.hospital;
    }
    public void setBank(String bank) {
        this.bank = bank;
    }
    
    public String getBank() {
        return this.bank;
    }
    public void setPost(String post) {
        this.post = post;
    }
    
    public String getPost() {
        return this.post;
    }
    public void setOther(String other) {
        this.other = other;
    }
    
    public String getOther() {
        return this.other;
    }
    public void setFloorArea(String floorArea) {
        this.floorArea = floorArea;
    }
    
    public String getFloorArea() {
        return this.floorArea;
    }
    public void setBuildingArea(String buildingArea) {
        this.buildingArea = buildingArea;
    }
    
    public String getBuildingArea() {
        return this.buildingArea;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getUrl() {
        return this.url;
    }

}
