package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileTemplateView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 类型（市调或者模板，生成pdf模板）
     */
    private String type;
    /**
     * 模板名称
     */
    private String name;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 房屋类型
     */
    private String houseType;
    /**
     * 文件地址
     */
    private String fileUrl;
    /**
     * 竞品地图base64编码
     * @return
     */
    private String imgBase64;
    public String getImgBase64() {
		return imgBase64;
	}

	public void setImgBase64(String imgBase64) {
		this.imgBase64 = imgBase64;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

    public String getHouseType() {
		return houseType;
	}

	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setType(String type) {
        this.type = type;
    }
    
    public String getType() {
        return this.type;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }

}