package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileCityView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 城市名称
     */
    private String cityName;
    /**
     * 经纬度
     */
    private String latitudeLongitude;

    public String getLatitudeLongitude() {
		return latitudeLongitude;
	}

	public void setLatitudeLongitude(String latitudeLongitude) {
		this.latitudeLongitude = latitudeLongitude;
	}

	public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    
    public String getCityName() {
        return this.cityName;
    }

}