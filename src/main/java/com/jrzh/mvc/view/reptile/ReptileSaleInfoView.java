package com.jrzh.mvc.view.reptile;

import java.util.Date;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileSaleInfoView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 成交套数
     */
    private String allNumber;
    /**
     * 成交面积
     */
    private String allArea;
    /**
     * 城市(
     */
    private String city;
    /**
     * 成交价格
     */
    private String allPrice;
    /**
     * 时间
     */
    private Date infoUpdataTime;
    /**
     * 结束时间
     */
    private String endTime;
    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 时间段
     */
    private String dateType;

    public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getDateType() {
		return dateType;
	}

	public void setDateType(String dateType) {
		this.dateType = dateType;
	}

	public void setAllNumber(String allNumber) {
        this.allNumber = allNumber;
    }
    
    public String getAllNumber() {
        return this.allNumber;
    }
    public void setAllArea(String allArea) {
        this.allArea = allArea;
    }
    
    public String getAllArea() {
        return this.allArea;
    }
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getCity() {
        return this.city;
    }
    public void setAllPrice(String allPrice) {
        this.allPrice = allPrice;
    }
    
    public String getAllPrice() {
        return this.allPrice;
    }
    public void setInfoUpdataTime(Date infoUpdataTime) {
        this.infoUpdataTime = infoUpdataTime;
    }
    
    public Date getInfoUpdataTime() {
        return this.infoUpdataTime;
    }

}