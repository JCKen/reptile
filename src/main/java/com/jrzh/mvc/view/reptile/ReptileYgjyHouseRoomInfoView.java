package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileYgjyHouseRoomInfoView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 关联房源id
     */
    private String houseId;
    /**
     * 关联楼层栋id
     */
    private String floorId;
    /**
     * 房号
     */
    private String roomNumber;
    /**
     * 房型
     */
    private String roomType;
    /**
     * 房间总面积
     */
    private String roomTotalArea;
    /**
     * 户型
     */
    private String roomApartment;
    /**
     * 房号
     */
    private String roomStatus;
    /**
     * 抵押状态
     */
    private String roomMortgage;
    /**
     * 查封状态
     */
    private String roomSealingState;
    /**
     * 所在楼层
     */
    private String louCeng;
    /**
     * 套内面积
     */
    private String roomRealArea;
    /**
     * 按建筑面积拟售单价
     */
    private String roomPrice;
    /**
     * 按套内面积拟售单价
     */
    private String roomRealPrice;

    public String getRoomPrice() {
		return roomPrice;
	}

	public void setRoomPrice(String roomPrice) {
		this.roomPrice = roomPrice;
	}

	public String getRoomRealPrice() {
		return roomRealPrice;
	}

	public void setRoomRealPrice(String roomRealPrice) {
		this.roomRealPrice = roomRealPrice;
	}

	public String getRoomRealArea() {
		return roomRealArea;
	}

	public void setRoomRealArea(String roomRealArea) {
		this.roomRealArea = roomRealArea;
	}

	public String getLouCeng() {
		return louCeng;
	}

	public void setLouCeng(String louCeng) {
		this.louCeng = louCeng;
	}

	public void setHouseId(String houseId) {
        this.houseId = houseId;
    }
    
    public String getHouseId() {
        return this.houseId;
    }
    public void setFloorId(String floorId) {
        this.floorId = floorId;
    }
    
    public String getFloorId() {
        return this.floorId;
    }
    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }
    
    public String getRoomNumber() {
        return this.roomNumber;
    }
    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }
    
    public String getRoomType() {
        return this.roomType;
    }
    public void setRoomTotalArea(String roomTotalArea) {
        this.roomTotalArea = roomTotalArea;
    }
    
    public String getRoomTotalArea() {
        return this.roomTotalArea;
    }
    public void setRoomApartment(String roomApartment) {
        this.roomApartment = roomApartment;
    }
    
    public String getRoomApartment() {
        return this.roomApartment;
    }
    public void setRoomStatus(String roomStatus) {
        this.roomStatus = roomStatus;
    }
    
    public String getRoomStatus() {
        return this.roomStatus;
    }
    public void setRoomMortgage(String roomMortgage) {
        this.roomMortgage = roomMortgage;
    }
    
    public String getRoomMortgage() {
        return this.roomMortgage;
    }
    public void setRoomSealingState(String roomSealingState) {
        this.roomSealingState = roomSealingState;
    }
    
    public String getRoomSealingState() {
        return this.roomSealingState;
    }

}