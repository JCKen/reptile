package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileCorrectionSetView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 竞品id
     */
    private String houseId;
    /**
     * 设置人
     */
    private String userId;
    /**
     * 修正项id
     */
    private String correctionId;
    /**
     * 修正值
     */
    private String correctionValue;

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }
    
    public String getHouseId() {
        return this.houseId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getUserId() {
        return this.userId;
    }
    public void setCorrectionId(String correctionId) {
        this.correctionId = correctionId;
    }
    
    public String getCorrectionId() {
        return this.correctionId;
    }
    public void setCorrectionValue(String correctionValue) {
        this.correctionValue = correctionValue;
    }
    
    public String getCorrectionValue() {
        return this.correctionValue;
    }

}