package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileCorrectionFactorView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 父ID
     */
    private String pid;
    /**
     * 修正项
     */
    private String correctionName;
    /**
     * 修正系数最大值
     */
    private String correctionMax;
    /**
     * 修正系数最小值
     */
    private String correctionMin;

    public void setPid(String pid) {
        this.pid = pid;
    }
    
    public String getPid() {
        return this.pid;
    }
    public void setCorrectionName(String correctionName) {
        this.correctionName = correctionName;
    }
    
    public String getCorrectionName() {
        return this.correctionName;
    }
    public void setCorrectionMax(String correctionMax) {
        this.correctionMax = correctionMax;
    }
    
    public String getCorrectionMax() {
        return this.correctionMax;
    }
    public void setCorrectionMin(String correctionMin) {
        this.correctionMin = correctionMin;
    }
    
    public String getCorrectionMin() {
        return this.correctionMin;
    }

}