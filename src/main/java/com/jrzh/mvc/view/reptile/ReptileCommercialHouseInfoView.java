package com.jrzh.mvc.view.reptile;

import java.util.Date;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileCommercialHouseInfoView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 用途
     */
    private String use;
    /**
     * 成交套数
     */
    private String transactionNumber;
    /**
     * 成交面积
     */
    private String transactionArea;
    /**
     * 城市
     */
    private String city;
    /**
     * 区域
     */
    private String part;
    /**
     * 成交均价
     */
    private String transactionPrice;
    /**
     * 可售面积
     */
    private String saleableArea;
    /**
     * 可售套数
     */
    private String availableSets;
    /**
     * 时间
     */
    private Date infoUpdataTime;/**
     * 是否按城市分组（0：是；1：否）
     */
    private String isCity;
    /**
     * 是否按城市区域分组（0：是；1：否）
     */
    private String isPart;
    /**
     * 是否按用途分组（0：是；1：否）
     */
    private String isUse;
    
    
    private String transactionAllPrice;
    
    private String transactionAllNumber;
    
    private String transactionAllArea;
    
    private String saleableAllArea;
    
    private String availableAllSets;
    
    private String months;

   	public String getMonths() {
   		return months;
   	}

   	public void setMonths(String months) {
   		this.months = months;
   	}

    public String getIsCity() {
		return isCity;
	}

	public void setIsCity(String isCity) {
		this.isCity = isCity;
	}

	public String getIsPart() {
		return isPart;
	}

	public void setIsPart(String isPart) {
		this.isPart = isPart;
	}

	public String getIsUse() {
		return isUse;
	}

	public void setIsUse(String isUse) {
		this.isUse = isUse;
	}

	public String getTransactionAllPrice() {
		return transactionAllPrice;
	}

	public void setTransactionAllPrice(String transactionAllPrice) {
		this.transactionAllPrice = transactionAllPrice;
	}

	public String getTransactionAllNumber() {
		return transactionAllNumber;
	}

	public void setTransactionAllNumber(String transactionAllNumber) {
		this.transactionAllNumber = transactionAllNumber;
	}

	public String getTransactionAllArea() {
		return transactionAllArea;
	}

	public void setTransactionAllArea(String transactionAllArea) {
		this.transactionAllArea = transactionAllArea;
	}

	public String getSaleableAllArea() {
		return saleableAllArea;
	}

	public void setSaleableAllArea(String saleableAllArea) {
		this.saleableAllArea = saleableAllArea;
	}

	public String getAvailableAllSets() {
		return availableAllSets;
	}

	public void setAvailableAllSets(String availableAllSets) {
		this.availableAllSets = availableAllSets;
	}

	public void setUse(String use) {
        this.use = use;
    }
    
    public String getUse() {
        return this.use;
    }
    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }
    
    public String getTransactionNumber() {
        return this.transactionNumber;
    }
    public void setTransactionArea(String transactionArea) {
        this.transactionArea = transactionArea;
    }
    
    public String getTransactionArea() {
        return this.transactionArea;
    }
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getCity() {
        return this.city;
    }
    public void setPart(String part) {
        this.part = part;
    }
    
    public String getPart() {
        return this.part;
    }
    public void setTransactionPrice(String transactionPrice) {
        this.transactionPrice = transactionPrice;
    }
    
    public String getTransactionPrice() {
        return this.transactionPrice;
    }
    public void setSaleableArea(String saleableArea) {
        this.saleableArea = saleableArea;
    }
    
    public String getSaleableArea() {
        return this.saleableArea;
    }
    public void setAvailableSets(String availableSets) {
        this.availableSets = availableSets;
    }
    
    public String getAvailableSets() {
        return this.availableSets;
    }
    public void setInfoUpdataTime(Date infoUpdataTime) {
        this.infoUpdataTime = infoUpdataTime;
    }
    
    public Date getInfoUpdataTime() {
        return this.infoUpdataTime;
    }

}