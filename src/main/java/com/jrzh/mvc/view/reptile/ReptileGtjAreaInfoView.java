package com.jrzh.mvc.view.reptile;


import java.util.Date;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileGtjAreaInfoView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 地块编号
     */
    private String areaNo;
    /**
     * 地块位置
     */
    private String address;
    /**
     * 地块用途
     */
    private String use;
    /**
     * 地块面积
     */
    private String acreage;
    /**
     * 成交价
     */
    private String price;
    /**
     * 竞得人
     */
    private String buyer;

    /**
     * 来源
     */
    private String from;
    /**
     * 数据链接
     */
    private String url;
    /**
     * 容积率
     */
    private String plotRatio;
    /**
     * 经度
     */
    private String lnt;
    
    /**
     * 纬度
     */
    private String lat;
    
    /**
     * 城市
     */
    private String city;
    
    /**
     * 出让时间
     */
    private Date time;
    
    
    
    public String getFrom() {
		return from;
	}

	public String getPlotRatio() {
		return plotRatio;
	}

	public void setPlotRatio(String plotRatio) {
		this.plotRatio = plotRatio;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getLnt() {
		return lnt;
	}

	public void setLnt(String lnt) {
		this.lnt = lnt;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public void setAreaNo(String areaNo) {
        this.areaNo = areaNo;
    }
    
    public String getAreaNo() {
        return this.areaNo;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getAddress() {
        return this.address;
    }
    public void setUse(String use) {
        this.use = use;
    }
    
    public String getUse() {
        return this.use;
    }
    public void setAcreage(String acreage) {
        this.acreage = acreage;
    }
    
    public String getAcreage() {
        return this.acreage;
    }
    public void setPrice(String price) {
        this.price = price;
    }
    
    public String getPrice() {
        return this.price;
    }
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }
    
    public String getBuyer() {
        return this.buyer;
    }

}