package com.jrzh.mvc.view.reptile;


import com.jrzh.framework.base.view.GeneralView;

public class ReptileEsfHousePriceView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 房价
     */
    private String housePrice;
    /**
     * 房子ID
     */
    private String houseId;
    /**
     * 房价单位(1. 元/㎡ 2. 万/套)
     */
    private String housePriceUnit;
    /**
     * 房价来源
     */
    private String infoSource;
    /**
     *价格状态（售价 / 成交价）
     */
    private String status;
    
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

    public void setHousePrice(String housePrice) {
        this.housePrice = housePrice;
    }
    
    public String getHousePrice() {
        return this.housePrice;
    }
    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }
    
    public String getHouseId() {
        return this.houseId;
    }
    public void setHousePriceUnit(String housePriceUnit) {
        this.housePriceUnit = housePriceUnit;
    }
    
    public String getHousePriceUnit() {
        return this.housePriceUnit;
    }
    public void setInfoSource(String infoSource) {
        this.infoSource = infoSource;
    }
    
    public String getInfoSource() {
        return this.infoSource;
    }

}