package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileAreaView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 区域名称
     */
    private String areaName;
    /**
     * 所属城市
     */
    private String ownCity;
    
    /**
     * 经纬度
     */
    private String latitudeLongitude;

    public String getOwnCity() {
		return ownCity;
	}

	public void setOwnCity(String ownCity) {
		this.ownCity = ownCity;
	}

	public String getLatitudeLongitude() {
		return latitudeLongitude;
	}

	public void setLatitudeLongitude(String latitudeLongitude) {
		this.latitudeLongitude = latitudeLongitude;
	}

	public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
    
    public String getAreaName() {
        return this.areaName;
    }

}