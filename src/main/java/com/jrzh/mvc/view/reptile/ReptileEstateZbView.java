package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileEstateZbView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 指标名称
     */
    private String zb;
    /**
     * 单位
     */
    private String unit;
    /**
     * 指标编号
     */
    private String code;
    /**
     * 指标分类
     */
    private String classify;
    
    /**
     * 城市
     */
    private String city;
    
    /**
     * 所属菜单
     */
    private String menu;
   
    private String url;
    
    public void setZb(String zb) {
        this.zb = zb;
    }
    
    public String getZb() {
        return this.zb;
    }
    public void setUnit(String unit) {
        this.unit = unit;
    }
    
    public String getUnit() {
        return this.unit;
    }
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCode() {
        return this.code;
    }

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}