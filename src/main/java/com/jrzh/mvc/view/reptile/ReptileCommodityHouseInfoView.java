package com.jrzh.mvc.view.reptile;

import java.util.Date;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileCommodityHouseInfoView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 户型
     */
    private String houseType;
    /**
     * 成交套数
     */
    private String transactionNumber;
    /**
     * 成交面积
     */
    private String transactionArea;
    /**
     * 城市
     */
    private String city;
    /**
     * 区域
     */
    private String part;
    /**
     * 成交均价
     */
    private String transactionPrice;
    /**
     * 可售面积
     */
    private String saleableArea;
    /**
     * 可售套数
     */
    private String availableSets;
    /**
     * 时间
     */
    private Date infoUpdataTime;
    /**
     * 是否按城市分组（0：是；1：否）
     */
    private String isCity;
    /**
     * 是否按城市区域分组（0：是；1：否）
     */
    private String isPart;
    /**
     * 是否按户型分组（0：是；1：否）
     */
    private String isHouseType;
    
    private String transactionAllPrice;
    
    private String transactionAllNumber;
    
    private String transactionAllArea;
    
    private String saleableAllArea;
    
    private String availableAllSets;
    
    private String months;

    private String ratio;//用于导出报告字段  供求比
    
    private String dealPrice;//用于导出报告字段  成交价格
    
    
    
	public String getDealPrice() {
		return dealPrice;
	}

	public void setDealPrice(String dealPrice) {
		this.dealPrice = dealPrice;
	}

	public String getRatio() {
		return ratio;
	}

	public void setRatio(String ratio) {
		this.ratio = ratio;
	}

	public String getMonths() {
		return months;
	}

	public void setMonths(String months) {
		this.months = months;
	}

	public String getTransactionAllPrice() {
		return transactionAllPrice;
	}

	public void setTransactionAllPrice(String transactionAllPrice) {
		this.transactionAllPrice = transactionAllPrice;
	}

	public String getTransactionAllNumber() {
		return transactionAllNumber;
	}

	public void setTransactionAllNumber(String transactionAllNumber) {
		this.transactionAllNumber = transactionAllNumber;
	}

	public String getTransactionAllArea() {
		return transactionAllArea;
	}

	public void setTransactionAllArea(String transactionAllArea) {
		this.transactionAllArea = transactionAllArea;
	}

	public String getSaleableAllArea() {
		return saleableAllArea;
	}

	public void setSaleableAllArea(String saleableAllArea) {
		this.saleableAllArea = saleableAllArea;
	}

	public String getAvailableAllSets() {
		return availableAllSets;
	}

	public void setAvailableAllSets(String availableAllSets) {
		this.availableAllSets = availableAllSets;
	}

	public String getIsCity() {
		return isCity;
	}

	public void setIsCity(String isCity) {
		this.isCity = isCity;
	}

	public String getIsPart() {
		return isPart;
	}

	public void setIsPart(String isPart) {
		this.isPart = isPart;
	}

	public String getIsHouseType() {
		return isHouseType;
	}

	public void setIsHouseType(String isHouseType) {
		this.isHouseType = isHouseType;
	}

	public void setHouseType(String houseType) {
        this.houseType = houseType;
    }
    
    public String getHouseType() {
        return this.houseType;
    }
    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }
    
    public String getTransactionNumber() {
        return this.transactionNumber;
    }
    public void setTransactionArea(String transactionArea) {
        this.transactionArea = transactionArea;
    }
    
    public String getTransactionArea() {
        return this.transactionArea;
    }
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getCity() {
        return this.city;
    }
    public void setPart(String part) {
        this.part = part;
    }
    
    public String getPart() {
        return this.part;
    }
    public void setTransactionPrice(String transactionPrice) {
        this.transactionPrice = transactionPrice;
    }
    
    public String getTransactionPrice() {
        return this.transactionPrice;
    }
    public void setSaleableArea(String saleableArea) {
        this.saleableArea = saleableArea;
    }
    
    public String getSaleableArea() {
        return this.saleableArea;
    }
    public void setAvailableSets(String availableSets) {
        this.availableSets = availableSets;
    }
    
    public String getAvailableSets() {
        return this.availableSets;
    }
    public void setInfoUpdataTime(Date infoUpdataTime) {
        this.infoUpdataTime = infoUpdataTime;
    }
    
    public Date getInfoUpdataTime() {
        return this.infoUpdataTime;
    }

}