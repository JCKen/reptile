/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.mvc.view.reptile.shanghaiAreaBean;

/**
 * Auto-generated: 2019-01-11 16:30:59
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class PageRootBean {

    private int flag;
    private PageData data;
    private String msg;
    public void setFlag(int flag) {
         this.flag = flag;
     }
     public int getFlag() {
         return flag;
     }

    public void setData(PageData  data) {
         this.data = data;
     }
     public PageData  getData() {
         return data;
     }

    public void setMsg(String msg) {
         this.msg = msg;
     }
     public String getMsg() {
         return msg;
     }
	@Override
	public String toString() {
		return "PageRootBean [flag=" + flag + ", data=" + data + ", msg=" + msg
				+ "]";
	}

     
}