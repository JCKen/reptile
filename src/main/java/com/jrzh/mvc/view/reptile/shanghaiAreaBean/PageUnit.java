/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.mvc.view.reptile.shanghaiAreaBean;

/**
 * Auto-generated: 2019-01-11 16:30:59
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class PageUnit {

    private String churnx;
    private String mianj;
    private String jiaoylx;
    private String blockstate;
    private String dyjy;
    private String rn;
    private String bianh;
    private String dqyw;
    private String lqsqsrs;
    private String dikmc;
    private String jydd;
    private String guihyt;
    private String jmsqrs;
    public void setChurnx(String churnx) {
         this.churnx = churnx;
     }
     public String getChurnx() {
         return churnx;
     }

    public void setMianj(String mianj) {
         this.mianj = mianj;
     }
     public String getMianj() {
         return mianj;
     }

    public void setJiaoylx(String jiaoylx) {
         this.jiaoylx = jiaoylx;
     }
     public String getJiaoylx() {
         return jiaoylx;
     }

    public void setBlockstate(String blockstate) {
         this.blockstate = blockstate;
     }
     public String getBlockstate() {
         return blockstate;
     }

    public void setDyjy(String dyjy) {
         this.dyjy = dyjy;
     }
     public String getDyjy() {
         return dyjy;
     }

    public void setRn(String rn) {
         this.rn = rn;
     }
     public String getRn() {
         return rn;
     }

    public void setBianh(String bianh) {
         this.bianh = bianh;
     }
     public String getBianh() {
         return bianh;
     }

    public void setDqyw(String dqyw) {
         this.dqyw = dqyw;
     }
     public String getDqyw() {
         return dqyw;
     }

    public void setLqsqsrs(String lqsqsrs) {
         this.lqsqsrs = lqsqsrs;
     }
     public String getLqsqsrs() {
         return lqsqsrs;
     }

    public void setDikmc(String dikmc) {
         this.dikmc = dikmc;
     }
     public String getDikmc() {
         return dikmc;
     }

    public void setJydd(String jydd) {
         this.jydd = jydd;
     }
     public String getJydd() {
         return jydd;
     }

    public void setGuihyt(String guihyt) {
         this.guihyt = guihyt;
     }
     public String getGuihyt() {
         return guihyt;
     }

    public void setJmsqrs(String jmsqrs) {
         this.jmsqrs = jmsqrs;
     }
     public String getJmsqrs() {
         return jmsqrs;
     }
	@Override
	public String toString() {
		return "PageList [churnx=" + churnx + ", mianj=" + mianj + ", jiaoylx="
				+ jiaoylx + ", blockstate=" + blockstate + ", dyjy=" + dyjy
				+ ", rn=" + rn + ", bianh=" + bianh + ", dqyw=" + dqyw
				+ ", lqsqsrs=" + lqsqsrs + ", dikmc=" + dikmc + ", jydd="
				+ jydd + ", guihyt=" + guihyt + ", jmsqrs=" + jmsqrs + "]";
	}

}