package com.jrzh.mvc.view.reptile;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileNewHouseView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 小区名
     */
    private String houseName;
    /**
     * 小区别名
     */
    private String houseOtherName;
    /**
     * 小区所在市
     */
    private String houseCity;
    /**
     * 小区所在区
     */
    private String housePart;
    /**
     * 小区详细地址
     */
    private String houseAddress;
    /**
     * 楼盘均价
     */
    private String housePrice;
    /**
     * 楼盘类型（商业、住宅、别墅、写字楼）
     */
    private String houseType;
    /**
     * 楼盘销售状态
     */
    private String houseSaleStatus;
    /**
     * 楼盘是否新开（根据网址新开楼盘进行判断）
     */
    private String isNewHouse;
    /**
     * 总价（多少钱一套）
     */
    private String houseAllPrice;
    /**
     * 楼盘标签
     */
    private String houseTab;
    /**
     * 楼盘评分
     */
    private String houseGrade;
    /**
     * 最新开盘时间
     */
    private String latestSaleTime;
    /**
     * 主力户型
     */
    private String mainUnit;
    /**
     * 售楼处地址
     */
    private String salesOfficeAddress;
    /**
     * 开发商
     */
    private String developers;
    /**
     * 物业类别
     */
    private String propertyCategory;
    /**
     * 建筑面积
     */
    private String buildingArea;
    /**
     * 占地面积
     */
    private String areaCovered;
    /**
     * 绿化率
     */
    private String afforestationRate;
    /**
     * 容积率
     */
    private String plotRatio;
    /**
     * 车位
     */
    private String parkingLot;
    /**
     * 供水方式
     */
    private String waterSupplyMode;
    /**
     * 供电方式
     */
    private String powerSupplyMode;
    /**
     * 产权年限
     */
    private String propertyRightYears;
    /**
     * 物业公司
     */
    private String propertyCompany;
    /**
     * 规划户数
     */
    private String planningHouseholds;
    /**
     * 交房时间
     */
    private String timeOfDelivery;
    /**
     * 物业费用
     */
    private String propertyCosts;
    /**
     * 信息更新时间
     */
    private Date infoUpdateTime;
    /**
     * 开始时间
     */
    private Date beginTime;
    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 信息来源
     */
    private String informationSources;
    /**
     * 纬度
     */
    private BigDecimal latitude;
    /**
     * 经度
     */
    private BigDecimal longitude;

    
    /**
     * 交通
     */
    private String traffic;
    
    /**
     * 周边幼儿园
     */
    private String kindergarten;
    
    /**
     * 周边学校
     */
    private String school;
    
    /**
     * 周边商业中心
     */
    private String powerCenter;
    
    /**
     * 周边医院
     */
    private String hospital;
    
    /**
     * 周边银行
     */
    private String bank;
    
    /**
     * 周边邮政
     */
    private String post;
    
    /**
     * 周边其他设施
     */
    private String other;
    
    /**
     * 价格单位
     */
    private String priceUnit;
    
    /**
     * 价格列表
     */
    private List<String> priceList;
    
    /**
     * 是否查询当天数据
     */
    private String isToday;
    /**
     * 楼盘建面
     */
    private String houseBuildingArea;
    /**
     * 成交总量
     */
    private Integer count;
    
    /**
     * 数据来源地址
     * @return
     */
    private String houseUrl;

    
	public String getHouseUrl() {
		return houseUrl;
	}

	public void setHouseUrl(String houseUrl) {
		this.houseUrl = houseUrl;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getHouseBuildingArea() {
		return houseBuildingArea;
	}

	public void setHouseBuildingArea(String houseBuildingArea) {
		this.houseBuildingArea = houseBuildingArea;
	}

	public String getIsToday() {
		return isToday;
	}

	public void setIsToday(String isToday) {
		this.isToday = isToday;
	}

	public List<String> getPriceList() {
		return priceList;
	}

	public void setPriceList(List<String> priceList) {
		this.priceList = priceList;
	}

	public String getPriceUnit() {
		return priceUnit;
	}

	public void setPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
	}

	public String getTraffic() {
		return traffic;
	}

	public void setTraffic(String traffic) {
		this.traffic = traffic;
	}

	public String getKindergarten() {
		return kindergarten;
	}

	public void setKindergarten(String kindergarten) {
		this.kindergarten = kindergarten;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}



	public String getPowerCenter() {
		return powerCenter;
	}

	public void setPowerCenter(String powerCenter) {
		this.powerCenter = powerCenter;
	}

	public String getHospital() {
		return hospital;
	}

	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getInformationSources() {
		return informationSources;
	}

	public void setInformationSources(String informationSources) {
		this.informationSources = informationSources;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getInfoUpdateTime() {
		return infoUpdateTime;
	}

	public void setInfoUpdateTime(Date infoUpdateTime) {
		this.infoUpdateTime = infoUpdateTime;
	}

	public void setHouseName(String houseName) {
        this.houseName = houseName;
    }
    
    public String getHouseName() {
        return this.houseName;
    }
    public void setHouseOtherName(String houseOtherName) {
        this.houseOtherName = houseOtherName;
    }
    
    public String getHouseOtherName() {
        return this.houseOtherName;
    }
    public void setHouseCity(String houseCity) {
        this.houseCity = houseCity;
    }
    
    public String getHouseCity() {
        return this.houseCity;
    }
    public void setHousePart(String housePart) {
        this.housePart = housePart;
    }
    
    public String getHousePart() {
        return this.housePart;
    }
    public void setHouseAddress(String houseAddress) {
        this.houseAddress = houseAddress;
    }
    
    public String getHouseAddress() {
        return this.houseAddress;
    }
    public void setHousePrice(String housePrice) {
        this.housePrice = housePrice;
    }
    
    public String getHousePrice() {
        return this.housePrice;
    }
    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }
    
    public String getHouseType() {
        return this.houseType;
    }
    public void setHouseSaleStatus(String houseSaleStatus) {
        this.houseSaleStatus = houseSaleStatus;
    }
    
    public String getHouseSaleStatus() {
        return this.houseSaleStatus;
    }
    public void setIsNewHouse(String isNewHouse) {
        this.isNewHouse = isNewHouse;
    }
    
    public String getIsNewHouse() {
        return this.isNewHouse;
    }
    public void setHouseAllPrice(String houseAllPrice) {
        this.houseAllPrice = houseAllPrice;
    }
    
    public String getHouseAllPrice() {
        return this.houseAllPrice;
    }
    public void setHouseTab(String houseTab) {
        this.houseTab = houseTab;
    }
    
    public String getHouseTab() {
        return this.houseTab;
    }
    public void setHouseGrade(String houseGrade) {
        this.houseGrade = houseGrade;
    }
    
    public String getHouseGrade() {
        return this.houseGrade;
    }
    public void setLatestSaleTime(String latestSaleTime) {
        this.latestSaleTime = latestSaleTime;
    }
    
    public String getLatestSaleTime() {
        return this.latestSaleTime;
    }
    public void setMainUnit(String mainUnit) {
        this.mainUnit = mainUnit;
    }
    
    public String getMainUnit() {
        return this.mainUnit;
    }
    public void setSalesOfficeAddress(String salesOfficeAddress) {
        this.salesOfficeAddress = salesOfficeAddress;
    }
    
    public String getSalesOfficeAddress() {
        return this.salesOfficeAddress;
    }
    public void setDevelopers(String developers) {
        this.developers = developers;
    }
    
    public String getDevelopers() {
        return this.developers;
    }
    public void setPropertyCategory(String propertyCategory) {
        this.propertyCategory = propertyCategory;
    }
    
    public String getPropertyCategory() {
        return this.propertyCategory;
    }
    public void setBuildingArea(String buildingArea) {
        this.buildingArea = buildingArea;
    }
    
    public String getBuildingArea() {
        return this.buildingArea;
    }
    public void setAreaCovered(String areaCovered) {
        this.areaCovered = areaCovered;
    }
    
    public String getAreaCovered() {
        return this.areaCovered;
    }
    public void setAfforestationRate(String afforestationRate) {
        this.afforestationRate = afforestationRate;
    }
    
    public String getAfforestationRate() {
        return this.afforestationRate;
    }
    public void setPlotRatio(String plotRatio) {
        this.plotRatio = plotRatio;
    }
    
    public String getPlotRatio() {
        return this.plotRatio;
    }
    public void setParkingLot(String parkingLot) {
        this.parkingLot = parkingLot;
    }
    
    public String getParkingLot() {
        return this.parkingLot;
    }
    public void setWaterSupplyMode(String waterSupplyMode) {
        this.waterSupplyMode = waterSupplyMode;
    }
    
    public String getWaterSupplyMode() {
        return this.waterSupplyMode;
    }
    public void setPowerSupplyMode(String powerSupplyMode) {
        this.powerSupplyMode = powerSupplyMode;
    }
    
    public String getPowerSupplyMode() {
        return this.powerSupplyMode;
    }
    public void setPropertyRightYears(String propertyRightYears) {
        this.propertyRightYears = propertyRightYears;
    }
    
    public String getPropertyRightYears() {
        return this.propertyRightYears;
    }
    public void setPropertyCompany(String propertyCompany) {
        this.propertyCompany = propertyCompany;
    }
    
    public String getPropertyCompany() {
        return this.propertyCompany;
    }
    public void setPlanningHouseholds(String planningHouseholds) {
        this.planningHouseholds = planningHouseholds;
    }
    
    public String getPlanningHouseholds() {
        return this.planningHouseholds;
    }
    public void setTimeOfDelivery(String timeOfDelivery) {
        this.timeOfDelivery = timeOfDelivery;
    }
    
    public String getTimeOfDelivery() {
        return this.timeOfDelivery;
    }
    public void setPropertyCosts(String propertyCosts) {
        this.propertyCosts = propertyCosts;
    }
    
    public String getPropertyCosts() {
        return this.propertyCosts;
    }

}