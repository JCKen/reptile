package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileUserView extends GeneralView {
	private static final long serialVersionUID = 1L;

	/**
	 * 员工ID
	 */
	private String userId;
	/**
	 * 是否停用（0：停用；1：启用）
	 */
	private String isStop;
	/**
	 * 员工手机
	 */
	private String mobile;
	/**
	 * 员工姓名
	 */
	private String userName;
	/**
	 * 部门名称
	 */
	private String deptName;
	/**
	 * 密码
	 */
	private String pwd;
	/**
	 * 是否是管理员
	 */
	private String isAdmin;

	public String getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(String isAdmin) {
		this.isAdmin = isAdmin;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPwd() {
		return this.pwd;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setIsStop(String isStop) {
		this.isStop = isStop;
	}

	public String getIsStop() {
		return this.isStop;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptName() {
		return this.deptName;
	}

}