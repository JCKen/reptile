package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileHousePriceView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 房价
     */
    private String housePrice;
    /**
     * 房子ID
     */
    private String houseId;
    /**
     * 房价单位(1. 元/㎡ 2. 万/套)
     */
    private String housePriceUnit;
    
    private String infoSource;

    public String getInfoSource() {
		return infoSource;
	}

	public void setInfoSource(String infoSource) {
		this.infoSource = infoSource;
	}

	public void setHousePrice(String housePrice) {
        this.housePrice = housePrice;
    }
    
    public String getHousePrice() {
        return this.housePrice;
    }
    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }
    
    public String getHouseId() {
        return this.houseId;
    }
    public void setHousePriceUnit(String housePriceUnit) {
        this.housePriceUnit = housePriceUnit;
    }
    
    public String getHousePriceUnit() {
        return this.housePriceUnit;
    }

}