package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileReportConfigView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 父节点ID
     */
    private String pid;
    /**
     * 页面名称
     */
    private String pageName;
    /**
     * 页面URL
     */
    private String pageUrl;
    /**
     * 页面是否显示(0.隐藏，1.显示)
     */
    private Integer isCheck;
    /**
     * 排序顺序
     */
    private Integer orderBy;

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getUserId() {
        return this.userId;
    }
    
    public void setPageName(String pageName) {
        this.pageName = pageName;
    }
    
    public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getPageName() {
        return this.pageName;
    }
    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }
    
    public String getPageUrl() {
        return this.pageUrl;
    }
    public void setIsCheck(Integer isCheck) {
        this.isCheck = isCheck;
    }
    
    public Integer getIsCheck() {
        return this.isCheck;
    }
    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }
    
    public Integer getOrderBy() {
        return this.orderBy;
    }

}