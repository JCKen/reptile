package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileTemplateSumInfoView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 模板ID
     */
    private String templateId;
    /**
     * 一手房占比
     */
    private String oneHouseRatio;
    /**
     * 二手房占比
     */
    private String twoHouseRatio;
    /**
     * 土地占比
     */
    private String landRatio;
    /**
     * 参考金额
     */
    private String referAmt;
    
    /**
     * 一手房金额
     */
    private String oneHouseAmt;
    /**
     * 二手房金额
     */
    private String twoHouseAmt;
    /**
     * 土地金额
     */
    private String landAmt;

    
    
    public String getOneHouseAmt() {
		return oneHouseAmt;
	}

	public void setOneHouseAmt(String oneHouseAmt) {
		this.oneHouseAmt = oneHouseAmt;
	}

	public String getTwoHouseAmt() {
		return twoHouseAmt;
	}

	public void setTwoHouseAmt(String twoHouseAmt) {
		this.twoHouseAmt = twoHouseAmt;
	}

	public String getLandAmt() {
		return landAmt;
	}

	public void setLandAmt(String landAmt) {
		this.landAmt = landAmt;
	}

	public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getUserId() {
        return this.userId;
    }
    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }
    
    public String getTemplateId() {
        return this.templateId;
    }
    public void setOneHouseRatio(String oneHouseRatio) {
        this.oneHouseRatio = oneHouseRatio;
    }
    
    public String getOneHouseRatio() {
        return this.oneHouseRatio;
    }
    public void setTwoHouseRatio(String twoHouseRatio) {
        this.twoHouseRatio = twoHouseRatio;
    }
    
    public String getTwoHouseRatio() {
        return this.twoHouseRatio;
    }
    public void setLandRatio(String landRatio) {
        this.landRatio = landRatio;
    }
    
    public String getLandRatio() {
        return this.landRatio;
    }
    public void setReferAmt(String referAmt) {
        this.referAmt = referAmt;
    }
    
    public String getReferAmt() {
        return this.referAmt;
    }

}