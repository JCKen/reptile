package com.jrzh.mvc.view.reptile;

import java.util.Date;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileHouseProjectNameView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 区名
     */
    private String areaName;
    /**
     * 城市名
     */
    private String cityName;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 总套数
     */
    private Double projectTotalNumber;
    /**
     * 总面积
     */
    private Double projectTotalArea;
    /**
     * 住宅套数
     */
    private Double projectHouseNumber;
    /**
     * 住宅面积
     */
    private Double projectHouseArea;
    /**
     * 可售总套数(未售)
     */
    private Double projectTotalHouseUnsoldNumber;
    /**
     * 可售总面积(未售)
     */
    private Double projectTotalHouseUnsoldArea;
    /**
     * 可售住宅套数
     */
    private Double projectHouseUnsoldNumber;
    /**
     * 可售住宅面积
     */
    private Double projectHouseUnsoldArea;
    /**
     * 已售总套数
     */
    private Double projectTotalHouseSoldNumber;
    /**
     * 已售总面积
     */
    private Double projectTotalHouseSoldArea;
    /**
     * 已售住宅套数
     */
    private Double projectHouseSoldNumber;
    /**
     * 已售住宅面积
     */
    private Double projectHouseSoldArea;
    /**
     * 已登记总套数
     */
    private Double projectRegisterTotalHouseNumber;
    /**
     * 已登记总面积
     */
    private Double projectRegisterTotalHouseArea;
    /**
     * 已登记住宅套数
     */
    private Double projectRegisterHouseNumber;
    /**
     * 已登记住宅面积
     */
    private Double projectRegisterHouseArea;
    /**
     * 合同撤消总套数
     */
    private Double projectCancellationsHouseArea;
    /**
     * 定价逾期总次数
     */
    private Double projectTotalPriceOverdueNumber;
    /**
     * 信息更新时间
     */
    private Date infoUpdateTime;
    /**
     * 房管局项目ID
     */
    private String projectId;
    
    private String developer;

   	public String getDeveloper() {
   		return developer;
   	}

   	public void setDeveloper(String developer) {
   		this.developer = developer;
   	}

    public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public Date getInfoUpdateTime() {
		return infoUpdateTime;
	}

	public void setInfoUpdateTime(Date infoUpdateTime) {
		this.infoUpdateTime = infoUpdateTime;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

    public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public void setProjectTotalNumber(Double projectTotalNumber) {
        this.projectTotalNumber = projectTotalNumber;
    }
    
    public Double getProjectTotalNumber() {
        return this.projectTotalNumber;
    }
    public void setProjectTotalArea(Double projectTotalArea) {
        this.projectTotalArea = projectTotalArea;
    }
    
    public Double getProjectTotalArea() {
        return this.projectTotalArea;
    }
    public void setProjectHouseNumber(Double projectHouseNumber) {
        this.projectHouseNumber = projectHouseNumber;
    }
    
    public Double getProjectHouseNumber() {
        return this.projectHouseNumber;
    }
    public void setProjectHouseArea(Double projectHouseArea) {
        this.projectHouseArea = projectHouseArea;
    }
    
    public Double getProjectHouseArea() {
        return this.projectHouseArea;
    }
    public void setProjectTotalHouseUnsoldNumber(Double projectTotalHouseUnsoldNumber) {
        this.projectTotalHouseUnsoldNumber = projectTotalHouseUnsoldNumber;
    }
    
    public Double getProjectTotalHouseUnsoldNumber() {
        return this.projectTotalHouseUnsoldNumber;
    }
    public void setProjectTotalHouseUnsoldArea(Double projectTotalHouseUnsoldArea) {
        this.projectTotalHouseUnsoldArea = projectTotalHouseUnsoldArea;
    }
    
    public Double getProjectTotalHouseUnsoldArea() {
        return this.projectTotalHouseUnsoldArea;
    }
    public void setProjectHouseUnsoldNumber(Double projectHouseUnsoldNumber) {
        this.projectHouseUnsoldNumber = projectHouseUnsoldNumber;
    }
    
    public Double getProjectHouseUnsoldNumber() {
        return this.projectHouseUnsoldNumber;
    }
    public void setProjectHouseUnsoldArea(Double projectHouseUnsoldArea) {
        this.projectHouseUnsoldArea = projectHouseUnsoldArea;
    }
    
    public Double getProjectHouseUnsoldArea() {
        return this.projectHouseUnsoldArea;
    }
    public void setProjectTotalHouseSoldNumber(Double projectTotalHouseSoldNumber) {
        this.projectTotalHouseSoldNumber = projectTotalHouseSoldNumber;
    }
    
    public Double getProjectTotalHouseSoldNumber() {
        return this.projectTotalHouseSoldNumber;
    }
    public void setProjectTotalHouseSoldArea(Double projectTotalHouseSoldArea) {
        this.projectTotalHouseSoldArea = projectTotalHouseSoldArea;
    }
    
    public Double getProjectTotalHouseSoldArea() {
        return this.projectTotalHouseSoldArea;
    }
    public void setProjectHouseSoldNumber(Double projectHouseSoldNumber) {
        this.projectHouseSoldNumber = projectHouseSoldNumber;
    }
    
    public Double getProjectHouseSoldNumber() {
        return this.projectHouseSoldNumber;
    }
    public void setProjectHouseSoldArea(Double projectHouseSoldArea) {
        this.projectHouseSoldArea = projectHouseSoldArea;
    }
    
    public Double getProjectHouseSoldArea() {
        return this.projectHouseSoldArea;
    }
    public void setProjectRegisterTotalHouseNumber(Double projectRegisterTotalHouseNumber) {
        this.projectRegisterTotalHouseNumber = projectRegisterTotalHouseNumber;
    }
    
    public Double getProjectRegisterTotalHouseNumber() {
        return this.projectRegisterTotalHouseNumber;
    }
    public void setProjectRegisterTotalHouseArea(Double projectRegisterTotalHouseArea) {
        this.projectRegisterTotalHouseArea = projectRegisterTotalHouseArea;
    }
    
    public Double getProjectRegisterTotalHouseArea() {
        return this.projectRegisterTotalHouseArea;
    }
    public void setProjectRegisterHouseNumber(Double projectRegisterHouseNumber) {
        this.projectRegisterHouseNumber = projectRegisterHouseNumber;
    }
    
    public Double getProjectRegisterHouseNumber() {
        return this.projectRegisterHouseNumber;
    }
    public void setProjectRegisterHouseArea(Double projectRegisterHouseArea) {
        this.projectRegisterHouseArea = projectRegisterHouseArea;
    }
    
    public Double getProjectRegisterHouseArea() {
        return this.projectRegisterHouseArea;
    }
    public void setProjectCancellationsHouseArea(Double projectCancellationsHouseArea) {
        this.projectCancellationsHouseArea = projectCancellationsHouseArea;
    }
    
    public Double getProjectCancellationsHouseArea() {
        return this.projectCancellationsHouseArea;
    }
    public void setProjectTotalPriceOverdueNumber(Double projectTotalPriceOverdueNumber) {
        this.projectTotalPriceOverdueNumber = projectTotalPriceOverdueNumber;
    }
    
    public Double getProjectTotalPriceOverdueNumber() {
        return this.projectTotalPriceOverdueNumber;
    }

}