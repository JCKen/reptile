package com.jrzh.mvc.view.reptile;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileEsfHouseView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 小区名
     */
    private String houseName;
    /**
     * 小区别名
     */
    private String houseOtherName;
    /**
     * 小区所在市
     */
    private String houseCity;
    /**
     * 小区所在区
     */
    private String housePart;
    /**
     * 小区所在小区
     */
    private String houseSmallPart;
    /**
     * 小区详细地址
     */
    private String houseAddress;
    /**
     * 房屋户型
     */
    private String houseType;
    /**
     * 户型结构
     */
    private String unitStructure;
    /**
     * 所在楼层
     */
    private String houseFloor;
    /**
     * 建筑面积
     */
    private String buildingArea;
    /**
     * 套内面积
     */
    private String areaCovered;
    /**
     * 梯户比例
     */
    private String ladderRatio;
    /**
     * 房屋朝向
     */
    private String houseOrientation;
    /**
     * 建筑类型
     */
    private String buildingType;
    /**
     * 建筑结构
     */
    private String buildingStructure;
    /**
     * 装修情况
     */
    private String renovationCondition;
    /**
     * 配备电梯
     */
    private String equippedWithSubway;
    /**
     * 产权年限
     */
    private String yearOfPropertyRights;
    /**
     * 楼盘均价
     */
    private String housePrice;
    /**
     * 挂牌时间
     */
    private String listingTime;
    /**
     * 房屋年限
     */
    private String yearOfHousing;
    /**
     * 抵押信息
     */
    private String mortgageInformation;
    /**
     * 上次交易时间
     */
    private String lastTradingTime;
    /**
     * 房屋用途
     */
    private String usageOfHouses;
    /**
     * 房本备案
     */
    private String housingFiling;
    /**
     * 产权所属
     */
    private String propertyRights;
    /**
     * 楼盘是否新开（根据网址新开楼盘进行判断）
     */
    private String isNewHouse;
    /**
     * 总价（多少钱一套）
     */
    private String houseAllPrice;
    /**
     * 信息来源
     */
    private String informationSources;
    /**
     * 信息更新时间
     */
    private Date infoUpdateTime;
    /**
     * 纬度
     */
    private BigDecimal latitude;
    /**
     * 经度
     */
    private BigDecimal longitude;
    /**
     * 附近交通
     */
    private String traffic;
    /**
     * 幼儿园
     */
    private String kindergarten;
    /**
     * 中小学
     */
    private String school;
    /**
     * 综合商场
     */
    private String powerCenter;
    /**
     * 医院
     */
    private String hospital;
    /**
     * 银行
     */
    private String bank;
    /**
     * 邮政
     */
    private String post;
    /**
     * 其他
     */
    private String other;
    /**
     * 房价单位(1. 元/㎡ 2. 万/套)
     */
    private String housePriceUnit;
    
    private String isToday;
    
    private List<String> priceList;
    
    //成交总量
    private Integer count;

    public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<String> getPriceList() {
		return priceList;
	}

	public void setPriceList(List<String> priceList) {
		this.priceList = priceList;
	}

	public String getIsToday() {
		return isToday;
	}

	public void setIsToday(String isToday) {
		this.isToday = isToday;
	}

	public void setHouseName(String houseName) {
        this.houseName = houseName;
    }
    
    public String getHouseName() {
        return this.houseName;
    }
    public void setHouseOtherName(String houseOtherName) {
        this.houseOtherName = houseOtherName;
    }
    
    public String getHouseOtherName() {
        return this.houseOtherName;
    }
    public void setHouseCity(String houseCity) {
        this.houseCity = houseCity;
    }

	public String getHouseCity() {
        return this.houseCity;
    }
    public void setHousePart(String housePart) {
        this.housePart = housePart;
    }
    
    public String getHousePart() {
        return this.housePart;
    }
    public void setHouseSmallPart(String houseSmallPart) {
        this.houseSmallPart = houseSmallPart;
    }
    
    public String getHouseSmallPart() {
        return this.houseSmallPart;
    }
    public void setHouseAddress(String houseAddress) {
        this.houseAddress = houseAddress;
    }
    
    public String getHouseAddress() {
        return this.houseAddress;
    }
    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }
    
    public String getHouseType() {
        return this.houseType;
    }
    public void setUnitStructure(String unitStructure) {
        this.unitStructure = unitStructure;
    }
    
    public String getUnitStructure() {
        return this.unitStructure;
    }
    public void setHouseFloor(String houseFloor) {
        this.houseFloor = houseFloor;
    }
    
    public String getHouseFloor() {
        return this.houseFloor;
    }
    public void setBuildingArea(String buildingArea) {
        this.buildingArea = buildingArea;
    }
    
    public String getBuildingArea() {
        return this.buildingArea;
    }
    public void setAreaCovered(String areaCovered) {
        this.areaCovered = areaCovered;
    }
    
    public String getAreaCovered() {
        return this.areaCovered;
    }
    public void setLadderRatio(String ladderRatio) {
        this.ladderRatio = ladderRatio;
    }
    
    public String getLadderRatio() {
        return this.ladderRatio;
    }
    public void setHouseOrientation(String houseOrientation) {
        this.houseOrientation = houseOrientation;
    }
    
    public String getHouseOrientation() {
        return this.houseOrientation;
    }
    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }
    
    public String getBuildingType() {
        return this.buildingType;
    }
    public void setBuildingStructure(String buildingStructure) {
        this.buildingStructure = buildingStructure;
    }
    
    public String getBuildingStructure() {
        return this.buildingStructure;
    }
    public void setRenovationCondition(String renovationCondition) {
        this.renovationCondition = renovationCondition;
    }
    
    public String getRenovationCondition() {
        return this.renovationCondition;
    }
    public void setEquippedWithSubway(String equippedWithSubway) {
        this.equippedWithSubway = equippedWithSubway;
    }
    
    public String getEquippedWithSubway() {
        return this.equippedWithSubway;
    }
    public void setYearOfPropertyRights(String yearOfPropertyRights) {
        this.yearOfPropertyRights = yearOfPropertyRights;
    }
    
    public String getYearOfPropertyRights() {
        return this.yearOfPropertyRights;
    }
    public void setHousePrice(String housePrice) {
        this.housePrice = housePrice;
    }
    
    public String getHousePrice() {
        return this.housePrice;
    }
    public void setListingTime(String listingTime) {
        this.listingTime = listingTime;
    }
    
    public String getListingTime() {
        return this.listingTime;
    }
    public void setYearOfHousing(String yearOfHousing) {
        this.yearOfHousing = yearOfHousing;
    }
    
    public String getYearOfHousing() {
        return this.yearOfHousing;
    }
    public void setMortgageInformation(String mortgageInformation) {
        this.mortgageInformation = mortgageInformation;
    }
    
    public String getMortgageInformation() {
        return this.mortgageInformation;
    }
    public void setLastTradingTime(String lastTradingTime) {
        this.lastTradingTime = lastTradingTime;
    }
    
    public String getLastTradingTime() {
        return this.lastTradingTime;
    }
    public void setUsageOfHouses(String usageOfHouses) {
        this.usageOfHouses = usageOfHouses;
    }
    
    public String getUsageOfHouses() {
        return this.usageOfHouses;
    }
    public void setHousingFiling(String housingFiling) {
        this.housingFiling = housingFiling;
    }
    
    public String getHousingFiling() {
        return this.housingFiling;
    }
    public void setPropertyRights(String propertyRights) {
        this.propertyRights = propertyRights;
    }
    
    public String getPropertyRights() {
        return this.propertyRights;
    }
    public void setIsNewHouse(String isNewHouse) {
        this.isNewHouse = isNewHouse;
    }
    
    public String getIsNewHouse() {
        return this.isNewHouse;
    }
    public void setHouseAllPrice(String houseAllPrice) {
        this.houseAllPrice = houseAllPrice;
    }
    
    public String getHouseAllPrice() {
        return this.houseAllPrice;
    }
    public void setInformationSources(String informationSources) {
        this.informationSources = informationSources;
    }
    
    public String getInformationSources() {
        return this.informationSources;
    }
    public void setInfoUpdateTime(Date infoUpdateTime) {
        this.infoUpdateTime = infoUpdateTime;
    }
    
    public Date getInfoUpdateTime() {
        return this.infoUpdateTime;
    }
    
    public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public void setTraffic(String traffic) {
        this.traffic = traffic;
    }
    
    public String getTraffic() {
        return this.traffic;
    }
    public void setKindergarten(String kindergarten) {
        this.kindergarten = kindergarten;
    }
    
    public String getKindergarten() {
        return this.kindergarten;
    }
    public void setSchool(String school) {
        this.school = school;
    }
    
    public String getSchool() {
        return this.school;
    }
    public void setPowerCenter(String powerCenter) {
        this.powerCenter = powerCenter;
    }
    
    public String getPowerCenter() {
        return this.powerCenter;
    }
    public void setHospital(String hospital) {
        this.hospital = hospital;
    }
    
    public String getHospital() {
        return this.hospital;
    }
    public void setBank(String bank) {
        this.bank = bank;
    }
    
    public String getBank() {
        return this.bank;
    }
    public void setPost(String post) {
        this.post = post;
    }
    
    public String getPost() {
        return this.post;
    }
    public void setOther(String other) {
        this.other = other;
    }
    
    public String getOther() {
        return this.other;
    }
    public void setHousePriceUnit(String housePriceUnit) {
        this.housePriceUnit = housePriceUnit;
    }
    
    public String getHousePriceUnit() {
        return this.housePriceUnit;
    }

}