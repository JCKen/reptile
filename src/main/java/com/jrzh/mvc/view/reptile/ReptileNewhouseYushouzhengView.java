package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileNewhouseYushouzhengView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 城市
     */
    private String city;
    /**
     * 区域
     */
    private String part;
    /**
     * 关联新房的id
     */
    private String houseId;
    /**
     * 预售证号
     */
    private String no;
    /**
     * 项目名
     */
    private String projectName;
    /**
     * 住宅预售套数
     */
    private String houseCount;
    /**
     * 住宅预售面积（平米）
     */
    private String houseArea;
    /**
     * 商业预售套数
     */
    private String businessCount;
    /**
     * 商业预售面积（平米）
     */
    private String businessArea;
    /**
     * 办公预售套数
     */
    private String officeCount;
    /**
     * 办公预售面积（平米）
     */
    private String officeArea;
    /**
     * 车位预售套数
     */
    private String carportCount;
    /**
     * 车位预售面积（平米）
     */
    private String carportArea;
    /**
     * 其他预售套数
     */
    private String otherCount;
    /**
     * 其他预售面积（平米）
     */
    private String otherArea;
    /**
     * 本期预售总套数
     */
    private String presellAllCount;
    /**
     * 本期预售总套数
     */
    private String presellAllArea;
    /**
     * 发证日期
     */
    private String openingDate;
    /**
     * 发证机关
     */
    private String office;
    /**
     * 是否抵押
     */
    private String hypothecate;
    /**
     * 地上面积(平米)
     */
    private String aboveArea;
    /**
     * 地下面积(平米)
     */
    private String underArea;
    /**
     * 有效期自
     */
    private String validityStart;
    /**
     * 有效期至
     */
    private String validityEnd;
    /**
     * 联系人
     */
    private String contacts;
    /**
     * 已建层数
     */
    private String buildedFloods;
    /**
     * 本期总单元套数
     */
    private String totalUnitCount;
    /**
     * 本期报建总面积
     */
    private String totalUnitArea;
    /**
     * 预售楼宇配套面积情况
     */
    private String matching;
    /**
     * 报建屋数
     */
    private String houseNumber;
    /**
     * 备注
     */
    private String desc;
    /**
     * 数据来源地址
     */
    private String url;
    /**
     * 数据来源
     */
    private String informationSources;

    public void setCity(String city) {
        this.city = city;
    }
    
    public String getCity() {
        return this.city;
    }
    public void setPart(String part) {
        this.part = part;
    }
    
    public String getPart() {
        return this.part;
    }
    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }
    
    public String getHouseId() {
        return this.houseId;
    }
    public void setNo(String no) {
        this.no = no;
    }
    
    public String getNo() {
        return this.no;
    }
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    
    public String getProjectName() {
        return this.projectName;
    }
    public void setHouseCount(String houseCount) {
        this.houseCount = houseCount;
    }
    
    public String getHouseCount() {
        return this.houseCount;
    }
    public void setHouseArea(String houseArea) {
        this.houseArea = houseArea;
    }
    
    public String getHouseArea() {
        return this.houseArea;
    }
    public void setBusinessCount(String businessCount) {
        this.businessCount = businessCount;
    }
    
    public String getBusinessCount() {
        return this.businessCount;
    }
    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea;
    }
    
    public String getBusinessArea() {
        return this.businessArea;
    }
    public void setOfficeCount(String officeCount) {
        this.officeCount = officeCount;
    }
    
    public String getOfficeCount() {
        return this.officeCount;
    }
    public void setOfficeArea(String officeArea) {
        this.officeArea = officeArea;
    }
    
    public String getOfficeArea() {
        return this.officeArea;
    }
    public void setCarportCount(String carportCount) {
        this.carportCount = carportCount;
    }
    
    public String getCarportCount() {
        return this.carportCount;
    }
    public void setCarportArea(String carportArea) {
        this.carportArea = carportArea;
    }
    
    public String getCarportArea() {
        return this.carportArea;
    }
    public void setOtherCount(String otherCount) {
        this.otherCount = otherCount;
    }
    
    public String getOtherCount() {
        return this.otherCount;
    }
    public void setOtherArea(String otherArea) {
        this.otherArea = otherArea;
    }
    
    public String getOtherArea() {
        return this.otherArea;
    }
    public void setPresellAllCount(String presellAllCount) {
        this.presellAllCount = presellAllCount;
    }
    
    public String getPresellAllCount() {
        return this.presellAllCount;
    }
    public void setPresellAllArea(String presellAllArea) {
        this.presellAllArea = presellAllArea;
    }
    
    public String getPresellAllArea() {
        return this.presellAllArea;
    }
    public void setOpeningDate(String openingDate) {
        this.openingDate = openingDate;
    }
    
    public String getOpeningDate() {
        return this.openingDate;
    }
    public void setOffice(String office) {
        this.office = office;
    }
    
    public String getOffice() {
        return this.office;
    }
    public void setHypothecate(String hypothecate) {
        this.hypothecate = hypothecate;
    }
    
    public String getHypothecate() {
        return this.hypothecate;
    }
    public void setAboveArea(String aboveArea) {
        this.aboveArea = aboveArea;
    }
    
    public String getAboveArea() {
        return this.aboveArea;
    }
    public void setUnderArea(String underArea) {
        this.underArea = underArea;
    }
    
    public String getUnderArea() {
        return this.underArea;
    }
    public void setValidityStart(String validityStart) {
        this.validityStart = validityStart;
    }
    
    public String getValidityStart() {
        return this.validityStart;
    }
    public void setValidityEnd(String validityEnd) {
        this.validityEnd = validityEnd;
    }
    
    public String getValidityEnd() {
        return this.validityEnd;
    }
    public void setContacts(String contacts) {
        this.contacts = contacts;
    }
    
    public String getContacts() {
        return this.contacts;
    }
    public void setBuildedFloods(String buildedFloods) {
        this.buildedFloods = buildedFloods;
    }
    
    public String getBuildedFloods() {
        return this.buildedFloods;
    }
    public void setTotalUnitCount(String totalUnitCount) {
        this.totalUnitCount = totalUnitCount;
    }
    
    public String getTotalUnitCount() {
        return this.totalUnitCount;
    }
    public void setTotalUnitArea(String totalUnitArea) {
        this.totalUnitArea = totalUnitArea;
    }
    
    public String getTotalUnitArea() {
        return this.totalUnitArea;
    }
    public void setMatching(String matching) {
        this.matching = matching;
    }
    
    public String getMatching() {
        return this.matching;
    }
    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }
    
    public String getHouseNumber() {
        return this.houseNumber;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public String getDesc() {
        return this.desc;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getUrl() {
        return this.url;
    }
    public void setInformationSources(String informationSources) {
        this.informationSources = informationSources;
    }
    
    public String getInformationSources() {
        return this.informationSources;
    }

}