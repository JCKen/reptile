/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.mvc.view.reptile.shanghaiAreaBean;

/**
 * Auto-generated: 2019-01-11 16:23:53
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class AreaInfoRootBean {

    private int flag;
    private AreaInfoData data;
    private int total;
    private String msg;
    public void setFlag(int flag) {
         this.flag = flag;
     }
     public int getFlag() {
         return flag;
     }

    public void setData(AreaInfoData data) {
         this.data = data;
     }
     public AreaInfoData getData() {
         return data;
     }

    public void setTotal(int total) {
         this.total = total;
     }
     public int getTotal() {
         return total;
     }

    public void setMsg(String msg) {
         this.msg = msg;
     }
     public String getMsg() {
         return msg;
     }
	@Override
	public String toString() {
		return "AreaInfoRootBean [flag=" + flag + ", data=" + data + ", total="
				+ total + ", msg=" + msg + "]";
	}

}