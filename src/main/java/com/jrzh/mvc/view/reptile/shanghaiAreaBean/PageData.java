/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.mvc.view.reptile.shanghaiAreaBean;
import java.util.List;

/**
 * Auto-generated: 2019-01-11 16:30:59
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class PageData {

    private List<PageUnit> list;
    private int total;
    public void setList(List<PageUnit> list) {
         this.list = list;
     }
     public List<PageUnit> getList() {
         return list;
     }

    public void setTotal(int total) {
         this.total = total;
     }
     public int getTotal() {
         return total;
     }
	@Override
	public String toString() {
		return "PageData [list=" + list + ", total=" + total + "]";
	}

}