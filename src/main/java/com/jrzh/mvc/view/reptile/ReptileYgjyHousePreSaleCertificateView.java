package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileYgjyHousePreSaleCertificateView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 房子ID
     */
    private String houseId;
    /**
     * 住宅套数
     */
    private String houseNumberSet;
    /**
     * 住宅面积
     */
    private String houseArea;
    /**
     * 商业套数
     */
    private String businessNumberSet;
    /**
     * 商业面积
     */
    private String businessArea;
    /**
     * 办公套数
     */
    private String officeNumberSet;
    /**
     * 办公面积
     */
    private String officeArea;
    /**
     * 车位套数
     */
    private String parkingNumberSet;
    /**
     * 车位面积
     */
    private String parkingArea;
    /**
     * 其他套数
     */
    private String otherNumberSet;
    /**
     * 其他面积
     */
    private String otherArea;
    /**
     * 预字第
     */
    private String preWord;
    /**
     * 预售幢数
     */
    private String preSaleNumber;
    /**
     * 报建屋数
     */
    private String houseBuiltNumber;
    /**
     * 已建层数
     */
    private String layerBuiltNumber;
    /**
     * 本期报建总面积
     */
    private String totalAreaReport;
    /**
     * 地上面积
     */
    private String aboveGroundArea;
    /**
     * 地下面积
     */
    private String undergroundArea;
    /**
     * 本期预售总建筑面积
     */
    private String preSaleGrossFloorArea;
    /**
     * 预售房屋占用土地是否抵押
     */
    private String preSaleHouseIsMortgaged;
    /**
     * 预售楼宇配套面积情况
     */
    private String preSaleBuildingSupportingArea;

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }
    
    public String getHouseId() {
        return this.houseId;
    }
    public void setHouseNumberSet(String houseNumberSet) {
        this.houseNumberSet = houseNumberSet;
    }
    
    public String getHouseNumberSet() {
        return this.houseNumberSet;
    }
    public void setHouseArea(String houseArea) {
        this.houseArea = houseArea;
    }
    
    public String getHouseArea() {
        return this.houseArea;
    }
    public void setBusinessNumberSet(String businessNumberSet) {
        this.businessNumberSet = businessNumberSet;
    }
    
    public String getBusinessNumberSet() {
        return this.businessNumberSet;
    }
    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea;
    }
    
    public String getBusinessArea() {
        return this.businessArea;
    }
    public void setOfficeNumberSet(String officeNumberSet) {
        this.officeNumberSet = officeNumberSet;
    }
    
    public String getOfficeNumberSet() {
        return this.officeNumberSet;
    }
    public void setOfficeArea(String officeArea) {
        this.officeArea = officeArea;
    }
    
    public String getOfficeArea() {
        return this.officeArea;
    }
    public void setParkingNumberSet(String parkingNumberSet) {
        this.parkingNumberSet = parkingNumberSet;
    }
    
    public String getParkingNumberSet() {
        return this.parkingNumberSet;
    }
    public void setParkingArea(String parkingArea) {
        this.parkingArea = parkingArea;
    }
    
    public String getParkingArea() {
        return this.parkingArea;
    }
    public void setOtherNumberSet(String otherNumberSet) {
        this.otherNumberSet = otherNumberSet;
    }
    
    public String getOtherNumberSet() {
        return this.otherNumberSet;
    }
    public void setOtherArea(String otherArea) {
        this.otherArea = otherArea;
    }
    
    public String getOtherArea() {
        return this.otherArea;
    }
    public void setPreWord(String preWord) {
        this.preWord = preWord;
    }
    
    public String getPreWord() {
        return this.preWord;
    }
    public void setPreSaleNumber(String preSaleNumber) {
        this.preSaleNumber = preSaleNumber;
    }
    
    public String getPreSaleNumber() {
        return this.preSaleNumber;
    }
    public void setHouseBuiltNumber(String houseBuiltNumber) {
        this.houseBuiltNumber = houseBuiltNumber;
    }
    
    public String getHouseBuiltNumber() {
        return this.houseBuiltNumber;
    }
    public void setLayerBuiltNumber(String layerBuiltNumber) {
        this.layerBuiltNumber = layerBuiltNumber;
    }
    
    public String getLayerBuiltNumber() {
        return this.layerBuiltNumber;
    }
    public void setTotalAreaReport(String totalAreaReport) {
        this.totalAreaReport = totalAreaReport;
    }
    
    public String getTotalAreaReport() {
        return this.totalAreaReport;
    }
    public void setAboveGroundArea(String aboveGroundArea) {
        this.aboveGroundArea = aboveGroundArea;
    }
    
    public String getAboveGroundArea() {
        return this.aboveGroundArea;
    }
    public void setUndergroundArea(String undergroundArea) {
        this.undergroundArea = undergroundArea;
    }
    
    public String getUndergroundArea() {
        return this.undergroundArea;
    }
    public void setPreSaleGrossFloorArea(String preSaleGrossFloorArea) {
        this.preSaleGrossFloorArea = preSaleGrossFloorArea;
    }
    
    public String getPreSaleGrossFloorArea() {
        return this.preSaleGrossFloorArea;
    }
    public void setPreSaleHouseIsMortgaged(String preSaleHouseIsMortgaged) {
        this.preSaleHouseIsMortgaged = preSaleHouseIsMortgaged;
    }
    
    public String getPreSaleHouseIsMortgaged() {
        return this.preSaleHouseIsMortgaged;
    }
    public void setPreSaleBuildingSupportingArea(String preSaleBuildingSupportingArea) {
        this.preSaleBuildingSupportingArea = preSaleBuildingSupportingArea;
    }
    
    public String getPreSaleBuildingSupportingArea() {
        return this.preSaleBuildingSupportingArea;
    }

}