/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.mvc.view.reptile.shanghaiAreaBean;

/**
 * Auto-generated: 2019-01-11 16:23:53
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class AreaInfoData {

    private String churnx;
    private String mianj;
    private String blockstate;
    private String jiaoylx;
    private String gg_id;
    private String bz;
    private String dz;
    private String zuol;
    private String flwj;
    private String block;
    private String nz;
    private String xz;
    private String url;
    private String hxjy;
    private String spurl;
    private String xianzhuang;
    private String rongjl;
    private String cjqk;
    private String ismark;
    private String zongbr;
    private String chengjrq;
    private String dikmc;
    private String zongbj;
    private String guihyt;
    
    public void setChurnx(String churnx) {
         this.churnx = churnx;
     }
     public String getChurnx() {
         return churnx;
     }

    public void setMianj(String mianj) {
         this.mianj = mianj;
     }
     public String getMianj() {
         return mianj;
     }

    public void setBlockstate(String blockstate) {
         this.blockstate = blockstate;
     }
     public String getBlockstate() {
         return blockstate;
     }

    public void setJiaoylx(String jiaoylx) {
         this.jiaoylx = jiaoylx;
     }
     public String getJiaoylx() {
         return jiaoylx;
     }

    public void setGg_id(String gg_id) {
         this.gg_id = gg_id;
     }
     public String getGg_id() {
         return gg_id;
     }

    public void setBz(String bz) {
         this.bz = bz;
     }
     public String getBz() {
         return bz;
     }

    public void setDz(String dz) {
         this.dz = dz;
     }
     public String getDz() {
         return dz;
     }

    public void setZuol(String zuol) {
         this.zuol = zuol;
     }
     public String getZuol() {
         return zuol;
     }

    public void setFlwj(String flwj) {
         this.flwj = flwj;
     }
     public String getFlwj() {
         return flwj;
     }

    public void setBlock(String block) {
         this.block = block;
     }
     public String getBlock() {
         return block;
     }

    public void setNz(String nz) {
         this.nz = nz;
     }
     public String getNz() {
         return nz;
     }

    public void setXz(String xz) {
         this.xz = xz;
     }
     public String getXz() {
         return xz;
     }

    public void setUrl(String url) {
         this.url = url;
     }
     public String getUrl() {
         return url;
     }

    public void setHxjy(String hxjy) {
         this.hxjy = hxjy;
     }
     public String getHxjy() {
         return hxjy;
     }

    public void setSpurl(String spurl) {
         this.spurl = spurl;
     }
     public String getSpurl() {
         return spurl;
     }

    public void setXianzhuang(String xianzhuang) {
         this.xianzhuang = xianzhuang;
     }
     public String getXianzhuang() {
         return xianzhuang;
     }

    public void setRongjl(String rongjl) {
         this.rongjl = rongjl;
     }
     public String getRongjl() {
         return rongjl;
     }

    public void setCjqk(String cjqk) {
         this.cjqk = cjqk;
     }
     public String getCjqk() {
         return cjqk;
     }

    public void setIsmark(String ismark) {
         this.ismark = ismark;
     }
     public String getIsmark() {
         return ismark;
     }

    public void setZongbr(String zongbr) {
         this.zongbr = zongbr;
     }
     public String getZongbr() {
         return zongbr;
     }

    public void setChengjrq(String chengjrq) {
         this.chengjrq = chengjrq;
     }
     public String getChengjrq() {
         return chengjrq;
     }

    public void setDikmc(String dikmc) {
         this.dikmc = dikmc;
     }
     public String getDikmc() {
         return dikmc;
     }

    public void setZongbj(String zongbj) {
         this.zongbj = zongbj;
     }
     public String getZongbj() {
         return zongbj;
     }

    public void setGuihyt(String guihyt) {
         this.guihyt = guihyt;
     }
     public String getGuihyt() {
         return guihyt;
     }
	@Override
	public String toString() {
		return "AreaInfoData [churnx=" + churnx + ", mianj=" + mianj
				+ ", blockstate=" + blockstate + ", jiaoylx=" + jiaoylx
				+ ", gg_id=" + gg_id + ", bz=" + bz + ", dz=" + dz + ", zuol="
				+ zuol + ", flwj=" + flwj + ", block=" + block + ", nz=" + nz
				+ ", xz=" + xz + ", url=" + url + ", hxjy=" + hxjy + ", spurl="
				+ spurl + ", xianzhuang=" + xianzhuang + ", rongjl=" + rongjl
				+ ", cjqk=" + cjqk + ", ismark=" + ismark + ", zongbr="
				+ zongbr + ", chengjrq=" + chengjrq + ", dikmc=" + dikmc
				+ ", zongbj=" + zongbj + ", guihyt=" + guihyt + "]";
	}

}