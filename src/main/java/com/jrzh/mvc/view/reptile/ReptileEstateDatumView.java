package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileEstateDatumView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 时间
     */
    private String time;
    /**
     * 指标编号
     */
    private String code;
    /**
     * 指标值
     */
    private String data;

    public void setTime(String time) {
        this.time = time;
    }
    
    public String getTime() {
        return this.time;
    }
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCode() {
        return this.code;
    }
    public void setData(String data) {
        this.data = data;
    }
    
    public String getData() {
        return this.data;
    }

}