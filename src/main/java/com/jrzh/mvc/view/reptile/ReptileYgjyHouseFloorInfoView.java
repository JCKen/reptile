package com.jrzh.mvc.view.reptile;

import com.jrzh.framework.base.view.GeneralView;

public class ReptileYgjyHouseFloorInfoView extends GeneralView {
	private static final long serialVersionUID = 1L;
    
    /**
     * 关联房屋信息id
     */
    private String houseId;
    /**
     * 楼层名称
     */
    private String floorName;
    /**
     * 阳光家缘id（用于查询楼层具体信息）
     */
    private String buildingId;
    /**
     * 爬取状态
     */
    private String status;
    /**
     * 上海房管局开盘单位ID
     */
    private String shStartId;
    /**
     * 深圳房管局开盘单位ID
     */
    private String szStartId;
    /**
     * 北京房管局开盘单位ID
     */
    private String bjStartId;
    /**
     * 广州房管局开盘单位ID
     */
    private String gzBuildingId;
    
	public String getGzBuildingId() {
		return gzBuildingId;
	}

	public void setGzBuildingId(String gzBuildingId) {
		this.gzBuildingId = gzBuildingId;
	}

	public String getShStartId() {
		return shStartId;
	}

	public void setShStartId(String shStartId) {
		this.shStartId = shStartId;
	}

	public String getSzStartId() {
		return szStartId;
	}

	public void setSzStartId(String szStartId) {
		this.szStartId = szStartId;
	}

	public String getBjStartId() {
		return bjStartId;
	}

	public void setBjStartId(String bjStartId) {
		this.bjStartId = bjStartId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setHouseId(String houseId) {
        this.houseId = houseId;
    }
    
    public String getHouseId() {
        return this.houseId;
    }
    public void setFloorName(String floorName) {
        this.floorName = floorName;
    }
    
    public String getFloorName() {
        return this.floorName;
    }
    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }
    
    public String getBuildingId() {
        return this.buildingId;
    }

}