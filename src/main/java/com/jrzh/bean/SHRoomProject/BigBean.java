package com.jrzh.bean.SHRoomProject;

import java.util.LinkedHashMap;
import java.util.List;

public class BigBean {

	private LinkedHashMap<String, List<JsonRoomBean>> moreInfoList;

	public LinkedHashMap<String, List<JsonRoomBean>> getMoreInfoList() {
		return moreInfoList;
	}

	public void setMoreInfoList(LinkedHashMap<String, List<JsonRoomBean>> moreInfoList) {
		this.moreInfoList = moreInfoList;
	}
	
}
