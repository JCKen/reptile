/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.bean.SHRoomProject;

/**
 * Auto-generated: 2019-01-14 15:56:23
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class JsonRoomBean {

    private String home_descent;
    private String pro_area;
    private String priv_flarea;
    private String cellar_area;
    private String building_id;
    private String data_state;
    private String state;
    private String flarea;
    private String plan_priv_flarea;
    private String eowner;
    private String building_type;
    private String plan_cellar_area;
    private String priv_area;
    private String land_type;
    private String co_flarea;
    private String plan_ex_flarea;
    private String floor_name;
    private String vocation;
    private String co_area;
    private String area;
    private String house_type;
    private String start_id;
    private String ex_area;
    private String plan_ratial;
    private String note;
    private String room_number;
    private String last_modi_time;
    private String adv_area;
    private String land_use;
    private String plan_co_flarea;
    private String status;
    private String right_of_owner;
    private String flat_style;
    private String ex_flarea;
    private String house_number;
    private String manual_status;
    private String ratial;
    private String floor;
    private String flat_type;
    private String state_date;
    private String land_use2;
    private String land_descent;
    private String pre_plan_id;
    private String sell_plan_id;
    private String house_id;
    private String districtid;
    private String plan_flarea;
    private String start_date;
	public String getHome_descent() {
		return home_descent;
	}
	public void setHome_descent(String home_descent) {
		this.home_descent = home_descent;
	}
	public String getPro_area() {
		return pro_area;
	}
	public void setPro_area(String pro_area) {
		this.pro_area = pro_area;
	}
	public String getPriv_flarea() {
		return priv_flarea;
	}
	public void setPriv_flarea(String priv_flarea) {
		this.priv_flarea = priv_flarea;
	}
	public String getCellar_area() {
		return cellar_area;
	}
	public void setCellar_area(String cellar_area) {
		this.cellar_area = cellar_area;
	}
	public String getBuilding_id() {
		return building_id;
	}
	public void setBuilding_id(String building_id) {
		this.building_id = building_id;
	}
	public String getData_state() {
		return data_state;
	}
	public void setData_state(String data_state) {
		this.data_state = data_state;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getFlarea() {
		return flarea;
	}
	public void setFlarea(String flarea) {
		this.flarea = flarea;
	}
	public String getPlan_priv_flarea() {
		return plan_priv_flarea;
	}
	public void setPlan_priv_flarea(String plan_priv_flarea) {
		this.plan_priv_flarea = plan_priv_flarea;
	}
	public String getEowner() {
		return eowner;
	}
	public void setEowner(String eowner) {
		this.eowner = eowner;
	}
	public String getBuilding_type() {
		return building_type;
	}
	public void setBuilding_type(String building_type) {
		this.building_type = building_type;
	}
	public String getPlan_cellar_area() {
		return plan_cellar_area;
	}
	public void setPlan_cellar_area(String plan_cellar_area) {
		this.plan_cellar_area = plan_cellar_area;
	}
	public String getPriv_area() {
		return priv_area;
	}
	public void setPriv_area(String priv_area) {
		this.priv_area = priv_area;
	}
	public String getLand_type() {
		return land_type;
	}
	public void setLand_type(String land_type) {
		this.land_type = land_type;
	}
	public String getCo_flarea() {
		return co_flarea;
	}
	public void setCo_flarea(String co_flarea) {
		this.co_flarea = co_flarea;
	}
	public String getPlan_ex_flarea() {
		return plan_ex_flarea;
	}
	public void setPlan_ex_flarea(String plan_ex_flarea) {
		this.plan_ex_flarea = plan_ex_flarea;
	}
	public String getFloor_name() {
		return floor_name;
	}
	public void setFloor_name(String floor_name) {
		this.floor_name = floor_name;
	}
	public String getVocation() {
		return vocation;
	}
	public void setVocation(String vocation) {
		this.vocation = vocation;
	}
	public String getCo_area() {
		return co_area;
	}
	public void setCo_area(String co_area) {
		this.co_area = co_area;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getHouse_type() {
		return house_type;
	}
	public void setHouse_type(String house_type) {
		this.house_type = house_type;
	}
	public String getStart_id() {
		return start_id;
	}
	public void setStart_id(String start_id) {
		this.start_id = start_id;
	}
	public String getEx_area() {
		return ex_area;
	}
	public void setEx_area(String ex_area) {
		this.ex_area = ex_area;
	}
	public String getPlan_ratial() {
		return plan_ratial;
	}
	public void setPlan_ratial(String plan_ratial) {
		this.plan_ratial = plan_ratial;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getRoom_number() {
		return room_number;
	}
	public void setRoom_number(String room_number) {
		this.room_number = room_number;
	}
	public String getLast_modi_time() {
		return last_modi_time;
	}
	public void setLast_modi_time(String last_modi_time) {
		this.last_modi_time = last_modi_time;
	}
	public String getAdv_area() {
		return adv_area;
	}
	public void setAdv_area(String adv_area) {
		this.adv_area = adv_area;
	}
	public String getLand_use() {
		return land_use;
	}
	public void setLand_use(String land_use) {
		this.land_use = land_use;
	}
	public String getPlan_co_flarea() {
		return plan_co_flarea;
	}
	public void setPlan_co_flarea(String plan_co_flarea) {
		this.plan_co_flarea = plan_co_flarea;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRight_of_owner() {
		return right_of_owner;
	}
	public void setRight_of_owner(String right_of_owner) {
		this.right_of_owner = right_of_owner;
	}
	public String getFlat_style() {
		return flat_style;
	}
	public void setFlat_style(String flat_style) {
		this.flat_style = flat_style;
	}
	public String getEx_flarea() {
		return ex_flarea;
	}
	public void setEx_flarea(String ex_flarea) {
		this.ex_flarea = ex_flarea;
	}
	public String getHouse_number() {
		return house_number;
	}
	public void setHouse_number(String house_number) {
		this.house_number = house_number;
	}
	public String getManual_status() {
		return manual_status;
	}
	public void setManual_status(String manual_status) {
		this.manual_status = manual_status;
	}
	public String getRatial() {
		return ratial;
	}
	public void setRatial(String ratial) {
		this.ratial = ratial;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getFlat_type() {
		return flat_type;
	}
	public void setFlat_type(String flat_type) {
		this.flat_type = flat_type;
	}
	public String getState_date() {
		return state_date;
	}
	public void setState_date(String state_date) {
		this.state_date = state_date;
	}
	public String getLand_use2() {
		return land_use2;
	}
	public void setLand_use2(String land_use2) {
		this.land_use2 = land_use2;
	}
	public String getLand_descent() {
		return land_descent;
	}
	public void setLand_descent(String land_descent) {
		this.land_descent = land_descent;
	}
	public String getPre_plan_id() {
		return pre_plan_id;
	}
	public void setPre_plan_id(String pre_plan_id) {
		this.pre_plan_id = pre_plan_id;
	}
	public String getSell_plan_id() {
		return sell_plan_id;
	}
	public void setSell_plan_id(String sell_plan_id) {
		this.sell_plan_id = sell_plan_id;
	}
	public String getHouse_id() {
		return house_id;
	}
	public void setHouse_id(String house_id) {
		this.house_id = house_id;
	}
	public String getDistrictid() {
		return districtid;
	}
	public void setDistrictid(String districtid) {
		this.districtid = districtid;
	}
	public String getPlan_flarea() {
		return plan_flarea;
	}
	public void setPlan_flarea(String plan_flarea) {
		this.plan_flarea = plan_flarea;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

}