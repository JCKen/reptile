/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.bean.SHRoomProject;

/**
 * Auto-generated: 2019-01-14 15:56:23
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Last_modi_time {

    private int date;
    private int day;
    private int hours;
    private int minutes;
    private int month;
    private int nanos;
    private int seconds;
    private long time;
    private int timezoneOffset;
    private int year;
    public void setDate(int date) {
         this.date = date;
     }
     public int getDate() {
         return date;
     }

    public void setDay(int day) {
         this.day = day;
     }
     public int getDay() {
         return day;
     }

    public void setHours(int hours) {
         this.hours = hours;
     }
     public int getHours() {
         return hours;
     }

    public void setMinutes(int minutes) {
         this.minutes = minutes;
     }
     public int getMinutes() {
         return minutes;
     }

    public void setMonth(int month) {
         this.month = month;
     }
     public int getMonth() {
         return month;
     }

    public void setNanos(int nanos) {
         this.nanos = nanos;
     }
     public int getNanos() {
         return nanos;
     }

    public void setSeconds(int seconds) {
         this.seconds = seconds;
     }
     public int getSeconds() {
         return seconds;
     }

    public void setTime(long time) {
         this.time = time;
     }
     public long getTime() {
         return time;
     }

    public void setTimezoneOffset(int timezoneOffset) {
         this.timezoneOffset = timezoneOffset;
     }
     public int getTimezoneOffset() {
         return timezoneOffset;
     }

    public void setYear(int year) {
         this.year = year;
     }
     public int getYear() {
         return year;
     }

}