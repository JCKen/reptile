package com.jrzh.bean.priceTrend;

import java.util.List;
import java.util.Map;

import com.jrzh.framework.base.BaseBean;

public class PriceBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6540019729162054293L;

	private List<String> times;
	private Map<String, List<String>> datas;

	public List<String> getTimes() {
		return times;
	}

	public void setTimes(List<String> times) {
		this.times = times;
	}

	public Map<String, List<String>> getDatas() {
		return datas;
	}

	public void setDatas(Map<String, List<String>> datas) {
		this.datas = datas;
	}

}
