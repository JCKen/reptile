/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.bean.sz;
import java.util.List;

/**
 * Auto-generated: 2019-03-04 10:41:38
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Data {

    private List<String> date;
    private String result;
    private List<String> esfUnitPrice;
    private List<String> ysfDealArea;
    private List<String> ysfUnitPrice;
    private List<String> ysfTotalTs;
    private List<String> esfDealArea;
    private List<String> bigEventCont;
    private List<String> esfTotalTs;
	public List<String> getDate() {
		return date;
	}
	public void setDate(List<String> date) {
		this.date = date;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public List<String> getEsfUnitPrice() {
		return esfUnitPrice;
	}
	public void setEsfUnitPrice(List<String> esfUnitPrice) {
		this.esfUnitPrice = esfUnitPrice;
	}
	public List<String> getYsfDealArea() {
		return ysfDealArea;
	}
	public void setYsfDealArea(List<String> ysfDealArea) {
		this.ysfDealArea = ysfDealArea;
	}
	public List<String> getYsfUnitPrice() {
		return ysfUnitPrice;
	}
	public void setYsfUnitPrice(List<String> ysfUnitPrice) {
		this.ysfUnitPrice = ysfUnitPrice;
	}
	public List<String> getYsfTotalTs() {
		return ysfTotalTs;
	}
	public void setYsfTotalTs(List<String> ysfTotalTs) {
		this.ysfTotalTs = ysfTotalTs;
	}
	public List<String> getEsfDealArea() {
		return esfDealArea;
	}
	public void setEsfDealArea(List<String> esfDealArea) {
		this.esfDealArea = esfDealArea;
	}
	public List<String> getBigEventCont() {
		return bigEventCont;
	}
	public void setBigEventCont(List<String> bigEventCont) {
		this.bigEventCont = bigEventCont;
	}
	public List<String> getEsfTotalTs() {
		return esfTotalTs;
	}
	public void setEsfTotalTs(List<String> esfTotalTs) {
		this.esfTotalTs = esfTotalTs;
	}
	
}