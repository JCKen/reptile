package com.jrzh.bean.macroscopic.estate;

/**
 * 数据 bean
 *
 */

public class DataBean {
	
	private String data;
	private String dotcount;
	private String hasdata;
	private String strdata;
	
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getDotcount() {
		return dotcount;
	}
	public void setDotcount(String dotcount) {
		this.dotcount = dotcount;
	}
	public String getHasdata() {
		return hasdata;
	}
	public void setHasdata(String hasdata) {
		this.hasdata = hasdata;
	}
	public String getStrdata() {
		return strdata;
	}
	public void setStrdata(String strdata) {
		this.strdata = strdata;
	}

	
}
