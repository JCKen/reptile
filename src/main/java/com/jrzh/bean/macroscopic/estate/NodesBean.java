package com.jrzh.bean.macroscopic.estate;

import com.jrzh.framework.base.BaseBean;

public class NodesBean extends BaseBean {
	private static final long serialVersionUID = -4854117887455172718L;
	
	/**
	 *  指标名称 或者 时间
	 */
	private String cname;
	/**
	 *  这个指标的编号
	 */
	private String code;
	private String  dotcount;
	/**
	 * 对这个指标的说明
	 */
	private String exp;
	private String ifshowcode;
	private String memo;
	private String name;
	private String nodesort;
	private String tag;
	/**
	 * 指标的单位
	 */
	private String unit;
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getExp() {
		return exp;
	}
	public void setExp(String exp) {
		this.exp = exp;
	}

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getDotcount() {
		return dotcount;
	}
	public void setDotcount(String dotcount) {
		this.dotcount = dotcount;
	}
	public String getIfshowcode() {
		return ifshowcode;
	}
	public void setIfshowcode(String ifshowcode) {
		this.ifshowcode = ifshowcode;
	}
	public String getNodesort() {
		return nodesort;
	}
	public void setNodesort(String nodesort) {
		this.nodesort = nodesort;
	}
	
	
}
