package com.jrzh.bean.macroscopic.estate;

import java.util.List;

public class WdNodeBean {
	
	/**
	 * 编号
	 */
	private String wdcode;
	/**
	 * 名称
	 */
	private String wdname;
	/**
	 *  node类
	 */
	private List<NodesBean> nodes;
	
	public String getWdcode() {
		return wdcode;
	}
	public void setWdcode(String wdcode) {
		this.wdcode = wdcode;
	}
	public String getWdname() {
		return wdname;
	}
	public void setWdname(String wdname) {
		this.wdname = wdname;
	}
	public List<NodesBean> getNodes() {
		return nodes;
	}
	public void setNodes(List<NodesBean> nodes) {
		this.nodes = nodes;
	}
	
	
	
	
	
}
