package com.jrzh.bean.macroscopic.estate;

public class ParentBean {
	
	private ReturnData returndata;
	private String returncode;
	
	
	
	public String getReturncode() {
		return returncode;
	}
	public void setReturncode(String returncode) {
		this.returncode = returncode;
	}
	public ReturnData getReturndata() {
		return returndata;
	}
	public void setReturndata(ReturnData returndata) {
		this.returndata = returndata;
	}
	
}
