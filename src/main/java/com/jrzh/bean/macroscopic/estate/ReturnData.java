package com.jrzh.bean.macroscopic.estate;

import java.util.List;

public class ReturnData {
	
	/**
	 * 数据集合
	 */
	private List<DataNodeBean> datanodes;
	/**
	 * 指标集合
	 */
	private List<WdNodeBean> wdnodes;

	public List<DataNodeBean> getDatanodes() {
		return datanodes;
	}
	public void setDatanodes(List<DataNodeBean> datanodes) {
		this.datanodes = datanodes;
	}
	public List<WdNodeBean> getWdnodes() {
		return wdnodes;
	}
	public void setWdnodes(List<WdNodeBean> wdnodes) {
		this.wdnodes = wdnodes;
	}
	
	
}
