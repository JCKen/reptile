package com.jrzh.bean.macroscopic.estate;

public class WdBean {
	
	private String valuecode;
	private String wdcode;
	
	public String getValuecode() {
		return valuecode;
	}
	public void setValuecode(String valuecode) {
		this.valuecode = valuecode;
	}
	public String getWdcode() {
		return wdcode;
	}
	public void setWdcode(String wdcode) {
		this.wdcode = wdcode;
	}
	
	
}
