package com.jrzh.bean.macroscopic.estate;

import java.util.List;

public class DataNodeBean {
	
	private String code;
	private DataBean data;
	private List<WdBean> wds;
	
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public DataBean getData() {
		return data;
	}
	public void setData(DataBean data) {
		this.data = data;
	}
	public List<WdBean> getWds() {
		return wds;
	}
	public void setWds(List<WdBean> wds) {
		this.wds = wds;
	}
	
	
	
}
