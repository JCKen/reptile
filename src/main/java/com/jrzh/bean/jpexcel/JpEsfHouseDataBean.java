package com.jrzh.bean.jpexcel;

import java.util.List;

import com.jrzh.framework.base.BaseBean;
import com.jrzh.mvc.view.reptile.ReptileTemplateInfoView;

public class JpEsfHouseDataBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2629247393733886140L;
	private List<ReptileTemplateInfoView> esfViews; // 导出的二手房数据
	private String esfHouseRatio; // 导出的二手房比例
	private String esfHouseAmt; // 导出的二手房价格
	public List<ReptileTemplateInfoView> getEsfViews() {
		return esfViews;
	}
	public void setEsfViews(List<ReptileTemplateInfoView> esfViews) {
		this.esfViews = esfViews;
	}
	public String getEsfHouseRatio() {
		return esfHouseRatio;
	}
	public void setEsfHouseRatio(String esfHouseRatio) {
		this.esfHouseRatio = esfHouseRatio;
	}
	public String getEsfHouseAmt() {
		return esfHouseAmt;
	}
	public void setEsfHouseAmt(String esfHouseAmt) {
		this.esfHouseAmt = esfHouseAmt;
	}
	
}
