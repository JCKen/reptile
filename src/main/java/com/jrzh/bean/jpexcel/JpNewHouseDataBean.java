package com.jrzh.bean.jpexcel;

import java.util.List;

import com.jrzh.framework.base.BaseBean;
import com.jrzh.mvc.view.reptile.ReptileTemplateInfoView;

public class JpNewHouseDataBean extends BaseBean {

	/**
	 * Jp导出一手房数据bean
	 */
	private static final long serialVersionUID = 5436656691303906345L;
	private List<ReptileTemplateInfoView> newViews; // 导出的一手房数据
	private String newHouseRatio; // 导出的一手房比例
	private String newHouseAmt; // 导出的一手房价格

	public List<ReptileTemplateInfoView> getNewViews() {
		return newViews;
	}

	public void setNewViews(List<ReptileTemplateInfoView> newViews) {
		this.newViews = newViews;
	}

	public String getNewHouseRatio() {
		return newHouseRatio;
	}

	public void setNewHouseRatio(String newHouseRatio) {
		this.newHouseRatio = newHouseRatio;
	}

	public String getNewHouseAmt() {
		return newHouseAmt;
	}

	public void setNewHouseAmt(String newHouseAmt) {
		this.newHouseAmt = newHouseAmt;
	}

}
