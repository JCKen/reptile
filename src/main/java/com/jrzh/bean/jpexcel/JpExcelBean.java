package com.jrzh.bean.jpexcel;

import com.jrzh.framework.base.BaseBean;

public class JpExcelBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5239823126853560576L;

	private String amt;		//结果金额
	private JpEsfHouseDataBean EsfHouseData;		//二手房数据
	private JpNewHouseDataBean newHouseData;		//新房数据
	private JpTudiDataBean tudiData;		//土地数据
	

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public JpEsfHouseDataBean getEsfHouseData() {
		return EsfHouseData;
	}

	public void setEsfHouseData(JpEsfHouseDataBean esfHouseData) {
		EsfHouseData = esfHouseData;
	}

	public JpNewHouseDataBean getNewHouseData() {
		return newHouseData;
	}

	public void setNewHouseData(JpNewHouseDataBean newHouseData) {
		this.newHouseData = newHouseData;
	}

	public JpTudiDataBean getTudiData() {
		return tudiData;
	}

	public void setTudiData(JpTudiDataBean tudiData) {
		this.tudiData = tudiData;
	}

}
