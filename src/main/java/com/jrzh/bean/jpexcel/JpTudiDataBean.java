package com.jrzh.bean.jpexcel;

import java.util.List;

import com.jrzh.framework.base.BaseBean;
import com.jrzh.mvc.view.reptile.ReptileTemplateInfoView;

public class JpTudiDataBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6750248743453349015L;
	
	private List<ReptileTemplateInfoView> tudiViews; // 导出的土地数据
	private String tudiHouseRatio; // 导出的土地比例
	private String  tudiHouseAmt; // 导出的土地价格
	public List<ReptileTemplateInfoView> getTudiViews() {
		return tudiViews;
	}
	public void setTudiViews(List<ReptileTemplateInfoView> tudiViews) {
		this.tudiViews = tudiViews;
	}
	public String getTudiHouseRatio() {
		return tudiHouseRatio;
	}
	public void setTudiHouseRatio(String tudiHouseRatio) {
		this.tudiHouseRatio = tudiHouseRatio;
	}
	public String getTudiHouseAmt() {
		return tudiHouseAmt;
	}
	public void setTudiHouseAmt(String tudiHouseAmt) {
		this.tudiHouseAmt = tudiHouseAmt;
	}
	
	
}
