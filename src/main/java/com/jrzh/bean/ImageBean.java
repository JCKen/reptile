package com.jrzh.bean;

import java.io.Serializable;

public class ImageBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3453326338095162867L;
	private String imageKey;
	private String imageData;

	public String getImageKey() {
		return imageKey;
	}

	public void setImageKey(String imageKey) {
		this.imageKey = imageKey;
	}

	public String getImageData() {
		return imageData;
	}

	public void setImageData(String imageData) {
		this.imageData = imageData;
	}

}
