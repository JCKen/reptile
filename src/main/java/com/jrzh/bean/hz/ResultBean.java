package com.jrzh.bean.hz;

import java.util.List;

public class ResultBean {
	
	private List<DataBean> data;

	public List<DataBean> getData() {
		return data;
	}

	public void setData(List<DataBean> data) {
		this.data = data;
	}
	
}
