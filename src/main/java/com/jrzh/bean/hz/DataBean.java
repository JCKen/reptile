package com.jrzh.bean.hz;

public class DataBean {
	
	private String ROWNO;
	private String LANDINFO_ID;
	private String NO;
	private String STATUS;
	private String START_TIME;
	private String CLOSE_TIME;
	private String ANN_STATUS;
	private String ADDRESS;
	private String USE_TO;
	private String SQUARE_AREA;
	private String CAPACITY_RATE;
	private String LAND_USE_TERM;
	private String SUPPLY_MODE;
	private String FACILITIES;
	private String CLASSIFICATION;
	private String LANDCOUNTER;
	private String STARTING_PRICE;
	private String OFFER;
	private String OFFERDATE;
	private String BIDDER;
	private String WINNER; // 成交状态  失败为 null；
	
	public String getROWNO() {
		return ROWNO;
	}
	public void setROWNO(String rOWNO) {
		ROWNO = rOWNO;
	}
	public String getLANDINFO_ID() {
		return LANDINFO_ID;
	}
	public void setLANDINFO_ID(String lANDINFO_ID) {
		LANDINFO_ID = lANDINFO_ID;
	}
	public String getNO() {
		return NO;
	}
	public void setNO(String nO) {
		NO = nO;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getSTART_TIME() {
		return START_TIME;
	}
	public void setSTART_TIME(String sTART_TIME) {
		START_TIME = sTART_TIME;
	}
	public String getCLOSE_TIME() {
		return CLOSE_TIME;
	}
	public void setCLOSE_TIME(String cLOSE_TIME) {
		CLOSE_TIME = cLOSE_TIME;
	}
	public String getANN_STATUS() {
		return ANN_STATUS;
	}
	public void setANN_STATUS(String aNN_STATUS) {
		ANN_STATUS = aNN_STATUS;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getUSE_TO() {
		return USE_TO;
	}
	public void setUSE_TO(String uSE_TO) {
		USE_TO = uSE_TO;
	}
	public String getSQUARE_AREA() {
		return SQUARE_AREA;
	}
	public void setSQUARE_AREA(String sQUARE_AREA) {
		SQUARE_AREA = sQUARE_AREA;
	}
	public String getCAPACITY_RATE() {
		return CAPACITY_RATE;
	}
	public void setCAPACITY_RATE(String cAPACITY_RATE) {
		CAPACITY_RATE = cAPACITY_RATE;
	}
	public String getLAND_USE_TERM() {
		return LAND_USE_TERM;
	}
	public void setLAND_USE_TERM(String lAND_USE_TERM) {
		LAND_USE_TERM = lAND_USE_TERM;
	}
	public String getSUPPLY_MODE() {
		return SUPPLY_MODE;
	}
	public void setSUPPLY_MODE(String sUPPLY_MODE) {
		SUPPLY_MODE = sUPPLY_MODE;
	}
	public String getFACILITIES() {
		return FACILITIES;
	}
	public void setFACILITIES(String fACILITIES) {
		FACILITIES = fACILITIES;
	}
	public String getCLASSIFICATION() {
		return CLASSIFICATION;
	}
	public void setCLASSIFICATION(String cLASSIFICATION) {
		CLASSIFICATION = cLASSIFICATION;
	}
	public String getLANDCOUNTER() {
		return LANDCOUNTER;
	}
	public void setLANDCOUNTER(String lANDCOUNTER) {
		LANDCOUNTER = lANDCOUNTER;
	}
	public String getSTARTING_PRICE() {
		return STARTING_PRICE;
	}
	public void setSTARTING_PRICE(String sTARTING_PRICE) {
		STARTING_PRICE = sTARTING_PRICE;
	}
	public String getOFFER() {
		return OFFER;
	}
	public void setOFFER(String oFFER) {
		OFFER = oFFER;
	}
	public String getOFFERDATE() {
		return OFFERDATE;
	}
	public void setOFFERDATE(String oFFERDATE) {
		OFFERDATE = oFFERDATE;
	}
	public String getBIDDER() {
		return BIDDER;
	}
	public void setBIDDER(String bIDDER) {
		BIDDER = bIDDER;
	}
	public String getWINNER() {
		return WINNER;
	}
	public void setWINNER(String wINNER) {
		WINNER = wINNER;
	}
	
	
}
