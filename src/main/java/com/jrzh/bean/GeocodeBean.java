package com.jrzh.bean;

import com.jrzh.framework.base.BaseBean;

/**
 * 腾讯地图经纬度bean
 */
public class GeocodeBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4854117887455172718L;
	
	private GeocodeBean geocode;
	
	private String country;
	
	private String province;
	
	private String citycode;
	
	private String city;
	
	private String district;
	
	private String township;
	
	private String adcode;
	
	private String street;
	
	private String number;
	
	private String location;
	
	private String level;

	public GeocodeBean getGeocode() {
		return geocode;
	}

	public void setGeocode(GeocodeBean geocode) {
		this.geocode = geocode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCitycode() {
		return citycode;
	}

	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getTownship() {
		return township;
	}

	public void setTownship(String township) {
		this.township = township;
	}

	public String getAdcode() {
		return adcode;
	}

	public void setAdcode(String adcode) {
		this.adcode = adcode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

}
