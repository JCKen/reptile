package com.jrzh.bean;

public class ReptilePartBean {
	
	public Long count;
	
	public Long avg;

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getAvg() {
		return avg;
	}

	public void setAvg(Long avg) {
		this.avg = avg;
	}

 }

