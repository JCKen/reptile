/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.bean.SHFloorProject;
import java.util.Date;

/**
 * Auto-generated: 2019-01-14 11:10:37
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class StartUnitList {

    private String bookingoffice;
    private Double sell_area;
    private Initial_date initial_date;
    private Double leaving_num;
    private Double z_area;
    private Double status;
    private String presell_id;
    private String bookingphone;
    private Double leaving_area;
    private String start_code;
    private Double z_num;
    private String presell_desc;
    private Double startFlag;
    private Double num;
    private Double sell_num;
    private Double area;
    private Double flag;
    private String start_id;
    private String project_id;
    private Double issalable;
    private Date start_date;
	public String getBookingoffice() {
		return bookingoffice;
	}
	public void setBookingoffice(String bookingoffice) {
		this.bookingoffice = bookingoffice;
	}
	public Double getSell_area() {
		return sell_area;
	}
	public void setSell_area(Double sell_area) {
		this.sell_area = sell_area;
	}
	public Initial_date getInitial_date() {
		return initial_date;
	}
	public void setInitial_date(Initial_date initial_date) {
		this.initial_date = initial_date;
	}
	public Double getLeaving_num() {
		return leaving_num;
	}
	public void setLeaving_num(Double leaving_num) {
		this.leaving_num = leaving_num;
	}
	public Double getZ_area() {
		return z_area;
	}
	public void setZ_area(Double z_area) {
		this.z_area = z_area;
	}
	public Double getStatus() {
		return status;
	}
	public void setStatus(Double status) {
		this.status = status;
	}
	public String getPresell_id() {
		return presell_id;
	}
	public void setPresell_id(String presell_id) {
		this.presell_id = presell_id;
	}
	public String getBookingphone() {
		return bookingphone;
	}
	public void setBookingphone(String bookingphone) {
		this.bookingphone = bookingphone;
	}
	public Double getLeaving_area() {
		return leaving_area;
	}
	public void setLeaving_area(Double leaving_area) {
		this.leaving_area = leaving_area;
	}
	public String getStart_code() {
		return start_code;
	}
	public void setStart_code(String start_code) {
		this.start_code = start_code;
	}
	public Double getZ_num() {
		return z_num;
	}
	public void setZ_num(Double z_num) {
		this.z_num = z_num;
	}
	public String getPresell_desc() {
		return presell_desc;
	}
	public void setPresell_desc(String presell_desc) {
		this.presell_desc = presell_desc;
	}
	public Double getStartFlag() {
		return startFlag;
	}
	public void setStartFlag(Double startFlag) {
		this.startFlag = startFlag;
	}
	public Double getNum() {
		return num;
	}
	public void setNum(Double num) {
		this.num = num;
	}
	public Double getSell_num() {
		return sell_num;
	}
	public void setSell_num(Double sell_num) {
		this.sell_num = sell_num;
	}
	public Double getArea() {
		return area;
	}
	public void setArea(Double area) {
		this.area = area;
	}
	public Double getFlag() {
		return flag;
	}
	public void setFlag(Double flag) {
		this.flag = flag;
	}
	public String getStart_id() {
		return start_id;
	}
	public void setStart_id(String start_id) {
		this.start_id = start_id;
	}
	public String getProject_id() {
		return project_id;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}
	public Double getIssalable() {
		return issalable;
	}
	public void setIssalable(Double issalable) {
		this.issalable = issalable;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

}