/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.bean.SHFloorProject;
import java.util.List;

/**
 * Auto-generated: 2019-01-14 11:10:37
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class JsonRootBean {

    private List<List<PriceInformationList>> priceInformationList;
    private List<StartUnitList> startUnitList;
    public void setPriceInformationList(List<List<PriceInformationList>> priceInformationList) {
         this.priceInformationList = priceInformationList;
     }
     public List<List<PriceInformationList>> getPriceInformationList() {
         return priceInformationList;
     }

    public void setStartUnitList(List<StartUnitList> startUnitList) {
         this.startUnitList = startUnitList;
     }
     public List<StartUnitList> getStartUnitList() {
         return startUnitList;
     }

}