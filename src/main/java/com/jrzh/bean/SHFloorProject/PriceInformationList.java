/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.bean.SHFloorProject;

/**
 * Auto-generated: 2019-01-14 11:10:37
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class PriceInformationList {

    private String building_name;
    private Initial_date initial_date;
    private int sell_area;
    private int leaving_num;
    private String building_id;
    private int hign_refprice;
    private int precon_num;
    private double leaving_area;
    private int precon_area;
    private int sell_num;
    private int num;
    private double area;
    private int ratio;
    private String start_id;
    private String project_id;
    private int low_refprice;
    private int issalable;
    private Start_date start_date;
    public void setBuilding_name(String building_name) {
         this.building_name = building_name;
     }
     public String getBuilding_name() {
         return building_name;
     }

    public void setInitial_date(Initial_date initial_date) {
         this.initial_date = initial_date;
     }
     public Initial_date getInitial_date() {
         return initial_date;
     }

    public void setSell_area(int sell_area) {
         this.sell_area = sell_area;
     }
     public int getSell_area() {
         return sell_area;
     }

    public void setLeaving_num(int leaving_num) {
         this.leaving_num = leaving_num;
     }
     public int getLeaving_num() {
         return leaving_num;
     }

    public void setBuilding_id(String building_id) {
         this.building_id = building_id;
     }
     public String getBuilding_id() {
         return building_id;
     }

    public void setHign_refprice(int hign_refprice) {
         this.hign_refprice = hign_refprice;
     }
     public int getHign_refprice() {
         return hign_refprice;
     }

    public void setPrecon_num(int precon_num) {
         this.precon_num = precon_num;
     }
     public int getPrecon_num() {
         return precon_num;
     }

    public void setLeaving_area(double leaving_area) {
         this.leaving_area = leaving_area;
     }
     public double getLeaving_area() {
         return leaving_area;
     }

    public void setPrecon_area(int precon_area) {
         this.precon_area = precon_area;
     }
     public int getPrecon_area() {
         return precon_area;
     }

    public void setSell_num(int sell_num) {
         this.sell_num = sell_num;
     }
     public int getSell_num() {
         return sell_num;
     }

    public void setNum(int num) {
         this.num = num;
     }
     public int getNum() {
         return num;
     }

    public void setArea(double area) {
         this.area = area;
     }
     public double getArea() {
         return area;
     }

    public void setRatio(int ratio) {
         this.ratio = ratio;
     }
     public int getRatio() {
         return ratio;
     }

    public void setStart_id(String start_id) {
         this.start_id = start_id;
     }
     public String getStart_id() {
         return start_id;
     }

    public void setProject_id(String project_id) {
         this.project_id = project_id;
     }
     public String getProject_id() {
         return project_id;
     }

    public void setLow_refprice(int low_refprice) {
         this.low_refprice = low_refprice;
     }
     public int getLow_refprice() {
         return low_refprice;
     }

    public void setIssalable(int issalable) {
         this.issalable = issalable;
     }
     public int getIssalable() {
         return issalable;
     }

    public void setStart_date(Start_date start_date) {
         this.start_date = start_date;
     }
     public Start_date getStart_date() {
         return start_date;
     }

}