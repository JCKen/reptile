/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.bean.zhoubian;

/**
 * Auto-generated: 2019-01-24 10:51:36
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Location {

    private double lat;
    private double lng;
    public void setLat(double lat) {
         this.lat = lat;
     }
     public double getLat() {
         return lat;
     }

    public void setLng(double lng) {
         this.lng = lng;
     }
     public double getLng() {
         return lng;
     }

}