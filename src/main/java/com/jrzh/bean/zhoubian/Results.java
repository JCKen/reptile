/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.bean.zhoubian;

/**
 * Auto-generated: 2019-01-24 10:51:36
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Results {

    private String name;
    private Location location;
    private String address;
    private String province;
    private String city;
    private String area;
    private String street_id;
    private String telephone;
    private int detail;
    private String uid;
    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setLocation(Location location) {
         this.location = location;
     }
     public Location getLocation() {
         return location;
     }

    public void setAddress(String address) {
         this.address = address;
     }
     public String getAddress() {
         return address;
     }

    public void setProvince(String province) {
         this.province = province;
     }
     public String getProvince() {
         return province;
     }

    public void setCity(String city) {
         this.city = city;
     }
     public String getCity() {
         return city;
     }

    public void setArea(String area) {
         this.area = area;
     }
     public String getArea() {
         return area;
     }

    public void setStreet_id(String street_id) {
         this.street_id = street_id;
     }
     public String getStreet_id() {
         return street_id;
     }

    public void setTelephone(String telephone) {
         this.telephone = telephone;
     }
     public String getTelephone() {
         return telephone;
     }

    public void setDetail(int detail) {
         this.detail = detail;
     }
     public int getDetail() {
         return detail;
     }

    public void setUid(String uid) {
         this.uid = uid;
     }
     public String getUid() {
         return uid;
     }

}