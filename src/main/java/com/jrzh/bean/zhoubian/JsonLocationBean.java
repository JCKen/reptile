/**
  * Copyright 2019 bejson.com 
  */
package com.jrzh.bean.zhoubian;
import java.util.List;

/**
 * Auto-generated: 2019-01-24 10:51:36
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class JsonLocationBean {

    private int status;
    private String message;
    private List<Results> results;
    public void setStatus(int status) {
         this.status = status;
     }
     public int getStatus() {
         return status;
     }

    public void setMessage(String message) {
         this.message = message;
     }
     public String getMessage() {
         return message;
     }

    public void setResults(List<Results> results) {
         this.results = results;
     }
     public List<Results> getResults() {
         return results;
     }

}