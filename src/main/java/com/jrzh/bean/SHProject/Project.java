package com.jrzh.bean.SHProject;

/**
 * Auto-generated: 2019-01-15 15:15:37
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Project {

    private Double reg_area;
    private Double sell_area;
    private Double leaving_num;
    private String memo;
    private String districtname;
    private String adurl;
    private Double precon_num;
    private Double leaving_area;
    private Double z_sell_num;
    private Double precon_area;
    private Double z_all_sign_cancel_avgprice;
    private Double z_precon_area;
    private Double all_sign_cancel_avgprice;
    private String comp_id;
    private Double area;
    private String name;
    private String longitude;
    private Double z_reg_area;
    private Double z_leaving_num;
    private Double z_sell_area;
    private Double z_leaving_area;
    private Double z_area;
    private Double z_reg_num;
    private Double djcancel_time;
    private Double z_all_sign_avgprice;
    private Double z_num;
    private String projectname;
    private Double cancel_time;
    private Double num;
    private String projectadr;
    private Double sell_num;
    private Double reg_num;
    private String regionname;
    private Double all_sign_avgprice;
    private String project_id;
    private String latitude;
    private Double z_precon_num;
	public Double getReg_area() {
		return reg_area;
	}
	public void setReg_area(Double reg_area) {
		this.reg_area = reg_area;
	}
	public Double getSell_area() {
		return sell_area;
	}
	public void setSell_area(Double sell_area) {
		this.sell_area = sell_area;
	}
	public Double getLeaving_num() {
		return leaving_num;
	}
	public void setLeaving_num(Double leaving_num) {
		this.leaving_num = leaving_num;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getDistrictname() {
		return districtname;
	}
	public void setDistrictname(String districtname) {
		this.districtname = districtname;
	}
	public String getAdurl() {
		return adurl;
	}
	public void setAdurl(String adurl) {
		this.adurl = adurl;
	}
	public Double getPrecon_num() {
		return precon_num;
	}
	public void setPrecon_num(Double precon_num) {
		this.precon_num = precon_num;
	}
	public Double getLeaving_area() {
		return leaving_area;
	}
	public void setLeaving_area(Double leaving_area) {
		this.leaving_area = leaving_area;
	}
	public Double getZ_sell_num() {
		return z_sell_num;
	}
	public void setZ_sell_num(Double z_sell_num) {
		this.z_sell_num = z_sell_num;
	}
	public Double getPrecon_area() {
		return precon_area;
	}
	public void setPrecon_area(Double precon_area) {
		this.precon_area = precon_area;
	}
	public Double getZ_all_sign_cancel_avgprice() {
		return z_all_sign_cancel_avgprice;
	}
	public void setZ_all_sign_cancel_avgprice(Double z_all_sign_cancel_avgprice) {
		this.z_all_sign_cancel_avgprice = z_all_sign_cancel_avgprice;
	}
	public Double getZ_precon_area() {
		return z_precon_area;
	}
	public void setZ_precon_area(Double z_precon_area) {
		this.z_precon_area = z_precon_area;
	}
	public Double getAll_sign_cancel_avgprice() {
		return all_sign_cancel_avgprice;
	}
	public void setAll_sign_cancel_avgprice(Double all_sign_cancel_avgprice) {
		this.all_sign_cancel_avgprice = all_sign_cancel_avgprice;
	}
	public String getComp_id() {
		return comp_id;
	}
	public void setComp_id(String comp_id) {
		this.comp_id = comp_id;
	}
	public Double getArea() {
		return area;
	}
	public void setArea(Double area) {
		this.area = area;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public Double getZ_reg_area() {
		return z_reg_area;
	}
	public void setZ_reg_area(Double z_reg_area) {
		this.z_reg_area = z_reg_area;
	}
	public Double getZ_leaving_num() {
		return z_leaving_num;
	}
	public void setZ_leaving_num(Double z_leaving_num) {
		this.z_leaving_num = z_leaving_num;
	}
	public Double getZ_sell_area() {
		return z_sell_area;
	}
	public void setZ_sell_area(Double z_sell_area) {
		this.z_sell_area = z_sell_area;
	}
	public Double getZ_leaving_area() {
		return z_leaving_area;
	}
	public void setZ_leaving_area(Double z_leaving_area) {
		this.z_leaving_area = z_leaving_area;
	}
	public Double getZ_area() {
		return z_area;
	}
	public void setZ_area(Double z_area) {
		this.z_area = z_area;
	}
	public Double getZ_reg_num() {
		return z_reg_num;
	}
	public void setZ_reg_num(Double z_reg_num) {
		this.z_reg_num = z_reg_num;
	}
	public Double getDjcancel_time() {
		return djcancel_time;
	}
	public void setDjcancel_time(Double djcancel_time) {
		this.djcancel_time = djcancel_time;
	}
	public Double getZ_all_sign_avgprice() {
		return z_all_sign_avgprice;
	}
	public void setZ_all_sign_avgprice(Double z_all_sign_avgprice) {
		this.z_all_sign_avgprice = z_all_sign_avgprice;
	}
	public Double getZ_num() {
		return z_num;
	}
	public void setZ_num(Double z_num) {
		this.z_num = z_num;
	}
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public Double getCancel_time() {
		return cancel_time;
	}
	public void setCancel_time(Double cancel_time) {
		this.cancel_time = cancel_time;
	}
	public Double getNum() {
		return num;
	}
	public void setNum(Double num) {
		this.num = num;
	}
	public String getProjectadr() {
		return projectadr;
	}
	public void setProjectadr(String projectadr) {
		this.projectadr = projectadr;
	}
	public Double getSell_num() {
		return sell_num;
	}
	public void setSell_num(Double sell_num) {
		this.sell_num = sell_num;
	}
	public Double getReg_num() {
		return reg_num;
	}
	public void setReg_num(Double reg_num) {
		this.reg_num = reg_num;
	}
	public String getRegionname() {
		return regionname;
	}
	public void setRegionname(String regionname) {
		this.regionname = regionname;
	}
	public Double getAll_sign_avgprice() {
		return all_sign_avgprice;
	}
	public void setAll_sign_avgprice(Double all_sign_avgprice) {
		this.all_sign_avgprice = all_sign_avgprice;
	}
	public String getProject_id() {
		return project_id;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public Double getZ_precon_num() {
		return z_precon_num;
	}
	public void setZ_precon_num(Double z_precon_num) {
		this.z_precon_num = z_precon_num;
	}

}