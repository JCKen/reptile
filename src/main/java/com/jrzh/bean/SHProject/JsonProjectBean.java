package com.jrzh.bean.SHProject;

/**
 * Auto-generated: 2019-01-15 15:15:37
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class JsonProjectBean {

    private Project project;
    public void setProject(Project project) {
         this.project = project;
     }
     public Project getProject() {
         return project;
     }

}