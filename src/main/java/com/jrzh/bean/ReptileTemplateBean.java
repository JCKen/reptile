package com.jrzh.bean;

import com.jrzh.mvc.view.reptile.ReptileTemplateInfoView;
import com.jrzh.mvc.view.reptile.ReptileTemplateSumInfoView;
import com.jrzh.mvc.view.reptile.ReptileTemplateView;

public class ReptileTemplateBean {
	
	public ReptileTemplateView view;
	
	public ReptileTemplateSumInfoView  sumView;
	
	public ReptileTemplateInfoView[] views;
	
	public ReptileTemplateInfoView[] newViews;
	
	public ReptileTemplateInfoView[] esfViews;
	
	public ReptileTemplateInfoView[] tudiViews;
	

	public ReptileTemplateSumInfoView getSumView() {
		return sumView;
	}

	public void setSumView(ReptileTemplateSumInfoView sumView) {
		this.sumView = sumView;
	}

	public ReptileTemplateInfoView[] getNewViews() {
		return newViews;
	}

	public void setNewViews(ReptileTemplateInfoView[] newViews) {
		this.newViews = newViews;
	}

	public ReptileTemplateInfoView[] getEsfViews() {
		return esfViews;
	}

	public void setEsfViews(ReptileTemplateInfoView[] esfViews) {
		this.esfViews = esfViews;
	}

	public ReptileTemplateInfoView[] getTudiViews() {
		return tudiViews;
	}

	public void setTudiViews(ReptileTemplateInfoView[] tudiViews) {
		this.tudiViews = tudiViews;
	}

	public ReptileTemplateView getView() {
		return view;
	}

	public void setView(ReptileTemplateView view) {
		this.view = view;
	}

	public ReptileTemplateInfoView[] getViews() {
		return views;
	}

	public void setViews(ReptileTemplateInfoView[] views) {
		this.views = views;
	}
 }

