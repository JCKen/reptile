package com.jrzh.bean;

import com.jrzh.framework.base.BaseBean;

public class ReptileSearchPriceResultBean extends BaseBean {
    
    private Double priceAvg;
    
    private Double priceSum;
    
    private String houseCity;
    
    private String housePart;
    
    private String houseStatus;

	public String getHouseStatus() {
		return houseStatus;
	}

	public void setHouseStatus(String houseStatus) {
		this.houseStatus = houseStatus;
	}

	public Double getPriceAvg() {
		return priceAvg;
	}

	public void setPriceAvg(Double priceAvg) {
		this.priceAvg = priceAvg;
	}

	public Double getPriceSum() {
		return priceSum;
	}

	public void setPriceSum(Double priceSum) {
		this.priceSum = priceSum;
	}

	public String getHouseCity() {
		return houseCity;
	}

	public void setHouseCity(String houseCity) {
		this.houseCity = houseCity;
	}

	public String getHousePart() {
		return housePart;
	}

	public void setHousePart(String housePart) {
		this.housePart = housePart;
	}
    
}