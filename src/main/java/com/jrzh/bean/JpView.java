package com.jrzh.bean;

import com.jrzh.framework.base.BaseBean;

public class JpView extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7624920651106978933L;
	private String id;
	private String houseName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

}
