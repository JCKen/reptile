package com.jrzh.bean;

import com.jrzh.framework.base.BaseBean;

/**
 * 腾讯地图经纬度bean
 */
public class LatitudeLongitudeBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4854117887455172718L;
	
	private GeocodeBean geocode;

	public GeocodeBean getGeocode() {
		return geocode;
	}

	public void setGeocode(GeocodeBean geocode) {
		this.geocode = geocode;
	}
	
}
