package com.jrzh.bean;

import com.jrzh.framework.base.BaseBean;

public class LayuiResultBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3752773171255998596L;
	private String code;
	private String msg;
	private String count;
	private Object data;
	
	
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
