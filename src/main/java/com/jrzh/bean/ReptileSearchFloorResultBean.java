package com.jrzh.bean;

import java.util.List;

import com.jrzh.framework.base.BaseBean;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseFloorInfoView;

public class ReptileSearchFloorResultBean extends BaseBean {
	
    private List<String> floorNameList;
	
    private List<List<ReptileYgjyHouseFloorInfoView>> floorList;


	public List<String> getFloorNameList() {
		return floorNameList;
	}

	public void setFloorNameList(List<String> floorNameList) {
		this.floorNameList = floorNameList;
	}

	public List<List<ReptileYgjyHouseFloorInfoView>> getFloorList() {
		return floorList;
	}

	public void setFloorList(List<List<ReptileYgjyHouseFloorInfoView>> floorList) {
		this.floorList = floorList;
	}
    
    
}