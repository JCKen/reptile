package com.jrzh.bean.jwd;

import java.util.List;

public class JwdBean {
	
	private String status;
	private String message;
	private List<JwdInfo> result;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<JwdInfo> getResult() {
		return result;
	}
	public void setResult(List<JwdInfo> result) {
		this.result = result;
	}
	
	
}
