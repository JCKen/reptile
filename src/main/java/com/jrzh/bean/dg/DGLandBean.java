package com.jrzh.bean.dg;

import java.util.List;

public class DGLandBean {
	
	private String total;
	private List<RowBean> rows;
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public List<RowBean> getRows() {
		return rows;
	}
	public void setRows(List<RowBean> rows) {
		this.rows = rows;
	}
	
	
}
