package com.jrzh.bean;

import java.util.List;

import com.jrzh.framework.base.BaseBean;
import com.jrzh.mvc.view.reptile.ReptileEsfHouseView;
import com.jrzh.mvc.view.reptile.ReptileNewHouseView;
import com.jrzh.mvc.view.reptile.ReptileYgjyHouseFloorInfoView;

public class ReptileSearchSurroundingResultBean extends BaseBean {
	
    private List<ReptileNewHouseView> newHouseList;
    
    private List<ReptileEsfHouseView> esfHouseList;

	public List<ReptileNewHouseView> getNewHouseList() {
		return newHouseList;
	}

	public void setNewHouseList(List<ReptileNewHouseView> newHouseList) {
		this.newHouseList = newHouseList;
	}

	public List<ReptileEsfHouseView> getEsfHouseList() {
		return esfHouseList;
	}

	public void setEsfHouseList(List<ReptileEsfHouseView> esfHouseList) {
		this.esfHouseList = esfHouseList;
	}
    
}