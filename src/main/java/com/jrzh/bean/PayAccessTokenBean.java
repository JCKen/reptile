package com.jrzh.bean;

import com.jrzh.framework.base.BaseBean;

public class PayAccessTokenBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4854117887455172718L;

	private String id_token;// JSON Web Token (JWT) that includes information
							// about the user. This field is returned only if
							// openid is specified in the scope. For more
							// information, see ID tokens.
	private String scope;// Permissions granted by the user. Howeve

	public String getId_token() {
		return id_token;
	}

	public void setId_token(String id_token) {
		this.id_token = id_token;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

}
