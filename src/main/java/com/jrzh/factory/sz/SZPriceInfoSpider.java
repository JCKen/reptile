package com.jrzh.factory.sz;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.jrzh.bean.sz.Data;
import com.jrzh.bean.sz.JsonRootBean;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileSaleInfoModel;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.util.HttpPostJsonUtils;

/**
 * @author xsc
 *深圳历史成交总价还有成交面积数据获取类
 */
public class SZPriceInfoSpider {
	
	final static String url = "http://ris.szpl.gov.cn:8080/api/marketInfoShow/getFjzsInfoData";
    final static String params = "{\"startDate\":\"2019-01-1\",\"endDate\":\"2019-03-1\",\"dateType\":\"\"}";
	
	public static void saveInfo(ReptileServiceManage reptileServiceManage,
			SessionUser user) throws ProjectException{
		String res = HttpPostJsonUtils.post(url, params);
		JsonRootBean jsonRootBean = JSON.parseObject(res,JsonRootBean.class);
		Data data = jsonRootBean.getData();
		//日期
		List<String> dateList = data.getDate();
		//二手房成交面积
		List<String> esfDealList = data.getEsfDealArea();
		//二手房成交套数
		List<String> esfTotalList = data.getEsfTotalTs();
		//二手房成交单价
		List<String> esfUnitList= data.getEsfUnitPrice();
		//一手房成交面积
		List<String> ysfDealList= data.getYsfDealArea();
		//一手房成交套数
		List<String> ysfTotalList= data.getYsfTotalTs();
		//一手房成交单价
		List<String> ysfUnitList = data.getYsfUnitPrice();
		for(Integer i = 0 ; i < dateList.size();i++) {
			ReptileSaleInfoModel model = new ReptileSaleInfoModel();
			model.setAllArea(ysfDealList.get(i));
			model.setAllNumber(ysfTotalList.get(i));
			model.setAllPrice(ysfUnitList.get(i));
			model.setCity("深圳");
			model.setInfoUpdataTime(DateUtil.format(dateList.get(i) + " 00:00:00"));
			System.out.println(DateUtil.format(dateList.get(i) + " 00:00:00"));
			reptileServiceManage.reptileSaleInfoService.add(model, user);
		}
	}
	
}
