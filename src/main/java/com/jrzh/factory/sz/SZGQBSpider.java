package com.jrzh.factory.sz;

import java.util.Date;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileCommercialHouseInfoModel;
import com.jrzh.mvc.model.reptile.ReptileCommodityHouseInfoModel;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

/**
 * @author xsc
 *深圳供求比获取类
 */
public class SZGQBSpider {
	
	public static String[] __EVENTTARGET = {"hypBa","hypFt","hypLg","hypLh","hypNs","hypYt"};
	
	public static void saveInfo(ReptileServiceManage reptileServiceManage,
			SessionUser user) throws ProjectException{
		Date infoUpdateTime = new Date();
		for(String s : __EVENTTARGET) {
			try {
				Document doc = Jsoup.connect("http://ris.szpl.gov.cn/szfdc/showcjgs/ysfcjgs.aspx?cjType=0").get();
				if(null == doc) return;
				Elements docEle = doc.select("input");
				if(null == docEle) return;
				String __EVENTARGUMENT = docEle.get(1).attr("value");
				String __VIEWSTATE = docEle.get(2).attr("value");
				String __VIEWSTATEGENERATOR = docEle.get(3).attr("value");
				String __VIEWSTATEENCRYPTED = docEle.get(4).attr("value");
				String __EVENTVALIDATION = docEle.get(5).attr("value");
				Connection conn = Jsoup.connect("http://ris.szpl.gov.cn/szfdc/showcjgs/ysfcjgs.aspx?cjType=0");
				conn.data("__EVENTTARGET", s);
				conn.data("__EVENTARGUMENT", __EVENTARGUMENT);
				conn.data("__VIEWSTATE", __VIEWSTATE);
				conn.data("__VIEWSTATEGENERATOR", __VIEWSTATEGENERATOR);
				conn.data("__VIEWSTATEENCRYPTED", __VIEWSTATEENCRYPTED);
				conn.data("__EVENTVALIDATION", __EVENTVALIDATION);
				Document teDoc = conn.post();
				if(null == teDoc) continue;
				Element partEle = teDoc.selectFirst("#ctl03_lbldistrict2");
				String part = null;
				if(null != partEle) {
					part = partEle.text();
				}
				Elements tableEle = teDoc.select("table");
				if(null == tableEle) continue;
				if(null != tableEle.get(0)) {
					Elements trEle = tableEle.get(0).select("tr");
					if(null != trEle) {
						for(Integer i = 1 ; i < trEle.size();i++) {
							Elements tdEles = trEle.get(i).select("td");
							if(null == tdEles && trEle.size() < 6) continue;
							if(i == trEle.size()-1 || i == trEle.size()-2) continue;
							//存放商品住房信息
							ReptileCommodityHouseInfoModel model = new ReptileCommodityHouseInfoModel();
							//用途
							model.setHouseType(tdEles.get(0).text());
							//成交套数
							model.setTransactionNumber(tdEles.get(1).text());
							//成交面积
							model.setTransactionArea(tdEles.get(2).text());
							//成交金额
							model.setTransactionPrice(tdEles.get(3).text());
							//可售套数
							model.setAvailableSets(tdEles.get(4).text());
							//可售面积
							model.setSaleableArea(tdEles.get(5).text());
							model.setCity("深圳");
							model.setPart(part);
							model.setInfoUpdataTime(DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd")+" 00:00:00"));
							reptileServiceManage.reptileCommodityHouseInfoService.add(model, user);
						}
					}
				}
				if(null != tableEle.get(2)) {
					Elements trEle = tableEle.get(2).select("tr");
					if(null != trEle) {
						for(Integer i = 1 ; i < trEle.size();i++) {
							Elements tdEles = trEle.get(i).select("td");
							if(null == tdEles && trEle.size() < 6) continue;
							if(i == trEle.size()-1 || i == trEle.size()-2) continue;
							//存放商品房信息
							ReptileCommercialHouseInfoModel model = new ReptileCommercialHouseInfoModel();
							model.setCity("深圳");
							model.setPart(part);
							//用途
							model.setUse(tdEles.get(0).text());
							//成交套数
							model.setTransactionNumber(tdEles.get(1).text());
							//成交面积
							model.setTransactionArea(tdEles.get(2).text());
							//成交金额
							model.setTransactionPrice(tdEles.get(3).text());
							//可售套数
							model.setAvailableSets(tdEles.get(4).text());
							//可售面积
							model.setSaleableArea(tdEles.get(5).text());
							model.setInfoUpdataTime(DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd")+" 00:00:00"));
							reptileServiceManage.reptileCommercialHouseInfoService.add(model, user);
						}
					}
				}
				Thread.sleep(2000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
}
