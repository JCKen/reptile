package com.jrzh.factory.sz;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.contants.UrlContants;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

public class SZAreaSpider {
	public static Log log = LogFactory.getLog(SZAreaSpider.class);
	
	private final static Pattern REGEX_PRICE_WORD = Pattern.compile("成交价:(.*?)元");
		
	private final static Pattern REGEX_ADDRESS_WORD = Pattern.compile("地址: (.*?) ");
	
	private final static Pattern REGEX_AREA_WORD = Pattern.compile("土地面积: (.*?)m²");
	
	private final static Pattern REGEX_USE_WORD = Pattern.compile("用途: (.*?) ");
	
	private final static Pattern REGEX_AREA_NO_WORD = Pattern.compile("[A-Z]\\d{3,7}-\\d{3,7}");
	
	private final static Pattern REGEX_AREA_BUYER_WORD = Pattern.compile("竞得人:(.*?) ");
	
	public static List<ReptileGtjAreaInfoModel> getInfo(Integer page , List<ReptileGtjAreaInfoModel> infoList) throws ProjectException {
		try {
			log.info("#######深圳土地当前页数#########【"+page+"】");
			Document pageDocument = Jsoup.connect(UrlContants.LAND_SZ_LIST_URL+page).timeout(50000).get();
			Elements tbody = pageDocument.select("tr");
			for (Element tr : tbody) {
				Element timeElement = tr.selectFirst(".afterToday");
				Date makeTime = new Date();
				if(null != timeElement) makeTime = DateUtil.format(timeElement.text(), "YYYY-MM-DD hh:mm");
				if(!StringUtils.contains(tr.text(), "成交价")) continue;
				String info = tr.text();
				ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
				Matcher m =  REGEX_ADDRESS_WORD.matcher(info);
				if (m.find()) model.setAddress(m.group().replace("地址: ", ""));
				m = REGEX_AREA_NO_WORD.matcher(info);
				if(m.find()) model.setAreaNo(m.group());
				m = REGEX_USE_WORD.matcher(info);
				if(m.find()) model.setUse(m.group().replace("用途:", ""));
				m = REGEX_AREA_WORD.matcher(info);
				if(m.find()){
					String	acreage = m.group().replace("土地面积: ", "");
					if(acreage.contains("m²")){
						acreage = acreage.replace("m²", "");
					}
					if(acreage.contains(",")){
						acreage = acreage.replace(",", "");
					}
					acreage = acreage.trim();
					model.setAcreage(acreage);
				}
				m = REGEX_PRICE_WORD.matcher(info);
				String price = m.group().replace("成交价:", "");
				Double temp = Double.parseDouble(price.replace(",", ""))/10000; // 改成万元
				if(m.find()) model.setPrice(temp.toString());
				m = REGEX_AREA_BUYER_WORD.matcher(info);
				if(m.find()){
					String buyer = m.group().replace("竞得人:", "");
					if(StringUtils.equals(buyer, "null")) continue;
					model.setBuyer(buyer);
				}
				model.setTime(makeTime);
				model.setUrl(UrlContants.LAND_SZ_LIST_URL + page);
				model.setCity("深圳");
				model.setFrom("深圳市土地房产交易中心");
				infoList.add(model);
			}
			return infoList;
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}
	
	public static List<ReptileGtjAreaInfoModel> getSZAreaInfo(Integer maxPage) {
		List<ReptileGtjAreaInfoModel> infoList = new LinkedList<ReptileGtjAreaInfoModel>();
		try {
			for (int i = 1; i < maxPage; i++) {
				infoList =getInfo(i, infoList);
			}
		} catch (ProjectException e) {
			e.printStackTrace();
		}
		return infoList;
	}
	
}
