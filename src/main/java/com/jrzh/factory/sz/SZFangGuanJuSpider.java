package com.jrzh.factory.sz;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.contants.Contants;
import com.jrzh.factory.base.inf.SpiderInf;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileYgjyHouseInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseFloorInfoModel;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseInfoModel;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseFloorInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseInfoSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public class SZFangGuanJuSpider  implements SpiderInf<ReptileYgjyHouseInfoModel> {

	private final static String URL = "http://ris.szpl.gov.cn/bol/index.aspx";
	
	private final static String DETAIL_URL = "http://ris.szpl.gov.cn/bol/projectdetail.aspx";
	
	private static Log log = LogFactory.getLog(SZFangGuanJuSpider.class);
	
	public void saveSHFangGuanJuHouseInfo(ReptileServiceManage reptileServiceManage, SessionUser user) throws ProjectException {
		new SZFangGuanJuSpider().spider(URL, null, 1, reptileServiceManage,user);
	}
	
	
	@Override
	public void spider(String url,String part, Integer index , ReptileServiceManage reptileServiceManage,
			SessionUser user) throws ProjectException {
		Date infoUpdateTime = new Date();
		try {
			Document doc = Jsoup.connect(url).timeout(50000).get();
			if(null == doc) {
				Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
			}
			Element selectAspNetPager = doc.selectFirst("#AspNetPager1");
			if(null == selectAspNetPager)  {
				Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);	
			}
			Elements allPage = selectAspNetPager.select("b");
			if(null == allPage)  {
				Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
			}
			Elements docEle = doc.select("input");
			if(null == docEle) {
				Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
			}
			String __EVENTTARGET = docEle.get(0).attr("value");
			String __EVENTARGUMENT = docEle.get(1).attr("value");
			String __VIEWSTATE = docEle.get(2).attr("value");
			String __VIEWSTATEGENERATOR = docEle.get(3).attr("value");
			String __VIEWSTATEENCRYPTED = docEle.get(4).attr("value");
			String __EVENTVALIDATION = docEle.get(5).attr("value");
			String tep_name = docEle.get(6).attr("value");
			String organ_name = docEle.get(7).attr("value");
			String site_address = docEle.get(8).attr("value");
			String Button1 = docEle.get(9).attr("value");
			String AspNetPager1 = docEle.get(11).attr("value");
			Connection conn = Jsoup.connect(URL).timeout(50000);
			conn.data("__EVENTTARGET", __EVENTTARGET);
			conn.data("__EVENTARGUMENT", __EVENTARGUMENT);
			conn.data("__VIEWSTATE", __VIEWSTATE);
			conn.data("__VIEWSTATEGENERATOR", __VIEWSTATEGENERATOR);
			conn.data("__VIEWSTATEENCRYPTED", __VIEWSTATEENCRYPTED);
			conn.data("__EVENTVALIDATION", __EVENTVALIDATION);
			conn.data("tep_name", tep_name);
			conn.data("organ_name", organ_name);
			conn.data("site_address", site_address);
			conn.data("Button1", Button1);
			conn.data("AspNetPager1_input", String.valueOf(index));
			conn.data("AspNetPager1", AspNetPager1);
			Document teDoc = conn.post();
			String allPageCount = allPage.last().text();
			Element selectDataList = teDoc.selectFirst("#DataList1");
			if(null == selectDataList) {
				Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
			}
			Element selectTable = selectDataList.selectFirst("table");
			if(null == selectTable) {
				Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
			}
			if(StringUtils.isEmpty(allPageCount) || StringUtils.equals(String.valueOf(index-1), allPageCount)) {
				log.info("结束了");
	        	return ;
			}
			Elements selectTr = selectTable.getElementsByAttributeValue("bgcolor", "#F5F9FC");
	        if(null != selectTr) {
	        	for(Element e : selectTr) {
	        		Elements selectTd = e.select("td");
	        		if(null == selectTd) continue;
	        		if(selectTd.size() < 6) {
	        			continue;
	        		}
	        		Element projectEle = selectTd.get(2).selectFirst("td");
	        		if(null == projectEle) {
	        			continue;
	        		}
	        		Element projectPreSold = selectTd.get(1).selectFirst("td");
	        		if(null == projectPreSold) {
	        			continue;
	        		}
	        		Element projectHref = projectEle.selectFirst("a");
	        		if(null == projectHref) {
	        			continue;
	        		}
	        		String projectUrl = projectHref.attr("href");
	        		if(StringUtils.isEmpty(projectUrl)) {
	        			continue;
	        		}
	        		String[] projectAtt = projectUrl.split("id=");
	        		if(projectAtt.length < 1) {
	        			continue;
	        		}
	        		String projectId = projectAtt[1];
	        		String detailUrl = DETAIL_URL + "?id=" + projectId;
	        		Document detailDoc;
	        		try {
	        			detailDoc = Jsoup.connect(detailUrl).timeout(50000).get();
					} catch (Exception e2) {
						e2.printStackTrace();
						continue;
					}
	        		if(null == detailDoc) {
	        			continue;
	        		}
	        		Elements detailClass = detailDoc.select(".a1");
	        		if(null == detailClass) {
	        			continue;
	        		}
	        		Map<String,String> map = new HashMap<>();
	        		for(Element ele : detailClass) {
	        			Elements detailTd = ele.select("td");
	        			if(detailTd.size() == 2) {
	        				map.put(detailTd.get(0).text(), detailTd.get(1).text());
	        			}else if(detailTd.size() == 4) {
	        				map.put(detailTd.get(0).text(), detailTd.get(1).text());
	        				map.put(detailTd.get(2).text(), detailTd.get(3).text());
	        			}else if(detailTd.size() == 6){
	        				map.put(detailTd.get(0).text(), detailTd.get(1).text());
	        				map.put(detailTd.get(2).text(), detailTd.get(3).text());
	        				map.put(detailTd.get(4).text(), detailTd.get(5).text());
	        			}else {
	        				continue;
	        			}
	        		}
	        		ReptileYgjyHouseInfoModel reptileYgjyHouseInfoModel = ReptileYgjyHouseInfoConvert.houseWorldMapToHouseModel(map);
	        		reptileYgjyHouseInfoModel.setCity("深圳");
	        		reptileYgjyHouseInfoModel.setUrl(detailUrl);
	        		reptileYgjyHouseInfoModel.setProductName(projectHref.text());
	        		reptileYgjyHouseInfoModel.setPreSalePermit(projectPreSold.text());
	        		reptileYgjyHouseInfoModel.setSaleUrl(projectUrl);
	        		reptileYgjyHouseInfoModel.setProjectId(projectId);
	        		reptileYgjyHouseInfoModel.setDataUpdateTime(DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd")+" 00:00:00"));
	        		
	        		//深圳房管局楼盘数据入库
	    			ReptileYgjyHouseInfoSearch search = new ReptileYgjyHouseInfoSearch();
	    			search.setEqualCity("深圳");
	    			search.setEqualProjectId(projectId);
	    			ReptileYgjyHouseInfoModel newReptileYgjyHouseInfoModel = reptileServiceManage.reptileYgjyHouseInfoService.first(search);
	    			String houseId = null;
	    			if(null == newReptileYgjyHouseInfoModel) {
	    				reptileServiceManage.reptileYgjyHouseInfoService.add(reptileYgjyHouseInfoModel, user);
	    				houseId = reptileYgjyHouseInfoModel.getId();
	    			}else {
	    				ReflectUtils.copySameFieldToTargetFilterNull(reptileYgjyHouseInfoModel,newReptileYgjyHouseInfoModel);
	    				reptileServiceManage.reptileYgjyHouseInfoService.edit(newReptileYgjyHouseInfoModel, user);
	    				houseId = reptileYgjyHouseInfoModel.getId();
	    			}
	    			
	    			//深圳房管局楼房信息入库
	    			String houseDetailUrl = "http://ris.szpl.gov.cn/bol/projectdetail.aspx?id=" + projectId;
					Document houseDetailDoc = Jsoup.connect(houseDetailUrl).timeout(50000).get();
					if(null == houseDetailDoc) {
						continue;
					}
					Element selectHouseDataList = houseDetailDoc.selectFirst("#DataList1");
					if(null == selectHouseDataList) {
						continue;
					}
					Elements selectDataListTr = selectHouseDataList.getElementsByAttributeValue("bgcolor", "#F5F9FC");
					if(null == selectDataListTr) {
						continue;
					}
	 				for(Element houseE : selectDataListTr) {
	 					ReptileYgjyHouseFloorInfoModel reptileYgjyHouseFloorInfoModel = new ReptileYgjyHouseFloorInfoModel();
						Elements selectDataListTd = houseE.select("td");
						if(null == selectDataListTd) continue;
						if(selectDataListTd.size() < 5) continue;
						Element buildingEle = selectDataListTd.get(4).selectFirst("a");
						if(null == buildingEle) continue;
						String buildingUrl = buildingEle.attr("href");
						if(StringUtils.isEmpty(buildingUrl)) continue;
						String[] buildingArr = buildingUrl.split("id=");
						if(buildingArr.length < 2) continue;
						String buildingIds = buildingArr[1];
						String buildingId = buildingIds.split("&presell")[0];
						reptileYgjyHouseFloorInfoModel.setHouseId(houseId);
						reptileYgjyHouseFloorInfoModel.setSzStartId(projectId);
						reptileYgjyHouseFloorInfoModel.setFloorName(selectDataListTd.get(1).text());
						reptileYgjyHouseFloorInfoModel.setStatus(Contants.WING);
						reptileYgjyHouseFloorInfoModel.setBuildingId(buildingId);
						ReptileYgjyHouseFloorInfoSearch reptileYgjyHouseFloorInfoSearch = new ReptileYgjyHouseFloorInfoSearch();
						reptileYgjyHouseFloorInfoSearch.setEqualBuildingId(buildingId);
						reptileYgjyHouseFloorInfoSearch.setEqualSzStartId(projectId);
						reptileYgjyHouseFloorInfoSearch.setEqualHouseId(houseId);
						ReptileYgjyHouseFloorInfoModel newModel = reptileServiceManage.reptileYgjyHouseFloorInfoService.first(reptileYgjyHouseFloorInfoSearch);
						if(null == newModel) {
							reptileServiceManage.reptileYgjyHouseFloorInfoService.add(reptileYgjyHouseFloorInfoModel, SessionUser.getSystemUser());
						}else {
							ReflectUtils.copySameFieldToTargetFilterNull(reptileYgjyHouseFloorInfoModel,newModel);
							reptileServiceManage.reptileYgjyHouseFloorInfoService.edit(newModel, SessionUser.getSystemUser());
						}
		 				Thread.sleep(1000);
	 				}
	        		
	        		Thread.sleep(1000);
	        	}
	        }
	        Integer page =  ++index ;
			spider(url, null, page, reptileServiceManage,user);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}
	
	public static void main(String[] args) {
		try {
			Document doc = Jsoup.connect("http://ris.szpl.gov.cn/szfdc/showcjgs/ysfcjgs.aspx?cjType=0").get();
			if(null == doc) return;
			Elements docEle = doc.select("input");
			if(null == docEle) return;
			String __EVENTTARGET = docEle.get(0).attr("value");
			String __EVENTARGUMENT = docEle.get(1).attr("value");
			String __VIEWSTATE = docEle.get(2).attr("value");
			String __VIEWSTATEGENERATOR = docEle.get(3).attr("value");
			String __VIEWSTATEENCRYPTED = docEle.get(4).attr("value");
			String __EVENTVALIDATION = docEle.get(5).attr("value");
			Connection conn = Jsoup.connect("http://ris.szpl.gov.cn/szfdc/showcjgs/ysfcjgs.aspx?cjType=0");
			conn.data("__EVENTTARGET", "hypBa");
			conn.data("__EVENTARGUMENT", __EVENTARGUMENT);
			conn.data("__VIEWSTATE", __VIEWSTATE);
			conn.data("__VIEWSTATEGENERATOR", __VIEWSTATEGENERATOR);
			conn.data("__VIEWSTATEENCRYPTED", __VIEWSTATEENCRYPTED);
			conn.data("__EVENTVALIDATION", __EVENTVALIDATION);
			Document teDoc = conn.post();
			System.out.println(teDoc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
