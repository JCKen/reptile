package com.jrzh.factory.base.inf;


import com.jrzh.common.exception.ProjectException;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public  interface SpiderInf<T> {
	public  void spider(String url ,String part,Integer index ,ReptileServiceManage reptileServiceManage,SessionUser sessionUser)throws ProjectException;
}
