package com.jrzh.factory.base.inf;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.jrzh.mvc.model.reptile.ReptileNewhouseYushouzhengModel;

public class YushouzhengInfoMap {

	public static void doInfoMap(ReptileNewhouseYushouzhengModel model, Map<String, String> map) {
		for (String key : map.keySet()) {
			if (StringUtils.contains(key, "预售总建筑面积")) {
				model.setPresellAllArea(map.get(key));
			}
			if (StringUtils.contains(key, "总单元套数")) {
				model.setPresellAllCount(map.get(key));
			}
			if (StringUtils.contains(key, "是否抵押")) {
				model.setHypothecate(map.get(key));
			}
			if (StringUtils.contains(key, "发证日期")) {
				model.setOpeningDate(map.get(key));
			}
			if (StringUtils.contains(key, "发证机关")) {
				model.setOffice(map.get(key));
			}
			if (StringUtils.contains(key, "地上面积")) {
				model.setAboveArea(map.get(key));
			}
			if (StringUtils.contains(key, "地下面积")) {
				model.setUnderArea(map.get(key));
			}
			if (StringUtils.contains(key, "总单元套数")) {
				model.setTotalUnitCount(map.get(key));
			}
			if (StringUtils.contains(key, "报建总面积")) {
				model.setTotalUnitArea(map.get(key));
			}
			if (StringUtils.contains(key, "有效期自")) {
				model.setValidityStart(map.get(key));
			}
			if (StringUtils.contains(key, "有效期至")) {
				model.setValidityEnd(map.get(key));
			}
			if (StringUtils.contains(key, "联系人")) {
				model.setContacts(map.get(key));
			}
			if (StringUtils.contains(key, "已建层数")) {
				model.setBuildedFloods(map.get(key));
			}
			if (StringUtils.contains(key, "预售楼宇配套面积情况")) {
				model.setMatching(map.get(key));
			}
			if (StringUtils.contains(key, "报建屋数")) {
				model.setHouseNumber(map.get(key));
			}
			if (StringUtils.contains(key, "备注")) {
				model.setDesc(map.get(key));
			}
			
			
			
			
			if (StringUtils.contains(key, "住宅套数")) {
				model.setHouseCount(map.get(key));
			}
			if (StringUtils.contains(key, "住宅面积")) {
				model.setHouseArea(map.get(key));
			}
			if (StringUtils.contains(key, "商业套数")) {
				model.setBusinessCount(map.get(key));
			}
			if (StringUtils.contains(key, "商业面积")) {
				model.setBusinessArea(map.get(key));
			}
			if (StringUtils.contains(key, "办公套数")) {
				model.setOfficeCount(map.get(key));
			}
			if (StringUtils.contains(key, "办公面积")) {
				model.setOfficeArea(map.get(key));
			}
			if (StringUtils.contains(key, "车位套数")) {
				model.setCarportCount(map.get(key));
			}
			if (StringUtils.contains(key, "车位面积")) {
				model.setCarportArea(map.get(key));
			}
			if (StringUtils.contains(key, "其他套数")) {
				model.setOtherCount(map.get(key));
			}
			if (StringUtils.contains(key, "其他面积")) {
				model.setOtherArea(map.get(key));
			}
		}
	}
}
