package com.jrzh.factory.fs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.contants.UrlContants;
import com.jrzh.factory.cd.CDAreaInfoSpider;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

public class FSAreaInfoSpider {

	private static Log log = LogFactory.getLog(CDAreaInfoSpider.class);

	public static List<ReptileGtjAreaInfoModel> getAreaInfo(Integer maxPage) throws ProjectException {
		List<ReptileGtjAreaInfoModel> list = new ArrayList<>();
		for (int i = 1; i <= maxPage; i++) {
			getInfo(i, list);
		}
		return list;
	}

	public static List<ReptileGtjAreaInfoModel> getInfo(int page, List<ReptileGtjAreaInfoModel> list) {
		log.info("佛山当前数据页：【" + page + "】");
		String url;
		if (page == 1) {
			url = UrlContants.LAND_FS_LIST_URL + "/index_29108.html";
		} else {
			url = UrlContants.LAND_FS_LIST_URL + "/index_29108_" + (page - 1) + ".html";
		}
		try {
			Document doc = Jsoup.connect(url).get();
			Elements selectLi = doc.select(".ewb-con-bd").select("ul").select("li");
			Elements selectA = selectLi.select("a");
			for (int k = 0; k < selectA.size(); k++) {
				String href = selectA.eq(k).attr("href");
				href = UrlContants.LAND_FS_LIST_URL + href.substring(href.indexOf(".") + 1, href.length());
				Document data = Jsoup.connect(href).get();
				Elements selectTable = data.select("table");
				if (selectTable == null || selectTable.size() == 0) {
					continue;
				}
				Elements selectTr = selectTable.eq(0).select("tr");
				Elements resource = selectTr.select("table");
				if (resource == null || resource.size() == 0) {
					resource = selectTr;
				}
				ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
				try {
					for (int i = 0; i < resource.size(); i++) {
						Elements tds = resource.eq(i).select("td");
						for (int j = 0; j < tds.size(); j++) {
							String code = tds.eq(j).text();
							if (code.contains("编号")) {
								model.setAreaNo(tds.eq(++j).text());
								j--;
								continue;
							}
							if (code.contains("地块位置")) {
								model.setAddress(tds.eq(++j).text());
								j--;
								continue;
							}
							if (code.contains("土地用途")) {
								model.setUse(tds.eq(++j).text());
								j--;
								continue;
							}
							if (code.contains("土地面积")) {
								String value = tds.eq(++j).text();
								if (value.contains(" ")) {
									value = value.replace(" ", "");
								}
								if (code.contains("公顷")) {
									Double area = Double.parseDouble(value);
									area = area * 10000;
									value = area.toString();
								}
								model.setAcreage(value);
								j--;
								continue;
							}
							if (code.contains("成交价")) {
								String price = tds.eq(++j).text();
								if (price.contains("人民币")) {
									price = price.substring(price.indexOf("人民币") + 3, price.indexOf("万元"));
								}
								if (code.contains("元/平方米")) {
									Double money = Double.parseDouble(price);
									Double area = Double.parseDouble(model.getAcreage());
									money = money * area * 2 / 10000;
									price = money.toString();
								}
								model.setPrice(price);
								continue;
							}
							if (code.contains("受让单位")) {
								model.setBuyer(tds.eq(++j).text());
								j--;
								continue;
							}
						}
					}
					model.setCity("佛山");
					model.setUrl(href);
					model.setFrom("佛山市公共资源交易中心");
					if (model.getAddress() != null) {
						list.add(model);
					}
					model = new ReptileGtjAreaInfoModel();
				} catch (Exception e) {
					Elements selectTh = resource.eq(0).select("td");
					resource.remove(0);
					for (int x = 0; x < resource.size(); x++) {
						Elements selectTd = resource.get(x).select("td");
						for (int j = 0; j < selectTh.size() + 1; j++) {
							if (selectTd.eq(j).text().contains("备注")) {
								break;
							}
							if (j == selectTh.size()) {
								model.setCity("佛山");
								model.setFrom("佛山市公共资源交易中心");
								list.add(model);
								model = new ReptileGtjAreaInfoModel();
								break;
							}
							if (selectTh.get(j).text().contains("编号")) {
								model.setAreaNo(selectTd.get(j).text());
								continue;
							}
							if (selectTh.get(j).text().contains("位置")) {
								model.setAddress(selectTd.get(j).text());
								continue;
							}
							if (selectTh.get(j).text().contains("面积")) {
								String area = selectTd.get(j).text();
								Double temp = Double.parseDouble(area);
								// 面积换算
								if (selectTh.get(j).text().contains("公顷")) {
									temp = temp * 10000;
								}
								model.setAcreage(temp.toString());
								continue;
							}
							if (selectTh.get(j).text().contains("成交价")) {
								String price = selectTd.get(j).text();
								if (!selectTh.get(j).text().contains("万元")) {
									Double money = Double.parseDouble(price);
									money = money / 10000;
									price = money.toString();
								}
								model.setPrice(price);
								continue;
							}
							if (selectTh.get(j).text().contains("竞得人") || selectTh.get(j).text().contains("受让单位")) {
								String buyer = selectTd.get(j).text();
								model.setBuyer(buyer);
							}
						}
					}
				}
			}
			return list;
		} catch (IOException e) {
			return null;
		}

	}

}
