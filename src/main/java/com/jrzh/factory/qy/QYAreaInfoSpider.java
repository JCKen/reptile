package com.jrzh.factory.qy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.contants.UrlContants;
import com.jrzh.factory.gz.GzAreaInfoSpider;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

public class QYAreaInfoSpider {

	private static Log log = LogFactory.getLog(GzAreaInfoSpider.class);

	public static List<ReptileGtjAreaInfoModel> getInfo(Integer page) throws ProjectException {
		List<ReptileGtjAreaInfoModel> list = new ArrayList<>();
		for (int x = 1; x <= page; x++) {
			String url = UrlContants.LAND_QY_LIST_URL + "/_public/tjggg.jsp?pageNo=" + page;
			try {
				log.info("清远土地数据第【" + x + "】页");
				Document document = Jsoup.connect(url).timeout(50000).get();
				Elements selectTable = document.getElementById("tabcontent3").select("table");
				Elements selectTr = selectTable.select("tr");
				selectTr.remove(0);
				for (int i = 0; i < selectTr.size(); i++) {
					if (!selectTr.get(i).select("td").last().text().contains("已成交")) {
						continue;
					}
					Element selectTd = selectTr.get(i).selectFirst("td");
					String str = selectTd.attr("onclick");
					String dataUrl = UrlContants.LAND_QY_LIST_URL
							+ str.substring(str.indexOf("/"), str.indexOf(",") - 1);
					Document data = Jsoup.connect(dataUrl).timeout(50000).get();
					Element selectDiv = data.getElementsByClass("left").first();
					Elements selectP = selectDiv.select("p");
					ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
					try {
						for (int j = 0; j < selectP.size() + 1; j++) {
							if (j == selectP.size()) {
								model.setCity("清远");
								model.setFrom("清远市国土资源交易网");
								list.add(model);
								break;
							}
							String value = selectP.get(j).text();
							if (j == 0) {
								int index = value.indexOf("；");
								String areaNo = value.substring(value.indexOf("】") + 1, value.indexOf("号"));
								String address = value.substring(value.indexOf("位于") + 2, index);
								String area;
								if (value.contains("平方米")) {
									area = value.substring(value.indexOf("面积") + 2, value.indexOf("平方米"));
								} else {
									index = value.indexOf("；", index + 1);
									area = value.substring(value.indexOf("面积") + 2, index);
								}
								model.setAreaNo(areaNo);
								model.setAddress(address);
								model.setAcreage(area);
							}
							if (value.contains("竞得人")) {
								value = selectP.get(++j).text();
								if (value.contains(")")) {
									value = value.replace(value.substring(value.indexOf("("), value.indexOf(")") + 1), "");
								}
								j--;
								model.setBuyer(value);
							}
							if (value.contains("成交价")) {
								value = selectP.get(++j).text();
								value = value.substring(0, value.indexOf("万"));
								model.setPrice(value);
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

}
