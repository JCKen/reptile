package com.jrzh.factory.jm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.contants.UrlContants;
import com.jrzh.mvc.convert.reptile.ReptileGtjAreaInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

public class JMAreaInfoSpider {
	private static Log log = LogFactory.getLog(JMAreaInfoSpider.class);
	private static ReptileGtjAreaInfoConvert convert = new ReptileGtjAreaInfoConvert();

	public static List<ReptileGtjAreaInfoModel> getInfo(int page) {
		List<ReptileGtjAreaInfoModel> list = new ArrayList<ReptileGtjAreaInfoModel>();
		for (int i = 1; i <= page; i++) {
			log.info("爬取江门第【" + i + "】页数据");
			String url = UrlContants.LAND_JM_URL + i + ".htm";
			try {
				Document doc = Jsoup.connect(url).get();
				Element ul = doc.getElementsByClass("itemtw").first();
				Elements lis = ul.select("li");
				for (Element li : lis) {
					if(li.select("a").first()==null){
						continue;
					}
					String dataUrl = li.select("a").first().attr("href");
					if (dataUrl == null)
						continue;
					Map<String, String> map = getData(dataUrl);
					ReptileGtjAreaInfoModel model = convert.JMmapToModel(map);
					if (model == null)
						continue;
					model.setCity("江门");
					model.setFrom("江门市公共资源交易网");
					model.setUrl(dataUrl);
					list.add(model);
				}
			} catch (IOException e) {
				log.info("江门土地爬虫出错");
				e.printStackTrace();
			}
		}
		return list;
	}
	public static Map<String, String> getData(String url) {
		Map<String, String> map = new HashMap<>();
		Document doc;
		try {
			doc = Jsoup.connect(url).get();
			Element table = doc.getElementsByClass("newsCon").first();
			if (table == null)
				return null;
			Elements trs = table.select("tr");
			for (Element tr : trs) {
				Elements tds = tr.select("td");
				if (tds.size() % 2 != 0)
					continue;
				for (int i = 0; i < tds.size(); i += 2) {
					map.put(tds.get(i).text().replace(" ", ""), tds.get(i + 1).text());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}
public static void main(String[] args) {
	
	getInfo(10);
}
}
