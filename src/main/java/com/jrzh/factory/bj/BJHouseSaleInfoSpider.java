package com.jrzh.factory.bj;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.factory.base.inf.SpiderInf;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileSaleInfoModel;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseInfoModel;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

//北京房管局销售表

public class BJHouseSaleInfoSpider  implements SpiderInf<ReptileYgjyHouseInfoModel> {
	
	public static Log log = LogFactory.getLog(BJHouseSaleInfoSpider.class);
	
	public final static String DOMIN="http://www.bjjs.gov.cn/bjjs/fwgl/fdcjy/fwjy/index.shtml";

	public void saveBJHouseSaleInfo(ReptileServiceManage reptileServiceManage,
			SessionUser user) throws ProjectException {
		BJHouseSaleInfoSpider bjHouseSaleInfoSpider = new BJHouseSaleInfoSpider();
		try {
			bjHouseSaleInfoSpider.spider(DOMIN, null, 1, reptileServiceManage,user);
		} catch (ProjectException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	
	@Override
	public void spider(String url, String part, Integer index, ReptileServiceManage reptileServiceManage,
			SessionUser user) throws ProjectException {
		Date infoUpdateTime = new Date();
		try {
			ReptileSaleInfoModel model = new ReptileSaleInfoModel();
			Document doc = Jsoup.connect(DOMIN).timeout(500000).get();
			Element mainEle = doc.selectFirst("#nr_box");
			if(null == mainEle) {
				return;
			}
			Elements tableEles = mainEle.select("table");
			if(null == tableEles) {
				return ;
			}
			Long area = null;
			Double number = null;
			if(null != tableEles.get(5)) {
				Elements tdEle = tableEles.get(5).select("td");
				if(null != tdEle) {
					area = Long.valueOf(tdEle.get(2).text());
					number = Double.valueOf(tdEle.get(4).text());
				}
			}
			if(null != tableEles.get(9)) {
				Elements tdEle = tableEles.get(9).select("td");
				if(null != tdEle) {
					area += Long.valueOf(tdEle.get(2).text());
					number += Double.valueOf(tdEle.get(4).text());
				}
			}
			model.setAllArea(area.toString());
			model.setAllNumber(number.toString());
			model.setCity("北京");
			model.setInfoUpdataTime(DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd")+" 00:00:00"));
			reptileServiceManage.reptileSaleInfoService.add(model, user);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		try {
			Document doc = Jsoup.connect(DOMIN).timeout(500000).get();
			Element mainEle = doc.selectFirst("#nr_box");
			if(null == mainEle) {
				return;
			}
			Elements tableEles = mainEle.select("table");
			if(null == tableEles) {
				return ;
			}
			if(null == tableEles.get(5)) {
				return;
			}
			Long area = null;
			Double number = null;
			if(null != tableEles.get(5)) {
				Elements tdEle = tableEles.get(5).select("td");
				if(null != tdEle) {
					
					System.out.println("area 5-2:"+tdEle.get(2).text());
					System.out.println("number 5-4:"+tdEle.get(4).text());
					area = Long.valueOf(tdEle.get(2).text());
					number = Double.valueOf(tdEle.get(4).text());
				}
			}
			if(null != tableEles.get(9)) {
				Elements tdEle = tableEles.get(9).select("td");
				if(null != tdEle) {
					System.out.println("area 9-2:"+tdEle.get(2).text());
					System.out.println("number 9-4:"+tdEle.get(4).text());
					area += Long.valueOf(tdEle.get(2).text());
					number += Double.valueOf(tdEle.get(4).text());
				}
			}
			System.out.println(area);
			System.out.println(number);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
