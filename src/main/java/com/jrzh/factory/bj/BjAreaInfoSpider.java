package com.jrzh.factory.bj;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.contants.UrlContants;
import com.jrzh.mvc.convert.reptile.ReptileGtjAreaInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

/**
 * 获取北京土地数据
 * @author Administrator
 * **/
public class BjAreaInfoSpider {
	
	public static Log log = LogFactory.getLog(BjAreaInfoSpider.class);
	
	public static List<ReptileGtjAreaInfoModel> getBJAreaInfo(String endrecord) throws ProjectException {
		try {
			log.info("####获取北京土地数据本地最大数量###【"+endrecord+"】");
			List<ReptileGtjAreaInfoModel> list = new LinkedList<ReptileGtjAreaInfoModel>();
			Document document = Jsoup.connect(UrlContants.LAND_BJ_ALL_URL+"?startrecord=1&endrecord="+endrecord).timeout(3200000).get();
			Elements aTags = document.select("a");
			log.info("####获取北京土地数据开始解析###");
			for (Element a : aTags) {
				String url = a.attr("href");
				try {
					ReptileGtjAreaInfoModel model = getInfo(url.replaceFirst(".", ""));
					list.add(model);
				} catch (ProjectException e) {
					e.printStackTrace();
					continue;
				}
			}
			return list;
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}
	
	/**
	 * 解析每一个土地数据详情
	 * **/
	public static ReptileGtjAreaInfoModel getInfo(String url) throws ProjectException {
		try {
			Document AreaPage = Jsoup.connect(UrlContants.LAND_BJ_DETAIL_URL + url).timeout(50000).get();
			Element tbody = AreaPage.selectFirst("tbody");
			Elements trs = tbody.select("tr");
			Map<String, String> dataMap = new HashMap<String, String>();
			ReptileGtjAreaInfoConvert convert = new ReptileGtjAreaInfoConvert();
			for (Element tr : trs) {
				Elements tds = tr.select("td");
				if (null == tds || tds.size() != 2)
					continue;
				dataMap.put(tds.get(0).text(), tds.get(1).text());
			}
			ReptileGtjAreaInfoModel model = convert.BJmapToModel(dataMap,UrlContants.LAND_BJ_DETAIL_URL + url);  // url 只用来保存到数据库
			return model;
		} catch (Exception e) {
			log.error(UrlContants.LAND_BJ_DETAIL_URL+url+" ： 【获取失败】");
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}
}
