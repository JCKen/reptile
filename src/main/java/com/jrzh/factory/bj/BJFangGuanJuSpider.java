package com.jrzh.factory.bj;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.factory.base.inf.SpiderInf;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseInfoModel;
import com.jrzh.mvc.model.reptile.ReptileYgjyHousePreSaleCertificateModel;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseInfoSearch;
import com.jrzh.mvc.search.reptile.ReptileYgjyHousePreSaleCertificateSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;


//楼盘基本信息
public class BJFangGuanJuSpider  implements SpiderInf<ReptileYgjyHouseInfoModel> {

	private final static String PAGE_URL_FIRST = "http://www.bjjs.gov.cn/eportal/ui?pageId=307670";
	
	private final static String PAGE_URL_SECOND = "http://www.bjjs.gov.cn/eportal/ui?pageId=307674";
	
	private final static String PAGE_URL_THREE = "http://www.bjjs.gov.cn/eportal/ui?pageId=307678";
	
	private final static String PAGE_URL_FOUR= "http://www.bjjs.gov.cn/eportal/ui?pageId=307682";
	
	private final static String PAGE_URL_FIVE = "http://www.bjjs.gov.cn/eportal/ui?pageId=412697";
	
	private final static Map<String,String> urlMap = new HashMap<>();
	static {
		urlMap.put("first", PAGE_URL_FIRST);
		urlMap.put("second", PAGE_URL_SECOND);
		urlMap.put("three", PAGE_URL_THREE);
		urlMap.put("four", PAGE_URL_FOUR);
		urlMap.put("five", PAGE_URL_FIVE);
	}
	
	private static Log log = LogFactory.getLog(BJFangGuanJuSpider.class);
	
	public void saveBJFangGuanJuHouseInfo(ReptileServiceManage reptileServiceManage, SessionUser user) throws ProjectException {
		new BJFangGuanJuSpider().spider(PAGE_URL_FIRST,null,1,reptileServiceManage,user);
	}
	
	@Override
	public void spider(String url,String part, Integer index , ReptileServiceManage reptileServiceManage,
			SessionUser user) throws ProjectException {
		Date infoUpdateTime = new Date();
		try {
			Connection conn = Jsoup.connect(url).timeout(50000);
			conn.data("currentPage", String.valueOf(index));
			Document document = conn.post();
			if(null == document) {
				Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
			}
			Element normalEle = document.selectFirst(".Normal");
			if(null == normalEle) {
				Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
			}
			Element jumpPageBox = normalEle.selectFirst("#jumpPageBox");
			if(null == jumpPageBox) {
				Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
			}
			String pageDoc = jumpPageBox.attr("onchange");
			String[] pageArr = pageDoc.split(",");
			if(pageArr.length < 2) {
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
			}
			String pageCount = StringUtils.replace(pageArr[1], ")", "");
			Elements tableEle = document.getElementsByAttributeValue("rules", "all");
			if(null == tableEle) {
				Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
			}
			
			Elements tableTr = tableEle.last().select("tr");
			if(null == tableTr) {
				Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
			}
			if(null == pageCount || StringUtils.equals(pageCount, String.valueOf(index-1))) {
				return ;
			}
			for(Integer j = 1;j < tableTr.size();j++) {
				ReptileYgjyHouseInfoModel reptileYgjyHouseInfoModel = new ReptileYgjyHouseInfoModel();
				Elements tableTd = tableTr.get(j).select("td");
				if(null == tableTd || tableTd.size() < 3 ) continue;
				String houseName = tableTd.get(0).text();
				String preSalePremit = tableTd.get(1).text();
				reptileYgjyHouseInfoModel.setCity("北京");
				reptileYgjyHouseInfoModel.setProductName(houseName);
				reptileYgjyHouseInfoModel.setPreSalePermit(preSalePremit);
				reptileYgjyHouseInfoModel.setDataUpdateTime(DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd")+" 00:00:00"));
				Element houseInfoUrlEle = tableTd.get(0).selectFirst("a");
				if(null == houseInfoUrlEle) continue;
				String houseInfoUrl = "http://www.bjjs.gov.cn/" + houseInfoUrlEle.attr("href");
				reptileYgjyHouseInfoModel.setUrl(houseInfoUrl);
				Document houseDocument;
				try {
					houseDocument = Jsoup.connect(houseInfoUrl).timeout(50000).get();
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
				if(null == houseDocument) continue;
				Element projectAddress = houseDocument.selectFirst("#坐落位置");
				Element developer = houseDocument.selectFirst("#开发企业");
				if(null != projectAddress) {
					reptileYgjyHouseInfoModel.setProjectAddress(projectAddress.text());
				}
				if(null != projectAddress) {
					reptileYgjyHouseInfoModel.setDeveloper(developer.text());
				}
				Element totalAreaEle = document.selectFirst("#准许销售面积");
				if(null != totalAreaEle) {
					reptileYgjyHouseInfoModel.setLandArea(Double.valueOf(totalAreaEle.text()));
				}
				//北京房管局楼盘数据入库
				String[] projectUrlArrs = houseInfoUrl.split("&projectID=");
				String projectUrlArr = projectUrlArrs[1];
				String projectId = projectUrlArr.split("&systemID=")[0];
				ReptileYgjyHouseInfoSearch search = new ReptileYgjyHouseInfoSearch();
				search.setEqualCity("北京");
				search.setEqualProjectId(projectId);
				ReptileYgjyHouseInfoModel newReptileYgjyHouseInfoModel = reptileServiceManage.reptileYgjyHouseInfoService.first(search);
				reptileYgjyHouseInfoModel.setProjectId(projectId);
				String houseId = null;
				if(null == newReptileYgjyHouseInfoModel) {
					reptileServiceManage.reptileYgjyHouseInfoService.add(reptileYgjyHouseInfoModel, user);
					houseId = reptileYgjyHouseInfoModel.getId();
				}else {
					ReflectUtils.copySameFieldToTargetFilterNull(reptileYgjyHouseInfoModel,newReptileYgjyHouseInfoModel);
					reptileServiceManage.reptileYgjyHouseInfoService.edit(newReptileYgjyHouseInfoModel, user);
					houseId = newReptileYgjyHouseInfoModel.getId();				
				}
				//预售证保存
				Document preDocument ;
				try {
					preDocument = Jsoup.connect(reptileYgjyHouseInfoModel.getUrl()).timeout(50000).get();
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
				ReptileYgjyHousePreSaleCertificateModel reptileYgjyHousePreSaleCertificateModel = new ReptileYgjyHousePreSaleCertificateModel();
				if(null == preDocument) continue;
				Element preDotalAreaEle = document.selectFirst("#准许销售面积");
				if(null != preDotalAreaEle) {
					reptileYgjyHousePreSaleCertificateModel.setTotalAreaReport(preDotalAreaEle.text());
				}
				reptileYgjyHousePreSaleCertificateModel.setHouseId(houseId);
				ReptileYgjyHousePreSaleCertificateSearch reptileYgjyHousePreSaleCertificateSearch = new ReptileYgjyHousePreSaleCertificateSearch();
				reptileYgjyHousePreSaleCertificateSearch.setEqualHouseId(houseId);
				ReptileYgjyHousePreSaleCertificateModel newReptileYgjyHousePreSaleCertificateModel = reptileServiceManage.reptileYgjyHousePreSaleCertificateService.first(reptileYgjyHousePreSaleCertificateSearch);
				if(null ==  newReptileYgjyHousePreSaleCertificateModel) {
					reptileServiceManage.reptileYgjyHousePreSaleCertificateService.add(reptileYgjyHousePreSaleCertificateModel, user);
				}else {
					ReflectUtils.copySameFieldToTargetFilterNull(reptileYgjyHousePreSaleCertificateModel,newReptileYgjyHousePreSaleCertificateModel);
    				reptileServiceManage.reptileYgjyHousePreSaleCertificateService.edit(newReptileYgjyHousePreSaleCertificateModel, user);
				}
				Thread.sleep(1000);
			}
	        Integer page =  ++index ;
			spider(url, null, page, reptileServiceManage, user);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
}
