package com.jrzh.factory.fh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

/**
 * 奉化土地获取 
 *
 */
public class FhAreaInfoSpider {
	
	public static List<ReptileGtjAreaInfoModel> getData(){
		List<ReptileGtjAreaInfoModel> list = new ArrayList<>();
		String dataurl = "http://ztb.fh.gov.cn/";
		Document doc;
		Connection connect = Jsoup.connect("http://ztb.fh.gov.cn/Transaction_list.aspx?cls=22");
		connect.data("__VIEWSTATE", "/wEPDwUKMTA4NTc2MTI4Mg9kFgJmD2QWAgIDD2QWBAIBDxYCHglpbm5lcmh0bWwFvAHmrKLov47ov5vlhaXlpYnljJblhazlhbHotYTmupDkuqTmmJPkuK3lv4PnvZHnq5nvvIE8c3BhbiBjbGFzcz0ibWwxNSBtcjE1Ij48aW1nIHNyYz0iaW1hZ2VzL3RvcF9pY28ucG5nIiBjbGFzcz0idm0gbXI1IiBhbHQ9IiIgLz4mbmJzcDsmbmJzcDs8L3NwYW4+5LuK5aSp5piv77yaIDIwMTnlubQwNeaciDMw5pelIOaYn+acn+Wbm2QCBg9kFgoCAQ8WAh8ABQzlnJ/lnLDlh7rorqlkAgMPFgIfAAUM5oiQ5Lqk5YWs56S6ZAIPDxYCHgVjbGFzcwUHbGVmdF9hMWQCJQ8WAh4LXyFJdGVtQ291bnQCDxYeZg9kFgJmDxUFCjIwMTctMTEtMjgEODg0NwIyMlHmuqrlj6PplYfkuq3muZblkI3luq3lnLDlnZfnmoTlm73mnInlu7rorr7nlKjlnLDkvb/nlKjmnYPmjILniYzlh7rorqnnu5PmnpzlhaznpLpR5rqq5Y+j6ZWH5Lqt5rmW5ZCN5bqt5Zyw5Z2X55qE5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6ZAIBD2QWAmYPFQUKMjAxNy0xMS0yOAQ4ODQ2AjIyXiDlsJrnlLDplYfkuIvnjovmnZHlt6XkuJotMuetiTTlrpflnLDlnZfnmoTlm73mnInlu7rorr7nlKjlnLDkvb/nlKjmnYPmjILniYzlh7rorqnnu5PmnpzlhaznpLpeIOWwmueUsOmVh+S4i+eOi+adkeW3peS4mi0y562JNOWul+WcsOWdl+eahOWbveacieW7uuiuvueUqOWcsOS9v+eUqOadg+aMgueJjOWHuuiuqee7k+aenOWFrOekumQCAg9kFgJmDxUFCjIwMTctMTEtMjQEODgxNwIyMnPopb/lnZ7ooZfpgZPln47plYfkvY7mlYjnlKjlnLDlho3lvIDlj5HljYHlm5vlj7fnrYk55a6X5Zyw5Z2X55qE5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6c+ilv+Wdnuihl+mBk+WfjumVh+S9juaViOeUqOWcsOWGjeW8gOWPkeWNgeWbm+WPt+etiTnlrpflnLDlnZfnmoTlm73mnInlu7rorr7nlKjlnLDkvb/nlKjmnYPmjILniYzlh7rorqnnu5PmnpzlhaznpLpkAgMPZBYCZg8VBQoyMDE3LTExLTIxBDg3NjkCMjJT5ruo5rW35paw5Yy65bel5LiaMTnlj7flnLDlnZfnmoTlm73mnInlu7rorr7nlKjlnLDkvb/nlKjmnYPmjILniYzlh7rorqnnu5PmnpzlhaznpLpT5ruo5rW35paw5Yy65bel5LiaMTnlj7flnLDlnZfnmoTlm73mnInlu7rorr7nlKjlnLDkvb/nlKjmnYPmjILniYzlh7rorqnnu5PmnpzlhaznpLpkAgQPZBYCZg8VBQoyMDE3LTExLTIwBDg3MTUCMjJg5aWJ5YyW6Ziz5YWJ5rW35rm+WUdCLTAyLTAy562J5Zub5a6X5Zyw5Z2X55qE5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6YOWlieWMlumYs+WFiea1t+a5vllHQi0wMi0wMuetieWbm+Wul+WcsOWdl+eahOWbveacieW7uuiuvueUqOWcsOS9v+eUqOadg+aMgueJjOWHuuiuqee7k+aenOWFrOekumQCBQ9kFgJmDxUFCjIwMTctMTEtMjAEODcxNAIyMlkgIOilv+Wdnuihl+mBk+W6t+S6readkeW3peS4muS4gOWPt+WcsOWdl+eahOWbveacieW7uuiuvueUqOWcsOS9v+eUqOadg+aMgueJjOe7k+aenOWFrOekulkgIOilv+Wdnuihl+mBk+W6t+S6readkeW3peS4muS4gOWPt+WcsOWdl+eahOWbveacieW7uuiuvueUqOWcsOS9v+eUqOadg+aMgueJjOe7k+aenOWFrOekumQCBg9kFgJmDxUFCjIwMTctMTEtMTQEODcxMwIyMmTljp/lroHms6Lmnb7np4DppbDlk4HmnInpmZDlhazlj7jnrYkz5a6X5Zyw5Z2X55qE5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6ZOWOn+WugeazouadvuengOmlsOWTgeaciemZkOWFrOWPuOetiTPlrpflnLDlnZfnmoTlm73mnInlu7rorr7nlKjlnLDkvb/nlKjmnYPmjILniYzlh7rorqnnu5PmnpzlhaznpLpkAgcPZBYCZg8VBQoyMDE3LTExLTA2BDg2MTQCMjJC6ZW/5rGA5p2R5Zyw5Z2X5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6QumVv+axgOadkeWcsOWdl+WbveacieW7uuiuvueUqOWcsOS9v+eUqOadg+aMgueJjOWHuuiuqee7k+aenOWFrOekumQCCA9kFgJmDxUFCjIwMTctMTEtMDYEODYxMwIyMk/pmLPlhYnmtbfmub5ZRy1BLTA15Zyw5Z2X55qE5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6T+mYs+WFiea1t+a5vllHLUEtMDXlnLDlnZfnmoTlm73mnInlu7rorr7nlKjlnLDkvb/nlKjmnYPmjILniYzlh7rorqnnu5PmnpzlhaznpLpkAgkPZBYCZg8VBQoyMDE3LTExLTAzBDg1MzYCMjJg5aWJ5YyW5YGl5bq35peF5ri45bCP6ZWHMeWPty0x562J5YWt5a6X5Zyw5Z2X5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6YOWlieWMluWBpeW6t+aXhea4uOWwj+mVhzHlj7ctMeetieWFreWul+WcsOWdl+WbveacieW7uuiuvueUqOWcsOS9v+eUqOadg+aMgueJjOWHuuiuqee7k+aenOWFrOekumQCCg9kFgJmDxUFCjIwMTctMTAtMzEEODUzMwIyMmHopb/lnZ7ooZfpgZPkuIvpmYjlurXlt6XkuJrkuIDlj7fnrYk05a6X5Zyw5Z2X5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6Yeilv+Wdnuihl+mBk+S4i+mZiOW6teW3peS4muS4gOWPt+etiTTlrpflnLDlnZflm73mnInlu7rorr7nlKjlnLDkvb/nlKjmnYPmjILniYzlh7rorqnnu5PmnpzlhaznpLpkAgsPZBYCZg8VBQoyMDE3LTA5LTMwBDgzNTMCMjJX5ruo5rW35paw5Yy65bel5LiaMeWPt+etiTLlrpflnLDlnZcg5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6V+a7qOa1t+aWsOWMuuW3peS4mjHlj7fnrYky5a6X5Zyw5Z2XIOWbveacieW7uuiuvueUqOWcsOS9v+eUqOadg+aMgueJjOWHuuiuqee7k+aenOWFrOekumQCDA9kFgJmDxUFCjIwMTctMDktMzAEODM1MgIyMl3lroHljZfotLjmmJPnianmtYHljLrlt6XkuJoyLTHlj7flnLDlnZfnmoTlm73mnInlu7rorr7nlKjlnLDkvb/nlKjmnYPmjILniYzlh7rorqnnu5PmnpzlhaznpLpd5a6B5Y2X6LS45piT54mp5rWB5Yy65bel5LiaMi0x5Y+35Zyw5Z2X55qE5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6ZAIND2QWAmYPFQUKMjAxNy0wOS0yOQQ4MzA4AjIyUCAg5Zub5piO6KW/6Lev5Y2X5L6n5Zyw5Z2X55qE5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6UCAg5Zub5piO6KW/6Lev5Y2X5L6n5Zyw5Z2X55qE5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6ZAIOD2QWAmYPFQUKMjAxNy0wOS0yNgQ4MzAyAjIyXyDojrzmuZbplYfmoJblh6TmnZHllYbkvY/kuIDlj7fnrYky5a6X5Zyw5Z2X5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6XyDojrzmuZbplYfmoJblh6TmnZHllYbkvY/kuIDlj7fnrYky5a6X5Zyw5Z2X5Zu95pyJ5bu66K6+55So5Zyw5L2/55So5p2D5oyC54mM5Ye66K6p57uT5p6c5YWs56S6ZAInD2QWEAIHDw9kFgIeBXN0eWxlBQ1kaXNwbGF5Om5vbmU7ZAIJDw9kFgIfAwUNZGlzcGxheTpub25lO2QCCw8PZBYCHwMFDWRpc3BsYXk6bm9uZTtkAg0PD2QWAh8DBQ1kaXNwbGF5Om5vbmU7ZAIRDw8WAh4EVGV4dAUCMTVkZAITDw8WAh8EBQIyMGRkAhUPDxYCHwQFATFkZAIXDw8WAh8EBQEyZGRkXRY6YEsdJb7jNgacsen24q/6YiV2LsyOI42OCztnGqE=");
		connect.data("__VIEWSTATEGENERATOR", "AA33242C");
		connect.data("__EVENTVALIDATION", "ctl00$ContentPlaceHolder1$Pager_Backstage1$LinkButton2");
		try {
			doc = connect.post();
			Element ul = doc.getElementsByClass("tl_rcon").select("ul").first();
			Elements lis = ul.select("li");
			for (Element li : lis) {
				String dataUrl = dataurl+li.select("a").first().attr("href");
				Document dataDoc = Jsoup.connect(dataUrl).get();
				Element table = dataDoc.getElementById("ContentPlaceHolder1_d_cont").selectFirst("table");
				Elements trs = table.select("tr");
				if(trs.size()<2){
					continue;
				}
				Elements ths = trs.get(0).select("td"); // 标题
				trs.remove(0);
				for (Element tr : trs) {
					Elements tds = tr.select("td");
					if(tds.size()!=ths.size()) break;
					ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
					for (int i = 0; i < tds.size()+1; i++) {
						if(i==tds.size()){
							model.setCity("奉化");
							model.setFrom("奉化区公共资源交易中心");
							model.setUrl(dataUrl);
							list.add(model);
							break;
						}
						String title = ths.get(i).text().replace(" ", "");
						if(title.contains("地块名称")){
							model.setAddress(tds.get(i).text());
							model.setAreaNo(tds.get(i).text());
							continue;
						}
						if(title.contains("出让金额（万元）")){
							model.setPrice(tds.get(i).text());
							continue;
						}
						if(title.contains("受让单位")){
							model.setBuyer(tds.get(i).text());
							continue;
						}
						if(title.contains("面积（m2）")){
							model.setAcreage(tds.get(i).text());
							continue;
						}
						if(title.contains("国土用途")){
							model.setUse(tds.get(i).text());
							continue;
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public static void main(String[] args) {
		List<ReptileGtjAreaInfoModel> list = getData();
		for (ReptileGtjAreaInfoModel model : list) {
			System.out.println(model.getAddress()+".."+model.getPrice());
		}
	}
}
