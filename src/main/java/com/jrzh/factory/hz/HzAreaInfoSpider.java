package com.jrzh.factory.hz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSONObject;
import com.jrzh.bean.hz.DataBean;
import com.jrzh.bean.hz.ResultBean;
import com.jrzh.mvc.convert.reptile.ReptileGtjAreaInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;
import com.jrzh.util.HttpPostJsonUtils;

public class HzAreaInfoSpider {
	
	public static Log log = LogFactory.getLog(HzAreaInfoSpider.class);
	private static ReptileGtjAreaInfoConvert convert = new ReptileGtjAreaInfoConvert();
	
	public static List<ReptileGtjAreaInfoModel> getData(int maxResult){
		List<ReptileGtjAreaInfoModel> list = new ArrayList<ReptileGtjAreaInfoModel>();
		String content = HttpPostJsonUtils.post("http://www.hzgtjy.com/Index/PublicResults?size="+maxResult+"&page=1&orderBy=CLOSE_TIME-desc","");
		ResultBean result = JSONObject.parseObject(content, ResultBean.class);
		List<DataBean> datas = result.getData();
		int index=1;
		for (DataBean bean : datas) {
			log.info("获取惠州第"+(index++)+"数据");
			if(StringUtils.isBlank(bean.getWINNER()))continue; //流拍
			String url = "http://www.hzgtjy.com/Index/LandInfo/"+bean.getLANDINFO_ID();
			try {
				Document doc = Jsoup.connect(url).get();
				if(doc.getElementById("detail2")==null)continue;
				Element table = doc.getElementById("detail2");
				Map<String, String> map = new HashMap<>();
				Elements trs = table.select("tr");
				for (Element tr : trs) {
					Elements tds = tr.select("td");
					if(tds.size()%2!=0)continue;
					for (int i = 0; i < tds.size(); i+=2) {
						map.put(tds.get(i).text().replace(" ", ""),tds.get(i+1).text());
					}
				}
				ReptileGtjAreaInfoModel model = convert.HzmapToModel(map);
				model.setCity("惠州");
				model.setUrl(url);
				model.setFrom("惠州市公共资源交易中心");
				model.setBuyer(bean.getBIDDER());
				model.setPrice(bean.getOFFER());
				list.add(model);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	public static void main(String[] args) {
		List<ReptileGtjAreaInfoModel> list = getData(210);
		for (ReptileGtjAreaInfoModel modle : list) {
			System.out.println(modle.getAcreage()+",,"+modle.getBuyer()+".."+modle.getAreaNo());
		}
	}
}
