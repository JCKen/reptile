package com.jrzh.factory.sh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.jrzh.mvc.model.reptile.ReptileEstateDatumModel;
import com.jrzh.mvc.model.reptile.ReptileEstateZbModel;

/**
 * 上海宏观数据获取
 */
public class ShMacroscopicSpider {

	public static List<ReptileEstateDatumModel> getDate() {
		List<ReptileEstateDatumModel> dataList = new ArrayList<>();
		for (ReptileEstateDatumModel model : getRk()) {
			dataList.add(model);
		}
		for (ReptileEstateDatumModel model : getRj()) {
			dataList.add(model);
		}
		for (ReptileEstateDatumModel model : getXf()) {
			dataList.add(model);
		}
		for (ReptileEstateDatumModel model : getCz()) {
			dataList.add(model);
		}
		return dataList;
	}

	public static List<ReptileEstateZbModel> getZB() {
		List<ReptileEstateZbModel> list = new ArrayList<>();
		ReptileEstateZbModel zbModel = new ReptileEstateZbModel();
		zbModel.setCity("上海");
		zbModel.setMenu("总人口");
		zbModel.setZb("常住人口");
		zbModel.setClassify("总人口");
		zbModel.setUnit("万人");
		zbModel.setCode("SHA0101");
		list.add(zbModel);

		zbModel = new ReptileEstateZbModel();
		zbModel.setCity("上海");
		zbModel.setMenu("国民经济");
		zbModel.setClassify("全市生产总值");
		zbModel.setZb("生产总值");
		zbModel.setUnit("亿元");
		zbModel.setCode("SHA0201");
		list.add(zbModel);

		zbModel = new ReptileEstateZbModel();
		zbModel.setCity("上海");
		zbModel.setMenu("国民经济");
		zbModel.setClassify("人均生产总值");
		zbModel.setZb("人均生产总值");
		zbModel.setUnit("元");
		zbModel.setCode("SHA0202");
		list.add(zbModel);

		zbModel = new ReptileEstateZbModel();
		zbModel.setCity("上海");
		zbModel.setMenu("国民经济");
		zbModel.setClassify("人均生产总值");
		zbModel.setZb("人均生产总值(上年=100)");
		zbModel.setUnit("");
		zbModel.setCode("SHA0203");
		list.add(zbModel);

		zbModel = new ReptileEstateZbModel();
		zbModel.setCity("上海");
		zbModel.setMenu("财政");
		zbModel.setClassify("上海市财政收支总额");
		zbModel.setZb("财政收入");
		zbModel.setUnit("亿元");
		zbModel.setCode("SHA0401");
		list.add(zbModel);

		zbModel = new ReptileEstateZbModel();
		zbModel.setCity("上海");
		zbModel.setMenu("财政");
		zbModel.setClassify("上海市财政收支总额");
		zbModel.setUnit("亿元");
		zbModel.setZb("财政支出");
		zbModel.setCode("SHA0402");
		list.add(zbModel);

		zbModel = new ReptileEstateZbModel();
		zbModel.setCity("上海");
		zbModel.setMenu("国内贸易");
		zbModel.setClassify("社会消费品零售额");
		zbModel.setZb("社会消费品零售总额");
		zbModel.setUnit("亿元");
		zbModel.setCode("SHA0401");
		list.add(zbModel);

		zbModel = new ReptileEstateZbModel();
		zbModel.setCity("上海");
		zbModel.setMenu("国民经济");
		zbModel.setClassify("居民消费水平");
		zbModel.setZb("农村居民消费指数(1978=100)");
		zbModel.setUnit("");
		zbModel.setCode("SHA0301");
		list.add(zbModel);

		zbModel = new ReptileEstateZbModel();
		zbModel.setCity("上海");
		zbModel.setMenu("国民经济");
		zbModel.setClassify("居民消费水平");
		zbModel.setZb("城镇居民消费指数(1978=100)");
		zbModel.setCode("SHA0302");
		zbModel.setUnit("");
		list.add(zbModel);

		zbModel = new ReptileEstateZbModel();
		zbModel.setCity("上海");
		zbModel.setMenu("国民经济");
		zbModel.setClassify("居民消费水平");
		zbModel.setZb("居民消费水平");
		zbModel.setCode("SHA0303");
		zbModel.setUnit("元");
		list.add(zbModel);

		return list;

	}

	// 获取人口
	public static List<ReptileEstateDatumModel> getRk() {
		List<ReptileEstateDatumModel> dataList = new ArrayList<>();
		ReptileEstateDatumModel dataModel = new ReptileEstateDatumModel();
		try {
			Document data = Jsoup.connect("http://www.stats-sh.gov.cn/tjnj/2018tjnj/C02/C0201B.htm").get();
			Elements trs = data.select("tr");
			int index = 0;
			for (int i = trs.size() - 1; i > 1; i--) {
				index++;
				if (index == 5) {
					return dataList; // 只拿前五条数据
				}
				Elements tds = trs.get(i).select("td");
				if (tds.eq(0).text().length() != 4) {
					index--;
					continue;
				}
				dataModel = new ReptileEstateDatumModel();
				dataModel.setTime(tds.eq(0).text());
				dataModel.setCode("SHA0101");
				dataModel.setData(trim(tds.eq(1).text()));
				dataList.add(dataModel);
			}
			return dataList;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	// 和生产总值

	// 人均 和生产总值
	public static List<ReptileEstateDatumModel> getRj() {
		List<ReptileEstateDatumModel> dataList = new ArrayList<>();
		ReptileEstateDatumModel dataModel = new ReptileEstateDatumModel();
		try {
			Document data = Jsoup.connect("http://www.stats-sh.gov.cn/tjnj/2018tjnj/C04/C0405B.htm").get();
			Elements trs = data.select("tr");
			int index = 0;
			for (int i = trs.size() - 1; i > 1; i--) {
				index++;
				if (index == 5) {
					return dataList;
				}
				Elements tds = trs.get(i).select("td");
				if (tds.eq(0).text().length() != 4) {
					index--;
					continue;
				}
				dataModel = new ReptileEstateDatumModel();
				dataModel.setTime(tds.eq(0).text());
				dataModel.setCode("SHA0201");
				dataModel.setData(trim(tds.eq(1).text()));
				dataList.add(dataModel);
				// - 另一个 值
				dataModel = new ReptileEstateDatumModel();
				dataModel.setTime(tds.eq(0).text());
				dataModel.setCode("SHA0202");
				dataModel.setData(trim(tds.eq(2).text()));
				dataList.add(dataModel);
				// - 另一个值
				dataModel = new ReptileEstateDatumModel();
				dataModel.setTime(tds.eq(0).text());
				dataModel.setCode("SHA0203");
				dataModel.setData(trim(tds.eq(4).text()));
				dataList.add(dataModel);
			}
			return dataList;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// 居民消费水平
	public static List<ReptileEstateDatumModel> getXf() {
		List<ReptileEstateDatumModel> dataList = new ArrayList<>();
		ReptileEstateDatumModel dataModel = new ReptileEstateDatumModel();
		try {
			Document data = Jsoup.connect("http://www.stats-sh.gov.cn/tjnj/2018tjnj/C04/C0414B.htm").get();
			Elements trs = data.select("tr");
			int index = 0;
			for (int i = trs.size() - 1; i > 1; i--) {
				index++;
				if (index == 5) {
					return dataList; // 只拿前五条数据
				}
				Elements tds = trs.get(i).select("td");
				if (tds.eq(0).text().length() != 4) {
					index--;
					continue;
				}
				dataModel = new ReptileEstateDatumModel();
				dataModel.setTime(tds.eq(0).text());
				dataModel.setCode("SHA0301");
				dataModel.setData(trim(tds.eq(5).text()));
				dataList.add(dataModel);

				dataModel = new ReptileEstateDatumModel();
				dataModel.setTime(tds.eq(0).text());
				dataModel.setCode("SHA0302");
				dataModel.setData(trim(tds.eq(6).text()));
				dataList.add(dataModel);

				dataModel = new ReptileEstateDatumModel();
				dataModel.setTime(tds.eq(0).text());
				dataModel.setCode("SHA0303");
				dataModel.setData(trim(tds.eq(1).text()));
				dataList.add(dataModel);
			}
			return dataList;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	// 财政收支
	public static List<ReptileEstateDatumModel> getCz() {
		List<ReptileEstateDatumModel> dataList = new ArrayList<>();
		ReptileEstateDatumModel dataModel = new ReptileEstateDatumModel();
		try {
			Document data = Jsoup.connect("http://www.stats-sh.gov.cn/tjnj/2018tjnj/C05/C0501B.htm").get();
			Elements trs = data.select("tr");
			int index = 0;
			for (int i = trs.size() - 1; i > 1; i--) {
				index++;
				if (index == 5) {
					return dataList; // 只拿前五条数据
				}
				Elements tds = trs.get(i).select("td");
				if (tds.eq(0).text().length() != 4) {
					index--;
					continue;
				}
				dataModel = new ReptileEstateDatumModel();
				dataModel.setTime(tds.eq(0).text());
				dataModel.setCode("SHA0401");
				dataModel.setData(trim(tds.eq(1).text()));
				dataList.add(dataModel);
				dataModel = new ReptileEstateDatumModel();
				dataModel.setTime(tds.eq(0).text());
				dataModel.setCode("SHA0402");
				dataModel.setData(trim(tds.eq(4).text()));
				dataList.add(dataModel);
			}
			return dataList;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	public static String trim(String str) {
		return str.replace(" ", "");
	}
}
