package com.jrzh.factory.sh;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jrzh.bean.SHProject.JsonProjectBean;
import com.jrzh.bean.SHProject.Project;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.factory.base.inf.SpiderInf;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileHouseProjectNameModel;
import com.jrzh.mvc.search.reptile.ReptileHouseProjectNameSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.tools.MyPayUtils;

public class SHFangGuanJuSpider  implements SpiderInf<ReptileHouseProjectNameModel> {

	private final static String URL = "http://www.fangdi.com.cn/service/freshHouse/getHosueList.action";
	
	private final static String PROJECT_URL = "http://www.fangdi.com.cn/service/freshHouse/queryProjectById.action";
	
	private final static Pattern PATTERN = Pattern.compile("'(.*?)'") ;
	
	private static Log log = LogFactory.getLog(SHFangGuanJuSpider.class);
	
	 public static PhantomJSDriver getPhantomJSDriver(){
        //设置必要参数
        DesiredCapabilities dcaps = new DesiredCapabilities();
        //ssl证书支持
        dcaps.setCapability("acceptSslCerts", true);
        //截屏支持
        dcaps.setCapability("takesScreenshot", false);
        //css搜索支持
        dcaps.setCapability("cssSelectorsEnabled", true);
        //js支持
        dcaps.setJavascriptEnabled(true);
        //驱动支持
        dcaps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"D:\\phantomjs\\bin\\phantomjs.exe");

        PhantomJSDriver driver = new PhantomJSDriver(dcaps);
        return  driver;
    }
	 
	 
	 public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");

        ChromeOptions c = new ChromeOptions();
//        c.addArguments("--headless");
        WebDriver driver = new ChromeDriver(c);
        driver.get("http://www.fangdi.com.cn/new_house/new_house_list.html");


        System.out.println(driver.getPageSource());
		 
		try {
			Thread.sleep(100000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        driver.quit();
    }
	 
	public  List<ReptileHouseProjectNameModel> saveSHFangGuanJuProjectInfo(ReptileServiceManage reptileServiceManage, SessionUser user) throws ProjectException {
		List<ReptileHouseProjectNameModel> models = new ArrayList<>();
		new SHFangGuanJuSpider().spider(URL, null, 1, reptileServiceManage,user);
		for(ReptileHouseProjectNameModel model : models){
			String projectId =  model.getProjectId();
			ReptileHouseProjectNameSearch search = new ReptileHouseProjectNameSearch();
			search.setEqualCityName("上海");
			search.setEqualProjectId(projectId);
			ReptileHouseProjectNameModel reptileHouseProjectNameModel = reptileServiceManage.reptileHouseProjectNameService.first(search);
			if(null == reptileHouseProjectNameModel) {
				reptileServiceManage.reptileHouseProjectNameService.add(model, user);
			}else {
				ReflectUtils.copySameFieldToTargetFilterNull(model,reptileHouseProjectNameModel);
				reptileServiceManage.reptileHouseProjectNameService.edit(reptileHouseProjectNameModel, user);
			}
		}
		return models;
	}
	
	
	
	@Override
	public void spider(String url,String part, Integer index ,ReptileServiceManage reptileServiceManage,
			SessionUser user) throws ProjectException {
		Date infoUpdateTime = new Date();
		try {
			Connection connect = Jsoup.connect(url).timeout(50000);
			connect.data("currentPage",String.valueOf(index));
			Document document = connect.post();
	        //获取总页数
	        if(null == document.select(".page_current")) {
	        	Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
	        }
	        if(null == document.select(".page_current").select(".page_search")) {
	        	Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
	        }
	        if(null == document.select(".page_current").select(".page_search").select("span")) {
	        	Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
	        }
	        if(null == document.select(".page_current").select(".page_search").select("span").select("input")) {
	        	Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
	        }
	        if(null == document.select(".page_current").select(".page_search").select("span").select("input").get(1)) {
	        	Thread.sleep(1000);
				Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
	        }
	        String ele =  document.select(".page_current").select(".page_search").select("span").select("input").get(1).attr("value");
	        String allPage = StringUtils.substring(ele, 2, ele.length()-3);
	        //当当前页数等于最大页，则结束
	        log.info("page=============" + index);
	        if(StringUtils.isEmpty(allPage) || StringUtils.equals(String.valueOf(index-1), allPage)) {
	        	log.info("结束了");
	        	return;
	        }
	        Elements houseEles = document.select(".default_row_tr");
	        if(null == houseEles) {
	        	Thread.sleep(1000);
	        	Integer page =  ++index ;
				spider(url, null, page, reptileServiceManage, user);
	        }
	        for(Element e : houseEles) {
	        	Element eleUrl = e.selectFirst("a");
	        	if(null == eleUrl) {
	        		continue;
	        	}
	        	Matcher matcher = PATTERN.matcher(eleUrl.attr("onclick"));
	        	if(matcher.find()) {
	        		//请求上海房管局数据接口
	        		String houseDetailId =  StringUtils.replace(matcher.group(), "'", "");
	        		String requestContent = MyPayUtils.sendHttp(houseDetailId,null,null, PROJECT_URL, 20000, 50000);
	        		Object status = JSONObject.parseObject(requestContent).get("result");
					if(null != status) continue;
					JsonProjectBean JsonProjectBean = JSON.parseObject(requestContent, JsonProjectBean.class);
					Project project = JsonProjectBean.getProject();
					ReptileHouseProjectNameModel reptileHouseProjectNameModel = new ReptileHouseProjectNameModel();
					reptileHouseProjectNameModel.setProjectId(houseDetailId);
					reptileHouseProjectNameModel.setCityName("上海");
					reptileHouseProjectNameModel.setInfoUpdateTime(DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd")+" 00:00:00"));
					reptileHouseProjectNameModel.setAreaName(project.getDistrictname());
					reptileHouseProjectNameModel.setProjectName(project.getProjectname());
					reptileHouseProjectNameModel.setDeveloper(project.getName());
					//总套数
					reptileHouseProjectNameModel.setProjectTotalNumber(project.getNum());
	        		//总面积
					reptileHouseProjectNameModel.setProjectTotalArea(project.getArea());
					//住宅套数
					reptileHouseProjectNameModel.setProjectHouseNumber(project.getZ_num());
					//住宅面积
					reptileHouseProjectNameModel.setProjectHouseArea(project.getZ_area());
					//可售总套数
					reptileHouseProjectNameModel.setProjectTotalHouseUnsoldNumber(project.getLeaving_num());
					//可售总面积
					reptileHouseProjectNameModel.setProjectTotalHouseUnsoldArea(project.getLeaving_area());
					//可售住宅套数
					reptileHouseProjectNameModel.setProjectHouseUnsoldNumber(project.getZ_leaving_num());
					//可售住宅面积
					reptileHouseProjectNameModel.setProjectHouseUnsoldArea(project.getZ_leaving_area());
					//已售总套数
					reptileHouseProjectNameModel.setProjectTotalHouseSoldNumber(project.getSell_num());
					//已售总面积
					reptileHouseProjectNameModel.setProjectTotalHouseSoldArea(project.getSell_area());
					//已售住宅套数
					reptileHouseProjectNameModel.setProjectHouseSoldNumber(project.getZ_sell_num());
					//已售住宅面积
					reptileHouseProjectNameModel.setProjectHouseSoldArea(project.getZ_sell_area());
					//已登记总套数
					reptileHouseProjectNameModel.setProjectTotalHouseSoldNumber(project.getReg_num());
					//已登记总面积
					reptileHouseProjectNameModel.setProjectRegisterTotalHouseNumber(project.getReg_area());
					//已登记住宅套数
					reptileHouseProjectNameModel.setProjectRegisterTotalHouseArea(project.getZ_reg_num());
					//已登记住宅面积
					reptileHouseProjectNameModel.setProjectHouseUnsoldArea(project.getZ_reg_area());
					
					//保存上海房管局项目信息
					ReptileHouseProjectNameSearch search = new ReptileHouseProjectNameSearch();
					search.setEqualCityName("上海");
					search.setEqualProjectId(houseDetailId);
					ReptileHouseProjectNameModel newReptileHouseProjectNameModel = reptileServiceManage.reptileHouseProjectNameService.first(search);
					if(null == newReptileHouseProjectNameModel) {
						reptileServiceManage.reptileHouseProjectNameService.add(reptileHouseProjectNameModel, user);
					}else {
						ReflectUtils.copySameFieldToTargetFilterNull(reptileHouseProjectNameModel,newReptileHouseProjectNameModel);
						reptileServiceManage.reptileHouseProjectNameService.edit(reptileHouseProjectNameModel, user);
					}
					Thread.sleep(1000);
	        	}
	        }
	        Integer page =  ++index ;
			spider(url, part, page, reptileServiceManage, user);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}
	
	
}
