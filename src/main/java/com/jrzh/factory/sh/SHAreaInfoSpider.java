package com.jrzh.factory.sh;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.alibaba.fastjson.JSON;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.mvc.convert.reptile.ReptileGtjAreaInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;
import com.jrzh.mvc.view.reptile.shanghaiAreaBean.AreaInfoRootBean;
import com.jrzh.mvc.view.reptile.shanghaiAreaBean.PageRootBean;
import com.jrzh.mvc.view.reptile.shanghaiAreaBean.PageUnit;

public class SHAreaInfoSpider {
	private static Log log = LogFactory.getLog(SHAreaInfoSpider.class);

	public static List<ReptileGtjAreaInfoModel> getBianhs(Integer index, List<ReptileGtjAreaInfoModel> list)
			throws ProjectException {
		try {
			log.info("###上海土地当前抓取页数" + index);
			Document doc = Jsoup.connect("http://www.shgtj.gov.cn/i/tdsc/dklb/?pn=" + index + "&ps=50&zt=1").timeout(50000).get();
			String info = doc.selectFirst("body").text();
			PageRootBean p = JSON.parseObject(info, PageRootBean.class);
			for (PageUnit u : p.getData().getList()) {
				try {
					ReptileGtjAreaInfoModel model = getAreaInfo(u.getBianh());
					if (null != model)
						list.add(model);
				} catch (ProjectException e) {
					continue;
				}
			}
			return list;
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}

	public static ReptileGtjAreaInfoModel getAreaInfo(String id) throws ProjectException {
		try {
			String year = StringUtils.substring(id, 0, 4);
			Date time = DateUtil.format(year, "YYYY");
			Document doc = Jsoup.connect("http://www.shgtj.gov.cn/i/tdsc/crdk/?id=" + id).timeout(50000).get();
			String resouce = doc.selectFirst("body").text();
			AreaInfoRootBean a = JSON.parseObject(resouce, AreaInfoRootBean.class);
			ReptileGtjAreaInfoModel r = new ReptileGtjAreaInfoConvert().SHBeanToInfoModel(a.getData());
			r.setTime(time);
			r.setUrl("http://www.shgtj.gov.cn/i/tdsc/crdk/?id=" + id);
			r.setCity("上海");
			r.setFrom("上海市规划和自然资源局");
			return r;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}

	public static List<ReptileGtjAreaInfoModel> getSHArea(Integer maxPage) {
		List<ReptileGtjAreaInfoModel> list = new LinkedList<ReptileGtjAreaInfoModel>();
		for (Integer i = 1; i <= maxPage; i++) {
			try {
				getBianhs(i, list);
			} catch (ProjectException e) {
				e.printStackTrace();
				continue;
			}
		}
		return list;
	}
}
