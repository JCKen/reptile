package com.jrzh.factory.gj;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.jrzh.bean.macroscopic.estate.DataNodeBean;
import com.jrzh.bean.macroscopic.estate.NodesBean;
import com.jrzh.bean.macroscopic.estate.ParentBean;
import com.jrzh.bean.macroscopic.estate.ReturnData;
import com.jrzh.bean.macroscopic.estate.WdNodeBean;
import com.jrzh.util.HttpPostJsonUtils;

/**
 * 国家数据房地产指标的获取方法
 * @author admin
 */
public class EstateBase {
	public static List<DataNodeBean> getData(String url) {
		String content = HttpPostJsonUtils.post(url,"");
		try {
			ParentBean bean = JSONObject.parseObject(content, ParentBean.class);
			ReturnData returnData = bean.getReturndata();
			if (returnData == null) {
				System.out.println("----------------");
				return null;
			}
			return returnData.getDatanodes();
		} catch (Exception e) {
			return null;
		}
		
	}

	public static List<NodesBean> getZb(String url) {
		String content = HttpPostJsonUtils.post(url, "");
		try {
			ParentBean bean = JSONObject.parseObject(content, ParentBean.class);
			ReturnData returnData = bean.getReturndata();
			if (returnData == null) {
				System.out.println("----------------");
				return null;
			}
			WdNodeBean wdnode = returnData.getWdnodes().get(0);
			if (wdnode == null) {
				System.out.println("*********************");
				return null;
			}
			List<NodesBean> nodes = wdnode.getNodes(); //   指标
			return nodes;
		} catch (Exception e) {
			return null;
		}
		
	}

}
