package com.jrzh.factory.xa;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

public class XaAreaInfoSider {
	
	public static Log log = LogFactory.getLog(XaAreaInfoSider.class);

	public static List<ReptileGtjAreaInfoModel> getInfo(Integer page) {
		List<ReptileGtjAreaInfoModel> list = new ArrayList<ReptileGtjAreaInfoModel>();
		try {
			for (int x = 1; x <= page; x++) {
				
				Document doc = Jsoup.connect("http://117.35.182.234:8080/resultShow.jsp?pageNo=" + x).get();
				log.info("获取西安第【"+x+"】页数据");
				Element table = doc.getElementById("dataTable");
				Elements trs = table.select("tr");
				Elements ths = trs.get(0).select("td");
				trs.remove(0);
				trs.remove(0);
				for (Element tr : trs) {
					Elements tds = tr.select("td");
					ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
					for (int i = 0; i < tds.size(); i++) {
						String title = ths.get(i).text().replace(" ", "");
						String content = tds.get(i).text().replace(" ", "");
						if (title.contains("操作")) {
							if (!content.contains("已成交")){
								model = new ReptileGtjAreaInfoModel();
								break;
							}
							String dataUrl = tds.get(i).attr("onclick");
							dataUrl = "http://117.35.182.234:8080/"
									+ dataUrl.substring(dataUrl.indexOf("'") + 1, dataUrl.indexOf(",") - 1);
							Map<String, String> map = getData(dataUrl);
							model.setUse(map.get("用途"));
							String price = map.get("成交价");
							if (price.contains("万元")) {
								model.setPrice(price.substring(0, price.indexOf("万元")));
							}else{
								model.setPrice(price);
							}
							String areage = map.get("面积");
							if (areage.contains("平方米")) {
								model.setAcreage(areage.substring(0, areage.indexOf("平方米")));
							}else{
								model.setAcreage(areage);
							}
							String roin = map.get("容积率");
							if (!roin.contains("文件")) {
								model.setPlotRatio(roin);
							}
							model.setAddress(map.get("地块坐落"));
							model.setCity("西安");
							model.setFrom("西安市国土资源网上交易系统");
							model.setUrl(dataUrl);
						}
						if (title.contains("资源编号")) {
							model.setAreaNo(content);
							continue;
						}
						if (title.contains("成交价(万元)")) {
							model.setPrice(content);
							continue;
						}
						if (title.contains("竞得人")) {
							model.setBuyer(content);
							continue;
						}
					}
					if (model != null) {
						list.add(model);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static Map<String, String> getData(String url) {
		Map<String, String> map = new HashMap<>();
		try {
			Document doc = Jsoup.connect(url).get();
			Element table = doc.select("table").last();
			Elements trs = table.select("tr");
			for (Element tr : trs) {
				Elements tds = tr.select("td");
				if (tds.size() % 2 != 0)
					continue;
				for (int i = 0; i < tds.size(); i += 2) {
					map.put(tds.get(i).text().replace(" ", ""), tds.get(i + 1).text());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}

	public static void main(String[] args) {

		List<ReptileGtjAreaInfoModel> list = getInfo(2);
		for (ReptileGtjAreaInfoModel model : list) {
			System.out.println(model.getAreaNo() + ".." + model.getPrice() + ".." + model.getAddress());
		}
	}
}
