package com.jrzh.factory.dg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSONObject;
import com.jrzh.bean.dg.DGLandBean;
import com.jrzh.bean.dg.RowBean;
import com.jrzh.contants.UrlContants;
import com.jrzh.factory.cd.CDAreaInfoSpider;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;
import com.jrzh.util.HttpPostJsonUtils;

public class DGAreaInfoSpider {
	private static Log log = LogFactory.getLog(CDAreaInfoSpider.class);

	public static List<ReptileGtjAreaInfoModel> getAreaInfo(int number){
		List<ReptileGtjAreaInfoModel> list = new ArrayList<>();
		for (int i = 0; i <= number; i++) {
			 getInfo(i,list);
		}
		return list;
	}
	
	public static List<ReptileGtjAreaInfoModel> getInfo(int number,List<ReptileGtjAreaInfoModel> list) {
		log.info("抓取东莞第【"+number+"】条数据");
		String content = HttpPostJsonUtils.post(
				"https://list.dgzb.com.cn/dggt/HomePage/publishList?pagesize="+number+"&currentpage=1&search=%7B%22startDate%22%3A%22%22%2C%22endDate%22%3A%22%22%2C%22extVal%22%3A%22%22%2C%22extVal2%22%3A%22%22%2C%22extVal3%22%3A%22%22%2C%22extVal4%22%3A%22%22%2C%22extVal5%22%3A%22%22%2C%22extVal6%22%3A-999999%2C%22extVal7%22%3A-999999%2C%22code%22%3A%2279%22%2C%22name%22%3A%22%22%7D",
				"");
		DGLandBean bean = JSONObject.parseObject(content, DGLandBean.class);
		List<RowBean> rows = bean.getRows();
		for (RowBean rowBean : rows) {
			String dataUrl = UrlContants.LAND_DG_DATA_URL + rowBean.getId();
			try {
				Document doc = Jsoup.connect(dataUrl).get();
				Elements resource = doc.select("table").select("tr");
				if (resource == null || resource.size() == 0) {
					continue;
				}
				Elements selectTh = resource.select("th");
				if (selectTh == null || selectTh.size() == 0) {
					selectTh = resource.eq(0).select("td");
				}
				resource.remove(0);
				for (int i = 0; i < resource.size(); i++) {
					ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
					Elements tds = resource.eq(i).select("td");
					for (int j = 0; j < selectTh.size() + 1; j++) {
						if(j == selectTh.size()){
							model.setUrl(dataUrl);
							model.setCity("东莞");
							model.setFrom("东莞市公共资源交易网");
							list.add(model);
							break;
						}
						String code = selectTh.eq(j).text();
						if (code.contains("地块编号")) {
							model.setAreaNo(tds.eq(j).text());
							continue;
						}
						if (code.contains("位置")) {
							model.setAddress(tds.eq(j).text());
							continue;
						}
						if (code.contains("面积")) {
							String value = tds.eq(j).text();
							if (value.contains(" ")) {
								value = value.replace(" ", "");
							}
							if (value.contains(",")) {
								value = value.replace(",", "");
							}
							if(code.contains("公顷")){
								Double area = Double.parseDouble(value);
								area = area * 10000;
								value = area.toString();
							}
							model.setAcreage(value);
							continue;
						}
						if (code.contains("土地用途")){
							model.setUse(tds.eq(j).text());
						}
						if (code.contains("成交价")) {
							String price = tds.eq(j).text();
							if (price.contains(",")) {
								price = price.replace(",", "");
							}
							if(price.contains(""))
							model.setPrice(price);
							continue;
						}
						if (code.contains("受让单位")|| code.contains("竞得人")) {
							model.setBuyer(tds.eq(j).text());
							continue;
						}
					}
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
}
