package com.jrzh.factory.zh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.contants.UrlContants;
import com.jrzh.factory.cd.CDAreaInfoSpider;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

public class ZHAreaInfoSpider {

	private static Log log = LogFactory.getLog(CDAreaInfoSpider.class);
	public static List<ReptileGtjAreaInfoModel> getInfo(int page) {
		List<ReptileGtjAreaInfoModel> list = new ArrayList<>();
		for (int i = 1; i <= page; i++) {
			log.info("获取珠海第" + i + "页数据");
			String url;
			if (i == 1) {
				url = UrlContants.LAND_ZH_LIST_URL + "/index.jhtml";
			} else {
				url = UrlContants.LAND_ZH_LIST_URL + "/index_" + i + ".jhtml";
			}
			Document doc;
			try {
				doc = Jsoup.connect(url).get();
				Elements elems = doc.getElementsByClass("rl-box-right"); // 获取列表的父级div
				Elements selectTable = elems.select("ul").select("li"); // 获取列表的table
				Elements selectA = selectTable.select("a"); // 获取列表的a标签
				Iterator<Element> elIterable = selectA.iterator();
				while (elIterable.hasNext()) {
					String dataUrl = elIterable.next().attr("href"); // 数据页面的链接
					Document dataDoc;
					// 连接超时 
					try {
						dataDoc = Jsoup.connect(dataUrl).get(); // 数据页面
					} catch (Exception e) {
						try {
							dataDoc = Jsoup.connect(dataUrl).get(); // 数据页面
						} catch (Exception e2) {
							continue;
						}
					}
					Element Table = dataDoc.select(".newsCon").select("table").last();
					if (Table == null) {
						continue;
					}
					Element dataTable = Table.selectFirst("table"); // 获取table
																	// 里的
					Elements selectTr = dataTable.select("tr");
					// 排除列表标题
					if (selectTr == null || selectTr.size() <= 1) {
						continue;
					}
					Elements selectTh = selectTr.select("th");
					if (selectTh.size() <= 0 || selectTh == null) {
						selectTh = selectTr.get(0).select("td");
					}
					selectTr.remove(0);
					for (int k = 0; k < selectTr.size(); k++) {
						Elements selectTd = selectTr.get(k).select("td");
						// 流拍
						if (selectTd.size() < selectTh.size()) {
							continue;
						}
						ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
						for (int j = 0; j < selectTh.size() + 1; j++) {
							if (j == selectTh.size()) {
								model.setUrl(dataUrl);
								model.setCity("珠海");
								model.setFrom("珠海市公共资源交易中心");
								list.add(model);
								break;
							}
							if (selectTh.get(j).text().contains("宗地编号") || selectTh.get(j).text().contains("交易序号")) {
								model.setAreaNo(selectTd.get(j).text());
							}
							if (selectTh.get(j).text().contains("位置")) {
								model.setAddress(selectTd.get(j).text());
							}
							if (selectTh.get(j).text().contains("面积")) {
								String area = selectTd.get(j).text();
								if (selectTh.get(j).text().contains("公顷")) {
									Double temp = Double.parseDouble(area);
									temp = temp * 10000;
									area = temp.toString();
								}
								model.setAcreage(area);
							}
							if (selectTh.get(j).text().contains("用途")) {
								model.setUse(selectTd.get(j).text());
							}
							if (selectTh.get(j).text().contains("竞得人")) {
								String buyer = selectTd.get(j).text();
								model.setBuyer(buyer);
							}
							if (selectTh.get(j).text().contains("成交价")) {
								String price = selectTd.get(j).text();
								if (price.contains(",")) {
									price = price.replace(",", "");
								}
								if (price.contains("，")) {
									price = price.replace("，", "");
								}
								if (!selectTh.get(j).text().contains("万元")) {
									Double money = Double.parseDouble(price);
									money = money / 10000;
									price = money.toString();
								}
								model.setPrice(price.toString());
							}
						}
					}
				}
			} catch (IOException e) {
				continue;
			}
		}
		return list;

	}

}
