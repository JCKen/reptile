package com.jrzh.factory.tj;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.contants.UrlContants;
import com.jrzh.mvc.convert.reptile.ReptileGtjAreaInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

public class TJAreaSpider {

	public static Log log = LogFactory.getLog(TJAreaSpider.class);

//	private final static Pattern REGEX_PRICE_WORD = Pattern.compile("成交价:(.*?)元");
//
//	private final static Pattern REGEX_ADDRESS_WORD = Pattern.compile("地址: (.*?) ");
//
//	private final static Pattern REGEX_AREA_WORD = Pattern.compile("土地面积: (.*?)m²");
//
//	private final static Pattern REGEX_AREA_NO_WORD = Pattern.compile("[A-Z]\\d{3,7}-\\d{3,7}");
//
//	private final static Pattern REGEX_AREA_BUYER_WORD = Pattern.compile("竞得人:(.*?) ");
//
//	private final static Pattern REGEX_AREA_PLOT_RATIO_WORD = Pattern.compile("容积率:(.*?) ");
	
	public static List<ReptileGtjAreaInfoModel> getInfo(Integer page, List<ReptileGtjAreaInfoModel> infoList)
			throws ProjectException, IOException, InterruptedException {
		try {
			log.info("#######天津土地当前页数#########【" + page + "】");
			String url = UrlContants.LAND_TJ_LIST_URL + page;
			Document pageDocument = Jsoup.connect(url).timeout(50000).get();
			Elements elements = pageDocument.getElementsByClass("rList fr");
			// 获取列表li
			Elements selectLi = elements.select("ul").first().select("li");
			Iterator<Element> elIterator = selectLi.iterator();
			while (elIterator.hasNext()) {
				String urls = "http://www.tjlandmarket.com";
				String href = elIterator.next().select("a").attr("href");
				Document datas = Jsoup.connect(urls + href).timeout(50000).get();// 请求到具体数据页
				Elements p = datas.select("dl").select("p");
				if (p.size() == 0 || p == null) {
					p = datas.select("tr");
				}
				HashMap<String, String> span = new HashMap<String, String>();
				for (int i = 0; i < p.size(); i++) {
					Elements spans = p.get(i).select("span");
					if (spans.size() == 0 || spans == null) {
						spans = p.get(i).select("td");
					}
					String spa = spans.text().replace(" ", ""); ////  内容
					if (spa.contains("供地方式") || spa.contains("年限")) {
						continue;
					}
					if (spans.text().indexOf("：") != -1 || spans.text().indexOf(":") != -1) {
						int index = -1;
						if (spans.text().indexOf("：") != -1) {
							index = spa.indexOf("：");
						}
						if (spans.text().indexOf(":") != -1) {
							index = spa.indexOf(":");
						}

						if (spa.contains("地块编号")) {
							spa = spa.substring(index + 1);
							span.put("areaNo", spa);
							continue;
						}
						if (spa.contains("容积率")) {
							spa = spa.substring(index + 1);
							if (spa.length() % 2 == 0) {
								String spa1 = spa.substring(0, spa.length() / 2);
								String spa2 = spa.substring(spa.length() / 2);
								if (spa1.equals(spa2)) {
									span.put("plotRatio", ReptileGtjAreaInfoConvert.changVal(spa1));
								}else {
									span.put("plotRatio", ReptileGtjAreaInfoConvert.changVal(spa));
								}
							} else {
								span.put("plotRatio", ReptileGtjAreaInfoConvert.changVal(spa));
							}
							continue;
						}
						if (spa.contains("位置")) {
							spa = spa.substring(index + 1);
							span.put("address", spa);
							continue;
						}
						if (spa.contains("面积")) {
							spa = spa.substring(index + 1);
							if (spa.contains("（平方米）")) {
								spa = spa.replace("（平方米）", "");
							}
							if (spa.contains("平方米")) {
								spa = spa.replace("平方米", "");
							}
							
							int k = spa.indexOf(".");
							if (k != -1) {
								try {
									spa = spa.substring(0, k + 2);
								} catch (Exception e) {
									spa = spa.substring(0,k);
								}
								span.put("acreage", spa);
							} else {
								if (spa.length() % 2 == 0) {
									String spa1 = spa.substring(0, spa.length() / 2);
									String spa2 = spa.substring(spa.length() / 2);
									if (spa1.equals(spa2)) {
										span.put("acreage", spa1);
									}else {
										span.put("acreage", spa);
									}
								} else {
									span.put("acreage", spa);
								}
							}
							continue;
						}
						if (spa.contains("途")) {
							spa = spa.substring(index + 1);
							span.put("use", spa);
							continue;
						}
						if (spa.contains("竞")) {
							spa = spa.substring(index + 1);
							if (StringUtils.equals(spa, "null")) {
								span = null;
								break;
							}
							span.put("buyer", spa);
							continue;
						}
						if (spa.contains("价")) {
							spa = spa.substring(index + 1);
							if (StringUtils.equals(spa, "null")) {
								span = null;
								break;
							}
							String regEx = "[^0-9.]"; //提取数字和.
							Pattern result = Pattern.compile(regEx);
							String price = result.matcher(spa).replaceAll("").trim();
							if(price==null||StringUtils.isBlank(price)){
								break;
							}
							String money;
							if(spa.contains("万元")){
								money = price;
							}else{
								Double res = Double.parseDouble(price)/10000;
								money = res.toString();
							}
							span.put("price", money);
							continue;
						}
						if (spa.contains("日期")) {
							spa = spa.substring(index + 1);
							if (StringUtils.equals(spa, "null")) {
								span = null;
								break;
							}
							span.put("time", spa);
							continue;
						}

					}
				}
				ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
				if (span != null && span.size() > 0) {
					model.setAreaNo(span.get("areaNo"));
					model.setAddress(span.get("address"));
					model.setAcreage(span.get("acreage"));
					model.setUse(span.get("use"));
					model.setBuyer(span.get("buyer"));
					model.setPlotRatio(span.get("plotRatio"));
					model.setPrice(span.get("price"));
					model.setCity("天津");
					model.setFrom("天津土地交易中心");
					model.setUrl(urls + href);
					infoList.add(model);
				}
			}
			return infoList;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}

	public static List<ReptileGtjAreaInfoModel> getTJAreaInfo(Integer maxPage)
			throws IOException, InterruptedException, ProjectException {
		List<ReptileGtjAreaInfoModel> infoList = new LinkedList<ReptileGtjAreaInfoModel>();
		for (int i = 1; i <= maxPage; i++) {
			infoList = getInfo(i, infoList);
		}
		System.out.println(infoList.size());
		return infoList;
	}

}
