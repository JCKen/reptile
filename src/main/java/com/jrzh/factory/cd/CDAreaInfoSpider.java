package com.jrzh.factory.cd;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

public class CDAreaInfoSpider {

	private static Log log = LogFactory.getLog(CDAreaInfoSpider.class);

	// 获取数据 将页面的数据用list返回

	public static List<ReptileGtjAreaInfoModel> getInfo(Integer page) {
		List<ReptileGtjAreaInfoModel> list = new ArrayList<ReptileGtjAreaInfoModel>();
		try {
			for (int i = 1; i <= page; i++) {
				log.info("###获取成都第【"+ i +"】页数据");
				Connection connect = Jsoup.connect("http://www.cdlr.gov.cn/second/zpggg.aspx?ClassID=001002002006001");
				Document doc; // 根据分页获取的页面
				if (i == 1) {
					doc = connect.get();
				} else {
					// post请求带的参数
					connect.data("__VIEWSTATE",
							"USTILRdJL57Xp9agU7Ib0tBWHAmxExJQtitlp5DjPY94FUmYpCRknaoksRFtKfUsL+7SNBzWdX3jiIS1eGF8hpy3i26yrewaNXHwAHxiu3fmDBew05JI+9Mw5sFtEB+uur7U6a0deoX054vM+IfZu6wkDvyUpPRYe2V0unpnezCLR/cBu3dLAdWZVToWT32ggXo3he2PL5WqBKgj4BKT9+yDtHt2BPIdrT/4uHbPV/CDFMxbKDp2KfrclBDTL78viRI3f+z+vJS6OEOM9XEJL5j2ibwL+e7HI7s8TitlM6wz43Br1k6Ge/dOAxtZHqjBti6PGnuXlpXC2cSA6dw25oChNfDvWIl7Z/NVmg/xTXbyEfG/jsMMePmS0R/cKErugqxJxlVgHbZ7zYbN99IxAZe07CDjPJzLZDHpjqWv4z3YZ5wsfi1z6YMJEKKOFhAhDK5UwiTOPjJNuj8ALNSMTg+Aue3Bt6nw/r+SYT+gwuWaUo9aI35ZZDvAoYtUPOWG4YvYU+YRNK2i43gWbXTk1PqJXPlouD/B2wu63c0RkiMHoYsOgpcoSkoIVi/YeirsRuAxhgVX8MgXv/gLnOfoatIEOyOXKZzX0wolYLXUOnhrxdZ+0o0+Ley0Bq+RONa/0wzB+io3aSi8qo6Th9bszZKKIYyJ0b7TgJRvVIuYUW65c/JgBC9DhvCRy4dWErJBZMYKgnKhyPBQJF6njhUFvT/KNOZ5BS+fiIaOrOjRtpQB/hnv1ImeePBfFl+nN3/iVOXc3nnLgohxdeFBhSVhV3USosY8PYrjlkkiQwMPhN6UU+xdPDhhTca6ix5DKBpckpetr/VgwQUGFbgAnEO/9ma7Q0FfVd7VTL94XZrW5OMBsDScE7Pax7zfCRNQZQmdh0+i5W4lYm0Qmzhhk1Z/YDPrDGB/GhjPV8IzKP7IP4wgnaiP2I+9+y3OSdBMRRIxfMfymMDIM00eHObnkckHLDQZ6Ttn5kh9yaFuVkBpMnd7WgfzWYZWq1Wqwg+XuWjGkRgm+B/LHPUhZCgTcXww2c1/R6DkyFUQRnekECb8TGr8YWcC9OyxW9FxaCkatjHdRjw/EGthmdwte9Cb9FnruJLfZH27RCigcovPSUxB0/Ggbr8jfTVvgNgdCUEWy1mwJ8RZHlg0XK/DMuRwTY6M3W2DOwOlxQo0oVedlIb8uqluRq2v45BdaCpXUs3OX278xhyHbMkD1EMdfOhkBsVSypccyWpUr1dM11kDCq0K6JOH386e4/aLZbzy9AW81h6sR0JpZSjHqEyPvgZJ3AHpJFQSsg385zH6D5Qyg2GUyWOrawVn1M5oomlMB3q5ikPovfYc2HwPNvQL9O4hAwBupmG4hjlpP+nxzxaqb436lf6NCMBHxpZoYSiIUY2QCaE8Y1QCj+hLTUzfM6mhF3Bm/TEWrrGP8a+t0lFkkxGrDHsQB5k2FejnE9wV/OQqoVInBrJE6zNiHnvGVOSe0mTspq75LJYlejNbwnVvXSH5CFgqEJOSMF72qG902xk5BQVFhUt3OQcJrR2V/c35/Mdxn5HfuGGg6ns+WrYVJjpXkKn/N1+dUvJyxTg/A7V5czmpH5skv81w2Xs6+QPjns3RCP+t585KSNV15pM1Cozj2MhBCHyciCwzk+uV+U83NpqukoKdchpaatz5pnWV5B06n73Xs4mwdmUxrJfukb9IcrR4aYu/kytYW7gVQHSYOJiPzBLrHxDN2mrw4ZJX0hDo0JXve7ETsONTcr+KzuLTzt7UEotH93F6vUA2J5oum2I8DhkSA79VDev5JKx+Ry79wZnahdEUBxEqBCrctdMFKKdNcjSzxwvoJZ8YnH0VyAh9FN1acDWYmGVrKrRWLvbulphzMIeORwqLrqk3WOU3acm2JPiUmD5eDcW2b0HrdsCcYdBFXnMXk0VokBY5svQg1LddZLPnJdzIL+Aupy0E/SsIuJri71aBo65VKHnxo8ARzvBnrvh/JR6yroUjx2YMlGGFkEz0X5eqaAtt7ewi5wjJ1x76KsR7AUOo4+q5KgqPWHIAcDUsFPmzeb+T4GZ75zjwlnXDEge82ZszN/YN1H5m6pVggDvLXuFzFNrObt42gNPEFsUqABo3mHiD7pDqAt4QhHD8NAHSkRPRG1rP3oISzsT/GLSemHW37xfPEBGfXvZDhN+DQ8FbdL0QELhS6Zg93ipg8FmX0r2xU3ynnGvE4EErYeL48kIe39RqTFlU/SO0xv6JoTFwlqSGLVdMbcOig/fRUoTYKZNbzQ7Snpm+Z0gXMOigtkDUsQt6z9mAdl3kRfDufy21pVqKcFhwlbAthKX95dyEeuHEgv1aqCyLYvP05KUvI3v5yC+xKm9Hh7oMSm29PkWoJRF7kFmCo1h2TIFSnPxKbRypL3gLFOQCdxGN1MSQ4c6/WVL/6MDSxKmivygurp0f761TgGiDCoFlkaGILvhjfXYmyyALXmzwrcA3hDy/1Y6CExUnxcyTPAGrkPrhv+B08KDfuMcSnFF5fKmV/VO99OUapxwd9UlrxMs+Ucu+GxH32GcIyMi7D4YSLVMRAhtS9hLDBIMBKL0Vg1O5WToAWiDx0Ho/4qFm7MC4uw3NbQ4BW+n0L9FYeioI9flFfnvuLnlcLvylt2laY4iKTY/ntUjOk34YnJCyBwXJxgPIv+GC1w6t9iekinpsjf/xNHn77+lE8AYu1K71GeSmEZXrifHA/f3+t0NK6CulWSkWrdMd0OrzTORcb3qxc+Wt0cxldlxa+1IwzqQOqWRKHTlZzI/nNSgIPMbMTw+Dj+dk+lWeXfoqAMGnty1CBlEg92fCEqG83Yz4OJlxpimw+tt39Gf9IyE6z9ZaTPVCi2kueO6EjCwlKATJbo5ulLkOQhuOXAH7sprRK4Nj5knt2joZVkS/N7B4+z8GkWM1SVRHL0KkE3WQeAVbKHP0aKekUy951i0Anxn7lNv9BYSMBGArGjqkNewV6N0zAR5+DhFE7RtasV7Sw18eyEwzNLOdAbFjSGP+9o8sltL4qRudyQ3UqpTETkrcsm2s/AgPnl/ojPFfQcSdiG+BHYMFJ1xswDsX68u4YNUA4xLRPYYXxp0Fnwxgk9ZZ7PjAsa4p4gTbt8OH9DYs76m3CDhHWjSvy6/v27YU/JBALCu/cdRpOteoBuze8yuWRl/nH52Zmy9rd3sY0ZQqZUTWuK9EmtaBNsStX/B5uzNWY6oYVv5+dDLZw8+Sfh99b/mBG/Rg9wa9yZvd/qUb5eq5k88spZWjhMm7I2ASatZk9beALJlm+2moxaV2J1AxPM1GvDdnmVxMvJOmYQM+GcN3K/yjcrD2eLqAKQGdI0DnK0UNaBBZuVeQL4O5/z1fxl/HBCsQgagzI+nO+wWhjugLYOmORXOsoEBEe9vKDAeUP3hpkPaYDfuJqwV7nI388trcWB/EKbG1W/SmFy1wpuq+FufAUSfcFq9CJY9oV1TByV7ed5w4DGEA1vsz5hvgfFfjUX66T8BpbpeKhE3M1oJTdBjdSE2e26lsxWB79rFpzfJPrj1MxGw8MFiBctL0DOkDwy1xTH9gXn0mtCfxmxbio9DUhpsCwVRpvEXvFsXH1P8V7VsyMRJOL+lYmYXmK++gvx/TWj4IG4HJY0JnburoqHJknkFEj2eq06sQEf/bX8gOAqOXQLKIYwfZl5n1izxCVqnE+GZZoe1rHGRLxNC02/I7Pm2H1JbKuzYRoLart6h0RtijEKUXjV0b9svcKlhdzVApCLie/lAg+/DGNvSE6s2sE/GrEp8eevu5pM3H17ZBcHAC572iIWaoDpEbyuAXsw1yEbsrdJ5S2Y/QQMw1DDtiroCLa338pUHLYS713NGJK82DbRZcCdtG0l1qof2/AGCvWtw9CPCF/QoxLHOiosr0Wc20jswRHbQrefdW+kbwDEInU/tqwsKZLlPgoSGsWURAwADoBmsrwCOGaANS6f609f6OEt6NXMukA6HoxZjBULSf0vOnl9eTK6dQfhbADKO9HDXu0Ce1TrMkIQy0HF+qw7iLEdmtL9UdrdDEArlusDtplCjReCw1DSq/wS68lr+GSWNKVkY7ZZjxe75Qrga5eQrrrnxOAKrBYFUJdeqnxob84rxDfsTLi0rvm/AmPHLvBBfiaX1ZjSJx+UIDE/sLQqZ/aTTVeEn/dos/iPkIaONOB8y5Ug5ecMwybFNxXcAwQ5RkCXWcK8xVndQYGowXTF5c9UG6cocSNFzC1XTK8EOnz4wkFHE2gS87lvGuc6DlO7Tj+hGo1qSXFXJu0zjR95Dw1U43Sfk9p7cPhryiHv22fGn9PiLWlpSHDqnmvLa0E/axWFj4gA4PkIIZtJSLzsUtyDP0KBV3lMZR2HoWVjlhGZ2OWTAvESSA14oadxoETeF2oiHdrbFIOwrHWlTCyvVdRJ75CumLim4HTof801zZBFiLgAG6IxiVIKs91v375jEMoPSzODhRgfGMvk9Xml3ExLlLvpgPFj+4xFDdy49AEjL/1wSfqaN1v2eTbYt3SjPJTV2MO1GFoixGaJ+GkVmpzgpJqAR9V5V2yMjg2RsteeG/ukHTniMnzYMnFoH/IXzZLWhdJUd1IOq1Ho+ktfSFPNfVeeB0gGooSEayqYnWNg3po83Zhp8mlozQLzihI0dYg83oEF5QDFfBsnKM04W6cNpb2l/Y7bO7wqYGReW5OwpRTWQSqxqxzq5JCZvWJWKRY/FefTYTeSIrZghH1yTVL1PXFcLoz56GgEbZfiKg1TCZXMgYn2pg2hzmMq1/0hOgCu3Scmyv98Y2");
					connect.data("__EVENTTARGET", "AspNetPager1");
					connect.data("__EVENTARGUMENT", i + "");
					connect.data("AspNetPager1_input", (i - 1) + "");
					doc = connect.post();
				}
				Elements elems = doc.getElementsByClass("wsdc"); // 获取列表的父级div
				if (elems == null || elems.size() <= 0) {
					continue;
				}
				Elements selectTable = elems.select("table"); // 获取列表的table
				if (selectTable == null || selectTable.size() <= 0) {
					continue;
				}
				Elements selectA = selectTable.select("a"); // 获取列表的a标签
				if (selectA == null || selectA.size() <= 0) {
					continue;
				}
				Iterator<Element> elIterable = selectA.iterator();
				// 循环进入 数据页
				int index = 1;
				while (elIterable.hasNext()) {
					StringBuffer str = new StringBuffer();
					str.append("http://www.cdlr.gov.cn/" + elIterable.next().attr("href"));
					String dataUrl = StringUtils.replace(str.toString(), "../", ""); // 数据页面的链接
					Document dataDoc = Jsoup.connect(dataUrl).get(); // 数据页面
					Elements dataTable = dataDoc.select("table");
					if (dataTable == null || dataTable.size() <= 0) {
						continue;
					}
					Elements selectTr = dataTable.get(0).select("tr");
					// 排除列表标题
					if (selectTr == null || selectTr.size() <= 1) {
						continue;
					}
					Elements selectTh = selectTr.select("th");
					if (selectTh.size() <= 0 || selectTh == null) {
						selectTh = selectTr.get(0).select("td");
					}
					selectTr.remove(0);
					String area; // 面积
					String price; // 成交价
					String buyer; // 买家
					index++;
					if (i == 39 && index == 10|| i == 39 && index == 18) {
						continue;
					}
					// 循环数据页的数据
					for (int k = 0; k < selectTr.size(); k++) {
						Elements selectTd = selectTr.get(k).select("td");
						ReptileGtjAreaInfoModel model = new ReptileGtjAreaInfoModel();
						for (int j = 0; j < selectTh.size() + 1; j++) {
							if (j == selectTh.size()) {
								model.setUrl(dataUrl);
								model.setCity("成都");
								model.setFrom("成都市规划和自然管理局");
								list.add(model);
								break;
							}
							if (selectTh.get(j).text().indexOf("宗地编号") >= 0) {
								model.setAreaNo(selectTd.get(j).text());
							}
							if (selectTh.get(j).text().indexOf("宗地位置") >= 0) {
								model.setAddress(selectTd.get(j).text());
							}
							if (selectTh.get(j).text().indexOf("净用地面积") >= 0) {
								area = selectTd.get(j).text();
								// 面积换算
								if (area.indexOf("平方米") > 0) {
									area = area.substring(0, area.indexOf("平方米"));
								}
								if (area.indexOf(" ") > 0) {
									area = area.substring(0, area.indexOf(" "));
								}
								if (area.indexOf("/亩") >= 0) {
									Double areas = Double.parseDouble(area) * 666.666;
									area = areas.toString();
								}
								model.setAcreage(area);
							}
							if (selectTh.get(j).text().indexOf("成交价") >= 0
									|| selectTh.get(j).text().indexOf("成交单价") >= 0) {
								price = selectTd.get(j).text();
								if (StringUtils.equals(price, "/")) {
									break; // 流拍 或终止
								}
								// 单价换算
								if (price.indexOf("：") >= 0 || price.indexOf(" ") >= 0) {
									price = price.substring(price.indexOf("：") + 1, price.length());
								}
								if (price.indexOf("万") >= 0) {
									price = price.substring(0, price.indexOf("万"));
									Double money = Double.parseDouble(price);
									money = money * 10000 / 666.66;
									price = money.toString();
								}
								if (price.indexOf("元") >= 0) {
									price = price.substring(0, price.indexOf("元"));
								}
								Double money = Double.parseDouble(price);
								money = money * Double.parseDouble(model.getAcreage()) / 10000;
								price = money.toString();
								model.setPrice(price);
							}
							if(selectTh.get(j).text().indexOf("成交总价")>=0){
								price =selectTd.get(j).text();
								if(StringUtils.equals(price, "/")) break;
								model.setPrice(price.substring(0, price.indexOf("万元")));
							}
							if (selectTh.get(j).text().indexOf("竞得人") >= 0) {
								buyer = selectTd.get(j).text();
								if (StringUtils.equals(buyer, "/")) {
									break; // 流拍 或终止
								}
								model.setBuyer(buyer);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			new ProjectException(e.getMessage());
		}
		return list;
	}
}
