package com.jrzh.factory.gz;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.utils.DateUtil;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.factory.webSpider.HouseWorldSpider;
import com.jrzh.factory.webSpider.UserAgent;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileNewhouseDealEverydayModel;
import com.jrzh.mvc.model.reptile.ReptileSaleInfoModel;
import com.jrzh.mvc.search.reptile.ReptileNewhouseDealEverydaySearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

//广州每日新建商品房签约信息
public class GZNewhouseDealEveryday {

	public static final String baseUrl = "http://www.gzcc.gov.cn/data/Category_177/Index.aspx";
	private static Log log = LogFactory.getLog(HouseWorldSpider.class);
	private static ReptileNewhouseDealEverydayModel model;

	public static void spider(ReptileServiceManage reptileServiceManage,SessionUser sessionUser) {
		Date now = new Date();
		Response res = null;
		Document document = null;
		ReptileSaleInfoModel saleInfoModel = new ReptileSaleInfoModel();
		Element infoTable = document.getElementsByClass("resultTableD").last();
		Element timeTable = document.getElementsByTag("table").last();
		try {
			res = Jsoup.connect(baseUrl).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom())).timeout(50000)
					.postDataCharset("utf-8").execute();
		} catch (Exception e) {
			try {
				res = Jsoup.connect(baseUrl).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom()))
						.timeout(50000).postDataCharset("utf-8").execute();
			} catch (Exception e2) {
				return;
			}
		}
		try {
			try {
				document = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), baseUrl);
			} catch (Exception e) {
				try {
					Thread.sleep(1000);
					document = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), baseUrl);
				} catch (Exception e1) {
					if (e1.getMessage().contains("Connection refused: connect")) {
						log.info(baseUrl + " ：连接被拒绝访问");
					} else {
						log.error(e1);
					}
					return;
				}
			}
			if (document == null)
				return;
			Elements trs;
			Elements tds;
			

			// 详细数据
			trs = infoTable.select("tr");
			Element td;
			Double allNumber = 0.00;
			Double allArea = 0.00;
			for (Element tr : trs) {
				tds = tr.select("td");
				if (tds.size() < 9)
					continue;
				model = new ReptileNewhouseDealEverydayModel();
				model.setCity("广州");
				// 区域信息
				td = tds.get(0);
				String part = td.text();
				if (StringUtils.contains(part, "越秀")) {
					model.setPart("越秀");
				}
				if (StringUtils.contains(part, "荔湾")) {
					model.setPart("荔湾");
				}
				if (StringUtils.contains(part, "海珠")) {
					model.setPart("海珠");
				}
				if (StringUtils.contains(part, "天河")) {
					model.setPart("天河");
				}
				if (StringUtils.contains(part, "白云")) {
					model.setPart("白云");
				}
				if (StringUtils.contains(part, "黄埔")) {
					model.setPart("黄埔");
				}
				if (StringUtils.contains(part, "萝岗")) {
					// 现在广州没有了萝岗区 萝岗区并入了黄埔区
					model.setPart("黄埔(萝岗)");
				}
				if (StringUtils.contains(part, "番禺")) {
					model.setPart("番禺");
				}
				if (StringUtils.contains(part, "花都")) {
					model.setPart("花都");
				}
				if (StringUtils.contains(part, "南沙")) {
					model.setPart("南沙");
				}
				if (StringUtils.contains(part, "从化")) {
					model.setPart("从化");
				}
				if (StringUtils.contains(part, "增城")) {
					model.setPart("增城");
				}
				// 住宅套数
				td = td.nextElementSibling();
				model.setHouseCount(td.text());
				// 住宅面积
				td = td.nextElementSibling();
				model.setHouseArea(td.text());
				// 商业套数
				td = td.nextElementSibling();
				model.setBusinessCount(td.text());
				// 商业面积
				td = td.nextElementSibling();
				model.setBusinessArea(td.text());
				// 办公套数
				td = td.nextElementSibling();
				model.setOfficeCount(td.text());
				// 办公面积
				td = td.nextElementSibling();
				model.setOfficeArea(td.text());
				// 车位套数
				td = td.nextElementSibling();
				model.setCarportCount(td.text());
				// 车位面积
				td = td.nextElementSibling();
				model.setCarportArea(td.text());

				// 获取交易日期
				trs = timeTable.select("tr");
				for (Element trEle : trs) {
					tds = trEle.select("td");
					for (Element str : tds) {
						if (StringUtils.contains(str.text(), "日期：")) {
							String time = str.text().split("日期：")[1];
							System.out.println(time);
							model.setDealDate(time);
						}
					}
				}
				
//				System.out.println("城市："+model.getCity());
//				System.out.println("区域："+model.getPart());
//				System.out.println("住宅套数："+model.getHouseCount());
//				System.out.println("住宅面积："+model.getHouseArea());
//				System.out.println("商业套数："+model.getBusinessCount());
//				System.out.println("商业面积："+model.getBusinessArea());
//				System.out.println("办公套数："+model.getOfficeCount());
//				System.out.println("办公面积："+model.getOfficeArea());
//				System.out.println("车位套数："+model.getCarportCount());
//				System.out.println("车位面积："+model.getCarportArea());
//				System.out.println("\n\n\n");
				
				Double number = Double.valueOf(model.getHouseCount()) + Double.valueOf(model.getBusinessCount()) + 
						Double.valueOf(model.getOfficeCount()) + Double.valueOf(model.getCarportCount());
				Double price = Double.valueOf(model.getHouseArea()) + Double.valueOf(model.getBusinessArea()) + 
						Double.valueOf(model.getOfficeArea()) + Double.valueOf(model.getCarportArea());
				allNumber += number;
				allArea += price;
				ReptileNewhouseDealEverydaySearch search = new ReptileNewhouseDealEverydaySearch();
				search.setEqualDealDate(model.getDealDate());
				search.setEqualCity(model.getCity());
				search.setEqualPart(model.getPart());
				ReptileNewhouseDealEverydayModel search_model = null;
				try {
					 search_model = reptileServiceManage.reptileNewhouseDealEverydayService.findBySearch(search);

				} catch (Exception e) {
					e.printStackTrace(); 
				}
				//List<ReptileNewhouseDealEverydayModel> models = reptileServiceManage.reptileNewhouseDealEverydayService.findListBySearch(search);
				if(search_model==null) { 
					//保存
					reptileServiceManage.reptileNewhouseDealEverydayService.add(model, sessionUser);
				}else {
					//修改
					ReflectUtils.copySameFieldToTargetFilterNull(model,search_model);
					reptileServiceManage.reptileNewhouseDealEverydayService.edit(search_model, sessionUser);
				}
			}
			
			//合并黄浦 和 黄浦（萝岗）的数据
			ReptileNewhouseDealEverydaySearch search2 = new ReptileNewhouseDealEverydaySearch();
			search2.setEqualPart("黄埔");
			search2.setEqualDealDate(model.getDealDate());
			search2.setEqualCity("广州");
			ReptileNewhouseDealEverydayModel huangpu = reptileServiceManage.reptileNewhouseDealEverydayService.findBySearch(search2);
			search2.setEqualPart("黄埔(萝岗)");
			ReptileNewhouseDealEverydayModel luogang = reptileServiceManage.reptileNewhouseDealEverydayService.findBySearch(search2);
			ReptileNewhouseDealEverydayModel newModel = new ReptileNewhouseDealEverydayModel();
			newModel.setCity("广州");
			newModel.setPart("黄埔");
			newModel.setDealDate(model.getDealDate());
			//住宅套数
			newModel.setHouseCount(Integer.toString((Integer.parseInt(huangpu.getHouseCount())+Integer.parseInt(luogang.getHouseCount()))));
			System.out.println("新套数："+newModel.getHouseCount());
			//住宅面积
			newModel.setHouseArea(Integer.toString((Integer.parseInt(huangpu.getHouseArea())+Integer.parseInt(luogang.getHouseArea()))));
			System.out.println("新面积："+newModel.getHouseArea());
			//商业套数
			newModel.setBusinessCount(Integer.toString((Integer.parseInt(huangpu.getBusinessCount())+Integer.parseInt(luogang.getBusinessCount()))));
			System.out.println("新套数："+newModel.getBusinessCount());
			//商业面积
			newModel.setBusinessArea(Integer.toString((Integer.parseInt(huangpu.getBusinessArea())+Integer.parseInt(luogang.getBusinessArea()))));
			System.out.println("新面积："+newModel.getBusinessArea());
			//办公套数
			newModel.setOfficeCount(Integer.toString((Integer.parseInt(huangpu.getOfficeCount())+Integer.parseInt(luogang.getOfficeCount()))));
			System.out.println("新套数："+newModel.getOfficeCount());
			//办公面积
			newModel.setOfficeArea(Integer.toString((Integer.parseInt(huangpu.getOfficeArea())+Integer.parseInt(luogang.getOfficeArea()))));
			System.out.println("新面积："+newModel.getOfficeArea());
			//车位套数
			newModel.setCarportCount(Integer.toString((Integer.parseInt(huangpu.getCarportCount())+Integer.parseInt(luogang.getCarportCount()))));
			System.out.println("新套数："+newModel.getCarportCount());
			//车位面积
			newModel.setCarportArea(Integer.toString((Integer.parseInt(huangpu.getCarportArea())+Integer.parseInt(luogang.getCarportArea()))));
			System.out.println("新面积："+newModel.getCarportArea());
			//删除原有的“黄埔（萝岗）”
			reptileServiceManage.reptileNewhouseDealEverydayService.delete(luogang.getId(), sessionUser);
			reptileServiceManage.reptileNewhouseDealEverydayService.delete(huangpu.getId(), sessionUser);
			//将新的黄埔更新
			reptileServiceManage.reptileNewhouseDealEverydayService.add(newModel, sessionUser);
			
			
			saleInfoModel.setCity("广州");
			saleInfoModel.setAllArea(String.valueOf(allArea));
			saleInfoModel.setAllNumber(String.valueOf(allNumber));
			saleInfoModel.setInfoUpdataTime(DateUtil.format(DateUtil.format(now, "yyyy-MM-dd")+" 00:00:00"));
			reptileServiceManage.reptileSaleInfoService.add(saleInfoModel, sessionUser);
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		GZNewhouseDealEveryday deal = new GZNewhouseDealEveryday();
		deal.spider(null,null);
	}
}
