package com.jrzh.factory.gz;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.contants.UrlContants;
import com.jrzh.mvc.convert.reptile.ReptileGtjAreaInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

public class GzAreaInfoSpider{

	private static Log log = LogFactory.getLog(GzAreaInfoSpider.class);
	
	private static ReptileGtjAreaInfoConvert convert = new ReptileGtjAreaInfoConvert();
	
	/**
	 *获取详情 
	 * 
	 **/
	public static ReptileGtjAreaInfoModel getHouseInfo(String url)throws ProjectException {
		try {
			Document document = Jsoup.connect(url).timeout(50000).get();
			Elements timeElement = document.select(".date");
			Date makeTime = new Date();
			for (Element e : timeElement) {
				if ( null !=e.selectFirst("b") ){
					makeTime = DateUtil.format(e.selectFirst("b").text(), "YYYY-MM-DD hh:mm:ss");
				}
			}
			Element element =document.selectFirst("tbody");
			if(null == element) return null;
			Elements trElements = element.select("tr");
			Map<String, String> dataMap = new HashMap<String, String>();
 			for(Element tr : trElements){
				Elements tds = tr.select("td");
				if(tds.size() != 2) continue;
				if(tds.get(1).text().contains("null")) return null;
				dataMap.put(tds.get(0).text(), tds.get(1).text());
			}
 			ReptileGtjAreaInfoModel model = convert.gZmapToModel(dataMap);
 			if(null == model) return null;
 			model.setTime(makeTime);
 			model.setUrl(url);
 			model.setCity("广州");
 			model.setFrom("广州市国土资源和规划委员会");
 			return model;
		} catch (IOException e) {
			e.printStackTrace();
			throw  new ProjectException("爬取失败");
		}
	}
	
	public static List<ReptileGtjAreaInfoModel> getHousePage(List<ReptileGtjAreaInfoModel> list, Integer page) throws ProjectException {
		try {
			log.info("###获取广州土地数据当期第###【"+page+"】页");
			String pageUrl ="_"+page;
			if(page == 1) {
				pageUrl ="https://www.gzlpc.gov.cn/gzlpc/ywpd_tdgl_tdjy_cjgs/list.shtml";
			}else{
				pageUrl = UrlContants.LAND_GZ_LIST_URL+pageUrl+".shtml";
			}
			Document pageDocument = Jsoup.connect(pageUrl).timeout(50000).get();
			Element alllist = pageDocument.selectFirst(".news_list");
			Elements lis = alllist.select("li");
			for(Element li : lis){
				String url = StringUtils.replace(li.selectFirst("a").attr("href") , "../..", "https://www.gzlpc.gov.cn");
				try {
					ReptileGtjAreaInfoModel model = getHouseInfo(url);
					if(null != model) list.add(model);
				} catch (ProjectException e) {
					continue;
				}
			}
			return list;
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}
	
	public static List<ReptileGtjAreaInfoModel> getGZAreaInfo(Integer maxPage) {
		List<ReptileGtjAreaInfoModel> models = new LinkedList<ReptileGtjAreaInfoModel>();
		for(Integer i = 1 ; i<maxPage ;i++){
			try {
				models = getHousePage(models, i);
			} catch (ProjectException e) {
				e.printStackTrace();
				continue;
			}
		}
		return models;
	}
}
