package com.jrzh.factory.gz;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.factory.base.inf.SpiderInf;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileYgjyHouseInfoModel;
import com.jrzh.mvc.search.reptile.ReptileYgjyHouseInfoSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public class GzYgjyHouseInfoSpider  implements SpiderInf<ReptileYgjyHouseInfoModel> {
	
	private static Log log = LogFactory.getLog(GzYgjyHouseInfoSpider.class);
	
	public static String DOMIN="http://www.gzcc.gov.cn";
	public static String URL = DOMIN+"/data/QueryService/Query.aspx?QueryID=58&page=";

	public void saveYgjyHouseInfo(ReptileServiceManage reptileServiceManage, SessionUser user) throws ProjectException {
		new GzYgjyHouseInfoSpider().spider(URL, null, 1, reptileServiceManage,user);
	}

	@Override
	public void spider(String url, String part, Integer index, ReptileServiceManage reptileServiceManage,
			SessionUser user) throws ProjectException {
		try {
			Integer checkIndex=index;
			Document doc = Jsoup.connect(url + index).timeout(500000).get();
			Elements pageElems = doc.getElementsByClass("pager");
			if(null == pageElems) {
				Thread.sleep(1000);
				checkIndex = checkIndex+1;
				spider(url, null, checkIndex, reptileServiceManage,user);
			}
			Elements pageSpan = pageElems.get(0).getElementsByTag("span");
			if(null == pageSpan) {
				Thread.sleep(1000);
				checkIndex = checkIndex+1;
				spider(url, null, checkIndex, reptileServiceManage,user);
			}
			String[] strs = pageSpan.get(0).getElementsByClass("disabled").text().split("/共");
			String allPage = StringUtils.replace(strs[1], "页", "");
			log.info("当前最大Page #######: "+allPage);
			if(StringUtils.isNotBlank(allPage)){
				Integer pageSize = Integer.parseInt(allPage);
				// 数据
				Elements table = doc.getElementsByClass("resultTableC");
				if (table == null)	return ;
				Elements trs = table.get(0).getElementsByTag("tr");
				ReptileYgjyHouseInfoSearch search = new ReptileYgjyHouseInfoSearch();
				for (int i = 0; i < trs.size(); i++) {
					if(i==0)continue;
					Elements tds = trs.get(i).getElementsByTag("td");
					ReptileYgjyHouseInfoModel model = new ReptileYgjyHouseInfoModel();
					model.setUrl(DOMIN+tds.get(1).getElementsByTag("a").get(0).attr("href").trim());	//地址
					model.setProductName(tds.get(1).text());	//项目名称
					model.setDeveloper(tds.get(2).text());//开发商
					model.setCity("广州");
					model.setPreSalePermit(tds.get(3).text());//预售证
					model.setProjectAddress(tds.get(4).text());//项目地址
					model.setHouseSold(StringUtils.isBlank(tds.get(5).text())? 0.00 : Double.valueOf(tds.get(5).text()));//已售
					model.setHouseUnsold(StringUtils.isBlank(tds.get(6).text())? 0.00 : Double.valueOf(tds.get(6).text()));
					//广州房管局楼盘信息入库
					search.setEqualCity("广州");
					search.setEqualPreSalePermit(model.getPreSalePermit());
					ReptileYgjyHouseInfoModel reptileYgjyHouseInfoModel = reptileServiceManage.reptileYgjyHouseInfoService.first(search);
					if(null == reptileYgjyHouseInfoModel) {
						reptileServiceManage.reptileYgjyHouseInfoService.add(model, user);
					}else {
						ReflectUtils.copySameFieldToTargetFilterNull(model,reptileYgjyHouseInfoModel);
						reptileServiceManage.reptileYgjyHouseInfoService.edit(reptileYgjyHouseInfoModel, user);
					}
				}
				Thread.sleep(1000);
				if (checkIndex >=pageSize) {
					log.info("结束了");
					return ;
				} else {
					// 爬取数据
					checkIndex = checkIndex+1;
					spider(url, null, checkIndex, reptileServiceManage, user);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
