package com.jrzh.factory.gz;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.factory.base.inf.YushouzhengInfoMap;
import com.jrzh.factory.webSpider.HouseWorldSpider;
import com.jrzh.factory.webSpider.UserAgent;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileNewhouseYushouzhengModel;
import com.jrzh.mvc.search.reptile.ReptileNewhouseYushouzhengSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;


/**
 * 广州预售证爬虫
 * @author ENZ-PC
 *
 */
public class GZYushouzheng {

	private final static String baseUrl = "http://www.gzcc.gov.cn/data/laho/ProjectSearch.aspx?pjn=&pn=";
	private static Log log = LogFactory.getLog(HouseWorldSpider.class);
	
	
	public static void spider(String houseId,String No,String part,ReptileServiceManage reptileServiceManage,SessionUser sessionUser) {
		String url = baseUrl + No;
		Response res = null;
		Document document = null;
		ReptileNewhouseYushouzhengModel model = new ReptileNewhouseYushouzhengModel();
		model.setNo(No);
		model.setHouseId(houseId);
		model.setCity("广州");
		model.setPart(part);
		try {
			res = Jsoup.connect(url).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom())).timeout(50000)
					.postDataCharset("utf-8").execute();
		} catch (Exception e) {
			try {
				res = Jsoup.connect(url).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom())).timeout(50000)
						.postDataCharset("utf-8").execute();
			} catch (Exception e2) {
				return;
			}
		}
		try {
			try {
				document = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), url);
			} catch (Exception e) {
				try {
					Thread.sleep(1000);
					document = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), url);
				} catch (Exception e1) {
					if (e1.getMessage().contains("Connection refused: connect")) {
						log.info(url +" ：连接被拒绝访问");
					} else {
						log.error(e1);
					}
					return;
				}
			}
			if(document==null) return;
			Element table = document.selectFirst(".wBd").selectFirst(".bd");
			if(table==null) return;
			if(StringUtils.contains(table.text(), "暂无数据")) {
				return;
			}
			Elements tds = table.getElementsByTag("tr").get(1).select("td");
			Element title = tds.get(1).selectFirst("a");
			String infoUrl = "http://www.gzcc.gov.cn/data/laho/preSell.aspx?pjID=100000019530&presell="+No;
			model.setUrl("http://www.gzcc.gov.cn"+title.attr("href").trim());
			model.setInformationSources("广州市住房和城乡建设局");
			model.setProjectName(title.text());
			
			try {
				res = Jsoup.connect(infoUrl).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom())).timeout(50000)
						.postDataCharset("utf-8").execute();
				document = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), infoUrl);
			} catch (Exception e) {
				return;
			}
			if(document==null) return;
			Element content = document.selectFirst(".content");
			if(content==null) return;
			table = content.selectFirst("table");
			Elements trs = table.select("tr");
			Map<String, String> infoMap = new HashMap<String ,String>();
			for (Element tr : trs) {
				if(StringUtils.isBlank(tr.text())) continue;
				if(StringUtils.contains(tr.text(), "说明")) continue;
				int size = tr.select(".tab_th_02").size();
				for(int i=0;i<size;i++) {
					Element keyEle = tr.getElementsByClass("tab_th_02").get(i);
					String key = keyEle.text();
					Element valueEle = keyEle.nextElementSibling();
					String value = valueEle.text();
					infoMap.put(key,value);
				}
			}
			
			
			table = table.nextElementSibling();
			trs = table.select("tr");
			for(int i=2;i<4 ;i++) {
				tds = trs.get(i).select("td");
				if(i==2) {
					for(int j=1;j<6;j++) {
						if(j==1) {
							infoMap.put("住宅套数：", tds.get(j).text());
						}
						if(j==2) {
							infoMap.put("商业套数：", tds.get(j).text());
						}
						if(j==3) {
							infoMap.put("办公套数：", tds.get(j).text());
						}
						if(j==4) {
							infoMap.put("车位套数：", tds.get(j).text());
						}
						if(j==5) {
							infoMap.put("其他套数：", tds.get(j).text());
						}
					}
				}
				if(i==3) {
					for(int j=1;j<6;j++) {
						if(j==1) {
							infoMap.put("住宅面积：", tds.get(j).text());
						}
						if(j==2) {
							infoMap.put("商业面积：", tds.get(j).text());
						}
						if(j==3) {
							infoMap.put("办公面积：", tds.get(j).text());
						}
						if(j==4) {
							infoMap.put("车位面积：", tds.get(j).text());
						}
						if(j==5) {
							infoMap.put("其他面积：", tds.get(j).text());
						}
					}
				}
				
			}
			
			YushouzhengInfoMap.doInfoMap(model, infoMap);
			/*System.out.println("\n\n\n");
			System.out.println("model房屋ID："+model.getHouseId());
			System.out.println("model预售证号："+model.getNo());
			System.out.println("model城市："+model.getCity());
			System.out.println("model区域："+model.getPart());
			System.out.println("model数据源："+model.getInformationSources());
			System.out.println("model数据地址："+model.getUrl());
			System.out.println("model总套数："+model.getAllCount());
			System.out.println("model总面积："+model.getAllArea());
			System.out.println("model发证日期："+model.getOpeningDate());
			System.out.println("model发证机关："+model.getOffice());
			System.out.println("model地上面积："+model.getAboveArea());
			System.out.println("model地下面积："+model.getUnderArea());
			System.out.println("model抵押："+model.getHypothecate());
			System.out.println("model住宅套数："+model.getHouseCount());
			System.out.println("model住宅面积："+model.getHouseArea());
			System.out.println("model商业套数："+model.getBusinessCount());
			System.out.println("model商业面积："+model.getBusinessArea());
			System.out.println("model办公套数："+model.getOfficeCount());
			System.out.println("model办公面积："+model.getOfficeArea());
			System.out.println("model车位套数："+model.getCarportCount());
			System.out.println("model车位面积："+model.getCarportArea());
			System.out.println("model其他套数："+model.getOtherCount());
			System.out.println("model其他面积："+model.getOtherArea());*/
			
			//查询数据库是否有数据
			ReptileNewhouseYushouzhengSearch search = new ReptileNewhouseYushouzhengSearch();
			search.setEqualHouseId(model.getHouseId());
			search.setEqualNo(model.getNo());
			search.setEqualCity("广州");
			search.setEqualProjectName(model.getProjectName());
			ReptileNewhouseYushouzhengModel newModel = reptileServiceManage.reptileNewhouseYushouzhengService.findModelBySearch(search);
			if(newModel==null) {
				//不存在
				reptileServiceManage.reptileNewhouseYushouzhengService.add(model,sessionUser);
			}else {
				//存在
				ReflectUtils.copySameFieldToTargetFilterNull(model,newModel);
				reptileServiceManage.reptileNewhouseYushouzhengService.edit(newModel, sessionUser);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		GZYushouzheng.spider("111", "20180384", "南沙",null,null);
	}
}
