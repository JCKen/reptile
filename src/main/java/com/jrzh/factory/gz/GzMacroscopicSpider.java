package com.jrzh.factory.gz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.mvc.model.reptile.ReptileEstateDatumModel;
import com.jrzh.mvc.model.reptile.ReptileEstateZbModel;

/*
 *  广州  宏观数据获取
 */
public class GzMacroscopicSpider {

	public static List<ReptileEstateDatumModel> dataList = new ArrayList<>();

	// 获取 广州国民经济核算
	public static void getGDP(String url, String year) {
		ReptileEstateDatumModel model = new ReptileEstateDatumModel();
		try {
			Document data = Jsoup.connect(url).get();
			Element table = data.getElementById("maintab");
			Elements total = table.select("tr").eq(1); // 当年 总额 (亿元 )
			String money = total.select("td").get(1).text(); // 总额
			String rate = total.select("td").get(2).text(); // 占比
			Elements agdp = table.select("tr").eq(11); // 当年人均（元）
			String aMoney = agdp.select("td").get(1).text(); // 总额
			String aRate = agdp.select("td").get(2).text(); // 占比

			model.setTime(year);
			model.setCode("GZ0101");
			model.setData(money);
			dataList.add(model);
			model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0102");
			model.setData(rate);
			dataList.add(model);
			model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0103");
			model.setData(aMoney);
			dataList.add(model);
			model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0104");
			model.setData(aRate);
			dataList.add(model);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 获取 广州 年度人口数 总人口和人口变动情况
	public static void getPopulation(String url, String year) {
		Document data;
		try {
			data = Jsoup.connect(url).get();
			Element table = data.getElementById("maintab");
			Elements tr = table.select("tr").eq(3);
			String total = tr.select("td").eq(1).text(); // 人口总数 （万人）
			Double count = Double.parseDouble(total);
			if(count>600000){
				count = count/10000;
			}
			ReptileEstateDatumModel model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0201");
			model.setData(count.toString());
			dataList.add(model);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 获取 广州 财政
	public static void getFinance(String url, String year) {
		Document data;
		try {
			data = Jsoup.connect(url).get();
			Element table = data.getElementById("maintab");
			// 收入
			Elements tr = table.select("tr").eq(3);
			String income = tr.select("td").eq(2).text(); // 值 (亿元)
			String incomeRate = tr.select("td").eq(4).text(); // 增长速率
			Double money = Double.parseDouble(income);
			if(money>10000000){
				money = money/10000;
			}
			// 支出
			Elements tr2;
			if(StringUtils.equals(year, "2016")||StringUtils.equals(year, "2015")||StringUtils.equals(year, "2014")){
				tr2 = table.select("tr").eq(12);
			}else{
				tr2 = table.select("tr").eq(11);
			}
			String pay = tr2.select("td").eq(2).text(); // 值 (亿元)
			String payRate = tr2.select("td").eq(4).text(); // 增长速率
			
			Double payCount = Double.parseDouble(pay);
			if(payCount>10000000){
				payCount = payCount/10000;
			}
			
			ReptileEstateDatumModel model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0301");
			model.setData(money.toString());
			dataList.add(model);
			model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0302");
			model.setData(incomeRate);
			dataList.add(model);
			model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0303");
			model.setData(payCount.toString());
			dataList.add(model);
			model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0304");
			model.setData(payRate);
			dataList.add(model);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 城市 居民消费指数 上年=100
	public static void getXf(String url, String year) {
		Document data;
		try {
			data = Jsoup.connect(url).get();
			Element table = data.getElementById("maintab");
			// 收入
			Elements tr = table.select("tr").eq(2);
			String income = tr.select("td").eq(1).text(); // 值
			ReptileEstateDatumModel model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0105");
			model.setData(income);
			dataList.add(model);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 消费品零售总额
	public static void getLs(String url, String year) {
		Document data;
		try {
			data = Jsoup.connect(url).get();
			Element table = data.getElementById("maintab");
			// 收入
			Elements tr = table.select("tr").eq(2);
			String income = tr.select("td").eq(1).text(); // 值
			Double money = Double.parseDouble(income);
			if(money>10000000){
				money = money/10000;
			}
			String rate = tr.select("td").eq(2).text(); // 比值 （%）
			ReptileEstateDatumModel model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0401");
			model.setData(money.toString());
			dataList.add(model);
			model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0402");
			model.setData(rate);
			dataList.add(model);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 房地产 区、县级市房地产开发投资额
	public static void getFdc(String url, String year) {
		Document data;
		try {
			data = Jsoup.connect(url).get();
			Element table = data.getElementById("maintab");
			// 收入
			Elements tr = table.select("tr").eq(2);
			String income = tr.select("td").eq(1).text(); // 值
			String rate = tr.select("td").eq(2).text(); // 比值 （%）
			Double money = Double.parseDouble(income);
			if(money>10000000){
				money = money/10000;
			}
			ReptileEstateDatumModel model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0501");
			model.setData(money.toString());
			dataList.add(model);
			model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0502");
			model.setData(rate);
			dataList.add(model);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 住户贷款 金融机构人民币信贷收支（二）
	public static void getDk(String url, String year) {
		Document data;
		try {
			data = Jsoup.connect(url).get();
			Element table = data.getElementById("maintab");
			// 收入
			Elements tr = table.select("tr").eq(6);
			String income = tr.select("td").eq(1).text(); // 值
			String rate = tr.select("td").eq(2).text(); // 比值 （%）
			ReptileEstateDatumModel model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0601");
			model.setData(income);
			dataList.add(model);
			model = new ReptileEstateDatumModel();
			model.setTime(year);
			model.setCode("GZ0602");
			model.setData(rate);
			dataList.add(model);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		getUrl();
		for (ReptileEstateDatumModel model : dataList) {
			System.out.println("时间：" + model.getTime() + ",code：" + model.getCode() + ",值：" + model.getData());
		}
	}

	//
	public static void getUrl() {
		Connection connect = Jsoup.connect(
				"http://210.72.4.52/gzStat1/yearqueryAction.do?method=search&title=%C4%EA%B6%C8%B1%A8%B1%ED%20&lmId=02");
		try {
			Document doc = connect.get();
			Elements selectA = doc.select("a");
			int index = 1;
			for (Element a : selectA) {
				if (index == 5) {
					return; // 只获取近5 年
				}
				index++;
				String str = a.attr("onclick");
				if (StringUtils.equals(str, "") || str == null) {
					continue;
				}
				List<String> parameters = new ArrayList<>();
				for (int i = 0; i < str.split("'").length; i++) {
					if (i % 2 != 0 && i != 0) {
						parameters.add(a.attr("onclick").split("'")[i]);
					}
				}
				String url = "http://210.72.4.52/gzStat1/yearqueryAction.do?method=det_Title&flId=" + parameters.get(0)
						+ "&flname=" + parameters.get(1) + "&title=" + parameters.get(2) + "&fbDate="
						+ parameters.get(3) + "&falg=" + parameters.get(4) + "&year=" + parameters.get(5);
				String year = parameters.get(5);
				Document data = Jsoup.connect(url).get(); // 当年的所有指标；
				Elements selectA2 = data.select("a");
				for (Element selA : selectA2) {
					String as = selA.attr("onclick");
					if (StringUtils.equals(as, "") || as == null) {
						continue;
					}
					List<String> parameters2 = new ArrayList<>();
					for (int i = 0; i < as.split("'").length; i++) {
						if (i % 2 != 0 && i != 0) {
							parameters2.add(selA.attr("onclick").split("'")[i]);
						}
					}
					String dataUrl = "http://210.72.4.52/gzStat1/yearqueryAction.do?method=displayRpt&ACTFLAG=3&FID="
							+ parameters2.get(0) + "&RPTID=" + parameters2.get(1) + "&RPTNAME=" + parameters2.get(2)
							+ "&FLTITLE=" + parameters2.get(3); // 每个指标的链接
					String title = parameters2.get(2);
					if (StringUtils.equals("地区生产总值", title)) {
						getGDP(dataUrl, year);
					}
					if (title.contains("总人口和人口变动情况")) {
						getPopulation(dataUrl, year);
					}
					if (title.contains("地方财政一般预算收支")) {
						getFinance(dataUrl, year);
					}
					if (title.contains("城市居民消费价格指数")) {
						getXf(dataUrl, year);
					}
					if (StringUtils.equals(title, "社会消费品零售总额")) {
						getLs(dataUrl, year);
					}
					if (title.contains("区、县级市房地产开发投资额")) {
						getFdc(dataUrl, year);
					}
					if (title.contains("金融机构人民币信贷收支（二）")) {
						getDk(dataUrl, year);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String[] split(String str) {
		return str.split(" ");
	}

	public static List<ReptileEstateZbModel> getAllZb() {
		List<ReptileEstateZbModel> list = new ArrayList<>();
		ReptileEstateZbModel model = new ReptileEstateZbModel();

		model.setCity("广州");
		model.setMenu("国民经济");
		model.setClassify("广州生产总值");
		model.setZb("生产总值");
		model.setUnit("亿元");
		model.setCode("GZ0101");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("国民经济");
		model.setClassify("广州生产总值");
		model.setZb("生产总值(比上年增长)");
		model.setUnit("%");
		model.setCode("GZ0102");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("国民经济");
		model.setClassify("广州人均生产总值");
		model.setZb("生产总值");
		model.setUnit("元");
		model.setCode("GZ0103");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("国民经济");
		model.setClassify("广州人均生产总值");
		model.setZb("生产总值(比上年增长)");
		model.setUnit("%");
		model.setCode("GZ0104");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("国民经济");
		model.setClassify("消费指数");
		model.setZb("城市居民消费指数(上年=100)");
		model.setUnit("");
		model.setCode("GZ0105");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("总人口");
		model.setClassify("总人口");
		model.setZb("年末总人口");
		model.setUnit("万人");
		model.setCode("GZ0201");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("广州财政");
		model.setClassify("财政收支");
		model.setZb("财政收入");
		model.setUnit("亿元");
		model.setCode("GZ0301");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("广州财政");
		model.setClassify("财政收支");
		model.setZb("财政收入(比上年增长)");
		model.setUnit("%");
		model.setCode("GZ0302");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("广州财政");
		model.setClassify("财政收支");
		model.setZb("财政支出");
		model.setUnit("亿元");
		model.setCode("GZ0303");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("广州财政");
		model.setClassify("财政收支");
		model.setZb("财政支出(比上年增长)");
		model.setUnit("%");
		model.setCode("GZ0304");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("广州贸易");
		model.setClassify("消费品零售总额及增长");
		model.setZb("消费品零售总额");
		model.setUnit("亿元");
		model.setCode("GZ0401");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("广州贸易");
		model.setClassify("消费品零售总额及增长");
		model.setZb("消费品零售额(比上年增长)");
		model.setUnit("");
		model.setCode("GZ0402");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("广州房地产投资");
		model.setClassify("房地产投资额及增长率");
		model.setZb("房地产投资额");
		model.setUnit("亿元");
		model.setCode("GZ0501");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("广州房地产投资");
		model.setClassify("房地产投资额及增长率");
		model.setZb("房地产投资增长(比上年增长)");
		model.setUnit("%");
		model.setCode("GZ0502");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("广州金融");
		model.setClassify("住户贷款及增长");
		model.setZb("住户贷款");
		model.setUnit("亿元");
		model.setCode("GZ0601");
		list.add(model);

		model = new ReptileEstateZbModel();
		model.setCity("广州");
		model.setMenu("广州金融");
		model.setClassify("住户贷款及增长");
		model.setZb("住户贷款(比上年增长)");
		model.setUnit("%");
		model.setCode("GZ0602");
		list.add(model);
		return list;
	}
}
