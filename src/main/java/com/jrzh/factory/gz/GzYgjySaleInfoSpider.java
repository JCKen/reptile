package com.jrzh.factory.gz;

import java.io.IOException;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.factory.base.inf.SpiderInf;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileNewhouseDealEverydayModel;
import com.jrzh.mvc.model.reptile.ReptileSaleInfoModel;
import com.jrzh.mvc.model.reptile.ReptileYgjySaleInfoModel;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public class GzYgjySaleInfoSpider  implements SpiderInf<ReptileYgjySaleInfoModel> {

	// 地址
	private final static String YGJY_SALE_URL = "http://www.gzcc.gov.cn/data/Category_177/Index.aspx";

	public static void saveYgjySaleInfo(ReptileServiceManage reptileServiceManage, SessionUser user)
			throws ProjectException {
		new GzYgjySaleInfoSpider().spider(YGJY_SALE_URL, null, null, reptileServiceManage,user);
	}

	@Override
	public void spider(String url, String part, Integer index, ReptileServiceManage reptileServiceManage,
			SessionUser user) throws ProjectException {
		try {
			Date now = new Date();
			Document doc = Jsoup.connect(url).timeout(500000).get();
			Elements elements = doc.getElementsByClass("resultTableD");
			Double allNumber = 0.00;
			Double allArea = 0.00;
			Element table2 = elements.get(2);
			Elements trs2 = table2.getElementsByTag("tr");
			ReptileSaleInfoModel model = new ReptileSaleInfoModel();
			for (int i = 0; i < trs2.size(); i++) {
				if (i < 3)
					continue;
				Elements tds = trs2.get(i).getElementsByTag("td");
				//住宅套数
				Double houseSetNumber = tds.get(1) == null ? 0.00 : Double.valueOf(tds.get(1).text());
				//住宅面积
				Double houseAcreage = tds.get(2) == null ? 0.00 : Double.valueOf(tds.get(2).text());
				//商业套数
				Double bizSetNumber = tds.get(3) == null ? 0.00 : Double.valueOf(tds.get(3).text());
				//商业面积
				Double bizAcreage = tds.get(4) == null ? 0.00 : Double.valueOf(tds.get(4).text());
				//办公套数
				Double officeSetNumber = tds.get(5) == null ? 0.00 : Double.valueOf(tds.get(5).text());
				//办公面积
				Double officeAcreage = tds.get(6) == null ? 0.00 : Double.valueOf(tds.get(6).text());
				//车位套数
				Double positionSetNumber = tds.get(7) == null ? 0.00 : Double.valueOf(tds.get(7).text());
				//车位面积
				Double positionAcreage = tds.get(8) == null ? 0.00 : Double.valueOf(tds.get(8).text());
				Double number = houseSetNumber + bizSetNumber + officeSetNumber + positionSetNumber;
				Double price = houseAcreage + bizAcreage + officeAcreage + positionAcreage;
				allNumber += number;
				allArea += price;
			}
			model.setCity("广州");
			model.setAllArea(String.valueOf(allArea));
			model.setAllNumber(String.valueOf(allNumber));
			model.setInfoUpdataTime(DateUtil.format(DateUtil.format(now, "yyyy-MM-dd")+" 00:00:00"));
			reptileServiceManage.reptileSaleInfoService.add(model, user);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		GzYgjySaleInfoSpider.saveYgjySaleInfo(null,null);
	}


}
