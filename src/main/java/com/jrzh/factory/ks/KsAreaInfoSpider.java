package com.jrzh.factory.ks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.mvc.convert.reptile.ReptileGtjAreaInfoConvert;
import com.jrzh.mvc.model.reptile.ReptileGtjAreaInfoModel;

public class KsAreaInfoSpider {

	private static Log log = LogFactory.getLog(KsAreaInfoSpider.class);
	private static ReptileGtjAreaInfoConvert convert = new ReptileGtjAreaInfoConvert();

	public static List<ReptileGtjAreaInfoModel> getInfo(Integer maxPage) {
		List<ReptileGtjAreaInfoModel> list = new ArrayList<>();
		for (int i = 1; i <= maxPage; i++) {
			Connection connect = Jsoup.connect(
					"http://search.mnr.gov.cn/was5/web/search?page=2&channelid=85275&searchword=xzq_dm%3D%E6%98%86%E5%B1%B1&keyword=xzq_dm%3D%E6%98%86%E5%B1%B1&perpage=10&outlinepage=10");
			connect.data("page", i+""); // 最大 25
			connect.data("searchword", "xzq_dm=昆山");
			connect.data("channelid", "85275");
			connect.data("keyword", "xzq_dm=昆山");
			connect.data("perpage", "10");
			connect.data("outlinepage", "10");
			log.info("获取昆山第【"+i+"】页数据");
			try {
				Document doc = connect.get();
				Element table = doc.getElementsByClass("gu-table-lan").first();
				Elements selA = table.select("a");
				for (Element a : selA) {
					String dataUrl = a.attr("href");
					Map<String, String> map = getData(dataUrl);
					ReptileGtjAreaInfoModel model = convert.getKsMap(map);
					if(model==null) continue;
					model.setUrl(dataUrl);
					list.add(model);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return list;
	}

	public static Map<String, String> getData(String url) {
		Map<String, String> map = new HashMap<>();
		try {
			Document dataDoc = Jsoup.connect(url).get();
			Element table = dataDoc.selectFirst("table");
			Elements trs = table.select("tr");
			for (Element tr : trs) {
				if (tr.select("td") == null)
					continue;
				Elements tds = tr.select("td");
				if (tds.size() % 2 != 0)
					continue;
				for (int i = 0; i < tds.size(); i += 2) {
					map.put(tds.get(i).text().replace(" ", ""), tds.get(i + 1).text().replace(" ", ""));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}

	public static void main(String[] args) {
		List<ReptileGtjAreaInfoModel> list = getInfo(2);
		for (ReptileGtjAreaInfoModel model : list) {
			System.out.println("面积："+model.getAcreage()+",位置："+model.getAddress()+",买家："+model.getBuyer()+",价格："+model.getPrice()+",容积率："+model.getPlotRatio());
			System.out.println("链接："+model.getUrl());
		}
	}
}
