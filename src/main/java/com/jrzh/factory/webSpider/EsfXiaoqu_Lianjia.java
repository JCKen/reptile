package com.jrzh.factory.webSpider;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.contants.EsfCityAreaConstants;
import com.jrzh.factory.base.inf.SpiderInf;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.framework.contants.GeneralConstant;
import com.jrzh.mvc.convert.reptile.ReptileEsfHousesXiaoquConvert;
import com.jrzh.mvc.model.reptile.ReptileEsfHousePriceModel;
import com.jrzh.mvc.model.reptile.ReptileEsfHousesXiaoquModel;
import com.jrzh.mvc.search.reptile.ReptileEsfHousesXiaoquSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

//成交 二手房  链家楼盘
public class EsfXiaoqu_Lianjia implements SpiderInf<ReptileEsfHousesXiaoquModel> {

	private final static String CITY_URL = "https://{0}.lianjia.com/xiaoqu/";
	private static Log log = LogFactory.getLog(EsfXiaoqu_Lianjia.class);

	public void saveEsfHouseInfoFromInGZ(ReptileServiceManage reptileServiceManage, SessionUser user)
			throws ProjectException {
		EsfXiaoqu_Lianjia esfHouseWorldSpider = new EsfXiaoqu_Lianjia();

		/*
		 * try { for (String city : EsfCityAreaConstants.ESF_HOUSE_AREA.cityMap) {
		 * String url = MessageFormat.format(CITY_URL, city); ArrayList<String> list =
		 * new ArrayList<>(); list =
		 * EsfCityAreaConstants.ESF_HOUSE_AREA.valueMap.get(city); for (String part :
		 * list) { esfHouseWorldSpider.spider(url, part, 1, reptileServiceManage, user);
		 * } } } catch (ProjectException e) { e.printStackTrace(); throw e; }
		 */

		// 单个城市数据测试

		try {
			String url = MessageFormat.format(CITY_URL, EsfCityAreaConstants.ESF_HOUSE_AREA.cityMap[8]);
			ArrayList<String> list = new ArrayList<>();
			list = EsfCityAreaConstants.ESF_HOUSE_AREA.valueMap.get(EsfCityAreaConstants.ESF_HOUSE_AREA.cityMap[8]);
			for (String part : list) {
				esfHouseWorldSpider.spider(url, part, 1, reptileServiceManage, user);
			}
		} catch (ProjectException e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Override
	public void spider(String url, String part, Integer index, ReptileServiceManage reptileServiceManage,
			SessionUser sessionUser) throws ProjectException {
		if (StringUtils.contains(url, "https://hz")) {
			url = "https://hui.lianjia.com/xiaoqu/";
		}
		try {
			log.info(url + part + "/pg" + index);
			Document document;
			// 连接超时
			Response res = null;
			String connect = url + part + "/pg" + index;
			try {
				res = Jsoup.connect(connect).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom()))
						.timeout(50000).postDataCharset("utf-8").execute();
			} catch (Exception e) {
				try {
					res = Jsoup.connect(connect).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom()))
							.timeout(50000).postDataCharset("utf-8").execute();
				} catch (Exception e2) {
					return;
				}
			}
			try {
				document = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), connect);
			} catch (Exception e) {
				try {
					document = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), connect);
				} catch (Exception e2) {
					return;
				}
			}
			if (null == document)
				return;
			try {
				Element xiaoqu = document.selectFirst(".typeList").selectFirst("a[href='/xiaoqu/']");
				if (xiaoqu == null)
					return;
			} catch (Exception e) {
				return;
			}
			Element nextElement = document.selectFirst(".house-lst-page-box");
			if (null == nextElement)
				return;
			String pageArr = nextElement.attr("page-data");
			String pageSize = pageArr.split(",")[0];
			String pageCount = pageSize.split(":")[1];
			// 判断当前页码是否最后一页
			if (StringUtils.equals(pageCount, String.valueOf(index - 1))) {
				return;
			}
			Element elementDoc = document.selectFirst(".listContent");
			if (null == elementDoc)
				return;
			Elements lis = elementDoc.select("li");
			ReptileEsfHousesXiaoquModel reptileEsfHousesXiaoquModel = null;
			for (Element li : lis) {
				reptileEsfHousesXiaoquModel = new ReptileEsfHousesXiaoquModel();
				Map<String, String> infoMap = new HashMap<String, String>();

				Element baseInfo = li.selectFirst(".info");
				Element houseName = baseInfo.selectFirst(".title").selectFirst("a");
				reptileEsfHousesXiaoquModel.setHouseName(houseName.text());
				Element positionInfo = baseInfo.selectFirst(".positionInfo");
				reptileEsfHousesXiaoquModel.setHousesPart(positionInfo.selectFirst(".district").text());
				String poSitionInfoText = positionInfo.text();
				Integer flag = poSitionInfoText.lastIndexOf("/");
				if (flag == -1)
					continue;
				String year = poSitionInfoText.substring(flag + 1);
				if (StringUtils.contains(year, " ")) {
					year = year.replace(" ", "");
				}
				Integer year_build;
				try {
					year = year.substring(0, year.indexOf("年建成"));
					year_build = Integer.parseInt(year);
				} catch (Exception e) {
					continue;
				}
				Integer now_year = Integer.parseInt(DateUtil.format(new Date(), "yyyy"));
				// System.out.println("建成年份： " + year);
				if (now_year - year_build > 10 || now_year - year_build < 0) {
					// System.out.println("楼龄>10年，跳过\n\n\n");
					continue;
				}
				reptileEsfHousesXiaoquModel.setHousesBuildingYear(year);
				Element price = li.selectFirst(".totalPrice");
				if (price == null)
					continue;
				String houseUrl = houseName.attr("href");
				reptileEsfHousesXiaoquModel.setUrl(houseUrl);
				Document houseDocument;
				// 连接超时

				try {
					res = Jsoup.connect(houseUrl).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom()))
							.timeout(50000).postDataCharset("utf-8").execute();
					houseDocument = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), houseUrl);
				} catch (Exception e) {
					try {
						houseDocument = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), houseUrl);
					} catch (Exception e2) {
						continue;
					}
				}
				if (null == houseDocument)
					continue;

				Element housePrice;
				Element houseInfoEle;
				Element houseAddress;
				Element rpMonthEle;
				try {
					housePrice = houseDocument.selectFirst(".xiaoquUnitPrice");
					houseInfoEle = houseDocument.selectFirst(".xiaoquInfo");
					houseAddress = houseDocument.selectFirst(".detailDesc");
					rpMonthEle = houseDocument.selectFirst(".xiaoquUnitPriceDesc");
				} catch (Exception e) {
					continue;
				}
				if (null != houseInfoEle) {
					Elements xiaoquInfoItems = houseInfoEle.select(".xiaoquInfoItem");
					for (Element item : xiaoquInfoItems) {
						String key = item.selectFirst(".xiaoquInfoLabel").text();
						String value = item.selectFirst(".xiaoquInfoContent").text();
						if (StringUtils.contains(key, " ")) {
							key = key.replace(" ", "");
						}
						if (StringUtils.contains(value, " ")) {
							value = value.replace(" ", "");
						}
						infoMap.put(key, value);
					}
				}
				ReptileEsfHousesXiaoquConvert.convertBaseInfo(reptileEsfHousesXiaoquModel, infoMap);
				if (null == housePrice)
					continue;
				if (houseAddress == null)
					continue;

				// 参考价月份处理
				if (rpMonthEle != null) {
					Integer mon = Integer.parseInt(rpMonthEle.text().split("月")[0]) * 1;
					reptileEsfHousesXiaoquModel.setRpMonth(mon.toString());
				}
				reptileEsfHousesXiaoquModel.setHousesAddress(houseAddress.text());
				reptileEsfHousesXiaoquModel.setHousesPrice(housePrice.text());
				reptileEsfHousesXiaoquModel.setInformationSources("链家");
				reptileEsfHousesXiaoquModel.setHousesType("住宅");
				reptileEsfHousesXiaoquModel.setInfoUpdateTime(DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
				String cityName = null;
				if (StringUtils.contains(url, "https://gz")) {
					cityName = "广州";
				} else if (StringUtils.contains(url, "https://sz")) {
					cityName = "深圳";
				} else if (StringUtils.contains(url, "https://bj")) {
					cityName = "北京";
				} else if (StringUtils.contains(url, "https://sh")) {
					cityName = "上海";
				} else if (StringUtils.contains(url, "https://cd")) {
					cityName = "成都";
				} else if (StringUtils.contains(url, "https://tj")) {
					cityName = "天津";
				} else if (StringUtils.contains(url, "https://dg")) {
					cityName = "东莞";
				} else if (StringUtils.contains(url, "https://fs")) {
					cityName = "佛山";
				} else if (StringUtils.contains(url, "https://zh")) {
					cityName = "珠海";
				}
				reptileEsfHousesXiaoquModel.setHousesCity(cityName);
				// 入库保存，先查询此二手房有没有存库
				ReptileEsfHousesXiaoquSearch search = new ReptileEsfHousesXiaoquSearch();
				search.setEqualInformationSources("链家");
				search.setEqualHousesPart(reptileEsfHousesXiaoquModel.getHousesPart());
				search.setEqualHousesCity(reptileEsfHousesXiaoquModel.getHousesCity());
				search.setequalHouseName(reptileEsfHousesXiaoquModel.getHouseName());
				search.setEqualHousesType(reptileEsfHousesXiaoquModel.getHousesType());
				ReptileEsfHousesXiaoquModel newEsfModel = reptileServiceManage.reptileEsfHousesXiaoquService
						.first(search);
				ReptileEsfHousePriceModel priceModel = new ReptileEsfHousePriceModel();
				priceModel.setHousePriceUnit("1");
				String ssPrice = reptileEsfHousesXiaoquModel.getHousesPrice();
				if (StringUtils.contains(ssPrice, "元")) {
					ssPrice.substring(0, ssPrice.indexOf("元"));
				}
				priceModel.setHousePrice(ssPrice);
				priceModel.setInfoSource("链家");
				if (null == newEsfModel) {
					System.out.println("保存链家二手房楼盘：" + reptileEsfHousesXiaoquModel.getHousesCity() + " "
							+ reptileEsfHousesXiaoquModel.getHousesPart() + " "
							+ reptileEsfHousesXiaoquModel.getHouseName());
					reptileServiceManage.reptileEsfHousesXiaoquService.add(reptileEsfHousesXiaoquModel, sessionUser);
					priceModel.setHouseId(reptileEsfHousesXiaoquModel.getId());
					reptileServiceManage.reptileEsfHousePriceService.add(priceModel, sessionUser);
				} else {
					System.out.println("修改链家二手房楼盘：" + reptileEsfHousesXiaoquModel.getHousesCity() + " "
							+ reptileEsfHousesXiaoquModel.getHousesPart() + " "
							+ reptileEsfHousesXiaoquModel.getHouseName());
					ReflectUtils.copySameFieldToTargetFilterNull(reptileEsfHousesXiaoquModel, newEsfModel);
					reptileServiceManage.reptileEsfHousesXiaoquService.edit(newEsfModel, sessionUser);
					priceModel.setHouseId(newEsfModel.getId());
					reptileServiceManage.reptileEsfHousePriceService.add(priceModel, sessionUser);
				}
				Thread.sleep(1000);
			}
			spider(url, part, ++index, reptileServiceManage, sessionUser);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}

	
	
}
