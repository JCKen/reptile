package com.jrzh.factory.webSpider;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.contants.EsfCityAreaConstants;
import com.jrzh.factory.base.inf.SpiderInf;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileEsfHouseConvert;
import com.jrzh.mvc.model.reptile.ReptileEsfHouseModel;
import com.jrzh.mvc.model.reptile.ReptileEsfHousePriceModel;
import com.jrzh.mvc.search.reptile.ReptileEsfHouseSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

//成交 二手房  链家
public class EsfLianJiaHouseSpider implements SpiderInf<ReptileEsfHouseModel> {

	private final static String CITY_URL = "https://{0}.lianjia.com/chengjiao/";

	private static Log log = LogFactory.getLog(EsfLianJiaHouseSpider.class);

	public void saveEsfHouseInfoFromInGZ(ReptileServiceManage reptileServiceManage, SessionUser user)
			throws ProjectException {
		EsfLianJiaHouseSpider esfHouseWorldSpider = new EsfLianJiaHouseSpider();

		try {
			for (String city : EsfCityAreaConstants.ESF_HOUSE_AREA.cityMap) {
				String url = MessageFormat.format(CITY_URL, city);
				ArrayList<String> list = new ArrayList<>();
				list = EsfCityAreaConstants.ESF_HOUSE_AREA.valueMap.get(city);
				for (String part : list) {
					esfHouseWorldSpider.spider(url, part, 1, reptileServiceManage, user);
				}
			}
		} catch (ProjectException e) {
			e.printStackTrace();
			throw e;
		}

		// 单个城市数据测试

		/*
		 * try { String url = MessageFormat.format(CITY_URL,
		 * EsfCityAreaConstants.ESF_HOUSE_AREA.cityMap[9]); ArrayList<String> list = new
		 * ArrayList<>(); list =
		 * EsfCityAreaConstants.ESF_HOUSE_AREA.valueMap.get(EsfCityAreaConstants.
		 * ESF_HOUSE_AREA.cityMap[9]); for (String part : list) {
		 * esfHouseWorldSpider.spider(url, part, 1, reptileServiceManage, user); } }
		 * catch (ProjectException e) { e.printStackTrace(); throw e; }
		 */

	}

	@Override
	public void spider(String url, String part, Integer index, ReptileServiceManage reptileServiceManage,
			SessionUser sessionUser) throws ProjectException {
		Date infoUpdateTime = new Date();
		try {
			log.info(url + part + "/pg" + index);
			Document document;
			// 连接超时
			try {
				document = Jsoup.connect(url + part + "/pg" + index).timeout(50000).get();
			} catch (Exception e) {
				try {
					document = Jsoup.connect(url + part + "/pg" + index).timeout(50000).get();
				} catch (Exception e2) {
					return;
				}
			}
			if (null == document)
				return;
			// 查询是否是成交房子
			Element aEle = document.selectFirst("a[href='/chengjiao/']");
			if (aEle == null) {
				log.info(part + "该城市没有成交房数据");
				return;
			}
			Element nextElement = document.selectFirst(".house-lst-page-box");
			if (null == nextElement)
				return;
			String pageArr = nextElement.attr("page-data");
			String pageSize = pageArr.split(",")[0];
			String pageCount = pageSize.split(":")[1];
			// 判断当前页码是否最后一页
			if (StringUtils.equals(pageCount, String.valueOf(index - 1))) {
				return;
			}
			Element elementDoc = document.selectFirst(".listContent");
			if (null == elementDoc)
				return;
			Elements elements = elementDoc.select("li");
			Iterator<Element> elIterator = elements.iterator();
			while (elIterator.hasNext()) {
				String cityName = null;
				if (StringUtils.contains(url, "https://gz")) {
					cityName = "广州";
				} else if (StringUtils.contains(url, "https://sz")) {
					cityName = "深圳";
				} else if (StringUtils.contains(url, "https://bj")) {
					cityName = "北京";
				} else if (StringUtils.contains(url, "https://sh")) {
					cityName = "上海";
				} else if (StringUtils.contains(url, "https://cd")) {
					cityName = "成都";
				} else if (StringUtils.contains(url, "https://tj")) {
					cityName = "天津";
				} else if (StringUtils.contains(url, "https://dg")) {
					cityName = "东莞";
				} else if (StringUtils.contains(url, "https://fs")) {
					cityName = "佛山";
				} else if (StringUtils.contains(url, "https://zh")) {
					cityName = "珠海";
				}
				String houseUrl = elIterator.next().selectFirst("a").attr("href");
				Document houseDocument;
				// 连接超时
				try {
					houseDocument = Jsoup.connect(houseUrl).timeout(50000).get();
				} catch (Exception e) {
					try {
						houseDocument = Jsoup.connect(houseUrl).timeout(50000).get();
					} catch (Exception e2) {
						continue;
					}
				}
				if (null == houseDocument)
					continue;
				Thread.sleep(50);
				Element houseNameEle;
				Element priceTotalEle;
				Element houseInfoEle;
				try {
					houseNameEle = houseDocument.selectFirst(".index_h1");
					priceTotalEle = houseDocument.selectFirst(".price");
					houseInfoEle = houseDocument.selectFirst("#introduction");
				} catch (Exception e) {
					continue;
				}
				Map<String, String> map = new HashMap<>();
				if (null != houseInfoEle) {
					Element houseInfoBaseE = houseInfoEle.selectFirst(".base");
					Element houseInfoTranE = houseInfoEle.selectFirst(".transaction");
					if (null != houseInfoBaseE) {
						Element houseInfoE = houseInfoBaseE.selectFirst(".content");
						if (null != houseInfoE) {
							Elements houseInfoLiE = houseInfoE.select("li");
							if (null != houseInfoLiE) {
								for (Element lis : houseInfoLiE) {
									String key = StringUtils.substring(lis.text(), 0, 4);
									String value = StringUtils.replace(lis.text(), key, "").trim();
									map.put(key, value);
								}
							}
						}
					}
					if (null != houseInfoTranE) {
						Element houseInfoE = houseInfoTranE.selectFirst(".content");
						if (null != houseInfoE) {
							Elements houseInfoLiE = houseInfoE.select("li");
							if (null != houseInfoLiE) {
								for (Element lis : houseInfoLiE) {
									String key = StringUtils.substring(lis.text(), 0, 4);
									String value = StringUtils.replace(lis.text(), key, "").trim();
									map.put(key, value);
								}
							}
						}
					}
				}
				ReptileEsfHouseModel esfHouseModel = ReptileEsfHouseConvert.mapToHouseModel(map);
				if (null != houseNameEle) {
					esfHouseModel.setHouseName(houseNameEle.text());
					esfHouseModel.setHouseOtherName(StringUtils.split(houseNameEle.text(), " ")[0]);
				}
				if (null != priceTotalEle) {
					Element priceTotalE = priceTotalEle.selectFirst("i");
					Element priceAvgE = priceTotalEle.selectFirst("b");
					if (null != priceTotalE) {
						esfHouseModel.setHouseAllPrice(priceTotalE.text());
					}
					if (null != priceAvgE) {
						esfHouseModel.setHousePrice(priceAvgE.text());
						esfHouseModel.setHousePriceUnit("1");
					}
				}
				esfHouseModel.setHouseUrl(houseUrl);
				esfHouseModel.setInformationSources("链家");
				esfHouseModel.setHouseCity(cityName);
				esfHouseModel.setHousePart(EsfCityAreaConstants.ESF_HOUSE_CITY.keyMap.get(part));
				esfHouseModel.setIsNewHouse("二手房");
				esfHouseModel.setInfoUpdateTime(
						DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd") + " 00:00:00"));

				// 入库保存，先查询此二手房有没有存库
				ReptileEsfHouseSearch search = new ReptileEsfHouseSearch();
				search.setEqualInformationSources("链家");
				search.setEqualHousePart(esfHouseModel.getHousePart());
				search.setEqualHouseCity(esfHouseModel.getHouseCity());
				search.setEqualHouseName(esfHouseModel.getHouseName());
				search.setEqualHouseType(esfHouseModel.getHouseType());
				ReptileEsfHouseModel newEsfModel = reptileServiceManage.reptileEsfHouseService.first(search);
				ReptileEsfHousePriceModel priceModel = new ReptileEsfHousePriceModel();
				String priceUnit = null;
				if (StringUtils.contains(esfHouseModel.getHousePriceUnit(), "㎡")
						|| StringUtils.contains(esfHouseModel.getHousePriceUnit(), "米")
						|| StringUtils.contains(esfHouseModel.getHousePriceUnit(), "平")) {
					priceUnit = "1";
				} else if (StringUtils.contains(esfHouseModel.getHousePriceUnit(), "套")) {
					priceUnit = "2";
				}
				priceModel.setHousePriceUnit(priceUnit);
				String ssPrice = esfHouseModel.getHousePrice();
				if (StringUtils.contains(esfHouseModel.getHousePrice(), "地图")) {
					ssPrice = "0";
				}
				if (StringUtils.contains(ssPrice, "元")) {
					ssPrice.substring(0, ssPrice.indexOf("元"));
				}
				esfHouseModel.setHousePrice(ssPrice);
				priceModel.setHousePrice(ssPrice);
				priceModel.setInfoSource("链家");
				if (null == newEsfModel) {
					System.out.println("保存链家二手房（成交）：" + esfHouseModel.getHouseName());
					reptileServiceManage.reptileEsfHouseService.add(esfHouseModel, sessionUser);
					priceModel.setHouseId(esfHouseModel.getId());
					reptileServiceManage.reptileEsfHousePriceService.add(priceModel, sessionUser);
				} else {
					System.out.println("修改链家二手房（成交）：" + esfHouseModel.getHouseName());
					ReflectUtils.copySameFieldToTargetFilterNull(esfHouseModel, newEsfModel);
					reptileServiceManage.reptileEsfHouseService.edit(newEsfModel, sessionUser);
					priceModel.setHouseId(newEsfModel.getId());
					reptileServiceManage.reptileEsfHousePriceService.add(priceModel, sessionUser);
				}
				Thread.sleep(1000);
			}
			spider(url, part, ++index, reptileServiceManage, sessionUser);
		} catch (InterruptedException e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}

}
