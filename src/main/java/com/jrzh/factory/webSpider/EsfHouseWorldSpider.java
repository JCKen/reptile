package com.jrzh.factory.webSpider;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.contants.EsfCityAreaConstants;
import com.jrzh.factory.base.inf.SpiderInf;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.model.reptile.ReptileEsfHouseModel;
import com.jrzh.mvc.model.reptile.ReptileEsfHousePriceModel;
import com.jrzh.mvc.search.reptile.ReptileEsfHouseSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

//房天下  成交二手房 
public class EsfHouseWorldSpider implements SpiderInf<ReptileEsfHouseModel> {

	private final static String CITY_URL = "https://{0}.esf.fang.com/chengjiao";

	private static Log log = LogFactory.getLog(EsfHouseWorldSpider.class);

	public void saveHouseInfoFromHouseWorldInGZ(ReptileServiceManage reptileServiceManage, SessionUser user)
			throws ProjectException {
		EsfHouseWorldSpider houseWorldSpider = new EsfHouseWorldSpider();

		try {
			for (String city : EsfCityAreaConstants.ESF_HOUSE_WORLD_AREA.cityMap) {
				String url = MessageFormat.format(CITY_URL, city);

				ArrayList<String> list = new ArrayList<>();
				list = EsfCityAreaConstants.ESF_HOUSE_WORLD_AREA.valueMap.get(city);
				for (String part : list) {
					houseWorldSpider.spider(url, part, 1, reptileServiceManage, user);
				}
			}
		} catch (ProjectException e) {
			e.printStackTrace();
			throw e;
		}

		// 单个城市数据测试

		/*
		 * try { String url = MessageFormat.format(CITY_URL,
		 * EsfCityAreaConstants.ESF_HOUSE_WORLD_AREA.cityMap[9]); if
		 * (StringUtils.equals(url, "https://bj.esf.fang.com/")) { url =
		 * "https://esf.fang.com/"; } ArrayList<String> list = new ArrayList<>(); list =
		 * EsfCityAreaConstants.ESF_HOUSE_WORLD_AREA.valueMap
		 * .get(EsfCityAreaConstants.ESF_HOUSE_WORLD_AREA.cityMap[9]); for (String part
		 * : list) { houseWorldSpider.spider(url, part, 1, reptileServiceManage, user);
		 * } } catch (ProjectException e) { e.printStackTrace(); throw e; }
		 */

	}

	@Override
	public void spider(String url, String part, Integer index, ReptileServiceManage reptileServiceManage,
			SessionUser user) throws ProjectException {
		if (StringUtils.contains(url, "https://bj")) {
			url = "https://esf.fang.com/chengjiao";
		}
		if (StringUtils.contains(url, "https://qy")) {
			url = "https://qingyuan.esf.fang.com/chengjiao";
		}
		Date infoUpdateTime = new Date();
		try {
			Map<String, String> header = new HashMap<String, String>();
			header.put("Host", url);
			header.put("User-Agent", " Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0");
			header.put("Accept", "  text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			header.put("Accept-Language", "zh-cn,zh;q=0.5");
			header.put("Connection", "keep-alive");
			Document document = new Document("");
			try {
				if (index == 1) {
					document = Jsoup.connect(url + part).headers(header).timeout(50000).get();
				} else {
					document = Jsoup.connect(url + part + "/i3" + index).headers(header).timeout(50000).get();
				}
			} catch (Exception e) {
				try {
					Thread.sleep(1000);
					if (index == 1) {
						document = Jsoup.connect(url + part).headers(header).timeout(50000).get();
					} else {
						document = Jsoup.connect(url + part + "/i3" + index).headers(header).timeout(50000).get();
					}
				} catch (Exception e1) {
					if (e1.getMessage().contains("Connection refused: connect")) {
						log.info(url + part + " ：连接被拒绝访问");
					} else {
						log.error(e1);
					}
					return;
				}
			}
			if (null == document)
				return;
			Elements elementLis = document.select(".houseList");
			if (null == elementLis)
				return;
			Elements elements = elementLis.select(".list");
			for (Element element : elements) {
				Element titleEle = element.selectFirst(".title");
				if (null == titleEle)
					continue;
				String houseName = titleEle.text();
				Element infoEle = titleEle.selectFirst("a");
				Document houseDoc = null;
				if (null != infoEle) {
					// 判断超链接地址为假地址
					if (StringUtils.equals(infoEle.attr("href"), "javascript:void(0)")) {
						log.info("无法查询该地区成交房数据，地址是个假地址");
						continue;
					}
					String infoUrl = url + infoEle.attr("href").replace("/chengjiao", "");
					// 连接超时
					try {
						houseDoc = Jsoup.connect(infoUrl).timeout(50000).get();
					} catch (Exception e) {
						try {
							houseDoc = Jsoup.connect(infoUrl).timeout(50000).get();
						} catch (Exception e2) {
							continue;
						}
					}
				} else {
					continue;
				}
				if (houseDoc == null)
					continue;
				Element priceAllEle;
				Element priceEle;
				Element orientationEle;
				Element informidEle;
				try {
					priceAllEle = houseDoc.selectFirst(".red");
					priceEle = houseDoc.selectFirst(".black");
					orientationEle = houseDoc.select(".black").last();
					informidEle = houseDoc.selectFirst(".informid");
				} catch (Exception e) {
					continue;
				}
				String priceAll = null;
				if (null != priceAllEle) {
					priceAll = priceAllEle.text() + "万";
				}
				String price = null;
				if (null != priceEle) {
					price = priceEle.text();
				}
				String orientation = null;
				if (null != orientationEle) {
					orientation = orientationEle.text();
				}
				String houseArea = null;
				String houseFloor = null;
				String houseAddress = null;
				if (null != informidEle) {
					Elements selectP = informidEle.select("p");
					if (null != selectP) {
						for (Element p : selectP) {
							if (StringUtils.equals(StringUtils.split(p.text(), "：")[0], "面积")) {
								houseArea = StringUtils.split(p.text(), "：")[1].replace("�O", "");
							}
							if (StringUtils.equals(StringUtils.split(p.text(), "：")[0], "楼层")) {
								houseFloor = StringUtils.split(p.text(), "：")[1];
							}
							if (StringUtils.equals(StringUtils.split(p.text(), "：")[0], "小区")) {
								houseAddress = StringUtils.split(p.text(), "：")[1];
							}
						}
					}
				}
				String cityName = null;
				if (StringUtils.contains(url, "https://gz")) {
					cityName = "广州";
				} else if (StringUtils.contains(url, "https://sz")) {
					cityName = "深圳";
				} else if (StringUtils.contains(url, "https://esf.fang.com/")) {
					cityName = "北京";
				} else if (StringUtils.contains(url, "https://sh")) {
					cityName = "上海";
				} else if (StringUtils.contains(url, "https://cd")) {
					cityName = "成都";
				} else if (StringUtils.contains(url, "https://tj")) {
					cityName = "天津";
				} else if (StringUtils.contains(url, "https://dg")) {
					cityName = "东莞";
				} else if (StringUtils.contains(url, "https://fs")) {
					cityName = "佛山";
				} else if (StringUtils.contains(url, "https://zh")) {
					cityName = "珠海";
				} else if (StringUtils.contains(url, "https://qingyuan")) {
					cityName = "清远";
				}
				// 保存二手房信息
				ReptileEsfHouseModel reptileEsfHouseModel = new ReptileEsfHouseModel();
				reptileEsfHouseModel.setAreaCovered(houseArea);
				reptileEsfHouseModel.setBuildingArea(houseArea);
				reptileEsfHouseModel.setHouseFloor(houseFloor);
				reptileEsfHouseModel.setHouseAddress(houseAddress);
				reptileEsfHouseModel.setHouseOrientation(orientation);
				reptileEsfHouseModel.setHouseName(houseName);
				reptileEsfHouseModel.setHouseOtherName(StringUtils.split(houseName, " ")[0]);
				reptileEsfHouseModel.setHouseAllPrice(priceAll);
				reptileEsfHouseModel.setHousePrice(price);
				reptileEsfHouseModel.setHousePriceUnit("1");
				reptileEsfHouseModel.setInfoUpdateTime(
						DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd") + " 00:00:00"));
				reptileEsfHouseModel.setHouseCity(cityName);
				reptileEsfHouseModel.setInformationSources("房天下");
				reptileEsfHouseModel.setHousePart(EsfCityAreaConstants.ESF_HOUSE_WORLD_CITY.keyMap.get(part));

				// 入库保存，先查询此二手房有没有存库
				ReptileEsfHouseSearch search = new ReptileEsfHouseSearch();
				search.setEqualInformationSources("房天下");
				search.setEqualHousePart(reptileEsfHouseModel.getHousePart());
				search.setEqualHouseCity(reptileEsfHouseModel.getHouseCity());
				search.setEqualHouseName(reptileEsfHouseModel.getHouseName());
				search.setEqualHouseType(reptileEsfHouseModel.getHouseType());
				ReptileEsfHouseModel newEsfModel = reptileServiceManage.reptileEsfHouseService.first(search);
				ReptileEsfHousePriceModel priceModel = new ReptileEsfHousePriceModel();
				// 保存价格
				priceModel.setHousePriceUnit("1");
				String ssPrice = reptileEsfHouseModel.getHousePrice();
				if (StringUtils.contains(reptileEsfHouseModel.getHousePrice(), "地图")) {
					ssPrice = "0";
				}
				reptileEsfHouseModel.setHousePrice(ssPrice);
				priceModel.setHousePrice(ssPrice);
				priceModel.setInfoUpdateTime(
						DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd") + " 00:00:00"));
				priceModel.setInfoSource("房天下");
				if (null == newEsfModel) {
					try {
						System.out.println("保存房天下二手房（成交）：" + reptileEsfHouseModel.getHouseName());
						reptileServiceManage.reptileEsfHouseService.add(reptileEsfHouseModel, user);
					} catch (Exception e) {
						System.out.println("_other: " + reptileEsfHouseModel.getOther());
						reptileEsfHouseModel.setOther("");
						reptileServiceManage.reptileEsfHouseService.add(reptileEsfHouseModel, user);
					}
					priceModel.setHouseId(reptileEsfHouseModel.getId());
					reptileServiceManage.reptileEsfHousePriceService.add(priceModel, user);
				} else {
					System.out.println("修改房天下二手房（成交）：" + reptileEsfHouseModel.getHouseName());
					ReflectUtils.copySameFieldToTargetFilterNull(reptileEsfHouseModel, newEsfModel);
					try {
						reptileServiceManage.reptileEsfHouseService.edit(newEsfModel, user);
					} catch (Exception e) {
						newEsfModel.setOther("");
						reptileServiceManage.reptileEsfHouseService.edit(newEsfModel, user);
					}
					priceModel.setHouseId(newEsfModel.getId());
					reptileServiceManage.reptileEsfHousePriceService.add(priceModel, user);
				}
				Thread.sleep(300);
			}
			Elements nextElement = document.select(".fanye");
			if (null == nextElement || null == nextElement.last() || !StringUtils.contains(nextElement.text(), "末页"))
				return;
			spider(url, part, ++index, reptileServiceManage, user);
		} catch (InterruptedException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}
}
