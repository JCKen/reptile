package com.jrzh.factory.webSpider;

import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.contants.EsfCityAreaConstants;
import com.jrzh.factory.base.inf.SpiderInf;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.framework.contants.GeneralConstant;
import com.jrzh.mvc.convert.reptile.ReptileEsfHousesXiaoquConvert;
import com.jrzh.mvc.model.reptile.ReptileEsfHousePriceModel;
import com.jrzh.mvc.model.reptile.ReptileEsfHousesXiaoquModel;
import com.jrzh.mvc.search.reptile.ReptileEsfHousesXiaoquSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
import com.jrzh.util.ChromeOptionsUtils;

//房天下  二手房——楼盘·小区
public class EsfXiaoqu_Fangtianxia implements SpiderInf<ReptileEsfHousesXiaoquModel> {

	private final static String CITY_URL = "https://{0}.esf.fang.com/housing/";

	private static Log log = LogFactory.getLog(EsfXiaoqu_Fangtianxia.class);

	static SimpleDateFormat sdf_year = new SimpleDateFormat("yyyy");
	static SimpleDateFormat sdf_all = new SimpleDateFormat("yyyy-MM-dd");

	public void saveHouseInfoFromHouseWorldInGZ(ReptileServiceManage reptileServiceManage, SessionUser user)
			throws ProjectException {
		EsfXiaoqu_Fangtianxia houseWorldSpider = new EsfXiaoqu_Fangtianxia();

		
		  try { for (String city : EsfCityAreaConstants.ESF_HOUSE_WORLD_AREA.cityMap) {
		  String url = MessageFormat.format(CITY_URL, city);
		  
		  ArrayList<String> list = new ArrayList<>(); list =
		  EsfCityAreaConstants.ESF_HOUSE_WORLD_AREA.valueMap.get(city); for (String
		  part : list) { houseWorldSpider.spider(url, part, 1, reptileServiceManage,
		  user); } } } catch (ProjectException e) { e.printStackTrace(); throw e; }
		 

		// 单个城市数据测试

		/*
		 * try { String url = MessageFormat.format(CITY_URL,
		 * EsfCityAreaConstants.ESF_HOUSE_WORLD_AREA.cityMap[12]); ArrayList<String>
		 * list = new ArrayList<>(); list =
		 * EsfCityAreaConstants.ESF_HOUSE_WORLD_AREA.valueMap
		 * .get(EsfCityAreaConstants.ESF_HOUSE_WORLD_AREA.cityMap[12]); for (String part
		 * : list) { houseWorldSpider.spider(url, part, 1, reptileServiceManage, user);
		 * } } catch (ProjectException e) { e.printStackTrace(); throw e; }
		 */

	}

	@Override
	public void spider(String url, String part, Integer index, ReptileServiceManage reptileServiceManage,
			SessionUser user) throws ProjectException {
		if (StringUtils.contains(url, "https://bj")) {
			url = "https://esf.fang.com/housing/";
		}
		if (StringUtils.contains(url, "https://xa")) {
			url = "https://xian.esf.fang.com/housing/";
		}
		if (StringUtils.contains(url, "https://hz")) {
			url = "https://huizhou.esf.fang.com/housing/";
		}
		ChromeDriverService service = ChromeOptionsUtils.getChromeDriverService();
        try {
			service.start();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        WebDriver driver = new RemoteWebDriver(service.getUrl(), DesiredCapabilities.chrome());
		try {
			Document document = null;
			Response res = null;
			String connect = url + part + "__0_0_0_0_" + index + "_0_0_0/";
			connect = "http://search.fang.com/captcha-verify/redirect?h=" + connect;
			String aa = null;
			try {
				driver.get(connect);
				Thread.sleep(2000);
				aa = driver.getPageSource();
				document = Jsoup.parse(aa);
			} catch (Exception e) {
				return ;
			}
			if (null == document)
				return;
			System.out.println(connect);
			ReptileEsfHousesXiaoquModel reptileEsfHouseModel = null;
			Elements elementLi = document.select(".houseList");
			if (null == elementLi)
				return;
			Elements elements = elementLi.select(".list");
			for (Element element : elements) {
				reptileEsfHouseModel = new ReptileEsfHousesXiaoquModel();
				Element baseInfo = element.selectFirst("dd");
				if (null == baseInfo)
					continue;
				Element titleEle = element.selectFirst(".plotTit");
				if (null == titleEle)
					continue;
				reptileEsfHouseModel.setHouseName(titleEle.text());

				Element houseBuildingYear = element.selectFirst(".sellOrRenthy").select("li").last();
				if (houseBuildingYear == null) {
					continue;
				} else {
					String year = houseBuildingYear.text();
					year = year.substring(0, year.indexOf("年建成"));
					Integer year_build = Integer.parseInt(year);
					Integer now_year = Integer.parseInt(DateUtil.format(new Date(), "yyyy"));
					// System.out.println("建成年份： " + year);
					if (now_year - year_build > 10 || now_year - year_build < 0) {
						// System.out.println("楼龄>10年，跳过\n\n\n");
						continue;
					}
					reptileEsfHouseModel.setHousesBuildingYear(year);
				}
				Element typeEle = element.selectFirst(".plotFangType");
				if (null == typeEle)
					continue;
				// System.out.println("楼盘类型：" + typeEle.text());
				String houseType = typeEle.text();
				reptileEsfHouseModel.setHousesType(houseType);
				Element priceEle = element.selectFirst(".listRiconwrap").selectFirst(".priceAverage");
				if (priceEle == null)
					continue;
				String housePrice = priceEle.text().substring(0, priceEle.text().indexOf("元"));
				reptileEsfHouseModel.setHousesPrice(housePrice);
				// System.out.println("楼盘均价：" + housePrice);
				if (StringUtils.equals(housePrice, "暂无")) {
					continue;
				}
				Document houseDoc = null;
				// 判断超链接地址为假地址
				if (StringUtils.equals(titleEle.attr("href"), "javascript:void(0)")) {
					continue;
				}
				String infoUrl = "https:" + titleEle.attr("href");
				// 连接超时

				try {
					res = Jsoup.connect(infoUrl).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom()))
							.timeout(50000).postDataCharset("gb18030").execute();
					houseDoc = Jsoup.parse(new String(res.bodyAsBytes(), "gb18030"), infoUrl);
				} catch (Exception e) {
					try {
						houseDoc = Jsoup.parse(new String(res.bodyAsBytes(), "gb18030"), infoUrl);
					} catch (Exception e2) {
						continue;
					}
				}
				if (houseDoc == null)
					continue;
				Element detailEle = null;
				if (StringUtils.contains(houseType, "写字楼") || StringUtils.contains(houseType, "商铺")) {
					// 写字楼、商铺
					Element floatr = houseDoc.selectFirst(".floatr");
					if (floatr == null)
						continue;
					detailEle = floatr.selectFirst("a");
				} else {
					// 住宅、别墅
					// 参考价月份
					try {
						Element rpMonthEle = houseDoc.selectFirst(".Rbiginfo");
						if (rpMonthEle != null) {
							if (StringUtils.isNotBlank(rpMonthEle.text())) {
								Integer mon = Integer.parseInt(rpMonthEle.text().substring(0, 2)) * 1;
								reptileEsfHouseModel.setRpMonth(mon.toString());
							}
						}
					} catch (Exception e) {
						reptileEsfHouseModel.setRpMonth(null);
					}
					Element xqxqEle = houseDoc.selectFirst("li[data='xqxq']");
					if (xqxqEle == null)
						continue;
					detailEle = xqxqEle.selectFirst("a");
				}
				if (detailEle == null)
					continue;
				String houseUrl = "https:" + detailEle.attr("href");
				reptileEsfHouseModel.setUrl(houseUrl);

				try {
					res = Jsoup.connect(houseUrl).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom()))
							.timeout(50000).postDataCharset("gb18030").execute();
					houseDoc = Jsoup.parse(new String(res.bodyAsBytes(), "gb18030"), houseUrl);
				} catch (Exception e) {
					try {
						houseDoc = Jsoup.parse(new String(res.bodyAsBytes(), "gb18030"), houseUrl);
					} catch (Exception e2) {
						continue;
					}
				}
				// System.out.println("详情页地址：" + "https:" + detailEle.attr("href"));
				if (houseDoc == null)
					continue;
				Elements infos = null;
				Map<String, String> infoMap = new HashMap<String, String>();
				if (StringUtils.contains(houseType, "写字楼") || StringUtils.contains(houseType, "商铺")) {
					// 商铺、写字楼
					Element mt10 = houseDoc.selectFirst("div[class='wrap mt10']");
					if (mt10 == null)
						continue;
					infos = mt10.select("dl[class='xiangqing']");
					if (infos == null)
						continue;
					for (Element info : infos) {
						Elements dds = info.select("dd");
						for (Element dd : dds) {
							String detail = dd.text();
							if (StringUtils.contains(detail, " ")) {
								detail = detail.replace(" ", "");
							}
							int sub_index = detail.indexOf("：");
							String key = detail.substring(0, sub_index + 1);
							String value = detail.substring(sub_index + 1);
							infoMap.put(key, value);
						}

					}
				} else {
					// 别墅、住宅
					// 别名
					Element otherName = houseDoc.selectFirst(".bm_title");
					if (otherName != null) {
						infoMap.put("别名", otherName.text().split("别名：")[1]);
					}
					// 详细信息
					Element con_left = houseDoc.selectFirst(".con_left");
					if (con_left == null)
						continue;
					infos = con_left.select(".box");
					if (infos == null)
						continue;
					for (Element info : infos) {
						Element box_tit = info.selectFirst(".box_tit");
						if (box_tit == null)
							continue;
						String infoType = box_tit.select("h3").text();
						if (StringUtils.contains(infoType, "基本信息")) {
							Element inforwrap = info.selectFirst(".inforwrap").selectFirst(".mr30");
							Elements dds = inforwrap.select("dd");
							for (Element dd : dds) {
								String detail = dd.text();
								if (StringUtils.contains(detail, " ")) {
									detail = detail.replace(" ", "");
								}
								int sub_index = detail.indexOf("：");
								String key = detail.substring(0, sub_index + 1);
								String value = detail.substring(sub_index + 1);
								infoMap.put(key, value);
							}
						}
						if (StringUtils.contains(infoType, "交通状况")) {
							String traffic = info.selectFirst(".inforwrap").selectFirst("dt").text();
							reptileEsfHouseModel.setTraffic(traffic);
						}
						if (StringUtils.contains(infoType, "周边信息")) {
							Element inforwrap = info.selectFirst(".inforwrap").selectFirst(".mr30");
							Elements dts = inforwrap.select("dt");
							for (Element dt : dts) {
								String detail = dt.text();
								if (StringUtils.contains(detail, " ")) {
									detail = detail.replace(" ", "");
								}
								int sub_index = detail.indexOf("：");
								String key = detail.substring(0, sub_index + 1);
								String value = detail.substring(sub_index + 1);
								infoMap.put(key, value);
							}
						}
					}
				}

				// 将详细信息进行判断赋值
				ReptileEsfHousesXiaoquConvert.convertBaseInfo(reptileEsfHouseModel, infoMap);
				if (StringUtils.isBlank(reptileEsfHouseModel.getBuildingArea())
						|| StringUtils.isBlank(reptileEsfHouseModel.getFloorArea()))
					continue;
				String cityName = null;
				if (StringUtils.contains(url, "https://gz")) {
					cityName = "广州";
				} else if (StringUtils.contains(url, "https://sz")) {
					cityName = "深圳";
				} else if (StringUtils.contains(url, "https://esf.fang.com/")) {
					cityName = "北京";
				} else if (StringUtils.contains(url, "https://sh")) {
					cityName = "上海";
				} else if (StringUtils.contains(url, "https://cd")) {
					cityName = "成都";
				} else if (StringUtils.contains(url, "https://tj")) {
					cityName = "天津";
				} else if (StringUtils.contains(url, "https://fs")) {
					cityName = "佛山";
				} else if (StringUtils.contains(url, "https://zh")) {
					cityName = "珠海";
				} else if (StringUtils.contains(url, "https://xian")) {
					cityName = "西安";
				} else if (StringUtils.contains(url, "https://ks")) {
					cityName = "昆山";
				} else if (StringUtils.contains(url, "https://huizhou")) {
					cityName = "惠州";
				} else if (StringUtils.contains(url, "https://jm")) {
					cityName = "江门";
				} else if (StringUtils.contains(url, "https://nb")) {
					cityName = "宁波";
				}
				reptileEsfHouseModel.setInformationSources("房天下");
				reptileEsfHouseModel.setInfoUpdateTime(DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
				reptileEsfHouseModel.setHousesCity(cityName);
				String housePart = EsfCityAreaConstants.ESF_HOUSE_WORLD_CITY.keyMap.get(part);
				reptileEsfHouseModel.setHousesPart(housePart);
				// System.out.println("楼盘所在区域："+housePart);
				// System.out.println("楼盘所在城市：" + cityName+"\n\n\n");

				/*
				 * System.out.println("楼盘名称：" + reptileEsfHouseModel.getHouseName());
				 * System.out.println("城市：" + reptileEsfHouseModel.getHousesCity());
				 * System.out.println("楼盘区域：" + reptileEsfHouseModel.getHousesPart());
				 * System.out.println("楼盘地址：" + reptileEsfHouseModel.getHousesAddress());
				 * System.out.println("楼盘建筑年代：" + reptileEsfHouseModel.getHousesBuildingYear());
				 * System.out.println("楼盘类型：" + reptileEsfHouseModel.getHousesType());
				 * System.out.println("楼盘均价：" + reptileEsfHouseModel.getHousesPrice());
				 * System.out.println("产权年限：" + reptileEsfHouseModel.getYearOfPropertyRights());
				 * System.out.println("信息来源：" + reptileEsfHouseModel.getInformationSources());
				 * System.out.println("更新时间：" + reptileEsfHouseModel.getInfoUpdateTime());
				 * System.out.println("附近交通：" + reptileEsfHouseModel.getTraffic());
				 * System.out.println("幼儿园：" + reptileEsfHouseModel.getKindergarten());
				 * System.out.println("中小学：" + reptileEsfHouseModel.getSchool());
				 * System.out.println("商场：" + reptileEsfHouseModel.getPowerCenter());
				 * System.out.println("医院：" + reptileEsfHouseModel.getHospital());
				 * System.out.println("银行：" + reptileEsfHouseModel.getBank());
				 * System.out.println("邮政：" + reptileEsfHouseModel.getPost());
				 * System.out.println("其他：" + reptileEsfHouseModel.getOther());
				 * System.out.println("占地面积：" + reptileEsfHouseModel.getFloorArea());
				 * System.out.println("建筑面积：" + reptileEsfHouseModel.getBuildingArea());
				 * System.out.println("url：" + reptileEsfHouseModel.getUrl());
				 * System.out.println("===================================\n\n\n");
				 */

				ReptileEsfHousesXiaoquSearch search = new ReptileEsfHousesXiaoquSearch();
				search.setEqualInformationSources("房天下");
				search.setEqualHousesPart(reptileEsfHouseModel.getHousesPart());
				search.setEqualHousesCity(reptileEsfHouseModel.getHousesCity());
				search.setequalHouseName(reptileEsfHouseModel.getHouseName());
				search.setEqualHousesType(reptileEsfHouseModel.getHousesType());
				ReptileEsfHousesXiaoquModel newEsfhouse = reptileServiceManage.reptileEsfHousesXiaoquService
						.first(search);
				ReptileEsfHousePriceModel priceModel = new ReptileEsfHousePriceModel();
				// 保存价格
				priceModel.setHousePriceUnit("1");
				String ssPrice = reptileEsfHouseModel.getHousesPrice();
				priceModel.setHousePrice(ssPrice);
				priceModel.setInfoUpdateTime(DateUtil.format(reptileEsfHouseModel.getInfoUpdateTime()));
				priceModel.setInfoSource("房天下");
				if (null == newEsfhouse) {
					try {
						System.out.println("保存房天下二手房楼盘：" + reptileEsfHouseModel.getHousesCity() + " "
								+ reptileEsfHouseModel.getHousesPart() + " " + reptileEsfHouseModel.getHouseName());
						reptileServiceManage.reptileEsfHousesXiaoquService.add(reptileEsfHouseModel, user);
					} catch (Exception e) {
						System.out.println("_other: " + reptileEsfHouseModel.getOther());
						reptileEsfHouseModel.setOther("");
						reptileServiceManage.reptileEsfHousesXiaoquService.add(reptileEsfHouseModel, user);
					}
					priceModel.setHouseId(reptileEsfHouseModel.getId());
					reptileServiceManage.reptileEsfHousePriceService.add(priceModel, user);
				} else {
					System.out.println("修改房天下二手房楼盘：" + reptileEsfHouseModel.getHousesCity() + " "
							+ reptileEsfHouseModel.getHousesPart() + " " + reptileEsfHouseModel.getHouseName());
					ReflectUtils.copySameFieldToTargetFilterNull(reptileEsfHouseModel, newEsfhouse);
					try {
						reptileServiceManage.reptileEsfHousesXiaoquService.edit(newEsfhouse, user);
					} catch (Exception e) {
						newEsfhouse.setOther("");
						reptileServiceManage.reptileEsfHousesXiaoquService.edit(newEsfhouse, user);
					}
					priceModel.setHouseId(newEsfhouse.getId());
					reptileServiceManage.reptileEsfHousePriceService.add(priceModel, user);
				}
				Thread.sleep(300);

			}
			Elements nextElement = document.select(".fanye");
			if (driver != null) {
				driver.quit();
				service.stop();
			}
			if (null == nextElement || null == nextElement.last() || !StringUtils.contains(nextElement.text(), "末页"))
				return;
			spider(url, part, ++index, reptileServiceManage, user);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			if (driver != null) {
				driver.quit();
				service.stop();
			}
		}
	}
}