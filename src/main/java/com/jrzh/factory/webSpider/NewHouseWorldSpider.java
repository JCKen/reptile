package com.jrzh.factory.webSpider;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.contants.CityAreaConstants;
import com.jrzh.factory.base.inf.SpiderInf;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileNewHouseConvert;
import com.jrzh.mvc.model.reptile.ReptileHousePriceModel;
import com.jrzh.mvc.model.reptile.ReptileNewHouseModel;
import com.jrzh.mvc.search.reptile.ReptileNewHouseSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public class NewHouseWorldSpider implements SpiderInf<ReptileNewHouseModel> {

	private final static String CITY_URL = "https://{0}.fang.lianjia.com/loupan/";

	private static Log log = LogFactory.getLog(NewHouseWorldSpider.class);

	public void saveNewHouseInfoFromInGZ(ReptileServiceManage reptileServiceManage, SessionUser user)
			throws ProjectException {
		NewHouseWorldSpider newHouseWorldSpider = new NewHouseWorldSpider();

		// 单个城市数据测试
		try {
			String url = MessageFormat.format(CITY_URL, CityAreaConstants.NEW_HOUSE_AREA.cityMap[8]);
			ArrayList<String> list = new ArrayList<>();
			list = CityAreaConstants.NEW_HOUSE_AREA.valueMap.get(CityAreaConstants.NEW_HOUSE_AREA.cityMap[8]);
			for (String part : list) {
				newHouseWorldSpider.spider(url, part, 1, reptileServiceManage, user);
			}
		} catch (ProjectException e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Override
	public void spider(String url, String part, Integer index, ReptileServiceManage reptileServiceManage, SessionUser user) throws ProjectException {
		if (StringUtils.contains(url, "https://hz")) {
			url = "https://hui.fang.lianjia.com/loupan/";
		}
		Date infoUpdateTime = new Date();
		Document document;
		String connect = url + part + "/pg" + index;
		Response res = null;
		try {
			res = Jsoup.connect(connect).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom())).timeout(50000).postDataCharset("utf-8").execute();
		} catch (Exception e) {
			try {
				res = Jsoup.connect(connect).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom())).timeout(50000).postDataCharset("utf-8").execute();
			} catch (Exception e2) {
				return;
			}
		}
		// 解决连接超时
		try {
			log.info("链家一手房链接：" + url + part + "/pg" + index);
			try {
				document = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), connect);
			} catch (Exception e) {
				try {
					document = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), connect);
				} catch (Exception e2) {
					return;
				}
			}
			if (document == null)
				return;
			String nextElement = document.selectFirst(".page-box").attr("data-total-count");
			if (StringUtils.isEmpty(nextElement) || StringUtils.equals("0", nextElement)
					|| StringUtils.equals(String.valueOf(index - 1), nextElement)) {
				return;
			}
			Elements elements = document.getElementsByAttributeValue("class",
					"resblock-list post_ulog_exposure_scroll has-results");
			if (null == elements)
				return;
			Iterator<Element> elIterator = elements.iterator();
			ReptileNewHouseSearch search = new ReptileNewHouseSearch();
			while (elIterator.hasNext()) {
				String urls = StringUtils.replace(url, "/loupan/", "");
				String cityName = null;
				if (StringUtils.contains(urls, "https://gz")) {
					cityName = "广州";
				} else if (StringUtils.contains(urls, "https://sz")) {
					cityName = "深圳";
				} else if (StringUtils.contains(urls, "https://bj")) {
					cityName = "北京";
				} else if (StringUtils.contains(urls, "https://sh")) {
					cityName = "上海";
				} else if (StringUtils.contains(urls, "https://cd")) {
					cityName = "成都";
				} else if (StringUtils.contains(urls, "https://tj")) {
					cityName = "天津";
				} else if (StringUtils.contains(urls, "https://fs")) {
					cityName = "佛山";
				} else if (StringUtils.contains(urls, "https://zh")) {
					cityName = "珠海";
				} else if (StringUtils.contains(urls, "https://hui")) {
					cityName = "惠州";
				}else if (StringUtils.contains(urls, "https://xa")) {
					cityName = "西安";
				}
				Elements resblockName = elIterator.next().select(".resblock-name");
				if (null == resblockName)
					continue;
				Elements selectA = resblockName.select("a");
				if (null == selectA)
					continue;
				String selectHref = selectA.attr("href");
				if (StringUtils.isBlank(selectHref))
					continue;
				String houseUrl = urls + selectHref;
				Document houseDocument = null;
				// 解决连接超时

				try {
					res = Jsoup.connect(houseUrl).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom()))
							.timeout(50000).postDataCharset("utf-8").execute();
					houseDocument = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), houseUrl);
				} catch (Exception e) {
					try {
						houseDocument = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), houseUrl);
					} catch (Exception e2) {
						continue;
					}
				}
				Thread.sleep(50);
				if (houseDocument == null)
					continue;

				Element priceElement = houseDocument.selectFirst(".junjia");
				Element priceUnitElement = houseDocument.selectFirst(".yuan");
				Element nameElement = houseDocument.selectFirst(".DATA-PROJECT-NAME");
				Element otherNameElement = houseDocument.selectFirst(".other-name");
				Element stateElement = houseDocument.selectFirst(".state");
				// Element typeElement = houseDocument.selectFirst(".popular");
				// // 房屋类型
				Element houseAreaEle = houseDocument.select(".houselist").last();
				// 主打戶型
				Elements mainUnitEles = houseDocument.select(".house-det");
				StringBuffer mainUnit = new StringBuffer();
				if (mainUnitEles != null) {
					for (Element unit : mainUnitEles) {
						Element info = unit.selectFirst(".info-li");
						if (info == null)
							continue;
						Element p1 = info.selectFirst(".p1");
						if (p1 == null)
							continue;
						String text = p1.text();
						if (text.contains(" "))
							text = text.replace(" ", "");
						if (text.contains("售罄"))
							text = text.replace("售罄", "");
						if (text.contains("在售"))
							text = text.replace("在售", "");
						mainUnit.append(text + ",");

					}
				}
				if (mainUnit != null && mainUnit.length() > 0)
					mainUnit = mainUnit.deleteCharAt(mainUnit.length() - 1);
				String area = null;
				if (null != houseAreaEle) {
					Element houseArea = houseAreaEle.selectFirst(".p1");
					if (null != houseArea) {
						area = houseArea.selectFirst("span").text();
						area = StringUtils.split(area, " ")[1];
						area = StringUtils.split(area, "m²")[0];
					}
				}
				if (null == houseDocument.selectFirst(".box-loupan"))
					continue;
				Elements housElements = houseDocument.selectFirst(".box-loupan").getElementsByAttributeValue("class",
						"desc-p clear");
				if (housElements == null)
					continue;
				Map<String, String> dataMap = new HashMap<String, String>();
				dataMap.put("houseUrlDetail", houseUrl + "xiangqing/");
				for (Element item : housElements) {
					String key = item.selectFirst("span").text();
					String value = item.select("span").last().text();
					dataMap.put(key, value);
				}

				// 爬取基本信息和周边
				ReptileNewHouseModel newHouseModel = ReptileNewHouseConvert.mapToHouseModel(dataMap);
				newHouseModel.setHouseBuildingArea(area);
				newHouseModel.setHouseUrl(houseUrl);
				newHouseModel.setHouseName(nameElement.text());
				if (null != otherNameElement) {
					newHouseModel.setHouseOtherName(otherNameElement.text().replace("别名/", ""));
				}
				if (null != stateElement) {
					newHouseModel.setHouseSaleStatus(stateElement.text());
				}
				if (null != priceUnitElement && null != priceElement) {
					if (StringUtils.contains(priceUnitElement.text(), "米")
							|| StringUtils.contains(priceUnitElement.text(), "m²")
							|| StringUtils.contains(priceUnitElement.text(), "平")) {
						try {
							String price = priceElement.text();
							if (StringUtils.isBlank(price)) {
								continue;
							}
							if (StringUtils.contains(price, "元")) {
								int flag = price.indexOf("元");
								price = price.substring(0, flag);
							}
							if (StringUtils.contains(price, ",")) {
								price = price.replace(",", "");
							}
							if (StringUtils.contains(price, ".")) {
								price = price.substring(0, price.indexOf("."));
							}
							int price_int;
							price_int = Integer.parseInt(price);
							if (price_int <= 0) {
								continue;
							}
							newHouseModel.setHousePrice(Integer.toString(price_int));
						} catch (Exception e) {
							continue;
						}
					}
					if (StringUtils.contains(priceUnitElement.text(), "套")) {
						try {
							if (StringUtils.isNotBlank(area)) {
								Long price = (Long.valueOf(priceElement.text()) * 10000) / Long.valueOf(area);
								if (price <= 0) {
									continue;
								}
								newHouseModel.setHousePrice(String.valueOf(price));
							} else {
								continue;
							}
						} catch (Exception e) {
							continue;
						}
					}
				} else {
					continue;
				}
				if (StringUtils.isEmpty(newHouseModel.getHousePrice()))
					continue;
				newHouseModel.setInformationSources("链家");
				newHouseModel.setHouseCity(cityName);
				if (StringUtils.isNotBlank(mainUnit.toString())) {
					if (mainUnit.toString().length() <= 64) {
						newHouseModel.setMainUnit(mainUnit.toString());
					}
				}
				newHouseModel.setHousePart(CityAreaConstants.NEW_HOUSE_CITY.keyMap.get(part));
				newHouseModel.setIsNewHouse("新房");
				newHouseModel.setInfoUpdateTime(
						DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd") + " 00:00:00"));

				if (StringUtils.isBlank(newHouseModel.getHouseAddress()))
					continue;
				// 入库保存，先查询此新房有没有存库
				String otherName = newHouseModel.getHouseOtherName();
				if (StringUtils.isNotBlank(newHouseModel.getHouseOtherName())) {
					otherName = StringUtils.split(newHouseModel.getHouseOtherName(), ",")[0];
				}
				if(StringUtils.equals(otherName, "无"))otherName=null;
				search.setEqualInformationSources("链家");
				search.setEqualHousePart(newHouseModel.getHousePart());
				search.setEqualHouseCity(newHouseModel.getHouseCity());
				search.setEqualHouseName(newHouseModel.getHouseName());
				search.setLikeHouseOtherName(otherName);
				search.setEqualHouseType(newHouseModel.getHouseType());
				ReptileNewHouseModel newModel = reptileServiceManage.reptileNewHouseService.first(search);
				ReptileHousePriceModel priceModel = new ReptileHousePriceModel();
				priceModel.setHousePriceUnit(newHouseModel.getHousePriceUnit());
				priceModel.setHousePrice(newHouseModel.getHousePrice());
				priceModel.setInfoSource("链家");
				if (null == newModel) {
					System.out.println("保存链家新房：" + cityName + " " + newHouseModel.getHousePart() + " "
							+ newHouseModel.getHouseName());
					reptileServiceManage.reptileNewHouseService.add(newHouseModel, user);
					priceModel.setHouseId(newHouseModel.getId());
					reptileServiceManage.reptileHousePriceService.add(priceModel, user);
				} else {
					System.out.println("修改链家新房：" + cityName + " " + newHouseModel.getHousePart() + " "
							+ newHouseModel.getHouseName());
					ReflectUtils.copySameFieldToTargetFilterNull(newHouseModel, newModel);
					reptileServiceManage.reptileNewHouseService.edit(newModel, user);
					priceModel.setHouseId(newModel.getId());
					reptileServiceManage.reptileHousePriceService.add(priceModel, user);
				}

				// 预售证爬取
				/*if (StringUtils.isNotBlank(dataMap.get("houseUrlDetail"))) {
					try {
						res = Jsoup.connect(dataMap.get("houseUrlDetail"))
								.userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom())).timeout(50000)
								.postDataCharset("utf-8").execute();
						document = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), dataMap.get("houseUrlDetail"));
						Element xTable = document.selectFirst("#xTable");
						if (xTable != null) {
							Element yushou = xTable.nextElementSibling();
							Elements trs = yushou.select("tr");
							for (Element tr : trs) {
								if (StringUtils.equals(cityName, "广州")) {
									Element td = tr.selectFirst("td");
									if (td != null) {
										String no = null;
										if (StringUtils.contains(td.text(), "预字第")) {
											no = tr.text().split("预字第")[1].split("号")[0];
										}
										String next = td.nextElementSibling().text();
										if (valiDateTimeWithLongFormat(next)) {
											String date = DateUtil.format(DateUtil.format(next, "yyyy-MM"))
													.split("-")[0];
											Integer year = Integer.parseInt(date);
											Integer now = Integer.parseInt(DateUtil.format(new Date(), "yyyy"));
											if (now - year <= 3 && now - year > 0) {
												GZYushouzheng.spider(priceModel.getHouseId(), no,
														CityAreaConstants.NEW_HOUSE_CITY.keyMap.get(part),
														reptileServiceManage, user);
											}

										}
									}
								}

							}
						}
					} catch (Exception e) {
						log.error(e);
						e.printStackTrace();
					}
				}*/

			}
			spider(url, part, ++index, reptileServiceManage, user);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}

	// 时间正则
	public static boolean valiDateTimeWithLongFormat(String timeStr) {
		String format = "((19|20)[0-9]{2})-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])";
		Pattern pattern = Pattern.compile(format);
		Matcher matcher = pattern.matcher(timeStr);
		if (matcher.matches()) {
			pattern = Pattern.compile("(\\d{4})-(\\d+)-(\\d+).*");
			matcher = pattern.matcher(timeStr);
			if (matcher.matches()) {
				int y = Integer.valueOf(matcher.group(1));
				int m = Integer.valueOf(matcher.group(2));
				int d = Integer.valueOf(matcher.group(3));
				if (d > 28) {
					Calendar c = Calendar.getInstance();
					c.set(y, m - 1, 1);
					int lastDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
					return (lastDay >= d);
				}
			}
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		Integer index = 1;
		String part = "liwan";
		Date infoUpdateTime = new Date();
		Document document;
		String url = "https://gz.fang.lianjia.com/loupan/";
		String connect = url + part + "/pg" + index;
		Response res = null;
		try {
			res = Jsoup.connect(connect).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom())).timeout(50000)
					.postDataCharset("utf-8").execute();
		} catch (Exception e) {
			try {
				res = Jsoup.connect(connect).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom()))
						.timeout(50000).postDataCharset("utf-8").execute();
			} catch (Exception e2) {
				return;
			}
		}
		// 解决连接超时
		try {
			log.info("链家一手房链接：" + connect);
			try {
				document = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), connect);
			} catch (Exception e) {
				try {
					document = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), connect);
				} catch (Exception e2) {
					return;
				}
			}
			if (document == null)
				return;
			String nextElement = document.selectFirst(".page-box").attr("data-total-count");
			if (StringUtils.isEmpty(nextElement) || StringUtils.equals("0", nextElement)
					|| StringUtils.equals(String.valueOf(index - 1), nextElement)) {
				return;
			}
			Elements elements = document.getElementsByAttributeValue("class",
					"resblock-list post_ulog_exposure_scroll has-results");
			if (null == elements)
				return;
			Iterator<Element> elIterator = elements.iterator();
			ReptileNewHouseSearch search = new ReptileNewHouseSearch();
			while (elIterator.hasNext()) {
				String urls = StringUtils.replace(url, "/loupan/", "");
				String cityName = null;
				if (StringUtils.contains(urls, "https://gz")) {
					cityName = "广州";
				} else if (StringUtils.contains(urls, "https://sz")) {
					cityName = "深圳";
				} else if (StringUtils.contains(urls, "https://bj")) {
					cityName = "北京";
				} else if (StringUtils.contains(urls, "https://sh")) {
					cityName = "上海";
				} else if (StringUtils.contains(urls, "https://cd")) {
					cityName = "成都";
				} else if (StringUtils.contains(urls, "https://tj")) {
					cityName = "天津";
				} else if (StringUtils.contains(urls, "https://dg")) {
					cityName = "东莞";
				} else if (StringUtils.contains(urls, "https://fs")) {
					cityName = "佛山";
				} else if (StringUtils.contains(urls, "https://zh")) {
					cityName = "珠海";
				} else if (StringUtils.contains(urls, "https://qy")) {
					cityName = "清远";
				}
				Elements resblockName = elIterator.next().select(".resblock-name");
				if (null == resblockName)
					continue;
				Elements selectA = resblockName.select("a");
				if (null == selectA)
					continue;
				String selectHref = selectA.attr("href");
				if (StringUtils.isBlank(selectHref))
					continue;
				String houseUrl = urls + selectHref;
				Document houseDocument = null;
				// 解决连接超时

				try {
					res = Jsoup.connect(houseUrl).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom()))
							.timeout(50000).postDataCharset("utf-8").execute();
					houseDocument = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), houseUrl);
				} catch (Exception e) {
					try {
						houseDocument = Jsoup.parse(new String(res.bodyAsBytes(), "utf-8"), houseUrl);
					} catch (Exception e2) {
						continue;
					}
				}
				Thread.sleep(50);
				if (houseDocument == null)
					continue;
				Element priceElement = houseDocument.selectFirst(".junjia");
				Element priceUnitElement = houseDocument.selectFirst(".yuan");
				Element nameElement = houseDocument.selectFirst(".DATA-PROJECT-NAME");
				Element otherNameElement = houseDocument.selectFirst(".other-name");
				Element stateElement = houseDocument.selectFirst(".state");
				// Element typeElement = houseDocument.selectFirst(".popular");
				// // 房屋类型
				Element houseAreaEle = houseDocument.select(".houselist").last();
				String area = null;
				if (null != houseAreaEle) {
					Element houseArea = houseAreaEle.selectFirst(".p1");
					if (null != houseArea) {
						area = houseArea.selectFirst("span").text();
						area = StringUtils.split(area, " ")[1];
						area = StringUtils.split(area, "m²")[0];
					}
				}
				if (null == houseDocument.selectFirst(".box-loupan"))
					continue;
				Elements housElements = houseDocument.selectFirst(".box-loupan").getElementsByAttributeValue("class",
						"desc-p clear");
				if (housElements == null)
					continue;
				Map<String, String> dataMap = new HashMap<String, String>();
				dataMap.put("houseUrlDetail", houseUrl + "xiangqing/");
				for (Element item : housElements) {
					String key = item.selectFirst("span").text();
					String value = item.select("span").last().text();
					dataMap.put(key, value);
				}

				// 爬取基本信息和周边
				ReptileNewHouseModel newHouseModel = ReptileNewHouseConvert.mapToHouseModel(dataMap);
				newHouseModel.setHouseBuildingArea(area);
				newHouseModel.setHouseUrl(houseUrl);
				newHouseModel.setHouseName(nameElement.text());
				if (null != otherNameElement) {
					newHouseModel.setHouseOtherName(otherNameElement.text().replace("别名/", ""));
				}
				if (null != stateElement) {
					newHouseModel.setHouseSaleStatus(stateElement.text());
				}
				if (null != priceUnitElement && null != priceElement) {
					if (StringUtils.contains(priceUnitElement.text(), "米")
							|| StringUtils.contains(priceUnitElement.text(), "m²")
							|| StringUtils.contains(priceUnitElement.text(), "平")) {
						try {
							String price = priceElement.text();
							if (StringUtils.isBlank(price)) {
								continue;
							}
							if (StringUtils.contains(price, "元")) {
								int flag = price.indexOf("元");
								price = price.substring(0, flag);
							}
							if (StringUtils.contains(price, ",")) {
								price = price.replace(",", "");
							}
							if (StringUtils.contains(price, ".")) {
								price = price.substring(0, price.indexOf("."));
							}
							int price_int;
							price_int = Integer.parseInt(price);
							if (price_int <= 0) {
								continue;
							}
							newHouseModel.setHousePrice(Integer.toString(price_int));
						} catch (Exception e) {
							continue;
						}
					}
					if (StringUtils.contains(priceUnitElement.text(), "套")) {
						try {
							if (StringUtils.isNotBlank(area)) {
								Long price = (Long.valueOf(priceElement.text()) * 10000) / Long.valueOf(area);
								if (price <= 0) {
									continue;
								}
								newHouseModel.setHousePrice(String.valueOf(price));
							} else {
								continue;
							}
						} catch (Exception e) {
							continue;
						}
					}
				} else {
					continue;
				}
				if (StringUtils.isEmpty(newHouseModel.getHousePrice()))
					continue;
				newHouseModel.setInformationSources("链家");
				newHouseModel.setHouseCity(cityName);
				newHouseModel.setHousePart(CityAreaConstants.NEW_HOUSE_CITY.keyMap.get(part));
				newHouseModel.setIsNewHouse("新房");
				newHouseModel.setInfoUpdateTime(
						DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd") + " 00:00:00"));

				System.out.println("楼盘名：" + newHouseModel.getHouseName());
				System.out.println("楼盘别名：" + newHouseModel.getHouseOtherName());
				System.out.println("楼盘地址：" + newHouseModel.getHouseAddress());
				System.out.println("楼盘类型：" + newHouseModel.getHouseType());
				System.out.println("楼盘城市：" + newHouseModel.getHouseCity());
				System.out.println("楼盘区域：" + newHouseModel.getHousePart());
				System.out.println("楼盘价格：" + newHouseModel.getHousePrice());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
