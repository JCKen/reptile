package com.jrzh.factory.webSpider;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.jrzh.common.exception.ProjectException;
import com.jrzh.common.utils.DateUtil;
import com.jrzh.common.utils.ReflectUtils;
import com.jrzh.contants.CityAreaConstants;
import com.jrzh.factory.base.inf.SpiderInf;
import com.jrzh.factory.gz.GZYushouzheng;
import com.jrzh.framework.bean.SessionUser;
import com.jrzh.mvc.convert.reptile.ReptileNewHouseConvert;
import com.jrzh.mvc.model.reptile.ReptileHousePriceModel;
import com.jrzh.mvc.model.reptile.ReptileNewHouseModel;
import com.jrzh.mvc.search.reptile.ReptileNewHouseSearch;
import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;

public class HouseWorldSpider implements SpiderInf<ReptileNewHouseModel> {

	private final static String CITY_URL = "https://{0}.newhouse.fang.com/house/s/";

	private final static Pattern PATTERN = Pattern.compile("//(.*?).fang.com/'");

	private final static Pattern HOUSE_ID_PATTEN = Pattern.compile("house/\\d{9,11}");

	private static Log log = LogFactory.getLog(HouseWorldSpider.class);

	public void saveHouseInfoFromHouseWorldInGZ(ReptileServiceManage reptileServiceManage, SessionUser user)
			throws ProjectException {
		HouseWorldSpider houseWorldSpider = new HouseWorldSpider();

		try {
			for (String city : CityAreaConstants.HOUSE_WORLD_AREA.cityMap) {
				String url = MessageFormat.format(CITY_URL, city);
				ArrayList<String> list = new ArrayList<>();
				list = CityAreaConstants.HOUSE_WORLD_AREA.valueMap.get(city);
				for (String part : list) {
					houseWorldSpider.spider(url, part, 1, reptileServiceManage, user);
				}
			}
		} catch (ProjectException e) {
			e.printStackTrace();
			throw e;
		}

		// 单个城市数据测试

		/*
		 * try { String url = MessageFormat.format(CITY_URL,
		 * CityAreaConstants.HOUSE_WORLD_AREA.cityMap[8]);
		 * 
		 * ArrayList<String> list = new ArrayList<>(); list =
		 * CityAreaConstants.HOUSE_WORLD_AREA.valueMap.get(CityAreaConstants.
		 * HOUSE_WORLD_AREA.cityMap[8]); for (String part : list) {
		 * houseWorldSpider.spider(url, part, 1, reptileServiceManage, user); } } catch
		 * (ProjectException e) { e.printStackTrace(); throw e; }
		 */

	}

	@Override
	public void spider(String url, String part, Integer index, ReptileServiceManage reptileServiceManage,
			SessionUser user) throws ProjectException {
		if (StringUtils.contains(url, "https://bj")) {
			url = "https://newhouse.fang.com/house/s/";
		}
		if (StringUtils.contains(url, "https://hz")) {
			url = "https://huizhou.newhouse.fang.com/house/s/";
		}
		if (StringUtils.contains(url, "https://xa")) {
			url = "https://xian.newhouse.fang.com/house/s/";
		}
		Date infoUpdateTime = new Date();
		Document document = new Document("");
		Response res = null;
		String connect = url + part + "/b9" + index;
		try {
			res = Jsoup.connect(connect).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom())).timeout(50000)
					.postDataCharset("gb18030").execute();
		} catch (Exception e) {
			try {
				res = Jsoup.connect(connect).userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom()))
						.timeout(50000).postDataCharset("gb18030").execute();
			} catch (Exception e2) {
				return;
			}
		}
		try {
			try {
				document = Jsoup.parse(new String(res.bodyAsBytes(), "gb18030"), connect);
			} catch (Exception e) {
				try {
					Thread.sleep(1000);
					document = Jsoup.parse(new String(res.bodyAsBytes(), "gb18030"), connect);
				} catch (Exception e1) {
					if (e1.getMessage().contains("Connection refused: connect")) {
						log.info(url + part + " ：连接被拒绝访问");
					} else {
						log.error(e1);
						e1.printStackTrace();
					}
					return;
				}
			}
			if (document == null)
				return;
			Element noFind = document.selectFirst(".gray3");
			if (noFind != null) {
				String text = noFind.text();
				if (text.contains("没有找到"))
					return;
			}
			Element newHouseEle = document.selectFirst("#newhouse_loupai_list");
			Elements elements = newHouseEle.select(".nlc_details");
			String otherName = null;
			for (Element element : elements) {
				if (StringUtils.equals("fffbf2", element.parent().parent().attr("class")))
					continue;
				Matcher matcher = PATTERN.matcher(element.toString());
				Elements areaEle = element.getElementsByAttributeValue("class", "house_type clearfix");
				String area = null;
				if (null != areaEle) {
					area = areaEle.text();
					if (area.contains(" "))
						area = area.replace(" ", "");
					if (StringUtils.contains(area, "~")) {
						area = StringUtils.split(area, "~")[1];
					} else if (StringUtils.contains(area, "-")) {
						area = StringUtils.split(area, "-")[1];
					}
					area = StringUtils.replace(area, "平米", "");
				}
				if (matcher.find()) {
					// String infoUrl = "https://" + matcher.group().replace("//", "").replace("'",
					// "");
					String infoUrl = null;
					try {
						infoUrl = "https:" + element.selectFirst("a").attr("href");
						Document houseDoc;
						String housePrice = null;
						String priceUnit = null;
						String houseName = null;
						String mainUnit = null;
						try {
							// 解决连接超时
							try {
								res = Jsoup.connect(infoUrl)
										.userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom())).timeout(50000)
										.postDataCharset("gb18030").execute();
								houseDoc = Jsoup.parse(new String(res.bodyAsBytes(), "gb18030"), infoUrl);
							} catch (Exception e) {
								e.printStackTrace();
								try {
									houseDoc = Jsoup.parse(new String(res.bodyAsBytes(), "gb18030"), infoUrl);
								} catch (Exception e2) {
									e2.printStackTrace();
									continue;
								}
							}
							if (null == houseDoc)
								continue;
							Element houseNameEle = houseDoc.selectFirst(".fixnav_tit");
							Element houseNameEle2 = houseDoc.selectFirst(".lp-name");
							Element mainUnitEle = houseDoc.selectFirst("#xfdsxq_B04_13");
							if (mainUnitEle != null) {
								Element zlhx = mainUnitEle.selectFirst(".zlhx");
								if (zlhx != null) {
									mainUnit = zlhx.text();
									if (mainUnit.contains(" "))
										mainUnit = mainUnit.replace(" ", "");
								}
							}
							if (null != houseNameEle) {
								Elements otherNameElement = houseDoc.selectFirst(".inf_left1").selectFirst(".tit")
										.select("span");
								if (null != otherNameElement) {
									otherName = otherNameElement.get(0).attr("title");
								}
								if (null != houseNameEle.select("a")) {
									houseName = houseNameEle.select("a").text();
								}
								Elements priceEles = houseDoc.getElementsByAttributeValue("class", "inf_left fl");
								if (null != priceEles) {
									String priceDesc = priceEles.first().text();
									Element priceE = priceEles.first().selectFirst(".prib");
									if (null != priceE) {
										if (StringUtils.contains(priceDesc, "米")
												|| StringUtils.contains(priceDesc, "m²")
												|| StringUtils.contains(priceDesc, "平")) {
											housePrice = priceE.text();
										}
										if (StringUtils.contains(priceDesc, "套")) {
											// 获取单价为套的房子的平均价
											housePrice = String.valueOf(
													(Long.valueOf(priceE.text()) * 10000) / Long.valueOf(area));
										}
									}
								}
							} else if (null != houseNameEle2) {
								Element otherNameElement = houseDoc.selectFirst(".inf_left1");
								if (null != otherNameElement) {
									Element otherNameElements = otherNameElement.selectFirst(".tit");
									if (null != otherNameElements) {
										Elements otherNameElementss = otherNameElements.select("span");
										if (null != otherNameElementss) {
											otherName = otherNameElementss.get(0).attr("title");
										}
									}
								}
								if (null == houseNameEle2.select("span"))
									continue;
								houseName = houseNameEle2.select("span").text();
								Elements priceEles = houseDoc.getElementsByAttributeValue("class", "l-price");
								if (null != priceEles) {
									String priceDesc = null;
									String priceE = null;
									if (null != priceEles.select("strong")) {
										priceDesc = priceEles.select("strong").text().trim();
									}
									if (null != priceEles.select("span")) {
										priceE = priceEles.select("span").text();
									}
									if (StringUtils.contains(priceE, "米") || StringUtils.contains(priceE, "m²")
											|| StringUtils.contains(priceE, "平")) {
										priceUnit = "1";
									}
									if (StringUtils.contains(priceE, "套")) {
										priceUnit = "2";
									}
									housePrice = priceDesc;
									try {
										if (StringUtils.isBlank(housePrice)) {
											continue;
										}
										if (Integer.parseInt(housePrice) <= 0) {
											continue;
										}
									} catch (Exception e) {
										continue;
									}
								}
							} else {
								continue;
							}
							try {
								Thread.sleep(500);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						} catch (Exception e) {
							continue;
						}
						if (StringUtils.isEmpty(housePrice) || StringUtils.contains(housePrice, "暂无")
								|| StringUtils.contains(housePrice, "待定"))
							continue;
						String cityName = null;
						if (StringUtils.contains(url, "https://gz")) {
							cityName = "广州";
						} else if (StringUtils.contains(url, "https://sz")) {
							cityName = "深圳";
						} else if (StringUtils.contains(url, "https://newhouse.fang.com/house/s/")) {
							cityName = "北京";
						} else if (StringUtils.contains(url, "https://sh")) {
							cityName = "上海";
						} else if (StringUtils.contains(url, "https://cd")) {
							cityName = "成都";
						} else if (StringUtils.contains(url, "https://tj")) {
							cityName = "天津";
						} else if (StringUtils.contains(url, "https://fs")) {
							cityName = "佛山";
						} else if (StringUtils.contains(url, "https://zh")) {
							cityName = "珠海";
						} else if (StringUtils.contains(url, "https://huizhou")) {
							cityName = "惠州";
						} else if (StringUtils.contains(url, "https://jm")) {
							cityName = "江门";
						} else if (StringUtils.contains(url, "https://xian")) {
							cityName = "西安";
						} else if (StringUtils.contains(url, "https://ks")) {
							cityName = "昆山";
						} else if (StringUtils.contains(url, "https://nb")) {
							cityName = "宁波";
						}

						Element detail = houseDoc.getElementById("xfptxq_B03_08");

						if (detail != null) {
							String houseUrl = null;
							if (!detail.attr("href").contains("https:")) {
								houseUrl = "https:" + detail.attr("href");
							}
							Document infohouseDoc;
							try {
								res = Jsoup.connect(houseUrl)
										.userAgent(UserAgent.UserAgentList.get(UserAgent.getRandom())).timeout(50000)
										.postDataCharset("gb18030").execute();
								infohouseDoc = Jsoup.parse(new String(res.bodyAsBytes(), "gb18030"), houseUrl);
							} catch (Exception e) {
								log.error("####房天下数据异常###");
								e.printStackTrace();
								continue;
							}
							if (infohouseDoc == null)
								continue;
							Map<String, String> zbMap = new HashMap<String, String>();
							Element zhoubianElement = infohouseDoc.selectFirst(".sheshi_zb");
							if (null != zhoubianElement) {
								Elements zbinfoElements = zhoubianElement.select("li");
								if (null != zbinfoElements) {
									for (Element zbeElement : zbinfoElements) {
										Element keyElement = zbeElement.selectFirst("span");
										if (null == keyElement)
											continue;
										String keyWord = zbeElement.select("span").text();
										String valueWord = zbeElement.text().replaceFirst(keyWord, "");
										zbMap.put(keyWord, valueWord);
									}
								}
							}

							Element priceInfoElement = infohouseDoc.selectFirst(".main-info-price");
							Map<String, String> infoMap = new HashMap<String, String>();
							if (null != priceInfoElement) {
								if (StringUtils.isNotBlank(priceInfoElement.text())) {
									String[] priceArray = StringUtils.split(priceInfoElement.text(), "： ");
									if (priceArray.length > 1) {
										infoMap.put(priceArray[0], priceArray[1]);
									}
								}

							}

							Elements infoElements = infohouseDoc.getElementsByClass("list clearfix");
							Iterator<Element> infoElementsIteraber = infoElements.iterator();
							while (infoElementsIteraber.hasNext()) {
								Elements tableElement = infoElementsIteraber.next().select("li");
								Iterator<Element> tableIterator = tableElement.iterator();
								while (tableIterator.hasNext()) {
									String info = tableIterator.next().text();
									if (StringUtils.isNotBlank(info)) {
										String[] infoArray = StringUtils.split(info, "：");
										if (infoArray.length > 1) {
											infoMap.put(infoArray[0], infoArray[1]);
										}
									}
								}
							}

							Elements infoElements2 = infohouseDoc.getElementsByClass("clearfix list");
							for (Element list : infoElements2) {
								Elements lis = list.select("li");
								for (Element li : lis) {
									String li_info = li.text();
									if (StringUtils.contains(li_info, " ")) {
										li_info = li_info.replace(" ", "");
									}
									if (StringUtils.contains(li_info, "：")) {
										int flag = li_info.indexOf("：");
										infoMap.put(li_info.substring(0, flag), li_info.substring(flag + 1));
									}
									if (StringUtils.contains(li_info, ":")) {
										int flag = li_info.indexOf(":");
										infoMap.put(li_info.substring(0, flag), li_info.substring(flag + 1));
									}
								}
							}

							ReptileNewHouseModel reptileNewHouseModel = ReptileNewHouseConvert
									.houseWorldMapToHouseModel(infoMap);
							reptileNewHouseModel = ReptileNewHouseConvert.getZhoubianInfo(reptileNewHouseModel, zbMap);
							if (StringUtils.isNotBlank(mainUnit)) {
								if (mainUnit.length() <= 64) {
									reptileNewHouseModel.setMainUnit(mainUnit);
								}
							}
							reptileNewHouseModel.setHouseUrl(houseUrl);
							reptileNewHouseModel.setBuildingArea(area);
							reptileNewHouseModel.setHouseName(houseName);
							reptileNewHouseModel.setInformationSources("房天下");
							reptileNewHouseModel.setHousePrice(housePrice);
							reptileNewHouseModel.setHousePriceUnit(priceUnit);
							reptileNewHouseModel.setInfoUpdateTime(
									DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd") + " 00:00:00"));
							reptileNewHouseModel.setHouseOtherName(otherName);
							reptileNewHouseModel.setHouseCity(cityName);
							reptileNewHouseModel.setIsNewHouse("新房");
							reptileNewHouseModel.setHousePart(CityAreaConstants.HOUSE_WORLD_CITY.keyMap.get(part));

							if (StringUtils.isBlank(reptileNewHouseModel.getHouseAddress())) {
								continue;
							} else if (StringUtils.contains(reptileNewHouseModel.getHouseAddress(), "无")) {
								continue;
							}
							// 入库保存，先查询此新房有没有存库
							ReptileNewHouseSearch search = new ReptileNewHouseSearch();
							search.setEqualInformationSources("房天下");
							search.setEqualHousePart(reptileNewHouseModel.getHousePart());
							search.setEqualHouseCity(reptileNewHouseModel.getHouseCity());
							search.setEqualHouseName(reptileNewHouseModel.getHouseName());
							search.setEqualHouseType(reptileNewHouseModel.getHouseType());
							ReptileNewHouseModel newModel = reptileServiceManage.reptileNewHouseService.first(search);
							ReptileHousePriceModel priceModel = new ReptileHousePriceModel();
							priceModel.setHousePriceUnit(priceUnit);
							priceModel.setHousePrice(reptileNewHouseModel.getHousePrice());
							priceModel.setInfoUpdateTime(
									DateUtil.format(DateUtil.format(infoUpdateTime, "yyyy-MM-dd") + " 00:00:00"));
							priceModel.setInfoSource("房天下");
							if (null == newModel) {
								System.out.println("保存房天下新房：" + cityName + " " + reptileNewHouseModel.getHousePart()
										+ " " + reptileNewHouseModel.getHouseName() + ","
										+ reptileNewHouseModel.getHouseOtherName());
								reptileServiceManage.reptileNewHouseService.add(reptileNewHouseModel, user);
								priceModel.setHouseId(reptileNewHouseModel.getId());
								reptileServiceManage.reptileHousePriceService.add(priceModel, user);
							} else {
								System.out.println("修改房天下新房：" + cityName + " " + reptileNewHouseModel.getHousePart()
										+ " " + reptileNewHouseModel.getHouseName() + ","
										+ reptileNewHouseModel.getHouseOtherName());
								ReflectUtils.copySameFieldToTargetFilterNull(reptileNewHouseModel, newModel);
								try {
									reptileServiceManage.reptileNewHouseService.edit(newModel, user);
								} catch (Exception e) {
									newModel.setOther("");
									reptileServiceManage.reptileNewHouseService.edit(newModel, user);
								}
								priceModel.setHouseId(newModel.getId());
								reptileServiceManage.reptileHousePriceService.add(priceModel, user);
							}
							// 预售证
							/*try {
								Element table_part = infohouseDoc.selectFirst(".table-part");
								if (table_part == null)
									continue;
								String TP = table_part.text();
								if (StringUtils.isBlank(TP))
									continue;
								if (TP.contains("预售许可证")) {
									Element table = table_part.selectFirst("table");
									Elements trs = table.select("tr");
									for (Element tr : trs) {
										if (StringUtils.equals(cityName, "广州")) {
											Element td = tr.selectFirst("td");
											if (td != null) {
												if (td.text().contains("预售许可证"))
													continue;
												String no = td.text();
												String next = td.nextElementSibling().text();
												if (valiDateTimeWithLongFormat(next)) {
													String date = DateUtil.format(DateUtil.format(next, "yyyy-MM"))
															.split("-")[0];
													Integer year = Integer.parseInt(date);
													Integer now = Integer.parseInt(DateUtil.format(new Date(), "yyyy"));
													if (now - year <= 3 && now - year >= 0) {
														GZYushouzheng.spider(priceModel.getHouseId(), no,
																CityAreaConstants.HOUSE_WORLD_CITY.keyMap.get(part),
																reptileServiceManage, user);
													}

												}
											}
										}

									}
								}
							} catch (Exception e) {
								log.error(e);
								e.printStackTrace();
							}*/
						}

					} catch (Exception eHttpStatusException) {
						log.error(infoUrl);
						eHttpStatusException.printStackTrace();
						continue;
					}
				}
			}
			Elements nextElement = document.select(".last");
			if (null == nextElement || null == nextElement.last()
					|| !StringUtils.equals(nextElement.last().text(), "尾页"))
				return;

			Integer page = ++index;
			spider(url, part, page, reptileServiceManage, user);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new ProjectException(e.getMessage());
		}
	}

	// 时间正则
	public static boolean valiDateTimeWithLongFormat(String timeStr) {
		String format = "((19|20)[0-9]{2})-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])";
		Pattern pattern = Pattern.compile(format);
		Matcher matcher = pattern.matcher(timeStr);
		if (matcher.matches()) {
			pattern = Pattern.compile("(\\d{4})-(\\d+)-(\\d+).*");
			matcher = pattern.matcher(timeStr);
			if (matcher.matches()) {
				int y = Integer.valueOf(matcher.group(1));
				int m = Integer.valueOf(matcher.group(2));
				int d = Integer.valueOf(matcher.group(3));
				if (d > 28) {
					Calendar c = Calendar.getInstance();
					c.set(y, m - 1, 1);
					int lastDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
					return (lastDay >= d);
				}
			}
			return true;
		}
		return false;
	}
}
