package com.jrzh.tools;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;

public class HttpClientBody {
	
	@SuppressWarnings("deprecation")
	public static String postBody(String url, String json, String charset){
   	 	if(StringUtils.isBlank(url)){
            return null;
   	 	}
   	 	try {
	   		 HttpClient client = new HttpClient();  
	   		 PostMethod post = new PostMethod(url);
	   		 post.setRequestHeader("content-type", "application/x-www-form-urlencoded;charset="+charset);
	   		 post.setRequestBody(json);
	   		 client.executeMethod(post);  
	   		 return post.getResponseBodyAsString();
		} catch (Exception e) {
			e.printStackTrace();
		}
   	 	return null;
   }
}
