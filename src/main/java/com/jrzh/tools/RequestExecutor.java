package com.jrzh.tools;


import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.PropertyFilter;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.google.common.collect.Lists;

public class RequestExecutor {
    /**
    * @param reqUrl
    * @param params
    * @return
    * @throws Exception
    */
   public static String requestByForm(String reqUrl, Object params, String title, String encoding, RequestConfig requestConfig) {
	   org.apache.http.client.HttpClient httpClient = null;
	   org.apache.http.HttpResponse resp = null;
       try {
           HttpPost httpPost = new HttpPost(reqUrl);
           httpPost.setConfig(requestConfig);
           httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
           httpPost.setHeader("X-LINE-ChannelId", "1625718361");
           httpPost.setHeader("X-LINE-ChannelSecret", "f3c09d9f8a6a17bf3a27a1904b05a2aa");
           List<NameValuePair> formparams = parse2formParam(params);
           UrlEncodedFormEntity postEntity = new UrlEncodedFormEntity(formparams, StringUtils.defaultIfBlank(encoding, "UTF-8"));
           httpPost.setEntity(postEntity);
           httpClient = HttpClients.createAuthNonHttpClient();
           resp = httpClient.execute(httpPost);
           System.out.println(resp);
           if (resp.getStatusLine().getStatusCode() != HttpStatus.SC_BAD_REQUEST &&
           		resp.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
               throw new Exception("requestByForm Exception StatusCode:"+resp.getStatusLine().getStatusCode());
           }
           org.apache.http.HttpEntity entity = resp.getEntity();
           String resultContent = EntityUtils.toString(entity, "UTF-8");
           System.out.println(resultContent);
           return resultContent;
       } catch (Exception e) {
           throw new RuntimeException(e.getMessage());
       } finally {
           HttpClientUtils.closeQuietly(resp);
           HttpClientUtils.closeQuietly(httpClient);
       }
   }
   @SuppressWarnings("unchecked")
private static List<NameValuePair> parse2formParam(Object params) {
       List<NameValuePair> nameValuePairs = Lists.newArrayList();
       if (params instanceof Map) {
           for (Map.Entry<String, Object> entry : ((Map<String, Object>) params).entrySet()) {
               if (entry.getValue() != null) {
            	   System.out.println(entry.getValue());
                   nameValuePairs.add(new BasicNameValuePair(entry.getKey(), entry.getValue().toString()));
               }
           }
       } else {
           SerializeFilter[] serializeFilters = new SerializeFilter[]{new PropertyFilter() {
               @Override
               public boolean apply(Object o, String s, Object o1) {
                   if (o1 == null) {
                       return false;
                   }
                   if (o1 instanceof String && StringUtils.isBlank((String) o1)) {
                       return false;
                   }
                   return true;
               }
           }/*, new NullToEmptyFilter()*/};
           String jsonString = JSON.toJSONString(params, serializeFilters/*, SerializerFeature.WriteMapNullValue, SerializerFeature.WriteNullStringAsEmpty*/);
           JSONObject jsonObject = JSON.parseObject(jsonString);
           for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
               nameValuePairs.add(new BasicNameValuePair(entry.getKey(), entry.getValue().toString()));
           }
       }
       System.out.println(nameValuePairs);
       return nameValuePairs;
   }
}
