package com.jrzh.tools;


import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

/**
 * <br>
 * <b>功能：</b>httpClient帮助类<br>
 * <b>作者：</b>rongweiqiao@cspaying.com<br>
 * <b>日期：</b> 2016/1/14 <br>
 * <b>版权所有：<b>乘势科技有限公司版权所有(C) 2015<br>
 */
@SuppressWarnings("deprecation")
public class HttpClients {

    /**
     * 普通http
     */
    public final static String HTTP_TYPE_COMMON = "1";
    /**
     * https无校验
     */
    public final static String HTTP_TYPE_AUTH_NONE = "2";
    /**
     * http单向认证
     */
    public final static String HTTP_TYPE_AUTH_SINGLE = "3";
    /**
     * http双向认证
     */
    public final static String HTTP_TYPE_AUTH_TWO = "4";

    /**
     * 获取httpClient
     *
     * @param httpType
     * @return
     */
    public static HttpClient createHttpClient(String httpType) {
        if (HttpClients.HTTP_TYPE_COMMON.equals(httpType)) {
            return org.apache.http.impl.client.HttpClients.createDefault();
        } else if (HttpClients.HTTP_TYPE_AUTH_NONE.equals(httpType)) {
            return createAuthNonHttpClient();
        } else if (HttpClients.HTTP_TYPE_AUTH_SINGLE.equals(httpType)) {
            throw new UnsupportedOperationException("");
        } else if (HttpClients.HTTP_TYPE_AUTH_TWO.equals(httpType)) {
            throw new UnsupportedOperationException("");
        }
        throw new UnsupportedOperationException("");
    }

    public static HttpClient createAuthNonHttpClient() {
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(5000000).setSocketTimeout(100000).build();
        return createAuthNonHttpClient(requestConfig);
    }

	public static HttpClient createAuthNonHttpClient(RequestConfig requestConfig) {
        SocketConfig socketConfig = SocketConfig.custom().build();
        RegistryBuilder<ConnectionSocketFactory> registryBuilder = RegistryBuilder.<ConnectionSocketFactory>create();
        ConnectionSocketFactory plainSF = new PlainConnectionSocketFactory();
        registryBuilder.register("http", plainSF);
        //指定信任密钥存储对象和连接套接字工厂
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            SSLContext sslContext =SSLContexts.custom().useTLS().loadTrustMaterial(trustStore, new AnyTrustStrategy()).build();
            LayeredConnectionSocketFactory sslSF = new SSLConnectionSocketFactory(sslContext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            registryBuilder.register("https", sslSF);
        } catch (KeyStoreException e) {
            throw new RuntimeException(e);
        } catch (KeyManagementException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        Registry<ConnectionSocketFactory> registry = registryBuilder.build();
        //设置连接参数
        ConnectionConfig connConfig = ConnectionConfig.custom().setCharset(Charset.forName("utf-8")).build();
        //设置连接管理器
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(registry);
        connManager.setDefaultConnectionConfig(connConfig);
        connManager.setDefaultSocketConfig(socketConfig);
        //指定cookie存储对象
        BasicCookieStore cookieStore = new BasicCookieStore();
        return HttpClientBuilder.create().setDefaultCookieStore(cookieStore).setDefaultRequestConfig(requestConfig).setConnectionManager(connManager).build();
    }

    private static class AnyTrustStrategy implements TrustStrategy {

        @Override
        public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            return true;
        }

    }

    /**
     * 发送json格式请求到指定地址
     *
     * @param url
     * @param json
     * @return
     */
	public static String sendRequest(String url, String json) {
        int timeout = 5000;                                     //超时时间
        long responseLength = 0;                         //响应长度
        String responseContent = null;                 //响应内容
        String strResult = "";
        HttpClient httpClient = new DefaultHttpClient();
        wrapClient(httpClient);
        try {
            HttpParams httpParams = httpClient.getParams();
            httpParams.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout);
            httpParams.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, timeout);
            HttpPost httpPost = new HttpPost(url);                        //创建HttpPost
            StringEntity se = new StringEntity(json, "UTF-8");
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            se.setContentEncoding("UTF-8");
            httpPost.setEntity(se);
            HttpResponse response = httpClient.execute(httpPost); //执行POST请求

            // 若状态码为200 ok
            if (response.getStatusLine().getStatusCode() == 200) {
                // 取出回应字串
                strResult = EntityUtils.toString(response.getEntity());
            }
            System.out.println("请求地址: " + httpPost.getURI());
            System.out.println("响应状态: " + response.getStatusLine());
            System.out.println("响应长度: " + responseLength);
            System.out.println("响应内容: " + responseContent);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (httpClient != null)
                httpClient.getConnectionManager().shutdown();//关闭连接,释放资源
        }
        return strResult;
    }

    /**
     * https 不验证证书
     *
     * @param httpClient
     */
	public static void wrapClient(HttpClient httpClient) {
        try {
            X509TrustManager xtm = new X509TrustManager() {   //创建TrustManager
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            //TLS1.0与SSL3.0基本上没有太大的差别，可粗略理解为TLS是SSL的继承者，但它们使用的是相同的SSLContext
            SSLContext ctx = SSLContext.getInstance("TLS");
            //使用TrustManager来初始化该上下文，TrustManager只是被SSL的Socket所使用
            ctx.init(null, new TrustManager[]{xtm}, null);
            //创建SSLSocketFactory
            SSLSocketFactory socketFactory = new SSLSocketFactory(ctx);
            //通过SchemeRegistry将SSLSocketFactory注册到我们的HttpClient上
            httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", 443, socketFactory));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * 发送json格式请求到指定地址
     *
     * @param url
     * @param json
     * @param charset编码
     * @param 普通http
     * @return
     * @throws Exception
     */
    public static String sendHttpRequest(String url, String json, String charset) throws Exception {
        return sendHttpRequest(url, json, charset, HttpClients.HTTP_TYPE_COMMON);
    }

    /**
     * 发送json格式请求到指定地址
     *
     * @param url
     * @param json
     * @param charset编码
     * @param httpType  通讯类型
     *                  1:普通http
     *                  2:https无校验
     *                  3:http单向认证
     *                  4:http双向认证
     * @return
     * @throws Exception
     */
    public static String sendHttpRequest(String url, String json, String charset, String httpType) throws Exception {
        HttpPost httpPost = new HttpPost(url);
        HttpEntity postEntity = new StringEntity(json, charset);
        httpPost.setEntity(postEntity);
        HttpResponse resp = HttpClients.createHttpClient(httpType).execute(httpPost);
        HttpEntity entity = resp.getEntity();
        return EntityUtils.toString(entity, charset);
    }


    public static void main(String[] args) throws Exception {
//        HttpPost httpPost = /*new HttpPost("https://www.one2pay.com.com.csp.");*/
//                new HttpPost("https://gdnybank.cspaying.com/aps/cloudplatform/api/trade.html");
//        HttpEntity postEntity = new StringEntity("test", "utf-8");
//        httpPost.setEntity(postEntity);
//        HttpResponse resp = HttpClients.createHttpClient(HttpClients.HTTP_TYPE_AUTH_NONE).execute(httpPost);
//        HttpEntity entity = resp.getEntity();
//        System.out.print(EntityUtils.toString(entity,"utf-8"));

        String response = sendHttpRequest("https://gdnybank.cspaying.com/aps/cloudplatform/api/trade.html", "test", "utf-8", HttpClients.HTTP_TYPE_AUTH_NONE);
        System.out.println(response);
    }

}
