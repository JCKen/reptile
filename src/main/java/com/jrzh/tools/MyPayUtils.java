package com.jrzh.tools;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jrzh.common.exception.ProjectException;

public class MyPayUtils {
	
	private static Logger log=LoggerFactory.getLogger(MyPayUtils.class);
	
	public static  String sendHttp(String projectID, String buildingID,String startID , String url,int connTimeOut,int socketTimeOut) throws ProjectException{
		HttpClient httpClient = null;
		HttpResponse resp = null;
		try {
			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("Content-type", "application/x-www-form-urlencoded"); 
			httpPost.setHeader("Server", "******");
			httpPost.setHeader("Connection", "keep-alive");
			httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.62 Safari/537.36");
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			NameValuePair projectId = new BasicNameValuePair("projectID", projectID);
			NameValuePair buildingId = new BasicNameValuePair("buildingID", buildingID);
			NameValuePair startId = new BasicNameValuePair("startID", startID);
			params.add(projectId);
			params.add(buildingId);
			params.add(startId);
			httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(connTimeOut).setSocketTimeout(socketTimeOut).build();
			httpClient = HttpClients.createAuthNonHttpClient(requestConfig);
			resp = httpClient.execute(httpPost);
			HttpEntity entity = resp.getEntity();
			String resultContent = EntityUtils.toString(entity,"utf-8");
			return resultContent;
		}catch(Exception e) {
			e.printStackTrace();
			log.info(e.getMessage());
		}finally {
			HttpClientUtils.closeQuietly(resp);
			HttpClientUtils.closeQuietly(httpClient);
		}
		return "";
	}
	
	public static  String sendHttpByGet(String url,int connTimeOut,int socketTimeOut) throws ProjectException{
		HttpClient httpClient = null;
		HttpResponse resp = null;
		try {
			HttpGet httpGet = new HttpGet(url);
			httpGet.setHeader("Content-Type", "application/json;charset=UTF-8");
			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(connTimeOut).setSocketTimeout(socketTimeOut).build();
			httpClient = HttpClients.createAuthNonHttpClient(requestConfig);
			resp = httpClient.execute(httpGet);
			HttpEntity entity = resp.getEntity();
			String resultContent = EntityUtils.toString(entity,"utf-8");
			return resultContent;
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}finally {
			HttpClientUtils.closeQuietly(resp);
			HttpClientUtils.closeQuietly(httpClient);
		}
		return "";
	}
}
