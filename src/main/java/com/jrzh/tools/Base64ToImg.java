package com.jrzh.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;

import com.jrzh.common.utils.DateUtil;
import com.jrzh.config.ProjectConfigration;

import sun.misc.BASE64Decoder;

@Controller
public class Base64ToImg {

	@SuppressWarnings("restriction")
	private static BASE64Decoder decoder = new BASE64Decoder();
	
	
	// base64字符串转化成图片
	@SuppressWarnings("restriction")
	public static String GenerateImage(ProjectConfigration projectConfigration, String imgStr) {
		// 对字节数组字符串进行Base64解码并生成图片

		if (imgStr == null) // 图像数据为空
			return null;
		

		if (StringUtils.contains(imgStr, "data:image/png;base64,"))
			imgStr = imgStr.replace("data:image/png;base64,", "");
		try {
			// Base64解码
			byte[] b = decoder.decodeBuffer(imgStr);
			for (int i = 0; i < b.length; ++i) {
				if (b[i] < 0) {// 调整异常数据
					b[i] += 256;
				}
			}
			// 生成jpeg图片
			// String imgFilePath = projectConfigration.mappingUrl +
			// "file/report/"+".jpg";//新生成的图片
			String dateDir = DateUtil.format(new Date(), "yyyy-MM-dd");
			String ctxPath = projectConfigration.getFileRootUrl() + "file/report/" + dateDir + "/";
			
			File file = new File(ctxPath);
			if (!file.exists()) file.mkdirs();
			String uuid = UUID.randomUUID().toString().replaceAll("\\-", "");// 返回一个随机UUID。
			String suffix = ".jpg";
			String newFileName = "map-" + uuid + (suffix != null ? suffix : "");// 构成新文件名。
			String fileUrl = file.getPath() + "/" + newFileName;
			fileUrl = fileUrl.replace("\\", "/");
			OutputStream out = new FileOutputStream(fileUrl);
			out.write(b);
			out.flush();
			out.close();
			fileUrl = fileUrl.replace(projectConfigration.getFileRootUrl(),projectConfigration.mappingUrl);
			return fileUrl;
		} catch (Exception e) {
			return null;
		}
	}
}
