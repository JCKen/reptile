package com.jrzh.tools;


import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONCreator;
import com.alibaba.fastjson.annotation.JSONField;

/**
 * <br>
 * <b>功能：</b>todo<br>
 */
@SuppressWarnings("serial")
public class NameValue implements Serializable {

    private String name;
    private String value;

    public NameValue() {

    }

    @JSONCreator
    public NameValue(@JSONField(name = "name") String name,
                     @JSONField(name = "value") String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "NameValue [name=" + name + ", value=" + value + "]";
    }
}
