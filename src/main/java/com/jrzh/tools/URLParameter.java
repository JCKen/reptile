package com.jrzh.tools;


/**
 * <br>
 * <b>功能：</b>todo<br>
 */
public class URLParameter extends NameValue {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3675638388396416203L;

	public URLParameter(String name, String value) {
        super(name, value);
    }

    public String encoding() {
        return String.format("%s=%s",
                URLEncodingTools.encoding(getName(), ToolConst.UTF_8, true),
                URLEncodingTools.encoding(getValue(), ToolConst.UTF_8, true));
    }

    @Override
    public String toString() {
        return String.format("[URLParameter name=%s, value=%s]", getName(),getValue());
    }
}
