package com.jrzh.tools;

import java.util.HashMap;
import java.util.Map;

public class UrlTools {
	
	/**
	 * 根据Url 返回地址后面追加的参数，结果为map
	 * **/
	public static Map<String, String> getUrlParams(String url){
        Map<String,String> map = new HashMap<>();
        url = url.replace("?",";");
        if (!url.contains(";")){
             return map;
        }
        if (url.split(";").length > 0){
             String[] arr = url.split(";")[1].split("&");
             for (String s : arr){
                  String key = s.split("=")[0];
                  String value = s.split("=")[1];
                  map.put(key,value);
             }
             return  map;

        }else{
             return map;
        }
   }
}
