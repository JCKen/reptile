package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_4 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_area";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		table(TABLE_NAME, "区域信息", true, 
					pk(),
					jrVarchar("_area_name", "区域名称", 128),
					jrVarchar("_own_city", "所属城市", 128),
					jrVarchar("_latitude_longitude", "经纬度", 128)
				);
	}

}
