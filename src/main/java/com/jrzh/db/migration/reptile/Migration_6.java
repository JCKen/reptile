package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_6 extends BaseMigration {
	private static final String TABLE_NAME = "reptile_ygjy_house_floor_infos";

	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		table(TABLE_NAME, "阳光家缘楼层信息", true, 
				pk(), 
				jrChar("_house_id", "关联房屋信息id", 36),
				jrVarchar("_floor_name", "楼层名称", 512),
				jrVarchar("_building_id", "阳光家缘id（用于查询楼层具体信息）", 512),
				jrVarchar("_status", "抓取状态", 128)
				);

	}

}
