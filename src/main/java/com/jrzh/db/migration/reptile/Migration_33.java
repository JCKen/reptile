package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define;
import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_33 extends BaseMigration {

	private static final String TABLE_NAME = "reptile_template_sum_info";

	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		table(TABLE_NAME, "模板汇总信息", true, 
				pk(),
				column("_user_id", "用户ID", DataTypes.VARCHAR,Define.length(512)),
				column("_template_id", "模板ID", DataTypes.VARCHAR,Define.length(512)),
				column("_one_house_ratio", "一手房占比", DataTypes.VARCHAR,Define.length(512)),
				column("_two_house_ratio", "二手房占比", DataTypes.VARCHAR,Define.length(512)),
				column("_land_ratio", "土地占比", DataTypes.VARCHAR, Define.length(512)),
				column("_one_house_amt", "一手房金额", DataTypes.VARCHAR,Define.length(512)),
				column("_two_house_amt", "二手房金额", DataTypes.VARCHAR,Define.length(512)),
				column("_land_amt", "土地金额", DataTypes.VARCHAR, Define.length(512)),
				column("_refer_amt", "参考金额", DataTypes.VARCHAR, Define.length(512))
			);
	}

}
