package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_41 extends BaseMigration{

	private final static String TABLE_NAME ="reptile_correction_set";
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_41##########");
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_41##########Begin");
		table(TABLE_NAME, "房价修正系数设定表", true, 
			pk(),
			jrChar("_house_id", "竞品id", 36),
			jrChar("_user_id", "设置人", 36),
			jrChar("_correction_id", "修正项id", 36),
			jrVarchar("_correction_value", "修正值", 10)
		);
		log.info("##########执行reptile模块 Migration_41##########Ending");
		
	}

}
