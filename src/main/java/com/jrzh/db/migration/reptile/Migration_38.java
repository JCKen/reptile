package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_38 extends BaseMigration{

	private final static String TABLE_NAME ="reptile_esf_house_price";
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_38##########");
		MigrationHelper.dropColumn("_status", TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_38##########Begin");
		addColumn(TABLE_NAME, "_status", "价格状态（售价 / 成交价）", DataTypes.VARCHAR, 215);
		log.info("##########执行reptile模块 Migration_38##########Ending");
		
	}

}
