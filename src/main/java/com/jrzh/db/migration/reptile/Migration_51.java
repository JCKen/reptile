package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

/**
 * 单点用户信息表
 * @author ych
 *
 */
public class Migration_51 extends BaseMigration{
	
	private static final String TABLE_NAME = "reptile_sso_user";
	
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_51##########");
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_51##########Begin");
		table(TABLE_NAME, "单点用户信息表", true,
				pk(),
				jrVarchar("_user_OID", "员工OID", 30),
				jrVarchar("_staff_code", "工号",30),
				jrVarchar("_user_name", "员工姓名", 50),
				jrVarchar("_org_name", "组织名称", 100),
				jrVarchar("_org_code", "组织编号", 30),
				jrVarchar("_organization_fk", "组织外键",30),
				jrVarchar("_dept_name", "部门名称", 100),
				jrVarchar("_dept_code", "部门编号", 30),
				jrVarchar("_organization_dept_fk", "部门外键",30),
				jrVarchar("_post_fk", "岗位外键", 30),
				jrVarchar("_post_name", "岗位名称",100),
				jrVarchar("_pipeline_code", "所属管线编码", 30),
				jrVarchar("_mobile", "手机", 100),
				jrVarchar("_office_phone", "办公电话",100),
				jrVarchar("_email", "邮件",100),
				jrVarchar("_IDNumberLast6", "身份证后6位", 100),
				jrVarchar("_is_stop", "是否停用", 4),
				jrVarchar("_is_leave_Agent", "是否停用",4),
				jrVarchar("_user_agent_fk", "离职代理人员", 30),
				jrVarchar("_order_code", "排序", 36)
		);
		log.info("##########执行reptile模块 Migration_51##########End");
	}

}
