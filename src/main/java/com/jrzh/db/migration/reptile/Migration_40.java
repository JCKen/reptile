package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_40 extends BaseMigration{

	private final static String TABLE_NAME ="reptile_correction_factor";
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_40##########");
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_40##########Begin");
		table(TABLE_NAME, "房价修正系数设定表", true, 
			pk(),
			jrChar("_pid", "父ID", 36),
			jrVarchar("_correction_name", "修正项", 512),
			jrVarchar("_correction_max", "修正系数最大值", 10),
			jrVarchar("_correction_min", "修正系数最小值", 10)
		);
		log.info("##########执行reptile模块 Migration_40##########Ending");
		
	}

}
