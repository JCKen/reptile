package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_50 extends BaseMigration{

private static final String TABLE_NAME="reptile_template";
	
	@Override
	public void down() {
		MigrationHelper.dropColumn("_img_base64", TABLE_NAME);
		
	}

	@Override
	public void up() {
		addColumn(TABLE_NAME, "_img_base64", "竞品地图base64码", DataTypes.LONGVARCHAR,0);
	}
}
