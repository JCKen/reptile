package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_42 extends BaseMigration{

	private final static String TABLE_NAME ="reptile_template_info";
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_42##########");
		MigrationHelper.dropColumn("_total_correction_value", TABLE_NAME);
		MigrationHelper.dropColumn("_compare_price", TABLE_NAME);
		MigrationHelper.dropColumn("_trade_tax", TABLE_NAME);
		MigrationHelper.dropColumn("_transaction_price", TABLE_NAME);
		MigrationHelper.dropColumn("_sub_price", TABLE_NAME);
		MigrationHelper.dropColumn("_estimate_price", TABLE_NAME);
		MigrationHelper.dropColumn("_floor_price_proportion", TABLE_NAME);
		MigrationHelper.dropColumn("_tudi_use", TABLE_NAME);
		MigrationHelper.dropColumn("_tudi_time", TABLE_NAME);
		MigrationHelper.dropColumn("_tudi_acreage", TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_42##########Begin");
		addColumn(TABLE_NAME, "_total_correction_value", "综合修正系数", DataTypes.VARCHAR, 512);
		addColumn(TABLE_NAME, "_compare_price", "对比价格", DataTypes.VARCHAR, 512);
		addColumn(TABLE_NAME, "_trade_tax", "二手交易税费系数", DataTypes.VARCHAR, 512);
		addColumn(TABLE_NAME, "_transaction_price", "二手房实际成交价", DataTypes.VARCHAR, 512);
		addColumn(TABLE_NAME, "_sub_price", "市场加权价", DataTypes.VARCHAR, 512);
		addColumn(TABLE_NAME, "_estimate_price", "土地预估售价", DataTypes.VARCHAR, 512);
		addColumn(TABLE_NAME, "_floor_price_proportion", "楼面价占比", DataTypes.VARCHAR, 512);
		addColumn(TABLE_NAME, "_tudi_use", "土地用途", DataTypes.VARCHAR, 512);
		addColumn(TABLE_NAME, "_tudi_time", "土地成交时间", DataTypes.VARCHAR, 512);
		addColumn(TABLE_NAME, "_tudi_acreage", "土地面积", DataTypes.VARCHAR, 512);
		log.info("##########执行reptile模块 Migration_42##########Ending");
		
	}

}
