package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define;
import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

/**
 * @author xsc
 * 模板表
 */
public class Migration_24 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_template";
	
	@Override
	public void down() {
		log.info("##########回退reptile模块 Migration_24##########Begin");
		MigrationHelper.dropTable(TABLE_NAME);
		log.info("##########回退reptile模块 Migration_24##########End");
	}

	@Override
	public void up() {
			log.info("##########执行reptile模块 Migration_24##########Begin");
			table(TABLE_NAME, "模板表", true,
				pk(),
				column("_type", "类型（市调或者模板，生成pdf模板）", DataTypes.VARCHAR,Define.length(512)),
				column("_name", "模板名称", DataTypes.VARCHAR,Define.length(512)),
				column("_user_id", "工号", DataTypes.VARCHAR,Define.length(512))
			);
			log.info("##########执行reptile模块 Migration_24##########End");
	}
	

}
