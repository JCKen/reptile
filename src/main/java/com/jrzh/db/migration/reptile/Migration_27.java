package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define;
import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

/**
 * @author xsc
 * 员工表
 */
public class Migration_27 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_sale_info";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
		log.info("##########回退reptile模块 Migration_22##########Begin");
		log.info("##########回退reptile模块 Migration_22##########End");
	}

	@Override
	public void up() {
			log.info("##########执行reptile模块 Migration_22##########Begin");
			table(TABLE_NAME, "北京房管局销售表", true,
				pk(),
				column("_all_number", "成交套数", DataTypes.VARCHAR,Define.length(512)),
				column("_all_area", "成交面积", DataTypes.VARCHAR,Define.length(512)),
				column("_city", "城市(", DataTypes.VARCHAR, Define.length(512)),
				column("_all_price", "成交价格", DataTypes.VARCHAR, Define.length(512)),
				column("_info_updata_time", "时间", DataTypes.TIMESTAMP, Define.length(512))
			);
			log.info("##########执行reptile模块 Migration_22##########End");
	}
	

}
