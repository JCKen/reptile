package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define;
import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

/**
 * @author xsc
 * 员工表
 */
public class Migration_23 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_user";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
		log.info("##########回退reptile模块 Migration_22##########Begin");
		log.info("##########回退reptile模块 Migration_22##########End");
	}

	@Override
	public void up() {
			log.info("##########执行reptile模块 Migration_22##########Begin");
			table(TABLE_NAME, "一手房信息表", true,
				pk(),
				column("_user_id", "员工ID", DataTypes.VARCHAR,Define.length(512)),
				column("_is_stop", "是否停用（0：停用；1：启用）", DataTypes.VARCHAR,Define.length(512)),
				column("_mobile", "员工手机", DataTypes.VARCHAR, Define.length(512)),
				column("_user_name", "员工姓名", DataTypes.VARCHAR, Define.length(512)),
				column("_dept_name", "部门名称", DataTypes.VARCHAR, Define.length(512))
			);
			log.info("##########执行reptile模块 Migration_22##########End");
	}
	

}
