package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_26 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_template";
	
	@Override
	public void down() {
		log.info("##########回退reptile模块 Migration_21##########Begin");
		MigrationHelper.dropTable(TABLE_NAME);
		log.info("##########回退reptile模块 Migration_21##########End");
	}

	@Override
	public void up() {
			log.info("##########执行reptile模块 Migration_21##########Begin");
			addColumn(TABLE_NAME, "_house_type", "房屋类型", DataTypes.VARCHAR,512);
			log.info("##########执行reptile模块 Migration_21##########End");
	}
	

}
