package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_45 extends BaseMigration{

private static final String TABLE_NAME="reptile_gtj_area_info";
	
	@Override
	public void down() {
		MigrationHelper.dropColumn("_url", TABLE_NAME);
		MigrationHelper.dropColumn("_plot_ratio", TABLE_NAME);
		
	}

	@Override
	public void up() {
		addColumn(TABLE_NAME, "_url", "数据链接", DataTypes.VARCHAR, 255);
		addColumn(TABLE_NAME, "_plot_ratio", "容积率", DataTypes.VARCHAR, 255);
	}
}
