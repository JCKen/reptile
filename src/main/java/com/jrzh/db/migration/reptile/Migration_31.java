package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_31 extends BaseMigration{
	
	private static final String TABLE_NAME="reptile_user";
	
	@Override
	public void down() {
		MigrationHelper.dropColumn("_pwd", TABLE_NAME);
	}

	@Override
	public void up() {
		addColumn(TABLE_NAME, "_pwd", "密码", DataTypes.VARCHAR, 516);
	}

}
