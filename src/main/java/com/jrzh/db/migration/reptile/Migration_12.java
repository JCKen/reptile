package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define;
import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

/**
 * 二手房信息表
 * @author Mr.xsc
 *
 */
public class Migration_12 extends BaseMigration{
	
	private static final String TABLE_NAME = "reptile_esf_house";
	
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_12##########");
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_12##########Begin");
		table(TABLE_NAME, "二手房信息表", true,
				pk(),
				column("_house_name", "小区名", DataTypes.VARCHAR ,Define.length(511)),
				column("_house_other_name", "小区别名", Define.DataTypes.VARCHAR, Define.length(511)),
				column("_house_city", "小区所在市", DataTypes.VARCHAR, Define.length(128)),
				column("_house_part", "小区所在区", DataTypes.VARCHAR, Define.length(128)),
				column("_house_small_part", "小区所在小区", DataTypes.VARCHAR, Define.length(128)),
				column("_house_address", "小区详细地址", DataTypes.VARCHAR, Define.length(512)),
				column("_house_type", "房屋户型", DataTypes.VARCHAR, Define.length(512)),
				column("_unit_structure", "户型结构", DataTypes.VARCHAR, Define.length(512)),
				column("_house_floor", "所在楼层", DataTypes.VARCHAR, Define.length(512)),
				column("_building_area", "建筑面积", DataTypes.VARCHAR, Define.length(256)),
				column("_area_covered", "套内面积", DataTypes.VARCHAR, Define.length(256)),
				column("_ladder_ratio", "梯户比例", DataTypes.VARCHAR, Define.length(256)),
				column("_house_orientation", "房屋朝向", DataTypes.VARCHAR, Define.length(256)),
				column("_building_type", "建筑类型", DataTypes.VARCHAR, Define.length(256)),
				column("_building_structure", "建筑结构", DataTypes.VARCHAR, Define.length(256)),
				column("_renovation_condition", "装修情况", DataTypes.VARCHAR, Define.length(256)),
				column("_equipped_with_subway", "配备电梯", DataTypes.VARCHAR, Define.length(256)),
				column("_year_of_property_rights", "产权年限", DataTypes.VARCHAR, Define.length(256)),
				column("_house_price", "楼盘均价", DataTypes.VARCHAR, Define.length(256)),
				column("_listing_time", "挂牌时间", DataTypes.VARCHAR, Define.length(256)),
				column("_year_of_housing", "房屋年限", DataTypes.VARCHAR, Define.length(256)),
				column("_mortgage_information", "抵押信息", DataTypes.VARCHAR, Define.length(256)),
				column("_last_trading_time", "上次交易时间", DataTypes.VARCHAR, Define.length(256)),
				column("_usage_of_houses", "房屋用途", DataTypes.VARCHAR, Define.length(256)),
				column("_housing_filing", "房本备案", DataTypes.VARCHAR, Define.length(256)),
				column("_property_rights", "产权所属", DataTypes.VARCHAR, Define.length(256)),
				column("_is_new_house", "楼盘是否新开（根据网址新开楼盘进行判断）", DataTypes.VARCHAR, Define.length(256)),
				column("_house_all_price", "总价（多少钱一套）", DataTypes.VARCHAR, Define.length(256)),
				column("_information_sources", "信息来源", DataTypes.VARCHAR, Define.length(1024)),
				column("_info_update_time", "信息更新时间", DataTypes.TIMESTAMP, Define.length(1024)),
				column("_traffic", "附近交通", DataTypes.LONGVARCHAR, Define.length(512)),
				column("_kindergarten", "幼儿园", DataTypes.LONGVARCHAR,Define.length(512)),
				column("_school", "中小学", DataTypes.LONGVARCHAR, Define.length(512)),
				column("_power_center", "综合商场", DataTypes.LONGVARCHAR, Define.length(512)),
				column("_hospital", "医院", DataTypes.LONGVARCHAR, Define.length(512)),
				column("_bank", "银行", DataTypes.LONGVARCHAR, Define.length(512)),
				column("_post","邮政", DataTypes.LONGVARCHAR, Define.length(512)),
				column("_other", "其他", DataTypes.LONGVARCHAR, Define.length(512)),
				column("_house_price_unit", "房价单位(1. 元/㎡ 2. 万/套)", DataTypes.LONGVARCHAR, Define.length(512))
		);
		MigrationHelper.addDecimalColumn("_latitude", TABLE_NAME, 32, 16, "_info_update_time");
		MigrationHelper.addDecimalColumn("_longitude", TABLE_NAME, 32, 16, "_latitude");
		log.info("##########执行reptile模块 Migration_12##########End");
	}

}
