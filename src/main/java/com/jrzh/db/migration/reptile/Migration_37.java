package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_37 extends BaseMigration{

	private final static String TABLE_NAME ="reptile_report_config";
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_37##########");
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_37##########Begin");
		table(TABLE_NAME, "市场报告配置表", true, 
				pk(),
				jrVarchar("_user_id", "用户ID", 512),
				jrChar("_pid", "父节点ID", 36),
				jrVarchar("_page_name", "页面名称", 512),
				jrVarchar("_page_url", "页面URL", 512),
				jrInt("_is_check", "页面是否显示(0.隐藏，1.显示)"),
				jrInt("_order_by", "排序顺序")
		);
		log.info("##########执行reptile模块 Migration_37##########Ending");
		
	}

}
