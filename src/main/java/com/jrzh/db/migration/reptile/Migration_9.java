package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_9 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_new_house";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
		log.info("##########回退reptile模块 Migration_09##########Begin");
		MigrationHelper.dropColumn("_peice_unit", TABLE_NAME);
		log.info("##########回退reptile模块 Migration_08##########End");
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_09##########Begin");
		addColumn(TABLE_NAME, "_peice_unit", "价格单位", DataTypes.LONGVARCHAR, 512);
		log.info("##########执行reptile模块 Migration_08##########End");
	}

}
