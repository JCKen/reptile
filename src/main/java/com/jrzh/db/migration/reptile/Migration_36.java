package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_36 extends BaseMigration {

	private static final String TABLE_NAME = "reptile_estate_datum";

	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_35##########Begin");
		table(TABLE_NAME, "宏观数据房地产部分的数据表", true, 
				pk(),
				jrVarchar("_time", "时间",512),
				jrVarchar("_code", "指标编号", 512),
				jrVarchar("_data", "指标值", 512)
		);
		log.info("##########执行reptile模块 Migration_35##########end");
	}

}
