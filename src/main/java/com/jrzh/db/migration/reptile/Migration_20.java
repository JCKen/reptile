package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_20 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_ygjy_house_floor_infos";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
		log.info("##########回退reptile模块 Migration_21##########Begin");
		log.info("##########回退reptile模块 Migration_21##########End");
	}

	@Override
	public void up() {
			log.info("##########执行reptile模块 Migration_21##########Begin");
			addColumn(TABLE_NAME, "_gz_building_id", "广州专属ID", DataTypes.VARCHAR,512);
			log.info("##########执行reptile模块 Migration_21##########End");
	}
	

}
