package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define;
import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

/**
 * 阳光佳缘预售证信息表
 * @author Mr.zwh
 *
 */
public class Migration_14 extends BaseMigration{
	
	private static final String TABLE_NAME = "reptile_ygjy_house_pre_sale_certificate";
	
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_14##########");
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_14##########Begin");
		table(TABLE_NAME, "阳光佳缘预售证信息表", true,
				pk(),
				column("_house_id", "房子ID", DataTypes.VARCHAR, Define.length(1024)),
				column("_house_number_set", "住宅套数", DataTypes.VARCHAR, Define.length(1024)),
				column("_house_area", "住宅面积", DataTypes.VARCHAR, Define.length(1024)),
				column("_business_number_set", "商业套数", DataTypes.VARCHAR, Define.length(1024)),
				column("_business_area", "商业面积", DataTypes.VARCHAR, Define.length(1024)),
				column("_office_number_set", "办公套数", DataTypes.VARCHAR, Define.length(1024)),
				column("_office_area", "办公面积", DataTypes.VARCHAR, Define.length(1024)),
				column("_parking_number_set", "车位套数", DataTypes.VARCHAR, Define.length(1024)),
				column("_parking_area", "车位面积", DataTypes.VARCHAR, Define.length(1024)),
				column("_other_number_set", "其他套数", DataTypes.VARCHAR, Define.length(1024)),
				column("_other_area", "其他面积", DataTypes.VARCHAR, Define.length(1024)),
				column("_pre_word", "预字第", DataTypes.VARCHAR, Define.length(1024)),
				column("_pre_sale_number", "预售幢数", DataTypes.VARCHAR, Define.length(1024)),
				column("_house_built_number", "报建屋数", DataTypes.VARCHAR, Define.length(1024)),
				column("_layer_built_number", "已建层数", DataTypes.VARCHAR, Define.length(1024)),
				column("_total_area_report", "本期报建总面积", DataTypes.VARCHAR, Define.length(1024)),
				column("_above_ground_area", "地上面积", DataTypes.VARCHAR, Define.length(1024)),
				column("_underground_area", "地下面积", DataTypes.VARCHAR, Define.length(1024)),
				column("_pre_sale_gross_floor_area", "本期预售总建筑面积", DataTypes.VARCHAR, Define.length(1024)),
				column("_pre_sale_house_is_mortgaged", "预售房屋占用土地是否抵押", DataTypes.VARCHAR, Define.length(1024)),
				column("_pre_sale_building_supporting_area", "预售楼宇配套面积情况", DataTypes.VARCHAR, Define.length(1024))
		);
		log.info("##########执行reptile模块 Migration_14##########End");
	}

}
