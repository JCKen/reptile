package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define;
import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

/**
 * @author xsc
 * 商品房成交信息表
 */
public class Migration_30 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_commercial_house_info";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
		log.info("##########回退reptile模块 Migration_22##########Begin");
		log.info("##########回退reptile模块 Migration_22##########End");
	}

	@Override
	public void up() {
			log.info("##########执行reptile模块 Migration_22##########Begin");
			table(TABLE_NAME, "商品房成交信息表", true,
				pk(),
				column("_use", "用途", DataTypes.VARCHAR,Define.length(512)),
				column("_transaction_number", "成交套数", DataTypes.VARCHAR,Define.length(512)),
				column("_transaction_area", "成交面积", DataTypes.VARCHAR,Define.length(512)),
				column("_city", "城市", DataTypes.VARCHAR, Define.length(512)),
				column("_part", "区域", DataTypes.VARCHAR, Define.length(512)),
				column("_transaction_price", "成交均价", DataTypes.VARCHAR, Define.length(512)),
				column("_saleable_area", "可售面积", DataTypes.VARCHAR, Define.length(512)),
				column("_available_sets", "可售套数", DataTypes.VARCHAR, Define.length(512)),
				column("_info_updata_time", "时间", DataTypes.TIMESTAMP, Define.length(512))
			);
			log.info("##########执行reptile模块 Migration_22##########End");
	}
	

}
