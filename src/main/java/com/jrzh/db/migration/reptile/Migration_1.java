package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define;
import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

/**
 * 一手房信息表
 * @author Mr.zwh
 *
 */
public class Migration_1 extends BaseMigration{
	
	private static final String TABLE_NAME = "reptile_new_house";
	
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_1##########");
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_1##########Begin");
		table(TABLE_NAME, "一手房信息表", true,
				pk(),
				column("_house_name", "小区名", DataTypes.VARCHAR ,Define.length(511)),
				column("_house_other_name", "小区别名", DataTypes.VARCHAR, Define.length(511)),
				column("_house_building_area", "楼盘建面", DataTypes.VARCHAR, Define.length(511)),
				column("_house_trading_authority", "交易权属", DataTypes.LONGVARCHAR, Define.length(512)),
				column("_house_url", "数据来源楼房链接", DataTypes.LONGVARCHAR, Define.length(512)),
				column("_house_city", "小区所在市", DataTypes.VARCHAR, Define.length(128)),
				column("_house_part", "小区所在区", DataTypes.VARCHAR, Define.length(128)),
				column("_house_address", "小区详细地址", DataTypes.VARCHAR, Define.length(512)),
				column("_house_price", "楼盘均价", DataTypes.VARCHAR, Define.length(256)),
				column("_house_type", "楼盘类型（商业、住宅、别墅、写字楼）", DataTypes.VARCHAR, Define.length(256)),
				column("_house_sale_status", "楼盘销售状态", DataTypes.VARCHAR, Define.length(256)),
				column("_is_new_house", "楼盘是否新开（根据网址新开楼盘进行判断）", DataTypes.VARCHAR, Define.length(256)),
				column("_house_all_price", "总价（多少钱一套）", DataTypes.VARCHAR, Define.length(256)),
				column("_house_tab", "楼盘标签", DataTypes.VARCHAR, Define.length(256)),
				column("_house_grade", "楼盘评分", DataTypes.VARCHAR, Define.length(256)),
				column("_latest_sale_time", "最新开盘时间", DataTypes.VARCHAR, Define.length(256)),
				column("_main_unit", "主力户型", DataTypes.VARCHAR, Define.length(256)),
				column("_sales_office_address", "售楼处地址", DataTypes.VARCHAR, Define.length(256)),
				column("_developers", "开发商", DataTypes.VARCHAR, Define.length(256)),
				column("_property_category", "物业类别", DataTypes.VARCHAR, Define.length(256)),
				column("_building_area", "建筑面积", DataTypes.VARCHAR, Define.length(256)),
				column("_area_covered", "占地面积", DataTypes.VARCHAR, Define.length(256)),
				column("_afforestation_rate", "绿化率", DataTypes.VARCHAR, Define.length(256)),
				column("_plot_ratio", "容积率", DataTypes.VARCHAR, Define.length(256)),
				column("_parking_lot", "车位", DataTypes.VARCHAR, Define.length(256)),
				column("_water_supply_mode", "供水方式", DataTypes.VARCHAR, Define.length(256)),
				column("_power_supply_mode", "供电方式", DataTypes.VARCHAR, Define.length(256)),
				column("_property_right_years", "产权年限", DataTypes.VARCHAR, Define.length(256)),
				column("_property_company", "物业公司", DataTypes.VARCHAR, Define.length(256)),
				column("_planning_households", "规划户数", DataTypes.VARCHAR, Define.length(256)),
				column("_time_of_delivery", "交房时间", DataTypes.VARCHAR, Define.length(256)),
				column("_property_costs", "物业费用", DataTypes.VARCHAR, Define.length(256)),
				column("_information_sources", "信息来源", DataTypes.VARCHAR, Define.length(1024)),
				column("_info_update_time", "信息更新时间", DataTypes.TIMESTAMP, Define.length(1024))
		);
		MigrationHelper.addDecimalColumn("_latitude", TABLE_NAME, 32, 16, "_info_update_time");
		MigrationHelper.addDecimalColumn("_longitude", TABLE_NAME, 32, 16, "_latitude");
		log.info("##########执行reptile模块 Migration_1##########End");
	}

}
