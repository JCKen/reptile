package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_25 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_template_info";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
		log.info("##########回退reptile模块 Migration_21##########Begin");
		log.info("##########回退reptile模块 Migration_21##########End");
	}

	@Override
	public void up() {
			log.info("##########执行reptile模块 Migration_21##########Begin");
			addColumn(TABLE_NAME, "_volume", "成交量", DataTypes.VARCHAR,512);
			addColumn(TABLE_NAME, "_house_name", "楼盘名", DataTypes.VARCHAR,512);
			log.info("##########执行reptile模块 Migration_21##########End");
	}
	

}
