package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_34  extends BaseMigration{
	
	private static final String TABLE_NAME="reptile_template";
	
	@Override
	public void down() {
		MigrationHelper.dropColumn("_file_url", TABLE_NAME);
	}

	@Override
	public void up() {
		addColumn(TABLE_NAME, "_file_url", "文件地址", DataTypes.VARCHAR, 1024);
	}

}
