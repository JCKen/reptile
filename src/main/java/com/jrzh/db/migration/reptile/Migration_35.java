package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_35 extends BaseMigration {

	private static final String TABLE_NAME = "reptile_estate_zb";

	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_34##########Begin");
		table(TABLE_NAME, "宏观数据房地产部分的指标", true, 
				pk(),
				jrVarchar("_zb", "指标名称",512),
				jrVarchar("_unit", "单位", 512),
				jrVarchar("_code", "指标编号", 512),
				jrVarchar("_classify", "属于哪个指标板块", 512)
		);
		log.info("##########执行reptile模块 Migration_34##########end");
	}

}
