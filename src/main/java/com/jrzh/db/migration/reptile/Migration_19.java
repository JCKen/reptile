package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_19 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_esf_house";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
		log.info("##########回退reptile模块 Migration_19##########Begin");
		log.info("##########回退reptile模块 Migration_10##########End");
	}

	@Override
	public void up() {
			log.info("##########执行reptile模块 Migration_19##########Begin");
			addColumn(TABLE_NAME, "_house_trading_authority", "交易权属", DataTypes.LONGVARCHAR,512);
			addColumn(TABLE_NAME, "_house_url", "数据来源楼房链接", DataTypes.LONGVARCHAR,512);
			log.info("##########执行reptile模块 Migration_19##########End");
	}
	

}
