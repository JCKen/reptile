package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;

public class Migration_16 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_ygjy_house_floor_infos";
	
	@Override
	public void down() {
	}

	@Override
	public void up() {
			log.info("##########执行reptile模块 Migration_16##########Begin");
			addColumn(TABLE_NAME, "_sh_start_id", "上海房管局开盘单位ID", DataTypes.LONGVARCHAR,512);
			addColumn(TABLE_NAME, "_sz_start_id", "深圳房管局开盘单位ID", DataTypes.LONGVARCHAR,512);
			addColumn(TABLE_NAME, "_bj_start_id", "北京房管局开盘单位ID", DataTypes.LONGVARCHAR,512);
			log.info("##########执行reptile模块 Migration_16##########End");
	}

}
