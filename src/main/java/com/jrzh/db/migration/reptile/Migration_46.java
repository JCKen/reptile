package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_46 extends BaseMigration{

private static final String TABLE_NAME="reptile_estate_zb";
	
	@Override
	public void down() {
		MigrationHelper.dropColumn("_city", TABLE_NAME);
		MigrationHelper.dropColumn("_menu", TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_46##########Begin");
		addColumn(TABLE_NAME, "_city", "城市", DataTypes.VARCHAR, 255);
		addColumn(TABLE_NAME, "_menu", "所属菜单", DataTypes.VARCHAR, 255);
	}
}
