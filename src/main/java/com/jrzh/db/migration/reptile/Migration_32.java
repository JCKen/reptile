package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_32 extends BaseMigration {
	
	private static final String TABLE_NAME = "reptile_template_info";

	@Override
	public void down() {
		MigrationHelper.dropColumn("_edit_amt", TABLE_NAME);
	}

	@Override
	public void up() {
		addColumn(TABLE_NAME, "_edit_amt", "修正金额", DataTypes.VARCHAR, 128);
	}

}
