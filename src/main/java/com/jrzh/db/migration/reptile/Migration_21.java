package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_21 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_ygjy_house_room_infos";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
		log.info("##########回退reptile模块 Migration_21##########Begin");
		log.info("##########回退reptile模块 Migration_21##########End");
	}

	@Override
	public void up() {
			log.info("##########执行reptile模块 Migration_21##########Begin");
			addColumn(TABLE_NAME, "_lou_ceng", "所在楼层", DataTypes.VARCHAR,512);
			addColumn(TABLE_NAME, "_room_real_area", "套内面积", DataTypes.VARCHAR,512);
			addColumn(TABLE_NAME, "_room_price", "按建筑面积拟售单价", DataTypes.VARCHAR,512);
			addColumn(TABLE_NAME, "_room_real_price", "按套内面积拟售单价", DataTypes.VARCHAR,512);
			addColumn(TABLE_NAME,"_info_update_time", "信息更新时间", DataTypes.TIMESTAMP, 1024);
			log.info("##########执行reptile模块 Migration_21##########End");
	}
	

}
