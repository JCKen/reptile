package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define;
import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

/**
 * 价格信息表
 * @author Mr.zwh
 *
 */
public class Migration_11 extends BaseMigration{
	
	private static final String TABLE_NAME = "reptile_house_price";
	
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_1##########");
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_1##########Begin");
		table(TABLE_NAME, "价格信息表", true,
				pk(),
				column("_house_price", "房价", DataTypes.VARCHAR, Define.length(1024)),
				column("_house_id", "房子ID", DataTypes.VARCHAR, Define.length(1024)),
				column("_house_price_unit", "房价单位(1. 元/㎡ 2. 万/套)", DataTypes.VARCHAR, Define.length(1024)),
				column("_info_source", "房价来源", DataTypes.VARCHAR, Define.length(1024)),
				column("_info_update_time", "信息更新时间", DataTypes.TIMESTAMP, Define.length(1024))
		);
		log.info("##########执行reptile模块 Migration_1##########End");
	}

}
