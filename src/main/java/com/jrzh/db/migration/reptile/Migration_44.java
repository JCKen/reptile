package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_44 extends BaseMigration{

	private final static String TABLE_NAME ="reptile_esf_houses_xiaoqu";
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_44##########");
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_44##########Begin");
		table(TABLE_NAME, "二手房小区信息", true, 
			pk(),
			jrVarchar("_house_name", "小区名", 512),
			jrVarchar("_houses_other_name", "小区别名", 512),
			jrVarchar("_houses_city", "小区所属城市", 512),
			jrVarchar("_houses_part", "小区所属区域", 512),
			jrVarchar("_houses_address", "所在地址", 512),
			jrVarchar("_houses_building_year", "小区建筑年代", 215),
			jrVarchar("_houses_type", "房屋类型", 512),
			jrVarchar("_houses_price", "房屋均价", 512),
			jrVarchar("_year_of_property_rights", "产权年限", 512),
			jrVarchar("_information_sources", "信息来源", 512),
			jrVarchar("_info_update_time", "信息更新时间", 512),
			jrVarchar("_developers", "开发商", 512),
			jrVarchar("_afforestation_rate", "绿化率", 512),
			jrVarchar("_plot_ratio", "容积率", 512),
			jrVarchar("_building_type", "建筑类型", 512),
			jrVarchar("_property_company", "物业公司", 512),
			jrVarchar("_total_households", "房屋总数", 512),
			jrVarchar("_property_costs", "物业费", 512),
			//jrVarchar("_traffic",  "附近交通", 10240),
			jrText("_traffic", "附近交通"),
			jrVarchar("_kindergarten", "幼儿园", 512),
			jrVarchar("_school", "中小学", 512),
			jrVarchar("_power_center", "综合商场", 512),
			jrVarchar("_hospital", "医院", 512),
			jrVarchar("_bank", "银行", 512),
			jrVarchar("_post", "邮政", 512),
			jrVarchar("_other", "其他", 512),
			jrVarchar("_floor_area", "占地面积", 512),
			jrVarchar("_building_area", "建筑面积", 512),
			jrVarchar("_url", "数据访问地址", 1024)
		);
		MigrationHelper.addDecimalColumn("_latitude", TABLE_NAME, 32, 16, "_info_update_time");
		MigrationHelper.addDecimalColumn("_longitude", TABLE_NAME, 32, 16, "_latitude");
		log.info("##########执行reptile模块 Migration_44##########Ending");
		
	}

}
