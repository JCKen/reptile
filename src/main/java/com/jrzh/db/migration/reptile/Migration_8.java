package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_8 extends BaseMigration{
	
	private static final String TABLE_NAME="reptile_new_house";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
		log.info("##########回退reptile模块 Migration_08##########Begin");
		MigrationHelper.dropColumn("_traffic", TABLE_NAME);
		MigrationHelper.dropColumn("_kindergarten", TABLE_NAME);
		MigrationHelper.dropColumn("_school", TABLE_NAME);
		MigrationHelper.dropColumn("_power_center", TABLE_NAME);
		MigrationHelper.dropColumn("_hospital", TABLE_NAME);
		MigrationHelper.dropColumn("_bank", TABLE_NAME);
		MigrationHelper.dropColumn("_post", TABLE_NAME);
		MigrationHelper.dropColumn("_other", TABLE_NAME);
		log.info("##########回退reptile模块 Migration_08##########End");
	}

	@Override
	public void up() {

					log.info("##########执行reptile模块 Migration_08##########Begin");
					addColumn(TABLE_NAME, "_traffic", "附近交通", DataTypes.LONGVARCHAR, 512);
					addColumn(TABLE_NAME, "_kindergarten", "幼儿园", DataTypes.LONGVARCHAR,512);
					addColumn(TABLE_NAME, "_school", "中小学", DataTypes.LONGVARCHAR, 512);
					addColumn(TABLE_NAME, "_power_center", "综合商场", DataTypes.LONGVARCHAR, 512);
					addColumn(TABLE_NAME, "_hospital", "医院", DataTypes.LONGVARCHAR, 512);
					addColumn(TABLE_NAME, "_bank", "银行", DataTypes.LONGVARCHAR, 512);
					addColumn(TABLE_NAME, "_post","邮政", DataTypes.LONGVARCHAR, 512);
					addColumn(TABLE_NAME, "_other", "其他", DataTypes.LONGVARCHAR, 512);
					log.info("##########执行reptile模块 Migration_08##########End");
	}

}
