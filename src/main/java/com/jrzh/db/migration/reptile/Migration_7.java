package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_7 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_ygjy_house_room_infos";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		table(TABLE_NAME, "楼层信息", true, 
					pk(),
					jrChar("_house_id", "关联房源id", 36),
					jrChar("_floor_id", "关联楼层栋id", 36),
					jrVarchar("_room_number", "房号", 64),
					jrVarchar("_room_type", "房型", 128),
					jrVarchar("_room_total_area", "房间总面积", 128),
					jrVarchar("_room_apartment", "户型", 128),
					jrVarchar("_room_status", "房号", 128),
					jrVarchar("_room_mortgage", "抵押状态", 128),
					jrVarchar("_room_sealing_state", "查封状态", 128)
				);
	}

}
