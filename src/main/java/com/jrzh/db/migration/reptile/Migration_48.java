package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_48 extends BaseMigration{

	private final static String TABLE_NAME ="reptile_newHouse_yushouzheng";
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_48##########");
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_48##########Begin");
		table(TABLE_NAME, "预售证信息表", true, 
				pk(),
				jrVarchar("_city", "城市", 512),
				jrVarchar("_part", "区域", 512),
				jrVarchar("_house_id", "一手房id", 512),
				jrVarchar("_no", "预售证号", 512),
				jrVarchar("_project_name", "项目名", 512),
				jrVarchar("_house_count", "住宅预售套数", 512),
				jrVarchar("_house_area", "住宅预售面积（平米）", 512),
				jrVarchar("_business_count", "商业预售套数", 512),
				jrVarchar("_business_area", "商业预售面积（平米）", 512),
				jrVarchar("_office_count", "办公预售套数", 512),
				jrVarchar("_office_area", "办公预售面积（平米）", 512),
				jrVarchar("_carport_count", "车位预售套数", 512),
				jrVarchar("_carport_area", "车位预售面积（平米）", 512),
				jrVarchar("_other_count", "其他预售套数", 512),
				jrVarchar("_other_area", "其他预售面积（平米）", 512),
				jrVarchar("_presell_all_count", "本期预售总套数", 512),
				jrVarchar("_presell_all_area", "本期预售总面积", 512),
				jrVarchar("_opening_date", "发证日期", 512),
				jrVarchar("_office", "发证机关", 512),
				jrVarchar("_hypothecate", "是否抵押", 512),
				jrVarchar("_above_area", "地上面积(平米)", 512),
				jrVarchar("_under_area", "地下面积(平米)", 512),
				jrVarchar("_validity_start", "有效期自", 512),
				jrVarchar("_validity_end", "有效期至", 512),
				jrVarchar("_contacts", "联系人", 512),
				jrVarchar("_builded_floods", "已建层数", 512),
				jrVarchar("_total_unit_count", "本期总单元套数", 512),
				jrVarchar("_total_unit_area", "本期报建总面积", 512),
				jrVarchar("_matching", "预售楼宇配套面积情况", 1024),
				jrVarchar("_house_number", "报建屋数", 1024),
				jrVarchar("_desc", "备注", 2048),
				jrVarchar("_url", "数据来源地址", 2048),
				jrVarchar("_information_sources", "数据来源", 512)
		);
		log.info("##########执行reptile模块 Migration_48##########Ending");
		
	}

}
