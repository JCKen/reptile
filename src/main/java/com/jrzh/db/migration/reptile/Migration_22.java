package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define;
import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

/**
 * @author xsc
 * 模板信息表
 */
public class Migration_22 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_template_info";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
		log.info("##########回退reptile模块 Migration_22##########Begin");
		log.info("##########回退reptile模块 Migration_22##########End");
	}

	@Override
	public void up() {
			log.info("##########执行reptile模块 Migration_22##########Begin");
			table(TABLE_NAME, "模板信息表", true,
				pk(),
				column("_user_id", "用户ID", DataTypes.VARCHAR,Define.length(512)),
				column("_house_id", "楼盘ID", DataTypes.VARCHAR,Define.length(512)),
				column("_template_id", "模板ID", DataTypes.VARCHAR,Define.length(512)),
				column("_correction", "修正值", DataTypes.VARCHAR,Define.length(512)),
				column("_house_price", "楼盘价格", DataTypes.VARCHAR,Define.length(512)),
				column("_house_weight", "楼盘权重", DataTypes.VARCHAR,Define.length(512)),
				column("_house_type", "楼盘类型（一手房或者二手房或者土地）", DataTypes.VARCHAR, Define.length(512)),
				column("_info_update_time", "信息生成时间", DataTypes.TIMESTAMP, Define.length(1024))
			);
			log.info("##########执行reptile模块 Migration_22##########End");
	}
	

}
