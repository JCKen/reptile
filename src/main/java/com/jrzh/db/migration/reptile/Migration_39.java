package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_39 extends BaseMigration{

	private final static String TABLE_NAME ="reptile_user";
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_39##########");
		MigrationHelper.dropColumn("_isAdmin", TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_39##########Begin");
		addColumn(TABLE_NAME, "_is_admin", "是否是管理员(0:否；1：是)",DataTypes.VARCHAR, 10);
		log.info("##########执行reptile模块 Migration_39##########Ending");
		
	}

}
