package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_49 extends BaseMigration{

	private final static String TABLE_NAME ="reptile_newHouse_deal_everydays";
	@Override
	public void down() {
		log.info("##########回滚reptile模块 Migration_49##########");
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_49##########Begin");
		table(TABLE_NAME, "每日新建商品房签约信息", true, 
				pk(),
				jrVarchar("_city", "城市", 512),
				jrVarchar("_part", "区域", 512),
				jrVarchar("_deal_date", "成交日期", 128),
				jrVarchar("_house_count", "住宅预售套数", 512),
				jrVarchar("_house_area", "住宅预售面积（平米）", 512),
				jrVarchar("_business_count", "商业预售套数", 512),
				jrVarchar("_business_area", "商业预售面积（平米）", 512),
				jrVarchar("_office_count", "办公预售套数", 512),
				jrVarchar("_office_area", "办公预售面积（平米）", 512),
				jrVarchar("_carport_count", "车位预售套数", 512),
				jrVarchar("_carport_area", "车位预售面积（平米）", 512),
				jrVarchar("_url", "数据来源地址", 2048),
				jrVarchar("_information_sources", "数据来源", 512)
		);
		log.info("##########执行reptile模块 Migration_497##########Ending");
		
	}

}
