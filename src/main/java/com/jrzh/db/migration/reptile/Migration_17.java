package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define;
import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;

public class Migration_17 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_house_project_name";
	
	@Override
	public void down() {
	}

	@Override
	public void up() {
		log.info("##########执行reptile模块 Migration_17##########Begin");
		table(TABLE_NAME, "项目名称信息总表", true,
				pk(),
				column("_area_name", "区名", DataTypes.VARCHAR ,Define.length(511)),
				column("_city_name", "城市名", Define.DataTypes.VARCHAR, Define.length(511)),
				column("_developer", "开发商", Define.DataTypes.VARCHAR, Define.length(511)),
				column("_project_name", "项目名称", DataTypes.VARCHAR, Define.length(128)),
				column("_project_total_number", "总套数", DataTypes.DOUBLE, Define.length(128)),
				column("_project_total_area", "总面积", DataTypes.DOUBLE, Define.length(512)),
				column("_project_house_number", "住宅套数", DataTypes.DOUBLE, Define.length(256)),
				column("_project_house_area", "住宅面积", DataTypes.DOUBLE, Define.length(256)),
				column("_project_total_house_unsold_number", "可售总套数(未售)", DataTypes.DOUBLE, Define.length(256)),
				column("_project_total_house_unsold_area", "可售总面积(未售)", DataTypes.DOUBLE, Define.length(256)),
				column("_project_house_unsold_number", "可售住宅套数", DataTypes.DOUBLE, Define.length(256)),
				column("_project_house_unsold_area", "可售住宅面积", DataTypes.DOUBLE, Define.length(256)),
				column("_project_total_house_sold_number", "已售总套数", DataTypes.DOUBLE, Define.length(256)),
				column("_project_total_house_sold_area", "已售总面积", DataTypes.DOUBLE, Define.length(256)),
				column("_project_house_sold_number", "已售住宅套数", DataTypes.DOUBLE, Define.length(256)),
				column("_project_house_sold_area", "已售住宅面积", DataTypes.DOUBLE, Define.length(256)),
				column("_project_register_total_house_number", "已登记总套数", DataTypes.DOUBLE, Define.length(256)),
				column("_project_register_total_house_area", "已登记总面积", DataTypes.DOUBLE, Define.length(256)),
				column("_project_register_house_number", "已登记住宅套数", DataTypes.DOUBLE, Define.length(256)),
				column("_project_register_house_area", "已登记住宅面积", DataTypes.DOUBLE, Define.length(256)),
				column("_project_cancellations_house_area", "合同撤消总套数", DataTypes.DOUBLE, Define.length(256)),
				column("_project_total_price_overdue_number", "定价逾期总次数", DataTypes.DOUBLE, Define.length(256)),
				column("_project_id", "房管局项目ID", DataTypes.VARCHAR, Define.length(256)),
				column("_info_update_time", "信息更新时间", DataTypes.TIMESTAMP, Define.length(1024))
			);
		log.info("##########执行reptile模块 Migration_17##########End");
	}

}
