package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_18 extends BaseMigration {

	private static final String TABLE_NAME = "reptile_gtj_area_info";

	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		log.info("##########回退reptile模块 Migration_18##########Begin");
		table(TABLE_NAME, "国土局地块交易表", true, 
				pk(),
				jrVarchar("_area_no", "地块编号", 512),
				jrVarchar("_address", "地块位置", 512),
				jrVarchar("_use", "地块用途", 512),
				jrVarchar("_acreage", "地块面积", 512),
				jrVarchar("_price", "成交价(万元)", 512),
				jrVarchar("_buyer", "竞得人",2048),
				jrVarchar("_from", "来源", 512),
				jrVarchar("_lnt", "经度", 512),
				jrVarchar("_lat", "纬度", 512),
				jrVarchar("_city", "城市", 512),
				jrTimestamp("_time", "出售时间")
			);
	}

}
