package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;

public class Migration_15 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_ygjy_house_infos";
	
	@Override
	public void down() {
	}

	@Override
	public void up() {
			log.info("##########执行reptile模块 Migration_15##########Begin");
			addColumn(TABLE_NAME, "_project_id", "房管局项目ID", DataTypes.LONGVARCHAR,512);
			addColumn(TABLE_NAME, "_city", "城市", DataTypes.LONGVARCHAR,512);
			log.info("##########执行reptile模块 Migration_15##########End");
	}

}
