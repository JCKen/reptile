package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_2 extends BaseMigration {

	private static final String TABLE_NAME = "reptile_ygjy_sale_infos";

	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		table(TABLE_NAME, "阳光家缘-销售统计表", true, 
				pk(),
				jrVarchar("_area", "区域名称", 128),
				jrDouble("_house_set_number", "住宅套数"),
				jrDouble("_house_acreage", "住宅面积"),
				jrDouble("_biz_set_number", "商业套数"),
				jrDouble("_biz_acreage", "商业面积"),
				jrDouble("_office_set_number", "办公套数"),
				jrDouble("_office_acreage", "办公面积"),
				jrDouble("_position_set_number", "办公套数"),
				jrDouble("_position_acreage", "办公面积"),
				jrVarchar("sale_type", "销售类型", 128),
				jrVarchar("_source", "数据源", 128)
			);
		MigrationHelper.addDateTime("_data_update_time", TABLE_NAME, "_source", "数据更新时间");
	}

}
