package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_5 extends BaseMigration {

	private static final String TABLE_NAME = "reptile_ygjy_house_infos";

	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		table(TABLE_NAME, "阳光家缘-房地产信息表", true, 
				 pk(),
				 jrVarchar("_product_name", "项目名称", 512),
				 jrVarchar("_url", "详情地址", 1024),
				 jrVarchar("_sale_url", "详情地址", 1024),
				 jrVarchar("_developer","开发商", 512),
				 jrVarchar("_pre_sale_permit", "预售证", 512),
				 jrVarchar("_project_address", "项目地址", 512),
				 jrVarchar("_administrative_area", "行政区", 512),
				 jrDouble("_land_area", "占地面积"),
				 jrDouble("_construction_area", "建筑面积"),
				 jrDouble("_sold_area", "已售面积"),
				 jrDouble("_unsold_area", "未售面积"),
				 jrDouble("_house_sold", "住宅已售"),
				 jrDouble("_house_unsold", "住宅未售"),
				 jrDouble("_sum_sold", "已售总数"),
				 jrDouble("_sum_unsold", "未售总数"),
				 jrVarchar("_source", "数据源", 128)
				);
		MigrationHelper.addDateTime("_data_update_time", TABLE_NAME, "_sum_unsold", "数据更新时间");
	}

}
