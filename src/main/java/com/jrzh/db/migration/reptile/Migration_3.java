package com.jrzh.db.migration.reptile;

import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_3 extends BaseMigration {
	
	private static final String TABLE_NAME="reptile_city";
	
	@Override
	public void down() {
		MigrationHelper.dropTable(TABLE_NAME);
	}

	@Override
	public void up() {
		table(TABLE_NAME, "城市信息", true, 
					pk(),
					jrVarchar("_city_name", "城市名称", 128),
					jrVarchar("_latitude_longitude", "经纬度", 128)
				);
	}	

}
