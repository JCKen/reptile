package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_47 extends BaseMigration{

private static final String TABLE_NAME="reptile_esf_houses_xiaoqu";
	
	@Override
	public void down() {
		MigrationHelper.dropColumn("_rp_month", TABLE_NAME);
		
	}

	@Override
	public void up() {
		addColumn(TABLE_NAME, "_rp_month", "参考价月份", DataTypes.VARCHAR, 125);
	}
}
