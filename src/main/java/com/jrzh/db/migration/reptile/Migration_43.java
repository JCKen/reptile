package com.jrzh.db.migration.reptile;

import com.eroi.migrate.Define.DataTypes;
import com.jrzh.framework.migration.BaseMigration;
import com.jrzh.framework.migration.MigrationHelper;

public class Migration_43 extends BaseMigration{

private static final String TABLE_NAME="sys_files";
	
	@Override
	public void down() {
		MigrationHelper.dropColumn("_power", TABLE_NAME);
	}

	@Override
	public void up() {
		addColumn(TABLE_NAME, "_power", "权限", DataTypes.VARCHAR, 255);
	}

}
