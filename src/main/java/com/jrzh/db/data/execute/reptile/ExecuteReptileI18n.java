package com.jrzh.db.data.execute.reptile;

import com.jrzh.framework.execute.ExecuteBase;
import com.jrzh.mvc.model.sys.I18nModel;
import com.jrzh.mvc.service.sys.I18nServiceI;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;

public class ExecuteReptileI18n extends ExecuteBase<I18nModel> {

	private static final long serialVersionUID = 4845429107863341289L;

	@Override
	public void execute(ApplicationContext context) {
		try {
			I18nServiceI i18nService = (I18nServiceI) context.getBean("i18nService");
			i18nService.saveInit("reptile");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Boolean equals(I18nModel source, I18nModel target) {
		return StringUtils.equals(source.getCode(), target.getCode())
				&& StringUtils.equals(source.getModel(), target.getModel())
				&& StringUtils.equals(source.getZh(), target.getZh())
				&& StringUtils.equals(source.getEn(), target.getEn())
				&& StringUtils.equals(source.getEn(), target.getTw())
				&& StringUtils.equals(source.getEn(), target.getHk());
	}

}
