package com.jrzh.db.data.execute.reptile;

import com.jrzh.framework.execute.ExecuteBase;
import com.jrzh.mvc.model.sys.ResourcesModel;
import com.jrzh.mvc.service.sys.ResourcesServiceI;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;

import java.io.InputStream;

public class ExecuteReptileResources extends ExecuteBase<ResourcesModel> {
	private static final long serialVersionUID = -3291193236603352223L;

	@Override
	public void execute(ApplicationContext context) {
		try {
			InputStream in = getInputStreamByName("resources", "reptile");
			if (null == in) {
				in = getInitFile("resources", "reptile");
			}
			if (null != in) {
				ResourcesServiceI resourcesService = (ResourcesServiceI) context.getBean("resourcesService");
				resourcesService.saveInit("reptile", in);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Boolean equals(ResourcesModel source, ResourcesModel target) {
		return StringUtils.equals(source.getCode(), target.getCode())
				&& StringUtils.equals(source.getPid(), target.getPid())
				&& StringUtils.equals(source.getName(), target.getName())
				&& StringUtils.equals(source.getIcon(), target.getIcon())
				&& StringUtils.equals(source.getRemark(), target.getRemark())
				&& StringUtils.equals(source.getUrl(), target.getUrl()) && source.getType() == target.getType()
				&& source.getSortNum() == target.getSortNum() && source.getIsDisable() == target.getIsDisable();
	}

}
