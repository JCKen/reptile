package com.jrzh.db.data.execute.reptile;

import java.io.InputStream;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;

import com.jrzh.framework.execute.ExecuteBase;
import com.jrzh.mvc.model.sys.ConfigModel;
import com.jrzh.mvc.service.reptile.ReptileAreaServiceI;
import com.jrzh.mvc.service.reptile.ReptileCityServiceI;
import com.jrzh.mvc.service.sys.ConfigServiceI;

public class ExecuteReptileConfig extends ExecuteBase<ConfigModel> {

	private static final long serialVersionUID = 4845429107863341289L;

	@Override
	public void execute(ApplicationContext context) {
		try {
			String project = "reptile";
			InputStream in = getInputStreamByName("config", project);
			InputStream sysIn = getInputStreamByName("sysConfig", project);
			InputStream cityIn = getInputStreamByName("city", project);
			InputStream areaIn = getInputStreamByName("area", project);
			if (null == in) {
				in = getInitFile("config", project);
			}
			if (null == sysIn) {
				sysIn = getInitFile("sysConfig", project);
			}
			if (null == cityIn) {
				cityIn = getInitFile("city", project);
			}
			if (null == areaIn) {
				areaIn = getInitFile("area", project);
			}

			if (null != in) {
				ConfigServiceI configService = (ConfigServiceI) context.getBean("configService");
				configService.saveInit(project, in);
			}
			if (null != sysIn) {
				ConfigServiceI configService = (ConfigServiceI) context.getBean("configService");
				configService.saveInit(project, sysIn);
			}
			if (null != cityIn) {
				ReptileCityServiceI reptileCityService = (ReptileCityServiceI) context.getBean("reptileCityService");
				reptileCityService.saveInit(project, cityIn);
			}
			if (null != areaIn) {
				ReptileAreaServiceI reptileAreaService = (ReptileAreaServiceI) context.getBean("reptileAreaService");
				reptileAreaService.saveInit(project, areaIn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Boolean equals(ConfigModel source, ConfigModel target) {
		return StringUtils.equals(source.getGroup(), target.getGroup())
				&& StringUtils.equals(source.getName(), target.getName())
				&& StringUtils.equals(source.getValue(), target.getValue())
				&& StringUtils.equals(source.getDataType(), target.getDataType());
	}
}
