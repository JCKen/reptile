//package com.jrzh.thread;
//
//import java.math.BigDecimal;
//import java.net.URLEncoder;
//import java.util.List;
//
//import org.apache.commons.lang.StringUtils;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//
//import com.alibaba.fastjson.JSONObject;
//import com.jrzh.common.exception.ProjectException;
//import com.jrzh.framework.bean.SessionUser;
//import com.jrzh.mvc.model.reptile.ReptileNewHouseModel;
//import com.jrzh.mvc.service.reptile.manager.ReptileServiceManage;
//import com.jrzh.tools.MyPayUtils;
//
//public class HtmlUnitTest extends Thread {
//
////	static Log log = LogFactory.getLog(LatitudeLongitudeThread.class);
////
////	private ReptileServiceManage reptileServiceManage;
////
////	public LatitudeLongitudeThread(ReptileServiceManage reptileServiceManage) {
////		this.reptileServiceManage = reptileServiceManage;
////	}
//
//	@Override
//	public void run() {
//		// 更新坐标方法
//		Object object = new Object();
//		synchronized (object) {
//			List<ReptileNewHouseModel> reptileNewHouseModelList = null;
//			try {
//				reptileNewHouseModelList = reptileServiceManage.reptileNewHouseService.findAll();
//			} catch (ProjectException e1) {
//				e1.printStackTrace();
//			}
//			for (ReptileNewHouseModel reptileNewHouseModel : reptileNewHouseModelList) {
//				String position = reptileNewHouseModel.getHouseCity() + reptileNewHouseModel.getHousePart()
//						+ reptileNewHouseModel.getHouseName();
//				try {// 具体获取坐标方法
//					if (null != reptileNewHouseModel.getLongitude() && null != reptileNewHouseModel.getLatitude())
//						continue;
//					if (StringUtils.isBlank(position)) {
//						continue;
//					}
//					Object[] obj = getJwd(position);
//					if(obj==null){
//						continue;
//					}
//					if(!StringUtils.equals(obj[0].toString(), "地产小区")){
//						position = reptileNewHouseModel.getHouseCity() + reptileNewHouseModel.getHousePart()
//						+ reptileNewHouseModel.getHouseAddress();
//						obj = getJwd(position);
//					}
//					String lng = obj[1].toString();
//					String lat = obj[2].toString();
//					log.info(lat+"----"+lng);
//					reptileNewHouseModel.setLatitude(new BigDecimal(lat));
//					reptileNewHouseModel.setLongitude(new BigDecimal(lng));
//					reptileServiceManage.reptileNewHouseService.edit(reptileNewHouseModel, SessionUser.getSystemUser());
//					Thread.sleep(300);
//				} catch (Exception e) {
//					e.printStackTrace();
//					continue;
//				}
//			}
//		}
//	}
//
//	public static Object[] getJwd(String city , String position) {
//		String[] appKeyArr = { "Zute9dUseygWr8g10lqn8O4nYfeWhb5G", "sXgxtgqipGdLR91g2WPEHec5db3BMrXe",
//				"3B5pgekUc6OYvUGoQcLHnPjumemNlXKP" };
//		Integer index = 0;
//		String requestContent = null;
//		JSONObject obj = null;
//		try {// 具体获取坐标方法
//			position = URLEncoder.encode(position, "utf-8");
//			String appkey = appKeyArr[index];
//			String url = "http://api.map.baidu.com/place/v2/suggestion?query="+position+"&region="+city+"&city_limit=true&output=json&ak="+appkey;
//			requestContent = MyPayUtils.sendHttpByGet(url, 2000000, 20000000);
//			obj = JSONObject.parseObject(requestContent);
//			if (StringUtils.equals(String.valueOf(obj.get("status")), "302")) {
//				appkey = appKeyArr[index++];
//				url = "http://api.map.baidu.com/geocoder/v2/?address=" + position + "&output=json&ak=" + appkey;
//				requestContent = MyPayUtils.sendHttpByGet(url, 2000000, 20000000);
//				obj = JSONObject.parseObject(requestContent);
//			}
//			if (null == obj.get("result"))
//				return null;
//			JSONObject result = JSONObject.parseObject(String.valueOf(obj.get("result")));
//			if (null == result.get("location")) {
//				return null;
//			}
//			String level = String.valueOf(result.get("level"));
//			JSONObject location = JSONObject.parseObject(String.valueOf(result.get("location")));
//			String lng = String.valueOf(location.get("lng"));
//			String lat = String.valueOf(location.get("lat"));
//			return new Object[] { level, lng, lat };
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//}
