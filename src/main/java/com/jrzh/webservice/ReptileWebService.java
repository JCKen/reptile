package com.jrzh.webservice;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ReptileWebService {
	
	 //测试环境地址
    public static String INVOICE_WS_URL = "http://10.1.30.191/webService/IOUserOrgPost.asmx?op=GetUserOrgPost";

	// 调用WS
    public static StringBuffer reptileWebService(String userName,String userPassword) throws Exception {
        //拼接请求报文
        String sendMsg = appendXmlContext(userName,userPassword);
        // 开启HTTP连接ַ
        InputStreamReader isr = null;
        BufferedReader inReader = null;
        StringBuffer result = null;
        OutputStream outObject = null;
        try {
            URL url = new URL(INVOICE_WS_URL);
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            // 设置HTTP请求相关信息
            httpConn.setRequestProperty("Content-Length",
                    String.valueOf(sendMsg.getBytes().length));
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            // 进行HTTP请求
            outObject = httpConn.getOutputStream();
            outObject.write(sendMsg.getBytes());
 
            if (200 != (httpConn.getResponseCode())) {
                throw new Exception("HTTP Request is not success, Response code is " + httpConn.getResponseCode());
            }
            // 获取HTTP响应数据
            isr = new InputStreamReader(httpConn.getInputStream(), "utf-8");
            inReader = new BufferedReader(isr);
            result = new StringBuffer();
            String inputLine;
            while ((inputLine = inReader.readLine()) != null) {
                result.append(inputLine);
            }
            return result;
 
        } catch (Exception e) {
            throw e;
        } finally {
            // 关闭输入流
            if (inReader != null) {
                inReader.close();
            }
            if (isr != null) {
                isr.close();
            }
            // 关闭输出流
            if (outObject != null) {
                outObject.close();
            }
        }
    }
    
    //拼接请求报文
    public static String appendXmlContext(String userName,String userPassword) {
        // 构建请求报文
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        stringBuffer.append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        stringBuffer.append("<soap:Header>");
        stringBuffer.append("<CredentialSoapHeader xmlns=\"http://tempuri.org/\">");
        stringBuffer.append("<UserName>" + userName + "</UserName>");
        stringBuffer.append("<UserPassword>" + userPassword + "</UserPassword>");
        stringBuffer.append("</CredentialSoapHeader>");
        stringBuffer.append("</soap:Header>");
        stringBuffer.append("<soap:Body>");
        stringBuffer.append("<GetUserOrgPost xmlns=\"http://tempuri.org/\">");
        stringBuffer.append("<FType>OutUser</FType>");
        stringBuffer.append("<FDate>2019-01-01</FDate>");
        stringBuffer.append("</GetUserOrgPost>");
        stringBuffer.append("</soap:Body>");
        stringBuffer.append("</soap:Envelope>");
        return stringBuffer.toString();
    }
}
